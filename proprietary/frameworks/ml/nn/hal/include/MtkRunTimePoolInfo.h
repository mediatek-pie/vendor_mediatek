/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_ML_NN_HAL_MTK_RUNTIME_POOL_INFO_H
#define ANDROID_ML_NN_HAL_MTK_RUNTIME_POOL_INFO_H

#include "HalInterfaces.h"
#include "MtkHalUtils.h"

#include <android-base/logging.h>
#include <vector>

namespace android {
namespace nn {

// Used to keep a pointer to each of the memory pools.
//
// In the case of an "mmap_fd" pool, owns the mmap region
// returned by getBuffer() -- i.e., that region goes away
// when the MtkRunTimePoolInfo is destroyed or is assigned to.
class MtkRunTimePoolInfo {
public:
    // If "fail" is not nullptr, and construction fails, then set *fail = true.
    // If construction succeeds, leave *fail unchanged.
    // getBuffer() == nullptr IFF construction fails.
    explicit MtkRunTimePoolInfo(const hidl_memory& hidlMemory, bool* fail);

    explicit MtkRunTimePoolInfo(uint8_t* buffer);

    // Implement move
    MtkRunTimePoolInfo(MtkRunTimePoolInfo&& other);
    MtkRunTimePoolInfo& operator=(MtkRunTimePoolInfo&& other);

    // Forbid copy
    MtkRunTimePoolInfo(const MtkRunTimePoolInfo&) = delete;
    MtkRunTimePoolInfo& operator=(const MtkRunTimePoolInfo&) = delete;

    ~MtkRunTimePoolInfo() { release(); }

    uint8_t* getBuffer() const { return mBuffer; }

    bool update() const;

private:
    void release();
    void moveFrom(MtkRunTimePoolInfo&& other);

    hidl_memory mHidlMemory;     // always used
    uint8_t* mBuffer = nullptr;  // always used
    sp<IMemory> mMemory;         // only used when hidlMemory.name() == "ashmem"
};

bool setRunTimePoolInfosFromHidlMemories(std::vector<MtkRunTimePoolInfo>* poolInfos,
                                         const hidl_vec<hidl_memory>& pools);


} // namespace nn
} // namespace android

#endif // ANDROID_ML_NN_HAL_MTK_RUNTIME_POOL_INFO_H
