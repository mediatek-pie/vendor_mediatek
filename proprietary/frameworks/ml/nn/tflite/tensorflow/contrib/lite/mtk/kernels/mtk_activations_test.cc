/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#include <cstdarg>
#include <gtest/gtest.h>
#include "tensorflow/contrib/lite/interpreter.h"
#include "tensorflow/contrib/lite/kernels/register.h"
#include "tensorflow/contrib/lite/kernels/test_util.h"
#include "tensorflow/contrib/lite/model.h"

namespace tflite {
namespace {

static bool enableNNAPI = false;

using ::testing::ElementsAreArray;

class BasePReluOpModel : public SingleOpModel {
 public:
  BasePReluOpModel(const TensorData input, const TensorData alpha) {
    input_ = AddInput(input);
    alpha_ = AddInput(alpha);
    if (input.type == TensorType_UINT8) {
      output_ = AddOutput({input.type, {}, 0, 0, 1. / 256});
    } else {
      output_ = AddOutput({input.type, {}});
    }
    SetBuiltinOp(BuiltinOperator_PRELU, BuiltinOptions_NONE, 0);
    BuildInterpreter({GetShape(input_), GetShape(alpha_)});
  }

  BasePReluOpModel(const TensorData &input,
                   const TensorData &alpha,
                   const TensorData &output) {
    input_ = AddInput(input);
    alpha_ = AddInput(alpha);
    output_ = AddOutput(output);
    SetBuiltinOp(BuiltinOperator_PRELU, BuiltinOptions_NONE, 0);
    BuildInterpreter({GetShape(input_), GetShape(alpha_)});
  }

 protected:
  int input_;
  int alpha_;
  int output_;
};

class FloatPReluOpModel : public BasePReluOpModel {
 public:
  using BasePReluOpModel::BasePReluOpModel;

  void SetInput(std::initializer_list<float> data) {
    PopulateTensor(input_, data);
  }
  void SetAlpha(std::initializer_list<float> data) {
    PopulateTensor(alpha_, data);
  }
  std::vector<float> GetOutput() { return ExtractVector<float>(output_); }
};

class QuantizedPReluOpModel : public BasePReluOpModel {
 public:
  using BasePReluOpModel::BasePReluOpModel;

  template <typename T>
  void SetInput(std::initializer_list<float> data) {
    QuantizeAndPopulate<T>(input_, data);
  }
  template <typename T>
  void SetAlpha(std::initializer_list<float> data) {
    QuantizeAndPopulate<T>(alpha_, data);
  }

  template <typename T>
  std::vector<T> GetOutput() {
    return ExtractVector<T>(output_);
  }
  template <typename T>
  std::vector<float> GetDequantizedOutput() {
    return Dequantize<T>(ExtractVector<T>(output_), GetScale(output_),
                         GetZeroPoint(output_));
  }
};

TEST(FloatActivationsOpTest, PRelu) {
  FloatPReluOpModel m({TensorType_FLOAT32, {1, 2, 2, 3}},
                      {TensorType_FLOAT32, {1, 1, 3}});

  m.SetInput({
      0.0f, 0.0f, 0.0f,     // Row 1, Column 1
      1.0f, 1.0f, 1.0f,     // Row 1, Column 2
      -1.0f, -1.0f, -1.0f,  // Row 2, Column 1
      -2.0f, -2.0f, -2.0f,  // Row 1, Column 2
  });
  m.SetAlpha({0.0f, 1.0f, 2.0f});
  m.UseNNAPI(tflite::enableNNAPI);
  m.Invoke();
  EXPECT_THAT(m.GetOutput(), ElementsAreArray({
                                 0.0f, 0.0f, 0.0f,    // Row 1, Column 1
                                 1.0f, 1.0f, 1.0f,    // Row 1, Column 2
                                 0.0f, -1.0f, -2.0f,  // Row 2, Column 1
                                 0.0f, -2.0f, -4.0f,  // Row 1, Column 2
                             }));
}

TEST(FloatActivationsOpTest, PReluPerChannelAlpha) {
  FloatPReluOpModel m({TensorType_FLOAT32, {1, 2, 2, 3}},
                      {TensorType_FLOAT32, {3}});

  m.SetInput({
      0.0f, 0.0f, 0.0f,     // Row 1, Column 1
      1.0f, 1.0f, 1.0f,     // Row 1, Column 2
      -1.0f, -1.0f, -1.0f,  // Row 2, Column 1
      -2.0f, -2.0f, -2.0f,  // Row 1, Column 2
  });
  m.SetAlpha({0.0f, 1.0f, 2.0f});
  m.UseNNAPI(tflite::enableNNAPI);
  m.Invoke();
  EXPECT_THAT(m.GetOutput(), ElementsAreArray({
                                 0.0f, 0.0f, 0.0f,    // Row 1, Column 1
                                 1.0f, 1.0f, 1.0f,    // Row 1, Column 2
                                 0.0f, -1.0f, -2.0f,  // Row 2, Column 1
                                 0.0f, -2.0f, -4.0f,  // Row 1, Column 2
                             }));
}

TEST(FloatActivationsOpTest, PReluSingleAlpha) {
  FloatPReluOpModel m({TensorType_FLOAT32, {1, 2, 2, 3}},
                      {TensorType_FLOAT32, {1}});

  m.SetInput({
      0.0f, 0.0f, 0.0f,     // Row 1, Column 1
      1.0f, 1.0f, 1.0f,     // Row 1, Column 2
      -1.0f, -1.0f, -1.0f,  // Row 2, Column 1
      -2.0f, -2.0f, -2.0f,  // Row 1, Column 2
  });
  m.SetAlpha({2.0f});
  m.UseNNAPI(tflite::enableNNAPI);
  m.Invoke();
  EXPECT_THAT(m.GetOutput(), ElementsAreArray({
                                 0.0f, 0.0f, 0.0f,    // Row 1, Column 1
                                 1.0f, 1.0f, 1.0f,    // Row 1, Column 2
                                 -2.0f, -2.0f, -2.0f,  // Row 2, Column 1
                                 -4.0f, -4.0f, -4.0f,  // Row 1, Column 2
                             }));
}

TEST(QuantizedActivationsOpTest, PRelu) {
  const float kMin = -2;
  const float kMax = 2 * 127.f/128.f;
  const float kPReluQuantizedTolerance = (kMax-kMin)/255;
  QuantizedPReluOpModel m(
      {TensorType_UINT8, {1, 2, 2, 3}, kMin, kMax},//input
      {TensorType_UINT8, {1, 1, 3}, 0, 1},//alpha
      {TensorType_UINT8, {1, 2, 2, 3}, kMin, kMax});//output
  m.SetInput<uint8_t>({
      0.0f, 0.0f, 0.0f,     // Row 1, Column 1
      1.0f, 1.0f, 1.0f,     // Row 1, Column 2
      -1.0f, -1.0f, -1.0f,  // Row 2, Column 1
      -2.0f, -2.0f, -2.0f,  // Row 1, Column 2
  });
  m.SetAlpha<uint8_t>({0.0f, 0.5f, 1.0f});
  m.UseNNAPI(tflite::enableNNAPI);
  m.Invoke();
  EXPECT_THAT(m.GetDequantizedOutput<uint8_t>(),
              ElementsAreArray(ArrayFloatNear(
                  {
                      0.0f, 0.0f, 0.0f,    // Row 1, Column 1
                      1.0f, 1.0f, 1.0f,    // Row 1, Column 2
                      0.0f, -0.5f, -1.0f,  // Row 2, Column 1
                      0.0f, -1.0f, -2.0f,  // Row 1, Column 2
                  },
                  kPReluQuantizedTolerance)));
  EXPECT_THAT(m.GetOutput<uint8_t>(),
              ElementsAreArray({128, 128, 128,
                                192, 192, 192,
                                128,  96,  64,
                                128,  64,   0,
              }));
}

TEST(QuantizedActivationsOpTest, PReluPerChannelAlpha) {
  const float kMin = -2;
  const float kMax = 2 * 127.f/128.f;
  const float kPReluQuantizedTolerance = (kMax-kMin)/255;
  QuantizedPReluOpModel m(
      /*input=*/{TensorType_UINT8, {1, 2, 2, 3}, kMin, kMax},
      /*alpha=*/{TensorType_UINT8, {3}, 0, 1},
      /*output=*/{TensorType_UINT8, {1, 2, 2, 3}, kMin, kMax});
  m.SetInput<uint8_t>({
      0.0f, 0.0f, 0.0f,     // Row 1, Column 1
      1.0f, 1.0f, 1.0f,     // Row 1, Column 2
      -1.0f, -1.0f, -1.0f,  // Row 2, Column 1
      -2.0f, -2.0f, -2.0f,  // Row 1, Column 2
  });
  m.SetAlpha<uint8_t>({0.0f, 0.5f, 1.0f});
  m.UseNNAPI(tflite::enableNNAPI);
  m.Invoke();
  EXPECT_THAT(m.GetDequantizedOutput<uint8_t>(),
              ElementsAreArray(ArrayFloatNear(
                  {
                      0.0f, 0.0f, 0.0f,    // Row 1, Column 1
                      1.0f, 1.0f, 1.0f,    // Row 1, Column 2
                      0.0f, -0.5f, -1.0f,  // Row 2, Column 1
                      0.0f, -1.0f, -2.0f,  // Row 1, Column 2
                  },
                  kPReluQuantizedTolerance)));
  EXPECT_THAT(m.GetOutput<uint8_t>(),
              ElementsAreArray({128, 128, 128,
                                192, 192, 192,
                                128,  96,  64,
                                128,  64,   0,
              }));
}

TEST(QuantizedActivationsOpTest, PReluSingleAlpha) {
  const float kMin = -2;
  const float kMax = 2 * 127.f/128.f;
  const float kPReluQuantizedTolerance = (kMax-kMin)/255;
  QuantizedPReluOpModel m(
      /*input=*/{TensorType_UINT8, {1, 2, 2, 3}, kMin, kMax},
      /*alpha=*/{TensorType_UINT8, {1}, 0, 0.5},
      /*output=*/{TensorType_UINT8, {1, 2, 2, 3}, kMin/2, kMax});
  m.SetInput<uint8_t>({
      0.0f, 0.0f, 0.0f,     // Row 1, Column 1
      1.0f, 1.0f, 1.0f,     // Row 1, Column 2
      -1.0f, -1.0f, -1.0f,  // Row 2, Column 1
      -2.0f, -2.0f, -2.0f,  // Row 1, Column 2
  });
  m.SetAlpha<uint8_t>({0.5f});
  m.UseNNAPI(tflite::enableNNAPI);
  m.Invoke();
  EXPECT_THAT(m.GetDequantizedOutput<uint8_t>(),
              ElementsAreArray(ArrayFloatNear(
                  {
                      0.0f, 0.0f, 0.0f,    // Row 1, Column 1
                      1.0f, 1.0f, 1.0f,    // Row 1, Column 2
                      -0.5f, -0.5f, -0.5f,  // Row 2, Column 1
                      -1.0f, -1.0f, -1.0f,  // Row 1, Column 2
                  },
                  kPReluQuantizedTolerance)));
  EXPECT_THAT(m.GetOutput<uint8_t>(),
              ElementsAreArray({85,  85,  85,
                                170, 170, 170,
                                42,  42,  42,
                                0,   0,   0,
              }));
}

}  // namespace
}  // namespace tflite

int main(int argc, char** argv) {
  ::tflite::LogToStderr();
  ::testing::InitGoogleTest(&argc, argv);
  for (int i = 0; i < argc; i++) {
    std::string arg(argv[i]);
    if (arg.find("--USENNAPI") != std::string::npos) {
        tflite::enableNNAPI = true;
        LOG(WARNING) << __FILE__ << " verified by NNAPI";
    }
  }
  return RUN_ALL_TESTS();
}