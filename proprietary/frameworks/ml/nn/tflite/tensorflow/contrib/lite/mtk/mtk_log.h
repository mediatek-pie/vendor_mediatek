/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#ifndef MTK_TENSORFLOW_CONTRIB_LITE_LOG_H_
#define MTK_TENSORFLOW_CONTRIB_LITE_LOG_H_

#include <string>
#include <iostream>
#include <fstream>
#include <android/log.h>
#include <cutils/properties.h>
#include "tensorflow/contrib/lite/context.h"
#include "tensorflow/contrib/lite/schema/schema_generated.h"

#define LOG_E(fmt, ...) \
  if (property_get_bool("debug.mtk_tflite.vlog", true)) { \
    __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, fmt, ##__VA_ARGS__); \
  }
#define LOG_D(fmt, ...) \
  if (property_get_bool("debug.mtk_tflite.vlog", false)) { \
    __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, fmt, ##__VA_ARGS__); \
  }
#define LOG_W(fmt, ...) \
  if (property_get_bool("debug.mtk_tflite.vlog", true)) { \
    __android_log_print(ANDROID_LOG_WARN, LOG_TAG, fmt, ##__VA_ARGS__); \
  }

inline void DumpOpOutput(std::string &kString) {
  char strPath[PROPERTY_VALUE_MAX] = {0};
  std::ofstream kFile;

  if (property_get("debug.mtk_tflite.dump_path", strPath, "") != 0) {
    kFile.open(strPath, std::ofstream::out | std::ofstream::app);
      if (kFile.is_open()) {
        kFile << kString;
        kFile.close();
      }
  } else {
    __android_log_print(ANDROID_LOG_DEBUG, "TFLite OpDump", "%s", kString.c_str());
  }
}

inline void DumpOpOutput(tflite::BuiltinOperator builtin_code,
                         std::vector<TfLiteTensor>& tensor_list) {
  if (!property_get_bool("debug.mtk_tflite.DumpOp", false)) {
    return;
  }

  std::string opName(tflite::EnumNameBuiltinOperator(builtin_code));
  std::string kString;

  kString += std::string("\n-------------------- Execution ----------------\n")
          + std::string("Device Name      : CPU")
          + std::string("\nOperations       : ") + opName + "\n";

  std::vector<TfLiteTensor>::iterator it = tensor_list.begin();
  int index = 0;
  while (it != tensor_list.end()) {
    kString += std::string("\n-------------------- Output [") + std::to_string(index)
            + std::string("] --------------------\n")
            + std::string("\nSize             : ") + std::to_string(it->bytes)
            + std::string("\nResult           : ");

    if (it->type == kTfLiteUInt8) {
      uint8_t* ptr = it->data.uint8;
      for (auto i = 0; i < it->bytes; i++) {
        kString += std::to_string(*ptr);
        if (i != it->bytes - 1) {
          kString += ",";
        }
        ptr++;
      }
      kString += "\n";
    } else if (it->type == kTfLiteFloat32) {
      float* ptr = it->data.f;
      for (auto i = 0; i < it->bytes/sizeof(float); i++) {
        kString += std::to_string(*ptr);
        if (i != it->bytes/sizeof(float) - 1) {
          kString += ",";
        }
        ptr++;
      }
      kString += "\n";
    }

    ++it;
    ++index;
  }

  DumpOpOutput(kString);
}

#endif  // MTK_TENSORFLOW_CONTRIB_LITE_LOG_H_
