/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

cc_defaults {
    name: "libtflite_mtk_extension_defaults",
    header_libs: [
        "libtflite_mtk_headers",
        "libneuralnetworks_headers",
        "libneuropilot_sdk_wrapper_headers",
    ],
    cflags: [
        "-Wall",
        "-Werror",
        "-Wextra",
        "-Wno-unused-parameter",
    ],
}

cc_library_static {
    name: "libtflite_mtk_extension",
    defaults: ["libtflite_mtk_extension_defaults"],
    vendor_available: true,
    //rtti: true,
    srcs: [
        "mtk_nnapi_delegate.cc",
        "mtk_interpreter.cc",
        "mtk_model.cc",
        "mtk_error_reporter.cc",
        "mtk_util.cc",
        "kernels/mtk_register.cc",
        "kernels/mtk_leakyrelu.cc",
        "kernels/mtk_transpose_conv.cc",
        "kernels/mtk_requantize.cc",
        "kernels/mtk_activations.cc",
        "kernels/mtk_opt.cc",
        "kernels/mtk_depth_to_space.cc",
        "kernels/mtk_abs.cc",
        "kernels/mtk_elu.cc",
        "kernels/mtk_reverse.cc",
        "kernels/mtk_min_pooling.cc",
        "kernels/mtk_roi_align.cc",
        "experimental/NeuroPilotTFLite.cpp",
    ],

    cflags: [
        "-DHAVE_NEUROPILOT",
        "-Wno-extern-c-compat",
        "-Wno-mismatched-tags",
        "-Wno-sign-compare",
        "-Wno-unused-lambda-capture",
        "-Wno-unused-variable",
        "-Wno-unused-local-typedef",
        "-Wno-missing-field-initializers",
    ],

    header_libs: [
        "libtflite_mtk_headers",
        "flatbuffer_headers",
        "libeigen",
        "gemmlowp_headers",
    ],

    shared_libs: [
        "libcutils",
        "liblog",
    ],
}

cc_test {
    name: "tflite_mtk_leakyrelu_test",
    defaults: ["tflite_test_defaults"],
    srcs = [
        "kernels/mtk_leakyrelu_test.cc",
    ],
    static_libs: [
        "libtflite_test_util",
    ],
}

cc_test {
    name: "tflite_mtk_transpose_conv_test",
    defaults: ["tflite_test_defaults"],
    srcs = [
        "kernels/mtk_transpose_conv_test.cc",
    ],
    static_libs: [
        "libtflite_test_util",
    ],
}

cc_test {
    name: "tflite_mtk_activations_test",
    defaults: ["tflite_test_defaults"],
    srcs = [
        "kernels/mtk_activations_test.cc",
    ],
    static_libs: [
        "libtflite_test_util",
    ],
}
