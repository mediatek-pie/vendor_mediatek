/* Copyright 2017 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

package com.mediatek.neuropilot;

import android.util.Log;

/** Static utility methods loading the TensorFlowLite runtime. */
public final class TensorFlowLite {
  private static final String TAG = "NeuroPilotTFLite";
  private static final String LIBNAME = "neuropilot_jni";

  private TensorFlowLite() {}

  /** Returns the version of the underlying TensorFlowLite runtime. */
  public static native String version();
  /** Returns the hash of the underlying TensorFlowLite runtime. */
  public static native String hash();

  /**
   * Load the TensorFlowLite runtime C library.
   */
  static boolean init() {
    try {
      System.loadLibrary(LIBNAME);
      Log.d(TAG, "Hash: " + hash());
      return true;
    } catch (UnsatisfiedLinkError e) {
      System.err.println("TensorFlowLite: failed to load native library: " + e.getMessage());
      return false;
    }
  }

  static {
    init();
  }
}
