/* Copyright 2017 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <limits>

#include "tensorflow/contrib/lite/builtin_op_data.h"
#include "tensorflow/contrib/lite/context.h"
#include "tensorflow/contrib/lite/kernels/internal/quantization_util.h"
#include "tensorflow/contrib/lite/mtk/kernels/internal/reference/mtk_reference_ops.h"
#include "tensorflow/contrib/lite/kernels/internal/tensor.h"
#include "tensorflow/contrib/lite/kernels/kernel_util.h"
#include "tensorflow/contrib/lite/kernels/op_macros.h"
#include "tensorflow/contrib/lite/nnapi/NeuralNetworksShim.h"

#include "mtk_log.h"
#define LOG_TAG "MtkLeakyRelu"
#include "tensorflow/contrib/lite/mtk/mtk_util.h"

#include "flatbuffers/flexbuffers.h"

namespace tflite {
namespace ops {
namespace mtk {
namespace leakyrelu {

#define CHECK_NN(x)                                         \
  if (x != ANEURALNETWORKS_NO_ERROR) {                      \
    LOG_E("Aborting since NN returned failure.");           \
    std::cout << "Aborting since NN returned failure."      \
              << __FILE__ << ":" << __LINE__ << std::endl;  \
    exit(1);                                                \
  }

struct OpData {
  float alpha = 0.1;
  int32_t pos_output_multiplier = 0;
  int32_t pos_output_shift = 0;
  int32_t neg_output_multiplier = 0;
  int32_t neg_output_shift = 0;
};

void* Init(TfLiteContext* context, const char* buffer, size_t length) {
  OpData* data = new OpData;

  const uint8_t* buffer_t = reinterpret_cast<const uint8_t*>(buffer);
  const flexbuffers::Map& m = flexbuffers::GetRoot(buffer_t, length).AsMap();
  data->alpha = m["alpha"].AsFloat();

  return data;
}

void Free(TfLiteContext* context, void* buffer) {
  delete reinterpret_cast<OpData*>(buffer);
}

TfLiteStatus Prepare(TfLiteContext* context, TfLiteNode* node) {
  OpData* data = reinterpret_cast<OpData*>(node->user_data);

  TF_LITE_ENSURE_EQ(context, NumInputs(node), 1);
  TF_LITE_ENSURE_EQ(context, NumOutputs(node), 1);
  const TfLiteTensor* input = GetInput(context, node, 0);
  TfLiteTensor* output = GetOutput(context, node, 0);
  TF_LITE_ENSURE_EQ(context, input->type, output->type);

  TfLiteType data_type = input->type;

  if (data_type != kTfLiteFloat32) {
    float alpha = data->alpha;
    // TF_LITE_ENSURE_EQ(context, alpha, 0.1); // Asserttion. Only support 0.1 ?

    float a_scale = (alpha-0)/255;

    float neg_multiplier = input->params.scale * a_scale / output->params.scale;
    float pos_multiplier = input->params.scale / output->params.scale;

    TF_LITE_ENSURE(context, neg_multiplier < 1);

    QuantizeMultiplierSmallerThanOneExp(neg_multiplier, &data->neg_output_multiplier, &data->neg_output_shift);
    if (pos_multiplier > 1) {
        QuantizeMultiplierGreaterThanOne(pos_multiplier, &data->pos_output_multiplier, &data->pos_output_shift);
    } else if (pos_multiplier < 1) {
        QuantizeMultiplierSmallerThanOneExp(pos_multiplier, &data->pos_output_multiplier, &data->pos_output_shift);
        data->pos_output_shift = -1 * data->pos_output_shift;
    } else {  // equal to one
        data->pos_output_multiplier = 0;
        data->pos_output_shift = 0;
    }
  }

  return context->ResizeTensor(context, output,
                               TfLiteIntArrayCopy(input->dims));
}

TfLiteStatus Eval(TfLiteContext* context, TfLiteNode* node) {
  const TfLiteTensor* input = GetInput(context, node, 0);
  TfLiteTensor* output = GetOutput(context, node, 0);
  OpData* data = reinterpret_cast<OpData*>(node->user_data);

  float alpha = data->alpha;

  switch (input->type) {
    case kTfLiteFloat32: {
      size_t elements = input->bytes / sizeof(float);
      float* in = input->data.f;
      float* in_end = in + elements;
      float* out = output->data.f;
      for (; in < in_end; in++, out++) {
          *out = std::max(*in, alpha * (*in));
      }
      return kTfLiteOk;
    }
    break;
    case kTfLiteUInt8: {
      reference_ops::mtk::LeakyRelu(
                        GetTensorData<uint8_t>(input), GetTensorDims(input),
                        GetTensorData<uint8_t>(output), GetTensorDims(output),
                        input->params.zero_point, output->params.zero_point,
                        data->pos_output_multiplier, data->pos_output_shift,
                        data->neg_output_multiplier, data->neg_output_shift);
      return kTfLiteOk;
    }
    break;
    default:
      context->ReportError(context, "Only float32 and uint8 supported currently.");
      return kTfLiteError;
  }
}
}  // namespace leakyrelu

TfLiteRegistration* Register_MTK_LEAKYRELU() {
  static TfLiteRegistration r = {leakyrelu::Init,
                                 leakyrelu::Free,
                                 leakyrelu::Prepare,
                                 leakyrelu::Eval};
  r.custom_name = strdup("MTK_LEAKYRELU");
  return &r;
}

}  // namespace mtk
}  // namespace ops

int32_t add_leaky_relu_params(ANeuralNetworksModel* nn_model,
        std::vector<uint32_t>& augmented_inputs, uint32_t& next_id, void* data) {
  auto add_scalar_float32 = [&nn_model, &augmented_inputs,
                             &next_id](float value) {
    ANeuralNetworksOperandType operand_type{.type = ANEURALNETWORKS_FLOAT32};
    CHECK_NN(ANeuralNetworksModel_addOperand(nn_model, &operand_type))
    CHECK_NN(ANeuralNetworksModel_setOperandValue(nn_model, next_id, &value,
                                                  sizeof(float)))
    augmented_inputs.push_back(next_id++);
  };

  auto builtin = reinterpret_cast<ops::mtk::leakyrelu::OpData*>(data);
  add_scalar_float32(builtin->alpha);
  return mtk::Hash("leakyrelumtk");
}

}  // namespace tflite
