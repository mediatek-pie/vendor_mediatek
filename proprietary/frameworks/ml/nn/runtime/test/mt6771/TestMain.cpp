/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "NeuralNetworksWrapper.h"

#include "Manager.h"
#include "Utils.h"

#include <gtest/gtest.h>
#include <cutils/properties.h>

using namespace android::nn::wrapper;

void setCpuOnly(bool cpuOnly) {
    const char* value = cpuOnly ? "1" : "0";
    property_set("debug.nn.cpuonly", value);
}

void allowCpuFallback(bool allow) {
    const char* value = allow ? "1" : "0";
    property_set("debug.nn.fallback.cpu.supported", value);
}

int runCases(int cases) {
    int ret = 0;
    switch(cases) {
        case 0:
            ::testing::GTEST_FLAG(filter) = "GeneratedCpuTests.*";
            android::nn::DeviceManager::get()->setUseCpuOnly(true);
            ret = RUN_ALL_TESTS();
            android::nn::DeviceManager::get()->setUseCpuOnly(false);
            break;
        case 1:
            ::testing::GTEST_FLAG(filter) = "GeneratedGpuTests.*";
            allowCpuFallback(false);
            ret = RUN_ALL_TESTS();
            allowCpuFallback(true);
            break;
        case 2:
            ::testing::GTEST_FLAG(filter) = "GeneratedApuTests.*";
            allowCpuFallback(false);
            ret = RUN_ALL_TESTS();
            allowCpuFallback(true);
            break;
        default:
            break;
    }
    return ret;
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);

    int ret = 0;
    android::nn::initVLogMask();

#if defined(NEUROPILOT_SANITY)
    for (int i = 0; i < 3; i++) {
        ret = ret || runCases(i);
    }
#else  // NEUROPILOT_SANITY
  #if defined(CPU_CTS)
    // Test with the CPU driver only.
    android::nn::DeviceManager::get()->setUseCpuOnly(true);
  #endif  // CPU_CTS
    ret = RUN_ALL_TESTS();
  #if defined(CPU_CTS)
    // Restore settings.
    android::nn::DeviceManager::get()->setUseCpuOnly(false);
  #endif  // CPU_CTS

#endif


    return ret;
}
