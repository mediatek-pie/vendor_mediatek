// Generated file (from: vendor_softmax_2D_float_random_gen_relaxed.mod.py). Do not edit
// Begin of an example
{
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {1.8179308922f, 0.0646354388f, 1.0954351055f, 0.0312213804f, 0.1423216214f, 0.0211464475f, 1.4575397704f, 1.4328052691f, 0.1544127642f, 0.8646019029f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {}
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.000383377075195312500f, 0.000027559697628021240f, 0.000106737017631530762f, 0.000006321817636489868f, 0.000001482665538787842f, 0.000001981854438781738f, 0.000136971473693847656f, 0.000115931034088134766f, 0.000010021030902862549f, 0.000029087066650390625f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {}
}
}, // End of an example
