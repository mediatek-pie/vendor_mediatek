#
# Copyright (C) 2018 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# model
model = Model()

i1 = Input("input", "TENSOR_FLOAT32", "{2, 5}") # batch = 2, depth = 5
beta = Float32Scalar("beta", 1.5254101852)
output = Output("output", "TENSOR_FLOAT32", "{2, 5}")

# model 1
model = model.Operation("SOFTMAX", i1, beta).To(output)
model = model.RelaxedExecution(True)

# Example 1. Input in operand 0,
input0 = {i1:
          [1.8179308922,0.0646354388,1.0954351055,0.0312213804,0.1423216214,
           0.0211464475,1.4575397704,1.4328052691,0.1544127642,0.8646019029]}

output0 = {output:
           [0.64756584167480469, 0.04464425891637802, 0.21510322391986847, 0.04242575541138649, 0.05026096850633621, 0.04272659122943878, 0.38218724727630615, 0.36803585290908813, 0.05235814303159714, 0.15469217300415039]}

# Instantiate an example
Example((input0, output0))
