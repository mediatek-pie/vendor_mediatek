#
# Copyright (C) 2018 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# model
model = Model()
i1 = Input("op1", "TENSOR_FLOAT32", "{1, 7, 3, 1}") # input 0
cons1 = Int32Scalar("cons1", 1)
cons5 = Int32Scalar("cons5", 5)
pad0 = Int32Scalar("pad0", 0)
act = Int32Scalar("act", 0)
i3 = Output("op3", "TENSOR_FLOAT32", "{1, 3, 3, 1}") # output 0
model = model.Operation("AVERAGE_POOL_2D", i1, pad0, pad0, pad0, pad0, cons1, cons1, cons1, cons5, act).To(i3)
model = model.RelaxedExecution(True)

# Example 1. Input in operand 0,
input0 = {i1: # input 0
          [0.2258824484,0.9706734256,1.6413920364,0.2832369346,0.8185727565,0.0342471476,1.8229091121,0.1073363719,1.4949156007,0.4734207296,0.4183717469,1.1009587853,0.1920909402,1.7236344284,1.8909641336,1.5252055463,0.6745594691,1.5186810196,0.2044424729,0.3467906231,1.3631865107]}
output0 = {i3: # output 0
          [0.59950798749923706, 0.80771768093109131, 1.23249554634094238, 0.85937273502349854, 0.74849498271942139, 1.20795333385467529, 0.84361380338668823, 0.65413850545883179, 1.47374117374420166]}
# Instantiate an example
Example((input0, output0))
