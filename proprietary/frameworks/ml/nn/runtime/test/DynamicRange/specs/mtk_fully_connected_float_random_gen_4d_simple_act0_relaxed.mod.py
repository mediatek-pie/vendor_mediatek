#
# Copyright (C) 2018 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# This test is for testing the input requirements of Fully Connected Op:
# the input's first dimension doesn't have to be the batch size, the
# input is reshaped as needed.

model = Model()
in0 = Input("op1", "TENSOR_FLOAT32", "{4, 1, 5, 1}")
weights = Parameter("op2", "TENSOR_FLOAT32", "{3, 10}", [
      1.6131302902,0.0590406138,1.7770792244,0.4824579224,0.9468421372,1.9934023285,1.1796773092,1.1781081512,1.2027892791,0.1060474049,1.1655493292,1.7728376638,0.5711190424,1.298725547,0.5116630966,0.9323873446,0.6197670253,1.6332614771,1.8247361622,0.3581210411,1.5552388189,1.1624979001,1.0145868245,1.1640163874,1.8952196161,0.6656816524,1.5643875863,1.4265459722,0.3812877888,0.3690465995
])
bias = Parameter("b0", "TENSOR_FLOAT32", "{3}", [0.0639008796,1.2992238741,0.0458298588])
out0 = Output("op3", "TENSOR_FLOAT32", "{2, 3}")
act = Int32Scalar("act", 0)
model = model.Operation("FULLY_CONNECTED", in0, weights, bias, act).To(out0)
model = model.RelaxedExecution(True)

# Example 1. Input in operand 0,
input0 = {in0: # input 0
          [1.4191119092,0.9965572723,0.2086782082,0.5726396362,1.9141479677,1.0001394096,0.5810222866,1.4751238855,1.2333524068,0.2357919453,1.8324837268,1.9268258845,0.6923205188,1.75555907,1.3930527631,0.0196721315,1.0454771158,0.3677317835,1.5742942251,1.4949102337]}
output0 = {out0: # output 0
               [10.79688167572021484, 12.59914779663085938, 12.15372943878173828, 10.28783321380615234, 14.91411113739013672, 13.84693336486816406]}

# Instantiate an example
Example((input0, output0))
