// Generated file (from: mtk_depthwise_conv2d_float_random_gen_relaxed.mod.py). Do not edit
// Begin of an example
{
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.9006898447f, 1.7894135905f, 1.9819008765f, 0.1872001949f, 1.3492162686f, 1.9966081308f, 0.6421406604f, 1.0679345703f, 0.997021895f, 0.5232610875f, 1.3549987615f, 0.9348634965f, 0.2159897651f, 1.8590187391f, 1.4806685743f, 0.1193190469f, 1.1447031068f, 0.546296992f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {}
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {6.864553928375244f, 1.5815883874893188f, 5.51840877532959f, 6.445183277130127f, 8.24923324584961f, 2.2956743240356445f, 3.9080398082733154f, 6.553283214569092f, 5.233180999755859f, 1.166940689086914f, 4.761036396026611f, 6.464462757110596f, 6.110971450805664f, 2.441307306289673f, 3.4234206676483154f, 4.620429515838623f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {}
}
}, // End of an example
