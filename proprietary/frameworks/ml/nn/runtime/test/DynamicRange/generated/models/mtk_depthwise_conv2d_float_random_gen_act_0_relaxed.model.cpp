// Generated file (from: mtk_depthwise_conv2d_float_random_gen_act_0_relaxed.mod.py). Do not edit
void CreateModel(Model *model) {
  OperandType type3(Type::INT32, {});
  OperandType type1(Type::TENSOR_FLOAT32, {1, 2, 2, 4});
  OperandType type0(Type::TENSOR_FLOAT32, {1, 3, 3, 2});
  OperandType type2(Type::TENSOR_FLOAT32, {4});
  // Phase 1, operands
  auto op1 = model->addOperand(&type0);
  auto op2 = model->addOperand(&type1);
  auto op3 = model->addOperand(&type2);
  auto pad0 = model->addOperand(&type3);
  auto act = model->addOperand(&type3);
  auto stride = model->addOperand(&type3);
  auto channelMultiplier = model->addOperand(&type3);
  auto op4 = model->addOperand(&type1);
  // Phase 2, operations
  static float op2_init[] = {1.8997076055f, 0.2746282284f, 1.7518149676f, 1.8707548223f, 1.806924003f, 0.0691976217f, 0.7399854053f, 1.9717478927f, 0.0009707382f, 0.9918566862f, 0.459853818f, 0.9850721822f, 1.322887103f, 0.3043321916f, 0.2622606215f, 0.1805015218f};
  model->setOperandValue(op2, op2_init, sizeof(float) * 16);
  static float op3_init[] = {0.2527918018f, 0.256753258f, 1.6168371771f, 1.582075712f};
  model->setOperandValue(op3, op3_init, sizeof(float) * 4);
  static int32_t pad0_init[] = {0};
  model->setOperandValue(pad0, pad0_init, sizeof(int32_t) * 1);
  static int32_t act_init[] = {0};
  model->setOperandValue(act, act_init, sizeof(int32_t) * 1);
  static int32_t stride_init[] = {1};
  model->setOperandValue(stride, stride_init, sizeof(int32_t) * 1);
  static int32_t channelMultiplier_init[] = {1};
  model->setOperandValue(channelMultiplier, channelMultiplier_init, sizeof(int32_t) * 1);
  model->addOperation(ANEURALNETWORKS_DEPTHWISE_CONV_2D, {op1, op2, op3, pad0, pad0, pad0, pad0, stride, stride, channelMultiplier, act}, {op4});
  // Phase 3, inputs and outputs
  model->identifyInputsAndOutputs(
    {op1},
    {op4});
  // Phase 4: set relaxed execution
  model->relaxComputationFloat32toFloat16(true);
  assert(model->isValid());
}

bool is_ignored(int i) {
  static std::set<int> ignore = {};
  return ignore.find(i) != ignore.end();
}
