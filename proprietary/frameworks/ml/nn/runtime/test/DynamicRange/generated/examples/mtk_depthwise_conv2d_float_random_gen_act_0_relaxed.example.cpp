// Generated file (from: mtk_depthwise_conv2d_float_random_gen_act_0_relaxed.mod.py). Do not edit
// Begin of an example
{
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.9006898447f, 1.7894135905f, 1.9819008765f, 0.1872001949f, 1.3492162686f, 1.9966081308f, 0.6421406604f, 1.0679345703f, 0.997021895f, 0.5232610875f, 1.3549987615f, 0.9348634965f, 0.2159897651f, 1.8590187391f, 1.4806685743f, 0.1193190469f, 1.1447031068f, 0.546296992f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {}
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {6.864553928375244f, 1.9796137809753418f, 9.61327838897705f, 2.575155735015869f, 0.25279179215431213f, 0.25675326585769653f, 1.6168371438980103f, 1.5820757150650024f, 5.233180999755859f, 2.4664394855499268f, 7.4750165939331055f, 2.0750715732574463f, 0.25279179215431213f, 0.25675326585769653f, 1.6168371438980103f, 1.5820757150650024f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {}
}
}, // End of an example
