// Generated file (from: mtk_fully_connected_float_random_gen_4d_simple_act0_relaxed.mod.py). Do not edit
// Begin of an example
{
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {1.4191119092f, 0.9965572723f, 0.2086782082f, 0.5726396362f, 1.9141479677f, 1.0001394096f, 0.5810222866f, 1.4751238855f, 1.2333524068f, 0.2357919453f, 1.8324837268f, 1.9268258845f, 0.6923205188f, 1.75555907f, 1.3930527631f, 0.0196721315f, 1.0454771158f, 0.3677317835f, 1.5742942251f, 1.4949102337f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {}
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {10.796881675720215f, 12.59914779663086f, 12.153729438781738f, 10.287833213806152f, 14.914111137390137f, 13.846933364868164f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {}
}
}, // End of an example
