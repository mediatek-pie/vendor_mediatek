import math

# model
model = Model()
i1 = Input("op1", "TENSOR_QUANT8_ASYMM", "{1, 256, 256, 1}, 1.0f, 0")
i2 = Output("op2", "TENSOR_QUANT8_ASYMM", "{1, 480, 480, 1}, 1.0f, 0")
w = Int32Scalar("width", 480) # an int32_t scalar bias
h = Int32Scalar("height", 480)
model = model.Operation("RESIZE_BILINEAR", i1, w, h).To(i2)

# Example 1. Input in operand 0,
input0 = {i1: # input 0
          [ 5 if y%2==0 else 255 for y in range(256) for x in range(256) ] }
output0 = {i2: # output 0
           [ 255 if y * 256 / 480 > 255 else
             round( y * 256 / 480 % 1 * 250 + 5 if y * 256 / 480 % 2 < 1
              else (1 - y * 256 / 480 % 1) * 250 + 5 ) for y in range(480) for x in range(480) ] }

# Instantiate an example
Example((input0, output0))
