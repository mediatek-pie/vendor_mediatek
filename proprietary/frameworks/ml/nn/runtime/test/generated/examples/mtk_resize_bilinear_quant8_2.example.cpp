// Generated file (from: mtk_resize_bilinear_quant8_2.mod.py). Do not edit
// Begin of an example
{
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {{0, {0, 11, 255, 11, 0, 17, 255, 17}}}
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {{0, {0, 11, 170, 11, 255, 11, 0, 15, 170, 15, 255, 15, 0, 17, 170, 17, 255, 17}}}
}
}, // End of an example
