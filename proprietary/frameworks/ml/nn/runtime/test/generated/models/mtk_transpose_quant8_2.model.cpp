// Generated file (from: mtk_transpose_quant8_2.mod.py). Do not edit
void CreateModel(Model *model) {
  OperandType type1(Type::TENSOR_INT32, {4});
  OperandType type2(Type::TENSOR_QUANT8_ASYMM, {1, 3, 2, 3}, 1.0, 0);
  OperandType type0(Type::TENSOR_QUANT8_ASYMM, {1, 3, 3, 2}, 1.0, 0);
  // Phase 1, operands
  auto input = model->addOperand(&type0);
  auto perms = model->addOperand(&type1);
  auto output = model->addOperand(&type2);
  // Phase 2, operations
  static int32_t perms_init[] = {0, 2, 3, 1};
  model->setOperandValue(perms, perms_init, sizeof(int32_t) * 4);
  model->addOperation(ANEURALNETWORKS_TRANSPOSE, {input, perms}, {output});
  // Phase 3, inputs and outputs
  model->identifyInputsAndOutputs(
    {input},
    {output});
  assert(model->isValid());
}

bool is_ignored(int i) {
  static std::set<int> ignore = {};
  return ignore.find(i) != ignore.end();
}
