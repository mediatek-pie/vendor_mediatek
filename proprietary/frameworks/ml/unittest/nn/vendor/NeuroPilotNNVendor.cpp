/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#include <stdio.h>
#include <gtest/gtest.h>

#include "NeuralNetworks.h"
#include "NeuroPilotNNVendor.h"

#define ROWS        4
#define COLS        4
#define ROWS_OUT    2
#define COLS_OUT    2

typedef float Matrix4x4[4][4];
typedef float MatrixInput[ROWS][COLS];
typedef float MatrixOutput[ROWS_OUT][COLS_OUT];
typedef float MatrixFilter[2][2];
typedef float MatrixBias[1];

static const MatrixInput inputMat = {
    {1.f, 2.f, 3.f, 4.f},
    {5.f, 6.f, 7.f, 8.f},
    {9.f, 10.f, 11.f, 12.f},
    {13.f, 14.f, 15.f, 16.f},
};

static const MatrixFilter filterMat = {
    {1.f, 1.f},
    {1.f, 1.f}
};

static const MatrixOutput matrix = {
    {2.f, 4.f},
    {6.f, 8.f}
};

static const MatrixBias biasMat = {1};

static const MatrixOutput expectedMat = {
    {17.f, 27.f},
    {53.f, 63.f},
};

static int CompareMatrices(const MatrixOutput expected, const MatrixOutput actual) {
    int errors = 0;
    for (int i = 0; i < ROWS_OUT; i++) {
        for (int j = 0; j < COLS_OUT; j++) {
            if (expected[i][j] != actual[i][j]) {
                printf("expected[%d][%d] != actual[%d][%d], %f != %f\n", i, j, i, j,
                       static_cast<double>(expected[i][j]), static_cast<double>(actual[i][j]));
                errors++;
            }
        }
    }
    return errors;
}

int createModelForTest(ANeuralNetworksModel *model) {
    //
    // create operand
    //
    std::vector<uint32_t> dim{1, ROWS, COLS, 1};
    ANeuralNetworksOperandType op = {
        .type              = static_cast<int32_t>(ANEURALNETWORKS_TENSOR_FLOAT32),
        .dimensionCount    = static_cast<uint32_t>(dim.size()),
        .dimensions        = dim.data(),
        .scale             = 0.0f,
        .zeroPoint         = 0
    };

    std::vector<uint32_t> dimOut{1, ROWS_OUT, COLS_OUT, 1};
    ANeuralNetworksOperandType opOut = {
        .type              = static_cast<int32_t>(ANEURALNETWORKS_TENSOR_FLOAT32),
        .dimensionCount    = static_cast<uint32_t>(dimOut.size()),
        .dimensions        = dimOut.data(),
        .scale             = 0.0f,
        .zeroPoint         = 0
    };


    std::vector<uint32_t> filterDim{1, 2, 2, 1};
    ANeuralNetworksOperandType opFilter = {
        .type              = static_cast<int32_t>(ANEURALNETWORKS_TENSOR_FLOAT32),
        .dimensionCount    = static_cast<uint32_t>(filterDim.size()),
        .dimensions        = filterDim.data(),
        .scale             = 0.0f,
        .zeroPoint         = 0
    };

    std::vector<uint32_t> biasDim{1};
    ANeuralNetworksOperandType opBias = {
        .type              = static_cast<int32_t>(ANEURALNETWORKS_TENSOR_FLOAT32),
        .dimensionCount    = static_cast<uint32_t>(biasDim.size()),
        .dimensions        = biasDim.data(),
        .scale             = 0.0f,
        .zeroPoint         = 0
    };

    std::vector<uint32_t> pad0Data;
    ANeuralNetworksOperandType pad0 = {
        .type              = static_cast<int32_t>(ANEURALNETWORKS_INT32),
        .dimensionCount    = static_cast<uint32_t>(pad0Data.size()),
        .dimensions        = pad0Data.data(),
        .scale             = 0.0f,
        .zeroPoint         = 0
    };

    std::vector<uint32_t> addDim{1, ROWS_OUT, COLS_OUT, 1};
    ANeuralNetworksOperandType addOp = {
        .type              = static_cast<int32_t>(ANEURALNETWORKS_TENSOR_FLOAT32),
        .dimensionCount    = static_cast<uint32_t>(addDim.size()),
        .dimensions        = addDim.data(),
        .scale             = 0.0f,
        .zeroPoint         = 0
    };

    std::vector<uint32_t> addScalarDim;
    ANeuralNetworksOperandType addScalar = {
        .type              = static_cast<int32_t>(ANEURALNETWORKS_INT32),
        .dimensionCount    = static_cast<uint32_t>(addScalarDim.size()),
        .dimensions        = addScalarDim.data(),
        .scale             = 0.0f,
        .zeroPoint         = 0
    };

    //
    // max_pooling_2d operands
    // index : 0 -> input matrix
    // index : 1 -> filter matrix
    // index : 2 -> bias matrix
    // index : 3 -> padding_left
    // index : 4 -> padding_right
    // index : 5 -> padding_top
    // index : 6 -> padding_bottom
    // index : 7 -> stride_width
    // index : 8 -> stride_height
    // index : 9 -> activation
    // index : 10 -> conv2D output (add input)
    // index : 11 -> add input
    // index : 12 -> add activation
    // index : 13 -> output
    //
#define ADD_OPERAND(m, t) \
    if (ANeuralNetworksModel_addOperand(m, &t) != ANEURALNETWORKS_NO_ERROR) { \
        printf("failed to ANeuralNetworksModel_addOperand\n"); \
        ANeuralNetworksModel_free(m); \
        return 0; \
    }

    ADD_OPERAND(model, op)
    ADD_OPERAND(model, opFilter)
    ADD_OPERAND(model, opBias)
    ADD_OPERAND(model, pad0)
    ADD_OPERAND(model, pad0)
    ADD_OPERAND(model, pad0)
    ADD_OPERAND(model, pad0)
    ADD_OPERAND(model, pad0)
    ADD_OPERAND(model, pad0)
    ADD_OPERAND(model, pad0)
    ADD_OPERAND(model, opOut)
    ADD_OPERAND(model, addOp)
    ADD_OPERAND(model, addScalar)
    ADD_OPERAND(model, addOp)

    //
    // set operand value
    //
#define SET_INT32_OPERAND_VALUE(v, i, m) \
    { \
        int32_t val(v); \
        if (ANeuralNetworksModel_setOperandValue(m, i, &val, sizeof(val)) != ANEURALNETWORKS_NO_ERROR) { \
            printf("failed to ANeuralNetworksModel_setOperandValue\n"); \
            ANeuralNetworksModel_free(m); \
            return 0; \
        } \
    }

    SET_INT32_OPERAND_VALUE(0, 3, model)
    SET_INT32_OPERAND_VALUE(0, 4, model)
    SET_INT32_OPERAND_VALUE(0, 5, model)
    SET_INT32_OPERAND_VALUE(0, 6, model)
    SET_INT32_OPERAND_VALUE(2, 7, model)
    SET_INT32_OPERAND_VALUE(2, 8, model)
    SET_INT32_OPERAND_VALUE(0, 9, model)
    SET_INT32_OPERAND_VALUE(0, 12, model)

    //
    // add operation
    //
    std::vector<uint32_t> in{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::vector<uint32_t> out{10};
    if (ANeuralNetworksModel_addOperation(
            model, ANEURALNETWORKS_CONV_2D, static_cast<uint32_t>(in.size()), in.data(),
            static_cast<uint32_t>(out.size()), out.data()) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_addOperation\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    std::vector<uint32_t> in2{10, 11, 12};
    std::vector<uint32_t> out2{13};
    if (ANeuralNetworksModel_addOperation(
            model, ANEURALNETWORKS_ADD, static_cast<uint32_t>(in2.size()), in2.data(),
            static_cast<uint32_t>(out2.size()), out2.data()) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_addOperation\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    //
    // set inputs and outputs
    //
    std::vector<uint32_t> in3{0, 1, 2, 11};
    std::vector<uint32_t> out3{13};
    if (ANeuralNetworksModel_identifyInputsAndOutputs(
            model, static_cast<uint32_t>(in3.size()), in3.data(),
            static_cast<uint32_t>(out3.size()), out3.data()) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_identifyInputsAndOutputs\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }
    return 1;
}

int createRequestForTest(ANeuralNetworksExecution *execution, MatrixOutput *outputMat) {
    //
    // set buffer
    //
    if (ANeuralNetworksExecution_setInput(
            execution, 0, nullptr, inputMat, sizeof(MatrixInput)) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_setInput - input\n");
        return 0;
    }

    if (ANeuralNetworksExecution_setInput(
            execution, 1, nullptr, filterMat, sizeof(MatrixFilter)) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_setInput - filter\n");
        return 0;
    }

    if (ANeuralNetworksExecution_setInput(
            execution, 2, nullptr, biasMat, sizeof(MatrixBias)) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_setInput - bias\n");
        return 0;
    }

    if (ANeuralNetworksExecution_setInput(
            execution, 3, nullptr, matrix, sizeof(MatrixOutput)) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_setInput - bias\n");
        return 0;
    }

    if (ANeuralNetworksExecution_setOutput(
            execution, 0, nullptr, outputMat, sizeof(MatrixOutput)) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_setOutput\n");
        return 0;
    }
    return 1;
}

static int startCompute(ANeuralNetworksExecution* execution) {
    //
    // start to compute
    //
    ANeuralNetworksEvent* event = nullptr;
    if (ANeuralNetworksExecution_startCompute(execution, &event) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_startCompute\n");
        return 0;
    }

    ANeuralNetworksEvent_wait(event);
    ANeuralNetworksEvent_free(event);

    return 1;
}

/*************************************************************************************************/
int utBasicCompute() {
    // Create Model Start
    ANeuralNetworksModel *model = nullptr;
    if (ANeuralNetworksModel_create(&model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_create\n");
        return 0;
    }

    if (createModelForTest(model) == 0) {
        return 0;
    }

    if (ANeuralNetworksModel_finish(model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_finish\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }
    // Create Model End

    // Create Compilation Start
    ANeuralNetworksCompilation* compilation = nullptr;
    if (ANeuralNetworksCompilation_create(model, &compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_create\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuralNetworksCompilation_finish(compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_finish\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }
    // Create Compilation End

    // Create Execution and Request Start
    ANeuralNetworksExecution* execution = nullptr;
    if (ANeuralNetworksExecution_create(compilation, &execution) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_create\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    MatrixOutput outputMat;
    memset(&outputMat, 0, sizeof(outputMat));
    if (createRequestForTest(execution, &outputMat) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }
    // Create Execution and Request End

    if (startCompute(execution) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }

    // Compare Results
    if (CompareMatrices(expectedMat, outputMat) != 0) {
        printf("failed to compare matrices\n");
        ANeuralNetworksExecution_free(execution);
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    ANeuralNetworksExecution_free(execution);
    ANeuralNetworksModel_free(model);
    ANeuralNetworksCompilation_free(compilation);

    return 1;
}
