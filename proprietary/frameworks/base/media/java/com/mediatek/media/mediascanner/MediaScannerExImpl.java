package com.mediatek.media.mediascanner;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.media.MediaScanner;
import android.provider.MediaStore.Images;
import android.os.Build;
import android.os.Handler;
import android.os.RemoteException;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;


public class MediaScannerExImpl extends MediaScanner {
    private final static String TAG = "MediaScannerExImpl";
    /**M: Add for control debug log.
     * @{**/
    private static final boolean LOGD = "eng".equals(Build.TYPE);
    private static final boolean DEBUG = Log.isLoggable(TAG, Log.DEBUG) || LOGD;
    /**@}**/
    private MediaInserterExImpl mMediaInserterExImpl;
    public MediaScannerExImpl(Context c, String volumeName) {
        super(c, volumeName);
    }

       /// M: MediaScanner Performance turning {@
       /// Add some new api for MediaScanner performance enhancement feature,
       /// we use threadpool to scan every folder in directories.

       /**
        * M: Pre-scan all, only call by this scanner created in thread pool.
        * @param volume The volume to scan.
        * @hide
        */
       public void preScanAll(String volume) {
           try {
               prescan(null, true);
           } catch (RemoteException e) {
               Log.e(TAG, "RemoteException in MediaScanner.scan()", e);
           }
       }

       /**
        * M: Post scan all, only call by this scanner created in thread pool.
        *
        * @param playlistFilePathList playlist file path, process them to database.
        * @hide
        */
       public void postScanAll(ArrayList<String> playlistFilePathList) {
           try {
               /// handle playlists last, after we know what media files are on the storage.
               /// Restore path list to file entry list, then process these playlist
               if (mProcessPlaylists) {
                   for (String path : playlistFilePathList) {
                       FileEntry entry = makeEntryFor(path);
                       File file = new File(path);
                       long lastModified = file.lastModified();
                       // add some slack to avoid a rounding error
                       long delta = (entry != null) ? (lastModified - entry.mLastModified) : 0;
                       boolean wasModified = delta > 1 || delta < -1;
                       if (entry == null || wasModified) {
                           if (wasModified) {
                               entry.mLastModified = lastModified;
                           } else {
                               entry = new FileEntry(0, path, lastModified, 0);
                           }
                           entry.mLastModifiedChanged = true;
                       }
                       mPlayLists.add(entry);
                   }
                   processPlayLists();
               }
           } catch (RemoteException e) {
               Log.e(TAG, "RemoteException in MediaScanner.postScanAll()", e);
           }

           int originalImageCount = 0;
           int originalVideoCount = 0;
           Cursor c = null;
           try {
               c = mMediaProvider
                   .query(mImagesUri.buildUpon().appendQueryParameter("force", "1").build(),
                         ID_PROJECTION, null, null, null, null);
               if (c != null) {
                   originalImageCount = c.getCount();
                   c.close();
                   c = null;
               }
               c = mMediaProvider
                   .query(mVideoUri.buildUpon().appendQueryParameter("force", "1").build(),
                          ID_PROJECTION, null, null, null, null);
               if (c != null) {
                   originalVideoCount = c.getCount();
                   c.close();
                   c = null;
               }
           } catch (RemoteException e) {
               Log.e(TAG, "RemoteException in MediaScanner.postScanAll()", e);
           } finally {
               if (null != c) {
                   c.close();
               }
           }

           if (DEBUG) {
               Log.v(TAG, "postScanAll");
           }
       }

    /**
        * M: Scan all given folder with right method.
        * Single file and empty folder need scan special one by one.
        *
        * @param insertHanlder use to do entries insert
        * @param folders The folders given to scan.
        * @param volume External or internal
        * @param isSingelFile whether the given folders is single file
        *
        * @return playlist file path scan in these folders
        *
        * @hide
        */
       public ArrayList<String> scanFolders(Handler insertHanlder,
       String[] folders, String volume, boolean isSingelFile) {
           try {
               /// Init mPlaylist because we may insert playlist in begin file.
               mPlayLists.clear();

               if (ENABLE_BULK_INSERTS) {
                   /// create MediaInserter for bulk inserts
                   mMediaInserterExImpl = new MediaInserterExImpl(insertHanlder, 100);
               }
               /// Single file scan it directly and folder need scan all it's sub files.
               for (String path : folders) {
                   if (isSingelFile) {
                       File file = new File(path);
                       long lastModifiedSeconds = file.lastModified()
                       / 1000; // lastModified is in milliseconds on Files.
                       mClient.doScanFile(path, null, lastModifiedSeconds, file.length(),
                               file.isDirectory(), false, isNoMediaPath(path));
                   } else {
                       processDirectory(path, mClient);
                   }
               }
               if (ENABLE_BULK_INSERTS) {
                   /// flush remaining inserts
                   mMediaInserterExImpl.flushAll();
                   mMediaInserterExImpl = null;
               }
           } catch (SQLException e) {
               /// this might happen if the SD card is removed while the media scanner is running
               Log.e(TAG, "SQLException in MediaScanner.scan()", e);
           } catch (UnsupportedOperationException e) {
               /// this might happen if the SD card is removed while the media scanner is running
               Log.e(TAG, "UnsupportedOperationException in MediaScanner.scan()", e);
           } catch (RemoteException e) {
               Log.e(TAG, "RemoteException in MediaScanner.scan()", e);
           }

           return mPlaylistFilePathList;
       }

       /**
        * M: Scan all given folder with right method.
        * Single file and empty folder need scan special one by one.
        *
        * @param folders The folders given to scan.
        * @param volume External or internal
        * @param isSingelFileOrEmptyFolder whether the given folders is single file or empty folder
        *
        * @return playlist file path scan in these folders
        * @hide
        */
       public ArrayList<String> scanFolders(String[] folders,
       String volume, boolean isSingelFileOrEmptyFolder) {
           try {
               /// Init mPlaylist because we may insert playlist in begin file.
               mPlayLists.clear();

               if (ENABLE_BULK_INSERTS) {
                   /// create MediaInserter for bulk inserts
                   mMediaInserterExImpl = new MediaInserterExImpl(mMediaProvider, 500);
               }
               /// M: Call doScanFile to scan folder and use processDirecitory to scan subfolders.
               for (String folder : folders) {
                     File file = new File(folder);
                     if (file.exists()) {
                         // lastModified is in milliseconds on Files.
                         long lastModifiedSeconds = file.lastModified() / 1000;
                         mClient.doScanFile(folder, null, lastModifiedSeconds, file.length(),
                                 file.isDirectory(), false, isNoMediaPath(folder));
                     }

                     if (!isSingelFileOrEmptyFolder) {
                       processDirectory(folder, mClient);
                     }
               }

               if (ENABLE_BULK_INSERTS) {
                   /// flush remaining inserts
                   mMediaInserterExImpl.flushAll();
                   mMediaInserterExImpl = null;
               }
           } catch (SQLException e) {
               /// this might happen if the SD card is removed while the media scanner is running
               Log.e(TAG, "SQLException in MediaScanner.scan()", e);
           } catch (UnsupportedOperationException e) {
               /// this might happen if the SD card is removed while the media scanner is running
               Log.e(TAG, "UnsupportedOperationException in MediaScanner.scan()", e);
           } catch (RemoteException e) {
               Log.e(TAG, "RemoteException in MediaScanner.scan()", e);
           }

           return mPlaylistFilePathList;
       }
       /// @}

}

