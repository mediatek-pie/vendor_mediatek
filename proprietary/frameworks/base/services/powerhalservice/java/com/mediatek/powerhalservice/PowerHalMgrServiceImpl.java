/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
package com.mediatek.powerhalservice;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemProperties;
import android.os.UEventObserver;
import android.os.UserHandle;
import android.util.Log;
import com.android.server.SystemService;
import com.mediatek.powerhalmgr.IPowerHalMgr;
import com.mediatek.powerhalwrapper.PowerHalWrapper;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import vendor.mediatek.hardware.power.V2_0.*;

public class PowerHalMgrServiceImpl extends IPowerHalMgr.Stub {
    private final String TAG = "PowerHalMgrServiceImpl";
    private static PowerHalWrapper mPowerHalWrap = null;

/*
    static {
        System.loadLibrary("powerhalmgrserv_jni");
    }
*/
    public PowerHalMgrServiceImpl(){
           mPowerHalWrap = PowerHalWrapper.getInstance();
    }

    public int scnReg() {
        return mPowerHalWrap.scnReg();
    }

    public void scnConfig(int handle, int cmd, int param_1,
                                          int param_2, int param_3, int param_4) {
        mPowerHalWrap.scnConfig(handle, cmd, param_1, param_2, param_3, param_4);
    }

    public void scnUnreg(int handle) {
        mPowerHalWrap.scnUnreg(handle);
    }

    public void scnEnable(int handle, int timeout) {
        mPowerHalWrap.scnEnable(handle, timeout);
    }

    public void scnDisable(int handle) {
        mPowerHalWrap.scnDisable(handle);
    }

    public void scnUltraCfg(int handle, int ultracmd, int param_1,
                                                 int param_2, int param_3, int param_4) {
        mPowerHalWrap.scnUltraCfg(handle, ultracmd, param_1, param_2, param_3, param_4);
    }

    public void mtkCusPowerHint(int hint, int data) {
        mPowerHalWrap.mtkCusPowerHint(hint, data);
    }

    private void log(String info) {
        Log.d("@M_" + TAG, "[PowerHalMgrServiceImpl] " + info + " ");
    }

    private void loge(String info) {
        Log.e("@M_" + TAG, "[PowerHalMgrServiceImpl] ERR: " + info + " ");
    }
}
