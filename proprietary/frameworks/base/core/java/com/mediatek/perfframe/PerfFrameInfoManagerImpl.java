/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
package com.mediatek.perfframe;

import android.util.Log;
import android.util.Printer;
import android.os.Build;
import android.os.Looper;
import android.os.Process;
import android.os.RemoteException;


import java.util.HashMap;
import java.util.Iterator;

/**
 * Use to dump message history and message queue.
 *
 * @hide
 */
public final class PerfFrameInfoManagerImpl extends PerfFrameInfoManager {

    private static final String TAG = "PerfFrameInfoManagerImpl";
    private static PerfFrameInfoManagerImpl sInstance = null;
    private static Object lock = new Object();

    private boolean draw_start;
    private boolean software_draw;

    public static native int nativeMarkFrameComplete(int tid, int frame_time, int frame_type, long frame_id);
    public static native int nativeMarkIntendedVsync(int tid, long frame_id);
    public static native int nativeMarkNoRender(long frame_id);
    public static native int nativeMarkDrawStart(long frame_id);

    private static final int SWUI = 0;
    private static final int HWUI = 1;
    private static final int GLSURFACE = 2;


    static {
        System.loadLibrary("perfframeinfo_jni");
    }

    public static PerfFrameInfoManagerImpl getInstance() {
        if (null == sInstance) {
            synchronized (lock) {
                if (null == sInstance) {
                    sInstance = new PerfFrameInfoManagerImpl();
                }
            }
        }
        return sInstance;
    }

    private PerfFrameInfoManagerImpl() {

    }

    @Override
    public void setDrawStart(long frame_id) {
         this.draw_start = true;
         nativeMarkDrawStart(frame_id);
         /*Log.i(TAG, __func__);*/
    }

    @Override
    public void setSoftwareDraw() {
         this.software_draw = true;
         /*Log.i(TAG, __func__);*/
    }

    @Override
    public void clearDrawStartAndMarkIntendedVsync(long frame_id) {
         int pid = android.os.Process.myPid();

         this.draw_start = false;
         this.software_draw = false;
         nativeMarkIntendedVsync(pid, frame_id);
    }

    @Override
    public void markDoFrameEnd(int frame_time, long frame_id) {
         int pid = android.os.Process.myPid();

         /*Log.i(TAG, __func__);*/
         if (this.software_draw)
             nativeMarkFrameComplete(pid, frame_time, SWUI, frame_id);
         else if (!this.draw_start)
             nativeMarkNoRender(frame_id);
    }

    @Override
    public void markGLDrawStart() {
         int tid = android.os.Process.myTid();

         /*Log.i(TAG, __func__);*/
         nativeMarkIntendedVsync(tid, 0);
    }

    @Override
    public void markGLDrawEnd(int frame_time) {
         int tid = android.os.Process.myTid();

         /*Log.i(TAG, __func__);*/
         nativeMarkFrameComplete(tid, frame_time, GLSURFACE, 0);
    }
}
