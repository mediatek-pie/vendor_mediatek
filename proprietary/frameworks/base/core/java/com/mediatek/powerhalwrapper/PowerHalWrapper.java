/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.powerhalwrapper;

import android.util.Log;
import android.util.Printer;
import android.os.Build;
import android.os.Looper;
import android.os.Process;
import android.os.RemoteException;


import java.util.HashMap;
import java.util.Iterator;
import android.util.Log;
import android.os.Trace;
import android.os.HwBinder;
import android.os.IHwBinder;
import android.os.IHwInterface;

/// MTK power
import vendor.mediatek.hardware.power.V2_0.*;

public class PowerHalWrapper {

    private static final String TAG = "PowerHalWrapper";

    private static final int AMS_BOOST_TIME = 30000;
    private static boolean AMS_BOOST_PROCESS_CREATE = true;
    private static boolean AMS_BOOST_PACK_SWITCH = true;
    private static boolean AMS_BOOST_ACT_SWITCH = true;
    private static boolean EXT_PEAK_PERF_MODE = false;
    private static int pboost_pc_timeout = 0;
    private static int pboost_act_timeout = 0;
    private static int pextpeak_period = 0;
    private static int exLchProcessCreate = 0;
    private static int exLchPackSwitch = 0;
    private static int exLchActSwitch = 0;
    private static PowerHalWrapper sInstance = null;
    private static Object lock = new Object();

    public static native int nativeMtkPowerHint(int hint, int data);
    public static native int nativeMtkCusPowerHint(int hint, int data);
    public static native int nativeQuerySysInfo(int cmd, int param);
    public static native int nativeNotifyAppState(String packname, String actname,
                                                          int pid, int status);
    public static native int nativeScnReg();
    public static native int nativeScnConfig(int hdl, int cmd,
                                             int param_1, int param_2, int param_3, int param_4);
    public static native int nativeScnUnreg(int hdl);
    public static native int nativeScnEnable(int hdl, int timeout);
    public static native int nativeScnDisable(int hdl);
    public static native int nativeScnUltraCfg(int hdl, int ultracmd,
                                             int param_1, int param_2, int param_3, int param_4);

    static {
        System.loadLibrary("perfframeinfo_jni");
    }

    public static PowerHalWrapper getInstance() {
        if (null == sInstance) {
            synchronized (lock) {
                if (null == sInstance) {
                    sInstance = new PowerHalWrapper();
                }
            }
            pextpeak_period = nativeQuerySysInfo(MtkQueryCmd.CMD_GET_EXT_PEAK_PERIOD, 0);
            Log.e(TAG, "pextpeak_period: "+ pextpeak_period);
            mtkExLchSort();
        }
        return sInstance;
    }

    private PowerHalWrapper() {

    }

    private static void mtkExLchSort() {
        int i, exLchScn = 0;

        exLchScn = nativeQuerySysInfo(MtkQueryCmd.CMD_GET_EXT_LAUNCH_FOLLOW, 0);
        if (exLchScn == MtkPowerHint.MTK_POWER_HINT_PROCESS_CREATE) {
            exLchProcessCreate = nativeQuerySysInfo(
                                MtkQueryCmd.CMD_GET_EXT_LAUNCH_DURATION, 0);
            Log.e(TAG, "<mtkExLchSort> exLchScn:" + exLchScn
                                          + " ,period:" + exLchProcessCreate);
        }

        exLchScn = nativeQuerySysInfo(MtkQueryCmd.CMD_GET_EXT_LAUNCH_FOLLOW_1, 0);
        if (exLchScn == MtkPowerHint.MTK_POWER_HINT_PACK_SWITCH) {
            exLchPackSwitch = nativeQuerySysInfo(
                             MtkQueryCmd.CMD_GET_EXT_LAUNCH_DURATION_1, 0);
            Log.e(TAG, "<mtkExLchSort> exLchScn:" + exLchScn
                                          + " ,period:" + exLchPackSwitch);
        }

        exLchScn = nativeQuerySysInfo(MtkQueryCmd.CMD_GET_EXT_LAUNCH_FOLLOW_2, 0);
        if (exLchScn == MtkPowerHint.MTK_POWER_HINT_ACT_SWITCH) {
            exLchActSwitch = nativeQuerySysInfo(
                            MtkQueryCmd.CMD_GET_EXT_LAUNCH_DURATION_2, 0);
            Log.e(TAG, "<mtkExLchSort> exLchScn:" + exLchScn
                                          + " ,period:" + exLchActSwitch);
        }

    }

    private void mtkPowerHint(int hint, int data) {
                nativeMtkPowerHint(hint, data);
    }

    public void mtkCusPowerHint(int hint, int data) {
                nativeMtkCusPowerHint(hint, data);
    }

    public int scnReg() {
               return nativeScnReg();
    }

    public int scnConfig(int hdl, int cmd, int param_1,
                                      int param_2, int param_3, int param_4) {
               nativeScnConfig(hdl, cmd, param_1, param_2, param_3, param_4);
               return 0;
    }

    public int scnUnreg(int hdl) {
               nativeScnUnreg(hdl);
               return 0;
    }

    public int scnEnable(int hdl, int timeout) {
               nativeScnEnable(hdl, timeout);
               return 0;
    }

    public int scnDisable(int hdl) {
               nativeScnDisable(hdl);
               return 0;
    }

    public int scnUltraCfg(int hdl, int ultracmd, int param_1,
                                             int param_2, int param_3, int param_4) {
               nativeScnUltraCfg(hdl, ultracmd, param_1, param_2, param_3, param_4);
               return 0;
    }

    public void galleryBoostEnable(int timeoutMs) {
            Log.e(TAG, "<galleryBoostEnable> do boost with " + timeoutMs + "ms");
            nativeMtkPowerHint(MtkPowerHint.MTK_POWER_HINT_GALLERY_BOOST, timeoutMs);
    }

    public void setRotationBoost(int boostTime) {
            Log.e(TAG, "<setRotation> do boost with " + boostTime + "ms");
            nativeMtkPowerHint(MtkPowerHint.MTK_POWER_HINT_APP_ROTATE, boostTime);
    }

    public void setSpeedDownload(int timeoutMs) {
            Log.e(TAG, "<setSpeedDownload> do boost with " + timeoutMs + "ms");
            nativeMtkPowerHint(MtkPowerHint.MTK_POWER_HINT_WIPHY_SPEED_DL, timeoutMs);
    }

    public void setWFD(boolean enable) {
            Log.e(TAG, "<setWFD> enable:" + enable);
            if (enable)
                nativeMtkPowerHint(MtkPowerHint.MTK_POWER_HINT_WFD,
                                                MtkHintOp.MTK_HINT_ALWAYS_ENABLE);
            else
                nativeMtkPowerHint(MtkPowerHint.MTK_POWER_HINT_WFD, 0);
    }

    public void setInstallationBoost(boolean enable) {
            Log.e(TAG, "<setInstallationBoost> enable:" + enable);
            if (enable)
                nativeMtkPowerHint(MtkPowerHint.MTK_POWER_HINT_PMS_INSTALL, 15000);
            else
                nativeMtkPowerHint(MtkPowerHint.MTK_POWER_HINT_PMS_INSTALL, 0);
    }


    /* AMS event handler */
    public void amsBoostResume(String lastResumedPackageName, String nextResumedPackageName) {
        //Log.e(TAG, "<amsBoostResume> last:" + lastResumedPackageName +
                                       //", next:" + nextResumedPackageName);

        Trace.asyncTraceBegin(Trace.TRACE_TAG_ACTIVITY_MANAGER, "amPerfBoost", 0);
                /*--make sure not re-entry ext_launch--*/
                nativeMtkPowerHint(MtkPowerHint.MTK_POWER_HINT_EXT_LAUNCH, 0);

                if (lastResumedPackageName == null ||
                    !lastResumedPackageName.equalsIgnoreCase(nextResumedPackageName)) {
                    AMS_BOOST_PACK_SWITCH = true;

                    /*--for ext peak perf mode--*/
                    if (EXT_PEAK_PERF_MODE) {
                        EXT_PEAK_PERF_MODE = false;
                        nativeMtkPowerHint(MtkPowerHint.MTK_POWER_HINT_PROCESS_CREATE, 0);
                    }

                    /*--main package switch--*/
                    nativeMtkPowerHint(MtkPowerHint.MTK_POWER_HINT_PACK_SWITCH,
                        AMS_BOOST_TIME);
                }
                else {
                    AMS_BOOST_ACT_SWITCH = true;

                    /*--for ext peak perf mode--*/
                    if (EXT_PEAK_PERF_MODE) {
                        EXT_PEAK_PERF_MODE = false;
                        nativeMtkPowerHint(MtkPowerHint.MTK_POWER_HINT_PROCESS_CREATE, 0);
                    }

                    /*--main activity switch--*/
                    nativeMtkPowerHint(MtkPowerHint.MTK_POWER_HINT_ACT_SWITCH,
                        AMS_BOOST_TIME);
                }
    }

    public void amsBoostProcessCreate(String hostingType, String packageName) {
        if(hostingType.compareTo("activity") == 0) {
            Trace.asyncTraceBegin(Trace.TRACE_TAG_ACTIVITY_MANAGER, "amPerfBoost", 0);
            AMS_BOOST_PROCESS_CREATE = true;
                    /*--make sure not re-entry ext_launch--*/
                    nativeMtkPowerHint(MtkPowerHint.MTK_POWER_HINT_EXT_LAUNCH, 0);
                    /*--main process create--*/
                    nativeMtkPowerHint(MtkPowerHint.MTK_POWER_HINT_PROCESS_CREATE, AMS_BOOST_TIME);
        }
    }

    public void amsBoostStop() {
        int duration = pextpeak_period;
        //log("amsBoostStop");
        if (AMS_BOOST_PACK_SWITCH) {
            AMS_BOOST_PACK_SWITCH = false;
                   nativeMtkPowerHint(MtkPowerHint.MTK_POWER_HINT_PACK_SWITCH, 0);
                   /*--if ext hint follow package switch--*/
                   if (exLchPackSwitch > 0)
                       nativeMtkPowerHint(MtkPowerHint.MTK_POWER_HINT_EXT_LAUNCH, exLchPackSwitch);
        }

        if (AMS_BOOST_ACT_SWITCH) {
            AMS_BOOST_ACT_SWITCH = false;
                    nativeMtkPowerHint(MtkPowerHint.MTK_POWER_HINT_ACT_SWITCH, 0);
                   /*--if ext hint follow act switch--*/
                   if (exLchActSwitch > 0)
                       nativeMtkPowerHint(MtkPowerHint.MTK_POWER_HINT_EXT_LAUNCH, exLchActSwitch);
        }

        if (AMS_BOOST_PROCESS_CREATE) {
            AMS_BOOST_PROCESS_CREATE = false;
                        pboost_pc_timeout = nativeQuerySysInfo(16, 0);
                        if (duration > 0 || pboost_pc_timeout > 0) {
                            EXT_PEAK_PERF_MODE = true;

                            if (pboost_pc_timeout > 0) {
                                duration += pboost_pc_timeout;
                                Log.e(TAG, "<amsBoostStop> duration: " + duration + "ms"
                                                + ", pboost_pc_timeout: " + pboost_pc_timeout);
                                pboost_pc_timeout = 0;
                            }
                            Log.e(TAG, "<amsBoostStop> duration: " + duration + "ms");

                            /*--make sure not re-entry ext_launch--*/
                            nativeMtkPowerHint(MtkPowerHint.MTK_POWER_HINT_EXT_LAUNCH, 0);

                            nativeMtkPowerHint(MtkPowerHint.MTK_POWER_HINT_PROCESS_CREATE,
                                                                                          duration);
                        } else {
                            nativeMtkPowerHint(MtkPowerHint.MTK_POWER_HINT_PROCESS_CREATE, 0);
                            /*--if ext hint follow process create--*/
                            if (exLchProcessCreate > 0)
                                nativeMtkPowerHint(MtkPowerHint.MTK_POWER_HINT_EXT_LAUNCH,
                                                                         exLchProcessCreate);
                        }
        }

        Trace.asyncTraceEnd(Trace.TRACE_TAG_ACTIVITY_MANAGER, "amPerfBoost", 0);
    }

    public void amsBoostNotify(int pid, String activityName, String packageName) {
        //Log.e(TAG, "amsBoostNotify pid:" + pid +
        //      ",activity:" + activityName + ", package:" + packageName);
                nativeNotifyAppState(packageName, activityName,
                                            pid, MtkActState.STATE_RESUMED);
    }

    private static void log(String info) {
        Log.d("@M_" + TAG, "[PerfServiceWrapper] " + info + " ");
    }

    private static void loge(String info) {
        Log.e("@M_" + TAG, "[PerfServiceWrapper] ERR: " + info + " ");
    }
}

