package com.mediatek.common.wallpaper;

import android.content.Context;
import java.util.ArrayList;
import java.util.List;
import com.mediatek.common.util.OperatorCustomizationFactoryLoader;
import com.mediatek.common.util.OperatorCustomizationFactoryLoader.OperatorFactoryInfo;

public class OpWallpaperCustomizationFactoryBase {

    public IWallpaperPlugin makeWallpaperPlugin(Context context) {
        return new DefaultWallpaperPlugin(context);
    }

    private static final List<OperatorFactoryInfo> sOpFactoryInfoList
                                                = new ArrayList<OperatorFactoryInfo>();
    static {
        sOpFactoryInfoList.add(
                new OperatorFactoryInfo("OP03Wallpaper.apk",
                         "com.mediatek.op03.wallpaper.Op03WallpaperCustomizationFactory",
                         "com.mediatek.op03.wallpaper",
                         "OP03",
                         "SEGDEFAULT"
                        ));

    }

    static OpWallpaperCustomizationFactoryBase sFactory = null;
    public static synchronized OpWallpaperCustomizationFactoryBase getOpFactory(Context context) {
        if (sFactory == null) {
            sFactory = (OpWallpaperCustomizationFactoryBase) OperatorCustomizationFactoryLoader
                           .loadFactory(context, sOpFactoryInfoList);
            if (sFactory == null) {
                sFactory = new OpWallpaperCustomizationFactoryBase();
            }
        }
        return sFactory;
    }
}