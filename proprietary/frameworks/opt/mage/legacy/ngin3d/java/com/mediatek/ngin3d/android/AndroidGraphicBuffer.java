/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.ngin3d.android;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.GraphicBuffer;

import java.util.LinkedList;

/**
 * The wrapper that access Android GraphicBuffer
 */
public final class AndroidGraphicBuffer {

    private AndroidGraphicBuffer() {
        // Do nothing
    }

    public static final LinkedList<GraphicBuffer> mRemovedGBList =
            new LinkedList<GraphicBuffer>();



    /*
    format example: ImageFormat.YV12
     */
    static public Object create(int width, int height, int format) {
        return GraphicBuffer.create(width, height, format, GraphicBuffer.USAGE_HW_TEXTURE | GraphicBuffer.USAGE_SW_WRITE_RARELY);
    }

    static public int getWidth(Object o) {
        return ((GraphicBuffer ) o).getWidth();
    }

    static public int getHeight(Object o) {
        return ((GraphicBuffer ) o).getHeight();
    }

    static public int getFormat(Object o) {
        return ((GraphicBuffer ) o).getFormat();
    }

    static public int getUsage(Object o) {
        return ((GraphicBuffer ) o).getUsage();
    }

    static public Canvas lockCanvas(Object o) {
        return ((GraphicBuffer ) o).lockCanvas();
    }

    static public Canvas lockCanvas(Object o, Rect dirty) {
        return ((GraphicBuffer ) o).lockCanvas(dirty);
    }

    static public void unlockCanvasAndPost(Object o, Canvas canvas) {
        ((GraphicBuffer ) o).unlockCanvasAndPost(canvas);
    }

    static public void destroy(Object o) {
        ((GraphicBuffer ) o).destroy();
        mRemovedGBList.add((GraphicBuffer )o);
    }

    static public boolean isDestroyed(Object o) {
        return ((GraphicBuffer ) o).isDestroyed();
    }

    static public GraphicBuffer getRemovingGraphicBuffer() {
        if (mRemovedGBList.isEmpty())
            return null;
        else
            return mRemovedGBList.pop();
    }



}
