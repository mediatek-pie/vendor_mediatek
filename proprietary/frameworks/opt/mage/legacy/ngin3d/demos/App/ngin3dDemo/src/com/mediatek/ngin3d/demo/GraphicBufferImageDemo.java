package com.mediatek.ngin3d.demo;

import android.app.Activity;
import android.graphics.ImageFormat;
import android.os.Bundle;
import com.mediatek.ngin3d.Dimension;
import com.mediatek.ngin3d.Image;
import com.mediatek.ngin3d.Point;
import com.mediatek.ngin3d.Rotation;
import com.mediatek.ngin3d.Stage;
import com.mediatek.ngin3d.android.AndroidGraphicBuffer;
import com.mediatek.ngin3d.android.StageView;
import com.mediatek.ngin3d.animation.PropertyAnimation;

public class GraphicBufferImageDemo extends Activity {

    private Stage mStage = new Stage();
    private StageView mStageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mStageView = new StageView(this, mStage);
        setContentView(mStageView);
        Object gb = AndroidGraphicBuffer.create(1920, 1080, ImageFormat.YV12);
        Image yuv = Image.createGraphicBufferImage(gb);
        yuv.setDoubleSided(true);
        yuv.setPosition(new Point(0.5f, 0.5f, true));
        yuv.setSize(new Dimension(750, 750));
        new PropertyAnimation(yuv, "rotation", new Rotation(0, 0, 90), new Rotation(0, 0, 450)).setDuration(7000).setLoop(true).start();
        mStage.add(yuv);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mStageView.onResume();
    }

    @Override
    protected void onPause() {
        mStageView.onPause();
        super.onPause();
    }
}