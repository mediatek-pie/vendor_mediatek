/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * A3M Appearance wrapper utilities.
 */
#pragma once
#ifndef JNI_APPEARANCE_WRAP_UTILITY_H
#define JNI_APPEARANCE_WRAP_UTILITY_H

#include <a3m/appearance.h>
#include <a3m/noncopyable.h>

template<typename T>
class Edit : private a3m::NonCopyable
{
private:
  a3m::Appearance::Ptr m_appearance;
  T m_state;

public:
  Edit(a3m::Appearance::Ptr const& appearance) :
    m_appearance(appearance),
    m_state(get())
  {
  }

  ~Edit()
  {
    set(m_state);
  }

  T* operator->()
  {
    return &m_state;
  }

  void set(T const& state);
  T const& get() const;
};

template<>
void Edit<a3m::PolygonMode>::set(a3m::PolygonMode const& state)
{
  m_appearance->setPolygonMode(state);
}

template<>
a3m::PolygonMode const& Edit<a3m::PolygonMode>::get() const
{
  return m_appearance->getPolygonMode();
}

template<>
void Edit<a3m::CompositingMode>::set(a3m::CompositingMode const& state)
{
  m_appearance->setCompositingMode(state);
}

template<>
a3m::CompositingMode const& Edit<a3m::CompositingMode>::get() const
{
  return m_appearance->getCompositingMode();
}

template<>
void Edit<a3m::Blender>::set(a3m::Blender const& state)
{
  Edit<a3m::CompositingMode>(m_appearance)->setBlender(state);
}

template<>
a3m::Blender const& Edit<a3m::Blender>::get() const
{
  return m_appearance->getCompositingMode().getBlender();
}

template<>
void Edit<a3m::Stencil>::set(a3m::Stencil const& state)
{
  Edit<a3m::CompositingMode>(m_appearance)->setStencil(state);
}

template<>
a3m::Stencil const& Edit<a3m::Stencil>::get() const
{
  return m_appearance->getCompositingMode().getStencil();
}

typedef Edit<a3m::PolygonMode> EditPolygonMode;
typedef Edit<a3m::CompositingMode> EditCompositingMode;
typedef Edit<a3m::Blender> EditBlender;
typedef Edit<a3m::Stencil> EditStencil;

static a3m::Blender const& getBlender(a3m::Appearance::Ptr const& appearance)
{
  return appearance->getCompositingMode().getBlender();
}

static a3m::Stencil const& getStencil(a3m::Appearance::Ptr const& appearance)
{
  return appearance->getCompositingMode().getStencil();
}

struct ScissorRectangle
{
  A3M_INT32 left;
  A3M_INT32 bottom;
  A3M_UINT32 width;
  A3M_UINT32 height;

  ScissorRectangle(a3m::Appearance::Ptr const& appearance)
  {
    appearance->getCompositingMode().getScissorRectangle(
        left, bottom, width, height);
  }
};

#endif // JNI_APPEARANCE_WRAP_UTILITY_H
