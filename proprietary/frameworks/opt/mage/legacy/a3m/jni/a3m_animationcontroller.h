/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * A3M AnimationController "Java" implementation.
 */
#pragma once
#ifndef JNI_A3M_ANIMATION_CONTROLLER_H
#define JNI_A3M_ANIMATION_CONTROLLER_H

#include <a3m/animation.h>
#include <a3m_animationcontroller.h>

class A3mAnimationController
{
private:
  a3m::AnimationController::Ptr m_native;

  A3mAnimationController(a3m::AnimationController::Ptr const& native) :
    m_native(native)
  {
  }

public:
  // The following functions are ignored by SWIG (see a3m.i)
  static A3mAnimationController* toWrapper(a3m::AnimationController::Ptr const& native)
  {
    return native ? new A3mAnimationController(native) : 0;
  }

  static a3m::AnimationController::Ptr toNative(A3mAnimationController* wrapper)
  {
    return wrapper ? wrapper->getNative() : a3m::AnimationController::Ptr();
  }

  a3m::AnimationController::Ptr const& getNative() const
  {
    return m_native;
  }

public:
  void update(float delta = 0.0f)
  {
    m_native->update(delta);
  }

  void play(bool update = true)
  {
    a3m::play(*m_native, update);
  }

  void pause(bool update = true)
  {
    a3m::pause(*m_native, update);
  }

  void stop(bool update = true)
  {
    a3m::stop(*m_native, update);
  }

  void seek(float progress)
  {
    a3m::seek(*m_native, progress);
  }

  void rewind(bool update = true)
  {
    a3m::rewind(*m_native, update);
  }

  bool isFinished()
  {
    return a3m::isFinished(*m_native);
  }

  bool isInsideLoop()
  {
    return a3m::isInsideLoop(*m_native);
  }

  void setProgress(float progress)
  {
    m_native->setProgress(progress);
  }

  float getProgress() const
  {
    return m_native->getProgress();
  }

  void setEnabled(bool enabled)
  {
    m_native->setEnabled(enabled);
  }

  bool getEnabled() const
  {
    return m_native->getEnabled();
  }

  void setPaused(bool paused)
  {
    m_native->setPaused(paused);
  }

  bool getPaused() const
  {
    return m_native->getPaused();
  }

  void setLooping(bool looping)
  {
    m_native->setLooping(looping);
  }

  bool getLooping() const
  {
    return m_native->getLooping();
  }

  void setSpeed(float speed)
  {
    m_native->setSpeed(speed);
  }

  float getSpeed() const
  {
    return m_native->getSpeed();
  }

  void setStart(float start)
  {
    m_native->setStart(start);
  }

  float getStart() const
  {
    return m_native->getStart();
  }

  void setEnd(float end)
  {
    m_native->setEnd(end);
  }

  float getEnd() const
  {
    return m_native->getEnd();
  }

  void setRange(float start, float end)
  {
    a3m::setRange(*m_native, start, end);
  }

  float getLength() const
  {
    return a3m::getLength(*m_native);
  }

  void setLoopStart(float start)
  {
    m_native->setLoopStart(start);
  }

  float getLoopStart() const
  {
    return m_native->getLoopStart();
  }

  void setLoopEnd(float end)
  {
    m_native->setLoopEnd(end);
  }

  float getLoopEnd() const
  {
    return m_native->getLoopEnd();
  }

  void setLoopRange(float start, float end)
  {
    a3m::setLoopRange(*m_native, start, end);
  }

  float getLoopLength() const
  {
    return a3m::getLoopLength(*m_native);
  }

  bool hasLoop() const
  {
    return a3m::hasLoop(*m_native);
  }

  A3mAnimation* getAnimation() const
  {
    return A3mAnimation::toWrapper(m_native->getAnimation());
  }
};

#endif // JNI_A3M_ANIMATION_CONTROLLER_H
