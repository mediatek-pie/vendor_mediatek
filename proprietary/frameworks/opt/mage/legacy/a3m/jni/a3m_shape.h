/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * A3M Shape "Java" implementation.
 */
#pragma once
#ifndef JNI_A3M_SHAPE_H
#define JNI_A3M_SHAPE_H

#include <a3m/shape.h>
#include <a3m_ray.h>

class A3mShape
{
private:
  a3m::Shape::Ptr m_native;
  a3m::RaycastResult m_raycastResult;

protected:
  A3mShape(a3m::Shape::Ptr const& native) :
    m_native(native)
  {
  }

public:
  // The following functions are ignored by SWIG (see a3m.i)
  static A3mShape* toWrapper(a3m::Shape::Ptr const& native)
  {
    return native ? new A3mShape(native) : 0;
  }

  static a3m::Shape::Ptr toNative(A3mShape* wrapper)
  {
    return wrapper ? wrapper->getNative() : a3m::Shape::Ptr();
  }

  a3m::Shape::Ptr const& getNative() const
  {
    return m_native;
  }

public:
  virtual ~A3mShape()
  {
  }

  void setTransform(A3mSceneNode const& node)
  {
    m_native->setTransform(node.getNative()->getWorldTransform());
  }

  bool raycast(A3mRay const& ray)
  {
    m_raycastResult = m_native->raycast(ray.getNative());
    return m_raycastResult.getIntersected();
  }

  float getRaycastDistance() const
  {
    return m_raycastResult.getDistance();
  }

  float getRaycastNormalX() const
  {
    return m_raycastResult.getNormal().x;
  }

  float getRaycastNormalY() const
  {
    return m_raycastResult.getNormal().y;
  }

  float getRaycastNormalZ() const
  {
    return m_raycastResult.getNormal().z;
  }
};

#endif // JNI_A3M_SHAPE_H
