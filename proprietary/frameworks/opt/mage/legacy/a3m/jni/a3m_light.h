/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * A3M Light "Java" implementation.
 */
#pragma once
#ifndef JNI_A3M_LIGHT_H
#define JNI_A3M_LIGHT_H

#include <a3m/light.h>
#include <a3m_scenenode.h>

class A3mLight : public A3mSceneNode
{
private:
  A3mLight(a3m::Light::Ptr const& native) :
    A3mSceneNode(native)
  {
  }

  // Avoid the minor inefficiency in getNativeLight() of creating a SharedPtr.
  a3m::Light* getNativeRaw() const
  {
    return static_cast<a3m::Light*>(getNative().get());
  }

public:
  // The following functions are ignored by SWIG (see a3m.i)
  static A3mLight* toWrapper(a3m::Light::Ptr const& native)
  {
    return native ? new A3mLight(native) : 0;
  }

  static a3m::Light::Ptr toNative(A3mLight* wrapper)
  {
    return wrapper ? wrapper->getNativeLight() : a3m::Light::Ptr();
  }

  a3m::Light::Ptr getNativeLight() const
  {
    return a3m::static_ptr_cast<a3m::Light>(getNative());
  }

public:
    void setLightType(int type)
    {
      getNativeRaw()->setLightType(static_cast<a3m::Light::LightType>(type));
    }

    int getLightType() const
    {
      return static_cast<int>(getNativeRaw()->getLightType());
    }

    void setColour(float r, float g, float b, float a)
    {
      getNativeRaw()->setColour(a3m::Colour4f(r, g, b, a));
    }

    float getColourR() const
    {
      return getNativeRaw()->getColour().r;
    }

    float getColourG() const
    {
      return getNativeRaw()->getColour().g;
    }

    float getColourB() const
    {
      return getNativeRaw()->getColour().b;
    }

    float getColourA() const
    {
      return getNativeRaw()->getColour().a;
    }

    void setAmbientLevel(float level)
    {
      getNativeRaw()->setAmbientLevel(level);
    }

    float getAmbientLevel() const
    {
      return getNativeRaw()->getAmbientLevel();
    }

    void setIntensity(float intensity)
    {
      getNativeRaw()->setIntensity(intensity);
    }

    float getIntensity() const
    {
      return getNativeRaw()->getIntensity();
    }

    void setIsAttenuated(bool isAttenuated)
    {
      getNativeRaw()->setIsAttenuated(isAttenuated);
    }

    bool getIsAttenuated()
    {
      return getNativeRaw()->getIsAttenuated();
    }

    void setAttenuationNear(float distance)
    {
      getNativeRaw()->setAttenuationNear(distance);
    }

    float getAttenuationNear()
    {
      return getNativeRaw()->getAttenuationNear();
    }

    void setAttenuationFar(float distance)
    {
      getNativeRaw()->setAttenuationFar(distance);
    }

    float getAttenuationFar() const
    {
      return getNativeRaw()->getAttenuationFar();
    }

    void setSpotInnerAngle(int units, float value)
    {
      getNativeRaw()->setSpotInnerAngle(
          a3m::Anglef(static_cast<a3m::Anglef::Units>(units), value));
    }

    float getSpotInnerAngle(int units) const
    {
      return getNativeRaw()->getSpotInnerAngle().get(
          static_cast<a3m::Anglef::Units>(units));
    }

    void setSpotOuterAngle(int units, float value)
    {
      getNativeRaw()->setSpotOuterAngle(
          a3m::Anglef(static_cast<a3m::Anglef::Units>(units), value));
    }

    float getSpotOuterAngle(int units) const
    {
      return getNativeRaw()->getSpotOuterAngle().get(
          static_cast<a3m::Anglef::Units>(units));
    }
};

#endif // JNI_A3M_LIGHT_H
