/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * A3M RenderBlock "Java" implementation.
 */
#pragma once
#ifndef JNI_A3M_RENDERBLOCK_H
#define JNI_A3M_RENDERBLOCK_H

#include <a3m/renderblock.h>     // a3m RenderBlock class being wrapped here
#include <a3m_renderblockbase.h> // wrapped RenderBlockBase class used as base
#include <a3m_camera.h>          // wrapped Camera class used in interface
#include <a3m_flagmask.h>        // wrapped FlagMask class used in interface
#include <a3m_rendertarget.h>    // wrapped RenderTarget class used in interface

class A3mRenderBlock : public A3mRenderBlockBase
{
private:
  A3mRenderBlock(a3m::RenderBlock::Ptr const& native) :
    A3mRenderBlockBase(native)
  {
  }

  // Avoid the minor inefficiency in getNativeRenderBlock() of creating a
  // SharedPtr.
  a3m::RenderBlock* getNativeRaw() const
  {
    return static_cast<a3m::RenderBlock*>(getNative().get());
  }

public:
  // The following functions are ignored by SWIG (see a3m.i)
  static A3mRenderBlock* toWrapper(a3m::RenderBlock::Ptr const& native)
  {
    return native ? new A3mRenderBlock(native) : 0;
  }

  static a3m::RenderBlock::Ptr toNative(A3mRenderBlock* wrapper)
  {
    return wrapper ? wrapper->getNativeRenderBlock() : a3m::RenderBlock::Ptr();
  }

  a3m::RenderBlock::Ptr getNativeRenderBlock() const
  {
    return a3m::static_ptr_cast<a3m::RenderBlock>(getNative());
  }

public:
    void setCamera(A3mCamera& camera)
    {
      getNativeRaw()->setCamera(camera.getNativeCamera());
    }

    void setViewport(float left, float bottom, float width, float height)
    {
      getNativeRaw()->setViewport(left, bottom, width, height);
    }

    void setBackgroundColour(float r, float g, float b, float a)
    {
      getNativeRaw()->setBackgroundColour(a3m::Colour4f(r, g, b, a));
    }

    void setRenderFlags(
        A3mFlagMask const& renderFlags,
        A3mFlagMask const& recursiveFlags)
    {
      getNativeRaw()->setRenderFlags(
          renderFlags.getNative(),
          recursiveFlags.getNative());
    }

    void setColourClear(bool clear)
    {
      getNativeRaw()->setColourClear(clear);
    }

    void setDepthClear(bool clear)
    {
      getNativeRaw()->setDepthClear(clear);
    }

    void setRenderTarget(A3mRenderTarget* target)
    {
      a3m::SharedPtr< a3m::RenderTarget > const& nativeRenderTarget =
        A3mRenderTarget::toNative( target );
      if( nativeRenderTarget )
      {
        a3m::Texture2D::Ptr const &texture =
          nativeRenderTarget->getColourTexture();
        if( texture )
        {
          getNativeRaw()->setViewport( 0, 0,
            texture->getWidth(), texture->getHeight() );
        }
      }
      getNativeRaw()->setRenderTarget( nativeRenderTarget );
    }

};

#endif // JNI_A3M_RENDERBLOCK_H
