/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * A3M Texture2D "Java" implementation.
 */
#pragma once
#ifndef JNI_A3M_TEXTURE2D_H
#define JNI_A3M_TEXTURE2D_H

#include <a3m/texture2d.h>

class A3mTexture2D
{
private:
  a3m::Texture2D::Ptr m_native;

  A3mTexture2D(a3m::Texture2D::Ptr const& native) :
    m_native(native)
  {
  }

public:
  // The following functions are ignored by SWIG (see a3m.i)
  static A3mTexture2D* toWrapper(a3m::Texture2D::Ptr const& native)
  {
    return native ? new A3mTexture2D(native) : 0;
  }

  static a3m::Texture2D::Ptr toNative(A3mTexture2D* wrapper)
  {
    return wrapper ? wrapper->getNative() : a3m::Texture2D::Ptr();
  }

  a3m::Texture2D::Ptr const& getNative() const
  {
    return m_native;
  }

public:
  int getWidth() const
  {
    return m_native->getWidth();
  }

  int getHeight() const
  {
    return m_native->getHeight();
  }

  void setHorizontalWrap(int mode)
  {
    m_native->setHorizontalWrap(
        static_cast<a3m::TextureParameters::WrapMode>(mode));
  }

  int getHorizontalWrap() const
  {
    return static_cast<int>(m_native->getHorizontalWrap());
  }

  void setVerticalWrap(int mode)
  {
    m_native->setVerticalWrap(
        static_cast<a3m::TextureParameters::WrapMode>(mode));
  }

  int getVerticalWrap() const
  {
    return static_cast<int>(m_native->getVerticalWrap());
  }

  void setMinFilter(int mode)
  {
    m_native->setMinFilter(
        static_cast<a3m::TextureParameters::FilterMode>(mode));
  }

  int getMinFilter() const
  {
    return static_cast<int>(m_native->getMinFilter());
  }

  void setMagFilter(int mode)
  {
    m_native->setMagFilter(
        static_cast<a3m::TextureParameters::FilterMode>(mode));
  }

  int getMagFilter() const
  {
    return static_cast<int>(m_native->getMagFilter());
  }

  int getMemoryUsage() const
  {
    return m_native->getSizeInBytes();
  }

  int getOpenGlTextureId() const
  {
    return m_native->getGlTexId();
  }

  void release()
  {
    m_native->release();
  }
};

#endif // JNI_A3M_TEXTURE2D_H
