/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/

#include <sstream>                  /* for std::ostringstream              */
#include <a3m/info.h>               /* For getBuildInfo()                  */
#include <a3m/window.h>             /* pss window functions                */
#include <a3m/renderdevice.h>       /* RenderDevice functions              */
#include <windows.h>                /* Gdi identifies                      */
#include <GdiPlus.h>                /* extract device information          */
#include "viewer.h"                 /* Viewer class                        */
#include "animdialog.h"             /* Animation controls                  */
#include "scenedialog.h"            /* Scene graph inspector               */

using namespace a3m;

/******************************************************************************
 * Constant Definitions
 ******************************************************************************/
#define WINDOW_WIDTH            480
#define WINDOW_HEIGHT           800
#define WINDOW_BPP              32

namespace
{
  /* Quit requested by user? */
  A3M_BOOL s_done = A3M_FALSE;
  A3M_BOOL s_mouseDown = A3M_FALSE;
  Applet *s_applet = 0;

  /*
   * Handler for application window events
   */
  class WindowEventHandler : public SimpleWindowListener
  {
    void onClose(Window* window)
    {
      s_done = A3M_TRUE;
    }

    void onKeyDown(Window* window, KeyCode key)
    {
      s_applet->keyDown( key );
    }

    void onKeyUp(Window* window, KeyCode key)
    {
      s_applet->keyUp( key );
    }

    void onMouseMove(Window* window, A3M_INT32 x, A3M_INT32 y)
    {
      if( s_applet && s_mouseDown )
      {
        s_applet->touchMove( x, y );
      }
    }

    void onMouseWheelScroll(Window* window, A3M_INT32 delta)
    {
      if( s_applet )
      {
        s_applet->mouseWheel( delta );
      }
    }

    void onMouseButtonUp(Window* window, MouseButton button,
        A3M_INT32 x, A3M_INT32 y)
    {
      if( s_applet && ( button == MOUSE_BUTTON_LEFT ) )
      {
        s_applet->touchUp( x, y );
        s_mouseDown = A3M_FALSE;
      }
    }

    void onMouseButtonDown(Window* window, MouseButton button,
        A3M_INT32 x, A3M_INT32 y)
    {
      if( s_applet && ( button == MOUSE_BUTTON_LEFT ) )
      {
        s_applet->touchDown( x, y );
        s_mouseDown = A3M_TRUE;
      }
    }

    void onResize( Window* window, A3M_INT32 width, A3M_INT32 height )
    {
      s_applet->onResize( width, height );
    }
  };
}

int main(int argc, char* argv[])
{
  // Initialize common controls (used in dialog boxes)
  INITCOMMONCONTROLSEX icx;
  icx.dwSize = sizeof(icx);
  icx.dwICC = ICC_LISTVIEW_CLASSES;
  InitCommonControlsEx(&icx);

  if( argc < 2 )
  {
    return 1;
  }

  // Hide the console window
  HWND hwnd = GetConsoleWindow();
  ShowWindow(hwnd, 0);

  HDC dc = GetDC(NULL);
  A3M_INT32 bitsPerPixel = GetDeviceCaps(dc, BITSPIXEL);
  if (bitsPerPixel != WINDOW_BPP )
  {
      pssLogError( __FILE__, __FUNCTION__, __LINE__,
           "Error: change your display setting color quality to 32 bits" );
      pssLogInfo( __FILE__, __FUNCTION__, __LINE__,
           "The gloviewer will exit after 5 seconds." );

      Sleep(5000);

      exit (0);
  }
  ReleaseDC(NULL, dc);

  std::ifstream stream;

  stream.open( argv[1] );
  if (!stream)
  {
    pssLogError( __FILE__, __FUNCTION__, __LINE__,
           "Error: unable to open configuratuion file" );
  }

  std::string line;
  A3M_INT32 windowsH = WINDOW_HEIGHT;
  A3M_INT32 windowsW = WINDOW_WIDTH;

  // find the user window's lenth and width
  while( getline( stream, line ) )
  {
    if( line.substr( 0, 12 ) == "window_size " )
    {
      std::istringstream strStream( line.substr( 12 ) );
      strStream >> windowsW >> windowsH;
      break;
    }
  }

  stream.close();

  Window::Ptr window = createWindow( TEXT("MAGE GloViewer"), windowsW, windowsH,
                                     WINDOW_BPP, A3M_TRUE);
  WindowEventHandler eventHandler;
  window->setListener(&eventHandler);

  HWND wndId = (HWND)window->getId();

  /*
   * Create modeless dialogs and hide them initially
   */
  AnimationDialog animDialog = AnimationDialog( HWND( wndId ) );
  animDialog.visible( A3M_FALSE );

  std::string cwd = argv[0];
  cwd = cwd.substr( 0, cwd.find_last_of( "\\/" ) );
  s_applet = new Viewer( cwd, argv[1], window );

  s_applet->init();

  while( !s_done )
  {

    s_applet->update();
    s_applet->draw();

    window->refresh();
  }

  delete s_applet;

  window.reset();

  return 0;
}

