/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/**
 * Viewer applet
 *
 */

#pragma once
#ifndef A3M_VIEWER_H
#define A3M_VIEWER_H

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include "applet.h"             /* Applet base class    */
#include "realtime.h"           /* Timer                */
#include "animdialog.h"         /* Animation dialog     */
#include <a3m/angle.h>          /* Angle class          */
#include <a3m/assetcachepool.h> /* AssetCachePool class */
#include <a3m/glofile.h>        /* GLO file loading     */
#include <a3m/colour.h>         /* Colour4f             */
#include <a3m/camera.h>         /* Camera class         */
#include <a3m/pointer.h>        /* a3m ScopedPtr class  */
#include <a3m/renderblock.h>    /* RenderBlock          */
#include <a3m/renderblockgroup.h> /* RenderBlockGroup   */
#include <a3m/simplerenderer.h> /* SimpleRenderer class */
#include <a3m/window.h>         /* a3m Window class     */

#include <string>               /* std::string          */
#include <vector>               /* std::vector          */
#include <fstream>              /* std::ifstream        */
#include <sstream>              /* std::istringstream   */

/** An Applet-derived class for viewing GLO files.
 */

class SceneDialog;
class StatsDialog;

class Viewer : public Applet
{
public:
  // Pass in name of configuration file
  Viewer( std::string const& cwd, std::string const& filename,
          a3m::Window::Ptr const& window  );

  ~Viewer();

  // Overrides from Applet
  virtual A3M_BOOL init();
  virtual void draw();
  virtual void update();
  virtual void touchMove( A3M_INT32 x, A3M_INT32 y );
  virtual void touchUp( A3M_INT32 x, A3M_INT32 y );
  virtual void touchDown( A3M_INT32 x, A3M_INT32 y );
  virtual void keyDown( a3m::KeyCode key );
  virtual void keyUp( a3m::KeyCode key );
  virtual void mouseWheel( A3M_INT32 delta );
  virtual void onResize( A3M_INT32 width, A3M_INT32 height );

private:

  void doAnimKeys( a3m::KeyCode key );
  void doCameraKeys( a3m::KeyCode key );

  void initConfig();    // Read config file and load glo files etc.
  void initCameras();   // Find cameras in scene
  void initLights();    // Add light if there are none in the scene

  // Updates the camera clipping planes, depending on the camera setup.
  void updateCameraClippingPlanes();
  // Updates the rotation of the axes indicator depending on the camera setup.
  void updateAxesIndicator();

  // Glo viewer can automatically adjust the clipping planes.
  A3M_BOOL m_autoClippingPlanes;

  // Filename is remembered in constructor, and loaded in init()
  std::string m_filename;

  // Collection of loaded GLO files.
  typedef std::vector< a3m::Glo::Ptr > GloList;
  typedef std::vector< GloList > GloSeqList;
  GloList m_glos;
  GloSeqList m_gloSeqs;

  // Scene nodes
  a3m::SceneNode::Ptr m_scene;
  a3m::SceneNode::Ptr m_overlay;
  a3m::SceneNode::Ptr m_axes;
  a3m::SceneNode::Ptr m_gridfloor;

  // Assets
  a3m::AssetCachePool::Ptr m_pool;

  // Rendering
  a3m::RenderBlockGroup m_renderBlockGroup;
  a3m::RenderBlock::Ptr m_sceneBlock;
  a3m::RenderBlock::Ptr m_overlayBlock;

  // Two camera nodes so that horizontal and vertical rotation can be dealt
  // With seperately. The m_cameraVRotate node's connected to the
  // m_cameraHRotate node. The m_cameraVRotate node's connected to the m_scene
  // node (them nodes, them nodes, them dry nodes...)
  a3m::SceneNode::Ptr m_cameraHRotate;
  a3m::SceneNode::Ptr m_cameraVRotate;

  a3m::Camera::Ptr m_camera;    // Default controllable camera
  a3m::Camera::Ptr m_overlayCamera;    // Camera used to render screen overlays
  A3M_UINT32 m_currentCamera;    // Index of current camera in m_cameras

  // Rotation angles
  a3m::Anglef m_hRotation;
  a3m::Anglef m_vRotation;

  // 'Look-at' position for camera and distance from camera
  a3m::Vector3f m_focus;
  A3M_FLOAT m_distance;

  // 'field of view' angle from camera
  a3m::Anglef m_fov;

  // the distance from the camera to the near clipping plane
  A3M_FLOAT m_near;

  // the distance from the camera to the far clipping plane
  A3M_FLOAT m_far;

  // Used to record relative mouse movement between calls to touchMove()
  A3M_INT32 m_lastX;
  A3M_INT32 m_lastY;

  A3M_BOOL m_ctrlDown; // Keep track of ctrl-key status
  A3M_BOOL m_shiftDown; // Keep track of shift-key status

  A3M_BOOL m_consoleVisible; // Keep track of the console visibility

  // For animation control.
  RealTime m_timer;
  A3M_BOOL m_animationPlaying;

  a3m::Colour4f m_background;

  std::vector< a3m::Camera * > m_cameras;

  a3m::ScopedPtr< SceneDialog > m_sceneDialog;  // Pointer to scene dialog
  a3m::ScopedPtr< StatsDialog > m_statsDialog;  // Pointer to stats dialog
  a3m::ScopedPtr< AnimationDialog > m_animDialog;  // Pointer to animation dialog

  a3m::Window::Ptr m_window;
};

#endif // A3M_VIEWER_H


