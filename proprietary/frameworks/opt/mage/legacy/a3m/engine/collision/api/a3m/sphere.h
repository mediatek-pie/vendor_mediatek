/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/** \file
 * Sphere collision shape class
 */
#pragma once
#ifndef A3M_SPHERE_H
#define A3M_SPHERE_H

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/shape.h>             /* for Shape                              */
#include <a3m/vector3.h>           /* for Vector3f                           */

/*****************************************************************************
 * A3M Namespace
 *****************************************************************************/
namespace a3m
{
  /**
   * \ingroup  a3mCollision
   *
   * A unit sphere used for collision detection.
   *
   * @{
   */

  /**
   * Collision sphere class.
   */
  class Sphere : public Shape
  {
  public:
    A3M_NAME_SHARED_CLASS(Sphere)

    /** Smart pointer type for this class */
    typedef SharedPtr<Sphere> Ptr;

    /**
     * Constructor.
     * Constructs a sphere of unit diameter.
     */
    Sphere()
    {
    }

    /**
     * Virtual destructor.
     * This class may be sub-classed to extend its functionality.
     */
    virtual ~Sphere()
    {
    }

    // Override
    virtual A3M_BOOL localRaycast(
      A3M_FLOAT& distance,
      /**< [out] Distance of the intersection along the ray. */
      Vector3f& normal,
      /**< [out] Normal of the surface at the point of intersection. */
      Ray const& ray
      /**< Ray to intersect with shape. */);
  };

  /** @} */

} // namespace a3m

#endif // A3M_SPHERE_H
