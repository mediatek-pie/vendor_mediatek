/**************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * Atomic operations.
 *
 */
#pragma once
#ifndef A3M_ATOMIC_H
#define A3M_ATOMIC_H

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <a3m/base_types.h>         /* for A3M_INT32                          */
#include <a3m/platform.h>           /* for A3M_PLATFORM                       */

#if A3M_PLATFORM == A3M_WINDOWS
#include <Windows.h>                /* for InterlockedIncrement() etc.        */
#elif A3M_PLATFORM == A3M_ANDROID
#include <cutils/atomic.h>          /* for android_atomic_inc() etc.          */
#endif

namespace a3m
{
  /**
   * \defgroup a3mAtomic Atomic operations
   * \ingroup  a3mRefTypes
   *
   * Defines platform-independent atomic operations.
   * In a multi-threaded environment, it is often necessary to ensure that
   * operations on a particular thread appear to occur instantaneously to the
   * rest of the system.  Failure to do so may result in subtle bugs which may
   * occur infrequently and be difficult to detect.
   */

  namespace atomic
  {
    /**
     * Performs an atomic increment on an unsigned 32-bit integer.
     */
    inline void inc(A3M_INT32& value /**< Value to increment */)
    {
#if A3M_PLATFORM == A3M_WINDOWS
      InterlockedIncrement(reinterpret_cast<A3M_UINT32*>(&value));
#elif A3M_PLATFORM == A3M_ANDROID
      android_atomic_inc(&value);
#elif A3M_PLATFORM == A3M_LINUX
      // \todo Implement atomic version for linux
      ++value;
#else
#error "Atomic increment is not supported on this platform."
#endif
    }

    /**
     * Performs an atomic decrement on an unsigned 32-bit integer.
     */
    inline void dec(A3M_INT32& value /**< Value to decrement */)
    {
#if A3M_PLATFORM == A3M_WINDOWS
      InterlockedDecrement(reinterpret_cast<A3M_UINT32*>(&value));
#elif A3M_PLATFORM == A3M_ANDROID
      android_atomic_dec(&value);
#elif A3M_PLATFORM == A3M_LINUX
      // \todo Implement atomic version for linux
      --value;
#else
#error "Atomic decrement is not supported on this platform."
#endif
    }

  } /* namespace atomic */

  /** @} */

} /* namespace a3m */

#endif /* A3M_ATOMIC_H */
