/**************************************************************************
 *
 * Copyright (c) 2013 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * Preprocessor compiler check macros.
 *
 */
#pragma once
#ifndef A3M_COMPILER_H
#define A3M_COMPILER_H

/******************************************************************************
 * Include Files
 ******************************************************************************/

/**
 * Indicates a compiler that is unknown (and possibly unsupported) by A3M. */
#define A3M_UNKNOWN_COMPILER 0
/** Used for versions of Microsoft Visual C++. */
#define A3M_MSVC 1
/** Used for all versions of GNU C++. */
#define A3M_GCC 2

#if defined _MSC_VER
#define A3M_COMPILER A3M_MSVC
#elif defined __GNUC__
#define A3M_COMPILER A3M_GCC
#else
// Only the last occurrence is parsed by doxygen.
/** Set to one of the defined compiler values depending on the compiler with
 *  which the code is being compiled.
 */
#define A3M_COMPILER A3M_UNKNOWN_COMPILER
#endif

#endif /* A3M_COMPILER_H */
