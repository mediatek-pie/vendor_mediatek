/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 **************************************************************************
 *   $Id: //swd1_mm/projects/a3m_android/jni/a3m/a3math/src/a3math_float.cpp#1 $
 *   $Revision: #1 $
 *   $DateTime: 2011/09/20 17:04:16 $
 ***************************************************************************/
/*
 * Fast floating point maths routines
 *
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <a3m/fpmaths.h>               /* Class API */
#include <math.h>                       /* for sin, cos etc. */

namespace a3m
{
  /******************************************************************************
   * Square Root Functions
   ******************************************************************************/

  /*
   * Calculate inverse square root (1/sqrt)
   */
  A3M_FLOAT invSqrt( A3M_FLOAT x  /* number */ )
  {
    /*
     * This code was take from the Wikipedia page:
     * http://en.wikipedia.org/wiki/Fast_inverse_square_root
     * there is an explanation of this trick on that page and in many other
     * places on the web. I have replaced the "magic number" with the one
     * discovered by Charles McEniry to be more accurate. The algorithm does
     * assume the floating point number is in IEEE 754 format.
     */
    union
    {
      A3M_FLOAT f;
      A3M_INT32 i;
    } tmp;
    A3M_FLOAT y;
    tmp.f = x;
    tmp.i = 0x5f375a86 - (tmp.i / 2); /* originally 0x5f3759df */
    y = tmp.f;
    return y * (1.5f - (0.5f * x * y * y));
  }

  /*
  * Calculate square root
  * return square root of given number
  */
  A3M_FLOAT sqrt( A3M_FLOAT x  /* number */ )
  {
    return ( x == 0.0f ) ? 0.0f : (1.0f / invSqrt( x ));
  }

  /******************************************************************************
   * Miscellaneous
   ******************************************************************************/
  /*
   * Calls the stdlib function.
   */
  A3M_FLOAT fmod(A3M_FLOAT numerator, A3M_FLOAT denominator)
  {
    return ::fmod(numerator, denominator);
  }

} /* namespace a3m */
