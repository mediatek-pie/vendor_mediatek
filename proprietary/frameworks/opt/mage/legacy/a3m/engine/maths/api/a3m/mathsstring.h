/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2013. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * Maths string conversion functions.
 */
#pragma once
#ifndef A3M_MATHSSTRING_H
#define A3M_MATHSSTRING_H

#include <string>                       /* for std::string                   */
#include <a3m/base_types.h>             /* for A3M_FLOAT etc.                */

namespace a3m
{

  /*
   * Forward declarations to avoid unnecessary dependencies.
   */

  template<typename T> class Angle;
  typedef Angle<A3M_FLOAT> Anglef;

  template<typename T> struct Quaternion;
  typedef Quaternion<A3M_FLOAT> Quaternionf;

  template<typename T> struct Vector2;
  typedef Vector2<A3M_FLOAT> Vector2f;
  typedef Vector2<A3M_INT32> Vector2i;
  typedef Vector2<A3M_BOOL> Vector2b;

  template<typename T> struct Vector3;
  typedef Vector3<A3M_FLOAT> Vector3f;
  typedef Vector3<A3M_INT32> Vector3i;
  typedef Vector3<A3M_BOOL> Vector3b;

  template<typename T> struct Vector4;
  typedef Vector4<A3M_FLOAT> Vector4f;
  typedef Vector4<A3M_INT32> Vector4i;
  typedef Vector4<A3M_BOOL> Vector4b;

  template<typename T> struct Matrix2;
  typedef Matrix2<A3M_FLOAT> Matrix2f;

  template<typename T> struct Matrix3;
  typedef Matrix3<A3M_FLOAT> Matrix3f;

  template<typename T> struct Matrix4;
  typedef Matrix4<A3M_FLOAT> Matrix4f;

  /**
   * Creates a string representation of an Anglef.
   * \return String representation
   */
  std::string toString(Anglef const& a /** Angle to convert */);

  /**
   * Creates a string representation of a Quaternionf.
   * \return String representation
   */
  std::string toString(Quaternionf const& q /** Quaternion to convert */);

  /**
   * Creates a string representation of a Vector2f.
   * \return String representation
   */
  std::string toString(Vector2f const& v /** Vector to convert */);

  /**
   * Creates a string representation of a Vector2i.
   * \return String representation
   */
  std::string toString(Vector2i const& v /** Vector to convert */);

  /**
   * Creates a string representation of a Vector2b.
   * \return String representation
   */
  std::string toString(Vector2b const& v /** Vector to convert */);

  /**
   * Creates a string representation of a Vector3f.
   * \return String representation
   */
  std::string toString(Vector3f const& v /** Vector to convert */);

  /**
   * Creates a string representation of a Vector3i.
   * \return String representation
   */
  std::string toString(Vector3i const& v /** Vector to convert */);

  /**
   * Creates a string representation of a Vector3b.
   * \return String representation
   */
  std::string toString(Vector3b const& v /** Vector to convert */);

  /**
   * Creates a string representation of a Vector4f.
   * \return String representation
   */
  std::string toString(Vector4f const& v /** Vector to convert */);

  /**
   * Creates a string representation of a Vector4i.
   * \return String representation
   */
  std::string toString(Vector4i const& v /** Vector to convert */);

  /**
   * Creates a string representation of a Vector4b.
   * \return String representation
   */
  std::string toString(Vector4b const& v /** Vector to convert */);

  /**
   * Creates a string representation of a Matrix2f.
   * \return String representation
   */
  std::string toString(Matrix2f const& m /** Matrix to convert */);

  /**
   * Creates a string representation of a Matrix3f.
   * \return String representation
   */
  std::string toString(Matrix3f const& m /** Matrix to convert */);

  /**
   * Creates a string representation of a Matrix4f.
   * \return String representation
   */
  std::string toString(Matrix4f const& m /** Matrix to convert */);

} // namespace a3m

#endif // A3M_MATHSSTRING_H
