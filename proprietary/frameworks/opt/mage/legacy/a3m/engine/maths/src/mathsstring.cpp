/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2013. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * Maths string conversion function implementations.
 */

#include <sstream>                      /* for std::ostringstream            */
#include <a3m/mathsstring.h>            /* this module's header              */
#include <a3m/angle.h>                  /* for Angle                         */
#include <a3m/matrix2.h>                /* for Matrix2                       */
#include <a3m/matrix3.h>                /* for Matrix3                       */
#include <a3m/matrix4.h>                /* for Matrix4                       */
#include <a3m/quaternion.h>             /* for Quaternion                    */
#include <a3m/vector2.h>                /* for Vector2                       */
#include <a3m/vector3.h>                /* for Vector3                       */
#include <a3m/vector4.h>                /* for Vector4                       */

namespace
{

  using namespace a3m;

  /*
   * Define a traits class containing suffixes associated with basic types.
   */
  template<typename T>
  struct TypeTraits
  {
  };

  template<>
  struct TypeTraits<A3M_FLOAT>
  {
    static A3M_CHAR8 const SUFFIX = 'f';
  };

  template<>
  struct TypeTraits<A3M_INT32>
  {
    static A3M_CHAR8 const SUFFIX = 'i';
  };

  template<>
  struct TypeTraits<A3M_BOOL>
  {
    static A3M_CHAR8 const SUFFIX = 'b';
  };

  template<typename T>
  std::string doToString(Angle<T> const& a)
  {
    // Degrees is most useful to humans.
    std::ostringstream stream;
    stream << "Angle" << TypeTraits<T>::SUFFIX << "(" <<
           getDegrees(a) << " degrees)";
    return stream.str();
  }

  template<typename T>
  std::string doToString(Vector2<T> const& v)
  {
    std::ostringstream stream;
    stream << "Vector2" << TypeTraits<T>::SUFFIX << "(" <<
           v.x << ", " << v.y << ")";
    return stream.str();
  }

  template<typename T>
  std::string doToString(Vector3<T> const& v)
  {
    std::ostringstream stream;
    stream << "Vector3" << TypeTraits<T>::SUFFIX << "(" <<
           v.x << ", " << v.y << ", " << v.z << ")";
    return stream.str();
  }

  template<typename T>
  std::string doToString(Vector4<T> const& v)
  {
    std::ostringstream stream;
    stream << "Vector4" << TypeTraits<T>::SUFFIX << "(" <<
           v.x << ", " << v.y << ", " << v.z << ", " << v.w << ")";
    return stream.str();
  }

  template<typename T>
  std::string doToString(Quaternion<T> const& q)
  {
    // Quaternion also prints its axis-angle representation for ease of
    // interpretation.
    Vector3<T> axis = getAxis(q);
    Angle<T> angle = getAngle(q);

    std::ostringstream stream;
    stream << "Quaternion" << TypeTraits<T>::SUFFIX << "(" <<
           q.a << ", " << q.b << ", " << q.c << ", " << q.d << ") (" <<
           std::endl <<
           "  " << toString(axis) << std::endl <<
           "  " << toString(angle) << std::endl <<
           ")";
    return stream.str();
  }

  template<typename T>
  std::string doToString(Matrix2<T> const& m)
  {
    std::ostringstream stream;
    stream << "Matrix2" << TypeTraits<T>::SUFFIX << "(" << std::endl <<
           "  " << toString(m.i) << ", " << std::endl <<
           "  " << toString(m.j) << std::endl <<
           ")";
    return stream.str();
  }

  template<typename T>
  std::string doToString(Matrix3<T> const& m)
  {
    std::ostringstream stream;
    stream << "Matrix3" << TypeTraits<T>::SUFFIX << "(" << std::endl <<
           "  " << toString(m.i) << ", " << std::endl <<
           "  " << toString(m.j) << ", " << std::endl <<
           "  " << toString(m.k) << std::endl <<
           ")";
    return stream.str();
  }

  template<typename T>
  std::string doToString(Matrix4<T> const& m)
  {
    std::ostringstream stream;
    stream << "Matrix4" << TypeTraits<T>::SUFFIX << "(" << std::endl <<
           "  " << toString(m.i) << ", " << std::endl <<
           "  " << toString(m.j) << ", " << std::endl <<
           "  " << toString(m.k) << ", " << std::endl <<
           "  " << toString(m.t) << std::endl <<
           ")";
    return stream.str();
  }

} // namespace

namespace a3m
{

  /*
   * We wrap the templated functions instead of exposing them in the header to
   * hide the dependency on the streams library (to keep compilation times
   * down).  In addition, this limits toString() to operating on known types.
   */

  std::string toString(Anglef const& a)
  {
    return doToString<A3M_FLOAT>(a);
  }

  std::string toString(Quaternionf const& q)
  {
    return doToString<A3M_FLOAT>(q);
  }

  std::string toString(Vector2f const& v)
  {
    return doToString<A3M_FLOAT>(v);
  }

  std::string toString(Vector2i const& v)
  {
    return doToString<A3M_INT32>(v);
  }

  std::string toString(Vector2b const& v)
  {
    return doToString<A3M_BOOL>(v);
  }

  std::string toString(Vector3f const& v)
  {
    return doToString<A3M_FLOAT>(v);
  }

  std::string toString(Vector3i const& v)
  {
    return doToString<A3M_INT32>(v);
  }

  std::string toString(Vector3b const& v)
  {
    return doToString<A3M_BOOL>(v);
  }

  std::string toString(Vector4f const& v)
  {
    return doToString<A3M_FLOAT>(v);
  }

  std::string toString(Vector4i const& v)
  {
    return doToString<A3M_INT32>(v);
  }

  std::string toString(Vector4b const& v)
  {
    return doToString<A3M_BOOL>(v);
  }

  std::string toString(Matrix2f const& m)
  {
    return doToString<A3M_FLOAT>(m);
  }

  std::string toString(Matrix3f const& m)
  {
    return doToString<A3M_FLOAT>(m);
  }

  std::string toString(Matrix4f const& m)
  {
    return doToString<A3M_FLOAT>(m);
  }

} // namespace a3m
