/**************************************************************************
 *
 * Copyright (c) 2013 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * A fragment shader for depth of field effect
 *
 */

precision mediump float;

/* Material uniforms */
uniform sampler2D u_m_sharp;
uniform sampler2D u_m_blurred;
uniform sampler2D u_m_depth;

uniform float u_m_focusDistance;
uniform float u_m_focusRangeReciprocal;
uniform float u_m_nearXFar;
uniform float u_m_far;
uniform float u_m_nearMinusFar;

varying vec2 v_texcoord;

void main()
{
    float depthBuf = texture2D( u_m_depth, v_texcoord ).x;
    float depthLinear = u_m_nearXFar / ( u_m_far + u_m_nearMinusFar * depthBuf );

    float focus_mix = smoothstep( abs( depthLinear - u_m_focusDistance ) *
                             u_m_focusRangeReciprocal, 0.0, 1.0 );

    gl_FragColor = mix( texture2D( u_m_sharp, v_texcoord ),
                        texture2D( u_m_blurred, v_texcoord ), focus_mix );
}
