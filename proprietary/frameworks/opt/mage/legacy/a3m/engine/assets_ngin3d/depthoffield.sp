1.0
vertex_source_file ngin3d#depthoffield.vert
fragment_source_file ngin3d#depthoffield.frag

uniform u_m_sharp M_SHARP_TEXTURE
uniform u_m_blurred M_BLURRED_TEXTURE
uniform u_m_depth M_DEPTH_TEXTURE
uniform u_m_focusDistance M_FOCUS_DISTANCE
uniform u_m_focusRangeReciprocal M_FOCUS_RANGE_RECIPROCAL
uniform u_m_nearXFar M_NEAR_X_FAR
uniform u_m_far M_FAR
uniform u_m_nearMinusFar M_NEAR_MINUS_FAR
