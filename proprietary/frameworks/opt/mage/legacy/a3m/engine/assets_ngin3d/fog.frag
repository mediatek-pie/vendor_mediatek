/**************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * A simple vertex shader example
 *
 */

precision mediump float;
precision mediump int;

#define FOG_INTENSITY 1.0

uniform sampler2D u_m_diffuseTexture;
uniform vec4 u_m_emissiveColour;

// Varyings
varying vec2 v_screen;

varying vec2 v_texCoord0;
varying vec2 v_texCoord1;
varying vec2 v_texCoord2;
varying vec2 v_texCoord3;

//------------------------------------------------------------------------------
void main(void)
{
  float fade = 1.0 - dot( v_screen, v_screen );
  float alpha = fade * ( texture2D( u_m_diffuseTexture, v_texCoord1 ).r +
                         texture2D( u_m_diffuseTexture, v_texCoord2 ).r +
                         texture2D( u_m_diffuseTexture, v_texCoord3 ).r );
  gl_FragColor = u_m_emissiveColour;
  gl_FragColor.a = alpha * FOG_INTENSITY;
}
