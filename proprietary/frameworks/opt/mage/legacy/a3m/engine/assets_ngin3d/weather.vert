/**************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * A simple vertex shader example
 *
 */

precision highp float;
precision highp int;

/* Transformation uniforms */
uniform mat4 u_t_model;
uniform mat4 u_t_modelViewProjection;
uniform mat3 u_t_normal;

attribute vec4 a_position;
attribute vec3 a_normal;
attribute vec2 a_uv0;

varying vec2 v_texcoord;
varying vec3 v_normal;


#define MAX_LIGHTS 3
#define MAX_TEXTURES 1


// Vertex Attributes


// Uniforms

uniform vec4 uCameraPosition;

// Uniforms
uniform int u_l_count;
uniform vec4 u_l_position[MAX_LIGHTS];
uniform vec4 u_l_ambient[MAX_LIGHTS];
uniform vec4 u_l_diffuse[MAX_LIGHTS];
uniform vec4 u_l_specular[MAX_LIGHTS];
uniform float u_l_radius_reciprocal[MAX_LIGHTS];

uniform vec4 u_m_ambientColour;
uniform vec4 u_m_diffuseColour;
uniform vec4 u_m_specularColour;
uniform vec4 u_m_emissiveColour;
uniform float u_m_shininess;

uniform sampler2D uTexture[MAX_TEXTURES];

// Varyings

varying vec4 v_colour;
varying vec2 v_texCoord;


//------------------------------------------------------------------------------
void main(void)
{
    gl_Position = u_t_modelViewProjection * a_position;
    vec3 vertexPosition = (u_t_model * a_position).xyz;

    v_colour = u_m_emissiveColour + vec4( 0.2, 0.2, 0.2, 1.0 ) * u_m_diffuseColour;

    vec3 normal = normalize( u_t_normal * a_normal );

    for (int i = 0; i < MAX_LIGHTS; ++i)
    {
        // Make sure not to exceed the total number of lights.
        if ( i < u_l_count )
        {
            vec3 vertexToLight = u_l_position[i].xyz - vertexPosition;

            float attenuation = 1.0 - ( length(vertexToLight) * u_l_radius_reciprocal[i] );

            if( attenuation > 0.0 )
            {
                // Calculate diffuse lighting
                float normalDotLight = ( dot(normal, normalize( vertexToLight ) ) + 1.0 ) * 0.4;

                vec4 lightColour = u_l_diffuse[i];

                if ( normalDotLight > 0.0 )
                {
                    v_colour += ( normalDotLight * attenuation ) * lightColour * u_m_diffuseColour;
                }
            }
        }
    }

    v_colour.a = 1.0;
    v_texCoord = a_uv0.xy;
}

