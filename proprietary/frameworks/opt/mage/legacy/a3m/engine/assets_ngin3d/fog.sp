1.0
vertex_source_file ngin3d#fog.vert
fragment_source_file ngin3d#fog.frag

uniform u_m_diffuseTexture M_DIFFUSE_TEXTURE
uniform u_m_emissiveColour M_EMISSIVE_COLOUR

uniform u_time TIME
