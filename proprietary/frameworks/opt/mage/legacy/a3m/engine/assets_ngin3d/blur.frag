/**************************************************************************
 *
 * Copyright (c) 2013 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * A fragment shader for blur stage of depth-of-field effect
 *
 */

precision mediump float;

/* Material uniforms */
uniform sampler2D u_m_diffuseTexture;

// Adjusted texture coordinates
varying vec2 v_texcoord0;
varying vec2 v_texcoord1;
varying vec2 v_texcoord2;
varying vec2 v_texcoord3;
varying vec2 v_texcoord4;

void main()
{
    vec4 c0 = texture2D( u_m_diffuseTexture, v_texcoord0 );
    vec4 c1 = texture2D( u_m_diffuseTexture, v_texcoord1 );
    vec4 c2 = texture2D( u_m_diffuseTexture, v_texcoord2 );
    vec4 c3 = texture2D( u_m_diffuseTexture, v_texcoord3 );
    vec4 c4 = texture2D( u_m_diffuseTexture, v_texcoord4 );

    // Note that the following factors should sum to 1.0
    float a0 = c0.a * 0.1;
    float a1 = c1.a * 0.25;
    float a2 = c2.a * 0.3;
    float a3 = c3.a * 0.25;
    float a4 = c4.a * 0.1;

    vec3 acc = c0.rgb * a0
             + c1.rgb * a1
             + c2.rgb * a2
             + c3.rgb * a3
             + c4.rgb * a4;

    gl_FragColor.rgb = acc;
    gl_FragColor.a = a0 + a1 + a2 + a3 + a4;
}
