/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * Android OS adaptor for the A3M Logging facility
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <android/log.h>       /* for __android_log_vprint() */
#include <a3m/base_types.h>    /* for A3M_INT32, etc.        */
#include <sstream>             /* For std::ostringstream     */

#define PSS_MAX_LOGGING_TEXT_LENGTH     1024

#define LOG_TAG "A3M"

// Anonymous namespace for local functions
namespace
{

  /*
   * Returns formatted log message containing module, function and message
   * information.
   */
  static std::string formatLogMessage(const A3M_CHAR8* pszModule,
                                      const A3M_CHAR8* pszFunction,
                                      const A3M_INT32 line,
                                      const A3M_CHAR8* pszFormat)
  {
    std::ostringstream stream;
    std::string fullModulePath(pszModule);

    // Extracting only name of the module without path
    std::string moduleName = fullModulePath.substr(
                               fullModulePath.find_last_of("/\\") + 1);
    stream << moduleName.c_str() << "; " << pszFunction
           << "; "  << line << "; " << pszFormat;

    return stream.str();
  }
}

/*****************************************************************************
 * Global Functions
 *****************************************************************************/
/*
 * Initialise the a3m logging interface.
 * \return none
 */
void pssLogInit()
{
  // Nothing to do
}

/*
 * Deinitialise the a3m logging interface.
 * It is currently a place holder for any resource needs to be released in the
 * future.
 * \return none
 */
void pssLogDeInit()
{
  // Nothing to do
}


/*
 * Error log messages to be used to log critical errors using a format string
 * and a variable number of format arguments.
 */
void pssLogError(const A3M_CHAR8* pszModule, const A3M_CHAR8* pszFunction,
                 A3M_INT32 line, const A3M_CHAR8* pszFormat, ...)
{
  std::string formattedMessage =
    formatLogMessage(pszModule, pszFunction, line, pszFormat);
  va_list ap;
  va_start( ap, pszFormat );
  __android_log_vprint( ANDROID_LOG_ERROR, LOG_TAG,
                        formattedMessage.c_str(), ap );
  va_end(ap);
}

/* Log a specified message using a format string and a variable number of
 * format arguments.
 */
void pssLogWarn(const A3M_CHAR8* pszModule, const A3M_CHAR8* pszFunction,
                A3M_INT32 line, const A3M_CHAR8* pszFormat, ...)
{
  std::string formattedMessage =
    formatLogMessage(pszModule, pszFunction, line, pszFormat);
  va_list ap;
  va_start( ap, pszFormat );
  __android_log_vprint( ANDROID_LOG_WARN, LOG_TAG,
                        formattedMessage.c_str(), ap );
  va_end(ap);
}

/* Log a specified message using a format string and a variable number of
 * format arguments.
 */
void pssLogInfo(const A3M_CHAR8* pszModule, const A3M_CHAR8* pszFunction,
                A3M_INT32 line, const A3M_CHAR8* pszFormat, ...)
{
  std::string formattedMessage =
    formatLogMessage(pszModule, pszFunction, line, pszFormat);
  va_list ap;
  va_start( ap, pszFormat );
  __android_log_vprint( ANDROID_LOG_INFO, LOG_TAG,
                        formattedMessage.c_str(), ap );
  va_end(ap);
}

/* Log a "debug" message using a format string and a variable number
 * of format arguments.
 */
void pssLogDebug(const A3M_CHAR8* pszModule, const A3M_CHAR8* pszFunction,
                 A3M_INT32 line, const A3M_CHAR8* pszFormat, ...)
{
  std::string formattedMessage =
    formatLogMessage(pszModule, pszFunction, line, pszFormat);
  va_list ap;
  va_start( ap, pszFormat );
  __android_log_vprint( ANDROID_LOG_DEBUG, LOG_TAG,
                        formattedMessage.c_str(), ap );
  va_end(ap);
}

