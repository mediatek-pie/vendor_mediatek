/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * Window class.
 */

#pragma once
#ifndef PSS_WINDOW_H
#define PSS_WINDOW_H

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/base_types.h>           /* for A3M_INT16 etc.                  */
#include <a3m/keycode.h>              /* for KeyCode enum                    */
#include <a3m/mouse.h>                /* for MouseButton enum                */
#include <a3m/noncopyable.h>          /* for Shared and SharedPtr            */
#include <a3m/pointer.h>              /* for NonCopyable                     */

/** \defgroup a3mPssWinApi PSS Windowing API types and functions
 *  \ingroup  a3mPss
 *
 *  Pss Window API provides services for creating scene window, get the window
 *  parameters, close or refresh the window.
 *
 * @{
 */

namespace a3m
{

  class Window;

  /**
   * Window event listener interface.
   * Should be implemented by classes that want to listen to window events by
   * calling Window::setListener().
   */
  class WindowListener
  {
  public:
    /**
     * Virtual destructor.
     */
    virtual ~WindowListener() {}

    /**
     * Called when a keyboard key is pressed.
     */
    virtual void onKeyUp(
      Window* window, /**< Calling window */
      KeyCode key /**< Code for the pressed key */) = 0;

    /**
     * Called when a keyboard key is released.
     */
    virtual void onKeyDown(
      Window* window, /**< Calling window */
      KeyCode key /**< Code for the released key */) = 0;

    /**
     * Called when a mouse button is pressed.
     */
    virtual void onMouseButtonUp(
      Window* window, /**< Calling window */
      MouseButton button, /**< Mouse button pressed */
      A3M_INT32 x, /**< X-coordinate of mouse */
      A3M_INT32 y /**< Y-coordinate of mouse */) = 0;

    /**
     * Called when a mouse button is pressed.
     */
    virtual void onMouseButtonDown(
      Window* window, /**< Calling window */
      MouseButton button, /**< Mouse button released */
      A3M_INT32 x, /**< X-coordinate of mouse */
      A3M_INT32 y /**< Y-coordinate of mouse */) = 0;

    /**
     * Called when the mouse is moved.
     */
    virtual void onMouseMove(
      Window* window, /**< Calling window */
      A3M_INT32 x, /**< X-coordinate of mouse */
      A3M_INT32 y /**< Y-coordinate of mouse */) = 0;

    /**
     * Called when the mouse wheel is scrolled.
     */
    virtual void onMouseWheelScroll(
      Window* window, /**< Calling window */
      A3M_INT32 delta /**< Amount by which mouse wheel was scrolled */) = 0;

    /**
     * Called when the mouse leaves the window area.
     */
    virtual void onMouseLeave(
      Window* window, /**< Calling window */
      A3M_INT32 x, /**< X-coordinate of mouse where it left the window */
      A3M_INT32 y /**< Y-coordinate of mouse where it left the window */) = 0;

    /**
     * Called when the window is resized.
     */
    virtual void onResize(
      Window* window, /**< Calling window */
      A3M_INT32 width /**< New width of window */,
      A3M_INT32 height /**< New height of window */) = 0;

    /**
     * Called when the window is moved.
     */
    virtual void onMove(
      Window* window, /**< Calling window */
      A3M_INT32 left, /**< New position of left edge of window */
      A3M_INT32 top /**< New position of top edge of window */) = 0;

    /**
     * Called when the window is closed.
     */
    virtual void onClose(
      Window* window /**< Calling window */) = 0;
  };

  /**
   * Implements empty callbacks for all WindowListener functions.
   * This class can be handy if you only want to implement a few callbacks,
   * but care should be taken to ensure that the functions are genuinely
   * re-implemented by the deriving class (i.e. no typos in the function
   * signatures).
   */
  class SimpleWindowListener : public WindowListener
  {
  public:
    /**
     * Virtual destructor.
     */
    virtual ~SimpleWindowListener() {}

    virtual void onKeyUp(Window* window, KeyCode key) {}
    virtual void onKeyDown(Window* window, KeyCode key) {}
    virtual void onMouseButtonUp(
      Window* window, MouseButton button, A3M_INT32 x, A3M_INT32 y) {}
    virtual void onMouseButtonDown(
      Window* window, MouseButton button, A3M_INT32 x, A3M_INT32 y) {}
    virtual void onMouseMove(Window* window, A3M_INT32 x, A3M_INT32 y) {}
    virtual void onMouseWheelScroll(Window* window, A3M_INT32 delta) {}
    virtual void onMouseLeave(Window* window, A3M_INT32 x, A3M_INT32 y) {}
    virtual void onResize(Window* window, A3M_INT32 width, A3M_INT32 height) {}
    virtual void onMove(Window* window, A3M_INT32 left, A3M_INT32 top) {}
    virtual void onClose(Window* window) {}
  };

  /**
   * Window class with associated OpenGL context.
   */
  class Window : public Shared, NonCopyable
  {
  public:
    /** Smart pointer type for this class */
    typedef SharedPtr<Window> Ptr;

    /**
     * Destructor
     */
    virtual ~Window() {}

    /**
     * Refreshes the window by flipping the back buffer and polling for events.
     */
    virtual void refresh() = 0;

    /**
     * Sets the listener for the window.
     * Passing null will remove any currently assigned listener.
     */
    virtual void setListener(WindowListener* listener /**< Listener */) = 0;

    /**
     * Set active window with given width and height reference the window's
     * top/left corner
     */
    virtual void setSize(A3M_INT32 width /**< window width */,
                         A3M_INT32 height /**< window's height*/) = 0;

    /**
     * Get the position of the left edge of the window.
     * \return Left edge coordinate.
     */
    virtual A3M_INT32 getLeft() const = 0;

    /**
     * Get the position of the top edge of the window.
     * \return Top edge coordinate.
     */
    virtual A3M_INT32 getTop() const = 0;

    /**
     * Get window width.
     * \return Width of the window.
     */
    virtual A3M_INT32 getWidth() const = 0;

    /**
     * Get window height.
     * \return Height of the window.
     */
    virtual A3M_INT32 getHeight() const = 0;

    /**
     * Returns the ID of the platform-specific window object.
     * This function will be platform dependant, and care should be taken when
     * using it, as it will result in unportable code.
     * \return Internal ID of window
     */
    virtual A3M_UINT32 getId() const = 0;
  };

  /**
   * Create a new window with the specified title, width, height and
   * bits-per-pixel.
   *
   * \todo How does it behave with oversize dimensions? etc.
   */
  Window::Ptr createWindow(
    const A3M_CHAR8* title,
    /**< Title text for the window */
    A3M_INT32 width,
    /**< Width of the window */
    A3M_INT32 height,
    /**< Height of the window */
    A3M_INT32 bpp,
    /**< Bits per pixel */
    A3M_BOOL setClientArea
    /**< If TRUE, window client area is resized to match the specified width
         and height*/ = A3M_FALSE);

  /**
   * Determines the size of the computer's screen in pixels.
   */
  void getScreenSize(
    A3M_INT32& width,
    /**< [out] Returned screen width */
    A3M_INT32& height
    /**< [out] Returned screen height */);

  /**
   * Swap active window orientation.
   */
  inline void swapOrientation(Window& window)
  {
    window.setSize(window.getHeight(), window.getWidth());
  }

} // namespace a3m

/** @} */

#endif // PSS_WINDOW_H
