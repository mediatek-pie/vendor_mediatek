/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * PSS Timer API declaration
 *
 */

#pragma once
#ifndef PSS_TIMER_H
#define PSS_TIMER_H

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/base_types.h> /* A3M_UINT32 etc. */

/** \defgroup  a3mPssTimerApi PSS Timer API
 *  \ingroup   a3mPss
 *
 *  The PSS Timer API provides a high resolution timer which measures CPU time.
 *  It only provides a time stamp representing the elapsed time since the timer
 *  was initialised.
 *
 *  @{
 */

/*****************************************************************************
 * Global Functions
 *****************************************************************************/
/**
 * PSS Timer initialisation routine.
 * Initialises the PSS hi-res timer.  This must be called before the rest of
 * the timer API can be used and should ideally be done at application startup.
 * If this is called multiple times only the first invocation has an effect.
 * When successful, TRUE is returned, else if a hi-res timer is not available
 * on this platform, FALSE is returned.
 *
 * \return TRUE if successful, else FALSE
 */
A3M_BOOL pssTimerInit(void);

/**
 * PSS Timer get elapsed cycle count.
 * This returns the number of clock cycles since #pssTimerInit was called.
 * To convert this into seconds, divide the returned cycle count with the
 * timer's clock frequency (obtained with #pssTimerGetFrequency).
 *
 * \return Clock cycle count since timer initialisation
 */
A3M_INT64 pssTimerGetTicks(void);

/**
 * PSS Timer get elapsed time in milliseconds.
 * This returns the time in milliseconds since #pssTimerInit was called.
 *
 * \return Elapsed time in milliseconds since timer initialisation
 */
A3M_UINT32 pssTimerGetTimeMs(void);

/**
 * PSS Timer get clock frequency.
 * This returns the frequency of the clock used to advance the timer
 * tick count.
 *
 * \return Timer clock frequency as ticks-per-second (Hz)
 */
A3M_UINT32 pssTimerGetFrequency(void);

/** @} */

#endif
