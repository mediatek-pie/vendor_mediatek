/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * PC keyboard key codes
 *
 */

#pragma once
#ifndef PSS_KEYCODE_H
#define PSS_KEYCODE_H

/******************************************************************************
 * Include Files
 ******************************************************************************/
/* None */


namespace a3m
{

  /******************************************************************************
   * Type Definitions
   ******************************************************************************/

  /** \defgroup  a3mPssKeyType PSS Key codes KEY_A, KEY_TAB, etc.
   *  \ingroup   a3mPss
   *  @{
   */

  /** Codes for keyboard keys */
  enum KeyCode
  {
    KEY_0,           /**< 0 key */
    KEY_1,           /**< 1 key */
    KEY_2,           /**< 2 key */
    KEY_3,           /**< 3 key */
    KEY_4,           /**< 4 key */
    KEY_5,           /**< 5 key */
    KEY_6,           /**< 6 key */
    KEY_7,           /**< 7 key */
    KEY_8,           /**< 8 key */
    KEY_9,           /**< 9 key */
    KEY_A,           /**< A key */
    KEY_B,           /**< B key */
    KEY_C,           /**< C key */
    KEY_D,           /**< D key */
    KEY_E,           /**< E key */
    KEY_F,           /**< F key */
    KEY_G,           /**< G key */
    KEY_H,           /**< H key */
    KEY_I,           /**< I key */
    KEY_J,           /**< J key*/
    KEY_K,           /**< K key */
    KEY_L,           /**< L key */
    KEY_M,           /**< M key */
    KEY_N,           /**< N key */
    KEY_O,           /**< O key */
    KEY_P,           /**< P key */
    KEY_Q,           /**< Q key */
    KEY_R,           /**< R key */
    KEY_S,           /**< S key */
    KEY_T,           /**< T key */
    KEY_U,           /**< U key */
    KEY_V,           /**< V key */
    KEY_W,           /**< W key */
    KEY_X,           /**< X key */
    KEY_Y,           /**< Y key */
    KEY_Z,           /**< Z key */

    KEY_GRAVE,          /**< ` key */
    KEY_MINUS,          /**< - key */
    KEY_EQUALS,         /**< = key */
    KEY_OPEN_BRACKET,   /**< [ key */
    KEY_CLOSE_BRACKET,  /**< ] key */
    KEY_SEMICOLON,      /**< ; key */
    KEY_APOSTROPHE,     /**< ' key */
    KEY_HASH,           /**< # key */
    KEY_BACKSLASH,      /**< \ key */
    KEY_COMMA,          /**< , key */
    KEY_PERIOD,         /**< PERIOD key */
    KEY_SLASH,          /**< / key */

    KEY_ESCAPE,       /**< ESCAPE key */
    KEY_TAB,          /**< TAB key */
    KEY_CAPS_LOCK,    /**< CAPS LOCK key */

    KEY_SHIFT,        /**< SHIFT key (left or right) */
    KEY_CONTROL,      /**< CTRL key (left or right) */
    KEY_ALT,          /**< ALT key (left or right) */
    KEY_SUPER,        /**< SUPER (Window) key (left or right) */
    KEY_MENU,         /**< MENU key */

    KEY_SPACE,        /**< SPACEBAR */
    KEY_BACKSPACE,    /**< BACKSPACE key */
    KEY_RETURN,       /**< ENTER key */

    KEY_PRINT_SCREEN, /**< PRINT SCREEN key */
    KEY_SCROLL_LOCK,  /**< SCROLL LOCK key */
    KEY_PAUSE,        /**< PAUSE key */

    KEY_INSERT,       /**< INSERT key */
    KEY_DELETE,       /**< DELETE key */
    KEY_HOME,         /**< HOME key */
    KEY_END,          /**< END key */
    KEY_PAGE_UP,      /**< PAGE UP key */
    KEY_PAGE_DOWN,    /**< PAGE DOWN key */

    KEY_UP,           /**< UP ARROW key */
    KEY_DOWN,         /**< DOWN ARROW key */
    KEY_LEFT,         /**< LEFT ARROW key */
    KEY_RIGHT,        /**< RIGHT ARROW key */

    // Numpad
    KEY_NUM_LOCK,           /**< NUM LOCK key */
    KEY_NUMPAD_0,           /**< Numeric keypad 0 key*/
    KEY_NUMPAD_1,           /**< Numeric keypad 1 key */
    KEY_NUMPAD_2,           /**< Numeric keypad 2 key */
    KEY_NUMPAD_3,           /**< Numeric keypad 3 key */
    KEY_NUMPAD_4,           /**< Numeric keypad 4 key */
    KEY_NUMPAD_5,           /**< Numeric keypad 5 key */
    KEY_NUMPAD_6,           /**< Numeric keypad 6 key */
    KEY_NUMPAD_7,           /**< Numeric keypad 7 key */
    KEY_NUMPAD_8,           /**< Numeric keypad 8 key */
    KEY_NUMPAD_9,           /**< Numeric keypad 9 key */
    KEY_NUMPAD_ADD,         /**< Numeric keypad + key */
    KEY_NUMPAD_SUBTRACT,    /**< Numeric keypad - key */
    KEY_NUMPAD_MULTIPLY,    /**< Numeric keypad * key */
    KEY_NUMPAD_DIVIDE,      /**< Numeric keypad / key */
    KEY_NUMPAD_DECIMAL,     /**< Numeric keypad decimal key */
    KEY_NUMPAD_SEPARATOR,   /**< Numeric keypad separator key */

    // Function keys
    KEY_F1,          /**< F1 key */
    KEY_F2,          /**< F2 key */
    KEY_F3,          /**< F3 key */
    KEY_F4,          /**< F4 key */
    KEY_F5,          /**< F5 key */
    KEY_F6,          /**< F6 key */
    KEY_F7,          /**< F7 key */
    KEY_F8,          /**< F8 key */
    KEY_F9,          /**< F9 key */
    KEY_F10,         /**< F10 key */
    KEY_F11,         /**< F11 key */
    KEY_F12,         /**< F12 key */
    KEY_F13,         /**< F13 key */
    KEY_F14,         /**< F14 key */
    KEY_F15,         /**< F15 key */
    KEY_F16,         /**< F16 key */
    KEY_F17,         /**< F17 key */
    KEY_F18,         /**< F18 key */
    KEY_F19,         /**< F19 key */
    KEY_F20,         /**< F20 key */
    KEY_F21,         /**< F21 key */
    KEY_F22,         /**< F22 key */
    KEY_F23,         /**< F23 key */
    KEY_F24,         /**< F24 key */

    // Platform-specific keys
    KEY_CLEAR,       /**< CLEAR key (Mac) */
    KEY_PLAY,        /**< PLAY key (Nokia/Ericsson definitions) */
    KEY_ZOOM,        /**< ZOOM key (Nokia/Ericsson definitions) */

    // Archaic keys
    KEY_CANCEL,      /**< CTRL+BREAK  (e.g. Ctrl-C) processing */
    KEY_EXEC,        /**< EXECUTE key */
    KEY_HELP,        /**< HELP key */
    KEY_PRINT,       /**< PRINT key */
    KEY_SELECT,      /**< SELECT key */

    KEY_UNKNOWN      /**< Any unsupported key */
  };

  /** @} */

} // namespace a3m

#endif
