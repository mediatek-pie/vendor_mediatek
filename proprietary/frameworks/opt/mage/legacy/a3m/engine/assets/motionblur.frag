/**************************************************************************
 *
 * Copyright (c) 2013 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * A fragment shader for motion blur effect
 *
 */

precision mediump float;

/* Material uniforms */
uniform sampler2D u_m_sharp;
uniform sampler2D u_m_velocity;

// Value of VELOCITY_SCALE should be 1/(NUM_SAMPLES*GEN_VELOCITY_SCALE)
// Value of COLOUR_MULT should be 1/NUM_SAMPLES
// NUM_SAMPLES is 4 for low quality and 8 for high quality
// GEN_VELOCITY_SCALE is defined in gen_velocity.vert

#ifdef HQMB
#define VELOCITY_SCALE 0.03125
#define COLOUR_MULT 0.125
#else
#define VELOCITY_SCALE 0.0625
#define COLOUR_MULT 0.25
#endif

// Value below which no blurring takes place (1/128.0). This value is chosen
// as being the smallest velocity which can be encoded in a signed byte.
#define VELOCITY_MIN 0.0078125

varying vec2 v_texcoord;

void main()
{
    vec4 col = texture2D( u_m_sharp, v_texcoord );
    vec2 velocity = ( texture2D( u_m_velocity, v_texcoord ).xy - 0.5 );
    vec2 abs_velocity = abs( velocity );
    if( ( abs_velocity.x > VELOCITY_MIN ) || ( abs_velocity.y > VELOCITY_MIN ) )
    {
        col += texture2D( u_m_sharp, v_texcoord + VELOCITY_SCALE * velocity );
        col += texture2D( u_m_sharp, v_texcoord + ( VELOCITY_SCALE * 2.0 ) * velocity );
        col += texture2D( u_m_sharp, v_texcoord + ( VELOCITY_SCALE * 3.0 ) * velocity );
#ifdef HQMB
        col += texture2D( u_m_sharp, v_texcoord + ( VELOCITY_SCALE * 4.0 ) * velocity );
        col += texture2D( u_m_sharp, v_texcoord + ( VELOCITY_SCALE * 5.0 ) * velocity );
        col += texture2D( u_m_sharp, v_texcoord + ( VELOCITY_SCALE * 6.0 ) * velocity );
        col += texture2D( u_m_sharp, v_texcoord + ( VELOCITY_SCALE * 7.0 ) * velocity );
#endif
        gl_FragColor = COLOUR_MULT * col;
    }
    else
    {
        gl_FragColor = col;
    }
}
