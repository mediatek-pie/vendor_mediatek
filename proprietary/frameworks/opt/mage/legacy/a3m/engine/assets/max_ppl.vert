/**************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * Vertex shader used for per pixel lighting of glofile contents exported
 * from 3ds Max.
 */

/*
 * Calulates tangent-space to world-space transformation matrix
 */

precision mediump float;

/* This should match the maximum values in the renderer */
#define MAX_JOINTS 20


/* Transformation uniforms */
uniform highp mat4 u_t_model;               // Model to world space transform
uniform highp mat4 u_t_viewProjection;      // World to projection space transform
uniform highp mat4 u_t_modelViewProjection; // Model to projection space transform
uniform highp mat3 u_t_normal;              // Model to world space normal transform
uniform vec4 u_l_cameraPosition;        // Camera position in world space

/* Joint uniforms */
uniform int u_j_count;                      // Number of joints per vertex
uniform highp mat4 u_j_world[MAX_JOINTS];   // Joint world space transforms


/* Vertex attributes */
attribute vec4 a_position;    // Position in model space
attribute vec3 a_normal;      // Normal in model space
attribute vec4 a_tangent;     // Tangent in model space. a_tangent.w contains 1
                              // for right-handed tangent space and -1 for left
                              // handed tangent space.
attribute vec2 a_uv0;         // Texture coordinate

attribute float a_jointIndex0;   // Index of attached joint
attribute float a_jointWeight0;  // Attachment weight of joint
attribute float a_jointIndex1;   // Index of attached joint
attribute float a_jointWeight1;  // Attachment weight of joint
attribute float a_jointIndex2;   // Index of attached joint
attribute float a_jointWeight2;  // Attachment weight of joint
attribute float a_jointIndex3;   // Index of attached joint
attribute float a_jointWeight3;  // Attachment weight of joint

// Texture coordinate copied from vertex attribute
varying lowp vec2 v_texCoord;

#ifdef BUMP
// Tangent to world space transform.
varying lowp mat3 v_tanToWorld;
#else
// Normal in world space
varying lowp vec3 v_normal;
#endif

// Vertex position in world space for lighting calulations
varying vec3 v_vertexPosition;

// Vector from vertex to camera in world space
varying vec3 v_vertexToCamera;

void main()
{
  vec3 normal;
  vec3 tangent;

  /* The skinning calculations performed here are duplicated in
   * max_pvl.vert, max_ppl.vert and gen_velocity.vert
   * Any changes made to one file should be made to all files.
   */
#ifdef SKIN
  // Perform a weighted sum of each joint transformation
  if (u_j_count > 0)
  {
    highp mat4 worldTransform = a_jointWeight0 * u_j_world[int(a_jointIndex0)];

    if (u_j_count > 1 && a_jointWeight1 > 0.0)
    {
      worldTransform += a_jointWeight1 * u_j_world[int(a_jointIndex1)];

      if (u_j_count > 2 && a_jointWeight2 > 0.0)
      {
        worldTransform += a_jointWeight2 * u_j_world[int(a_jointIndex2)];

        if (u_j_count > 3 && a_jointWeight3 > 0.0)
        {
          worldTransform += a_jointWeight3 * u_j_world[int(a_jointIndex3)];
        }
      }
    }

    // Apply skinning transformation to vertex data
    // Calculate vertex position in world space for lighting calulations
    vec4 vertPos = worldTransform * a_position;
    v_vertexPosition = vertPos.xyz;
    gl_Position = u_t_viewProjection * vertPos;
    normal = normalize((worldTransform * vec4(a_normal, 0.0)).xyz);
    tangent = normalize((worldTransform * vec4(a_tangent.xyz, 0.0)).xyz);
  }
  else
#endif
  {
    // Final position is model position multiplied by model view projection matrix
    gl_Position = u_t_modelViewProjection * a_position;

    // Convert tangent and normal to world space and normalize
    normal = normalize( u_t_normal * a_normal );
    tangent = normalize( u_t_normal * a_tangent.xyz );

    // Calculate vertex position in world space for lighting calulations
    v_vertexPosition = (u_t_model * a_position).xyz;
  }

  // Pass through texture coordinate
  v_texCoord = a_uv0;

#ifdef BUMP
  v_tanToWorld[2] = normal;
  v_tanToWorld[0] = tangent;

  float norm_det = dot( cross( u_t_normal[0], u_t_normal[1] ), u_t_normal[2] );

  // Calculate bitangent from normal and tangent.
  // Use a_tangent.w to construct left or right handed space transform
  v_tanToWorld[1] =
    normalize( cross( v_tanToWorld[2], v_tanToWorld[0] ) * a_tangent.w * norm_det );

  // Ensure tangent space orthonormal (again using a_tangent.w for handedness)
  v_tanToWorld[0] =
    normalize( cross( v_tanToWorld[1], v_tanToWorld[2] ) * a_tangent.w * norm_det );
#else
  // Convert normal to world space and normalize
  v_normal = normal;
#endif // BUMP


  // Calculate vector from vertex to camera in world space
  v_vertexToCamera = u_l_cameraPosition.xyz - v_vertexPosition;

}
