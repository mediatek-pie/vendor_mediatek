/**************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * Legacy fallback shader for Glo files and built-in primitives.
 * This shader will eventually be removed from A3M entirely, as it only exists
 * to maintain backwards compatibility with applications using very old Glo
 * files and the built-in primitives.
 *
 */

precision mediump float;

/* Material uniforms */
uniform sampler2D u_m_diffuseTexture;
uniform vec4 u_m_diffuseColour;
uniform vec4 u_m_ambientColour;
uniform vec4 u_m_emissiveColour;

varying vec2 v_texcoord;
varying vec3 v_normal;

void main()
{
  // Performs basic per-pixel diffuse lighting using a single fixed light.  The
  // light is not a tranditional directional light, but instead lights the
  // object based on how far from pointing directly up each face is (i.e.
  // upwards-facing faces are fully lit, sideways-facing faces are half lit,
  // and downwards-facing facing are completely unlit.
  vec3 normal = normalize( v_normal );
  float diffuse = dot( normal, vec3( 0.0, 1.0, 0.0 ) ) * 0.5 + 0.5;

  vec4 texture = texture2D( u_m_diffuseTexture, v_texcoord );

  // Emissive and ambient light is also added, but no specular.
  gl_FragColor = u_m_emissiveColour + ( u_m_ambientColour + diffuse * u_m_diffuseColour) * texture;
  gl_FragColor.a = u_m_diffuseColour.a * texture.a;
}
