/**************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * Legacy fallback shader for Glo files and built-in primitives.
 * This shader will eventually be removed from A3M entirely, as it only exists
 * to maintain backwards compatibility with applications using very old Glo
 * files and the built-in primitives.
 *
 */

/* Transformation uniforms */
uniform mat4 u_t_modelViewProjection;
uniform mat3 u_t_normal;

attribute vec4 a_position;
attribute vec3 a_normal;
attribute vec2 a_uv0;

varying vec2 v_texcoord;
varying vec3 v_normal;

void main()
{
  // Pass on minimal information required for per-pixel diffuse lighting.
  gl_Position = u_t_modelViewProjection * a_position;
  v_texcoord = a_uv0;
  v_normal = u_t_normal * a_normal;
}
