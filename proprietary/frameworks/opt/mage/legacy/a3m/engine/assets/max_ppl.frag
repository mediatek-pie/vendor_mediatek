/**************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * Fragment shader used for per pixel lighting of glofile contents exported
 * from 3ds Max.
 */

precision mediump float;

/* This should match the maximumum number of lights in the renderer */
#define MAX_LIGHTS 5

/* This is just below 1/256, which would be zero when quantized to an 8-bit
 * value. */
#define FOG_OPAQUE_EPSILON 0.0039

/* Material uniforms */
uniform sampler2D u_m_diffuseTexture;    // Diffuse texture including alpha
uniform sampler2D u_m_specularTexture;   // Specular texture (alpha ignored)
uniform sampler2D u_m_ambientTexture;    // Ambient texture (alpha ignored)
uniform sampler2D u_m_emissiveTexture;   // Emissive texture (alpha ignored)

#if defined( CUBE )
uniform samplerCube u_m_reflectionTexture; // Cube-mapped environment texture
#else
uniform sampler2D u_m_reflectionTexture; // Sphere-mapped environment texture
                                         // (alpha ignored)
#endif

uniform sampler2D u_m_bumpmapTexture;    // Bumpmap (normal) texture

/* Material uniforms */
uniform vec4 u_m_ambientColour;   // Ambient colour (alpha ignored)
uniform vec4 u_m_diffuseColour;   // Diffuse colour with alpha.
uniform vec4 u_m_specularColour;  // Specular colour (alpha ignored)
uniform vec4 u_m_emissiveColour;  // Emissive colour (alpha ignored)
uniform float u_m_shininess;      // Exponent used in specular calculation
uniform float u_m_specularLevel;  // Specular colour scale factor
uniform float u_m_selfIllumination; // Emissive colour scale factor
uniform float u_m_opacity;        // Opacity value

/* Lighting uniforms */
uniform vec4 u_l_cameraPosition;        // Camera position in world space
uniform int u_l_count;                  // Number of lights
uniform vec4 u_l_position[MAX_LIGHTS];  // Light position in world space
uniform vec4 u_l_ambient[MAX_LIGHTS];   // Ambient colour of light
uniform vec4 u_l_diffuse[MAX_LIGHTS];   // Diffuse colour of light
uniform vec4 u_l_specular[MAX_LIGHTS];  // Specular colour of light
uniform float u_l_spot_inner_dot[MAX_LIGHTS]; // cos of inner spot half-angle
uniform float u_l_spot_outer_dot[MAX_LIGHTS]; // cos of outer spot half-angle
uniform vec3 u_l_spot_direction[MAX_LIGHTS]; // direction of spot
uniform float u_l_attenuation_near[MAX_LIGHTS];  // Distance before light attenuates

// 1 / ( attFar - attNear ) measured in world space units
uniform float u_l_attenuation_reciprocal[MAX_LIGHTS];

/* Scene-wide uniforms */
uniform vec4 u_fogColour;
uniform float u_fogDensity;

// Do not calculate the ambient colour if Bump, Spec and Reflection is being
// used. This reduces the total shader cycle count.  Most issues with the 1.8
// DDK compiler/drivers have been avoided with a lower cycle count.
#if defined(BUMP) && defined(SPEC) && defined(REFL) && defined(AMBI)
#undef AMBI
#endif

/* Varyings */
// Texture coordinate copied from vertex attribute
varying lowp vec2 v_texCoord;


#ifdef BUMP
// Tangent to world space transform.
varying lowp mat3 v_tanToWorld;
#else
// Normal in world space
varying lowp vec3 v_normal;
#endif

// Vertex position in world space for lighting calulations
varying vec3 v_vertexPosition;

// Vector from vertex to camera in world space
varying vec3 v_vertexToCamera;

struct ColourComps
{
  lowp vec4 diffuseColour;
  lowp vec4 specularColour;
  lowp vec4 ambientColour;
};

ColourComps initColourComps = ColourComps(
  // Initialize colour components before accumulation for each light
  vec4( 0., 0., 0., 1. ),
  vec4( 0., 0., 0., 1. ),
  vec4( 0., 0., 0., 1. )
);

ColourComps lightLoop(int i, vec3 normal, ColourComps accumLight, vec3 vertexToCameraNorm)
{
    float lightLevel; // Result of attenuation due to distance and/or spot

    // Calculate vector from vertex to light in world space
    vec3 vertexToLight = u_l_position[i].xyz;

    // This assignment is to work around a possible compiler bug: testing the
    // value of u_l_position[i].w directly in the condition below results in
    // the condition never being met and u_l_position being treated as the
    // direction of the light.
    float w = u_l_position[i].w;

    if( w != 0.0 ) // Point or spot light
    {
      vertexToLight -= v_vertexPosition;
      // Calculate attenuation due to distance from light
      // This approximates to that used 3ds Max
      float distance = length( vertexToLight );
      float distance_x_r = (distance - u_l_attenuation_near[i]) *
                           u_l_attenuation_reciprocal[i];
#ifdef INVSQROOT // for 3ds Max
      lightLevel = ( distance_x_r > 1.0 ) ?
        0.0 : sqrt( 1.0 - distance_x_r );
#else // Generic and works for Blender
      lightLevel = ( distance_x_r > 1.0 ) ?
        0.0 : ( 1.0 - distance_x_r );
#endif
      vertexToLight = ( 1.0 / distance ) * vertexToLight;
      lightLevel = min( lightLevel, 1.0 );
    }
    else // Directional light (vertexToLight is constant)
    {
      lightLevel = 1.0;
    }

    // If lightLevel is positive then this light contributes
    if( lightLevel > 0.0 )
    {
      // Calculate spot light fall-off
      if( u_l_spot_inner_dot[i] > -1.0 )
      {
        float dir_dot = dot( vertexToLight, u_l_spot_direction[i] );
        lightLevel *= smoothstep( u_l_spot_outer_dot[i],
                                  u_l_spot_inner_dot[i],
                                  dir_dot );
      }

      // Calculate diffuse lighting if the angle between the surface normal and
      // light ray is less than 90 degrees
      float normalDotLight = dot(normal, vertexToLight );
      if( normalDotLight > 0.0 )
      {
        accumLight.diffuseColour += ( normalDotLight * lightLevel )
                                      * u_l_diffuse[i];
      }

      // Calculate specular lighting using Blinn model.
      // Specular intensity peaks where the normal lies exactly halfway between
      // the light source and the eye

      // NOTE: normalize will produce undefined results for a zero-length
      // vector. However, the probability of this happening for a pixel is very
      // low and the likely artefact (a single pixel incorrectly coloured) is
      // so small that the cost of extra instructions to avoid it is not
      // justified.
      vec3 halfVector = normalize( vertexToCameraNorm + vertexToLight );
      float normalDotHalf = dot( normal, halfVector );
      if( normalDotHalf > 0.0 )
      {
        accumLight.specularColour += pow( normalDotHalf, u_m_shininess )
                                          * lightLevel
                                          * u_l_specular[i];
      }

      // Simply add ambient component
      accumLight.ambientColour += lightLevel * u_l_ambient[i];
    }
   return accumLight;
  }


void main()
{
  float fogTransparency = 1.0;

  // Calculate fog density up front, as we can avoid lighting calculations if
  // the fog is completely opaque.
  if (u_fogDensity > 0.0)
  {
    // Calculate per-pixel fog
    float fogDepth = length(v_vertexToCamera);

    // Emulates the old "exp2" fixed-pipeline fog.
    fogTransparency = exp(-u_fogDensity * u_fogDensity * fogDepth * fogDepth);
    fogTransparency = clamp(fogTransparency, 0.0, 1.0);

    // Return early if fog is completely opaque.
    if (fogTransparency < FOG_OPAQUE_EPSILON)
    {
      gl_FragColor = u_fogColour;
      return;
    }
  }

  // Find normal in tangent space
#ifdef BUMP
  lowp vec3 bumpNormal = texture2D( u_m_bumpmapTexture, v_texCoord ).xyz * 2.0 - 1.0;
#ifdef INVY
  bumpNormal.y = - bumpNormal.y;
#endif
  // Convert normal to world space
  lowp vec3 normal = normalize( v_tanToWorld * bumpNormal );
#else
  lowp vec3 normal = normalize( v_normal );
#endif

  // Lighting calculations need a normalized vertex-to-camera vector.
  vec3 vertexToCameraNorm = normalize( v_vertexToCamera );

  ColourComps accumLight = initColourComps;

  // Certain GPU's (Tegra3/Adreno) cannot handle large conditional loops
  // so we have to unroll the per light calculations.
  // This is also a limitation in the 1.8 DDK for PVR.
  if ( u_l_count > 0 )
  {
    accumLight = lightLoop(0, normal, accumLight, vertexToCameraNorm);
  }
  if ( u_l_count > 1 )
  {
    accumLight = lightLoop(1, normal, accumLight, vertexToCameraNorm);
  }

#if !defined(BUMP) && !defined(REFL)
  // There is a performance limit with the platform that restricts us to
  // using only two lights when doing intensive effects like bump and
  // reflection.  This requires us to be careful in the renderer or
  // artwork to make sure the primary lights are ordered first.
  if ( u_l_count > 2 )
  {
    accumLight = lightLoop(2, normal, accumLight, vertexToCameraNorm);
  }
  if ( u_l_count > 3 )
  {
    accumLight = lightLoop(3, normal, accumLight, vertexToCameraNorm);
  }
#endif

  // Multiply colour components already calculated by their corresponding
  // material colours.  Diffuse lighting flatness is determined by the self-
  // illumination value.
  accumLight.diffuseColour = u_m_diffuseColour * ( u_m_selfIllumination +
      ( 1.0 - u_m_selfIllumination ) * accumLight.diffuseColour );
  accumLight.specularColour *= u_m_specularColour * u_m_specularLevel;
  accumLight.ambientColour *= u_m_ambientColour;

  // Use the diffuse colour alpha and opacity as the 'overall' alpha.
  accumLight.diffuseColour.a = u_m_diffuseColour.a * u_m_opacity;

  /* Calculate sphere map environment reflection. */

  // Calculate reflection vector (view vector reflected off the surface) in
  // world space
  vec3 reflectCamera = -reflect( vertexToCameraNorm, normal );

  // Cube map just needs un-normalized reflection vector.
  // We flip the z-axis, as cube map look-up is left-handed when we are viewing
  // as an observer inside the cube.
  vec3 reflectWorld = reflectCamera;
  reflectWorld.z = -reflectWorld.z;

  // Calculate sphere map texture coordinates. This is done here to gain
  // performance at the cost of quality.

  // Calculate   m = 2 * sqrt( r.x^2 + r.y^2 + (r.z + 1)^2)
  // See http://www.reindelsoftware.com/Documents/Mapping/Mapping.html

  //vec3 reflectCamera = (u_t_view * vec4( reflectWorld, 0.0 )).xyz;

  reflectCamera.z += 1.0;
  float m = 2.0 * length( reflectCamera );

  vec2 sphereCoord = vec2( reflectCamera.x / m + 0.5,
                     1.0 - ( reflectCamera.y / m + 0.5 ) );

  // Look up value from diffuse texture map
  lowp vec4 diffuseTex = texture2D( u_m_diffuseTexture, v_texCoord );

  // Look up value from specular texture map
  lowp vec4 specularTex = texture2D( u_m_specularTexture, v_texCoord );

  // Multiply by diffuse colour
  lowp vec4 diffuse = diffuseTex * accumLight.diffuseColour;

  // Colour calculated from sum of:
  // diffuse colour as calculated above,
  // specular colour (specular texture X specular lighting component),
  // ambient colour (ambient texture X ambient lighting X diffuse texture),
  // emmisive colour (emissive texture X emissive colour),
  // reflection (specular texture X environment map texture look-up)
  vec4 colour = vec4( 0., 0., 0., 0. )
#if defined( DIFF )
    + diffuse
#endif
#if defined( SPEC )
    + specularTex * accumLight.specularColour
#endif
#if defined( AMBI )
    + texture2D( u_m_ambientTexture, v_texCoord )
                 * accumLight.ambientColour * diffuseTex
#endif
#if defined( EMIS )
    + texture2D( u_m_emissiveTexture, v_texCoord ) * u_m_emissiveColour
#endif
#if defined( REFL )
    + specularTex
#if defined( CUBE )
    * textureCube( u_m_reflectionTexture, reflectWorld )
#else
    * texture2D( u_m_reflectionTexture, sphereCoord )
#endif // defined( CUBE )

#endif // defined( REFL )
  ;

  // Use diffuse component alpha
  colour.a = diffuse.a;

  // If there is any fog for this pixel, blend fog with rest of scene.
  if (fogTransparency < 1.0)
  {
    colour.rgb = mix(u_fogColour.rgb, colour.rgb, fogTransparency);
  }

  gl_FragColor = colour;
}
