/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * Scene Node Utility Functions
 *
 */
#pragma once
#ifndef A3M_SCENEUTILITY_H
#define A3M_SCENEUTILITY_H

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/pointer.h>  /* for SharedPtr */
#include <a3m/solid.h>  /* for Solid */
#include <a3m/vector2.h>  /* for Vector2f */

namespace a3m
{
  /** \defgroup a3mSceneutility Scene Utility
   * \ingroup  a3mScenenodes
   *
   * Utility functions for performing commonly required scene operations.
   *
   * @{
   */

  // Forward declarations
  class Appearance;
  class AssetCachePool;
  class SceneNode;
  class ShaderProgram;

  /** Set Shader Program for Node.
   * Sets the shader program for all Solid objects belonging to the supplied
   * SceneNode recursively.
   */
  void setShaderProgram( SceneNode& node, /**< Scene node to change */
                         SharedPtr< ShaderProgram > const& program
                         /**< ShaderProgram to use for all descendent Solids */
                       );

  /** Set Appearance for Node.
   * Sets the shader program for all Solid objects belonging to the supplied
   * SceneNode recursively.
   */
  void setAppearance( SceneNode& node, /**< Scene node to change */
                      SharedPtr< Appearance > const& appearance
                      /**< Appearance to use for all descendent Solids */
                    );

  /**
   * Points a scene node along a world-space vector.
   */
  void point(
    /**  */
    SceneNode& node,
    /** World-space forward-vector with which to align node */
    Vector3f const& worldForward,
    /** World-space up-vector with which to align node */
    Vector3f const& worldUp = Vector3f::Y_AXIS,
    /** Local forward-vector to point at world-space target */
    Vector3f const& localForward = -Vector3f::Z_AXIS,
    /** Local up-vector to align with world-space up-vector */
    Vector3f const& localUp = Vector3f::Y_AXIS);

  /**
   * Creates a unit square solid.
   * \copydetails createSquareMesh()
   *
   * \return Square solid
   */
  Solid::Ptr createSquare(
    AssetCachePool& assetCachePool,
    /**< Asset pool used to manage assets and resources */
    Vector2f const& uvScale
    /**< Scale factor applied to the UV coordinates */
    = Vector2f(1.0f, 1.0f));

  /**
   * Creates a unit cube solid.
   * \copydetails createCubeMesh()
   *
   * \return Cube solid
   */
  Solid::Ptr createCube(
    AssetCachePool& assetCachePool,
    /**< Asset pool used to manage assets and resources */
    Vector2f const& uvScale
    /**< Scale factor applied to the UV coordinates */
    = Vector2f(1.0f, 1.0f));

  /**
   * Creates a unit sphere solid (radius == 0.5).
   * \copydetails createSphereMesh()
   *
   * \return Sphere solid
   */
  Solid::Ptr createSphere(
    AssetCachePool& assetCachePool,
    /**< Asset pool used to manage assets and resources */
    A3M_UINT32 segmentCount,
    /**< The number of spherical segments (latitudinal)
     * The minimum number of segments is 2 */
    A3M_UINT32 wedgeCount,
    /**< The number of spherical wedges (longitudinal)
     * The minimum number of wedges is 2 */
    Vector2f const& uvScale
    /**< Scale factor applied to the UV coordinates */
    = Vector2f(1.0f, 1.0f));

  /**
   * Visits a node and all of its children recursively.
   */
  void visitScene(SceneNodeVisitor& visitor, /**< Visitor to use */
                  SceneNode& node /**< First node to visit */);

  /**
   * Conditionally visits a node and all of its children recursively.
   * The condition for visiting any node is set by the filter flags.  All flags
   * passed to the function must be set in a node for the visit to occur.  A
   * particular flag can recursively affect child nodes if it is set in the
   * recursive flags mask.  Flags only recurse if they are set to a value other
   * than their default (which may be TRUE or FALSE).
   */
  void visitScene(SceneNodeVisitor& visitor, /**< Visitor to use */
                  SceneNode& node, /**< First node to visit */
                  FlagMask const& filterFlags, /**< Flags required for visit */
                  FlagMask const& recursiveFlags /**< Flags treated as
                                                      recursive */);

  /**
   * Sets the state of one or more flags for this node.
   */
  void setFlags(
    SceneNode& node, /**< Scene node whose flags to set */
    FlagMask const& mask, /**< Mask defining which flags to set */
    A3M_BOOL state /**< State to which the flags will be set */ );

  /** Returns the state of one or more flags for this node.
   * The state of several flags may be checked at once by providing a mask with
   * multiple set bits (TRUE will be returned only if all of the flags are
   * set).
   * \return TRUE if all the specified flags are set.
   */
  inline A3M_BOOL getFlags(
    SceneNode const& node, /**< Scene node whose flags to check */
    FlagMask const& mask /**< Mask defining which flags to check */ )
  {
    return node.getFlags().get( mask );
  }

  /** Checks the derived state of one or more flags for this node.
   * Derived flags take into account the state of the flags of parents of the
   * scene node (not just the direct parent, but also their parents, and so
   * on).  A flag state is only inherited from a parent when it is set to the
   * non-default state (e.g. a flag called VISIBLE whose default state is TRUE
   * will only be inherited if it is set to FALSE; conversely, a flag called
   * HIDDEN whose default state is FALSE will only be inherited when it is set
   * to TRUE).
   *
   * \return TRUE if all the specified derived flags are set.
   */
  A3M_BOOL getDerivedFlags(
    SceneNode const& node, /**< Scene node whose flags to check */
    FlagMask const& mask /**< Mask defining which flags to check */ );

  /** @} */

} /* namespace a3m */

#endif /* A3M_SCENEUTILITY_H */
