/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * Light Implementation.
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/light.h>            /* Light() and member functions */
#include <a3m/fpmaths.h>          /* for abs() */
#include <a3m/scenenodevisitor.h> /* For SceneNodeVisitor interface */

/*****************************************************************************
 * A3M Namespace
 *****************************************************************************/
namespace a3m
{
  // Instantiate the type ID for this class (please ensure this is unique!)
  A3M_UINT32 const Light::NODE_TYPE = 'L';

  /*
   * Constructor
   */
  Light::Light() :
    m_active( A3M_TRUE ),
    m_castsShadows( A3M_FALSE ),
    m_lightType( LIGHTTYPE_OMNI ),
    m_intensity( 1.0f ),
    m_attenuation_near( 1.0f ),
    m_attenuation_far( 1000.0f ),
    m_spotInnerAngle( Anglef::DEGREES, 30.0f ),
    m_spotOuterAngle( Anglef::DEGREES, 45.0f ),
    m_colour( Colour4f::WHITE ),
    m_ambientLevel( 0.0f ),
    m_isAttenuated( A3M_FALSE ),
    m_lightName( "" )
  {
  }

  /*
   * Sets the Light's name.
   */
  void Light::setLightName( A3M_CHAR8 const* name )
  {
    m_lightName = name;
  }

  /*
   * Activate or deactivate the light.
   */
  void Light::setActive( A3M_BOOL active )
  {
    m_active = active;
  }

  /*
   * Sets light type parameter as one of the supported in enum LightType.
   * Default light type is "Directional" (LIGHTTYPE_DIRECTIONAL)
   */
  void Light::setLightType( LightType lightType )
  {
    m_lightType = lightType;
  }

  /*
   * Sets diffuse colour for a light.<BR>
   * Here, colour values (r,g,b,a) are passed in colour4f struct format.
   */
  void Light::setColour( const Colour4f& colour )
  {
    m_colour = colour;
  }

  /*
   * Sets the ambient light level.
   */
  void Light::setAmbientLevel( A3M_FLOAT level )
  {
    m_ambientLevel = level;
  }

  /*
   * Sets intesity of a light.
   * Intensity is the multiplication factor to light colour and can be used to
   * dim the light.
   */
  void Light::setIntensity( A3M_FLOAT intensity )
  {
    m_intensity = intensity;
  }

  /*
   * Set distance at which light intensity begins to attenuate.
   */
  void Light::setAttenuationNear( A3M_FLOAT attNear )
  {
    m_attenuation_near = abs( attNear );
  }

  /*
   * Set distance at which light intensity falls to zero.
   */
  void Light::setAttenuationFar( A3M_FLOAT attFar )
  {
    m_attenuation_far = abs( attFar );
  }

  /*
   * Sets inner angle for inner region of a spot light.
   */
  void Light::setSpotInnerAngle( Anglef const& spotInnerAngle )
  {
    m_spotInnerAngle = abs( spotInnerAngle );
  }

  /*
   * Sets full angle including inner and outer region of a spot light.
   */
  void Light::setSpotOuterAngle( Anglef const& spotOuterAngle )
  {
    m_spotOuterAngle = abs( spotOuterAngle );
  }

  /*
   * Accept a visitor.
   */
  void Light::accept( SceneNodeVisitor& visitor )
  {
    visitor.visit( this );
  }

} /* namespace a3m */
