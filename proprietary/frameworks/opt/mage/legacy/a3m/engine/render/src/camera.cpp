/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/** \file
 * A3M Camera Implementation.
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/camera.h>           /* Camera API */
#include <a3m/log.h>              /* A3M log API */
#include <a3m/vector3.h>          /* class Vector3f */
#include <a3m/scenenodevisitor.h> /* For SceneNodeVisitor interface */

namespace
{

  using namespace a3m;

  /*
   * Generates an orthographic projection matrix.
   */
  void getOrthographicProjection(
    Matrix4f* projection,
    A3M_FLOAT aspect,
    A3M_FLOAT width,
    A3M_FLOAT zNear,
    A3M_FLOAT zFar)
  {
    A3M_FLOAT height = width / aspect;
    A3M_FLOAT depth = zFar - zNear;

    *projection =
      Matrix4f(
        Vector4f( 2.0f / width, 0.0f,           0.0f,                   0.0f ),
        Vector4f( 0.0f,         2.0f / height,  0.0f,                   0.0f ),
        Vector4f( 0.0f,         0.0f,          -2.0f / depth,           0.0f ),
        Vector4f( 0.0f,         0.0f,          -(zFar + zNear) / depth, 1.0f ) );
  }

  /*
   * Generates a perspective projection matrix.
   */
  void getPerspectiveProjection(
    Matrix4f* projection,
    A3M_FLOAT aspect,
    Anglef const& fov,
    A3M_FLOAT zNear,
    A3M_FLOAT zFar)
  {
    A3M_FLOAT f = 1.0f / tan( fov / 2.0f );
    A3M_FLOAT fx, fy;

    if( abs(aspect) > 1.0f )
    {
      fx = f / aspect;
      fy = f;
    }
    else
    {
      fx = f;
      fy = f * aspect;
    }

    *projection =
      Matrix4f(
        Vector4f( fx,    0.0f, 0.0f,                                    0.0f ),
        Vector4f( 0.0f,  fy,   0.0f,                                    0.0f ),
        Vector4f( 0.0f,  0.0f, (zNear + zFar) / (zNear - zFar),        -1.0f ),
        Vector4f( 0.0f,  0.0f, (2.0f * zFar * zNear) / (zNear - zFar),  0.0f ) );
  }

} // namespace

/*****************************************************************************
 * A3M Namespace
 *****************************************************************************/
namespace a3m
{
  namespace
  {
    /** Matrix4 * Vector3 multiply.
        \return the product.
        */
    template< typename T >
    Vector3<T> operator*(
      Matrix4<T> const& a /**< a matrix4! */,
      Vector3<T> const& b /**< a vector3! */)
    {
      return Vector3<T>(a * Vector4<T>(b, T(1)));
    }
  }

  // Instantiate the type ID for this class (please ensure this is unique!)
  A3M_UINT32 const Camera::NODE_TYPE = 'C';

  /*
   * Constructor
   */
  Camera::Camera() :
    m_projectionType(PERSPECTIVE), /* type of projection               */
    m_fov(Anglef::DEGREES, 60.0f), /* set default angle as 60 degrees  */
    m_width(2.0f),                 /* default range is -1 to 1         */
    m_zNear(0.1f),                 /* near clipping plane              */
    m_zFar(100.0f),                /* far clipping plane               */

    m_stereoZFocal(50.0f),         /* focal plane as half zFar         */
    m_stereoEyeSep(0.0f)           /* eye separation of 0 disables stereo */
  {
  }

  /*
   * Destructor
   */
  Camera::~Camera()
  {
  }

  /* getStereoWorldTransform
   *
   * This is a version of getWorldTransform that returns two transformations
   * one for each eye/camera position.
   *
   * Most stereoscopic examples assume the camera is pointing along the Z axis
   * (with Y up) and thus just needs shifting in X.
   * This code implements the general case, taking into account the full camera
   * transformation.
   *
   * The camera is shifted to \e its right and left along its local X axis.
   *
   */
  void Camera::getStereoWorldTransform( Matrix4f* leftEye,
                                        Matrix4f* rightEye)
  {
    Matrix4f tm = getWorldTransform(); // transformation matrix

    // Calculate local 'right'
    Vector3f right = normalize( cross( Vector3f( tm.j ), Vector3f( tm.k ) ) );

    // The necessary shift is half the eye separation, in the local right/left
    // direction
    Vector4f eyeshift = Vector4f( right * (m_stereoEyeSep / 2.0f), 0.0f);

    // Modify the positions in the camera transform
    Matrix4f temp = tm;
    temp.t = temp.t - eyeshift;
    *leftEye = temp;

    temp =  tm;
    temp.t = temp.t + eyeshift;
    *rightEye = temp;
  }

  void Camera::getProjection(Matrix4f* projection, A3M_FLOAT aspect)
  {
    switch (m_projectionType)
    {
    case ORTHOGRAPHIC:
      getOrthographicProjection(projection, aspect, m_width, m_zNear, m_zFar);
      break;

    case PERSPECTIVE:
      /* Sanity limiter. We've had instances of customers setting Z-Near to
       * zero.  This instantly results in massive Z fighting as Far/Near
       * becomes infinity. In this case, defensively degrade using a near
       * setting so that far/near ~= 1000.
       */
      if( m_zNear == 0.f )
      {
        A3M_LOG_WARN("Near clipping distance modified to be non-zero.");
        m_zNear = m_zFar / 1000.f;
      }

      // Remove risk of div-by-zero in projection calculation
      if( m_zNear == m_zFar )
      {
        A3M_LOG_WARN("Clipping plane separation was zero.");
        // Again, use a defensive setting of 1:1000
        m_zFar = 1000.f;
        m_zNear = 1.f;
      }

      getPerspectiveProjection(projection, aspect, m_fov, m_zNear, m_zFar);
      break;
    }
  }

  /* getStereoProjection
   *
   * This is a version of getProjection that returns two separate projections
   * one for each eye/camera position. The 'classic' glFrustum version of
   * the matrix is created as this gives us the parameters we need to
   * bend the frustrum for off-axis stereoscopic projection.
   *
   * A useful reference is:
   * http://paulbourke.net/miscellaneous/stereographics/stereorender/
   *
   */
  void Camera::getStereoProjection(Matrix4f* leftEye, Matrix4f* rightEye,
                                   A3M_FLOAT aspect) const
  {
    // Orthographic projections by definiton lack any effect of depth.
    if (m_projectionType == ORTHOGRAPHIC)
    {
      getOrthographicProjection(leftEye, aspect, m_width, m_zNear, m_zFar);
      *rightEye = *leftEye;
      return;
    }

    A3M_FLOAT smallerHalf, right, left, top, bottom, width, height;
    A3M_FLOAT depth, w, h, aR, aL, c, d, e;

    smallerHalf = m_zNear * a3m::tan( m_fov / 2.0f );

    // start with conventional symmetric frustum
    if( abs(aspect) > 1.0f ) // landscape
    {
      top = smallerHalf; // For landscape FOV is vertical
      bottom = -top;
      right = top * aspect; // X > Y
      left = -right;
    }
    else // portrait
    {
      right = smallerHalf;
      left = -right;
      top = right / aspect; // Aspect <1; need Y > X
      bottom = -top;
    }

    width = right - left;
    height = top - bottom;
    depth = m_zFar - m_zNear;

    /* frustum matrix
     *  (  W  0  A  0  )
     *  (  0  H  B  0  )
     *  (  0  0  C  D  )
     *  (  0  0  -1 0  )
     */

    c = -(m_zFar + m_zNear) / depth;
    d = - 2.0f * (m_zFar * m_zNear) / depth;

    w = 2.0f * m_zNear / width;
    h = 2.0f * m_zNear / height;


    // Adapt for left/right eye positions

    // frustum shift is centre-to-eye distance, scaled according to the
    // ratio of the frustum near Z and the specified focal Z
    e = 0.5f * m_stereoEyeSep * m_zNear / m_stereoZFocal;

    // 'A' component = right+left/right-left
    // For right eye, 'right' is reduced by the shift, and
    // 'left', a negative value, is increased in magnitude
    aR = ((right - e) + (left - e)) / width;
    // For left eye, reverse sign of e
    aL = ((right + e) + (left + e)) / width;

    // 'B' = (top+bottom)/(top-bottom) but as top = -bottom, b = 0


    *leftEye = Matrix4f(
                 Vector4f( w,     0.0f, 0.0f,  0.0f ), // NB *column* 1
                 Vector4f( 0.0f,  h,    0.0f,  0.0f ),
                 Vector4f( aL,    0.0f, c,    -1.0f ),
                 Vector4f( 0.0f,  0.0f, d,     0.0f ) );

    *rightEye = Matrix4f(
                  Vector4f( w,     0.0f, 0.0f,  0.0f ),
                  Vector4f( 0.0f,  h,    0.0f,  0.0f ),
                  Vector4f( aR,    0.0f, c,    -1.0f ),
                  Vector4f( 0.0f,  0.0f, d,     0.0f ) );
  }

  /*
   * Accept a visitor.
   */
  void Camera::accept( SceneNodeVisitor& visitor )
  {
    visitor.visit( this );
  }

} /* namespace a3m */
