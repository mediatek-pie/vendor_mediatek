/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * A3M animation implementation
 */
#include <a3m/animation.h>              /* This module's header               */
#include <a3m/fpmaths.h>                /* for clamp()                        */

namespace
{

  using namespace a3m;

  A3M_BOOL startsBefore(Animation::Ptr const& lhs, Animation::Ptr const& rhs)
  {
    return lhs->getStart() < rhs->getStart();
  }

  A3M_BOOL endsBefore(Animation::Ptr const& lhs, Animation::Ptr const& rhs)
  {
    return lhs->getEnd() < rhs->getEnd();
  }

} // namespace

namespace a3m
{

  /***************************************************************************
   *                            AnimationGroup                               *
   ***************************************************************************/

  void AnimationGroup::addAnimation(Animation::Ptr const& animation)
  {
    m_animations.push_back(animation);
  }

  A3M_INT32 AnimationGroup::getAnimationCount() const
  {
    return static_cast<A3M_INT32>(m_animations.size());
  }

  Animation::Ptr AnimationGroup::getAnimation(A3M_INT32 i) const
  {
    if (i >= getAnimationCount())
    {
      A3M_LOG_ERROR("Index %d exceeds animation count %d.",
                    i, getAnimationCount());
      return Animation::Ptr();
    }

    return m_animations[i];
  }

  void AnimationGroup::apply(A3M_FLOAT progress)
  {
    for (A3M_INT32 i = 0; i < getAnimationCount(); ++i)
    {
      m_animations[i]->apply(progress);
    }
  }

  A3M_FLOAT AnimationGroup::getStart() const
  {
    if (m_animations.empty())
    {
      return 0.0f;
    }

    // Find the earliest animation in the group.
    AnimationVector::const_iterator it =
      std::min_element(m_animations.begin(), m_animations.end(), startsBefore);
    return (*it)->getStart();
  }

  A3M_FLOAT AnimationGroup::getEnd() const
  {
    if (m_animations.empty())
    {
      return 0.0f;
    }

    // Find the latest animation in the group.
    AnimationVector::const_iterator it =
      std::max_element(m_animations.begin(), m_animations.end(), endsBefore);
    return (*it)->getEnd();
  }

  /***************************************************************************
   *                         AnimationController                             *
   ***************************************************************************/

  AnimationController::AnimationController(Animation::Ptr const& animation) :
    m_enabled(A3M_TRUE),
    m_paused(A3M_FALSE),
    m_looping(A3M_TRUE),
    m_speed(1.0f),
    m_progress(0.0f),
    m_start(animation->getStart()),
    m_end(animation->getEnd()),
    m_loopStart(0.0f),
    m_loopEnd(0.0f),
    m_animation(animation)
  {
  }

  void AnimationController::update(A3M_FLOAT delta)
  {
    // If not enabled, don't update
    if (!m_enabled)
    {
      return;
    }

    // Clamp the previous progress value in case it has been set outside of the
    // animation's range.
    m_progress = clamp(m_progress, m_start, m_end);
    A3M_FLOAT newProgress = m_progress;

    // Only update the progress when not paused.
    if (!m_paused)
    {
      // Advance the progress by the appropriate amount.
      A3M_FLOAT scaledDelta = m_speed * delta;
      newProgress += scaledDelta;

      // Only take loop points into account if looping is enabled, and a loop
      // is defined.
      if (m_looping && m_loopStart < m_loopEnd)
      {
        A3M_FLOAT loopSize = m_loopEnd - m_loopStart;
        A3M_ASSERT(loopSize > 0.0f);

        // If the progress moved forwards and started before the end of the
        // loop, check to see if it has moved beyond the end of the loop.
        if (scaledDelta > 0.0f && m_progress <= m_loopEnd)
        {
          // Loop around so that the progress remains within the loop.
          while (newProgress >= m_loopEnd)
          {
            newProgress -= loopSize;
          }
        }

        // If the progress moved backwards and started after the start of the
        // loop, check to see if it has moved beyond the start of the loop.
        if (scaledDelta < 0.0f && m_progress >= m_loopStart)
        {
          // Loop around so that the progress remains within the loop.
          while (newProgress <= m_loopStart)
          {
            newProgress += loopSize;
          }
        }
      }

      // Clamp the new progress to the limits of the animation.
      newProgress = clamp(newProgress, m_start, m_end);
    }

    // Apply the animation to its target.
    m_progress = newProgress;
    m_animation->apply(m_progress);
  }

  /***************************************************************************
   *                           Free Functions                                *
   ***************************************************************************/

  void play(AnimationController& controller, A3M_BOOL update)
  {
    controller.setEnabled(A3M_TRUE);
    controller.setPaused(A3M_FALSE);

    if (isFinished(controller))
    {
      rewind(controller, update);
    }
    else if (update)
    {
      controller.update();
    }
  }

  void pause(AnimationController& controller, A3M_BOOL update)
  {
    controller.setPaused(A3M_TRUE);

    if (update)
    {
      controller.update();
    }
  }

  void stop(AnimationController& controller, A3M_BOOL update)
  {
    controller.setEnabled(A3M_TRUE);
    rewind(controller, update);
    controller.setEnabled(A3M_FALSE);
  }

  void seek(AnimationController& controller, A3M_FLOAT progress)
  {
    controller.setProgress(progress);
    controller.update();
  }

  void rewind(AnimationController& controller, A3M_BOOL update)
  {
    A3M_FLOAT speed = controller.getSpeed();

    if (speed > 0.0f)
    {
      controller.setProgress(controller.getStart());
    }
    else if (speed < 0.0f)
    {
      controller.setProgress(controller.getEnd());
    }

    if (update)
    {
      controller.update();
    }
  }

  A3M_BOOL isFinished(AnimationController const& controller)
  {
    // When inside a loop, the controller can never be finished.
    if (isInsideLoop(controller))
    {
      return A3M_FALSE;
    }

    A3M_BOOL finished = A3M_FALSE;
    A3M_FLOAT speed = controller.getSpeed();

    // Move to the start or the end, depending on whether the animation is
    // playing in reverse or not.
    if (speed > 0.0f)
    {
      if (controller.getProgress() >= controller.getEnd())
      {
        finished = A3M_TRUE;
      }
    }
    else if (speed < 0.0f)
    {
      if (controller.getProgress() <= controller.getStart())
      {
        finished = A3M_TRUE;
      }
    }

    return finished;
  }

  A3M_BOOL hasLoop(AnimationController const& controller)
  {
    return controller.getLoopStart() < controller.getLoopEnd();
  }

  A3M_BOOL isInsideLoop(AnimationController const& controller)
  {
    if (hasLoop(controller) && controller.getLooping() &&
        controller.getProgress() >= controller.getLoopStart() &&
        controller.getProgress() <= controller.getLoopEnd())
    {
      return A3M_TRUE;
    }

    return A3M_FALSE;
  }

  void setRange(
    AnimationController& controller, A3M_FLOAT start, A3M_FLOAT end)
  {
    controller.setStart(start);
    controller.setEnd(end);
  }

  void setLoopRange(
    AnimationController& controller, A3M_FLOAT start, A3M_FLOAT end)
  {
    controller.setLoopStart(start);
    controller.setLoopEnd(end);
  }

  A3M_FLOAT getLength(Animation const& animation)
  {
    return std::max(0.0f, animation.getEnd() - animation.getStart());
  }

  A3M_FLOAT getLength(AnimationController const& controller)
  {
    return std::max(0.0f, controller.getEnd() - controller.getStart());
  }

  A3M_FLOAT getLoopLength(AnimationController const& controller)
  {
    return std::max(0.0f, controller.getLoopEnd() - controller.getLoopStart());
  }

} // namespace a3m
