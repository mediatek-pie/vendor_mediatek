/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 *  Render block group class
 */
#pragma once
#ifndef A3M_RENDERBLOCKGROUP_H
#define A3M_RENDERBLOCKGROUP_H

#include <a3m/renderblockbase.h> /* for this class's base class             */
#include <vector>                /* data member m_blocks is a std::vector   */

/*****************************************************************************
 * A3M Namespace
 *****************************************************************************/
namespace a3m
{
  /** \defgroup a3mRenderBlockGroup A3M Render Block Group
   * \ingroup  a3mRefRender
   *
   * @{
   */

  /**
   * RenderBlockGroup class
   *
   * A RenderBlockGroup is a container for RenderBlocks which itself inherits
   * the RenderBlock interface. Child blocks are added and stored using
   * SharedPtrs, so do not need to be managed by the client code.
   */
  class RenderBlockGroup : public RenderBlockBase
  {
  public:
    /** Smart pointer type for this class */
    typedef SharedPtr< RenderBlockGroup > Ptr;

    /** Default constructor
     */
    RenderBlockGroup();

    /** Add block to group
     * The new block will be added at the end of the group (and so be the last
     * to be rendered).
     */
    void addBlock( SharedPtr< RenderBlockBase > const& block /**< new block */ );

    /** Remove block from group
     */
    void removeBlock( SharedPtr< RenderBlockBase > const& block
                      /**< block to remove */ );

    /** Render this block.
     */
    virtual void render();

    /** Update the time for shader based effects.
     */
    virtual void update( A3M_FLOAT timeInSeconds
                         /**< Time (mod 60) to set in uniform u_time */ );

    /** Set stereoscopic parameters for camera
     */
    virtual void setStereo(
      A3M_FLOAT zFocal /**< distance camera to focal plane in world units */,
      A3M_FLOAT eyeSep /**< distance between camera positions in world units */
    );

  private:
    std::vector< SharedPtr< RenderBlockBase > > m_blocks;
    A3M_FLOAT m_zFocal;
    A3M_FLOAT m_eyeSep;
  };
  /** @} */

} /* namespace a3m */

#endif /* A3M_RENDERBLOCKGROUP_H */
