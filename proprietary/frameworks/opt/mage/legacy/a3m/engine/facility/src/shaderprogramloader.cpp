/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * ShaderProgramLoader Implementation
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/log.h>                    /* for logging                     */
#include <a3m/detail/assetpath.h>       /* AssetPath                       */
#include <a3m/detail/digestmaker.h>     /* Compare cached version          */
#include <fileutility.h>                /* FileToString etc.               */
#include <stdstringutility.h>           /* for endsWithI()                 */
#include <shaderprogramloader.h>        /* This class's API                */
#include <map>                          /* std::map                        */
#include <string>                       /* for std::string::substr()       */

namespace
{
  typedef std::map<std::string, std::string> StringMap;

  const A3M_INT32 CURRENT_MAJOR_VERSION = 1;
  const A3M_INT32 CURRENT_MINOR_VERSION = 0;
  const A3M_INT32 CURRENT_CACHE_VERSION = 1;

  A3M_CHAR8 const* ERROR_SHADER_NAME = "a3m#error.sp";

  using namespace a3m;

  /*
   * Load a pre-compiled shader from a source
   */
  ShaderProgram::Ptr loadBinary( ShaderProgramCache& cache,
                                 Stream::Ptr& stream,
                                 A3M_CHAR8 const* name )
  {
    ShaderProgram::Ptr shaderProgram;

    A3M_INT32 version;
    if( stream->read( &version, sizeof( version ) ) != sizeof( version ) )
    {
      // Shader may not be in the cache
      A3M_LOG_WARN( "Failed to read -cached- shader \"%s\"", name );
      return shaderProgram;
    }

    if( version != CURRENT_CACHE_VERSION )
    {
      A3M_LOG_ERROR( "Unrecognized cached shader version\"%s\"", name );
      return shaderProgram;
    }

    A3M_INT32 format;
    if( stream->read( &format, sizeof( format ) ) != sizeof( format ) )
    {
      A3M_LOG_ERROR( "Failed to load cached shader \"%s\"", name );
      return shaderProgram;
    }

    FileToString file( *stream );
    shaderProgram = cache.create( file.length(), format, file.get() );

    return shaderProgram;
  }


  /*
   * Split name into a filename (any characters before the first '$' character)
   * and a set of "#define" statements (one for each '$' delimited string)
   *
   * e.g. if (input) name == "simple.sp$DIFF$SPEC"
   *      then (output) filename = "simple.sp"
   *      and  (output) predefines = "#define DIFF\n#define SPEC\n"
   */
  void parseName( std::string const& name,
                  std::string& filename,
                  std::string& predefines )
  {
    size_t pos = name.find_first_of( '$', 0 );
    filename = name.substr( 0, pos );

    while( pos != std::string::npos )
    {
      // pos is index of '$' character
      size_t next_pos = name.find_first_of( '$', pos + 1 );
      predefines += "#define " + name.substr( pos + 1, next_pos - pos - 1 ) + '\n';
      pos = next_pos;
    }
  }

  /*
   * Parse a shader program file which has been read into a CharRange object
   * if the function reports success then vertexSourceFile and
   * fragmentSourceFile will contain the names of the vertex and fragment shader
   * source files.
   * returns A3M_TRUE for success.
   */
  A3M_BOOL parseShaderFile( A3M_CHAR8 const* name, CharRange range,
                            CharRange& vertexSourceFile,
                            CharRange& fragmentSourceFile,
                            StringMap& uniforms )
  {
    if( range.empty() )
    {
      A3M_LOG_ERROR( "ShaderProgram file: %s empty", name );
      return A3M_FALSE;
    }

    /* Check version number (major and minor) */
    A3M_INT32 major = readInt( range, -1 );
    if( major != CURRENT_MAJOR_VERSION )
    {
      A3M_LOG_ERROR( "ShaderProgram file: %s wrong version", name );
      return A3M_FALSE;
    }

    if( !requireChar( range, '.' ) )
    {
      A3M_LOG_ERROR( "ShaderProgram file: %s expected '.'", name );
      return A3M_FALSE;
    }
    A3M_INT32 minor = readInt( range, -1 );

    if( minor != CURRENT_MINOR_VERSION )
    {
      A3M_LOG_ERROR( "ShaderProgram file: %s wrong version", name );
      return A3M_FALSE;
    }

    /* Look for and remember shader filenames */
    while( !range.empty() )
    {
      CharRange tag = readToken( range );
      if( tag == "vertex_source_file" )
      {
        vertexSourceFile = readToken( range );
      }
      else if( tag == "fragment_source_file" )
      {
        fragmentSourceFile = readToken( range );
      }
      else if( tag == "uniform" )
      {
        std::string uniformInstanceName( readToken( range ).begin );
        std::string uniformPropertyName( readToken( range ).begin );

        if( uniformInstanceName.empty() || uniformPropertyName.empty() )
        {
          A3M_LOG_ERROR("ShaderProgram file: %s uniform declaration incomplete",
                        name);
        }
        else
        {
          uniforms[uniformInstanceName] = uniformPropertyName;
        }
      }
      else if( std::string( tag.begin, tag.end ).substr(0, 2) == "//" )
      {
        // This is a comment, so skip the entire line
        readTo( range, '\n' );
      }
      else
      {
        A3M_LOG_ERROR( "ShaderProgram file: %s unexpected tag %s",
                       name, tag.begin );
        return A3M_FALSE;
      }
      eatWhite( range );
    }

    if( vertexSourceFile.empty() )
    {
      A3M_LOG_ERROR( "ShaderProgram file: %s missing vertex source", name );
      return A3M_FALSE;
    }

    if( fragmentSourceFile.empty() )
    {
      A3M_LOG_ERROR( "ShaderProgram file: %s missing fragment source", name );
      return A3M_FALSE;
    }

    return A3M_TRUE;
  }

  /*
   * Write a compiled version of the shader to the given stream
   * No error is reported if the file cannot be written (this is expected
   * on the Windows platform anyway).
   */
  void writeCompiledShader( Stream& stream,
                            ShaderProgram& shaderProgram,
                            detail::DigestMaker::Digest const& digest )
  {
    A3M_UINT32 size;
    A3M_UINT32 format;
    A3M_CHAR8* binary;

    if( shaderProgram.getBinary( size, format, binary ) )
    {
      digest.write( stream );
      stream.write( &CURRENT_CACHE_VERSION,
                    sizeof( CURRENT_CACHE_VERSION ) );
      stream.write( &format, sizeof( format ) );
      stream.write( binary, size );
      delete [] binary;
    }
  }

} /* anonymous namespace */

namespace a3m
{
  ShaderProgramLoader::ShaderProgramLoader() :
    m_binaryCachingEnabled(getShaderProgramBinariesSupported())
  {
    // Log shader binary caching status for user's reference.
    if (m_binaryCachingEnabled)
    {
      A3M_LOG_INFO("Shader program binary caching is enabled.");
    }
    else
    {
      A3M_LOG_INFO("Shader program binary caching is disabled.");
    }
  }

  /*
   * Loads a new ShaderProgram from the file system.
   */
  ShaderProgram::Ptr ShaderProgramLoader::load(
    ShaderProgramCache& cache, A3M_CHAR8 const* name)
  {
    std::string compiledName = std::string(name) + 'c';
    Stream::Ptr compiledStream;

    detail::DigestMaker::Digest compiledDigest;

    if (m_binaryCachingEnabled)
    {
      // Attempt to load a precompiled shader from regular sources first.
      compiledStream = cache.getStream( compiledName.c_str() );

      // If not found, try loading from the runtime cache directory.
      if( compiledStream )
      {
        A3M_LOG_INFO( "Precompiled shader binary \"%s\" found",
                      compiledName.c_str() );
      }
      else
      {
        compiledStream = cache.getCacheStream( compiledName.c_str() );

        if( compiledStream )
        {
          A3M_LOG_INFO( "Cached shader binary \"%s\" found",
                        compiledName.c_str() );
        }
      }

      if( compiledStream )
      {
        compiledDigest.read( *compiledStream );
      }
    }

    // Split name into filename and "#define" statements
    std::string filename, predefines;
    parseName( name, filename, predefines );

    // Read shader source
    Stream::Ptr stream = cache.getStream(filename.c_str());

    ShaderProgram::Ptr shaderProgram;

    if (!stream)
    {
      return shaderProgram;
    }

    FileToString file( *stream );
    CharRange range( file ), vertexSourceFile, fragmentSourceFile;
    StringMap uniforms;

    if( !parseShaderFile( filename.c_str(), range,
                          vertexSourceFile, fragmentSourceFile,
                          uniforms ) )
    {
      // The error will already be logged by loadShaderFile
      return shaderProgram;
    }

    // Open the vertex and fragment shader files
    Stream::Ptr vFile = cache.getStream( vertexSourceFile.begin );
    if( !vFile )
    {
      vertexSourceFile.nullTerminate();
      A3M_LOG_ERROR( "Reading vertex shader file: %s", vertexSourceFile.begin );
      return shaderProgram;
    }

    Stream::Ptr fFile = cache.getStream( fragmentSourceFile.begin );
    if( !fFile )
    {
      fragmentSourceFile.nullTerminate();
      A3M_LOG_ERROR( "Reading vertex shader file: %s", fragmentSourceFile.begin );
      return shaderProgram;
    }

    // Load shader sources and prepend #defines
    std::string vertexSource = predefines + FileToString( *vFile ).get();
    std::string fragmentSource = predefines + FileToString( *fFile ).get();

    // Compute hash
    detail::DigestMaker digestMaker;
    digestMaker.append( vertexSource.c_str(), vertexSource.length() );
    digestMaker.append( fragmentSource.c_str(), fragmentSource.length() );

    // Load the cached version if digests match (if no cached file exists this
    // will also fail)
    if( digestMaker.digest() == compiledDigest )
    {
      shaderProgram = loadBinary( cache, compiledStream, filename.c_str() );
    }

    if( shaderProgram )
    {
      A3M_LOG_INFO( "Loaded precompiled shader binary \"%s\".", name );
    }
    else // It either didn't match or failed to load.
    {
      shaderProgram = cache.create( vertexSource.c_str(),
                                    fragmentSource.c_str() );
      compiledStream.reset(); // Make sure file not left open.

      if( shaderProgram )
      {
        A3M_LOG_INFO( "Compiled shader from source \"%s\".", name );

        if( m_binaryCachingEnabled )
        {
          Stream::Ptr stream = cache.getCacheStream(
                                 compiledName.c_str(), A3M_TRUE );
          if( stream )
          {
            writeCompiledShader( *stream, *shaderProgram,
                                 digestMaker.digest() );

            A3M_LOG_INFO( "Wrote compiled shader to file \"%s\".",
                          compiledName.c_str() );
          }
          else
          {
            A3M_LOG_INFO( "Failed to write compiled shader to file \"%s\".",
                          compiledName.c_str() );
          }
        }
      }
      else
      {
        A3M_LOG_INFO( "Failed to compile shader \"%s\" from source.", name );
      }
    }

    if( shaderProgram )
    {
      // Associate the uniforms with their properties
      for( A3M_INT32 i = 0; i < shaderProgram->getUniformCount(); ++i )
      {
        A3M_CHAR8 const* uniformInstanceName = shaderProgram->getUniformInstanceName(i);
        StringMap::iterator it = uniforms.find(uniformInstanceName);

        if (it == uniforms.end())
        {
          A3M_LOG_WARN( "ShaderProgram file: %s uniform \"%s\" is not "
                        "declared in the shader program file",
                        name, uniformInstanceName );
        }
        else
        {
          shaderProgram->setUniformPropertyName(i, it->second.c_str());
        }
      }
    }
    else
    {
      A3M_BOOL errorShaderHasError = std::string(ERROR_SHADER_NAME) == name;
      A3M_ASSERT(!errorShaderHasError);

      // Avoid infinite recursion if there is an error in the error shader.
      if (!errorShaderHasError)
      {
        shaderProgram = load(cache, ERROR_SHADER_NAME);
      }
    }

    return shaderProgram;
  }

  A3M_BOOL ShaderProgramLoader::isKnown(A3M_CHAR8 const* name)
  {
    return endsWithI(name, ".sp");
  }

  void ShaderProgramLoader::setBinaryCachingEnabled(A3M_BOOL flag)
  {
    if (flag && !getShaderProgramBinariesSupported())
    {
      A3M_LOG_ERROR(
        "Shader program binaries are not supported; please check "
        "support using getShaderProgramBinariesSupported()");
      return;
    }

    m_binaryCachingEnabled = flag;
  }

  A3M_BOOL ShaderProgramLoader::getBinaryCachingEnabled() const
  {
    return m_binaryCachingEnabled;
  }

} /* namespace a3m */
