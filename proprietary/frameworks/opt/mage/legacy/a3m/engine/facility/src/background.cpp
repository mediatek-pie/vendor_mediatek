/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * Background Implementaion.
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/

#include <GLES2/gl2.h>              /* OpenGL ES 2.0 API */

#include <a3m/background.h>         /* This module's header */
#include <a3m/log.h>                /* for A3M_LOG_ functions */
#include <a3m/rendercontext.h>      /* for RenderContext */

#include <error.h>                  /* for CHECK_GL_ERROR */

/*****************************************************************************
 * A3M Namespace
 *****************************************************************************/
namespace a3m
{

  /*
   * Constructor
   */
  Background::Background() :
    m_colour( 0.f, 0.f, 0.f, 0.f ),
    m_depth( 1.0f ),
    m_stencil( 0 ),
    m_colourMaskR( A3M_TRUE ),
    m_colourMaskG( A3M_TRUE ),
    m_colourMaskB( A3M_TRUE ),
    m_colourMaskA( A3M_TRUE ),
    m_depthMask( A3M_TRUE ),
    m_stencilMask( ~0 )
  {
  }

  Background::Background( Colour4f const& colour ) :
    m_colour( colour ),
    m_depth( 1.0f ),
    m_stencil( 0 ),
    m_colourMaskR( A3M_TRUE ),
    m_colourMaskG( A3M_TRUE ),
    m_colourMaskB( A3M_TRUE ),
    m_colourMaskA( A3M_TRUE ),
    m_depthMask( A3M_TRUE ),
    m_stencilMask( ~0 )
  {
  }

  /*
   * Sets the background colour.
   */
  void Background::setColour( const Colour4f& colour )
  {
    m_colour = colour;
  }

  /*
   * Sets the background depth value.
   */
  void Background::setDepth( A3M_FLOAT depth )
  {
    m_depth = depth;
  }

  /*
   * Sets the background stencil value.
   */
  void Background::setStencil( A3M_INT32 stencil )
  {
    m_stencil = stencil;
  }

  /*
   * Sets the colour buffer clear mask value.
   */
  void Background::setColourMask( A3M_BOOL rMask,
                                  A3M_BOOL gMask,
                                  A3M_BOOL bMask,
                                  A3M_BOOL aMask )
  {
    m_colourMaskR = rMask;
    m_colourMaskG = gMask;
    m_colourMaskB = bMask;
    m_colourMaskA = aMask;
  }

  /*
   * Sets the depth buffer clear mask value.
   */
  void Background::setDepthMask( A3M_BOOL depthMask )
  {
    m_depthMask = depthMask;
  }

  /*
   * Sets the stencil buffer clear mask value.
   */
  void Background::setStencilMask( A3M_UINT32 stencilMask )
  {
    m_stencilMask = stencilMask;
  }

  /*
   * Clears all buffers with set clear values and applies all buffer masks.
   */
  void Background::enable(RenderContext& context) const
  {
    context.setColourMask( m_colourMaskR, m_colourMaskG, m_colourMaskB, m_colourMaskA );
    context.setDepthWriteEnabled( m_depthMask );
    a3m::setStencilMask( context, m_stencilMask );

    context.setClearDepth( m_depth );
    context.setClearStencil( m_stencil );
    context.setClearColour( m_colour );

    // We want to clear the entire screen, so make sure scissor test is off
    context.setScissorTestEnabled( A3M_FALSE );

    context.clear();
  }

} /* namespace a3m */
