/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * ShaderProgramLoader class
 *
 */

#pragma once
#ifndef A3M_SHADERPROGRAMLOADER_H
#define A3M_SHADERPROGRAMLOADER_H

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/shaderprogram.h>     /* ShaderProgram etc. */
#include <a3m/assetloader.h>       /* AssetLoader */

namespace a3m
{
  /** \ingroup a3mAssetloader
   *
   * Shader Program Loader is a default AssetLoader for loading ShaderProgram
   * objects from the file system.
   *
   * @{
   */

  /**
   * ShaderProgramLoader class.
   * Creates new ShaderPrograms from data read from the file system.
   */
  class ShaderProgramLoader : public AssetLoader<ShaderProgram>
  {
  public:
    /** Constructor.
     */
    ShaderProgramLoader();

    /** Destructor.
     */
    virtual ~ShaderProgramLoader() {}

    // Override
    ShaderProgram::Ptr load(
      ShaderProgramCache& cache, /**< Cache to use to create the asset */
      A3M_CHAR8 const* name /**< Name of the asset to load */);

    // Override
    A3M_BOOL isKnown(
      A3M_CHAR8 const* name /**< Name of the asset */);

    /** Enables or disables shader program binary caching.
     * If shader binaries are not supported, this function will log an error,
     * and the shader caching enabled flag will remain FALSE.  If shader
     * binaries are supported, it will be enabled by default.
     */
    void setBinaryCachingEnabled(A3M_BOOL flag /**< Caching enable flag */);

    /** Returns whether shader program binary caching is enabled.
     * If shader caching is not supported, this function will always return
     * FALSE.
     * \return TRUE if shader caching is enabled and supported
     */
    A3M_BOOL getBinaryCachingEnabled() const;

  private:
    A3M_BOOL m_binaryCachingEnabled; /* Whether to cache shader binaries */
  };

  /** @} */

} /* namespace a3m */

#endif /* A3M_SHADERPROGRAMLOADER_H */
