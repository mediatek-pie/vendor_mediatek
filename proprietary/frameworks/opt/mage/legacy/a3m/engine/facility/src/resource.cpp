/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * Resource Implementation
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <GLES2/gl2.h>                    /* for glGenBuffers etc. */

#include <error.h>                        /* for CHECK_GL_ERROR */
#include <a3m/detail/glresource.h>        /* for GlResource */
#include <a3m/detail/bufferresource.h>    /* for BufferResource */
#include <a3m/detail/programresource.h>   /* for ProgramResource */
#include <a3m/detail/shaderresource.h>    /* for ShaderResource */
#include <a3m/detail/textureresource.h>   /* for TextureResource */
#include <a3m/log.h>                      /* for logging */

namespace a3m
{
  namespace detail
  {
    /*************************************************************************
     * GlResource
     *************************************************************************/

    GlResource::GlResource() :
      m_state(Resource::UNALLOCATED),
      m_id(0)
    {
    }

    GlResource::GlResource(A3M_UINT32 id) :
      m_state(Resource::ALLOCATED),
      m_id(id)
    {
    }

    GlResource::~GlResource()
    {
      // Resource should have been deallocated or released before destruction,
      // or we may have a memory leak.
      A3M_ASSERT(m_state != Resource::ALLOCATED);
    }

    A3M_BOOL GlResource::deallocate()
    {
      A3M_BOOL successful = A3M_FALSE;

      // Deallocate only if resource is allocated, and release only if not
      // yet allocated.
      if (m_state == Resource::ALLOCATED)
      {
        doDeallocate(m_id);
        m_id = 0;
        m_state = Resource::DEALLOCATED;
        successful = A3M_TRUE;
      }
      else if (m_state == Resource::UNALLOCATED)
      {
        m_state = Resource::RELEASED;
      }

      return successful;
    }

    A3M_BOOL GlResource::release()
    {
      A3M_BOOL successful = A3M_FALSE;

      // Report successful release only if not already deallocated
      if (m_state != Resource::DEALLOCATED)
      {
        m_id = 0;
        m_state = Resource::RELEASED;
        successful = A3M_TRUE;
      }

      return successful;
    }

    A3M_BOOL GlResource::allocate()
    {
      if (m_state == Resource::UNALLOCATED)
      {
        m_id = doAllocate();

        if (m_id)
        {
          m_state = Resource::ALLOCATED;
          return A3M_TRUE;
        }
        else
        {
          m_state = Resource::RELEASED;
          return A3M_FALSE;
        }
      }
      else
      {
        A3M_LOG_ERROR( "Cannot allocate resource more than once", 0);
      }

      return A3M_FALSE;
    }

    /*************************************************************************
     * BufferResource
     *************************************************************************/

    A3M_UINT32 BufferResource::doAllocate()
    {
      A3M_UINT32 id;
      glGenBuffers(1, &id);
      CHECK_GL_ERROR;

      if (!id)
      {
        A3M_LOG_ERROR( "Failed to create OpenGL buffer", 0);
      }

      return id;
    }

    void BufferResource::doDeallocate(A3M_UINT32 id)
    {
      glDeleteBuffers(1, &id);
      CHECK_GL_ERROR;
    }

    /*************************************************************************
     * ProgramResource
     *************************************************************************/

    A3M_UINT32 ProgramResource::doAllocate()
    {
      A3M_UINT32 id = glCreateProgram();
      CHECK_GL_ERROR;

      if (!id)
      {
        A3M_LOG_ERROR( "Failed to create OpenGL program", 0);
      }

      return id;
    }

    void ProgramResource::doDeallocate(A3M_UINT32 id)
    {
      glDeleteProgram(id);
      CHECK_GL_ERROR;
    }

    /*************************************************************************
     * ShaderResource
     *************************************************************************/

    A3M_UINT32 ShaderResource::doAllocate()
    {
      A3M_UINT32 id = glCreateShader(getGlShaderType());
      CHECK_GL_ERROR;

      if (!id)
      {
        A3M_LOG_ERROR( "Failed to create OpenGL shader", 0);
      }

      return id;
    }

    void ShaderResource::doDeallocate(A3M_UINT32 id)
    {
      glDeleteShader(id);
      CHECK_GL_ERROR;
    }

    A3M_UINT32 ShaderResource::getGlShaderType() const
    {
      switch (m_shaderType)
      {
      case VERTEX:
        return GL_VERTEX_SHADER;
        break;

      case FRAGMENT:
        return GL_FRAGMENT_SHADER;
        break;

      default:
        return GL_INVALID_ENUM;
        break;
      }
    }

    /*************************************************************************
     * TextureResource
     *************************************************************************/

    TextureResource::TextureResource(A3M_UINT32 id) : GlResource( id )
    {
    }

    A3M_UINT32 TextureResource::doAllocate()
    {
      A3M_UINT32 id;
      glGenTextures(1, &id);
      CHECK_GL_ERROR;

      if (!id)
      {
        A3M_LOG_ERROR( "Failed to create OpenGL texture", 0);
      }

      return id;
    }

    void TextureResource::doDeallocate(A3M_UINT32 id)
    {
      glDeleteTextures(1, &id);
      CHECK_GL_ERROR;
    }

  } /* namespace detail */

} /* namespace a3m */

