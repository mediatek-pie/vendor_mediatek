/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * Built-in property names.
 * This file contains names for properties which are recognised and used by the
 * various subsystems of A3M.  These names should always be used within A3M,
 * rather than the strings they represent.
 */
#pragma once
#ifndef A3M_PROPERTIES_H
#define A3M_PROPERTIES_H

#include <a3m/base_types.h> /* A3M_CHAR8 */

/******************************************************************************
 * Include Files
 ******************************************************************************/

namespace a3m
{

  namespace properties
  {

    /***********************
     * Material properties *
     ***********************/

    extern A3M_CHAR8 const* M_DIFFUSE_COLOUR;
    extern A3M_CHAR8 const* M_AMBIENT_COLOUR;
    extern A3M_CHAR8 const* M_EMISSIVE_COLOUR;
    extern A3M_CHAR8 const* M_SPECULAR_COLOUR;

    extern A3M_CHAR8 const* M_SHININESS;
    extern A3M_CHAR8 const* M_OPACITY;
    extern A3M_CHAR8 const* M_SELF_ILLUMINATION;
    extern A3M_CHAR8 const* M_SPECULAR_LEVEL;
    extern A3M_CHAR8 const* M_UV_OFFSET_SCALE;

    extern A3M_CHAR8 const* M_DIFFUSE_TEXTURE;
    extern A3M_CHAR8 const* M_AMBIENT_TEXTURE;
    extern A3M_CHAR8 const* M_EMISSIVE_TEXTURE;
    extern A3M_CHAR8 const* M_SPECULAR_TEXTURE;

    extern A3M_CHAR8 const* M_SPECULAR_LEVEL_TEXTURE;
    extern A3M_CHAR8 const* M_GLOSS_TEXTURE;
    extern A3M_CHAR8 const* M_FILTER_COLOUR_TEXTURE;
    extern A3M_CHAR8 const* M_REFLECTION_TEXTURE;
    extern A3M_CHAR8 const* M_REFRACTION_TEXTURE;
    extern A3M_CHAR8 const* M_MIRROR_TEXTURE;
    extern A3M_CHAR8 const* M_BUMP_TEXTURE;
    extern A3M_CHAR8 const* M_DISPLACEMENT_TEXTURE;

    extern A3M_CHAR8 const* M_BLACK_TEXTURE;
    extern A3M_CHAR8 const* M_WHITE_TEXTURE;
    extern A3M_CHAR8 const* M_ERROR_TEXTURE;

    /********************
     * Light properties *
     ********************/

    extern A3M_CHAR8 const* L_COUNT;
    extern A3M_CHAR8 const* L_DIFFUSE_COLOUR;
    extern A3M_CHAR8 const* L_AMBIENT_COLOUR;
    extern A3M_CHAR8 const* L_SPECULAR_COLOUR;
    extern A3M_CHAR8 const* L_POSITION;
    extern A3M_CHAR8 const* L_ATTENUATION_NEAR;
    extern A3M_CHAR8 const* L_ATTENUATION_RECIPROCAL;
    extern A3M_CHAR8 const* L_SPOT_DIRECTION;
    // Cosine of the half-angle of the light's "full brightness" cone
    extern A3M_CHAR8 const* L_SPOT_INNER_COS;
    // Cosine of the half-angle of the light's "fall-off" cone
    extern A3M_CHAR8 const* L_SPOT_OUTER_COS;

    /*****************************
     * Skeleton joint properties *
     *****************************/

    extern A3M_CHAR8 const* J_COUNT;
    extern A3M_CHAR8 const* J_WORLD;

    /************************
     * Transform properties *
     ************************/

    extern A3M_CHAR8 const* T_MODEL;
    extern A3M_CHAR8 const* T_VIEW;
    extern A3M_CHAR8 const* T_MODEL_VIEW;
    extern A3M_CHAR8 const* T_VIEW_PROJECTION;
    extern A3M_CHAR8 const* T_MODEL_VIEW_PROJECTION;
    extern A3M_CHAR8 const* T_OLD_MODEL_VIEW_PROJECTION;
    extern A3M_CHAR8 const* T_NORMAL_MODEL;
    extern A3M_CHAR8 const* T_CAMERA_POSITION;

    /********************
     * Scene properties *
     ********************/

    extern A3M_CHAR8 const* TIME;
    extern A3M_CHAR8 const* FOG_ENABLED;
    extern A3M_CHAR8 const* FOG_COLOUR;
    extern A3M_CHAR8 const* FOG_DENSITY;
    extern A3M_CHAR8 const* MOTION_BLUR_FACTOR;
    extern A3M_CHAR8 const* DELTA_TIME;

  } // namespace properties

} // namespace a3m

#endif // A3M_PROPERTIES_H
