/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2013. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * std::string Utility Function Implementation
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <cctype>                 /* for std::tolower()                      */
#include <stdstringutility.h>     /* This module's header                    */

namespace
{

  /*
   * Used to define case sensitivity of algorithms at compile-time.
   */
  enum CaseSensitivity
  {
    CASE_SENSITIVE,
    CASE_INSENSITIVE
  };

  /*
   * Formats individual characters according to case sensitivity.
   */
  template<CaseSensitivity>
  struct Format
  {
    static A3M_CHAR8 character(A3M_CHAR8 chr);
  };

  template<>
  A3M_CHAR8 Format<CASE_SENSITIVE>::character(A3M_CHAR8 chr)
  {
    return chr;
  }

  template<>
  A3M_CHAR8 Format<CASE_INSENSITIVE>::character(A3M_CHAR8 chr)
  {
    return std::tolower(chr);
  }

  /*
   * Templated version of endsWith() which handles case sensitivity.
   */
  template<CaseSensitivity sensitivity>
  A3M_BOOL localEndsWith(std::string const& str, std::string const& substr)
  {
    A3M_INT32 start = str.length() - substr.length();

    if (start < 0)
    {
      return A3M_FALSE;
    }

    for (A3M_UINT32 i = 0; i < substr.length(); ++i)
    {
      if (Format<sensitivity>::character(str[i + start]) !=
          Format<sensitivity>::character(substr[i]))
      {
        return A3M_FALSE;
      }
    }

    return A3M_TRUE;
  }

} // namespace

namespace a3m
{

  A3M_BOOL endsWith(std::string const& str, std::string const& substr)
  {
    return localEndsWith<CASE_SENSITIVE>(str, substr);
  }

  A3M_BOOL endsWithI(std::string const& str, std::string const& substr)
  {
    return localEndsWith<CASE_INSENSITIVE>(str, substr);
  }

} /* namespace a3m */
