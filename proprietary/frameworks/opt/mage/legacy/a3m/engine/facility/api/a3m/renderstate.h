/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2013. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * Render state types.
 *
 */
#pragma once
#ifndef A3M_RENDERSTATE_H
#define A3M_RENDERSTATE_H

namespace a3m
{
  /** \defgroup a3mRenderState RenderState
   * \ingroup  a3mFacility
   *
   * A collection of enums used to describe render state.
   *
   * @{
   */

  /**
   * Describes which sides of a polygon to draw.
   */
  enum CullingMode
  {
    CULL_BACK,           /**< The back-facing side of a polygon is not to be
                              drawn */
    CULL_FRONT,          /**< The front-facing side of a polygon is not to be
                              drawn */
    CULL_FRONT_AND_BACK, /**< Both front and back facing polygons are not to
                              be drawn. This is not the mode usually set. It
                              is here to allow an application to access
                              OpenGL ES 2.0 corresponding function which in
                              essence renders nothing. */
    CULL_NONE            /**< Both faces of a polygon are to be draw. This is
                              not the mode usually set. This disables
                              culling. */
  };

  /**
   * Describes the winding direction of on-screen polygons.
   */
  enum WindingOrder
  {
    WIND_CCW,         /**< Front-facing is a polygon having its vertices
                           in counter-clockwise order in screen space. */
    WIND_CW           /**< Front-facing is a polygon having its vertices
                           in clockwise order in screen space. */
  };

  /**
   * Describes the comparison function for depth test in enum.
   * ref: the depth test reference value
   * mask: the depth test bitmask
   */
  enum DepthFunction
  {
    DEPTH_NEVER,        /**< Always fails */
    DEPTH_LESS,         /**< Passes if ( ref & mask ) < ( depth & mask ) */
    DEPTH_EQUAL,        /**< Passes if ( ref & mask ) = ( depth & mask ) */
    DEPTH_LEQUAL,       /**< Passes if ( ref & mask ) <= ( depth & mask ) */
    DEPTH_GREATER,      /**< Passes if ( ref & mask ) > ( depth & mask ) */
    DEPTH_NOTEQUAL,     /**< Passes if ( ref & mask ) != ( depth & mask ) */
    DEPTH_GEQUAL,       /**< Passes if ( ref & mask ) >= ( depth & mask ) */
    DEPTH_ALWAYS        /**< Always passes */
  };

  /**
   * Describes the stencil parameters for facing primitives in enum.
   */
  enum StencilFace
  {
    STENCIL_BACK,         /**< Back-facing primitive */
    STENCIL_FRONT,        /**< Front-facing primitive */
    STENCIL_NUM_FACES     /**< Enum size count for easy iteration */
  };

  /**
   * Describes the comparison function for stencil test in enum.
   * ref: the stencil test reference value
   * mask: the stencil test bitmask
   */
  enum StencilFunction
  {
    STENCIL_NEVER,        /**< Always fails */
    STENCIL_LESS,         /**< Passes if ( ref & mask ) < ( stencil & mask ) */
    STENCIL_EQUAL,        /**< Passes if ( ref & mask ) = ( stencil & mask ) */
    STENCIL_LEQUAL,       /**< Passes if ( ref & mask ) <= ( stencil & mask ) */
    STENCIL_GREATER,      /**< Passes if ( ref & mask ) > ( stencil & mask ) */
    STENCIL_NOTEQUAL,     /**< Passes if ( ref & mask ) != ( stencil & mask ) */
    STENCIL_GEQUAL,       /**< Passes if ( ref & mask ) >= ( stencil & mask ) */
    STENCIL_ALWAYS        /**< Always passes */
  };

  /**
   * Describes the stencil operation in enum.
   */
  enum StencilOperation
  {
    STENCIL_ZERO,      /**< Sets the stencil buffer value to zero */
    STENCIL_KEEP,      /**< Leaves the existing stencil buffer contents unmodified */
    STENCIL_REPLACE,   /**< Copies the stencil reference value to the buffer */
    STENCIL_INCR,      /**< Increments the stencil buffer value by one */
    STENCIL_DECR,      /**< Decrements the stencil buffer value by one */
    STENCIL_INVERT,    /**< Performs a bitwise inversion to the stencil buffer
                            value */
    STENCIL_INCR_WRAP, /**< Increments the stencil buffer value by one, but wrap the
                            value if the stencil value overflows. */
    STENCIL_DECR_WRAP  /**< Decrements the stencil buffer value by one, but wrap the
                            value if the stencil value underflows. */
  };

  /**
   * Describes the blender factors in enum.
   */
  enum BlendFactor
  {
    BLEND_CONSTANT_ALPHA,      /**< The source or destination colour (or
                               * equivalently alpha) is to be multiplied by
                               * the alpha component of the constant blend
                               * colour. */
    BLEND_CONSTANT_COLOUR,     /**< The source or destination colour (or
                               * equivalently alpha) is to be multiplied
                               * component-wise by the constant blend colour
                               * (or alpha). */
    BLEND_DST_ALPHA,           /**< The source or destination colour (or
                               * equivalently alpha) is to be multiplied by
                               * the alpha component of the destination
                               * colour. */
    BLEND_DST_COLOUR,          /**< The source or destination colour (or
                               * equivalently alpha) is to be multiplied
                               * component-wise by the destination colour
                               * and alpha. */
    BLEND_ONE,                 /**< The source or destination colour (or
                               * equivalently alpha) is to be multiplied by
                               * one (that is, taken as such). */
    BLEND_ONE_MINUS_CONSTANT_ALPHA, /**< The source or destination colour (or
                               * equivalently alpha) is to be multiplied by
                               * one minus the alpha component of the
                               * constant blend colour. */
    BLEND_ONE_MINUS_CONSTANT_COLOUR,/**< The source or destination colour (or
                               * equivalently alpha) is to be multiplied
                               * component-wise by one minus the constant
                               * blend colour (or alpha). */
    BLEND_ONE_MINUS_DST_ALPHA, /**< The source or destination colour (or
                               * equivalently alpha) is to be multiplied by
                               * one minus the alpha component of the
                               * destination blend colour. */
    BLEND_ONE_MINUS_DST_COLOUR,/**< The source or destination colour (or
                               * equivalently alpha) is to be multiplied
                               * component-wise by one minus the destination
                               * colour and alpha. */
    BLEND_ONE_MINUS_SRC_ALPHA, /**< The source or destination colour (or
                               * equivalently alpha) is to be multiplied by
                               * one minus the alpha component of the source
                               * colour. */
    BLEND_ONE_MINUS_SRC_COLOUR,/**< The source or destination colour (or
                               * equivalently alpha) is to be multiplied
                               * component-wise by one minus the source
                               * colour and alpha. */
    BLEND_SRC_ALPHA,           /**< The source or destination colour (or
                               * equivalently alpha) is to be multiplied by
                               * the alpha component of the source colour. */
    BLEND_SRC_ALPHA_SATURATE,  /**< The source colour is to be multiplied by
                               * the source alpha, or one minus the
                               * destination  alpha, whichever is the
                               * smallest. */
    BLEND_SRC_COLOUR,          /**< The source or destination colour (or
                               * equivalently alpha) is to be multiplied
                               * component-wise by the source colour and
                               * alpha. */
    BLEND_ZERO                 /**< The source or destination colour (or
                               * equivalently alpha) is to be multiplied
                               * by zero (that is, ignored). */
  };

  /**
   * Describes the blender function in enum.
   */
  enum BlendFunction
  {
    BLEND_ADD,                      /**< The default additive blending mode */
    BLEND_REVERSE_SUBTRACT,         /**< Reversed subtractive blending      */
    BLEND_SUBTRACT,                 /**< Subtractive blending              */
  };

  /** @} */

} // namespace a3m

#endif /* A3M_RENDERSTATE_H */
