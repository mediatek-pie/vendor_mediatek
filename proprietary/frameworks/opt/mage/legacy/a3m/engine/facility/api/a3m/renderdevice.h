/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * RenderDevice class
 *
 */
#pragma once
#ifndef A3M_RENDERDEVICE_H
#define A3M_RENDERDEVICE_H

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/base_types.h>            /* Base types used by all A3M modules */
#include <a3m/platform.h>              /* for A3M_PLATFORM                   */

#if A3M_PLATFORM == A3M_WINDOWS
#include <WinError.h> /* for NO_ERROR, so that we can undefine it. */
#undef NO_ERROR
#endif

/*****************************************************************************
 * A3M Namespace
 *****************************************************************************/
namespace a3m
{
  /** \defgroup a3mRendevice Render Device
   *  \ingroup  a3mRefScene
   *
   *  The Render Device provides a low-level API for rendering operations such as
   *  state manipulation, state queries and primitive rendering.
   *
   *  @{
   */

  /**
   * Render device namespace. This namespace encapsulates functions used for
   * rendering operations.
   *
   */
  namespace RenderDevice
  {

    /** \name Enumerated type 'ErrorCode'.
     * ErroCode enum lists various errors returned by rendering device.
     *
     */
    enum ErrorCode
    {
      NO_ERROR,
      INVALID_ENUM,
      INVALID_VALUE,
      INVALID_OPERATION,
      INVALID_FRAMEBUFFER_OPERATION,
      OUT_OF_MEMORY,
      UNDEFINED_ERROR
    };

    /*************************************************************************
     * RenderDevice Functions
     *************************************************************************/
    /** \name RenderDevice Functions
     * Below functions are defined within RenderDevice namespace.
     * @{
     */

    /**
     * Retrieves the last error reported by the
     * rendering device as an ErrorCode enum.
     *
     * \return Enumerated error code.
     */
    ErrorCode getError(
      A3M_BOOL cached = true
                        /**< Specifies whether get the cached error code. */);

    /**
     * Retrieves the last error reported by the
     * rendering device as a string.
     *
     * It converts ErrorCode enum in to an appropriate string.
     *
     * \return Pointer to NULL-terminated error string
     */
    const A3M_CHAR8* getErrorString(
      ErrorCode error
      /**< Error code to convert to string */);

    /**
     * Retrieves the vendor name for this rendering device
     * as a string.
     *
     * \return Pointer to NULL-terminated string.
     */
    const A3M_CHAR8* getVendor();

    /**
     * Retrieves the version for this rendering device
     * as a string.
     *
     * \return Pointer to NULL-terminated version string.
     */
    const A3M_CHAR8* getVersion();

    /**
     * Returns the maximum number of four-component uniforms supported in a
     * vertex shader.
     *
     * \return Maximum number of uniforms
     */
    A3M_INT32 getMaxVertexUniformVectors();

    /**
     * Returns the maximum number of four-component uniforms supported in a
     * fragment shader.
     *
     * \return Maximum number of uniforms
     */
    A3M_INT32 getMaxFragmentUniformVectors();

    /**
     * Returns the maximum number of four-component vertex attributes supported.
     *
     * \return Maximum number of attributes
     */
    A3M_INT32 getMaxVertexAttributeVectors();

    /**
     * Returns the maximum number of four-component varyings supported.
     *
     * \return Maximum number of varyings
     */
    A3M_INT32 getMaxVaryingVectors();

    /**
     * Returns a space-separated string list of supported extensions.
     *
     * \return Pointer to space seperated and NULL-terminated list of strings.
     */
    const A3M_CHAR8* getExtensions();

    /**
     * Returns whether a particular extension is supported.
     *
     * \return TRUE if the extension is supported
     */
    A3M_BOOL isExtensionSupported(A3M_CHAR8 const* name /**< Extension name */);

    /**
     * Retrieves the shader language version supported by
     * this rendering device as a string.
     *
     * \return Pointer to NULL-terminated shader (language) version string.
     */
    const A3M_CHAR8* getShaderVersion();

    /**
     * Get pixels from the colour buffer
     * Use this function to read the pixel values from a rectangular region of
     * the current colour buffer (may be the device or a RenderTarget object)
     * into a supplied region of memory. The pointer you supply to this routine
     * must point to an array of at least width*height*4 bytes. Each pixel is
     * four bytes in the order RGBA.
     */
    void getPixels( A3M_INT32 left, /**< left edge of region */
                    A3M_INT32 bottom, /**< bottom edge of region */
                    A3M_UINT32 width,  /**< width of region to read */
                    A3M_UINT32 height, /**< height of region to read */
                    A3M_UINT8* pixels  /**< [out] pixel buffer */ );

  } /* namespace RenderDevice */

  /** @} */

} /* namespace a3m */

#endif /* A3M_RENDERDEVICE_H */

