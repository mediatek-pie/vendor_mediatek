/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * ResourceCache class
 *
 */
#ifndef A3M_RESOURCECACHE_H
#define A3M_RESOURCECACHE_H

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <list>                         /* for std::list */

#include <a3m/detail/resource.h>        /* for Resource   */
#include <a3m/pointer.h>                /* for SharedPtr   */
#include <a3m/noncopyable.h>            /* for NonCopyable */

namespace a3m
{
  namespace detail
  {
    /** \defgroup a3mResourcecache ResourceCache
     * \ingroup  a3mInt
     *
     * A resource cache is a collection of Resource objects.
     * All resources should be stored in a resource cache, so that they can all
     * be released on demand from a centralised location. Resource caches are an
     * implementation detail of the the different AssetCache subclasses (e.g.
     * ShaderProgramCache, Texture2DCache) which need access to device
     * resources.
     *
     * @{
     */

    /**
     * ResourceCache class.
     */
    class ResourceCache : public Shared, NonCopyable
    {
    public:
      A3M_NAME_SHARED_CLASS( ResourceCache )

      /** Smart pointer type for this class */
      typedef SharedPtr<ResourceCache> Ptr;

      /**
       * Destructor
       */
      ~ResourceCache();

      /**
       * Adds a resource to the cache.
       */
      void add(detail::Resource::Ptr const& resource /**< Resource to add */);

      /**
       * Releases (not deallocates) and removes all resources held in the cache.
       */
      void release();

      /**
       * Deallocates unique resources and removes DEALLOCATED and RELEASED
       * resources from cache.
       * This function should be called periodically to ensure that the cache
       * does not fill up with unique, DEALLOCATED and RELEASED resource objects.
       * When a resource is unique (only referenced by the cache) it is
       * unreachable and should be released by calling this function.
       */
      void flush();

    private:
      typedef std::list<detail::Resource::Ptr> ResourceList;

      ResourceList m_resources; /* List of cached resources */
    };

    /** @} */

  } /* namespace detail */

} /* namespace a3m */

#endif /* A3M_RESOURCECACHE_H */

