/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * IndexBuffer class
 *
 */
#pragma once
#ifndef A3M_INDEX_BUFFER_H
#define A3M_INDEX_BUFFER_H

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/detail/bufferresource.h> /* for BufferResource */
#include <a3m/assetcache.h> /* for AssetCache */
#include <a3m/pointer.h> /* for SharedPtr */
#include <a3m/renderdevice.h> /* for render() */
#include <a3m/detail/resourcecache.h> /* for ResourceCache */

/*****************************************************************************
 * A3M Namespace
 *****************************************************************************/
namespace a3m
{
  /** \defgroup a3mIndexbuffer Index Buffer
   * \ingroup  a3mRefScene
   *
   * Index buffer objects contain indices to the vertices used in a draw
   * operation.
   *
   * Index buffers are assets and are therefore created and managed by an
   * IndexBufferCache.  Often, the client will want to combine IndexBuffer
   * objects and VertexBuffer objects into a Mesh asset, using the MeshCache.
   *
   * \note
   * Generally, the client will want to use AssetCachePool to create and manage
   * asset caches, rather than creating them manually.
   *
   * Example:
   * \code
   * // Create an IndexBufferCache.
   * IndexBufferCache::Ptr cache( new IndexBufferCache() );
   *
   * // Index array storing indices
   * A3M_UINT16 indices[] = {0, 1, 2, 0, 2, 3, 1, 2, 4};
   *
   * // Creates shared index buffer pointer
   * a3m::IndexBuffer::Ptr ibptr = cache->create(
   *   a3m::IndexBuffer::PRIMITIVE_TRIANGLES, 9, indices );
   *
   * ibptr->commit(); // Commits index buffer to GPU
   *
   * program.bindAttribs( *vb );
   *
   * a3m::RenderDevice::render(*vb, *ibptr, *ap);
   *                 // Calls a3m::RenderDevice::render()
   *                 //   which is friend to IndexBuffer class.
   *                 // It renders primitive.
   * \endcode
   * @{
   */

  // Forward declarations
  class IndexBufferCache;

  /**
   * Index buffer class.
   *
   * Holds a chunk of memory containing index array. It contains member
   * functions to commit() vertices to GPU and to draw a mesh using specified
   * primitive.
   */
  class IndexBuffer : public Shared, NonCopyable
  {
  public:
    A3M_NAME_SHARED_CLASS( IndexBuffer )

    /** Smart pointer type */
    typedef a3m::SharedPtr< IndexBuffer > Ptr;

    /** Cache type for this class */
    typedef IndexBufferCache CacheType;

    /**
     * Enum Primitive
     * Describes the type of primitive in enum.
     */
    enum Primitive
    {
      PRIMITIVE_POINTS,         /**< Points */
      PRIMITIVE_LINE_STRIP,     /**< Joins vertices to form strip of line */
      PRIMITIVE_LINE_LOOP,      /**< Line strip where last vertex joins 1st */
      PRIMITIVE_LINES,          /**< Lines joining pair of vertices */
      PRIMITIVE_TRIANGLE_STRIP, /**< Triangle strip */
      PRIMITIVE_TRIANGLE_FAN,   /**< Triangle fan */
      PRIMITIVE_TRIANGLES       /**< Triangle for every 3 vertices */
    };

    /**
     * Destructor.
     *
     * Frees index array memory and deletes index buffer.
     */
    ~IndexBuffer();

    /**
     * Checks validity of construction of index buffer.
     *
     * \return A3M_TRUE if construction is succeded else A3M_FALSE.
     */
    // \todo Index buffer is now always valid.  Check whether this can be
    // removed.
    A3M_BOOL isValid() const { return m_valid; }

    /**
     * Access to the index array's data.
     *
     * \return Writable pointer to the index array's data.
     */
    A3M_UINT16* data() { return m_indexArray ; }

    /**
     * Const access to the index array's data.
     *
     * \return Non-writable pointer to the index array's data.
     */
    A3M_UINT16 const* data() const { return m_indexArray ; }

    /**
     * Returns the number of indices in the buffer.
     *
     * \return Number of indices
     */
    A3M_INT32 getIndexCount() const { return m_count; }

    /**
     * Returns the type of primitive rendered by the index buffer.
     *
     * \return Type of primitive
     */
    Primitive getPrimitiveType() const { return m_primitive; }

    /**
     * Commits the IndexBuffer for rendering.
     *
     * Committing an IndexBuffer allows the implementation to optimize
     * the indices for better performance and reduced memory consumption.
     * It offloads data to GPU.
     * After a buffer is committed, the index data is no longer available to
     * change.
     *
     * \return None.
     */
    void commit();

    /**
     * Renders primitives.
     */
    void draw();

  private:
    friend class IndexBufferCache; /* Is IndexBuffer's factory class */

    /**
     * Private constructor.
     * This constructor is called by IndexBufferCache.
     */
    IndexBuffer(
      Primitive primitive, /**< Primitive type to be rendered */
      A3M_INT32 count, /**< Number of indices to be rendered */
      A3M_BOOL allocate, /**< A3M_TRUE = allocate index array memory  */
      detail::BufferResource::Ptr const& resource /**< Buffer resource */);

    Primitive     m_primitive;       /* Primitive type */
    A3M_UINT16*   m_indexArray;      /* Pointer to index array where indices
                                        to be rendered are stored */
    A3M_INT32     m_count;           /* Number of indices to be rendered */
    detail::BufferResource::Ptr m_resource; /* OpenGL index buffer resource */
    A3M_BOOL      m_valid;           /* True if construction of index buffer
                                        has succeded else false */

  };

  /**
   * AssetCache specialised for storing and creating IndexBuffer assets.
   */
  class IndexBufferCache : public AssetCache<IndexBuffer>
  {
  public:
    /** Smart pointer type for this class */
    typedef SharedPtr< IndexBufferCache > Ptr;

    /**
     * Constructs an IndexBuffer with an index array.
     * Dynamically creates memory and copies index array to it.
     * \return The index buffer, or null if the memory could not be allocated
     */
    IndexBuffer::Ptr create(
      IndexBuffer::Primitive primitive, /**< Primitive type to be rendered */
      A3M_UINT32 count, /**< Number of indices to be rendered */
      A3M_UINT16 const* indices, /**< Pointer to an array of unsigned 16-bit
                                     indices in client memory. If a null
                                     pointer is passed, no data will be copied
                                     into the index array. */
      A3M_CHAR8 const* name = 0 /**< Optional name to give the asset. If
                                    omitted, the asset will not be reachable
                                    via the AssetCache::get() function. */);

    /**
     * Creates an IndexBuffer without an index array.
     * An IndexBuffer without an array causes vertices to be rendered in the
     * order that they appear in the VertexBuffer.
     * \return The index buffer
     */
    IndexBuffer::Ptr create(
      IndexBufferCache& cache, /**< Cache to use */
      IndexBuffer::Primitive primitive, /**< Primitive type to be rendered */
      A3M_UINT32 count, /**< Number of indices to be rendered */
      A3M_CHAR8 const* name = 0 /**< Optional name to give the asset. If
                                    omitted, the asset will not be reachable
                                    via the AssetCache::get() function. */);
  };

  /**
   * Calculates the triangle count of an index buffer.
   * This function does not count line or point primitives.
   * \return Triangle count
   */
  A3M_INT32 getTriangleCount(
    /** Index buffer */
    IndexBuffer const& indexBuffer);

  /** @} */

} /* namespace a3m */

#endif /* A3M_INDEX_BUFFER_H */
