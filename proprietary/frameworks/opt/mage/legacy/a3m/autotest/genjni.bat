@echo off
REM Generates a JNI wrapper for the autotest using SWIG.
REM Requires SWIG to be on your system path.
swig -v -c++ -java -o jni/autotest_wrap.cpp -outdir java/com/mediatek/a3m/autotest -package com.mediatek.a3m.autotest autotest_java.i
