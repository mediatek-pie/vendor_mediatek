/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/*
 * A3M Texture unit test.
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <a3m/texture2d.h>              /* for Texture2D                      */
#include <a3m/texturecube.h>            /* for TextureCube                    */
#include <texture2dloader.h>            /* for Texture2DLoader                */
#include <texturecubeloader.h>          /* for TextureCubeLoader              */
#include <autotest_common.h>            /* for AutotestTest                   */
#include <memfilesource_autotest.h>     /* for create_memfilesource_autotest  */

using namespace a3m;

namespace
{

  A3M_CHAR8 const* TEST_TEXTURES[] =
  {
    "autotest#test.bmp",
    "autotest#test.jpg",
    "autotest#test.png",
    "autotest#test.tga"
  };

  A3M_CHAR8 const* TEST_CUBE_MAP = "autotest#cube_map.png";

  A3M_UINT32 const TEST_SIZE = 2;

  /*
   * Test fixture.
   */
  struct A3mTexture2DTest : public AutotestTest
  {
    Texture2DCache::Ptr cache;

    A3mTexture2DTest()
    {
      cache.reset(new Texture2DCache());
      cache->registerLoader(Texture2DLoader::Ptr(new Texture2DLoader()));
      cache->registerSource(create_memfilesource_autotest());
    }

    ~A3mTexture2DTest()
    {
      cache->release();
    }
  };

  /*
   * Test fixture.
   */
  struct A3mTextureCubeTest : public AutotestTest
  {
    TextureCubeCache::Ptr cache;

    A3mTextureCubeTest()
    {
      cache.reset(new TextureCubeCache());
      cache->registerLoader(TextureCubeLoader::Ptr(new TextureCubeLoader()));
      cache->registerSource(create_memfilesource_autotest());
    }

    ~A3mTextureCubeTest()
    {
      cache->release();
    }
  };

} // namespace

/*
 * Validate loading of various texture formats.
 */
TEST_F(A3mTexture2DTest, LoadFormats)
{
  A3M_INT32 count = static_cast<A3M_INT32>(sizeof(TEST_TEXTURES) / sizeof(A3M_CHAR8*));

  for (A3M_INT32 i = 0; i < count; ++i)
  {
    A3M_LOG_INFO("Loading \"%s\"...", TEST_TEXTURES[i]);
    Texture2D::Ptr texture = cache->get(TEST_TEXTURES[i]);

    EXPECT_TRUE(texture);

    if (texture)
    {
      EXPECT_EQ(TEST_SIZE, texture->getWidth());
      EXPECT_EQ(TEST_SIZE, texture->getHeight());
    }
  }

  Texture2D::Ptr texture = cache->get("nonexistent-file");
  EXPECT_FALSE(texture);
}

/*
 * Validate loading of cube maps.
 */
TEST_F(A3mTextureCubeTest, Load)
{
  TextureCube::Ptr texture;

  texture = get(*cache,
                TEST_TEXTURES[0], TEST_TEXTURES[0],
                TEST_TEXTURES[0], TEST_TEXTURES[0],
                TEST_TEXTURES[0], TEST_TEXTURES[0]);

  EXPECT_TRUE(texture);

  texture = cache->get(TEST_CUBE_MAP);

  EXPECT_TRUE(texture);

  texture = get(*cache,
                TEST_TEXTURES[0], "nonexistent-file",
                TEST_TEXTURES[0], TEST_TEXTURES[0],
                TEST_TEXTURES[0], TEST_TEXTURES[0]);

  EXPECT_FALSE(texture);
}
