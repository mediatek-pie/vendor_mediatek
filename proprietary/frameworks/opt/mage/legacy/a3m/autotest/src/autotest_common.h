/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/*
 * A3M Autotest utility functions.
 */

#pragma once
#ifndef AUTOTEST_COMMON_H
#define AUTOTEST_COMMON_H

/******************************************************************************
 * Include Files
 ******************************************************************************/

#include <a3m/platform.h>           /* for A3M_PLATFORM                       */

// Only Windows supports heap checks and native context creation.
#if A3M_PLATFORM == A3M_WINDOWS

#include <crtdbg.h>                 /* for _Crt*() memory functions           */
#include <a3m/window.h>             /* for pssCreateWindow() etc.             */

#endif // A3M_PLATFORM == A3M_WINDOWS

#include <iostream>                 /* for std::ostream                       */
#include <gtest/gtest.h>            /* Google C++ Testing Framework           */
#include <a3m/angle.h>              /* for Anglef                             */
#include <a3m/assert.h>             /* for A3M_ASSERT                         */
#include <a3m/colour.h>             /* for Colour4f                           */
#include <a3m/matrix2.h>            /* for Matrix2f                           */
#include <a3m/matrix3.h>            /* for Matrix3f                           */
#include <a3m/matrix4.h>            /* for Matrix4f                           */
#include <a3m/quaternion.h>         /* for Quaternionf                        */
#include <a3m/renderdevice.h>       /* for RenderDevice                       */
#include <a3m/vector2.h>            /* for Vector2f                           */
#include <a3m/vector3.h>            /* for Vector3f                           */
#include <a3m/vector4.h>            /* for Vector4f                           */

// Windows supports context creation and memory leak checks.
#if A3M_PLATFORM == A3M_WINDOWS

/*
 * Object controlling the lifetime of an OpenGL context.
 */
class AutotestContext
{
public:
  AutotestContext() :
    m_window(a3m::createWindow("A3M Autotest", 480, 800, 32, 0))
  {
    A3M_ASSERT(m_window);
  }

private:
  a3m::Window::Ptr m_window;
};

/*
 * Checks the state of the heap on creation and destruction, and performs a
 * check to see whether there have been any memory leaks.
 */
class AutotestMemoryCheck
{
private:
  _CrtMemState m_oldState;

public:
  AutotestMemoryCheck()
  {
    _CrtMemCheckpoint(&m_oldState);
  }

  ~AutotestMemoryCheck()
  {
    _CrtMemState newState;
    _CrtMemState stateDiff;

    _CrtMemCheckpoint(&newState);
    int isDifferent = _CrtMemDifference(&stateDiff, &m_oldState, &newState);

    EXPECT_EQ(isDifferent, 0);
  }
};

#else

/*
 * In Android, the OpenGL context is created on the Java side.
 */
class AutotestContext
{
};

/*
 * No heap debugging facilities are available on Android.
 */
class AutotestMemoryCheck
{
};

#endif // A3M_PLATFORM == A3M_WINDOWS

/*
 * Base test class which automatically performs a memory leak check, if
 * supported.
 */
class AutotestTest : public testing::Test
{
public:
  ~AutotestTest()
  {
    // Check that there are no uncleared OpenGL errors remaining.
    EXPECT_EQ(a3m::RenderDevice::getError(), a3m::RenderDevice::NO_ERROR);
  }

  /*
   * Clears the GL error state.
   * This should be used whenever a test expects the GL error state to be set, as
   * the state is required to be cleared before each test finishes.
   */
  static void clearError()
  {
    a3m::RenderDevice::getError();
  }

private:
  AutotestMemoryCheck m_memoryCheck;
};

/*
 * Define the insertion operator for basic A3M types.
 */

inline std::ostream& operator<<(std::ostream& stream, a3m::Anglef const& value)
{
  stream << getDegrees(value) << " degrees";

  return stream;
}

inline std::ostream& operator<<(std::ostream& stream, a3m::Colour4f const& value)
{
  stream << "(" <<
         value.r << ", " <<
         value.g << ", " <<
         value.b << ", " <<
         value.a << ")";

  return stream;
}

inline std::ostream& operator<<(std::ostream& stream, a3m::Vector2f const& value)
{
  stream << "(" <<
         value.x << ", " <<
         value.y << ")";

  return stream;
}

inline std::ostream& operator<<(std::ostream& stream, a3m::Vector3f const& value)
{
  stream << "(" <<
         value.x << ", " <<
         value.y << ", " <<
         value.z << ")";

  return stream;
}

inline std::ostream& operator<<(std::ostream& stream, a3m::Vector4f const& value)
{
  stream << "(" <<
         value.x << ", " <<
         value.y << ", " <<
         value.z << ", " <<
         value.w << ")";

  return stream;
}

inline std::ostream& operator<<(std::ostream& stream, a3m::Matrix2f const& value)
{
  stream << "(" <<
         value.i << ", " <<
         value.j << ")";

  return stream;
}

inline std::ostream& operator<<(std::ostream& stream, a3m::Matrix3f const& value)
{
  stream << "(" <<
         value.i << ", " <<
         value.j << ", " <<
         value.k << ")";

  return stream;
}

inline std::ostream& operator<<(std::ostream& stream, a3m::Matrix4f const& value)
{
  stream << "(" <<
         value.i << ", " <<
         value.j << ", " <<
         value.k << ", " <<
         value.t << ")";

  return stream;
}

inline std::ostream& operator<<(std::ostream& stream, a3m::Quaternionf const& value)
{
  a3m::Vector3f axis = getAxis(value);
  a3m::Anglef angle = getAngle(value);

  stream << "(" <<
         value.a << ", " <<
         value.b << ", " <<
         value.c << ", " <<
         value.d << ") a.k.a. " <<
         angle << " around " <<
         axis;

  return stream;
}

#endif // AUTOTEST_COMMON_H
