/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2013. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/*
 * A3M ScopedPtr unit test.
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <a3m/pointer.h>                /* for ScopedPtr                      */
#include <autotest_common.h>            /* for AutotestTest                   */

using namespace a3m;

namespace
{

  class TestClass
  {
  public:
    TestClass(A3M_INT32* count = 0) :
      m_count(count)
    {
      if (m_count)
      {
        ++*m_count;
      }
    }

    ~TestClass()
    {
      if (m_count)
      {
        --*m_count;
      }
    }

  private:
    A3M_INT32* m_count;
  };

  typedef ScopedPtr<TestClass> TestPtr;

  /*
   * Test fixture.
   */
  struct A3mScopedPtrTest : public AutotestTest
  {
    A3mScopedPtrTest() :
      count(0),
      ptr(new TestClass(&count))
    {
    }

    A3M_INT32 count;
    TestPtr ptr;
  };

} // namespace

TEST_F(A3mScopedPtrTest, ConstructorDestructorGet)
{
  count = 0;

  {
    TestClass* raw = new TestClass(&count);
    TestPtr ptr(raw);

    EXPECT_EQ(raw, ptr.get());
    EXPECT_EQ(1, count);
  }

  EXPECT_EQ(0, count);

  TestPtr null;
  EXPECT_EQ(0, null.get());
}

TEST_F(A3mScopedPtrTest, Reset)
{
  EXPECT_EQ(1, count);
  ptr.reset(new TestClass(&count));
  EXPECT_EQ(1, count);
  ptr.reset();
  EXPECT_EQ(0, count);
}

TEST_F(A3mScopedPtrTest, Swap)
{
  TestPtr other(new TestClass(&count));
  EXPECT_EQ(2, count);

  TestClass* raw = ptr.get();
  TestClass* otherRaw = other.get();

  swap(ptr, other);

  EXPECT_EQ(otherRaw, ptr.get());
  EXPECT_EQ(raw, other.get());
  EXPECT_EQ(2, count);
}

TEST_F(A3mScopedPtrTest, Indirection)
{
  EXPECT_EQ(ptr.get(), &*ptr);
}

TEST_F(A3mScopedPtrTest, BoolTest)
{
  EXPECT_TRUE(ptr);
  ptr.reset();
  EXPECT_FALSE(ptr);
  ptr.reset(new TestClass());
  EXPECT_TRUE(ptr);

  TestPtr null;
  EXPECT_FALSE(null);
}
