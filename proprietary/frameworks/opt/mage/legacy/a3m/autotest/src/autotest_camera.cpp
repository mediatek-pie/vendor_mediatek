/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/*
 * A3M Camera unit test.
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <a3m/camera.h>                 /* for Camera                         */
#include <autotest_common.h>            /* for AutotestTest                   */

using namespace a3m;

namespace
{

  /*
   * Test fixture.
   */
  struct A3mCameraTest : public AutotestTest
  {
    Camera::Ptr camera;

    A3mCameraTest() :
      camera(new Camera())
    {
    }
  };

} // namespace

/*
 * Validate state after construction.
 */
TEST_F(A3mCameraTest, InitialState)
{
  EXPECT_FLOAT_EQ(60.0f, getDegrees(camera->getFov()));
  EXPECT_FLOAT_EQ(0.1f, camera->getNear());
  EXPECT_FLOAT_EQ(100.0f, camera->getFar());
}

/*
 * Test Camera::set/getFov().
 */
TEST_F(A3mCameraTest, Fov)
{
  camera->setFov(degrees(45.0f));
  EXPECT_FLOAT_EQ(45.0f, getDegrees(camera->getFov()));
}

/*
 * Test Camera::set/getNear().
 */
TEST_F(A3mCameraTest, Near)
{
  camera->setNear(10.0f);
  EXPECT_FLOAT_EQ(10.0f, camera->getNear());
}

/*
 * Test Camera::set/getFar().
 */
TEST_F(A3mCameraTest, Far)
{
  camera->setFar(10.0f);
  EXPECT_FLOAT_EQ(10.0f, camera->getFar());
}

/*
 * Verify projection matrix calculation.
 */
TEST_F(A3mCameraTest, Projection)
{
  a3m::Matrix4f projection;

  camera->setFov(degrees(45.0f));
  camera->setNear(1.0f);
  camera->setFar(50.0f);

  // Verify with aspect ratio of 2.
  camera->getProjection(&projection, 2.0f);

  EXPECT_NEAR(1.207f, projection.i.x, 0.001f);
  EXPECT_NEAR(2.414f, projection.j.y, 0.001f);
  EXPECT_NEAR(-1.041f, projection.k.z, 0.001f);
  EXPECT_NEAR(-2.041f, projection.t.z, 0.001f);

  // Verify with aspect ratio of 0.5.
  camera->getProjection(&projection, 0.5f);

  EXPECT_NEAR(2.414f, projection.i.x, 0.001f);
  EXPECT_NEAR(1.207f, projection.j.y, 0.001f);
  EXPECT_NEAR(-1.041f, projection.k.z, 0.001f);
  EXPECT_NEAR(-2.041f, projection.t.z, 0.001f);
}
