/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2013. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/*
 * A3M RenderContext unit test.
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <GLES2/gl2.h>                  /* for glGetIntegerv() etc.           */
#include <a3m/rendercontext.h>          /* for RenderContext                  */
#include <autotest_common.h>            /* for AutotestTest                   */
#include <memfilesource_autotest.h>     /* for create_memfilesource_autotest  */
#include <error.h>                      /* for CHECK_GL_ERROR                 */

using namespace a3m;

namespace
{

  /*
   * Test fixture.
   */
  struct A3mRenderContextTest : public AutotestTest
  {
    RenderContext::Ptr context;

    A3mRenderContextTest() :
      context(new RenderContext())
    {
    }
  };

} // namespace

TEST_F(A3mRenderContextTest, InitialState)
{
  EXPECT_FALSE(context->isBlendEnabled());
  EXPECT_EQ(Colour4f(0.0f, 0.0f, 0.0f, 0.0f), context->getBlendColour());
  EXPECT_EQ(BLEND_ONE, context->getSrcRgbBlendFactor());
  EXPECT_EQ(BLEND_ONE, context->getSrcAlphaBlendFactor());
  EXPECT_EQ(BLEND_ZERO, context->getDstRgbBlendFactor());
  EXPECT_EQ(BLEND_ZERO, context->getDstAlphaBlendFactor());
  EXPECT_EQ(BLEND_ADD, context->getRgbBlendFunction());
  EXPECT_EQ(BLEND_ADD, context->getAlphaBlendFunction());

  EXPECT_EQ(CULL_NONE, context->getCullingMode());
  EXPECT_EQ(WIND_CCW, context->getWindingOrder());
  EXPECT_EQ(1, context->getLineWidth());

  EXPECT_TRUE(context->getColourMaskR());
  EXPECT_TRUE(context->getColourMaskG());
  EXPECT_TRUE(context->getColourMaskB());
  EXPECT_TRUE(context->getColourMaskA());
  EXPECT_TRUE(context->isDepthWriteEnabled());
  EXPECT_FLOAT_EQ(0.0f, context->getDepthRangeNear());
  EXPECT_FLOAT_EQ(1.0f, context->getDepthRangeFar());
  EXPECT_FLOAT_EQ(0.0f, context->getDepthOffsetFactor());
  EXPECT_FLOAT_EQ(0.0f, context->getDepthOffsetUnits());
  EXPECT_FALSE(context->isDepthTestEnabled());
  EXPECT_EQ(DEPTH_LESS, context->getDepthTestFunction());

  A3M_INT32 scissorBox[4];
  glGetIntegerv(GL_SCISSOR_BOX, scissorBox);
  CHECK_GL_ERROR;

  EXPECT_FALSE(context->isScissorTestEnabled());
  EXPECT_EQ(scissorBox[0], context->getScissorBoxLeft());
  EXPECT_EQ(scissorBox[1], context->getScissorBoxBottom());
  EXPECT_EQ(scissorBox[2], context->getScissorBoxWidth());
  EXPECT_EQ(scissorBox[3], context->getScissorBoxHeight());

  EXPECT_FALSE(context->isStencilTestEnabled());

  for (A3M_INT32 i = 0; i < STENCIL_NUM_FACES; ++i)
  {
    StencilFace face = static_cast<StencilFace>(i);

    EXPECT_EQ(STENCIL_ALWAYS, context->getStencilFunction(face));
    EXPECT_EQ(0, context->getStencilReference(face));
    EXPECT_EQ(~0u, context->getStencilReferenceMask(face));

    EXPECT_EQ(STENCIL_KEEP, context->getStencilFail(face));
    EXPECT_EQ(STENCIL_KEEP, context->getStencilPassDepthFail(face));
    EXPECT_EQ(STENCIL_KEEP, context->getStencilPassDepthPass(face));
    EXPECT_EQ(~0u, context->getStencilMask(face));
  }

  A3M_INT32 viewport[4];
  glGetIntegerv(GL_VIEWPORT, viewport);
  CHECK_GL_ERROR;

  EXPECT_EQ(viewport[0], context->getViewportLeft());
  EXPECT_EQ(viewport[1], context->getViewportBottom());
  EXPECT_EQ(viewport[2], context->getViewportWidth());
  EXPECT_EQ(viewport[3], context->getViewportHeight());

  EXPECT_EQ(Colour4f(0.0f, 0.0f, 0.0f, 0.0f), context->getClearColour());
  EXPECT_EQ(1.0f, context->getClearDepth());
  EXPECT_EQ(0, context->getClearStencil());
}

TEST_F(A3mRenderContextTest, Blending)
{
  context->setBlendEnabled(A3M_TRUE);
  EXPECT_TRUE(context->isBlendEnabled());

  context->setBlendColour(Colour4f(0.1f, 0.2f, 0.3f, 0.4f));
  EXPECT_EQ(Colour4f(0.1f, 0.2f, 0.3f, 0.4f), context->getBlendColour());

  context->setBlendFactors(
    BLEND_CONSTANT_ALPHA, BLEND_DST_ALPHA, BLEND_DST_COLOUR, BLEND_SRC_ALPHA);
  EXPECT_EQ(BLEND_CONSTANT_ALPHA, context->getSrcRgbBlendFactor());
  EXPECT_EQ(BLEND_DST_ALPHA, context->getSrcAlphaBlendFactor());
  EXPECT_EQ(BLEND_DST_COLOUR, context->getDstRgbBlendFactor());
  EXPECT_EQ(BLEND_SRC_ALPHA, context->getDstAlphaBlendFactor());

  context->setBlendFunctions(BLEND_REVERSE_SUBTRACT, BLEND_SUBTRACT);
  EXPECT_EQ(BLEND_REVERSE_SUBTRACT, context->getRgbBlendFunction());
  EXPECT_EQ(BLEND_SUBTRACT, context->getAlphaBlendFunction());
}

TEST_F(A3mRenderContextTest, Primitives)
{
  context->setCullingMode(CULL_BACK);
  EXPECT_EQ(CULL_BACK, context->getCullingMode());

  context->setWindingOrder(WIND_CW);
  EXPECT_EQ(WIND_CW, context->getWindingOrder());

  context->setLineWidth(5);
  EXPECT_EQ(5, context->getLineWidth());
}

TEST_F(A3mRenderContextTest, ColourMask)
{
  context->setColourMask(A3M_FALSE, A3M_TRUE, A3M_TRUE, A3M_FALSE);
  EXPECT_FALSE(context->getColourMaskR());
  EXPECT_TRUE(context->getColourMaskG());
  EXPECT_TRUE(context->getColourMaskB());
  EXPECT_FALSE(context->getColourMaskA());
}

TEST_F(A3mRenderContextTest, Depth)
{
  context->setDepthWriteEnabled(A3M_TRUE);
  EXPECT_TRUE(context->isDepthWriteEnabled());

  context->setDepthRange(0.1f, 0.9f);
  EXPECT_FLOAT_EQ(0.1f, context->getDepthRangeNear());
  EXPECT_FLOAT_EQ(0.9f, context->getDepthRangeFar());

  context->setDepthOffset(0.2f, 0.8f);
  EXPECT_FLOAT_EQ(0.2f, context->getDepthOffsetFactor());
  EXPECT_FLOAT_EQ(0.8f, context->getDepthOffsetUnits());

  context->setDepthTestEnabled(A3M_TRUE);
  EXPECT_TRUE(context->isDepthTestEnabled());

  context->setDepthTestFunction(DEPTH_LEQUAL);
  EXPECT_EQ(DEPTH_LEQUAL, context->getDepthTestFunction());
}

TEST_F(A3mRenderContextTest, Scissor)
{
  context->setScissorTestEnabled(A3M_TRUE);
  EXPECT_TRUE(context->isScissorTestEnabled());

  context->setScissorBox(1, 2, 3, 4);
  EXPECT_EQ(1, context->getScissorBoxLeft());
  EXPECT_EQ(2, context->getScissorBoxBottom());
  EXPECT_EQ(3, context->getScissorBoxWidth());
  EXPECT_EQ(4, context->getScissorBoxHeight());
}

TEST_F(A3mRenderContextTest, Stencil)
{
  context->setStencilTestEnabled(A3M_TRUE);
  EXPECT_TRUE(context->isStencilTestEnabled());

  for (A3M_INT32 i = 0; i < STENCIL_NUM_FACES; ++i)
  {
    StencilFace face = static_cast<StencilFace>(i);

    context->setStencilFunction(face, STENCIL_LESS, i, i * 2);
    EXPECT_EQ(STENCIL_LESS, context->getStencilFunction(face));
    EXPECT_EQ(i, context->getStencilReference(face));
    EXPECT_EQ(i * 2u, context->getStencilReferenceMask(face));

    context->setStencilOperations(face, STENCIL_REPLACE, STENCIL_INVERT, STENCIL_ZERO);
    EXPECT_EQ(STENCIL_REPLACE, context->getStencilFail(face));
    EXPECT_EQ(STENCIL_INVERT, context->getStencilPassDepthFail(face));
    EXPECT_EQ(STENCIL_ZERO, context->getStencilPassDepthPass(face));

    context->setStencilMask(face, i * 3);
    EXPECT_EQ(i * 3u, context->getStencilMask(face));
  }
}

TEST_F(A3mRenderContextTest, Viewport)
{
  context->setViewport(5, 6, 7, 8);
  EXPECT_EQ(5, context->getViewportLeft());
  EXPECT_EQ(6, context->getViewportBottom());
  EXPECT_EQ(7, context->getViewportWidth());
  EXPECT_EQ(8, context->getViewportHeight());
}

TEST_F(A3mRenderContextTest, Clear)
{
  context->setClearColour(Colour4f(0.2f, 0.4f, 0.6f, 0.8f));
  EXPECT_EQ(Colour4f(0.2f, 0.4f, 0.6f, 0.8f), context->getClearColour());

  context->setClearDepth(2.1f);
  EXPECT_EQ(2.1f, context->getClearDepth());

  context->setClearStencil(832);
  EXPECT_EQ(832, context->getClearStencil());
}
