/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2013. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/*
 * A3M Version unit test.
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <vector>                       /* for std::vector                    */
#include <a3m/version.h>                /* for Version                        */
#include <autotest_common.h>            /* for AutotestTest                   */
#include <memfilesource_autotest.h>     /* for create_memfilesource_autotest  */

using namespace a3m;

namespace
{

  A3M_INT32 const VERSION_COUNT = 4;
  A3M_INT32 const EXTRA_COUNT = 2;

  A3M_CHAR8 const* EXTRA_STRINGS[] =
  {
    "",
    "foo",
    "bar"
  };

  /*
   * Test fixture.
   */
  struct A3mVersionTest : public AutotestTest
  {
  };

  void expectLt(Version const& a, Version const& b)
  {
    EXPECT_FALSE(a == b);
    EXPECT_TRUE(a != b);
    EXPECT_TRUE(a < b);
    EXPECT_FALSE(a > b);
    EXPECT_TRUE(a <= b);
    EXPECT_FALSE(a >= b);
  }

  void expectGt(Version const& a, Version const& b)
  {
    EXPECT_FALSE(a == b);
    EXPECT_TRUE(a != b);
    EXPECT_FALSE(a < b);
    EXPECT_TRUE(a > b);
    EXPECT_FALSE(a <= b);
    EXPECT_TRUE(a >= b);
  }

  void expectEq(Version const& a, Version const& b)
  {
    EXPECT_TRUE(a == b);
    EXPECT_FALSE(a != b);
    EXPECT_FALSE(a < b);
    EXPECT_FALSE(a > b);
    EXPECT_TRUE(a <= b);
    EXPECT_TRUE(a >= b);
  }

} // namespace

/*
 * Validate state after construction.
 */
TEST_F(A3mVersionTest, InitialState)
{
  Version version;

  EXPECT_EQ(0, version.getMajor());
  EXPECT_EQ(0, version.getMinor());
  EXPECT_EQ(0, version.getPatch());
  EXPECT_STREQ("", version.getExtra());
}

/*
 * Test comparison functions.
 */
TEST_F(A3mVersionTest, Comparison)
{
  A3M_INT32 count = VERSION_COUNT * VERSION_COUNT * VERSION_COUNT * EXTRA_COUNT;
  std::vector<Version> versions(count);

  // Create a range of Version values.
  A3M_INT32 c = 0;

  for (A3M_INT32 major = 0; major < VERSION_COUNT; ++major)
  {
    for (A3M_INT32 minor = 0; minor < VERSION_COUNT; ++minor)
    {
      for (A3M_INT32 patch = 0; patch < VERSION_COUNT; ++patch)
      {
        for (A3M_INT32 extra = 0; extra < EXTRA_COUNT; ++extra)
        {
          versions[c++] = Version(major, minor, patch, EXTRA_STRINGS[extra]);
        }
      }
    }
  }

  // Test cases where (a < b), (a > b) and (a == b).
  for (A3M_INT32 i = 0; i < count; ++i)
  {
    for (A3M_INT32 j = 0; j < count; ++j)
    {
      Version& a = versions[i];
      Version& b = versions[j];

      if (a.getMajor() < b.getMajor())
      {
        expectLt(a, b);
      }
      else if (a.getMajor() > b.getMajor())
      {
        expectGt(a, b);
      }
      else
      {
        if (a.getMinor() < b.getMinor())
        {
          expectLt(a, b);
        }
        else if (a.getMinor() > b.getMinor())
        {
          expectGt(a, b);
        }
        else
        {
          if (a.getPatch() < b.getPatch())
          {
            expectLt(a, b);
          }
          else if (a.getPatch() > b.getPatch())
          {
            expectGt(a, b);
          }
          else
          {
            expectEq(a, b);
          }
        }
      }
    }
  }
}
