/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/*
 * A3M VertexBuffer unit test.
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <a3m/renderdevice.h>           /* for RenderDevice                   */
#include <a3m/vertexbuffer.h>           /* for VertexBuffer                   */
#include <autotest_common.h>            /* for AutotestTest                   */

using namespace a3m;

namespace
{

  A3M_FLOAT const POSITION[] =
  {
    -0.5f, -0.5f, 0.0f,
    0.5f, -0.5f, 0.0f,
    0.5f, 0.5f, 0.0f,
    -0.5f, 0.5f, 0.0f
  };

  A3M_UINT8 const COLOUR[] =
  {
    255, 0, 0,
    0, 255, 0,
    0, 0, 255,
    255, 255, 255
  };

  /*
   * Test fixture.
   */
  struct A3mVertexArrayTest : public AutotestTest
  {
    VertexArray::Ptr position;
    VertexArray::Ptr colour;

    A3mVertexArrayTest() :
      position(new VertexArray(4, 3, POSITION)),
      colour(new VertexArray(4, 3, COLOUR))
    {
    }
  };

  /*
   * Test fixture.
   */
  struct A3mVertexBufferTest : public A3mVertexArrayTest
  {
    VertexBuffer::Ptr buffer;
    VertexBufferCache::Ptr cache;

    A3mVertexBufferTest()
    {
      cache.reset(new VertexBufferCache());
      buffer = cache->create();

      buffer->addAttrib(position, "a_position");
      buffer->addAttrib(
        colour,
        "a_colour",
        VertexBuffer::ATTRIB_FORMAT_UINT8,
        VertexBuffer::ATTRIB_USAGE_WRITE_MANY,
        A3M_TRUE);
    }
  };

} // namespace

/*
 * Validate state after construction.
 */
TEST_F(A3mVertexBufferTest, InitialState)
{
  EXPECT_TRUE(buffer);
}

/*
 * Test committing.
 */
TEST_F(A3mVertexBufferTest, Commit)
{
  buffer->commit();
  EXPECT_EQ(RenderDevice::NO_ERROR, RenderDevice::getError());
}

/*
 * Test enabling of attributes.
 */
TEST_F(A3mVertexBufferTest, EnableAttrib)
{
  EXPECT_TRUE(buffer->enableAttrib(0, "a_position"));
  EXPECT_TRUE(buffer->enableAttrib(0, "a_colour"));
  EXPECT_FALSE(buffer->enableAttrib(0, "a_invalid"));
}
