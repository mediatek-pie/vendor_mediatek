/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/*
 * A3M Background unit test.
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <GLES2/gl2.h>                  /* for glGetFloatv() etc.             */
#include <a3m/background.h>             /* for Background                     */
#include <a3m/rendercontext.h>          /* for RenderContext                  */
#include <autotest_common.h>            /* for AutotestTest                   */
#include <memfilesource_autotest.h>     /* for create_memfilesource_autotest  */

using namespace a3m;

namespace
{

  /*
   * Test fixture.
   */
  struct A3mBackgroundTest : public AutotestTest
  {
    Background background;
    RenderContext context;
  };

} // namespace

/*
 * Verify state after construction.
 */
TEST_F(A3mBackgroundTest, InitialState)
{
  EXPECT_EQ(Colour4f(0.0f, 0.0f, 0.0f, 0.0f), background.getColour());
  EXPECT_TRUE(background.getColourMaskR());
  EXPECT_TRUE(background.getColourMaskG());
  EXPECT_TRUE(background.getColourMaskB());
  EXPECT_TRUE(background.getColourMaskA());

  EXPECT_EQ(1.0f, background.getDepth());
  EXPECT_TRUE(background.getDepthMask());

  EXPECT_EQ(0.0f, background.getStencil());
  EXPECT_EQ(~0u, background.getStencilMask());
}

/*
 * Validate OpenGL state.
 */
TEST_F(A3mBackgroundTest, Colour)
{
  background.setColour(Colour4f(1.0f, 0.2f, 0.3f, 0.8f));
  background.enable(context);

  Colour4f glColour;
  glGetFloatv(GL_COLOR_CLEAR_VALUE, &glColour.r);
  EXPECT_EQ(glColour, background.getColour());
}

/*
 * Validate OpenGL state.
 */
TEST_F(A3mBackgroundTest, Depth)
{
  background.setDepth(0.4f);
  background.enable(context);

  // Compare against depth clear value returned by the GPU
  A3M_FLOAT glDepth;
  glGetFloatv(GL_DEPTH_CLEAR_VALUE, &glDepth);
  EXPECT_FLOAT_EQ(glDepth, background.getDepth());
}

/*
 * Validate OpenGL state.
 */
TEST_F(A3mBackgroundTest, Stencil)
{
  // First we need to get the size of the stencil buffer.
  GLint stencilBits;
  glGetIntegerv(GL_STENCIL_BITS, &stencilBits);

  A3M_UINT32 stencilValue = 0x12345678 >> (32 - stencilBits);
  background.setStencil(stencilValue);
  background.enable(context);

  GLint glStencil;
  glGetIntegerv(GL_STENCIL_CLEAR_VALUE, &glStencil);
  EXPECT_EQ(glStencil, background.getStencil());
}

/*
 * Validate OpenGL state.
 */
TEST_F(A3mBackgroundTest, ColourMask)
{
  background.setColourMask(A3M_FALSE, A3M_FALSE, A3M_TRUE, A3M_FALSE);
  background.enable(context);

  GLboolean glColourMask[4];
  glGetBooleanv(GL_COLOR_WRITEMASK, glColourMask);
  EXPECT_TRUE(glColourMask[0] == 0);
  EXPECT_TRUE(glColourMask[1] == 0);
  EXPECT_TRUE(glColourMask[2] != 0);
  EXPECT_TRUE(glColourMask[3] == 0);
}

/*
 * Validate OpenGL state.
 */
TEST_F(A3mBackgroundTest, DepthMask)
{
  background.setDepthMask(A3M_FALSE);
  background.enable(context);

  GLboolean glDepthMask;
  glGetBooleanv(GL_DEPTH_WRITEMASK, &glDepthMask);
  EXPECT_EQ(glDepthMask, 0);
}

/*
 * Validate OpenGL state.
 */
TEST_F(A3mBackgroundTest, StencilMask)
{
  background.setStencilMask(0);
  background.enable(context);

  GLint glStencilMask;
  glGetIntegerv(GL_STENCIL_WRITEMASK, &glStencilMask);
  EXPECT_EQ(
    static_cast<A3M_UINT32>(glStencilMask),
    background.getStencilMask());
}
