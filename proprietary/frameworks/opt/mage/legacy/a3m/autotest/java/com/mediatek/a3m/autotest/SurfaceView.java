/******************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ******************************************************************************/

package com.mediatek.a3m.autotest;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

public class SurfaceView extends GLSurfaceView {
    private boolean mRun;
    private boolean mFinished;
    private boolean mPassed;

    public SurfaceView(Context context) {
        super(context);
        initialize();
    }

    public SurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public void initialize() {
        setEGLContextClientVersion(2);
        setRenderer(new Renderer());
    }

    public boolean run() {
        mRun = true;

        while (!mFinished) {
        }

        return mPassed;
    }

    public class Renderer implements GLSurfaceView.Renderer {
        public void onDrawFrame(GL10 gl) {
            if (mRun) {
                mPassed = (0 == Autotest.run());
                mFinished = true;
            }
        }

        public void onSurfaceChanged(GL10 gl, int width, int height) {
        }

        public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        }
    }
}
