/******************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ******************************************************************************/

package com.mediatek.a3m.autotest;

import android.test.ActivityInstrumentationTestCase2;

public class Test extends ActivityInstrumentationTestCase2<Main> {
    public Test() {
        super(Main.class);
    }

    public void testAutotest() {
        boolean passed = getActivity().run();
        assertEquals(
            "A3M autotest failed; see junit-report.xml for details.",
            true, passed);
    }
}
