/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * J3M Texture interface.
 */

package com.mediatek.j3m;

/*!
 * \ingroup a3mJusrAppear
 * @{
 */
/**
 * Base Texture interface.
 */
public interface Texture {
    /**
     * Filter modes as defined in a3m::TextureParameters::FilterMode enum.
     *
     */
    public static class FilterMode {
        /** Choose nearest pixel in texture */
        public static final int NEAREST = 0;
        /** Interpolate between neighbouring pixels */
        public static final int LINEAR = 1;
        /** Choose nearest pixel from nearest mipmap */
        public static final int NEAREST_MIPMAP_NEAREST = 2;
        /** Choose nearest pixel from interpolation between two mipmaps */
        public static final int NEAREST_MIPMAP_LINEAR = 3;
        /** Interpolate between neighbouring pixels in nearest mipmap */
        public static final int LINEAR_MIPMAP_NEAREST = 4;
        /** Interpolate between neighbouring pixels in interpolation between two mipmaps */
        public static final int LINEAR_MIPMAP_LINEAR = 5;
    }

    /**
     * Wrap modes as defined in a3m::TextureParameters::WrapMode enum.
     *
     */
    public static class WrapMode {
        /** Repeat / wrap-around the texture when the edge is reached: ABCABC */
        public static final int REPEAT = 0;
        /** Use the edge pixel value after edge is reached: ABCCCC */
        public static final int CLAMP = 1;
        /** Repeat the texture in reverse order, when the edge is reached: ABCCBA */
        public static final int MIRROR = 2;
    }

    /**
     * Texture formats as defined in a3m::Texture::Format enum.
     *
     */
    public static class Format {
        /** 4 channel texture format: red, green, blue, alpha (transparency) */
        public static final int RGBA = 0;
        /** 3 channel texture format: red, green, blue colour only */
        public static final int RGB = 1;
        /** 2 channel texture format: luminance and alpha */
        public static final int LUMINANCE_ALPHA = 2;
        /** 1 channel texture format: luminance information only */
        public static final int LUMINANCE = 3;
        /** 1 channel texture format: transparency information only */
        public static final int ALPHA = 4;
        /** 1 channel texture format: depth information only */
        public static final int DEPTH = 5;
    }

    /**
     * Texture data types as defined in a3m::Texture::Type enum.
     *
     */
    public static class Type {
        /** texture packing of one byte per channel */
        public static final int UNSIGNED_BYTE = 0;
        /** texture packing of two bytes per channel, typically used for depth
         * maps */
        public static final int UNSIGNED_SHORT = 1;
        /** texture packing of 4 bits per channel (RGBA in two bytes) */
        public static final int UNSIGNED_SHORT_4_4_4_4 = 2;
        /** texture packing of RGB at 5 bits each, and 1 bit for alpha */
        public static final int UNSIGNED_SHORT_5_5_5_1 = 3;
        /** texture packing '565' - RGB in two bytes with 6 bits for green as
         * the eye is most sensitive to green levels */
        public static final int UNSIGNED_SHORT_5_6_5 = 4;
    }
}

/*! @} */
