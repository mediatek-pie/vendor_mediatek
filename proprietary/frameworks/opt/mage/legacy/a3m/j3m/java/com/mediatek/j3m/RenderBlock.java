/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * J3M RenderBlock interface.
 */

package com.mediatek.j3m;

/**
 * \ingroup a3mJusrRender
 * @{
 */
/**
 * A RenderBlock is used to associate a renderer, camera and scene.
 * Render blocks are the basic building block of the rendering pipeline.  A
 * render block simply takes a scene and uses a renderer to render it from a
 * camera's perspective.  An application might store several RenderBlocks (in a
 * RenderBlockGroup) to render different aspects (e.g. UI and 3D model) of the
 * application.  Different kinds of render block may exist in the future for
 * different purposes.
 */
public interface RenderBlock extends RenderBlockBase {
    /**
     * Sets the camera.
     *
     * @param camera the camera used when rendering the scene.
     */
    void setCamera(Camera camera);

    /**
     * Sets the portion of the screen to which to render.
     *
     * @param left left edge of the screen area
     * @param bottom bottom edge of the screen area
     * @param width width of the screen area
     * @param height height of the screen area
     */
    void setViewport(float left, float bottom, float width, float height);

    /**
     * Sets the background colour.
     * This is only used if the background clear mask is true (see
     * setColourClear).
     *
     * @param r Red colour component
     * @param g Green colour component
     * @param b Blue colour component
     * @param a Alpha colour component
     */
    void setBackgroundColour(float r, float g, float b, float a);

    /**
     * Sets flags used for filtering rendered nodes.
     * You may not want to render every single node in the scene in each render
     * block.  Flags allow filtering of the scene graph to selectively render
     * specific parts only.  A common flag would be a "VISIBLE" flag, which
     * would be used for convenience to universally disable an object when
     * rendering without having to remove it from the scene graph.  Another
     * might be a "CASTS_SHADOW" flag, which would only be taken into account
     * by a shadow rendering block.
     *
     * Flags can be marked as recursive, in which case all child nodes will
     * inherit the status of the flag when it changes to its non-default value
     * (e.g. VISIBLE would be true by default, but once a node's VISIBLE flag
     * becomes false, all of it's children are excluded by the filter,
     * irrespective of the state of their own VISIBLE flag).
     *
     * @param renderFlags Flags by which to filter the scene graph
     * @param recursiveFlags Flags which are marked as recursive
     */
    void setRenderFlags(FlagMask renderFlags, FlagMask recursiveFlags);

    /**
     * Sets the colour clear mask.
     * Call this method with true if you want the colour buffer to be
     * cleared before rendering. If you set the flag to false the colour
     * buffer will retain the values of the last RenderBlock to be rendered.
     *
     * By default the background is cleared to black.
     *
     * @param clear true to clear colour buffer.
     */
    void setColourClear(boolean clear);

    /**
     * Sets depth clear flag.
     * Call this method with true if you want the depth buffer to be cleared
     * before rendering. If you set the flag to false the depth buffer will
     * retain the values of the last RenderBlock to be rendered.
     *
     * By default the depth buffer is cleared before rendering.
     *
     * @param clear true to clear depth buffer.
     */
    void setDepthClear(boolean clear);

    /**
     * Sets a RenderTarget.
     * Call this method to use a RenderTarget instead of rendering directly to
     * the device (render to texture technique).
     *
     * @param target the Rendertarget to use.
     */
    void setRenderTarget(RenderTarget target);
}

/*! @} */
