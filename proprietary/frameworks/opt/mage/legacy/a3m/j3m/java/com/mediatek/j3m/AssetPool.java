/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * J3M AssetPool interface.
 */

package com.mediatek.j3m;

import android.content.res.AssetManager;
import android.content.res.Resources;

/* Extend JNI Overview */ /*! \addtogroup a3mJusr

<h2>A3M Asset Memory Management</h2>

3D models, scenes loaded from Glo3 files, bitmaps for textures, etc. are
collectively known as "assets".

The AssetPool class provides methods for identifying where files should be
loaded from (resource paths, file paths, etc.), identifying the location for a
file cache for A3M's use, and facilities for loading up 2D texture images.

2D texture images (class Texture2D) may be loaded from file (by specifying a
filename) or may be created programatically (by specifiying a size and a byte
array for the data).

*/

/*!
 * \ingroup a3mJusrAppear
 * @{
 */
/**
 * The AssetPool class - internal storage for assets.
 */
public interface AssetPool {
    /**
     * Registers a filesystem path as a source for loading assets.
     * @param path The path to add as a source
     */
    void registerSource(String path);

    /**
     * Registers an Android Resources container as a source for loading assets.
     * @param resources The Resources object to add as a source
     */
    void registerSource(Resources resources);

    /**
     * Registers an Android AssetManager as a source for loading assets.
     * @param assets The AssetManager to add as a source
     */
    void registerSource(AssetManager assets);

    /**
     * Sets the filesystem path in which certain type of assets are cached.
     * Setting this path will allow pre-processed assets to be cached, so that
     * they will load faster the next time they are required.  An example of a
     * cached asset is precompiled shader programs.
     * @param path
     */
    void setCacheSource(String path);

    /**
     * Flush the pool.
     * Flushing removes any assets from the pool which are not referenced
     * externally.
     */
    void flush();

    /**
     * Release the resources held by the pool.
     * It is important to call this function before the OpenGL context is
     * destroyed, as it will release ownership of all OpenGL assets back to the
     * context.  Not doing so can be problematic, as the context may be
     * destroyed before the pool.
     */
    void release();

    /**
     * Returns the ShaderProgram with the given name.
     * If the program has already been loaded into the pool, the existing
     * program is returned, otherwise, the program will be loaded and cached in
     * the pool for next time.
     *
     * @param name Name of the asset
     * @return Cached ShaderProgram object
     */
    ShaderProgram getShaderProgram(String name);

    /**
     * Returns the Texture2D with the given name.
     * If the texture has already been loaded into the pool, the existing
     * texture is returned, otherwise, the texture will be loaded and cached in
     * the pool for next time.
     *
     * @param name Name of the asset
     * @return Cached Texture2D object
     */
    Texture2D getTexture2D(String name);


    /**
     * Creates a Texture2D object from a block of pixel data.
     *
     * @param width Width of the texture in pixels
     * @param height Height of the texture in pixels
     * @param format Format of the pixel data
     * @param type Type of the pixel data
     * @param pixels Array of pixel data
     * @return New Texture2D object
     */
    Texture2D createTexture2D(
        int width, int height,
        int format, int type, byte[] pixels);

    /**
     * Creates a named Texture2D object from a block of pixel data.
     * The texture will be cached in the pool by name so that it can be
     * retrieved from the pool later on.
     *
     * @param width Width of the texture in pixels
     * @param height Height of the texture in pixels
     * @param format Format of the pixel data
     * @param type Type of the pixel data
     * @param pixels Array of pixel data
     * @param name Name to give the texture
     * @return New Texture2D object
     */
    Texture2D createTexture2D(
        int width, int height,
        int format, int type, byte[] pixels, String name);

    /**
     * Creates a named Texture2D object from a block of pixel data.
     * The texture will be cached in the pool by name so that it can be
     * retrieved from the pool later on.
     *
     * @param width Width of the texture in pixels
     * @param height Height of the texture in pixels
     * @param format Format of the pixel data
     * @param type Type of the pixel data
     * @param bitmap bitmap object
     * @return New Texture2D object
     */
    Texture2D createTexture2D(
        int width, int height,
        int format, int type, android.graphics.Bitmap bitmap);

    /**
     * Creates a Texture2D object from a graphic buffer.
     *
     * @param gb GraphicBuffer object that store the pixels
     * @return New Texture2D object
     */
    Texture2D createTexture2D(android.graphics.GraphicBuffer gb);

    /**
     * Returns the TextureCube with the given name.
     * If the texture has already been loaded into the pool, the existing
     * texture is returned, otherwise, the texture will be loaded and cached in
     * the pool for next time.
     *
     * @param name Name of the asset
     * @return Cached TextureCube object
     */
    TextureCube getTextureCube(String name);

    /**
     * Loads a named appearance file.
     * Unlike textures and programs, appearances are not cached in the pool,
     * and are loaded from file each time.
     *
     * @param name Name of the appearance
     * @return Appearance object
     */
    Appearance loadAppearance(String name);

    /**
     * Applies an appearance file to an existing appearance.
     * Appearance files can specify a selection of appearance attributes, and
     * this function can be used to apply those attributes over the top of an
     * already loaded appearance.
     *
     * @param appearance Appearance to which to apply settings
     * @param name Name of appearance file to apply
     */
    void applyAppearance(Appearance appearance, String name);

    /**
     * Loads a named model file.
     * Unlike textures and programs, models are not cached in the pool, and are
     * loaded from file each time.
     *
     * @param name Name of the model
     * @return Model object
     */
    Model loadModel(String name);

    /**
     * Loads a named model file, applying its appearance to a portion of the
     * scene graph.
     * This function can be useful when a model's geometry and animations are
     * divided between several files.  Geometry can be loaded into the scene
     * graph, and this function can be used to apply an animation afterwards.
     *
     * @param name Name of the model
     * @param scene Root node of scene to which to apply animation
     * @return Model object
     */
    Model loadModel(String name, SceneNode scene);

    /**
     * Returns the total amount of memory occupied by all Texture2Ds in bytes.
     * This figure is a best estimation, and might not be representative of the
     * exact amount of memory used by any particular OpenGL implementation.
     *
     * @return Memory usage estimation in bytes
     */
    int getTexture2DMemoryUsage();

    /**
     * Creates a square solid.
     * The square is of size 1x1 and points positively down the z-axis.
     *
     * @return Square solid
     */
    Solid createSquare();

    /**
     * Creates a square solid with UV scaling factors.
     * The square is of size 1x1 and points positively down the z-axis.
     *
     * @param scaleU Factor by which to scale the U texture coordinates
     * @param scaleV Factor by which to scale the V texture coordinates
     * @return Square solid
     */
    Solid createSquare(float scaleU, float scaleV);

    /**
     * Creates a cube solid.
     * The cube is of size 1x1x1.
     *
     * @return Cube solid
     */
    Solid createCube();

    /**
     * Creates a cube solid with UV scaling factors.
     * The cube is of size 1x1.
     *
     * @param scaleU Factor by which to scale the U texture coordinates
     * @param scaleV Factor by which to scale the V texture coordinates
     * @return Cube solid
     */
    Solid createCube(float scaleU, float scaleV);

    /**
     * Creates a sphere solid.
     * The sphere has a diameter of 1, and is divided vertically into a number
     * of segments and horizontally into a number of wedges.
     *
     * @param segmentCount Number of vertical segments
     * @param wedgeCount Number of horizontal wedges
     * @return Sphere solid
     */
    Solid createSphere(int segmentCount, int wedgeCount);

    /**
     * Creates a sphere solid with UV scaling factors.
     * The sphere has a diameter of 1, and is divided vertically into a number
     * of segments and horizontally into a number of wedges.
     *
     * @param segmentCount Number of vertical segments
     * @param wedgeCount Number of horizontal wedges
     * @param scaleU Factor by which to scale the U texture coordinates
     * @param scaleV Factor by which to scale the V texture coordinates
     * @return Sphere solid
     */
    Solid createSphere(
        int segmentCount, int wedgeCount,
        float scaleU, float scaleV);
}

/*! @} */
