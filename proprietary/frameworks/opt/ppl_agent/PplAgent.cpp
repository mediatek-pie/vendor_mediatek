/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#define MTK_LOG_ENABLE 1
#include "PplAgent.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <cutils/properties.h>
#include <utils/StrongPointer.h>

/* Has 3 type to store the ppl data:
 * 1. /data/nvram/dm/ppl_config:
 *   The origianl type, use nvram partition without write protection
 *   to write/read file.
 * 2. block device supplied by fs_mgr:
 *   For nvram partition with write protection on M MT6755, use this
 *   block to write/read raw data directly.
 * 3. /nvcfg/ppl/ppl_config:
 *   For nvram partition with write protection on & after M MT6797,
 *   use this ext4 partition to write/read file.
 */

using android::hardware::configureRpcThreadpool;
using android::hardware::joinRpcThreadpool;
using android::pplagent::PplAgent;

#ifdef MTK_WRITE_ON_PROTECTION
#ifndef RAW_DATA_PARTITION
#define CONTROL_DATA_ROOT_PATH "/mnt/vendor/nvcfg/ppl/"
#endif
#endif

#ifndef CONTROL_DATA_ROOT_PATH
#define CONTROL_DATA_ROOT_PATH "/mnt/vendor/nvdata/dm/"
#endif

#define CONTROL_DATA_FILE_PATH CONTROL_DATA_ROOT_PATH "ppl_config"

#define PPL_MNT_POINT "/ppl"

/* hard limit */
#define MAX_FILE_SIZE (4*1024)
#define BINDER_READ_NO_EXCEPTION (0)

#define STATUS_ENABLED 0x2
#define STATUS_LOCKED 0x4
#define STATUS_SIM_LOCKED 0x8

/// SECURITY:ALPS04103751 @{
#define SYSTEM_UID 1000
#define PHONE_UID  1001
/// @}

void convertVector2Array(std::vector<uint8_t> in, char* out) {
    int size = in.size();
    for(int i = 0; i < size; i++) {
        out[i] = in.at(i);
    }
}

void convertArray2Vector(const char* in, int len, std::vector<uint8_t>& out) {
    out.clear();
    for(int i = 0; i < len; i++) {
        out.push_back(in[i]);
    }
}


PplAgent::PplAgent() {
    ALOGI("PplAgent created start");
    readFstab();
    rawDataPath = getDataPath();
    ALOGI("PplAgent created end");
}

PplAgent::~PplAgent() {
    if (NULL != rawDataPath) {
        free(rawDataPath);
        rawDataPath = NULL;
    }
    ALOGI("PplAgent destroy end");
}

int PplAgent::isSupportWriteOnProtection() {
    #ifdef MTK_WRITE_ON_PROTECTION
        return 1;
    #endif
    return 0;
}

int PplAgent::isRawDataPartition() {
    #ifdef RAW_DATA_PARTITION
        return 1;
    #endif
    return 0;
}

Return<void> PplAgent::readControlData(readControlData_cb _hidl_cb) {
    ALOGI("readControlData enter");

    if (!isCallerUidHaveReadPermission()) {
        return Void();
    }

    int fd = -1;
    int32_t size = 0;
    char *buff = NULL;
    union sizeUnion {
        int size;
        char buf[4];
    } su;

    if (isSupportWriteOnProtection() && isRawDataPartition()) {
        if ((fd = open(rawDataPath, O_RDONLY)) != -1) {
            do {
                if (-1 == read(fd, su.buf, 4)) break;
                size = su.size;
                ALOGD("readControlData raw size = %d", size);
                buff = (char *) malloc(size);
                if (-1 == read(fd, buff, size)) break;
                close(fd);
                std::vector<uint8_t> tmp(size);
                convertArray2Vector(buff, size, tmp);
                _hidl_cb(tmp);
                return Void();// buff;
            } while (0);
            ALOGD("read raw data error = %s", strerror(errno));
            if (NULL != buff) {
                free(buff);
                buff = NULL;
            }
            close(fd);
            _hidl_cb({});
            return Void();//  NULL;
        } else {
            ALOGE("readControlData open raw failed:%d", fd);
            ALOGE("readControlData errno = %s", strerror(errno));
            _hidl_cb({});
            return Void();//  NULL;
        }
    }

    if (-1 == (fd = open(CONTROL_DATA_FILE_PATH, O_RDONLY))) {
        ALOGD("open control data file error = %s", strerror(errno));
        _hidl_cb({});
        return Void();//NULL;
    } else {
        // get file size
        struct stat file_stat;
        bzero(&file_stat, sizeof(file_stat));
        if (-1 == stat(CONTROL_DATA_FILE_PATH, &file_stat)) {
            ALOGD("stat control data file error = %s", strerror(errno));
            close(fd);
            _hidl_cb({});
            return Void();//NULL;
        }
        size = file_stat.st_size;
        buff = (char *) malloc(size);
        if (-1 == read(fd, buff, size)) {
            ALOGD("read ControlData error = %s", strerror(errno));
            free(buff);
            buff = NULL;
            close(fd);
            _hidl_cb({});
            return Void();//  NULL;
        }
        close(fd);
        std::vector<uint8_t> tmp(size);
        convertArray2Vector(buff, size, tmp);
        _hidl_cb(tmp);
        ALOGI("readControlData from file exit");
        return Void();//  buff;
    }
}

Return<int32_t> PplAgent::writeControlData(const hidl_vec<uint8_t>& data, int32_t size) {
    ALOGD("writeControlData enter. data size = %d", size);
    if (!isCallerUidHaveWritePermission()) {
        return 0;
    }
    if (data == NULL || size == 0 || size > MAX_FILE_SIZE) {
        return 0;
    }

    int fd = -1;
    char buff[size];
    convertVector2Array(data, buff);

    /* write data to raw */
    if (isSupportWriteOnProtection() && isRawDataPartition()) {
        if ((fd = open(rawDataPath, O_WRONLY | O_TRUNC)) >= 0) {
            write(fd, &size , 4);
            write(fd, buff, size);
            fsync(fd);
            close(fd);
            ALOGD("writeControlData to raw done");
            //wirte to file for debug
            if ((fd = open(CONTROL_DATA_FILE_PATH, O_CREAT | O_WRONLY | O_TRUNC, 0775)) >= 0) {
                write(fd, &size , 4);
                write(fd, buff, size);
                fsync(fd);
                close(fd);
                ALOGD("writeControlData to backup file exit");
            }
            return 1;
        } else {
            ALOGE("writeControlData open raw failed:%d", fd);
            ALOGE("writeControlData errno = %s", strerror(errno));
            return 0;
        }
    }

    if (-1 == (fd = open(CONTROL_DATA_FILE_PATH, O_CREAT | O_WRONLY | O_TRUNC, 0775))) {
        ALOGD("open control data file error = %s", strerror(errno));
        return 0;
    } else {
        write(fd, buff, size);
        fsync(fd);
        close(fd);
        FileOp_BackupToBinRegionForDM();
        ALOGI("writeControlData to file exit");
        return 1;
    }
}

Return<int32_t> PplAgent::needLock() {
    int fd = -1;
    int offset;

    if (isSupportWriteOnProtection() && isRawDataPartition()){
        if (-1 == (fd = open(rawDataPath, O_RDONLY))) {
            ALOGD("open control data raw error = %s", strerror(errno));
            return 0;
        }
        offset = 5; //size offset 4
    } else {
        if (-1 == (fd = open(CONTROL_DATA_FILE_PATH, O_RDONLY))) {
            ALOGD("open control data file error = %s", strerror(errno));
            return 0;
        }
        offset = 1;
    }

    // get ControlData.status which is at the second byte
    if (-1 == lseek(fd, offset, SEEK_SET)) {
        ALOGD("lseek %d byte error!", offset);
        close(fd);
        return 0;
    }
    char cstatus;
    if (-1 == read(fd, &cstatus, 1)) {
        ALOGD("read to get ControlData.status error = %s", strerror(errno));
        close(fd);
        return 0;
    }
    close(fd);

    int istatus = cstatus;
    ALOGD("istatus = %d\n", istatus);
    if ((istatus & STATUS_ENABLED) == STATUS_ENABLED
        && ((istatus & STATUS_LOCKED) == STATUS_LOCKED
        || (istatus & STATUS_SIM_LOCKED) == STATUS_SIM_LOCKED)) {
        return 1;
    } else {
        return 0;
    }
}

/// SECURITY:ALPS04103751 @{
bool PplAgent::isCallerUidHaveReadPermission() {
    hardware::IPCThreadState* ipcState = hardware::IPCThreadState::selfOrNull();
    if (ipcState == NULL) {
        ALOGE("isCallerUidHaveReadPermission, ipcState is null");
        return false;
    }
    int uid = ipcState->getCallingUid();
    // check read UID permission
    if (uid == SYSTEM_UID || uid == PHONE_UID) {
        return true;
    }
    ALOGE("isCallerUidHaveReadPermission Fail!(uid:%d)", uid);
    return false;
}

bool PplAgent::isCallerUidHaveWritePermission() {
    hardware::IPCThreadState* ipcState = hardware::IPCThreadState::selfOrNull();
    if (ipcState == NULL) {
        ALOGE("isCallerUidHaveWritePermission, ipcState is null");
        return false;
    }
    int uid = ipcState->getCallingUid();
    // check wirte UID permission
    if (uid == SYSTEM_UID) {
        return true;
    }
    ALOGE("isCallerUidHaveWritePermission Fail! (uid:%d)", uid);
    return false;
}
/// @}

int PplAgent::readFstab() {
    fstab = fs_mgr_read_fstab_default();
    if (NULL == fstab) {
        ALOGD("failed to open default");
        return -1;
    }
    return 0;
}

char* PplAgent::getDataPath() {
    struct fstab_rec *rec = NULL;
    char *source = NULL;

    ALOGD("getDataPath\n");
    rec = fs_mgr_get_entry_for_mount_point(fstab, PPL_MNT_POINT);
    if (NULL == rec) {
        ALOGE("failed to get entry for %s\n", PPL_MNT_POINT);
        return NULL;
    }

    asprintf(&source, "%s", rec->blk_device);
    ALOGD("getDataPath = %s\n", source);
    return source;
}

int PplAgent::freeFstab() {
    fs_mgr_free_fstab(fstab);
    return 0;
}

int main() {
    umask(000);
    ALOGD("CONTROL_DATA_ROOT_PATH = %s", CONTROL_DATA_ROOT_PATH);
    if (-1 == access(CONTROL_DATA_ROOT_PATH, F_OK)) {
        if (-1 == mkdir(CONTROL_DATA_ROOT_PATH, 0775)) {
            ALOGD("make control data path error = %s", strerror(errno));
            return 0;
        }
    }

    android::sp<IPplAgent> agent = new PplAgent();
    configureRpcThreadpool(1, true);
    status_t status = agent->registerAsService();
    if (status != android::OK) {
        ALOGW("Register FAILED. status=%d", status);
        return status;
    }

    joinRpcThreadpool();
    return 0;
}

