/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**
 * @file ap_monitor_defs.h
 *
 * @version 1.0
 *
 * Date 20160429
 *
 * @brief The header file declares KPI types and their corresponding structures
 *
 * @copyright MediaTek Inc. (C) 2015. All rights reserved.
 */

#ifndef _AP_MONITOR_DEFS_H_
#define _AP_MONITOR_DEFS_H_

#include <stdio.h>
#include <stdbool.h>

#ifdef  __cplusplus
extern "C" {
#endif

typedef enum {
    KPI_TYPE_INVALID = -1,
    KPI_TYPE_MDMI_BEGIN,
    KPI_TYPE_IP_OTA_PACKET,
    KPI_TYPE_IP_OTA_PACKET_WITH_PAYLOAD,
    KPI_TYPE_WIFI_IP_OTA_PACKET,
    KPI_TYPE_WIFI_IP_OTA_PACKET_WITH_PAYLOAD,
    KPI_TYPE_DNS_OTA_MESSAGE,
    KPI_TYPE_WIFI_DNS_OTA_MESSAGE,
    KPI_TYPE_WIFI_ISAKMP_OTA_MESSAGE,
    KPI_TYPE_CALL_EVENT_WITH_RAT,
    KPI_TYPE_SIP_OTA_PACKET,
    KPI_TYPE_WIFI_SIP_OTA_PACKET,
    KPI_TYPE_WIFI_RTP_OTA_PACKET,
    KPI_TYPE_VOLTE_CALL_EVENT,
    KPI_TYPE_LTE_TO_WIFI_HANDOVER_EVENT,
    KPI_TYPE_WIFI_TO_LTE_HANDOVER_EVENT,
    KPI_TYPE_EMBMS_SDCH_RECEPTION_EVENT,
    KPI_TYPE_EMBMS_SESSION_START_EVENT,
    KPI_TYPE_EMBMS_SESSION_STOP_EVENT,
    KPI_TYPE_EMBMS_REJOIN_SERVICE_TIMER_EXPIRED_EVENT,
    KPI_TYPE_EMBMS_OUT_SERVICE_AREA_EVENT,
    KPI_TYPE_EMBMS_IN_SERVICE_AREA_EVENT,
    KPI_TYPE_EMBMS_LATE_SEGMENT_ARRIVAL_EVENT,
    KPI_TYPE_EMBMS_DASH_HTTP_ERROR_EVENT,
    KPI_TYPE_EMBMS_FILE_DOWNLOAD_COMPLETE_EVENT,
    KPI_TYPE_EMBMS_DASH_CLIENT_LATE_REQ_EVENT,
    KPI_TYPE_EMBMS_BOOT_CONFIG_FILE_EVENT,
    KPI_TYPE_EMBMS_SA_FILE_EVENT,
    KPI_TYPE_EMBMS_REGISTERED_APP_LIST_EVENT,
    KPI_TYPE_EMBMS_SERVICE_ANNOUNCEMENT_EVENT,
    KPI_TYPE_EMBMS_APP_REGISTERED_EVENT,
    KPI_TYPE_EMBMS_OFFSET_TIME_FR_EVENT,
    KPI_TYPE_EMBMS_RANDOM_TIME_PERIOD_FR_EVENT,
    KPI_TYPE_EMBMS_OFFSET_TIME_RR_EVENT,
    KPI_TYPE_EMBMS_RANDOM_TIME_PERIOD_RR_EVENT,
    KPI_TYPE_EMBMS_CHANNEL_CHANGE_TIME_EVENT,
    KPI_TYPE_EMBMS_SERVICE_QUALITY,
    KPI_TYPE_WIFI_INFO,
    KPI_TYPE_WIFI_INFO_HLOS,
    KPI_TYPE_WIFI_INTERNET_CONN_STATUS,
    KPI_TYPE_WIFI_SCAN_EVENT,
    KPI_TYPE_WIFI_ASSOCIATION_EVENT,
    KPI_TYPE_WIFI_REASSOCIATION_EVENT,
    KPI_TYPE_WIFI_DISCONNECTION_EVENT,
    KPI_TYPE_WIFI_AUTHENTICATION_EVENT,
    KPI_TYPE_WIFI_DEAUTHENTICATION_EVENT,
    KPI_TYPE_WIFI_POWER_SAVE_EVENT,
    KPI_TYPE_WIFI_DHCP_POWER_FAILURE_EVENT,
    KPI_TYPE_WIFI_DNS_QUERY_EVENT,
    KPI_TYPE_SIP_REGISTRATION_EVENT,
    KPI_TYPE_SIP_PUBLISH_EVENT,
    KPI_TYPE_SIP_SUBSCRIPTION_EVENT,
    KPI_TYPE_VOLTE_CALL_STATE_CHANGE_EVENT,
    KPI_TYPE_WIFI_DPD_INTERNAL_EVENT,
    KPI_TYPE_WIFI_NAT_KEEP_ALIVE_EVENT,
    KPI_TYPE_SUPL_STATISTICS,
    KPI_TYPE_SUPL_MESSAGE,
    KPI_TYPE_MDMI_END,
    KPI_TYPE_MDMI_TOTAL = KPI_TYPE_MDMI_END - KPI_TYPE_MDMI_BEGIN - 1,
    KPI_TYPE_MAPI_BEGIN = 1000,
    KPI_TYPE_GPS_LOCATION_INFO,
    KPI_TYPE_MAPI_END,
    KPI_TYPE_MAPI_TOTAL = KPI_TYPE_MAPI_END - KPI_TYPE_MAPI_BEGIN - 1,
    KPI_TYPE_MAX = KPI_TYPE_MDMI_TOTAL + KPI_TYPE_MAPI_TOTAL,  //define new kpi type above this enum
} KPI_TYPE;

typedef struct ApmOtaPacket {
    int     ip_version; /* 1 for Ipv4, 2 for Ipv6 */
    int     direction;  /* 1 for upstream, 2 for downstream */
    uint8_t *payload;   /* Payload buffer */
    size_t  length;     /* length of payload */
} ApmOtaPacket;

/* structure for KPI_TYPE_IP_OTA_PACKET */
typedef ApmOtaPacket ApmIpOtaPacket;

/* structure for KPI_TYPE_IP_OTA_PACKET_WITH_PAYLOAD */
typedef ApmOtaPacket ApmIpOtaPacketWithPayload;

/* structure for KPI_TYPE_WIFI_IP_OTA_PACKET */
typedef ApmOtaPacket ApmWifiIpOtaPacket;

/* structure for KPI_TYPE_WIFI_IP_OTA_PACKET_WITH_PAYLOAD */
typedef ApmOtaPacket ApmWifiIpOtaPacketWithPayload;

/* structure for KPI_TYPE_DNS_OTA_MESSAGE */
typedef ApmOtaPacket ApmDnsOtaPacket;

/* structure for KPI_TYPE_WIFI_DNS_OTA_MESSAGE */
typedef ApmOtaPacket ApmWifiDnsOtaPacket;

/* structure for KPI_TYPE_WIFI_ISAKMP_OTA_MESSAGE */
typedef ApmOtaPacket ApmWifiISAKMPOtaPacket;

/* structure for KPI_TYPE_CALL_EVENT_WITH_RAT */
typedef struct {
    int callStatus;
    int callType;
    int ratInfo;
    int callDirection;
} ApmCallEventWithRAT;

/* structure for KPI_TYPE_SIP_OTA_PACKET */
typedef struct {
    int     direction;  /* 1 for upstream, 2 for downstream */
    uint8_t *payload;   /* Includes IP header , TCP/UDP header, SIP payload plus options */
    size_t  length;     /* length of payload */
} ApmSipOtaPacket;

/* structure for KPI_TYPE_WIFI_SIP_OTA_PACKET */
typedef ApmSipOtaPacket ApmWifiSipOtaPacket;

/* structure for KPI_TYPE_WIFI_RTP_OTA_PACKET */
typedef ApmSipOtaPacket ApmWifiRtpOtaPacket;

/* structure for KPI_TYPE_VOLTE_CALL_EVENT */
typedef struct {
    int callStatus;     /* 0 for inactive, 1 for active */
    int callType;
} ApmVolteCallEvent;

typedef enum {
    ApmLteToWifiHandover_PR_NOTHING,        /* No components present */
    ApmLteToWifiHandover_PR_handoverAttempt,
    ApmLteToWifiHandover_PR_handoverResult
} ApmLteToWifiHandover_PR;

typedef enum {
    Apmresult_PR_NOTHING,   /* No components present */
    Apmresult_PR_success,
    Apmresult_PR_failure
} Apmresult_PR;

typedef struct {
    uint8_t     *accessPointName;
    size_t                  accessPointNameLength;
    int                     callMode;
} ApmLteToWifiHOSuccess;

typedef enum {
    ApmLteToWifiHOFailure_PR_NOTHING,   /* No components present */
    ApmLteToWifiHOFailure_PR_ipsecTimeoutFailure,
    ApmLteToWifiHOFailure_PR_ipsecFailureErrorCode
} ApmLteToWifiHOFailure_PR;

typedef struct {
    ApmLteToWifiHOFailure_PR    present;
    int                         ipsecFailureErrorCode;      /* Fill when present = ApmLteToWifiHOFailure_PR_ipsecFailureErrorCode */
} ApmLteToWifiHOFailure;

typedef struct {
    uint8_t     *epdgIpAddress;
    size_t    epdgIpAddressLength;
    struct {
        Apmresult_PR        present;
        union {
            ApmLteToWifiHOSuccess    success;               /* Fill when present = Apmresult_PR_success */
            ApmLteToWifiHOFailure    failure;               /* Fill when present = Apmresult_PR_failure */
        } choice;
    } result;
} ApmLteToWifiHOResult;

/* structure for KPI_TYPE_LTE_TO_WIFI_HANDOVER_EVENT */
typedef struct {
    ApmLteToWifiHandover_PR     present;
    ApmLteToWifiHOResult        handoverResult;
} ApmLteToWifiHandover;

typedef enum {
    ApmWifiToLteHandover_PR_NOTHING,    /* No components present */
    ApmWifiToLteHandover_PR_handoverAttempt,
    ApmWifiToLteHandover_PR_handoverResult
} ApmWifiToLteHandover_PR;

typedef struct {
    uint8_t     *accessPointName;
    size_t      accessPointNameLength;
    int         callMode;
} ApmWifiToLteHOSuccess;

typedef struct {
    uint8_t     *epdgIpAddress;
    size_t      epdgIpAddressLength;
    struct {
        Apmresult_PR present;
        union {
            ApmWifiToLteHOSuccess   success;       /* Fill when present = Apmresult_PR_success */
            int                     failure;       /* Fill when present = Apmresult_PR_failure */
        } choice;
    } result;
} ApmWifiToLteHOResult;

/* structure for KPI_TYPE_WIFI_TO_LTE_HANDOVER_EVENT */
typedef struct {
    ApmWifiToLteHandover_PR present;
    ApmWifiToLteHOResult     handoverResult;        /* Fill when present = ApmWifiToLteHandover_PR_handoverResult */
} ApmWifiToLteHandover;

typedef struct {
    unsigned int     year;
    unsigned int     month;
    unsigned int     day;
    unsigned int     hour;
    unsigned int     minute;
    unsigned int     second;
    unsigned int     millisecond;
} ApmTimeStamp;

/* structure for KPI_TYPE_EMBMS_SDCH_RECEPTION_EVENT */
typedef struct {
    ApmTimeStamp  actualTime;
} ApmSDCHReceptionEvent;

/* structure for KPI_TYPE_EMBMS_SESSION_START_EVENT */
typedef struct {
    ApmTimeStamp    actualTime;
    uint8_t         *tmgi;
    size_t          tmgiLength;
} ApmSessionStartEvent;

/* structure for KPI_TYPE_EMBMS_SESSION_STOP_EVENT */
typedef ApmSessionStartEvent ApmSessionStopEvent;

/* structure for KPI_TYPE_EMBMS_REJOIN_SERVICE_TIMER_EXPIRED_EVENT */
typedef ApmSDCHReceptionEvent ApmRejoinServiceTimerExpiredEvent;

/* structure for KPI_TYPE_EMBMS_OUT_SERVICE_AREA_EVENT */
typedef ApmSDCHReceptionEvent ApmOutServiceAreaEvent;

/* structure for KPI_TYPE_EMBMS_IN_SERVICE_AREA_EVENT */
typedef ApmSDCHReceptionEvent ApmInServiceAreaEvent;

/* structure for KPI_TYPE_EMBMS_LATE_SEGMENT_ARRIVAL_EVENT */
typedef struct {
    long        lateSegmentArrivalTime;
    uint8_t     *tmgi;
    size_t      tmgiLength;
} ApmLateSegmentArrivalEvent;

/* structure for KPI_TYPE_EMBMS_DASH_HTTP_ERROR_EVENT */
typedef struct {
    int         httpErrorCode;
    uint8_t     *tmgi;
    size_t      tmgiLength;
} ApmDashHttpErrorEvent;

/* structure for KPI_TYPE_EMBMS_FILE_DOWNLOAD_COMPLETE_EVENT */
typedef struct {
    uint8_t*    fileName;
    size_t      fileNameLength;
    bool        success;
    uint8_t     *tmgi;
    size_t      tmgiLength;
} ApmFileDownloadCompleteEvent;

/* structure for KPI_TYPE_EMBMS_DASH_CLIENT_LATE_REQ_EVENT */
typedef struct {
    long     timeDifference;
    uint8_t     *tmgi;
    size_t      tmgiLength;
} ApmDashClientLateRequestEvent;

/* structure for KPI_TYPE_EMBMS_BOOT_CONFIG_FILE_EVENT */
typedef struct {
    uint8_t *bootConfigFileContent;
    size_t  bootConfigFileContentLength;
} ApmBootConfigFileEvent;

/* structure for KPI_TYPE_EMBMS_SA_FILE_EVENT */
typedef struct {
    uint8_t *saFile;
    size_t  saFileLength;
} ApmSAFileEvent;

typedef struct ApmAppInstanceIDNode {
    uint8_t*                    appInstanceID;
    size_t                      appInstanceIDLength;
    struct ApmAppInstanceIDNode *next;
} ApmAppInstanceIDNode;

/* structure for KPI_TYPE_EMBMS_REGISTERED_APP_LIST_EVENT */
typedef struct {
    ApmAppInstanceIDNode    *registeredAppList;    /* Linked list of AppInstanceID */
} ApmRegisteredAppListEvent;

typedef struct ApmEarfcnNode {
    int                     earfcn;
    struct ApmEarfcnNode    *next;
} ApmEarfcnNode;

typedef struct ApmTmgiNode {
    uint8_t             *tmgi;
    size_t              tmgiLength;
    struct ApmTmgiNode  *next;
} ApmTmgiNode;

typedef struct ApmSaiNode {
    int                 sai;
    struct ApmSaiNode   *next;
} ApmSaiNode;

typedef struct ApmServiceClassNode {
    uint8_t                     *serviceClass;
    size_t                      serviceClassLength;
    struct ApmServiceClassNode  *next;
} ApmServiceClassNode;

typedef struct ApmServiceAnnouncementSessionNode {
    uint8_t                                     *serviceID;
    size_t                                      serviceIDLength;
    ApmEarfcnNode                               *frequencyList;     /* Linked list of Earfcn */
    ApmTmgiNode                                 *tmgiList;          /* Linked list of Tmgi */
    ApmSaiNode                                  *saiList;           /* Linked list of Sai */
    ApmServiceClassNode                         *serviceClassList;  /* Linked list of Service class */
    struct ApmServiceAnnouncementSessionNode    *next;
} ApmServiceAnnouncementSessionNode;

/* structure for KPI_TYPE_EMBMS_SERVICE_ANNOUNCEMENT_EVENT */
typedef ApmServiceAnnouncementSessionNode ApmServiceAnnouncementEvent;

/* structure for KPI_TYPE_EMBMS_APP_REGISTERED_EVENT */
typedef struct {
    ApmAppInstanceIDNode    *appRegisteredEvent;   /* Linked list of AppInstanceID */
} ApmAppRegisteredEvent;

/* structure for KPI_TYPE_EMBMS_OFFSET_TIME_FR_EVENT */
typedef struct {
    int         offsetTimeForFileRepair;
    uint8_t     *tmgi;
    size_t      tmgiLength;
} ApmOffsetTimeFREvent;

/* structure for KPI_TYPE_EMBMS_RANDOM_TIME_PERIOD_FR_EVENT */
typedef struct {
    int         randomTimePeriodForFileRepair;
    uint8_t     *tmgi;
    size_t      tmgiLength;
} ApmRandomTimePeriodFREvent;

/* structure for KPI_TYPE_EMBMS_OFFSET_TIME_RR_EVENT */
typedef struct {
    int         offsetTimeforReceptionReport;
    uint8_t     *tmgi;
    size_t      tmgiLength;
} ApmOffsetTimeRREvent;

/* structure for KPI_TYPE_EMBMS_RANDOM_TIME_PERIOD_RR_EVENT */
typedef struct {
    int         randomTimePeriodForReceptionReport;
    uint8_t     *tmgi;
    size_t      tmgiLength;
} ApmRandomTimePeriodRREvent;

/* structure for KPI_TYPE_EMBMS_CHANNEL_CHANGE_TIME_EVENT */
typedef struct {
    int         channelChangeTime;
    uint8_t     *tmgi;
    size_t      tmgiLength;
} ApmChannelChangeTimeEvent;

/* structure for KPI_TYPE_EMBMS_SERVICE_QUALITY */
typedef struct {
    int     totalSegments;
    int     streamFECFailureRate;
    int     fileFECFailureRate;
    int     totalSegmentsLost;
    uint8_t     *tmgi;
    size_t      tmgiLength;
} ApmServiceQuality;

/* structure for KPI_TYPE_WIFI_INFO */
typedef struct {
    uint8_t     *bssid;
    size_t      bssidLength;
    uint8_t     *ssid;
    size_t      ssidLength;
    uint8_t     *rssi;
    size_t      rssiLength;
    uint8_t     *detailState;
    size_t      detailStateLength;
    uint8_t     *supplicantState;
    size_t      supplicantStateLength;
    uint8_t     *ip;
    size_t      ipLength;
    int         linkSpeed;
    uint8_t     *macAddress;
    size_t      macAddressLength;
    uint8_t     *networkID;
    size_t      networkIDLength;
    uint8_t     *hiddenSSID;
    size_t      hiddenSSIDLength;
    uint8_t     *gateway;
    size_t      gatewayLength;
    uint8_t     *netMask;
    size_t      netMaskLength;
    uint8_t     *dns1;
    size_t      dns1Length;
    uint8_t     *dns2;
    size_t      dns2Length;
    uint8_t     *serverIP;
    size_t      serverIPLength;
} ApmWifiInfo;

/* structure for KPI_TYPE_WIFI_INFO_HLOS */
typedef ApmWifiInfo ApmWifiInfoHLOS;

typedef enum {
    ApmWifiInternetConnectionStatus_PR_NOTHING,    /* No components present */
    ApmWifiInternetConnectionStatus_PR_noWifiInternetConnection,
    ApmWifiInternetConnectionStatus_PR_wifiInternetConnected
} ApmWifiInternetConnectionStatus_PR;

/* structure for KPI_TYPE_WIFI_INTERNET_CONN_STATUS */
typedef struct {
    ApmWifiInternetConnectionStatus_PR  present;
    int                                 linkSpeed; /* Fill when present = ApmWifiInternetConnectionStatus_PR_wifiInternetConnected */
} ApmWifiInternetConnectionStatus;

typedef struct ApmWifiChannelNode {
    int                         wifiFrequency;
    int                         channelNumber;
    struct ApmWifiChannelNode   *next;
} ApmWifiChannelNode;

typedef struct {
    int     wifiFrequency;
    int     channelNumber;
} ApmWifiChannel;

typedef struct {
    int     channelWidth;
    int     secondaryChannelOffset;
} ApmHTCapability;

typedef struct ApmRateNode {
    int                 rate;
    struct ApmRateNode  *next;
} ApmRateNode;

typedef struct ApmWifiAccessPointNode {
    uint8_t                         *bssid;
    size_t                          bssidLength;
    uint8_t                         *ssid;
    size_t                          ssidLength;
    ApmWifiChannel                  operationChannel;
    ApmRateNode                     *supportedRates; /* Linked list of Rate */
    int                             security;
    bool                            qosCapability;
    int                             countryInfo;
    ApmHTCapability                 htCapability;
    struct ApmWifiAccessPointNode   *next;
} ApmWifiAccessPointNode;

/* structure for KPI_TYPE_WIFI_SCAN_EVENT */
typedef struct {
    ApmTimeStamp            timeScanStart;
    ApmTimeStamp            timeScanEnd;
    ApmWifiChannelNode      *scannedChannelList; /* Linked list of WifiChannel */
    int                     scanType;
    ApmWifiAccessPointNode  *scannedAccessPoints; /* Linked list of WifiAccessPoint */
} ApmWifiScanEvent;

/* structure for KPI_TYPE_WIFI_ASSOCIATION_EVENT */
typedef struct {
    uint8_t     *bssid;
    size_t      bssidLength;
    uint8_t     *ssid;
    size_t      ssidLength;
    int         result; /* 0 = success, 1 = failure */
} ApmWifiAssociationEvent;

/* structure for KPI_TYPE_WIFI_REASSOCIATION_EVENT */
typedef ApmWifiAssociationEvent ApmWifiReAssociationEvent;

/* structure for KPI_TYPE_WIFI_DISCONNECTION_EVENT */
typedef struct {
    uint8_t     *bssid;
    size_t      bssidLength;
    uint8_t     *ssid;
    size_t      ssidLength;
    int         reasonCode;
} ApmWifiDisconnectionEvent;

/* structure for KPI_TYPE_WIFI_AUTHENTICATION_EVENT */
typedef struct {
    uint8_t     *bssid;
    size_t      bssidLength;
    int         result; /* 0 = success, 1 = failure */
} ApmWifiAuthenticationEvent;

/* structure for KPI_TYPE_WIFI_DEAUTHENTICATION_EVENT */
typedef struct {
    uint8_t     *bssid;
    size_t      bssidLength;
    int         reasonCode;
} ApmWifiDeAuthenticationEvent;

/* structure for KPI_TYPE_WIFI_POWER_SAVE_EVENT */
typedef int ApmWifiPowerSaveEvent;

/* structure for KPI_TYPE_WIFI_DHCP_POWER_FAILURE_EVENT */
typedef struct {
    uint8_t     *failureReason;
    size_t      failureReasonLength;
} ApmWifiDhcpFailureEvent;

typedef enum {
    ApmWifiDnsQueryEvent_PR_NOTHING,   /* No components present */
    ApmWifiDnsQueryEvent_PR_dnsQuery,
    ApmWifiDnsQueryEvent_PR_dnsResult
} ApmWifiDnsQueryEvent_PR;

/* structure for KPI_TYPE_WIFI_DNS_QUERY_EVENT */
typedef struct {
    ApmWifiDnsQueryEvent_PR present;
    bool dnsResult; /* Fill when present = ApmWifiDnsQueryEvent_PR_dnsResult */
} ApmWifiDnsQueryEvent;

/* structure for KPI_TYPE_SIP_REGISTRATION_EVENT */
typedef struct {
    int     registrationType;
    int     registrationResult;
} ApmSipRegistrationEvent;

typedef enum {
    ApmSipPublishEvent_PR_NOTHING,              /* No components present */
    ApmSipPublishEvent_PR_publishNotAvailable,
    ApmSipPublishEvent_PR_publishAvailable
} ApmSipPublishEvent_PR;

/* structure for KPI_TYPE_SIP_PUBLISH_EVENT */
typedef struct {
    ApmSipPublishEvent_PR   present;
    int                     publishAvailable;   /* Fill when present = ApmSipPublishEvent_PR_publishAvailable */
} ApmSipPublishEvent;

typedef enum {
    ApmSipSubscriptionEvent_PR_NOTHING,                 /* No components present */
    ApmSipSubscriptionEvent_PR_subscriptionNotAvalable,
    ApmSipSubscriptionEvent_PR_subscriptionAvailable
} ApmSipSubscriptionEvent_PR;

typedef struct ApmEventTypeNode {
    uint8_t                     *eventType;
    size_t                      eventTypeLength;
    struct ApmEventTypeNode     *next;    /* Linked list for EventType */
} ApmEventTypeNode;

typedef struct {
    ApmEventTypeNode    *eventTypes;
    int                 subscriptionResult;
} ApmSubscriptionAvailable;

/* structure for KPI_TYPE_SIP_SUBSCRIPTION_EVENT */
typedef struct {
    ApmSipSubscriptionEvent_PR  present;
    ApmSubscriptionAvailable     subscriptionAvailable;     /* Fill when present = ApmSipSubscriptionEvent_PR_subscriptionAvailable */
} ApmSipSubscriptionEvent;

/* structure for KPI_TYPE_VOLTE_CALL_STATE_CHANGE_EVENT */
typedef struct {
    int     state;
    int     cause;
} ApmVolteCallStateChangeEvent;

/* structure for KPI_TYPE_WIFI_DPD_INTERNAL_EVENT */
typedef struct {
    int     dpdInterval;
} ApmWifiDpdIntervalEvent;

/* structure for KPI_TYPE_WIFI_NAT_KEEP_ALIVE_EVENT */
typedef struct {
    ApmTimeStamp    natKeepAliveTimestamp;
} ApmWifiNatKeepAliveEvent;

/* structure for KPI_TYPE_VOLTE_IP_OTA_MESSAGE */
typedef ApmOtaPacket ApmVolteIpOtaPacket;

/* structure for KPI_TYPE_NON_VOLTE_IP_OTA_MESSAGE */
typedef ApmOtaPacket ApmNonVolteIpOtaPacket;

/* structure for KPI_TYPE_GPS_LOCATION_INFO */
typedef struct ApmGpsLocationInfo {
    float               distance;
    float atimuth;
    float dopplerSpeed;
} ApmGpsLocationInfo;

/* structure for KPI_TYPE_SUPL_STATISTICS */
typedef struct ApmSuplStatistics {
    int                 numSuplInit;
    int                 numSuplPosInit;
    int                 numSuplPos;
    int                 averageTimeBetweenSuplPosInitAndSuplInit;
    int                 miniTimeBetweenSuplPosInitAndSuplInit;
    int                 maxTimeBetweenSuplPosInitAndSuplInit;
    int                 averageTimeBetweenSuplPosAndSuplInit;
    int                 miniTimeBetweenSuplPosAndSuplInit;
    int                 maxTimeBetweenSuplPosAndSuplInit;
} ApmSuplStatistics;

/* structure for KPI_TYPE_SUPL_MESSAGE */
typedef struct ApmSuplMessage {
    uint8_t             suplType;
    uint8_t             *rawMessage;
    size_t              rawMessageLength;
} ApmSuplMessage;


#ifdef  __cplusplus
}
#endif

#endif
