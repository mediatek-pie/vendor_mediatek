LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/frameworks/opt/mdm))
include $(CLEAR_VARS)
LOCAL_MODULE := mdi_redirector
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_MODULE_OWNER := mtk
LOCAL_MODULE_TAGS := optional
LOCAL_SHARED_LIBRARIES := libutils liblog libc++ libasn1c_core libasn1c_mapi libapmonitor libbinder vendor.mediatek.hardware.apmonitor@1.0 libhidlbase libhidltransport libhwbinder
LOCAL_INIT_RC := mdi_redirector.rc
LOCAL_SRC_FILES := $(call get-prebuilt-src-arch,arm arm64)_$(TARGET_ARCH_VARIANT)_$(TARGET_CPU_VARIANT)/mdi_redirector
include $(BUILD_PREBUILT)
endif
