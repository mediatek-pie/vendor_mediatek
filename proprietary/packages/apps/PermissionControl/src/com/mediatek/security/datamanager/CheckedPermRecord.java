/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */


package com.mediatek.security.datamanager;

/** @hide */
public class CheckedPermRecord {
    /** The name to be updated. */
    public final String mPackageName;
    public String mPermission;
    public int mPermissionId = -1;
    /** Enabled or not. */
    /** It's ok to use the permission. */
    public static final int STATUS_GRANTED = 0;
    /** Forbidden to use the permission, be care for the error handling. */
    public static final int STATUS_DENIED = 1;
    /** Ask user to grant the permission or not. */
    public static final int STATUS_FIRST_CHECK = 3;
    private int status = STATUS_FIRST_CHECK;

    public CheckedPermRecord(String _packageName, String _permission) {
        mPackageName = _packageName;
        mPermission = _permission;
    }

    public CheckedPermRecord(String _packageName, String _permission, int _status) {
        this(_packageName, _permission);
        status = _status;
    }

    public CheckedPermRecord(String _packageName, String _permission, boolean enable) {
        this(_packageName, _permission);
        setEnable(enable);
    }

    public CheckedPermRecord(String _packageName, int _permission) {
        mPackageName = _packageName;
        mPermissionId = _permission;
    }

    public CheckedPermRecord(String _packageName, int _permission, int _status) {
        this(_packageName, _permission);
        status = _status;
    }

    public CheckedPermRecord(CheckedPermRecord data) {
        this(data.mPackageName, data.mPermission, data.status);
        mPermissionId = data.mPermissionId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean isEnable() {
        return status == STATUS_GRANTED;
    }

    public void setEnable(boolean enable) {
        status = enable?STATUS_GRANTED:STATUS_DENIED;
    }

    @Override
    public String toString() {
        return "CheckedPermRecord {"
                + this.mPackageName + ", " + this.status + "}";
    }
}

