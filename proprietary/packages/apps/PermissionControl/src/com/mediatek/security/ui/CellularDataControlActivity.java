package com.mediatek.security.ui;

import java.util.List;

import android.app.ActionBar;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.provider.Settings;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.TelephonyIntents;
import com.mediatek.security.R;
import com.mediatek.security.datamanager.CheckedPermRecord;
import com.mediatek.security.datamanager.DatabaseManager;
import com.mediatek.security.service.PermControlService;
import com.mediatek.security.service.PermControlUtils;
import com.mediatek.security.ui.AutoBootAppManageActivity.AutoBootAdapter;

public class CellularDataControlActivity extends AutoBootAppManageActivity
        implements CompoundButton.OnCheckedChangeListener {
    private static String TAG = "CellularDataControlActivity";
    private PackageManager mPackageManager;

    private Switch mSwitch;
    private TextView mSwitchText;
    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            Log.d(TAG, "received broadcast " + action);
            switch(action) {
            case TelephonyIntents.ACTION_SIM_STATE_CHANGED:
            case Intent.ACTION_AIRPLANE_MODE_CHANGED:
            case ConnectivityManager.CONNECTIVITY_ACTION:
                refreshUi(false);
                break;
            case PermControlUtils.CELLULAR_DATA_UPDATE:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSwitch.setEnabled(true);
                    }
                });
                load();
                break;
            default:
                break;
            }
        }
    };

    @Override
    public void onCreate(android.os.Bundle icicle) {
        getIntent().putExtra("permName", getString(R.string.cellular_data_message));
        super.onCreate(icicle);
        findViewById(R.id.switch_bar).setVisibility(View.VISIBLE);
        mSwitch = (Switch )findViewById(R.id.switch1);
        mSwitchText = (TextView )findViewById(R.id.switch_text);

        mSummaryText.setVisibility(View.GONE);
        mEmptyView.setText(R.string.loading);

        mPackageManager = getPackageManager();

        getActionBar().setDisplayHomeAsUpEnabled(true);
        mSwitch.setOnCheckedChangeListener(this);
        mSwitch.setChecked(PermControlUtils.isCellularDataControlOn(this));
        onCheckedChanged(null, PermControlUtils.isCellularDataControlOn(this));
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            refreshUi(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
        case android.R.id.home:
            onBackPressed();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
        if (ActivityManager.isUserAMonkey()) {
            Log.d("@M_" + TAG, "Monkey is running");
            return;
        }
        if (isChecked) {
            mEmptyView.setText(R.string.loading);
            mSwitchText.setText(R.string.on);
        } else {
            mEmptyView.setText(R.string.control_cellular_data);
            mSwitchText.setText(R.string.off);
        }

        boolean isChanged = false;
        if (isChecked != PermControlUtils
                .isCellularDataControlOn(CellularDataControlActivity.this)) {
            PermControlUtils.enableCellularDataControl(isChecked,
                    CellularDataControlActivity.this);
            isChanged = true;
            mSwitch.setEnabled(false);
        }

        if (isChecked) {
            Intent intent = new Intent();
            intent.setClass(this, PermControlService.class);
            startService(intent);
        } else {
            load();
        }

        if ((!isChecked) && isChanged) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    List<CheckedPermRecord> records = DatabaseManager
                            .getCellularPerm();
                    for (CheckedPermRecord record : records) {
                        if (record.getStatus() != CheckedPermRecord.STATUS_GRANTED) {
                            try {
                                ApplicationInfo appInfo = mPackageManager
                                        .getApplicationInfo(
                                                record.mPackageName, 0);
                                PermControlUtils.setFirewallPolicy(
                                        CellularDataControlActivity.this,
                                        appInfo.uid, 0, isChecked);
                            } catch (NameNotFoundException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mSwitch.setEnabled(true);
                        }
                    });

                    Intent intent = new Intent();
                    intent.setClass(CellularDataControlActivity.this,
                            PermControlService.class);
                    stopService(intent);
                }
            }).start();
        }
    }

    @Override
    protected void registerReceiver() {
        final IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        filter.addAction(TelephonyIntents.ACTION_SIM_STATE_CHANGED);
        filter.addAction(PermControlUtils.CELLULAR_DATA_UPDATE);
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        getApplicationContext().registerReceiver(mBroadcastReceiver, filter);
    }

    @Override
    protected void unRegisterReceiver() {
        getApplicationContext().unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    protected void refreshUi(boolean dataChanged) {
        if (dataChanged) {
            mApdater.notifyDataSetChanged();
        }

        if (mApdater.mReceiverList == null
                || mApdater.mReceiverList.size() == 0) {
            mEmptyView.setVisibility(View.VISIBLE);
        } else {
            mEmptyView.setVisibility(View.GONE);
        }

        int count = mListView.getChildCount();
        boolean enabled = isSwitchEnable();
        for (int i = 0; i < count; i++) {
            View view = mListView.getChildAt(i);
            Switch sswitch = (Switch) view.findViewById(R.id.status);
            View appName = view.findViewById(R.id.app_name);
            sswitch.setEnabled(enabled);
            appName.setEnabled(enabled);
        }
    }

    @Override
    protected void handleItemClick(CheckedPermRecord info) {
        try {
            ApplicationInfo appInfo = mPackageManager.getApplicationInfo(
                    info.mPackageName, 0);
            boolean isChanged = DatabaseManager.modifyCellularDatePerm(info);
            if (isChanged) {
                PermControlUtils.setFirewallPolicy(this, appInfo.uid, 0, !info.isEnable());
            }
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected List<CheckedPermRecord> loadData() {
        if (PermControlUtils.isCellularDataControlOn(this)) {
            List<CheckedPermRecord> records = DatabaseManager.getCellularPerm();
            if (records == null || records.size() == 0) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mEmptyView.setText(R.string.no_cellular_data_app);
                    }

                });
            }
            return records;
        } else {
            return null;
        }
    }

    @Override
    protected boolean isSwitchEnable() {
        int dataSubId = getDefaultDataSubId();
        boolean enable = true;
        if ((getInsertedSimCardNum() == 0) || (!isValidSubId(dataSubId))
                || (!isRadioPowerOn(dataSubId))
                || (!isCellularDataOn(dataSubId)) || isAirPlaneModeOn()
                || (!PermControlUtils.isCellularDataControlOn(this))) {
            enable = false;
        } else {
            enable = true;
        }
        return enable;
    }

    private int getInsertedSimCardNum() {
        int simNumber = 0;

        TelephonyManager telephonyManager = getSystemService(TelephonyManager.class);
        if (telephonyManager == null) {
            Log.d(TAG, "TelephonyManagerEx is null");
            return 0;
        }

        for (int i = 0; i < telephonyManager.getSimCount(); i++) {
            if (telephonyManager.hasIccCard(i)) {
                Log.d(TAG, "Slot(" + i + ") has iccCard");
                simNumber++;
            }
        }

        Log.i(TAG, "getInsertedSimCardNum =" + simNumber);
        return simNumber;
    }

    private int getDefaultDataSubId() {
        int subId = SubscriptionManager.getDefaultDataSubscriptionId();
        Log.i(TAG, "getDefaultDataSubId =" + subId);
        return subId;
    }

    private boolean isValidSubId(int subId) {
        return subId != SubscriptionManager.INVALID_SUBSCRIPTION_ID;
    }

    private boolean isRadioPowerOn(int subId) {
        boolean isRadioOn = false;
        ITelephony iTel = ITelephony.Stub.asInterface(
                ServiceManager.getService(Context.TELEPHONY_SERVICE));

        if (iTel != null && isValidSubId(subId)) {
            try {
                isRadioOn = iTel.isRadioOnForSubscriber(subId,
                        getApplicationContext().getOpPackageName());
            } catch (RemoteException e) {
                Log.i(TAG, "isRadioOn  failed to get radio state for sub = "
                        + subId);
                isRadioOn = false;
            }
        } else {
            Log.i(TAG, "isRadioOn  failed because  iTel= null, subId =" + subId);
        }
        Log.i(TAG, "isRadioPowerOn =" + isRadioOn);
        return isRadioOn;
    }

    private boolean isCellularDataOn(int subId) {
        TelephonyManager tm = getSystemService(TelephonyManager.class);
        final boolean isCellularDataOn = tm.getDataEnabled(subId);
        Log.i(TAG, "isCellularDataOn =" + isCellularDataOn);
        return isCellularDataOn;
    }

    private boolean isAirPlaneModeOn() {
        boolean airplaneModeOn = Settings.Global.getInt(getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        Log.i(TAG, "isAirPlaneModeOn =" + airplaneModeOn);

        return airplaneModeOn;
    }
}
