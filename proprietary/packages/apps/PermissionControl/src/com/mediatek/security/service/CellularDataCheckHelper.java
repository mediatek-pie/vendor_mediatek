package com.mediatek.security.service;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;

import com.mediatek.security.datamanager.CheckedPermRecord;
import com.mediatek.security.datamanager.DatabaseManager;

public class CellularDataCheckHelper {
    private static final String TAG = "CellularDataCheckHelper";
    private Context mContext;
    private CheckedPermRecord mCheckedCellular;
    private int mUid;

    public CellularDataCheckHelper(@NonNull Context context,
            @NonNull CheckedPermRecord record, int uid) {
        mCheckedCellular = record;
        mContext = context;
        mUid = uid;
    }

    public void handleClick(boolean isGrant) {
        int status = CheckedPermRecord.STATUS_FIRST_CHECK;
        if (isGrant) {
            status = CheckedPermRecord.STATUS_GRANTED;
        } else {
            status = CheckedPermRecord.STATUS_DENIED;
        }
        mCheckedCellular.setStatus(status);

        boolean isChanged = DatabaseManager
                .modifyCellularDatePerm(mCheckedCellular);
        if (isChanged) {
            PermControlUtils.setFirewallPolicy(mContext, mUid, 0,
                    !mCheckedCellular.isEnable());
            Intent intent = new Intent();
            intent.putExtra(PermControlUtils.PACKAGE_NAME,
                    mCheckedCellular.mPackageName);
            intent.setAction(PermControlUtils.CELLULAR_DATA_UPDATE);
            mContext.sendBroadcast(intent);
        }
        String permission = mCheckedCellular.mPermission;
        if (mCheckedCellular.mPermissionId != -1) {
            permission = mContext.getString(mCheckedCellular.mPermissionId);
        }
        if (mCheckedCellular.getStatus() == CheckedPermRecord.STATUS_DENIED) {
            PermControlUtils
                    .showDenyToast(mContext, mCheckedCellular.mPackageName,
                            permission);
        }

        Log.d(TAG, "handleClick: pkg = " + mCheckedCellular.mPackageName
                + ", permission = " + permission
                + ", status = " + mCheckedCellular.getStatus()
                + ", is changed = " + isChanged);
    }
}
