package com.mediatek.security.ui;

import android.os.Bundle;
import android.util.Log;

public class CellularDataCheckActivityTwo extends CellularDataCheckActivity{
    private static String mCurrentCheckPkgTwo;
    private static final String TAG_TWO = "CellularDataCheckActivityTwo";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = TAG_TWO;
    }

    @Override
    protected void finishCheck() {
        synchronized(CellularDataCheckActivityTwo.class) {
            if (mCurrentCheckPkgTwo != null) {
                Log.d(TAG_TWO, "finishCheck remove + " + mCurrentCheckPkgTwo);
                mCurrentCheckPkgTwo = null;
            } else {
                Log.e(TAG_TWO, "finishCheck error + " + mCurrentCheckPkgTwo);
            }
        }
    }

    public synchronized static boolean canCheckTwo(String pkgName) {
        boolean can = false;
        if (mCurrentCheckPkgTwo == null) {
            mCurrentCheckPkgTwo = pkgName;
            Log.d(TAG_TWO, "canCheckTwo, add " + pkgName);
            can = true;
        }
        Log.d(TAG_TWO, "canCheckTwo " + can);
        return can;
    }

    public synchronized static String getCueerntPkgTwo() {
        return mCurrentCheckPkgTwo == null?null:new String(mCurrentCheckPkgTwo);
    }
}
