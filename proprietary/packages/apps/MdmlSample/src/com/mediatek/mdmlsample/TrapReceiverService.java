package com.mediatek.mdmlsample;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.mediatek.mdml.MonitorTrapReceiver;
import com.mediatek.mdml.Msg;
import com.mediatek.mdml.PlainDataDecoder;
import com.mediatek.mdml.TRAP_TYPE;
import com.mediatek.mdml.TrapHandlerInterface;
import com.mediatek.icdcodec.ICDCodec;
import com.mediatek.icdcodec.ICDMsg;
import com.mediatek.icdcodec.ICDPacketHeader;
import com.mediatek.icdcodec.ICDPayloadDecodedJsonString;


import java.lang.ref.WeakReference;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Arrays;

/**
 * Service that wrapped MonitorTrapReceiver and is responsible for handling trap.
 */
public class TrapReceiverService extends Service implements TrapHandlerInterface, TrapReceiverInitListener{
    private static final String TAG = "TrapReceiverService";
    MonitorTrapReceiver m_trapReceiver;//should be initialized after getting session ID
    private final IBinder mBinder = new LocalBinder();
    // Trap decoder objects
    private PlainDataDecoder m_plainDataDecoder;
    // Output file related objects
    String outputFilePath = Environment.getExternalStorageDirectory().getPath() + "/Download/MDML_output.txt";
    OutDbgStr outputFile;
    int trapCount = 0;

    @Override
    public void onCreate(){
        Log.d(TAG, "onCreate()");
        // Get MDML MonitorTrapReceiver objects and connection state from app object
        AppApplication app = (AppApplication) getApplicationContext();
        // Get trap decoder object from app object
        m_plainDataDecoder = app.GetPlainDataDecoder();
        // create output file (use OutDbgStr)
        outputFile = new OutDbgStr(outputFilePath);
        // Setup ICDCodec
        String jsonPath = "/sdcard/Download/AllICD.json";
        int ret = ICDCodec.LoadICDDatabase(jsonPath);
        Log.d(TAG, "LoadICDDatabase ret = " + ret);
    }

    @Override
    public int onStartCommand(Intent intenr, int flags, int startID){
        Toast.makeText(this, "TrapReceiverService Start", Toast.LENGTH_SHORT).show();
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind()");
        return mBinder;
    }

    @Override
    public void onDestroy(){
        Toast.makeText(this, "TrapReceiverService Done", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onDestroy()");
        // close output file (use OutDbgStr)
        outputFile.close();
    }

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        TrapReceiverService getService() {
            // Return this instance of LocalService so clients can call public methods
            return TrapReceiverService.this;
        }
    }

    // Public methods for clients
    WeakReference<NewTrapListener> m_trapListeners;
    public void registerLogUpdater(NewTrapListener listener){
        Log.d(TAG, "registerLogUpdater()");
        m_trapListeners = new WeakReference<NewTrapListener>(listener);
    }

    public void ProcessTrap(long timeInMillisecond, TRAP_TYPE type, int len, byte[] data, int offset) {
        Log.d(TAG, "frame is incoming: timestamp = [" + timeInMillisecond + "], type = [" + type.GetValue() + "], len = [" + len + "]");
        // Dump trap number
        trapCount++;
        OutDbgStr.writeLine("#*" + trapCount + "*#  ");
        OutDbgStr.writeLine(String.valueOf(timeInMillisecond));
        // Message object
        Msg msg = null;

        // Decode trap by trap type
        switch (type) {
            case TRAP_TYPE_OTA: {
                int traceID = (int) Utils.GetIntFromByte(data, offset, 4);
                Log.d(TAG, "[OTA]traceID: " + traceID);
                OutDbgStr.writeLine("[OTA]traceID: " + traceID);
                msg = m_plainDataDecoder.otaInfo_getMsg(data, offset);
                DumpOTAMessageFields(msg);
                break;
            }
            case TRAP_TYPE_EM: {
                int msgID = (int) Utils.GetIntFromByte(data, offset + 6, 2);
                Log.d(TAG, "[EM] " + m_plainDataDecoder.msgInfo_getMsgName(msgID) + " " + msgID);
                OutDbgStr.writeLine("[EM] " + m_plainDataDecoder.msgInfo_getMsgName(msgID) + " " + msgID);
                msg = m_plainDataDecoder.msgInfo_getMsg(data, offset);
                DumpGeneralMessageFields(msg);
                break;
            }
            case TRAP_TYPE_ICD_RECORD: {
                byte [] trap_data = Arrays.copyOfRange(data, offset, data.length);
                ICDMsg recordMsg = ICDCodec.getICDMsg(trap_data);
                if(recordMsg != null){
                    if(recordMsg.headerInfo.return_code == 1){  // msg parse successfully
                        Log.d(TAG, "[ICD] Record code : " + recordMsg.headerInfo.code);
                        OutDbgStr.writeLine("ICD record:"+recordMsg.headerInfo.code);

                        ICDPayloadDecodedJsonString jsonString = recordMsg.GetPayloadDecodedJsonString();
                        if(jsonString.return_code == 1){ // payload decoded successfully
                            OutDbgStr.writeLine(jsonString.json_string);
                        } else {
                            OutDbgStr.writeLine("Payload JSON string decoded failed, ret : " + jsonString.return_code);
                        }
                    }else{
                        Log.d(TAG, "[ICD] record msg parse error, return code : " + recordMsg.headerInfo.return_code);
                        OutDbgStr.writeLine("ICD msg parse error, return code : " + recordMsg.headerInfo.return_code);
                    }
                }
                break;
            }
            case TRAP_TYPE_ICD_EVENT: {
                byte [] trap_data = Arrays.copyOfRange(data, offset, data.length);
                ICDMsg eventMsg = ICDCodec.getICDMsg(trap_data);
                if(eventMsg != null){
                    if(eventMsg.headerInfo.return_code == 1){  // msg parse successfully
                        Log.d(TAG, "[ICD] Event code : " + eventMsg.headerInfo.code);
                        OutDbgStr.writeLine("ICD event:"+eventMsg.headerInfo.code);

                        ICDPayloadDecodedJsonString jsonString = eventMsg.GetPayloadDecodedJsonString();
                        if(jsonString.return_code == 1){ // payload decoded successfully
                            OutDbgStr.writeLine(jsonString.json_string);
                        } else {
                            OutDbgStr.writeLine("Payload JSON string decoded failed, ret : " + jsonString.return_code);
                        }
                    }else{
                        Log.d(TAG, "[ICD] event msg parse error, return code : " + eventMsg.headerInfo.return_code);
                        OutDbgStr.writeLine("ICD msg parse error, return code : " + eventMsg.headerInfo.return_code);
                    }
                }
                break;
            }
            case TRAP_TYPE_DISCARDINFO:
                OutDbgStr.writeLine("[Discard ... ]");
                break;
            default:
                Log.e(TAG, "[Unknown Type, Error ...]");
                OutDbgStr.writeLine("[Unknown Type, Error ...]");
                break;
        }

        // End token for one trap
        OutDbgStr.writeLine("*#*#");

        Log.d(TAG, "Trigger Activity listener !");
        if (m_trapListeners != null && m_trapListeners.get() != null) {
            m_trapListeners.get().NewTrapArrival();
        }
        Log.d(TAG, "frame is incoming... done!");
    }

    private void DumpGeneralMessageFields(Msg msg){
        if (msg != null) {
            Log.d(TAG, "Print General fields !");
            String[] fieldList = msg.getFieldList();
            if (fieldList != null) {
                // dump local field
                for (String fieldName : fieldList) {
                    byte[] fieldValueBytes = msg.getFieldValue(fieldName);
                    if (fieldValueBytes == null) {
                       OutDbgStr.writeLine("     " + fieldName + " : " +  "null");
                    } else if (fieldValueBytes.length <= 4) { // single value
                        OutDbgStr.write("     " + fieldName + " : ");
                        long fieldValue = Utils.GetIntFromByte(fieldValueBytes, 0, fieldValueBytes.length);
                        OutDbgStr.write(" " + fieldValue);
                        OutDbgStr.newLine();
                    }
                }
                // dump peer buffer
                OutDbgStr.write("     Peer Buffer : ");
                int peerSize = msg.getPeerBufferLength();
                if(peerSize > 0) {
                    byte [] peerBufBytes = msg.getPeerBufferValue();
                    OutDbgStr.writeLine("length = " + peerSize);
                    for (int i = 0 ; i < peerSize ; i++) {
                        OutDbgStr.write(String.format("0x%02x", peerBufBytes[i]) + "(" + Byte.toString(peerBufBytes[i]) + ") ");
                    }
                    OutDbgStr.newLine();
                }
                else {
                    OutDbgStr.writeLine("null");
                }
            }
        }
    }

    private void DumpOTAMessageFields(Msg msg) {
        if (msg != null) {
            Log.d(TAG, "Print OTA fields !");
            // get OTA global ID
            byte[] otaOffset = msg.getOtaMsgFieldValue();
            String otaGlobalID = msg.getOtaMsgGlobalID(otaOffset);
            if (otaGlobalID != null) {
                OutDbgStr.writeLine("     Global ID : " + otaGlobalID);
            }
            // get OTA raw data
            int raw_offset = msg.getOtaMsgAirMsgRawDataBufOffset(otaOffset);
            int raw_length = msg.getOtaMsgAirMsgRawDataBufLength(otaOffset);
            Log.d(TAG, "raw length = " + raw_length + ",  otaOffset.length - raw_offset - 2 = " + (otaOffset.length - raw_offset - 2));
            if(raw_length > 0){
                OutDbgStr.write("     OTA raw data : ");
                for (int i=0; i<raw_length; i++) {
                    OutDbgStr.write(String.format("0x%02x", otaOffset[raw_offset+i]) + "(" + Byte.toString(otaOffset[raw_offset+i]) + ") ");
                }
                OutDbgStr.newLine();
            }
        }
    }

    /**
     * In order to make MonitorTrapReceiver and Session to be one-to-one correspondence,
     * MonitorTrapReceiver should be initialized after getting session ID.
     * 
     * @param szServerName [IN] The host name of the receiver.
     */
    private void InitMDMLTrapReceiver(long sessionId, String serverName) {
        if (0 >= sessionId) {
            throw new Error("Invalid session id! MonitorTrapReceiver"
                    + "should be constructed after getting a valid session ID");
        }
        m_trapReceiver = new MonitorTrapReceiver(sessionId, serverName);
        // setup trap handler to trap receiver
        m_trapReceiver.SetTrapHandler(this);
    }

    @Override
    public void onTrapReceiverInit(long sessionId, String serverName) {
        InitMDMLTrapReceiver(sessionId, serverName);
    }
}
