package com.mediatek.mtklogger.controller;

import android.content.SharedPreferences;
import android.text.TextUtils;

import com.log.handler.LogHandler;
import com.log.handler.LogHandlerUtils.LogType;
import com.mediatek.mtklogger.MyApplication;
import com.mediatek.mtklogger.utils.Utils;

/**
 * @author MTK81255
 *
 */
public class NetworkLogController extends AbstractLogController {
    private static final String TAG = Utils.TAG + "/NetworkLogController";

    public static NetworkLogController sInstance = new NetworkLogController(LogType.NETWORK_LOG);
    private SharedPreferences mDefaultSharedPreferences =
            MyApplication.getInstance().getDefaultSharedPreferences();

    private NetworkLogController(LogType logType) {
        super(logType);
    }

    public static NetworkLogController getInstance() {
        return sInstance;
    }

    @Override
    public boolean startLog(String logPath) {
        String configPath = getConfigPath();
        if (configPath != null && !configPath.isEmpty()) {
            logPath = configPath;
        }
        return LogHandler.getInstance().startNetworkLog(logPath, getRecycleSize(),
                getLimitedPackageSize());
    }

    private int getRecycleSize() {
        int recycleSize = Utils.DEFAULT_LOG_SIZE;
        try {
            recycleSize = Integer.parseInt(mDefaultSharedPreferences.getString(
                    Utils.KEY_NETWORK_LOG_LOGSIZE, String.valueOf(Utils.DEFAULT_LOG_SIZE)));
        } catch (NumberFormatException e) {
            Utils.loge(TAG, "parser recycleSize failed!");
            recycleSize = Utils.DEFAULT_LOG_SIZE;
        }
        return recycleSize;
    }

    /**
     * Get each network log package limited size, 0 mean no limitation.
     *
     * @return
     */
    private int getLimitedPackageSize() {
        boolean limitPackageEnabled =
                mDefaultSharedPreferences.getBoolean(Utils.KEY_NT_LIMIT_PACKAGE_ENABLER, false);
        if (!limitPackageEnabled) {
            return 0;
        }
        String limitedPackageSizeStr =
                mDefaultSharedPreferences.getString(Utils.KEY_NT_LIMIT_PACKAGE_SIZE,
                        String.valueOf(Utils.VALUE_NT_LIMIT_PACKAGE_DEFAULT_SIZE));

        int limitedPackageSize = Utils.VALUE_NT_LIMIT_PACKAGE_DEFAULT_SIZE;
        if (!TextUtils.isEmpty(limitedPackageSizeStr)) {
            try {
                int tempSize = Integer.parseInt(limitedPackageSizeStr);
                if (tempSize >= 0) {
                    limitedPackageSize = tempSize;
                }
            } catch (NumberFormatException e) {
                Utils.loge(TAG, "parser limitedPackageSizeStr failed : " + limitedPackageSizeStr);
            }
        }
        return limitedPackageSize;
    }

    @Override
    public boolean stopLog() {
        boolean isNeedCheck =
                mDefaultSharedPreferences.getBoolean(Utils.KEY_NETWORK_LOG_DO_PING, false);
        return LogHandler.getInstance().stopNetworkLog(isNeedCheck);
    }

}
