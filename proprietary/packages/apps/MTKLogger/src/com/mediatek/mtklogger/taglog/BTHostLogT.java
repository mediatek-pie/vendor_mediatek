package com.mediatek.mtklogger.taglog;

/**
 * @author MTK81255
 *
 */
public class BTHostLogT extends LogInstanceForTaglog {

    /**
     * @param logType int
     * @param tagLogInformation TagLogInformation
     */
    public BTHostLogT(int logType, TagLogInformation tagLogInformation) {
        super(logType, tagLogInformation);
    }

}