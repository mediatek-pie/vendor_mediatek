package com.mediatek.mtklogger.controller;

import com.log.handler.LogHandler;
import com.log.handler.LogHandlerUtils.LogType;
import com.mediatek.mtklogger.utils.Utils;

import java.io.File;

/**
 * @author MTK81255
 *
 */
public abstract class AbstractLogController {
    private static final String TAG = Utils.TAG + "/LogController";

    protected LogType mLogType;

    /**
     * @param logType
     *            LogType
     */
    protected AbstractLogController(LogType logType) {
        mLogType = logType;
    }

    public LogType getLogType() {
        return mLogType;
    }

    /**
     * @param logPath
     *            String
     * @return boolean
     */
    public boolean startLog(String logPath) {
        String configPath = getConfigPath();
        if (configPath != null && !configPath.isEmpty()) {
            logPath = configPath;
        }
        return LogHandler.getInstance().startTypeLog(mLogType, logPath);
    }

    /**
     * @return boolean
     */
    public boolean stopLog() {
        return LogHandler.getInstance().stopTypeLog(mLogType);
    }

    /**
     * @param logPath
     *            String
     * @return boolean
     */
    public boolean rebootLog(String logPath) {
        boolean isStopSuccess = stopLog();
        boolean isStartSuccess = startLog(logPath);
        return isStopSuccess && isStartSuccess;
    }

    /**
     * @param logSize
     *            int
     * @return boolean
     */
    public boolean setLogRecycleSize(int logSize) {
        return LogHandler.getInstance().setTypeLogRecycleSize(mLogType, logSize);
    }

    /**
     * @param enable
     *            boolean
     * @return boolean
     */
    public boolean setBootupLogSaved(boolean enable) {
        return LogHandler.getInstance().setBootupTypeLogSaved(mLogType, enable);
    }

    public boolean isLogRunning() {
        return LogHandler.getInstance().isTypeLogRunning(mLogType);
    }

    /**
     * @return String
     */
    public String getRunningLogPath() {
        String runningLogPath = null;
        if (!isLogRunning()) {
            return null;
        }
        String parentPath = Utils.getCurrentLogPath() + Utils.MTKLOG_PATH
                + Utils.LOG_PATH_MAP.get(LogControllerUtils.LOG_TYPE_OBJECT_TO_INT.get((mLogType)));
        File fileTree = new File(parentPath + File.separator + Utils.LOG_TREE_FILE);
        File logFile = Utils.getLogFolderFromFileTree(fileTree, false);
        if (null != logFile && logFile.exists()) {
            runningLogPath = logFile.getAbsolutePath();
        }
        Utils.logi(TAG, "<--getRunningLogPath(), logType = " + mLogType + ", runningLogPath = "
                + runningLogPath);
        return runningLogPath;
    }

    public String getConfigPath() {
        return Utils.getConfigStringValue(LogControllerUtils.LOG_CONFIG_PATH_KEY.get(mLogType));
    }

    /**
     * @param cmd
     *            String
     * @return boolean
     */
    public boolean dealWithADBCommand(String cmd) {
        return true;
    }

}
