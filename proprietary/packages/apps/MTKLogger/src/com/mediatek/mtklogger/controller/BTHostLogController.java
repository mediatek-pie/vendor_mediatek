package com.mediatek.mtklogger.controller;

import android.content.SharedPreferences;

import com.log.handler.LogHandler;
import com.log.handler.LogHandlerUtils.BTFWLogLevel;
import com.log.handler.LogHandlerUtils.LogType;
import com.mediatek.mtklogger.MyApplication;
import com.mediatek.mtklogger.settings.ConnsysLogSettings;
import com.mediatek.mtklogger.settings.SettingsActivity;
import com.mediatek.mtklogger.utils.Utils;

/**
 * @author MTK81255
 *
 */
public class BTHostLogController extends AbstractLogController {
    private static final String TAG = Utils.TAG + "/BTHostLogController";

    public static BTHostLogController sInstance = new BTHostLogController(LogType.BTHOST_LOG);

    private SharedPreferences mDefaultSharedPreferences =
            MyApplication.getInstance().getDefaultSharedPreferences();

    private BTHostLogController(LogType logType) {
        super(logType);
    }

    public static BTHostLogController getInstance() {
        return sInstance;
    }

    @Override
    public boolean startLog(String logPath) {
        setHCISnoopLogSize();
        setBTStackLogEnable();
        setBTFWLogLevel();
        return super.startLog(logPath);
    }

    /**
     * setBTFirmwareLogLevel.
     */
    public void setBTFWLogLevel() {
        boolean connsysLogSwitchEnable = mDefaultSharedPreferences.getBoolean(
                SettingsActivity.KEY_LOG_SWITCH_MAP.get(Utils.LOG_TYPE_CONNSYSFW), true);
        String btFWLogLevel = connsysLogSwitchEnable
                ? mDefaultSharedPreferences.getString(ConnsysLogSettings.KEY_BTFW_LOG_LEVEL, "2")
                : "0";
        Utils.logi(TAG, "setBTFirmwareLogLevel. btFWLogLevel = " + btFWLogLevel);
        LogHandler.getInstance().setBTFWLogLevel(BTFWLogLevel.getBTFWLogLevelByID(btFWLogLevel));
    }

    /**
     * setHCISnoopLogSize.
     */
    public void setHCISnoopLogSize() {
        int hciSnoopLogSize = 2048;
        String hciSnoopLogSizStr = "";
        try {
            hciSnoopLogSizStr = mDefaultSharedPreferences
                    .getString(ConnsysLogSettings.KEY_BTHOST_LOGSIZE, "2048");
            hciSnoopLogSize = Integer.parseInt(String.valueOf(hciSnoopLogSizStr));
        } catch (NumberFormatException e) {
            Utils.loge(TAG,
                    "Integer.parseInt(" + String.valueOf(hciSnoopLogSizStr) + ") is error!");
        }
        setLogRecycleSize(hciSnoopLogSize);
    }

    /**
     * setBTStackLogEnable.
     */
    public void setBTStackLogEnable() {
        boolean btStackLogEnable = mDefaultSharedPreferences
                .getBoolean(ConnsysLogSettings.KEY_BTSTACK_LOG_ENABLE, false);
        Utils.logi(TAG, "setBTStackLogEnable. btStackLogEnable = " + btStackLogEnable);
        LogHandler.getInstance().setBTHostDebuglogEnable(btStackLogEnable);
    }

}
