package com.mediatek.mtklogger.controller;

import com.log.handler.LogHandler;
import com.log.handler.LogHandlerUtils.LogType;
import com.log.handler.LogHandlerUtils.MobileLogSubLog;
import com.mediatek.mtklogger.utils.Utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author MTK81255
 *
 */
public class MobileLogController extends AbstractLogController {

    private static final String TAG = Utils.TAG + "/MobileLogController";

    public static MobileLogController sInstance = new MobileLogController(LogType.MOBILE_LOG);

    private MobileLogController(LogType logType) {
        super(logType);
    }

    public static MobileLogController getInstance() {
        return sInstance;
    }

    /**
     * @param enable
     *            boolean
     * @param subMobileLogInt
     *            int
     * @return boolean
     */
    public boolean setSubLogEnable(boolean enable, int subMobileLogInt) {
        MobileLogSubLog mobileLogSubLog = convertToSubMobileLogObject(subMobileLogInt);
        if (mobileLogSubLog != null) {
            return setSubLogEnable(enable, mobileLogSubLog);
        } else {
            Utils.loge(TAG, "When setSubLogEnable(), fail to get MobileLogSubLog"
                    + " of subMobileLogInt = " + subMobileLogInt);
            return false;
        }
    }

    /**
     * @param enable
     *            boolean
     * @param subMobileLog
     *            MobileLogSubLog
     * @return boolean
     */
    public boolean setSubLogEnable(boolean enable, MobileLogSubLog subMobileLog) {
        return LogHandler.getInstance().setSubMobileLogEnable(subMobileLog, enable);
    }

    private MobileLogSubLog convertToSubMobileLogObject(int subMobileLogInt) {
        switch (subMobileLogInt) {
        case 1:
            return MobileLogSubLog.AndroidLog;
        case 2:
            return MobileLogSubLog.KernelLog;
        case 3:
            return MobileLogSubLog.SCPLog;
        case 4:
            return MobileLogSubLog.ATFLog;
        case 5:
            return MobileLogSubLog.BSPLog;
        case 6:
            return MobileLogSubLog.MmediaLog;
        case 7:
            return MobileLogSubLog.SSPMLog;
        case 8:
            return MobileLogSubLog.ADSPLog;
        default:
            return null;
        }
    }

    public static final Map<MobileLogSubLog, String> SUB_LOG_SETTINGS_ID_MAP =
            new HashMap<MobileLogSubLog, String>();
    static {
        SUB_LOG_SETTINGS_ID_MAP.put(MobileLogSubLog.AndroidLog, "mobilelog_androidlog");
        SUB_LOG_SETTINGS_ID_MAP.put(MobileLogSubLog.KernelLog, "mobilelog_kernellog");
        SUB_LOG_SETTINGS_ID_MAP.put(MobileLogSubLog.SCPLog, "mobilelog_scplog");
        SUB_LOG_SETTINGS_ID_MAP.put(MobileLogSubLog.ATFLog, "mobilelog_atflog");
        SUB_LOG_SETTINGS_ID_MAP.put(MobileLogSubLog.BSPLog, "mobilelog_bsplog");
        SUB_LOG_SETTINGS_ID_MAP.put(MobileLogSubLog.MmediaLog, "mobilelog_mmedialog");
        SUB_LOG_SETTINGS_ID_MAP.put(MobileLogSubLog.SSPMLog, "mobilelog_sspmlog");
        SUB_LOG_SETTINGS_ID_MAP.put(MobileLogSubLog.ADSPLog, "mobilelog_adsp_log");
    }

    public static final Map<MobileLogSubLog, String> SUB_LOG_CONFIG_STRING_MAP =
            new HashMap<MobileLogSubLog, String>();
    static {
        SUB_LOG_CONFIG_STRING_MAP.put(MobileLogSubLog.AndroidLog,
                "com.mediatek.log.mobile.AndroidLog");
        SUB_LOG_CONFIG_STRING_MAP.put(MobileLogSubLog.KernelLog,
                "com.mediatek.log.mobile.KernelLog");
        SUB_LOG_CONFIG_STRING_MAP.put(MobileLogSubLog.SCPLog, "com.mediatek.log.mobile.SCPLog");
        SUB_LOG_CONFIG_STRING_MAP.put(MobileLogSubLog.ATFLog, "com.mediatek.log.mobile.ATFLog");
        SUB_LOG_CONFIG_STRING_MAP.put(MobileLogSubLog.BSPLog, "com.mediatek.log.mobile.BSPLog");
        SUB_LOG_CONFIG_STRING_MAP.put(MobileLogSubLog.MmediaLog,
                "com.mediatek.log.mobile.MmediaLog");
        SUB_LOG_CONFIG_STRING_MAP.put(MobileLogSubLog.SSPMLog,
                "com.mediatek.log.mobile.SSPMLog");
        SUB_LOG_CONFIG_STRING_MAP.put(MobileLogSubLog.ADSPLog,
                "com.mediatek.log.mobile.ADSPLog");
    }

    /**
     * @param size
     *            int
     * @return boolean
     */
    public boolean setMobileLogTotalRecycleSize(int size) {
        return LogHandler.getInstance().setMobileLogTotalRecycleSize(size);
    }
}
