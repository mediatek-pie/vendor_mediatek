package com.mediatek.mtklogger.controller;

import com.log.handler.LogHandlerUtils.LogType;

/**
 * @author MTK81255
 *
 */
public class GPSHostLogController extends AbstractLogController {

    public static GPSHostLogController sInstance = new GPSHostLogController(LogType.GPSHOST_LOG);

    private GPSHostLogController(LogType logType) {
        super(logType);
    }

    public static GPSHostLogController getInstance() {
        return sInstance;
    }

    @Override
    public boolean setLogRecycleSize(int logSize) {
        return true;
    }

}
