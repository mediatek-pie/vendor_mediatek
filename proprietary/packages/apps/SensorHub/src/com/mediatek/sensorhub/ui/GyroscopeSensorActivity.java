package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;

import com.mediatek.sensorhub.settings.Utils;

public class GyroscopeSensorActivity extends CustomerSensorBaseActivity {

    public GyroscopeSensorActivity() {
        super(Sensor.STRING_TYPE_GYROSCOPE);
    }
}
