package com.mediatek.sensorhub.settings;

public class MtkSensor {

    // MTK Sensors string type
    public static final String STRING_TYPE_PEDOMETER = "android.sensor.pedometer";
    public static final String STRING_TYPE_IN_POCKET = "android.sensor.in_pocket";
    public static final String STRING_TYPE_ACTIVITY = "android.sensor.activity";
    public static final String STRING_TYPE_PDR = "android.sensor.pdr";
    public static final String STRING_TYPE_FREEFALL = "android.sensor.freefall";
    public static final String STRING_TYPE_FLAT = "android.sensor.flat";
    public static final String STRING_TYPE_FACE_DOWN = "android.sensor.face_down";
    public static final String STRING_TYPE_SHAKE = "android.sensor.shake";
    public static final String STRING_TYPE_BRING_TO_SEE = "android.sensor.bring_to_see";
    public static final String STRING_TYPE_ANSWERCALL = "android.sensor.answer_call";
    public static final String STRING_TYPE_FLOOR_COUNT = "android.sensor.floor_count";

}
