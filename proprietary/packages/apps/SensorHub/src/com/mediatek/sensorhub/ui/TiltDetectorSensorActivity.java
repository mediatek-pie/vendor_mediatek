package com.mediatek.sensorhub.ui;

import android.content.Context;
import android.hardware.Sensor;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.SwitchPreference;
import android.util.Log;

import com.mediatek.sensorhub.settings.Utils;
import com.mediatek.sensorhub.ui.ButtonPreference;
import com.mediatek.sensorhub.ui.ButtonPreference.OnButtonClickCallback;

public class TiltDetectorSensorActivity extends BaseActivity implements OnPreferenceChangeListener,
        OnButtonClickCallback {
    private static final String TAG = "TiltDetectorSensorActivity";
    private static final long TIME_OUT = 5*1000;
    private static final String TYPE = Sensor.SENSOR_STRING_TYPE_TILT_DETECTOR;
    private String mSoundOnKey = TYPE + Utils.KEY_NOTIFY_STATUS_SUFFIX;
    private String mScreenOnKey = TYPE + Utils.KEY_SCREEN_ON;
    private String mPickUpKey = TYPE + "_pick_up";
    private String mPutDownKey = TYPE + "_put_down";
    private SwitchPreference mSensorPreference;
    private SwitchPreference mSoundOnPref;
    private SwitchPreference mScreenOnPref;
    private Preference mPickUpPref;
    private Preference mPutDownPref;
    private PowerManager mPm;

    public TiltDetectorSensorActivity() {
        super(TYPE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        addPreferencesFromResource(R.xml.tilt_detector_pref);
        initializeAllPreferences();
    }

    private void initializeAllPreferences() {
        mSensorPreference = (SwitchPreference) Utils.createPreference(Utils.TYPE_SWITCH,
                mSensorKeyMap.get(TYPE).getName(), TYPE,
                getPreferenceScreen(), this);
        String soundOn = getString(R.string.notify_by_sound);
        mSoundOnPref = (SwitchPreference) Utils.createPreference(Utils.TYPE_SWITCH, soundOn,
                mSoundOnKey, getPreferenceScreen(), this);
        String screenOn = getString(R.string.notify_by_screen_on);
        mScreenOnPref = (SwitchPreference) Utils.createPreference(Utils.TYPE_SWITCH, screenOn,
                mScreenOnKey, getPreferenceScreen(), this);
        String pickUp = getString(R.string.pick_up_count);
        mPickUpPref = Utils.CreateClearButtonPreference(mPickUpKey, pickUp,
                getPreferenceScreen(), this, this);
        String putDown = getString(R.string.put_down_count);
        mPutDownPref = Utils.CreateClearButtonPreference(mPutDownKey, putDown,
                getPreferenceScreen(), this, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updatePreferenceStatus();
    }

    private void updatePreferenceStatus() {
        mSensorPreference.setChecked(Utils.getSensorStatus(TYPE));
        mSoundOnPref.setChecked(Utils.getSensorStatus(mSoundOnKey));
        mScreenOnPref.setChecked(Utils.getSensorStatus(mScreenOnKey));
        String pickUpCount = String.valueOf(Utils.getTriggerCount(mPickUpKey));
        mPickUpPref.setSummary(pickUpCount);
        String putDonwCount = String.valueOf(Utils.getTriggerCount(mPutDownKey));
        mPutDownPref.setSummary(putDonwCount);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        boolean bNewValue = (Boolean) newValue;
        String prefKey = preference.getKey();
        if (preference == mSensorPreference) {
            Utils.setSensorStatus(prefKey, bNewValue);
            if (mBound) {
                Log.d(TAG, "regestorSensor : " + prefKey + ", switch = " + bNewValue);
                mSensorService.registerSensor(prefKey, bNewValue);
            }
        } else if (preference instanceof SwitchPreference) {
            Utils.setSensorStatus(prefKey, bNewValue);
        }
        return true;
    }

    @Override
    public void onSensorChanged(float[] value) {
        Log.d(TAG, "onSensorChanged : " +(int)value[0]);
        int pickUpCount = Utils.getTriggerCount(mPickUpKey);
        int putDownCount = Utils.getTriggerCount(mPutDownKey);
        mSensorPreference.setSummary(String.valueOf((int)value[0]));

        switch ((int)value[0]) {
            case 0 :
                updateCountAndSummay(mPutDownKey, ++putDownCount);
                notifyByScreen(Utils.getSensorStatus(mScreenOnKey), false);
                break;
            case 1 :
                updateCountAndSummay(mPickUpKey, ++pickUpCount);
                notifyByScreen(Utils.getSensorStatus(mScreenOnKey), true);
                break;
            default :
                break;
        }
    }

    private void notifyByScreen(boolean notify, boolean screenOn) {
        if(!notify) {
            Log.d(TAG, "The notify switch is off");
            return;
        }
        try {
            if (screenOn && !mPm.isScreenOn()) {
                Log.d(TAG, "Notify user by screen on");
                wakeup();
            } else if (!screenOn && mPm.isScreenOn()){
                Log.d(TAG, "Notify user by screen off");
                suspend();
            } else {
                Log.d(TAG, "No need to notify, the screen is already on/off");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void wakeup() {
        WakeLock wl = mPm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                    PowerManager.ACQUIRE_CAUSES_WAKEUP, TAG);
        wl.acquire(TIME_OUT);
    }

    private void suspend() {
        try {
            mPm.goToSleep(SystemClock.uptimeMillis());
        } catch (SecurityException e) {
            Log.d(TAG, "Was unable to call PowerManager.goToSleep.");
        }
    }

    public void onClearClick(String key) {
        Log.d(TAG, "onClearClick");
        updateCountAndSummay(key, 0);
    }

    private void updateCountAndSummay(String key, int value) {
        Utils.setTriggerCount(key, value);
        findPreference(key).setSummary(String.valueOf((value)));
    }
}
