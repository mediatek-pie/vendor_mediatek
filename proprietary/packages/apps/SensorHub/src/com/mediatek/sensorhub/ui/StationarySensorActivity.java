package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;

import com.mediatek.sensorhub.settings.Utils;

public class StationarySensorActivity extends BaseTriggerSensorActivity {

    public StationarySensorActivity() {
        super(Sensor.STRING_TYPE_STATIONARY_DETECT);
    }
}
