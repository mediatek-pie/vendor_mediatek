package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;

import com.mediatek.sensorhub.settings.Utils;

public class SmdSensorActivity extends BaseTriggerSensorActivity {

    public SmdSensorActivity() {
        super(Sensor.STRING_TYPE_SIGNIFICANT_MOTION);
    }
}
