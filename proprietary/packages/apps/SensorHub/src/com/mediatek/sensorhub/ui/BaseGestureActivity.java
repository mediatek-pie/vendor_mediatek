package com.mediatek.sensorhub.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.hardware.Sensor;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.SwitchPreference;
import android.util.Log;

import com.mediatek.sensorhub.settings.Utils;
import com.mediatek.sensorhub.ui.ButtonPreference;
import com.mediatek.sensorhub.ui.ButtonPreference.OnButtonClickCallback;

public class BaseGestureActivity extends BaseActivity implements OnPreferenceChangeListener,
        OnSharedPreferenceChangeListener, OnButtonClickCallback {
    private static final String TAG = "BaseGestureActivity";
    private static final long TIME_OUT = 2000;
    public String mType;
    private String mAutoEnableKey;
    private String mSoundOnKey;
    private String mScreenOnKey;
    private String mCountKey;
    private SwitchPreference mSensorPreference;
    private SwitchPreference mAutoEnablePref;
    private SwitchPreference mSoundOnPref;
    private SwitchPreference mScreenOnPref;
    private Preference mCountPref;
    private PowerManager mPm;

    public BaseGestureActivity(String type) {
        super(type);
        mType = type;
        mAutoEnableKey = mType + Utils.KEY_AUTO_TRIGGER_STATUS_SUFFIX;
        mSoundOnKey = mType + Utils.KEY_NOTIFY_STATUS_SUFFIX;
        mScreenOnKey = mType + Utils.KEY_SCREEN_ON;
        mCountKey = mType + Utils.KEY_TRIGGER_COUNT;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        addPreferencesFromResource(R.xml.base_gesture_pref);
        initializeAllPreferences();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updatePreferenceStatus();
        Utils.getSharedPreferences(this, Utils.SHARED_PREF_SENSOR_HUB)
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        Utils.getSharedPreferences(this, Utils.SHARED_PREF_SENSOR_HUB)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    private void initializeAllPreferences() {
        Sensor sensor = mSensorKeyMap.get(mType);
        if (sensor != null) {
            Utils.createPreference(Utils.TYPE_SWITCH, sensor.getName(), mType,
                    getPreferenceScreen(), this);
            String autoEnabled = getString(R.string.auto_enabled_title);
            Utils.createPreference(Utils.TYPE_SWITCH, autoEnabled, mAutoEnableKey,
                    getPreferenceScreen(), this);
            String soundOn = getString(R.string.notify_by_sound);
            mSoundOnPref = (SwitchPreference) Utils.createPreference(Utils.TYPE_SWITCH, soundOn,
                    mSoundOnKey, getPreferenceScreen(), this);
            String screenOn = getString(R.string.notify_by_screen_on);
            Utils.createPreference(Utils.TYPE_SWITCH, screenOn, mScreenOnKey,
                    getPreferenceScreen(), this);
            String reportCount = getString(R.string.report_count);
            Utils.CreateClearButtonPreference(mCountKey, reportCount, getPreferenceScreen(),
                    this, this);
        }

    }

    public void onClearClick(String key) {
        Log.d(TAG, "onClearClick, key = " + key);
        updateCountAndSummay(key, 0);
    }

    @Override
    public void onSensorChanged(float[] value) {
        int count = Utils.getTriggerCount(mCountKey);
        Log.d(TAG, "onSensorChanged : " + mType +", count = " + count);
        updateCountAndSummay(mCountKey, ++count);
        notifyByScreenOn(Utils.getSensorStatus(mScreenOnKey));
    }

    private void updateCountAndSummay(String key, int value) {
        Utils.setTriggerCount(key, value);
        findPreference(key).setSummary(String.valueOf((value)));
    }

    private void updatePreferenceStatus() {
        mSensorPreference = (SwitchPreference) findPreference(mType);
        if (mSensorPreference != null) {
            mSensorPreference.setChecked(Utils.getSensorStatus(mType));
        }
        mAutoEnablePref = (SwitchPreference) findPreference(mAutoEnableKey);
        if (mAutoEnablePref != null) {
            mAutoEnablePref.setChecked(Utils.getSensorStatus(mAutoEnableKey));
        }
        mSoundOnPref = (SwitchPreference) findPreference(mSoundOnKey);
        if (mSoundOnPref != null) {
            mSoundOnPref.setChecked(Utils.getSensorStatus(mSoundOnKey));
        }
        mScreenOnPref = (SwitchPreference) findPreference(mScreenOnKey);
        if (mScreenOnPref != null) {
            mScreenOnPref.setChecked(Utils.getSensorStatus(mScreenOnKey));
        }
        mCountPref = findPreference(mCountKey);
        if (mCountPref != null) {
            String count = String.valueOf(Utils.getTriggerCount(mCountKey));
            Log.d(TAG, "updatePreferenceStatus : " + count);
            mCountPref.setSummary(count);
        }
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        boolean bNewValue = (Boolean) newValue;
        String prefKey = preference.getKey();
        if (preference == mSensorPreference) {
            Utils.setSensorStatus(prefKey, bNewValue);
            if (mBound) {
                mSensorService.registerSensor(preference.getKey(), bNewValue);
            }
        } else if (preference instanceof SwitchPreference) {
            Utils.setSensorStatus(prefKey, bNewValue);
        }
        return true;
    }

    private void notifyByScreenOn(boolean notify) {
        if(!notify || mPm.isScreenOn()) {
             Log.d(TAG, "No need to notify");
            return;
        }
        try {
            Log.d(TAG, "Notify user by screen on");
            WakeLock wl= mPm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                    PowerManager.ACQUIRE_CAUSES_WAKEUP, TAG);
            wl.acquire(TIME_OUT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (!mCountKey.equals(key)) {
            boolean status = sharedPreferences.getBoolean(key, false);
            Log.d(TAG, "onSharedPreferenceChanged : " + key + " status " + status);
            SwitchPreference preference = (SwitchPreference) findPreference(key);
            if (preference != null) {
                preference.setChecked(status);
            }
        }
    }
}
