package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;

import com.mediatek.sensorhub.settings.Utils;

public class StepDetectorSensorActivity extends CustomerSensorBaseActivity {

    public StepDetectorSensorActivity() {
        super(Sensor.STRING_TYPE_STEP_DETECTOR);
    }
}
