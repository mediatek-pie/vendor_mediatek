package com.mediatek.sensorhub.ui;

import android.app.ActionBar;
import android.app.PendingIntent;
import android.content.Intent;
import android.hardware.Sensor;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.SwitchPreference;
import android.util.Log;
import android.view.Gravity;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.mediatek.sensorhub.settings.MtkSensor;
import com.mediatek.sensorhub.settings.Utils;
import com.mediatek.sensorhub.stresstest.SensorStressTestActivity;

import java.util.Arrays;

public class SensorSettings extends BaseActivity implements
        CompoundButton.OnCheckedChangeListener {

    private static final String TAG = "SensorSettings";
    private static final String KEY_GESTURE_WAKE_UP = "gesture_wake_up_pref";
    private static final String KEY_GOOGLE_GESTURE = "google_gestures_pref";
    private static final String KEY_COMPOSITE_PREF = "composite_pref";
    private static final String KEY_FUSION_PREF = "fusion_pref";
    private static final String KEY_STRESS_PREF = "stress_test_pref";

    private Switch mActionBarSwitch;

    private Preference mAnswerCall;
    private Preference mAccelePref;
    private Preference mLightPref;
    private Preference mProximityPref;
    private Preference mStepCounter;
    private Preference mStepDetector;
    private Preference mPressure;
    private Preference mGyroscope;
    private Preference mMagneticField;
    private Preference mInPocketPref;
    private Preference mPedometerPref;
    private Preference mAcitvityPref;
    private Preference mSmdPref;
    private Preference mPdrPref;
    private Preference mGoogleGsturePref;
    private Preference mCompositePref;
    private Preference mStationaryPref;
    private Preference mFusionPref;
    private Preference mDeviceOritation;
    private Preference mMotionDetect;
    private Preference mFloorCounter;
    private Preference mFlatPref;
    private Preference mStressTest;
    private Preference mTiltDetector;

    public SensorSettings() {
        super(TAG);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.sensorhub_settings_pref);
        addSensorsList();
        initActionButton();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils.acquireScreenWakeLock(this);
        updatePreferenceStatus();
    }

    @Override
    protected void onPause() {
        Utils.releaseScreenWakeLock();
        super.onPause();
    }

    private void initActionButton() {
        mActionBarSwitch = new Switch(getLayoutInflater().getContext());
        final int padding = getResources().getDimensionPixelSize(R.dimen.action_bar_switch_padding);
        mActionBarSwitch.setPaddingRelative(0, 0, padding, 0);
        getActionBar().setDisplayOptions(
                ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_CUSTOM,
                ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_CUSTOM);
        getActionBar()
                .setCustomView(
                        mActionBarSwitch,
                        new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,
                                ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER_VERTICAL
                                        | Gravity.END));
        getActionBar().setDisplayHomeAsUpEnabled(false);
        mActionBarSwitch.setChecked(Utils.getSensorStatus(Utils.LOG_STATUS));
        mActionBarSwitch.setOnCheckedChangeListener(this);
    }

    private void addSensorsList() {

        // Add sensor list
        mAnswerCall = getPreference(Utils.TYPE_PREFERENCE, MtkSensor.STRING_TYPE_ANSWERCALL);
        mAccelePref = getPreference(Utils.TYPE_PREFERENCE, Sensor.STRING_TYPE_ACCELEROMETER);
        mLightPref = getPreference(Utils.TYPE_PREFERENCE, Sensor.STRING_TYPE_LIGHT);
        mProximityPref = getPreference(Utils.TYPE_PREFERENCE, Sensor.STRING_TYPE_PROXIMITY);
        mStepCounter = getPreference(Utils.TYPE_PREFERENCE, Sensor.STRING_TYPE_STEP_COUNTER);
        mStepDetector = getPreference(Utils.TYPE_PREFERENCE, Sensor.STRING_TYPE_STEP_DETECTOR);
        mPressure = getPreference(Utils.TYPE_PREFERENCE, Sensor.STRING_TYPE_PRESSURE);
        mGyroscope = getPreference(Utils.TYPE_PREFERENCE, Sensor.STRING_TYPE_GYROSCOPE);
        mMagneticField = getPreference(Utils.TYPE_PREFERENCE, Sensor.STRING_TYPE_MAGNETIC_FIELD);
        mInPocketPref = getPreference(Utils.TYPE_PREFERENCE, MtkSensor.STRING_TYPE_IN_POCKET);
        mPedometerPref = getPreference(Utils.TYPE_PREFERENCE, MtkSensor.STRING_TYPE_PEDOMETER);
        mAcitvityPref = getPreference(Utils.TYPE_PREFERENCE, MtkSensor.STRING_TYPE_ACTIVITY);
        mSmdPref = getPreference(Utils.TYPE_PREFERENCE, Sensor.STRING_TYPE_SIGNIFICANT_MOTION);
        mPdrPref = getPreference(Utils.TYPE_PREFERENCE, MtkSensor.STRING_TYPE_PDR);
        mStationaryPref = getPreference(Utils.TYPE_PREFERENCE,
                Sensor.STRING_TYPE_STATIONARY_DETECT);
        mDeviceOritation = getPreference(Utils.TYPE_PREFERENCE,
                Sensor.STRING_TYPE_DEVICE_ORIENTATION);
        mMotionDetect = getPreference(Utils.TYPE_PREFERENCE, Sensor.STRING_TYPE_MOTION_DETECT);
        // Add Google Geatures entrance
        mGoogleGsturePref = Utils.createPreference(Utils.TYPE_PREFERENCE,
                getString(R.string.orginal_gesture_title), KEY_GOOGLE_GESTURE,
                getPreferenceScreen(), this);
        // Add composite entrance
        mCompositePref = Utils.createPreference(Utils.TYPE_PREFERENCE,
                getString(R.string.composite_sensor_title), KEY_COMPOSITE_PREF,
                getPreferenceScreen(), this);
        // Add Fusion sensors entrance
        mFusionPref = Utils.createPreference(Utils.TYPE_PREFERENCE,
                getString(R.string.fusion_sensor_title), KEY_FUSION_PREF, getPreferenceScreen(),
                this);
        mFloorCounter = getPreference(Utils.TYPE_PREFERENCE, MtkSensor.STRING_TYPE_FLOOR_COUNT);
        mFlatPref = getPreference(Utils.TYPE_PREFERENCE, MtkSensor.STRING_TYPE_FLAT);
        mTiltDetector = getPreference(Utils.TYPE_PREFERENCE, Sensor.SENSOR_STRING_TYPE_TILT_DETECTOR);
        mStressTest = Utils.createPreference(Utils.TYPE_PREFERENCE,
                getString(R.string.stress_title), KEY_STRESS_PREF, getPreferenceScreen(),
                this);
    }

    private Preference getPreference(int prefType, String sensorType) {
        Sensor sensor = mSensorKeyMap.get(sensorType);
        if (sensor != null) {
            return Utils.createPreference(prefType, sensor.getName(), sensorType,
                    getPreferenceScreen(), this);
        } else {
            return null;
        }
    }

    private void updatePreferenceStatus() {
        updatePreferenceStatus(mAnswerCall);
        updatePreferenceStatus(mAccelePref);
        updatePreferenceStatus(mLightPref);
        updatePreferenceStatus(mProximityPref);
        updatePreferenceStatus(mStepCounter);
        updatePreferenceStatus(mStepDetector);
        updatePreferenceStatus(mPressure);
        updatePreferenceStatus(mGyroscope);
        updatePreferenceStatus(mMagneticField);
        updatePreferenceStatus(mInPocketPref);
        updatePreferenceStatus(mAcitvityPref);
        updatePreferenceStatus(mPedometerPref);
        updatePreferenceStatus(mPdrPref);
        updatePreferenceStatus(mSmdPref);
        updatePreferenceStatus(mStationaryPref);
        updatePreferenceStatus(mDeviceOritation);
        updatePreferenceStatus(mMotionDetect);
        mGoogleGsturePref
                .setSummary(Utils.getMultiSensorsStatus(Utils.orginalGestureType)
                        ? R.string.running_summary : R.string.space_summary);
        mCompositePref
                .setSummary(Utils.getMultiSensorsStatus(Utils.compositeType)
                        ? R.string.running_summary : R.string.space_summary);
        boolean fisionRunning = Utils.getSensorStatus(Sensor.STRING_TYPE_GAME_ROTATION_VECTOR)
                || Utils.getSensorStatus(Sensor.STRING_TYPE_ORIENTATION);
        mFusionPref
                .setSummary(fisionRunning ? R.string.running_summary
                        : R.string.space_summary);
        updatePreferenceStatus(mFloorCounter);
        updatePreferenceStatus(mFlatPref);
        updatePreferenceStatus(mTiltDetector);
    }

    private void updatePreferenceStatus(Preference pref) {
        if (pref != null) {
            boolean status = Utils.getSensorStatus(pref.getKey());
            if (pref instanceof SwitchPreference) {
                SwitchPreference prefer = (SwitchPreference) pref;
                prefer.setChecked(status);
            } else if (pref instanceof Preference) {
                pref.setSummary(status ? R.string.running_summary : R.string.space_summary);
            }
        }
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        Log.d(TAG, "onPreferenceTreeClick : " + preference.getTitle());
        Intent intent = new Intent();
        if (preference == mGoogleGsturePref) {
            intent.setClass(this, GoogleGesturesActivity.class);
        } else if (preference == mInPocketPref) {
            intent.setClass(this, InPocketSensorActivity.class);
        } else if (preference == mPedometerPref) {
            intent.setClass(this, PedometerSensorActivity.class);
        } else if (preference == mAcitvityPref) {
            intent.setClass(this, ActivitySensorActivity.class);
        } else if (preference == mSmdPref) {
            intent.setClass(this, SmdSensorActivity.class);
        } else if (preference == mCompositePref) {
            intent.setClass(this, CompositeSensorActivity.class);
        } else if (preference == mPdrPref) {
            intent.setClass(this, PdrSensorActivity.class);
        } else if (preference == mStationaryPref) {
            intent.setClass(this, StationarySensorActivity.class);
        } else if (preference == mAccelePref) {
            intent.setClass(this, AccSensorActivity.class);
        } else if (preference == mLightPref) {
            intent.setClass(this, LightSensorActivity.class);
        } else if (preference == mProximityPref) {
            intent.setClass(this, ProximitySensorActivity.class);
        } else if (preference == mStepCounter) {
            intent.setClass(this, StepCounterSensorActivity.class);
        } else if (preference == mStepDetector) {
            intent.setClass(this, StepDetectorSensorActivity.class);
        } else if (preference == mPressure) {
            intent.setClass(this, PressureSensorActivity.class);
        } else if (preference == mMagneticField) {
            intent.setClass(this, MagneticFieldSensorActivity.class);
        } else if (preference == mGyroscope) {
            intent.setClass(this, GyroscopeSensorActivity.class);
        } else if (preference == mFusionPref) {
            intent.setClass(this, FusionSensorActivity.class);
        } else if (preference == mDeviceOritation) {
            intent.setClass(this, DeviceOrientationSensorActivity.class);
        } else if (preference == mMotionDetect) {
            intent.setClass(this, MotionDetectSensorActivity.class);
        } else if (preference == mAnswerCall) {
            intent.setClass(this, AnswerCallSensorActivity.class);
        } else if (preference == mFloorCounter) {
            intent.setClass(this, FloorCounterSensorActivity.class);
        } else if (preference == mFlatPref) {
            intent.setClass(this, FlatSensorActivity.class);
        } else if (preference == mTiltDetector) {
            intent.setClass(this, TiltDetectorSensorActivity.class);
        } else if (preference == mStressTest) {
            intent.setClass(this, SensorStressTestActivity.class);
        } else {
            intent = null;
        }
        if (intent != null) {
            startActivity(intent);
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    @Override
    public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
        // enable or disable log
        Utils.setSensorStatus(Utils.LOG_STATUS, (Boolean) arg1);
        if (mBound) {
            mSensorService.recordLogs((Boolean) arg1);
        }
    }
}
