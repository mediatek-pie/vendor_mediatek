package com.mediatek.connectivity;

import android.app.ExpandableListActivity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupExpandListener;

import com.mediatek.connectivity.CdsEmbmsService.EmbmsBinder;

import java.util.ArrayList;
import java.util.List;

/**
 * Activity for eMBMS sessions list.
 *
 */
public class CdsEmbmsSessionListActivity extends ExpandableListActivity {
    private static final String TAG = "CdsEmbms";

    private CdsEmbmsListAdapter mListAdapter;
    private ExpandableListView mExpListView;
    private List<String> mListDataHeader;
    private List<CdsEmbmsSessionEntry> mListDataChild;
    private CdsEmbmsService mService;

    private int mLastExpandedPosition = -1;
    private boolean mBound;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mExpListView = getExpandableListView();

        mExpListView.setDividerHeight(2);
        mExpListView.setGroupIndicator(null);
        mExpListView.setClickable(true);

        // preparing list data
        prepareListData();
        mListAdapter = new CdsEmbmsListAdapter(this, mListDataHeader, mListDataChild);

        // setting list adapter
        mExpListView.setAdapter(mListAdapter);
        mExpListView.setOnChildClickListener(this);
        mExpListView.setOnGroupExpandListener(new OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                if (mLastExpandedPosition != -1
                        && groupPosition != mLastExpandedPosition) {
                    mExpListView.collapseGroup(mLastExpandedPosition);
                }
                mLastExpandedPosition = groupPosition;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, CdsEmbmsService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    /*
     * Preparing the list data
     */
    private void prepareListData() {
        mListDataHeader = new ArrayList<String>();
        mListDataChild = new ArrayList<CdsEmbmsSessionEntry>();

        SharedPreferences dataStore = this.getSharedPreferences(
                                        CdsEmbmsConsts.EMBMS_FILE, 0);
        for (int i = 0; i < CdsEmbmsConsts.MAX_SESSION_NUM; i ++) {
            // Adding header data.
            String header = getString(R.string.embms) + "-" + (i + 1);
            mListDataHeader.add(header);

            // Adding child data.
            String sessionName = CdsEmbmsConsts.EMBMS_SESSION_INFO + i;
            String sessionInfo = dataStore.getString(sessionName, "");
            CdsEmbmsSessionEntry entry = new CdsEmbmsSessionEntry();
            Log.i(TAG, "prepareListData:" + i + ":" + sessionInfo);
            if (sessionInfo.length() != 0) {
                String[] sessions = sessionInfo.split(CdsEmbmsConsts.SEP);
                for (int j = 0; j < sessions.length; j++) {
                    String s = sessions[j];
                    switch (j) {
                        case 0:
                            boolean v = (("true".equals(s)) ? true : false);
                            entry.setIsEnabled(v);
                            break;
                        case 1:
                            if (s != null) {
                                entry.setTmgi(s);
                            } else {
                                j = sessions.length; // Exit the loop
                            }
                            break;
                        case 2:
                            if (s != null) {
                                entry.setSessionId(s);
                            } else {
                                j = sessions.length; // Exit the loop
                            }
                            break;
                        case 3:
                            if (s != null) {
                                entry.setSaiList(s);
                            }
                            break;
                        case 4:
                            if (s != null) {
                                entry.setFreqList(s);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            mListDataChild.add(entry);
        }
    }


    /** Defines callbacks for service binding, passed to bindService(). */
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className,
                IBinder service) {
            Log.i(TAG, "onServiceConnected");
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            EmbmsBinder binder = (EmbmsBinder) service;
            mService = binder.getService();
            mListAdapter.setSrvBinder(mService);
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
}