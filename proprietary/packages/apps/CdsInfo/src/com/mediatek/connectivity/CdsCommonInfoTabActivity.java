/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.connectivity;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Common Class for fragment activity.
 *
 */
public class CdsCommonInfoTabActivity extends ListActivity  {
    private static final String TAG = "CdsCommonInfoTabActivity";

    private static FunctionActivity[] sFunctionActivities;

    private class FunctionActivity {
        private String mTitle;
        private Class<? extends Activity> mActivityClass;

        public FunctionActivity(String name, Class<? extends Activity> activityClass) {
            this.mActivityClass = activityClass;
            this.mTitle = name;
        }

        @Override
        public String toString() {
            return mTitle;
        }
    }

    public static final String TAB_STRINGS[] = {
                "Framework Service", "Native Network",
                "Traffic"};
    public static final Class TAB_INTENT_CLASS_NAMES[] = {
                com.mediatek.connectivity.CdsFrameworkSrvActivity.class,
                com.mediatek.connectivity.CdsNatvieNetworkSrvActivity.class,
                com.mediatek.connectivity.CdsTrafficStatActivity.class};

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cds_common_info);

        // Instantiate the list of FunctionActivity
        sFunctionActivities = new FunctionActivity[]{
                new FunctionActivity(TAB_STRINGS[0], TAB_INTENT_CLASS_NAMES[0]),
                new FunctionActivity(TAB_STRINGS[1], TAB_INTENT_CLASS_NAMES[1]),
                new FunctionActivity(TAB_STRINGS[2], TAB_INTENT_CLASS_NAMES[2]),
        };

        setListAdapter(new ArrayAdapter<FunctionActivity>(this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                sFunctionActivities));
    }

    @Override
    protected void onListItemClick(ListView listView, View view, int position, long id) {
        // Launch the sample associated with this list position.
        startActivity(new Intent(CdsCommonInfoTabActivity.this,
                    sFunctionActivities[position].mActivityClass));
    }
}