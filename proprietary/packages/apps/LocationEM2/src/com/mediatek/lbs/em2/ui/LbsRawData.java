package com.mediatek.lbs.em2.ui;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ToggleButton;
import android.widget.TextView;
import android.widget.LinearLayout;

import android.location.GnssMeasurementsEvent;
import android.location.LocationManager;

import java.util.Collection;
import android.location.GnssMeasurement;
import android.location.GnssNavigationMessage;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;


public class LbsRawData extends Activity {

    protected ToggleButton mToggleButtonMeasurement;
    protected ToggleButton mToggleButtonNavigation;
    protected ToggleButton mToggleButtonRegress;
    protected TextView mTextViewMessage;
    protected TextView mTextViewLogNavi;
    protected TextView mTextViewLogMeas;
    protected LinearLayout mLayoutDebug;
    private LocationManager mLocationManager = null;
    private int mMeasuNum = 0;
    private int mNaviNum = 0;
    private String mMeasFileName = "";
    private String mNaviFileName = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.raw);
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        initUI();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private GnssMeasurementsEvent.Callback mMeasurementCallback =
            new GnssMeasurementsEvent.Callback() {
        @Override
        public void onGnssMeasurementsReceived(GnssMeasurementsEvent eventArgs) {
            mMeasuNum++;

            Collection<GnssMeasurement>  mReadOnlyMeasurements = eventArgs.getMeasurements();

            //output file
            int mSatNum = 0;
            String strout = new String();
            String str = new String();
            strout = getTimeString2(new Date().getTime()) +
                    ",getClock: " + eventArgs.getClock() + "\n";
            for (GnssMeasurement measurement : mReadOnlyMeasurements) {
                strout +=  "meas:[" + (mSatNum) +  "] :" + measurement + "\n";
                str += " Svid=" + measurement.getSvid() + " STA=" + measurement.getState() + "\n";
                mSatNum++;
            }
            write2File("MEAS", "MEAS_" + mMeasFileName  + ".txt", strout, true, false);


            //logout
            str = "Measurement Count=" + mMeasuNum + "\n" +
                    "Satellite Num=" + mSatNum + "\n" + str;
            log(str);
            //mTextViewLogMeas.setText(str); //can't be accessed by non UI thread


        }

        public void onStatusChanged(int status) {}
    };

    private GnssNavigationMessage.Callback mNavigationCallback =
            new GnssNavigationMessage.Callback() {
        @Override
        public void onGnssNavigationMessageReceived(
                GnssNavigationMessage naviMsg) {
            mNaviNum++;

            //output file
            String strout = new String();
            strout = getTimeString2(new Date().getTime()) + ":" + naviMsg + "\n";
            write2File("NAVI", "NAVI_" + mNaviFileName  + ".txt", strout, true, false);

            String str = new String();
            //logout
            str += "Navigation Count=" + mNaviNum + " Svid=" + naviMsg.getSvid() +
                    " Type=" + naviMsg.getType() + "\n";
            log("" + str);
            //mTextViewLogNavi.setText(str); //can't be accessed by non UI thread

        }

        public void onStatusChanged(int status) {}
    };

    protected void initUI() {
        mToggleButtonMeasurement = (ToggleButton) findViewById(R.id.toggleButton_measurement);
        mToggleButtonNavigation = (ToggleButton) findViewById(R.id.toggleButton_navigation);
        mToggleButtonRegress = (ToggleButton) findViewById(R.id.toggleButton_regress);
        mTextViewLogNavi       = (TextView) findViewById(R.id.TextView_navi);
        mTextViewLogMeas       = (TextView) findViewById(R.id.TextView_meas);
        mLayoutDebug = (LinearLayout) findViewById(R.id.LinearLayout_Debug);

        mLayoutDebug.setVisibility(View.VISIBLE);

        mToggleButtonRegress.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean result = true;
                if (mToggleButtonRegress.isChecked()) {
                    mToggleButtonMeasurement.performClick();
                    mToggleButtonNavigation.performClick();
                    mToggleButtonMeasurement.setEnabled(false);
                    mToggleButtonNavigation.setEnabled(false);
                } else {
                    mToggleButtonMeasurement.performClick();
                    mToggleButtonNavigation.performClick();
                    mToggleButtonMeasurement.setEnabled(true);
                    mToggleButtonNavigation.setEnabled(true);
                }
                String str = "regress Opt:" + mToggleButtonRegress.isChecked();
                log(str + " result:" + result);
            }
        });

        mToggleButtonMeasurement.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean result = true;
                if (mToggleButtonMeasurement.isChecked()) {
                    mMeasuNum = 0;
                    result = mLocationManager
                            .registerGnssMeasurementsCallback(mMeasurementCallback);
                    mMeasFileName = getTimeString2(new Date().getTime());

                } else {
                    mLocationManager.unregisterGnssMeasurementsCallback(mMeasurementCallback);
                }
                String str = "Measurement Opt:" + mToggleButtonMeasurement.isChecked();
                mTextViewLogMeas.setText(str);
                log(str + " result:" + result);
            }
        });

        mToggleButtonNavigation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean result = true;
                if (mToggleButtonNavigation.isChecked()) {
                    mNaviNum = 0;
                    result = mLocationManager
                            .registerGnssNavigationMessageCallback(mNavigationCallback);
                    mNaviFileName = getTimeString2(new Date().getTime());
                } else {
                    mLocationManager.unregisterGnssNavigationMessageCallback(mNavigationCallback);
                }
                String str = "Navigation Opt:" + mToggleButtonNavigation.isChecked();
                mTextViewLogNavi.setText(str);
                log(str + " result:" + result);
            }
        });

    }

    private String getTimeString2(long milliseconds) {
        Date date = new Date(milliseconds);
        String str = String.format("%04d%02d%02d_%02d%02d%02d",
                (date.getYear() + 1900),
                (date.getMonth() + 1),
                date.getDate(),
                date.getHours(),
                date.getMinutes(),
                date.getSeconds());
        return str;
    }

    //true: success
    public boolean write2File(String folder, String fileName,
            String data, boolean isAppendMode, boolean toSdcard) {
        DataOutputStream dos;
        String fullFileName;
        if (toSdcard) {
            fullFileName = "/sdcard/" + folder;
        } else {
            fullFileName = "/data/data/" + this.getPackageName() + "/" + folder;
        }
        File f = new File(fullFileName);
        f.mkdir();
        fullFileName += "/" + fileName;
        f = new File(fullFileName);
        try {
            if (isAppendMode) {
                dos = new DataOutputStream(new FileOutputStream(fullFileName, true));
            } else {
                dos = new DataOutputStream(new FileOutputStream(f));
            }
            dos.writeBytes(data);
            dos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private static final String PROPERTIES_FILE = "/data/misc/gps.conf";

    private static String getGPSConfig(String key) {
        String result = null;
        Properties mProperties = new Properties();
        try {
            File file = new File(PROPERTIES_FILE);
            FileInputStream stream = new FileInputStream(file);
            mProperties.load(stream);
            stream.close();

            result = mProperties.getProperty(key);
            log("getConfig key:" + key + " val:" + result);
        } catch (IOException e) {
            log("getConfig fail key:" + key + " ex:" + e);
        }

        return result;
    }

    protected static void log(Object msg) {
        Log.d("LocationEM", "" + msg);
    }

    protected static void logw(Object msg) {
        Log.d("LocationEM", "[agps] WARNING: " + msg);
    }
}
