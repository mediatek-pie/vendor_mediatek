package com.google.android.systemui;

import android.content.Context;
import android.util.ArrayMap;

import com.android.systemui.Dependency;
import com.android.systemui.Dependency.DependencyProvider;
import com.android.systemui.SystemUIFactory;
import com.android.systemui.assist.AssistManager;
import com.android.systemui.statusbar.policy.DeviceProvisionedController;

/**
 * Factory for Google specific behavior classes.
 */
public class SystemUIGoogleFactory extends SystemUIFactory {

    @Override
    public void injectDependencies(ArrayMap<Object, DependencyProvider> providers,
            Context context) {
        super.injectDependencies(providers, context);
        providers.put(AssistManager.class, () -> new AssistManagerGoogle(
                Dependency.get(DeviceProvisionedController.class), context));
    }
}
