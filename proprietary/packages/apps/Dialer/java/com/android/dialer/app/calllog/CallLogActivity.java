/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.dialer.app.calllog;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.CallLog.Calls;
import android.support.annotation.VisibleForTesting;
import android.support.design.widget.Snackbar;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import com.android.contacts.common.list.ViewPagerTabs;
import com.android.dialer.app.DialtactsActivity;
import com.android.dialer.app.R;
import com.android.dialer.calldetails.CallDetailsActivity;
import com.android.dialer.common.Assert;
import com.android.dialer.common.LogUtil;
import com.android.dialer.constants.ActivityRequestCodes;
import com.android.dialer.database.CallLogQueryHandler;
import com.android.dialer.logging.Logger;
import com.android.dialer.logging.ScreenEvent;
import com.android.dialer.logging.UiAction;
import com.android.dialer.performancereport.PerformanceReport;
import com.android.dialer.postcall.PostCall;
import com.android.dialer.util.TransactionSafeActivity;
import com.android.dialer.util.ViewUtil;

import com.mediatek.dialer.calllog.CallLogMultipleDeleteActivity;
import com.mediatek.dialer.ext.ExtensionManager;
import com.mediatek.dialer.ext.ICallLogAction;

/** Activity for viewing call history. */
public class CallLogActivity extends TransactionSafeActivity
    implements ViewPager.OnPageChangeListener, ICallLogAction {

  @VisibleForTesting static final int TAB_INDEX_ALL = 0;
  @VisibleForTesting static final int TAB_INDEX_MISSED = 1;
  private static final int TAB_INDEX_COUNT =
      /// M: for Plug-in @{
      ExtensionManager.getCallLogExtension().getTabIndexCount();
      ///@}

  private ViewPager viewPager;
  private ViewPagerTabs viewPagerTabs;
  private ViewPagerAdapter viewPagerAdapter;
  private CallLogFragment allCallsFragment;
  private CallLogFragment missedCallsFragment;
  /// M: for Plugin-in @{
  /// Google code:
  /// private String[] tabTitles;
  private CharSequence[] tabTitles;
  ///@}
  private boolean isResumed;
  private int selectedPageIndex;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    /// M: for Plug-in. @{
    ExtensionManager.getCallLogExtension().onCreate(this, savedInstanceState);
    /// @}

    setContentView(R.layout.call_log_activity);
    getWindow().setBackgroundDrawable(null);

    final ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayShowHomeEnabled(true);
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setDisplayShowTitleEnabled(true);
    actionBar.setElevation(0);

    int startingTab = TAB_INDEX_ALL;
    final Intent intent = getIntent();
    if (intent != null) {
      final int callType = intent.getIntExtra(CallLog.Calls.EXTRA_CALL_TYPE_FILTER, -1);
      if (callType == CallLog.Calls.MISSED_TYPE) {
        startingTab = TAB_INDEX_MISSED;
      }
    }
    selectedPageIndex = startingTab;

    /// M: for Plugin-in @{
    /// Google code:
    /// tabTitles = new String[TAB_INDEX_COUNT];
    tabTitles = new CharSequence[TAB_INDEX_COUNT];
    /// @}
    tabTitles[0] = getString(R.string.call_log_all_title);
    tabTitles[1] = getString(R.string.call_log_missed_title);

    viewPager = (ViewPager) findViewById(R.id.call_log_pager);

    viewPagerAdapter = new ViewPagerAdapter(getFragmentManager());
    viewPager.setAdapter(viewPagerAdapter);
    viewPager.setOffscreenPageLimit(1);

    /// M: for Plug-in. @{
    ExtensionManager.getCallLogExtension().initCallLogTab(tabTitles, viewPager);
    /// @}

    viewPager.setOnPageChangeListener(this);

    viewPagerTabs = (ViewPagerTabs) findViewById(R.id.viewpager_header);

    viewPagerTabs.setViewPager(viewPager);
    viewPager.setCurrentItem(startingTab);

    /** M: Fix CR ALPS03452486. Save and restore the fragments. @{ */
    restoreFragments(savedInstanceState);
    /** @} */
  }

  @Override
  protected void onResume() {
    // Some calls may not be recorded (eg. from quick contact),
    // so we should restart recording after these calls. (Recorded call is stopped)
    PostCall.restartPerformanceRecordingIfARecentCallExist(this);
    if (!PerformanceReport.isRecording()) {
      PerformanceReport.startRecording();
    }

    isResumed = true;
    super.onResume();
    sendScreenViewForChildFragment();

    /// M: for Plug-in. @{
    ExtensionManager.getCallLogExtension().onResume(this);
    /// @}
  }

  @Override
  protected void onPause() {
    isResumed = false;
    super.onPause();
  }

  @Override
  protected void onStop() {
    if (!isChangingConfigurations() && viewPager != null) {
      // Make sure current index != selectedPageIndex
      selectedPageIndex = -1;
      updateMissedCalls(viewPager.getCurrentItem());
    }
    super.onStop();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    final MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.call_log_options, menu);
    /// M: for Plug-in. @{
    ExtensionManager.getCallLogExtension()
        .createCallLogMenu(this, menu, viewPagerTabs, this);
    /// @}
    return true;
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    final MenuItem itemDeleteAll = menu.findItem(R.id.delete_all);
    if (allCallsFragment != null && itemDeleteAll != null) {
      // If onPrepareOptionsMenu is called before fragments are loaded, don't do anything.
      final CallLogAdapter adapter = allCallsFragment.getAdapter();
      /** M: Fix CR ALPS01884065. The isEmpty() be overrided with loading state of data.
       *  Here, it should not care about the loading state. So, use getCount() to check
       *  is the adapter really empty. @{ */
      itemDeleteAll.setVisible(adapter != null && adapter.getItemCount() > 0);
    }

    /// M: [Multi-Delete] for CallLog multiple delete @{
    final MenuItem itemDelete = menu.findItem(R.id.delete);
    CallLogFragment fragment = getCurrentCallLogFragment();

    /// M: for plug-in set the current call log fragment@{
    ExtensionManager.getCallLogExtension().setCurrentCallLogFragment(this,
                            getRtlPosition(viewPager.getCurrentItem()), fragment);
    ///@}

    if (fragment != null && itemDelete != null) {
      final CallLogAdapter adapter = fragment.getAdapter();
      itemDelete.setVisible(adapter != null && adapter.getItemCount() > 0);
    }
    ///@}

    /// M: for plug-in. @{
    ExtensionManager.getCallLogExtension().prepareCallLogMenu(this,
        menu, fragment, itemDeleteAll,
        fragment != null ? fragment.getAdapter().getItemCount() : 0);
    ///@}
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (!isSafeToCommitTransactions()) {
      return true;
    }

    if (item.getItemId() == android.R.id.home) {
      PerformanceReport.recordClick(UiAction.Type.CLOSE_CALL_HISTORY_WITH_CANCEL_BUTTON);
      final Intent intent = new Intent(this, DialtactsActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      startActivity(intent);
      return true;
    } else if (item.getItemId() == R.id.delete_all) {
      ClearCallLogDialog.show(getFragmentManager());
      return true;
    /// M: [Multi-Delete] for CallLog multiple delete @{
    } else if (item.getItemId() == R.id.delete) {
      final Intent delIntent = new Intent(this, CallLogMultipleDeleteActivity.class);
      delIntent.putExtra(CallLogQueryHandler.CALL_LOG_TYPE_FILTER, getCurrentCallLogFilteType());
      startActivity(delIntent);
      return true;
    }
    ///@}
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    viewPagerTabs.onPageScrolled(position, positionOffset, positionOffsetPixels);
  }

  @Override
  public void onPageSelected(int position) {
    updateMissedCalls(position);
    selectedPageIndex = position;
    if (isResumed) {
      sendScreenViewForChildFragment();
    }
    viewPagerTabs.onPageSelected(position);
  }

  @Override
  public void onPageScrollStateChanged(int state) {
    viewPagerTabs.onPageScrollStateChanged(state);
  }

  private void sendScreenViewForChildFragment() {
    Logger.get(this).logScreenView(ScreenEvent.Type.CALL_LOG_FILTER, this);
  }

  private int getRtlPosition(int position) {
    if (ViewUtil.isRtl()) {
      return viewPagerAdapter.getCount() - 1 - position;
    }
    return position;
  }

  private void updateMissedCalls(int position) {
    if (position == selectedPageIndex) {
      return;
    }

    /// M: for plug-in. @{
    if (ExtensionManager.getCallLogExtension().updateMissedCalls(
        getRtlPosition(position))) {
      return;
    }
    /// @}

    switch (getRtlPosition(position)) {
      case TAB_INDEX_ALL:
        if (allCallsFragment != null) {
          allCallsFragment.markMissedCallsAsReadAndRemoveNotifications();
        }
        break;
      case TAB_INDEX_MISSED:
        if (missedCallsFragment != null) {
          missedCallsFragment.markMissedCallsAsReadAndRemoveNotifications();
        }
        break;
      default:
        throw Assert.createIllegalStateFailException("Invalid position: " + position);
    }
  }

  @Override
  public void onBackPressed() {
    PerformanceReport.recordClick(UiAction.Type.PRESS_ANDROID_BACK_BUTTON);
    super.onBackPressed();
  }

  /** Adapter for the view pager. */
  public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
      super(fm);
    }

    @Override
    public long getItemId(int position) {
      return getRtlPosition(position);
    }

    @Override
    public Fragment getItem(int position) {

      /// M: for plug-in. @{
      Fragment fragment = null;
      if ((fragment = ExtensionManager.getCallLogExtension()
          .getCallLogFragmentItem(position)) != null) {
        return fragment;
      }
      ///@}

      switch (getRtlPosition(position)) {
        case TAB_INDEX_ALL:
          return new CallLogFragment(
              CallLogQueryHandler.CALL_TYPE_ALL, true /* isCallLogActivity */);
        case TAB_INDEX_MISSED:
          return new CallLogFragment(Calls.MISSED_TYPE, true /* isCallLogActivity */);
        default:
          throw new IllegalStateException("No fragment at position " + position);
      }
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
      final CallLogFragment fragment = (CallLogFragment) super.instantiateItem(container, position);
      /// M: for plug-in. @{
      Object obj = ExtensionManager.getCallLogExtension().instantiateCallLogFragmentItem(
          CallLogActivity.this , getRtlPosition(position), fragment);
      if (obj != null) {
        LogUtil.d("CallLogActivity.instantiateItem", "Init by plugin");
        return fragment;
      }
      ///@}

      switch (getRtlPosition(position)) {
        case TAB_INDEX_ALL:
          allCallsFragment = fragment;
          break;
        case TAB_INDEX_MISSED:
          missedCallsFragment = fragment;
          break;
        default:
          throw Assert.createIllegalStateFailException("Invalid position: " + position);
      }
      return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
      return tabTitles[position];
    }

    @Override
    public int getCount() {
      return TAB_INDEX_COUNT;
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == ActivityRequestCodes.DIALTACTS_CALL_DETAILS) {
      if (resultCode == RESULT_OK
          && data != null
          && data.getBooleanExtra(CallDetailsActivity.EXTRA_HAS_ENRICHED_CALL_DATA, false)) {
        String number = data.getStringExtra(CallDetailsActivity.EXTRA_PHONE_NUMBER);
        Snackbar.make(findViewById(R.id.calllog_frame), getString(R.string.ec_data_deleted), 5_000)
            .setAction(
                R.string.view_conversation,
                v -> startActivity(IntentProvider.getSendSmsIntentProvider(number).getIntent(this)))
            .setActionTextColor(getResources().getColor(R.color.dialer_snackbar_action_text_color))
            .show();
      }
    }

    /// M: for plug-in. @{
    ExtensionManager.getCallLogExtension().onActivityResult(
        requestCode, resultCode, data);
    /// @}

    super.onActivityResult(requestCode, resultCode, data);
  }

  //M: -------------------------- MediaTek feature -----------------------------------

  /**
   * M: Called to restore fragment when calllog created.
   * @param savedInstanceState Bundle
   */
  private void restoreFragments(Bundle savedInstanceState) {
    LogUtil.d("CallLogActivity.restoreFragments", "savedInstanceState= " + savedInstanceState);

    /// M: for plug-in. @{
    ExtensionManager.getCallLogExtension().restoreFragments(savedInstanceState, this,
        viewPagerAdapter, viewPagerTabs);
    /// @}
  }


  /**
   * M: Called to get current CallLogFragment.
   * @return CallLogFragment if index match
   */
  public CallLogFragment getCurrentCallLogFragment() {
    int position = viewPager.getCurrentItem();
    position = getRtlPosition(position);

    /// M: for plug-in. @{
    CallLogFragment calllogFragment =
        (CallLogFragment) ExtensionManager.getCallLogExtension().getCurrentFragment(position);
    /// @}

    if (calllogFragment != null) {
      return calllogFragment;
    }

    if (position == TAB_INDEX_ALL) {
      return allCallsFragment;
    } else if (position == TAB_INDEX_MISSED) {
      return missedCallsFragment;
    }
    return null;
  }

  /**
   * M: Called to get current calllog type
   * @return int if calllog type
   */
  public int getCurrentCallLogFilteType() {
    int position = viewPager.getCurrentItem();
    position = getRtlPosition(position);

    /// M: for plug-in. @{
    int type = ExtensionManager.getCallLogExtension().getCurrentCallLogFilteType(position);
    if (type >=0) {
      return type;
    }
    /// @}

    if (position == TAB_INDEX_ALL) {
      return CallLogQueryHandler.CALL_TYPE_ALL;
    } else if (position == TAB_INDEX_MISSED) {
      return Calls.MISSED_TYPE;
    }
    return CallLogQueryHandler.CALL_TYPE_ALL;
  }

  /**
   * M: Called to save instance of CalllogFragment created in plugin.
   * @param outState Bundle
   */
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    /// M: for plug-in. @{
    ExtensionManager.getCallLogExtension().onSaveInstanceState(this, outState);
    /// @}
  }

  /**
   * M: Called when plugin select phone account to show the calllog.
   */
  public void updateCallLogScreen() {
    if (allCallsFragment != null) {
      allCallsFragment.forceToRefreshData();
    }
  }

  /// M: for Plug-in @{
  @Override
  public void processBackPressed() {
  }

  /**
   * M: Called in plugin to check if the activity is resumed.
   * @return true if the activity is resumed
   */
  public boolean isActivityResumed() {
    return isResumed;
  }

  /**
   * M: Called to clear activity hashmap in plugin.
   */
  @Override
  protected void onDestroy() {
    /// M: for plug-in. @{
    ExtensionManager.getCallLogExtension().onDestroy(this);
    /// @}
    super.onDestroy();
  }
}
