/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.dialer.ext;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.telecom.PhoneAccountHandle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

public interface ICallLogExtension {

    /**
     * for op01.
     * called when host create menu, to add plug-in own menu here
     * @param activity Activity
     * @param menu Menu
     * @param tabs the ViewPagerTabs used in activity
     * @param callLogAction callback plug-in need if things need to be done by host
     * @internal
     */
    void createCallLogMenu(Activity activity, Menu menu, HorizontalScrollView tabs,
            ICallLogAction callLogAction);

    /**
     * for op01.
     * called when host prepare menu, prepare plug-in own menu here
     * @param activity the current activity
     * @param menu the Menu Created
     * @param fragment the current fragment
     * @param itemDeleteAll the optionsmenu delete all item
     * @param adapterCount adapterCount
     * @internal
     */
    public void prepareCallLogMenu(Activity activity, Menu menu,
            Fragment fragment, MenuItem itemDeleteAll, int adapterCount);

    /**
     * for op01.
     * called when call log query, plug-in should customize own query here
     * @param typeFiler current query type
     * @param builder the query selection Stringbuilder, modify to change query selection
     * @param selectionArgs the query selection args, modify to change query selection
     * @internal
     */
    void appendQuerySelection(int typeFiler, StringBuilder builder, List<String> selectionArgs);

    /**
     * for op01.
     * @param activity the current activity
     * @param outState save state
     * @internal
     */
    void onSaveInstanceState(Activity activity, Bundle outState);


    /**
     * For op01.
     * plug-in manage the state and unregister receiver
     * @param activity the current activity
     * @internal
     */
    public void onDestroy(Activity activity);

    /**
     * For op01.
     * plug-in manage the state and unregister receiver
     * @param fragment the current fragment
     * @internal
     */
    public void onDestroy(Fragment fragment);

    /**
     * For op01.
     * plug-in init the reject mode in the host
     * @param activity the current activity
     * @param bundle bundle
     * @internal
     */
    public void onCreate(Activity activity, Bundle bundle);

    /**
     * For op01.
     * plug-in init calllog fragment reference in plugin
     * @param fragment the current fragment
     * @param bundle bundle
     * @internal
     */
    public void onCreate(Fragment fragment, Bundle bundle);

    /**
     * For op01.
     * plug-in insert auto reject icon resource for dialer search
     * @param callTypeDrawable callTypeDrawable
     * @param useLargeIcons  need large icon or small icon
     * @param context to get host call type icon size
     * @internal
     */
    public void addResourceForDialerSearch(HashMap<Integer, Drawable>
        callTypeDrawable, boolean useLargeIcons, Context context);


    /** Define all the parameters order that pass to plugin. */
    public static final int VIDEO_BUTTON_PARAMS_INDEX_PLACE_CALL = 0;
    public static final int VIDEO_BUTTON_PARAMS_INDEX_IS_VIDEO_SHOWN = 1;
    public static final int VIDEO_BUTTON_PARAMS_INDEX_CALL_TYPE_ICONS = 2;
    public static final int VIDEO_BUTTON_PARAMS_INDEX_NUMBER = 3;
    public static final int VIDEO_BUTTON_PARAMS_INDEX_CONTACT_LOOKUP_URI = 4;

    /**
    /**
     * For OP18.
     * plug-for Draw VoWifi & VoVolte Call Icon
     * @param scaledHeight int
     */
    public void drawWifiVolteCallIcon(int scaledHeight);

    /**
     * For OP18.
     * plug-in for Draw VoWifi & VoVolte Canvas
     * @param left int
     * @param canvas Canvas
     * @param callTypeIconViewObj Object
     */
    public void drawWifiVolteCanvas(int left, Canvas canvas,
                     Object callTypeIconViewObj);

    /**
     * For OP18.
     * plug-in for Show VoWifi & VoVolte Call Icon
     * @param object Object
     * @param features int
     */
    public void setShowVolteWifi(Object object, int features);

    /**
     * For OP18.
     * plug-in to group Call log according to number and Call Feature
     * @param cursor Cursor
     * @return boolean true or false
     */
    public boolean sameCallFeature(Cursor cursor);


    /**
     * For OP18.
     * Customize Bind action buttons
     * @param object Object
     */
     public void customizeBindActionButtons(Object object);

     /**
      * Request capability when tap call log item.
      * @param number String
      */
     void onExpandViewHolderActions(String number);

     /**.
      * For OP01
      * Called when click expand view in calllist item view holder
      * @param obj the host calllist item view holder
      * @param show whether show action
      */
     void showActions(Object obj, boolean show);

    /**.
     * for OP02
     * plug-in handle Activity Result
     * @param requestCode requestCode
     * @param resultCode resultCode
     * @param data the intent return by setResult
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data);

    /**.
     * for OP02
     * plug-in refresh the CallLogFragment to show th notice
     * @param fragment the current fragment
     */
    public void updateNotice(Fragment fragment);

    /**.
     * for OP02
     * plug-in to create account info in calllog fragment
     * @param fragment the current fragment
     * @param view the current view
     */
    public void onViewCreated(Fragment fragment, View view);

    /**
     * for OP12, OP02,OP09 multi tabs.
     * Get Tab Index Count
     * @return int
     */
   int getTabIndexCount();

   /**
    * for OP02,OP09 multi tabs.
    * Init call Log tab
    * @param tabTitles tabTitles
    * @param viewPager viewPager
    */
    void initCallLogTab(CharSequence[] tabTitles, ViewPager viewPager);

   /**
    * for OP02,OP09 multi tabs.
    * Set the Current CallLog Fragment
    * @param activity activity
    * @param position position
    * @param fragment fragment
    */
    void setCurrentCallLogFragment(Activity activity, int position,
                          Fragment fragment);

  /**
    * for OP02,OP09 multi tabs.
    * Get the CallLog Item
    * @param position position
    * @return Fragment
    */
   Fragment getCallLogFragmentItem(int position);

   /**
     * for OP12, OP02,OP09 multi tabs.
     * Instantiate CallLog Item
     * @param activity activity
     * @param position position
     * @param fragment fragment
     * @return Object if plugin init the item return obj, else return null
     */
   Object instantiateCallLogFragmentItem(Activity activity, int position,
                               Fragment fragment);

    /**
     * for op09
     * Called to restore fragment when created
     * @param bundle the create of bundle
     * @param context the current context
     * @param pagerAdapter the view pager adapter used in activity
     * @param tabs the ViewPagerTabs used in activity
     */
    public void restoreFragments(Bundle bundle, Context context,
            FragmentPagerAdapter pagerAdapter, HorizontalScrollView tabs);

    /**
     * for op09
     * Get current fragment by index
     * @param pos int
     * @return Fragment
     */
     public Fragment getCurrentFragment(int index);

     /**
      * for op09
      * Get current calllog type by position
      * @param pos int
      * @return int
      */
     public int getCurrentCallLogFilteType(int pos);

     /**
      * for op09
      * called when CalllogActivity resume
      * @param activity the current activity
      */
     public void onResume(Activity activity);

     /**
      * for op02/op09
      * called when selec other page to clear missed call
      * @param index the index of fragment
      * @return boolean if plugin processed return true, or else return false to process in host
      */
     public boolean updateMissedCalls(int index);

     /**
      * for op02/op09
      * called to update empty message
      * @param fragment the CallLogFragment
      * @param filter the type of call filter
      * @return boolean if plugin processed return true, or else return false to process in host
      */
     public boolean updateEmptyMessage(Fragment fragment, int filter);

     /**
      * for op01
      * called when CallLogItemView attached
      * @param Object the CallLogItemView
      */
     public void onAttachedToWindow(Fragment fragment, Object obj);

     /**
      * for op01
      * called when CallLogItemView detached
      * @param Object the CallLogItemView
      */
     public void onDetachedFromWindow(Object obj);

    /**
     * Plug-in get call type icon.
     * @param callType int
     * @param res Object
     * @return return the call type icon or null
     */
     Drawable getCallTypeDrawable(int callType, Object res);

    /**
     * for op01.
     * whether need pre show primary action view or not. op01 need update callback button,
     * there will be flickering phenomena if callback button pre show.
     *  return true if need show primary action view in advance
     */
    boolean isNeedPreShowPrimaryUi();
}
