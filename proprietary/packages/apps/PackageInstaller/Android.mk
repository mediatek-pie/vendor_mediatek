LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_USE_AAPT2 := true

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := \
    $(call all-java-files-under, src)

LOCAL_STATIC_ANDROID_LIBRARIES += \
    android-support-car \
    $(ANDROID_SUPPORT_DESIGN_TARGETS) \
    android-support-transition \
    android-support-compat \
    android-support-media-compat \
    android-support-core-utils \
    android-support-core-ui \
    android-support-fragment \
    android-support-v7-appcompat \
    android-support-v7-preference \
    android-support-v7-recyclerview \
    android-support-v14-preference \
    android-support-v17-leanback \
    android-support-v17-preference-leanback \
    car-list \
    car-stream-ui-lib \
    SettingsLib

LOCAL_STATIC_JAVA_LIBRARIES := \
    xz-java \
    android-support-annotations \
    services.core

#M: add mediatek-cta
LOCAL_JAVA_LIBRARIES := mediatek-cta \
                        mediatek-framework
#M: change apk name
LOCAL_PACKAGE_NAME := MtkPackageInstaller
LOCAL_CERTIFICATE := platform

LOCAL_PRIVILEGED_MODULE := true
#M: override AOSP PackageInstaller by MtkPackageInstaller
LOCAL_OVERRIDES_PACKAGES := PackageInstaller

LOCAL_PROGUARD_ENABLED := disabled
#LOCAL_PROGUARD_FLAG_FILES := proguard.flags

# Comment for now unitl all private API dependencies are removed
# LOCAL_SDK_VERSION := system_current
LOCAL_PRIVATE_PLATFORM_APIS := true

include $(BUILD_PACKAGE)
