package com.mediatek.mms.ext;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public interface IOpSmsReceiverExt {
    /**
     * @internal
     */
    void onReceiveWithPrivilege(Intent intent, String action);

    Bundle getSmsExtraParams(Context context, int subId);

    int getSmsEncodingType(Context context);
}
