package com.mediatek.mms.ext;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class DefaultOpSmsReceiverExt extends ContextWrapper implements
        IOpSmsReceiverExt {

    public DefaultOpSmsReceiverExt(Context base) {
        super(base);
    }

    @Override
    public void onReceiveWithPrivilege(Intent intent, String action) {
    }

    @Override
    public Bundle getSmsExtraParams(Context context, int subId) {
        return null;
    }

    @Override
    public int getSmsEncodingType(Context context) {
        return SmsMessage.ENCODING_UNKNOWN;
    }
}
