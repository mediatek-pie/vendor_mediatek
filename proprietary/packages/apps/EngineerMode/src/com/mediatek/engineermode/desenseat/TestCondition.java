package com.mediatek.engineermode.desenseat;


import android.app.KeyguardManager;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.PowerManager;
import android.provider.Settings;

import com.mediatek.engineermode.EmUtils;

/**
 * Class to control test condition.
 * @author mtk80357
 *
 */
class TestCondition {
    private static final String TAG = "DesenseAT/TestCondition";
    private boolean mWifiEnable;
    private boolean mBtEnable;
    private WifiManager mWifiManager = null;
    private BluetoothAdapter mBtAdapter = null;
    private KeyguardManager.KeyguardLock mKeyguardLock = null;
    private PowerManager.WakeLock mWakeLock = null;
    private boolean mAirplaneMode;

    void init(Context context) {
        mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        KeyguardManager keyguardMgr = (KeyguardManager) context
                .getSystemService(Context.KEYGUARD_SERVICE);
        mKeyguardLock = keyguardMgr.newKeyguardLock(TAG);

    }

    void setCondition(Context context) {
        //Disable wifi
        mWifiEnable = mWifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLED;
        if (mWifiEnable) {
            Util.switchWifi(context, false);
        }
        //Disable BT
        mBtEnable = mBtAdapter.getState() == BluetoothAdapter.STATE_ON;
        if (mBtEnable) {
            Util.switchBt(false);
        }
        EmUtils.initPoweroffmdMode(true,true);
        //Enable airplanemode
        mAirplaneMode = Settings.System.getInt(context.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        if (!mAirplaneMode) {
            Util.switchAirplaneMode(context, true);
        }
        //Disable keyguard
        mKeyguardLock.disableKeyguard();
        //Keep screen on
//        mWakeLock.acquire();
    }

    void resetCondition(Context context) {
        Util.switchWifi(context, mWifiEnable);
        Util.switchBt(mBtEnable);
        Util.switchAirplaneMode(context, mAirplaneMode);
        EmUtils.initPoweroffmdMode(false,true);
        mKeyguardLock.reenableKeyguard();
//        mWakeLock.release();
    }

}
