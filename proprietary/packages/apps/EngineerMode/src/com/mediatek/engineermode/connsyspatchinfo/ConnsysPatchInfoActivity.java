package com.mediatek.engineermode.connsyspatchinfo;


import android.app.Activity;
import android.os.Bundle;
import android.os.SystemProperties;
import android.widget.TextView;

import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.R;

public class ConnsysPatchInfoActivity extends Activity  {

    private static final String TAG = "ConnsysPatchInfo";
    private static final String PROPERTY = "persist.vendor.connsys.patch.version";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connsys_patch_info);
        String strVer = SystemProperties.get(PROPERTY);
        Elog.i(TAG, "version:" + strVer);
        TextView text = (TextView) findViewById(R.id.rom_patch_ver_tv);
        if ((strVer != null) && (!strVer.isEmpty())) {
            text.setText(strVer);
        }
    }
}
