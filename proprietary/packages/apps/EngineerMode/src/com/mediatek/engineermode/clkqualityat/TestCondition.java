package com.mediatek.engineermode.clkqualityat;


import com.mediatek.engineermode.EmUtils;

import android.app.KeyguardManager;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.provider.Settings;


/**
 * Class to control test condition.
 * @author mtk80357
 *
 */
class TestCondition {
    private static final String TAG = "ClkQualityAt/TestCondition";
    private boolean mWifiEnable;
    private boolean mBtEnable;
    private WifiManager mWifiManager = null;
    private KeyguardManager.KeyguardLock mKeyguardLock = null;
    private boolean mAirplaneMode;

    void init(Context context) {
        mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        KeyguardManager keyguardMgr = (KeyguardManager) context
                .getSystemService(Context.KEYGUARD_SERVICE);
        mKeyguardLock = keyguardMgr.newKeyguardLock(TAG);
    }

    void setCondition(Context context) {
        //Disable wifi
        mWifiEnable = mWifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLED;
        if (mWifiEnable) {
            Util.switchWifi(context, false);
        }
        EmUtils.initPoweroffmdMode(true, true);
        //Enable airplanemode
        mAirplaneMode = Settings.System.getInt(context.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        if (!mAirplaneMode) {
            Util.switchAirplaneMode(context, true);
        }
        //Disable keyguard
        mKeyguardLock.disableKeyguard();

    }

    void resetCondition(Context context) {
        Util.switchWifi(context, mWifiEnable);
        Util.switchAirplaneMode(context, mAirplaneMode);
        EmUtils.initPoweroffmdMode(false, true);
        mKeyguardLock.reenableKeyguard();

    }

}
