package com.mediatek.engineermode.iotconfig;

public class IotConfigConstant {

    //public
    public static final String FK_SS_BOOLCONFIG = "persist.vendor.ss.boolconfig";
    public static final String FK_SS_BOOLVALUE = "persist.vendor.ss.boolvalue";
    public static final String FK_SS_AUID = "persist.vendor.ss.auid";
    public static final String FK_SS_DIGEST_ID = "persist.vendor.ss.digest.id";
    public static final String FK_SS_DIGEST_PWD = "persist.vendor.ss.digest.pwd";
    public static final String FK_SS_MEDIATYPE = "persist.vendor.ss.mediatype";

    //NON-93Modem
    public static final String FK_SS_CONTENTTYPE = "persist.vendor.ss.contenttype";
    public static final String FK_SS_XCAPROOT = "persist.vendor.ss.xcaproot";
    public static final String FK_SS_RELEID_CFU = "persist.vendor.ss.ruleid.cfu";
    public static final String FK_SS_RELEID_CFB = "persist.vendor.ss.ruleid.cfb";
    public static final String FK_SS_RELEID_CFNRY = "persist.vendor.ss.ruleid.cfnry";
    public static final String FK_SS_RELEID_CFNRC = "persist.vendor.ss.ruleid.cfnrc";
    public static final String FK_SS_RELEID_CFNL = "persist.vendor.ss.ruleid.cfnl";
    public static final String FK_SS_XCAPPORT = "persist.vendor.ss.xcapport";
    public static final String FK_SS_ALIVETIMER = "persist.vendor.ss.alivetimer";
    public static final String FK_SS_REQTIMER = "persist.vendor.ss.reqtimer";
    public static final String FK_SS_CDTIMER = "persist.vendor.ss.cdtimer";
    public static final String FK_SS_GBA_BSFSERVERURL = "persist.vendor.gba.bsfserverurl";
    public static final String FK_SS_GBA_ENABLEBATRUSTALL = "persist.vendor.gba.enablegbatrustall";
    public static final String FK_SS_GBA_ENABLEBAFORCERUN = "persist.vendor.gba.enablegbaforcerun";
    public static final String FK_SS_GBA_GBATYPE = "persist.vendor.gba.gbatype";

    //93Modem
    public static final String FK_SS_URL_ENCODING = "persist.vendor.ss.urlencoding";
    public static final String FK_SS_NAFHOST = "persist.vendor.ss.nafhost";
    public static final String FK_SS_XCAP_PROTOCOL = "persist.vendor.ss.xcap.protocol";
    public static final String FK_SS_XCAP_PORT = "persist.vendor.ss.xcap.port";
    public static final String FK_SS_BSFHOST = "persist.vendor.ss.bsfhost";
    public static final String FK_SS_GBA_PROTOCOL = "persist.vendor.ss.gba.protocol";
    public static final String FK_SS_GBA_PORT = "persist.vendor.ss.gba.port";
    public static final String FK_SS_GBA_TYPE = "persist.vendor.ss.gba.type";
    public static final String FK_SS_BSFURLPATH = "persist.vendor.ss.bsfurlpath";
    public static final String FK_SS_IMEIHEADER = "persist.vendor.ss.imeiheader";
    public static final String BOOLEANTYPE = "boolean";
    public static final String STRINGTYPE = "String";
    public static final String INTEGERTYPE = "int";

    public static boolean isNumeric(String s) {
        if (s != null && !"".equals(s.trim()))
            return s.matches("^[0-9]*$");
        else
            return false;
    }

}
