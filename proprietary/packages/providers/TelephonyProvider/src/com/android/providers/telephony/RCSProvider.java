package com.android.providers.telephony;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.net.Uri.Builder;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Log;
import android.provider.BaseColumns;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.Telephony;
import android.provider.Telephony.Mms;
import android.provider.Telephony.MmsSms;
import android.provider.Telephony.MmsSms.PendingMessages;
import android.provider.Telephony.Sms;
import android.provider.Telephony.Threads;
import android.os.Binder;
import android.os.SystemProperties;
import android.os.UserHandle;

import com.google.android.mms.pdu.PduHeaders;

import mediatek.telephony.MtkTelephony.MtkThreads;
/**
 * This class is only for OP01 RCS feature
 */
public class RCSProvider extends ContentProvider {

    private static final String TAG = "Mms/Provider/Rcs";

    public static final String TABLE_MESSAGE = "rcs_message";
    public static final String TABLE_CONVERSATIONS = "rcs_conversations";
    public static final String VIEW_RCS_THREADS = "rcs_threads";
    public static final String VIEW_RCS_THREADS_DAPI = "rcs_threads_dapi";

    public static final int RCS_TABLE_TO_USE = 5;

    public static final String COLUMN_ID               = "_id";

    public static final String MESSAGE_COLUMN_DATE_SENT        = "date_sent";
    public static final String MESSAGE_COLUMN_SEEN             = "seen";
    public static final String MESSAGE_COLUMN_LOCKED           = "locked";
    public static final String MESSAGE_COLUMN_SUB_ID           = "sub_id";
    public static final String MESSAGE_COLUMN_IPMSG_ID         = "ipmsg_id";
    public static final String MESSAGE_COLUMN_CLASS            = "class";
    public static final String MESSAGE_COLUMN_FILE_PATH        = "file_path";
    public static final String MESSAGE_COLUMN_MESSAGE_ID       = "CHATMESSAGE_MESSAGE_ID";
    public static final String MESSAGE_COLUMN_CHAT_ID          = "CHATMESSAGE_CHAT_ID";
    public static final String MESSAGE_COLUMN_CONTACT_NUMBER   = "CHATMESSAGE_CONTACT_NUMBER";
    public static final String MESSAGE_COLUMN_BODY             = "CHATMESSAGE_BODY";
    public static final String MESSAGE_COLUMN_TIMESTAMP        = "CHATMESSAGE_TIMESTAMP";
    public static final String MESSAGE_COLUMN_MESSAGE_STATUS   = "CHATMESSAGE_MESSAGE_STATUS";
    public static final String MESSAGE_COLUMN_TYPE             = "CHATMESSAGE_TYPE";
    public static final String MESSAGE_COLUMN_DIRECTION        = "CHATMESSAGE_DIRECTION";
    public static final String MESSAGE_COLUMN_FLAG             = "CHATMESSAGE_FLAG";
    public static final String MESSAGE_COLUMN_ISBLOCKED        = "CHATMESSAGE_ISBLOCKED";
    public static final String MESSAGE_COLUMN_CONVERSATION     = "CHATMESSAGE_CONVERSATION";
    public static final String MESSAGE_COLUMN_MIME_TYPE        = "CHATMESSAGE_MIME_TYPE";
    public static final String MESSAGE_COLUMN_AT_ME            = "at_me";

    public static final String CONVERSATION_COLUMN_READ          = "read";
    public static final String CONVERSATION_COLUMN_ERROR         = "error";
    public static final String CONVERSATION_COLUMN_ATTACHMENT    = "has_attachment";
    public static final String CONVERSATION_COLUMN_CLASS         = MESSAGE_COLUMN_CLASS;
    public static final String CONVERSATION_COLUMN_CONVERSATION  = MESSAGE_COLUMN_CONVERSATION;
    public static final String CONVERSATION_COLUMN_RECIPIENTS    = "CHATMESSAGE_RECIPIENTS";
    public static final String CONVERSATION_COLUMN_BODY          = MESSAGE_COLUMN_BODY;
    public static final String CONVERSATION_COLUMN_TIMESTAMP     = MESSAGE_COLUMN_TIMESTAMP;
    public static final String CONVERSATION_COLUMN_FLAG          = MESSAGE_COLUMN_FLAG;
    public static final String CONVERSATION_COLUMN_TYPE          = MESSAGE_COLUMN_TYPE;
    public static final String CONVERSATION_COLUMN_MESSAGE_COUNT = "CHATMESSAGE_MESSAGE_COUNT";
    public static final String CONVERSATION_COLUMN_UNREAD_COUNT  = "CHATMESSAGE_UNREAD_COUNT";
    public static final String CONVERSATION_COLUMN_MIME_TYPE     = MESSAGE_COLUMN_MIME_TYPE;
    public static final String CONVERSATION_COLUMN_AT_ME         = MESSAGE_COLUMN_AT_ME;

    public static final String DAPI_CONVERSATION_BODY            = "DAPI_CONVERSATION_BODY";
    public static final String DAPI_CONVERSATION_TIMESTAMP       = "DAPI_CONVERSATION_TIMESTAMP";
    public static final String DAPI_CONVERSATION_TYPE            = "DAPI_CONVERSATION_TYPE";
    public static final String DAPI_CONVERSATION_UNREAD_COUNT    = "DAPI_CONVERSATION_UNREAD_COUNT";
    public static final String DAPI_CONVERSATION_MESSAGE_COUNT   = "DAPI_CONVERSATION_MSG_COUNT";
    public static final String DAPI_CONVERSATION_MIMETYPE        = "DAPI_CONVERSATION_MIMETYPE";
    public static final String DAPI_CONVERSATION_STATUS          = "DAPI_CONVERSATION_STATUS";

    public static final String THREADS_COLUMN_ID               = "_id";
    public static final String THREADS_COLUMN_SNIPPET          = "snippet";
    public static final String THREADS_COLUMN_SNIPPET_CS       = "snippet_cs";
    public static final String THREADS_COLUMN_TYPE             = "type";
    public static final String THREADS_COLUMN_DATE             = "date";
    public static final String THREADS_COLUMN_READCOUNT        = "readcount";
    public static final String THREADS_COLUMN_MESSAGE_COUNT    = "message_count";
    public static final String THREADS_COLUMN_ERROR            = "error";
    public static final String THREADS_COLUMN_READ             = "read";
    public static final String THREADS_COLUMN_HAS_ATTACHMENT   = "has_attachment";
    public static final String THREADS_COLUMN_STATUS           = "status";
    public static final String THREADS_COLUMN_RECIPIENT_IDS    = "recipient_ids";
    public static final String THREADS_COLUMN_ARCHIVED         = "archived";
    public static final String THREADS_COLUMN_CLASS            = "class";
    public static final String THREADS_COLUMN_RECIPIENTS       = "CHATMESSAGE_RECIPIENTS";
    public static final String THREADS_COLUMN_FLAG             = "CHATMESSAGE_FLAG";
    public static final String THREADS_COLUMN_MESSAGE_TYPE     = "CHATMESSAGE_TYPE";
    public static final String THREADS_COLUMN_MIME_TYPE        = "CHATMESSAGE_MIME_TYPE";
    public static final String THREADS_COLUMN_AT_ME            = CONVERSATION_COLUMN_AT_ME;

    public static final String DAPI_THREADS_CONVERSATION_ID = "CHATMESSAGE_CONVERSATION_ID";
    public static final String DAPI_THREADS_RECIPIENTS      = CONVERSATION_COLUMN_RECIPIENTS;
    public static final String DAPI_THREADS_BODY            = CONVERSATION_COLUMN_BODY;
    public static final String DAPI_THREADS_TIMESTAMP       = CONVERSATION_COLUMN_TIMESTAMP;
    public static final String DAPI_THREADS_FLAG            = CONVERSATION_COLUMN_FLAG;
    public static final String DAPI_THREADS_TYPE            = CONVERSATION_COLUMN_TYPE;
    public static final String DAPI_THREADS_UNREAD_COUNT    = CONVERSATION_COLUMN_UNREAD_COUNT;
    public static final String DAPI_THREADS_MESSAGE_COUNT   = CONVERSATION_COLUMN_MESSAGE_COUNT;
    public static final String DAPI_THREADS_MIMETYPE        = CONVERSATION_COLUMN_MIME_TYPE;
    public static final String DAPI_THREADS_MESSAGE_STATUS  = MESSAGE_COLUMN_MESSAGE_STATUS;
    /**
     * class value
     */
    public static final int CLASS_NORMAL     = 0;
    public static final int CLASS_BURN       = 1;
    public static final int CLASS_EMOTICON   = 2;
    public static final int CLASS_CLOUD      = 3;
    public static final int CLASS_SYSTEM     = 11;
    public static final int CLASS_INVITATION = 12;

    /**
     * CHATMESSAGE_MESSAGE_STATUS values in table rcs_message
     */
    public static final int MESSAGE_STATUS_UNREAD           = 0;
    public static final int MESSAGE_STATUS_READ             = 2;
    public static final int MESSAGE_STATUS_SENDING          = 3;
    public static final int MESSAGE_STATUS_SENT             = 4;
    public static final int MESSAGE_STATUS_FAILED           = 5;
    public static final int MESSAGE_STATUS_TO_SEND          = 6;
    public static final int MESSAGE_STATUS_DELIVERED        = 7;

    /**
     * CHATMESSAGE_TYPE values
     */
    public static final int TYPE_SMSMMS     = 0;
    public static final int TYPE_SMS        = 1;
    public static final int TYPE_MMS        = 2;
    public static final int TYPE_IM         = 3;
    public static final int TYPE_XML        = 4;
    public static final int TYPE_FT         = 5;

    /**
     * CHATMESSAGE_FLAG values
     */
    public static final int FLAG_OTO        = 1;
    public static final int FLAG_OTM        = 2;
    public static final int FLAG_MTM        = 3;
    public static final int FLAG_OFFICIAL   = 4;

    /**
     * CHATMESSAGE_DIRECTION values in table rcs_message
     */
    public static final int DIRECTION_INCOMING      = 0;
    public static final int DIRECTION_OUTGOING      = 1;
    public static final Uri CONTENT_URI_UNION = Uri.parse("content://mms-sms-rcs");

    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        return 0;
    }

    @Override
    public String getType(Uri uri) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String where,
            String[] whereArgs) {
        return 0;
    }

    static final String RCS_UPDATE_MESSAGE_STATUS =
      "UPDATE rcs_conversations SET " + DAPI_CONVERSATION_STATUS + " = " +
      "(SELECT " +
      "(CASE WHEN T.msg_type=2 THEN T.status " +
      "      WHEN T.msg_type=1 AND T.box=1 AND T.read=0 THEN " + RCSProvider.MESSAGE_STATUS_UNREAD +
      "      WHEN T.msg_type=1 AND T.box=1 AND T.read=1 THEN " + RCSProvider.MESSAGE_STATUS_READ +
      "      WHEN T.msg_type=1 AND T.box=2 THEN " + RCSProvider.MESSAGE_STATUS_SENT +
      "      WHEN T.msg_type=1 AND T.box=3 THEN " + RCSProvider.MESSAGE_STATUS_TO_SEND +
      "      WHEN T.msg_type=1 AND T.box=5 THEN " + + RCSProvider.MESSAGE_STATUS_FAILED +
      "      ELSE " + RCSProvider.MESSAGE_STATUS_SENDING +
      " END) AS MESSAGE_STATUS FROM ((%s) T)) " +
      " WHERE rcs_conversations." + CONVERSATION_COLUMN_CONVERSATION + "=%s ";

    static final String RCS_UNION_QUERY =
        " SELECT 1 AS msg_type, read, type AS box, date AS timestamp, 0 AS status, thread_id " +
        "   FROM sms WHERE sms.thread_id=%s " +
        " UNION " +
        " SELECT 1 AS msg_type,read,msg_box AS box,date*1000 AS timestamp,0 AS status, thread_id " +
        "   FROM pdu " +
        "   WHERE (pdu.m_type=132 OR pdu.m_type=130 OR pdu.m_type=128) AND pdu.thread_id=%s " +
        " UNION " +
        " SELECT 2 AS msg_type, 0 AS read, 0 AS box, CHATMESSAGE_TIMESTAMP AS timestamp, " +
        "       CHATMESSAGE_MESSAGE_STATUS AS status, CHATMESSAGE_CONVERSATION AS thread_id " +
        "   FROM rcs_message WHERE CHATMESSAGE_CONVERSATION=%s AND class=0 " +
        " ORDER BY timestamp DESC LIMIT 1";


}
