/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.atci.utils;

/**
 * Handler Interface for {@link AtParser}.<p>
 * @hide
 */
public abstract class AtCommandHandler {

    /**
     * Handle Basic commands "ATA".<p>
     * These are single letter commands such as ATA and ATD. Anything following
     * the single letter command ('A' and 'D' respectively) will be passed as
     * 'arg'.<p>
     * For example, "ATDT1234" would result in the call
     * handleBasicCommand("T1234").<p>
     * @param arg Everything following the basic command character.
     * @return    The result of this command.
     */
    public AtCommandResult handleBasicCommand(String arg) {
        return new AtCommandResult(AtCommandResult.ERROR);
    }

    /**
     * Handle Actions command "AT+FOO".<p>
     * Action commands are part of the Extended command syntax, and are
     * typically used to signal an action on "FOO".<p>
     * @return The result of this command.
     */
    public AtCommandResult handleActionCommand() {
        return new AtCommandResult(AtCommandResult.ERROR);
    }

    /**
     * Handle Read command "AT+FOO?".<p>
     * Read commands are part of the Extended command syntax, and are
     * typically used to read the value of "FOO".<p>
     * @return The result of this command.
     */
    public AtCommandResult handleReadCommand() {
        return new AtCommandResult(AtCommandResult.ERROR);
    }

    public AtCommandResult handleReadCommand(Object[] args) {
        return new AtCommandResult(AtCommandResult.ERROR);
    }

    /**
     * Handle Set command "AT+FOO=...".<p>
     * Set commands are part of the Extended command syntax, and are
     * typically used to set the value of "FOO". Multiple arguments can be
     * sent.<p>
     * AT+FOO=[<arg1>[,<arg2>[,...]]]<p>
     * Each argument will be either numeric (Integer) or String.
     * handleSetCommand is passed a generic Object[] array in which each
     * element will be an Integer (if it can be parsed with parseInt()) or
     * String.<p>
     * Missing arguments ",," are set to empty Strings.<p>
     * @param args Array of String and/or Integer's. There will always be at
     *             least one element in this array.
     * @return     The result of this command.
     */
    // Typically used to set this parameter
    public AtCommandResult handleSetCommand(Object[] args) {
        return new AtCommandResult(AtCommandResult.ERROR);
    }

    /**
     * Handle Test command "AT+FOO=?".<p>
     * Test commands are part of the Extended command syntax, and are typically
     * used to request an indication of the range of legal values that "FOO"
     * can take.<p>
     * By default we return an OK result, to indicate that this command is at
     * least recognized.<p>
     * @return The result of this command.
     */
    public AtCommandResult handleTestCommand() {
        return new AtCommandResult(AtCommandResult.OK);
    }

    public AtCommandResult handleTestCommand(Object[] args) {
        return new AtCommandResult(AtCommandResult.OK);
    }

}
