package com.mediatek.phone.ext;

import android.app.Activity;

/**
 * Telecom account registry extension plugin for op12.
 */
public interface IEmergencyDialerExt {

    /**
     * Called to get the ECBM dialog text.
     * @param phoneObj phone object
     * @param dialogType dialog Type
     * @param millisUntilFinished time to finish ECBM
     * @return dialog text
     */
    String getDialogText(Object phoneObj, int dialogType, long millisUntilFinished);
}
