/*
 * Copyright (C) 2015, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * Contributed by: Giesecke & Devrient GmbH.
 */

package org.simalliance.openmobileapi.uicc2terminal;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
/// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @{
import android.os.Handler;
/// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @}
import android.os.IBinder;
/// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @{
import android.os.Message;
import android.os.RemoteCallbackList;
/// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @}
import android.os.RemoteException;
import android.telephony.IccOpenLogicalChannelResponse;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.MissingResourceException;
import java.util.NoSuchElementException;

import org.simalliance.openmobileapi.internal.ByteArrayConverter;
/// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @{
import org.simalliance.openmobileapi.service.ISmartcardServiceCallback;
/// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @}
import org.simalliance.openmobileapi.service.ITerminalService;
import org.simalliance.openmobileapi.service.SmartcardError;
import org.simalliance.openmobileapi.service.OpenLogicalChannelResponse;
import org.simalliance.openmobileapi.internal.Util;
/// M: @ {
import android.os.ServiceManager;
import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.PhoneConstants;
import com.mediatek.internal.telephony.IMtkTelephonyEx;
import android.os.SystemProperties;
import android.telephony.SubscriptionManager;
/// }

public final class Uicc2Terminal extends Service {

    private static final String TAG = "Uicc2Terminal";

    /// M: @ {
    // Card state is ready (RIL state String)
    private static final String RIL_CARD_READY = "READY";
    private static final int mUiccNumber = 1; // UICC Number
    private static final int MAX_LOGICAL_CHANNEL_NUMBER = 20;
    private static final int GET_SUBID_NULL_ERROR = -99;
    /// }

    public static final String ACTION_SIM_STATE_CHANGED =
        "org.simalliance.openmobileapi.action.SIM2_STATE_CHANGED";

    /// M: @ {
    private ITelephony manager = null;
    private IBinder mBinder = null;
    private IMtkTelephonyEx managerEx = null;
    private IBinder mBinderEx = null;
    /// }

    private List<Integer> mChannelIds;

    private BroadcastReceiver mSimReceiver;

    private String mCurrentSelectedFilePath;

    private byte[] mAtr;

    private Object mAtrLock = new Object();

    private int mSubId;
    private Object mSubIdLock = new Object();

    /// M: TS.27 15.9.3.2.2 @{
    private static final boolean sDebug = !"user".equals(Build.TYPE);
    /// M: TS.27 15.9.3.2.2 @}

    /// M: Send select command by SIM framework @{
    private static final boolean sSelectByTerminal = false;
    /// M: Send select command by SIM framework @}

    @Override
    public IBinder onBind(Intent intent) {
        return new TerminalServiceImplementation();
    }

    @Override
    public void onCreate() {
        /// M: @ {
        // Get telephony manager
        try {
            mBinder = ServiceManager.getService("phone");
            manager = ITelephony.Stub.asInterface(mBinder);

            mBinderEx = ServiceManager.getService("phoneEx");
            managerEx = IMtkTelephonyEx.Stub.asInterface(mBinderEx);
        } catch (Exception ex) {
            Log.d(TAG, "Exception for getService(phone) ");
            ex.printStackTrace();
        }
        Log.d(TAG, "Uicc2Terminal(), telephony manager = " + manager);
        Log.d(TAG, "Uicc2Terminal(), telephony manager ex = " + managerEx);

        /// }

        /// M: Error handling for hot-plug SIM card @{
        resetChannelIds();
        /// M: Error handling for hot-plug SIM card @}

        registerSimStateChangedEvent();
        mCurrentSelectedFilePath = "";

        synchronized(mAtrLock) {
            mAtr = null; // Reset ATR cache
        }
        resetSubId();
    }

    @Override
    public void onDestroy() {
        unregisterSimStateChangedEvent();
        super.onDestroy();
    }

    /// M: Error handling for hot-plug SIM card @{
    private void resetChannelIds() {
        mChannelIds = new ArrayList<>();
        // Occupy mChannelIds[0] to avoid return channel number = 0 on openLogicalChannel
        mChannelIds.add(0xFFFFFFFF);
        for (int i = 1; i < MAX_LOGICAL_CHANNEL_NUMBER; i++) {
            mChannelIds.add(0x00);
        }
    }
    /// M: Error handling for hot-plug SIM card @}

    /**
     * Performs all the logic for opening a logical channel.
     *
     * @param aid The AID to which the channel shall be opened, empty string to
     * specify "no AID".
     *
     * @return The index of the opened channel ID in the mChannelIds list.
     */
    private OpenLogicalChannelResponse iccOpenLogicalChannel(String aid, byte p2)
            throws NoSuchElementException, MissingResourceException, IOException {
        Log.d(TAG, "iccOpenLogicalChannel > " + hideAid(aid)
            + " p2(byte)=" + p2 + " p2(int)=" + ((int) p2 & 0xFF));
        // Remove any previously stored selection response
        IccOpenLogicalChannelResponse response;
        String selectAIDResponse = null;

        /// M: @ {
        Log.d(TAG, "internalOpenLogicalChannel --> iccOpenLogicalChannel");
        try {
            if (manager == null || (mBinder != null && !mBinder.isBinderAlive())) {
                mBinder = ServiceManager.getService("phone");
                manager = ITelephony.Stub.asInterface(mBinder);
                Log.d(TAG, "iccOpenLogicalChannel(), re-get telephony manager = " + manager);
            }
            response = manager.iccOpenLogicalChannel(
                getSubIdBySlot(mUiccNumber), getOpPackageName(),
                aid, (int) p2 & 0xFF);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new IOException("iccOpenLogicalChannel failed");
        }

        int status = response.getStatus();
        if (status != IccOpenLogicalChannelResponse.STATUS_NO_ERROR) {
            Log.d(TAG, "iccOpenLogicalChannel failed.");
            // An error occured.
            if (status == IccOpenLogicalChannelResponse.STATUS_MISSING_RESOURCE) {
                Log.d(TAG, "all channels are used");

                /// M: OMAPI Test Spec. V2.2 - 6.4.7 ID5d
                /* Failure in retrieving rules due to the lack of a new logical
                 * channel (and only this failure) should result in a null
                 * return value and not a security exception. This is in line
                 * with the GlobalPlatform Secure Element Access Control
                 * Specification, as the access to the SE applet will be denied
                 * anyway.
                 */
                throw new MissingResourceException("all channels are used", "", "");
            }
            if (status == IccOpenLogicalChannelResponse.STATUS_NO_SUCH_ELEMENT) {
                throw new NoSuchElementException("Applet not found");
            }

            /// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @{
            if (status == IccOpenLogicalChannelResponse.STATUS_UNKNOWN_ERROR) {
                notifyEvent(ISmartcardServiceCallback.IOErrorEventType);
            }
            /// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @}

            throw new IOException("iccOpenLogicalChannel failed");
        }

        // Operation succeeded
        // Set the select response
        // Save channel ID. First check if there is any channelID which is empty
        // to reuse it.
        for (int i = 1; i < MAX_LOGICAL_CHANNEL_NUMBER; i++) {
            if (mChannelIds.get(i) == 0) {
                mChannelIds.set(i, response.getChannel());
                if ("".equals(aid)) {
                    Log.d(TAG, "aid is not provided, just executes the manage" +
                        " channel operation without SELECT command!");
                    Log.d(TAG, "Manage Channel Response:" +
                        ByteArrayConverter.byteArrayToHexString(response.getSelectResponse()));
                    return new OpenLogicalChannelResponse(i, null);
                }
                try {
                    /// M: Send select command by SIM framework @{
                    if (sSelectByTerminal == false) {
                        selectAIDResponse = ByteArrayConverter.byteArrayToHexString(response.getSelectResponse());
                    } else {
                        Log.d(TAG, "aid:" + hideAid(aid) + " p2:" + String.format("%02X", p2));
                        selectAIDResponse = transmitSelectCommand(mChannelIds.get(i), aid, p2);
                    }
                    /// M: Send select command by SIM framework @}

                    /// M: OMAPI Test Spec. 6.5.4 ID12 for case 4 @{
                    /// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @{
                    selectAIDResponse = handleSelectResponse(mChannelIds.get(i), selectAIDResponse);
                    /// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @}
                    /// M: OMAPI Test Spec. 6.5.4 ID12 for case 4 @}
                } catch (Exception ex) {
                    ex.printStackTrace();
                    throw new IOException("iccTransmitApduLogicalChannel failed");
                }
                if (selectAIDResponse == null) {
                    Log.d(TAG, "DATA+SW1SW2 = null");
                } else {
                    Log.d(TAG, "DATA+SW1SW2 = " + hideData(selectAIDResponse));
                }

                return new OpenLogicalChannelResponse(i,
                    ByteArrayConverter.hexStringToByteArray(selectAIDResponse));
            }
        }

        // If no channel ID is empty, append one at the end of the list.
        throw new IOException("out of channels");
        /// }
    }

    private void registerSimStateChangedEvent() {
        Log.v(TAG, "register to android.intent.action.SIM_STATE_CHANGED event");

        IntentFilter intentFilter = new IntentFilter("android.intent.action.SIM_STATE_CHANGED");
        mSimReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if ("android.intent.action.SIM_STATE_CHANGED".equals(intent.getAction())) {
                    /// M: @ {
                    /*Bundle extras = intent.getExtras();
                    boolean simReady = (extras != null)
                            && "READY".equals(extras.getString("ss"));
                    boolean simLoaded = (extras != null)
                            && "LOADED".equals(extras.getString("ss"));
                    if (simReady || simLoaded) {
                        Log.i(TAG, "SIM is ready or loaded. Checking access rules for updates.");
                        Intent i = new Intent(ACTION_SIM_STATE_CHANGED);
                        sendBroadcast(i);
                    }*/
                    String iccState;
                    int simId;
                    iccState = intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE);
                    simId = intent.getIntExtra(PhoneConstants.SLOT_KEY, -1);
                    if(simId != mUiccNumber)
                        return;
                    if (iccState == null) {
                        iccState = "NULL";
                    }
                    Log.d(TAG, "ACTION_SIM_STATE_CHANGED receiver with iccState = " + iccState +
                        ", simId = " + simId);
                    if (iccState.equals(IccCardConstants.INTENT_VALUE_ICC_LOADED)) {
                        if (simId == mUiccNumber) {
                            Log.i(TAG, "SIM"+ simId +
                                " is ready. Checking access rules for updates.");
                            Intent i = new Intent(ACTION_SIM_STATE_CHANGED);
                            sendBroadcast(i);

                            synchronized (mAtrLock) {
                                mAtr = null; // Reset ATR cache
                            }
                            getSubIdBySlot(mUiccNumber);
                        }
                    /// M: Error handling for hot-plug SIM card @{
                    } else if (iccState.equals(IccCardConstants.INTENT_VALUE_ICC_ABSENT)) {
                        if (simId == mUiccNumber) {
                            resetChannelIds();

                            synchronized (mAtrLock) {
                                mAtr = null; // Reset ATR cache
                            }
                            resetSubId();
                        }
                    }
                    /// M: Error handling for hot-plug SIM card @}
                    /// }
                }
            }
        };
        registerReceiver(mSimReceiver, intentFilter);
    }

    private void unregisterSimStateChangedEvent() {
        if (mSimReceiver != null) {
            Log.v(TAG, "unregister SIM_STATE_CHANGED event");
            unregisterReceiver(mSimReceiver);
            mSimReceiver = null;
        }
    }

    /**
     * The Terminal service interface implementation.
     */
    final class TerminalServiceImplementation extends ITerminalService.Stub {

        @Override
        public OpenLogicalChannelResponse internalOpenLogicalChannel(
                byte[] aid,
                byte p2,
                SmartcardError error) throws RemoteException {
            if (aid == null && (
                SystemProperties.get("persist.vendor.st_nfc_gsma_support", "0").equals("0")
                || SystemProperties.get("persist.vendor.nxp_nfc_gsma_support", "0").equals("0")
                )) {
                return null; /// M: OP-011: OMAPI Test Spec. V2.2 - 6.4.7 ID3b
            }
            try {
                return iccOpenLogicalChannel(ByteArrayConverter.byteArrayToHexString(aid), p2);
            } catch (Exception e) {
                if(!(e instanceof MissingResourceException)) {
                    Log.e(TAG, "Exception at internalOpenLogicalChannel", e);
                }
                error.set(e);
                return null;
            }

        }

        @Override
        public void internalCloseLogicalChannel(int channelNumber, SmartcardError error)
                throws RemoteException {
            try {
                if (channelNumber == 0) {
                    return;
                }
                if (mChannelIds.get(channelNumber) == 0) {
                    throw new IllegalStateException("Channel not open");
                }

                /// M: @ {
                Log.d(TAG,
                    "internalCloseLogicalChannel() --> ITelephony.iccCloseLogicalChannel()");
                if (manager.iccCloseLogicalChannel(getSubIdBySlot(mUiccNumber),
                    mChannelIds.get(channelNumber)) == false) {
                    throw new IOException("close channel failed");
                }
                /// }
                mChannelIds.set(channelNumber, 0);
            } catch (Exception e) {
                Log.e(TAG, "Exception at internalCloseLogicalChannel", e);
                error.set(e);
            }
        }

        @Override
        public byte[] internalTransmit(byte[] command, SmartcardError error)
            throws RemoteException {
            try {
                Log.d(TAG, "internalTransmit > " +
                    ByteArrayConverter.byteArrayToHexString(command));
                int cla = Util.clearChannelNumber(command[0]) & 0xFF;
                int ins = command[1] & 0xff;
                int p1 = command[2] & 0xff;
                int p2 = command[3] & 0xff;
                int p3 = -1;
                /*--- Fixed for APDU command with header(4 bytes - CLA, INS, P1 & P2) only---*/
                if (command.length == 4) {
                    p3 = 0;
                }
                if (command.length > 4) {
                    p3 = command[4] & 0xff;
                }
                String data = null;
                if (command.length > 5) {
                    data = ByteArrayConverter.byteArrayToHexString(command, 5, command.length - 5);
                }

                int channelNumber = Util.parseChannelNumber(command[0]);

                String response;
                if (channelNumber == 0) {
                    Log.d(TAG, "internalTransmit() --> ITelephony.iccTransmitApduBasicChannel()");
                    try {
                        response = manager.iccTransmitApduBasicChannel(
                            getSubIdBySlot(mUiccNumber), getOpPackageName(), cla, ins, p1, p2, p3, data);
                    } catch (Exception ex) {
                        throw new IOException("transmit command failed");
                    }
                } else {
                    if ((channelNumber > 0) && (mChannelIds.get(channelNumber) == 0)) {
                        throw new IOException("Channel not open");
                    }

                    try {
                        Log.d(TAG,
                                "APDU = "
                                + String.format("[%02x]%02x%02x%02x%02x%02x:",
                                mChannelIds.get(channelNumber), cla & 0xFC, ins, p1, p2, p3) +
                                data);

                        Log.d(TAG,
                            "internalTransmit() --> ITelephony.iccTransmitApduLogicalChannel()");
                        response = manager.iccTransmitApduLogicalChannel(
                            getSubIdBySlot(mUiccNumber), mChannelIds.get(channelNumber), cla,
                            ins, p1, p2, p3, data);
                        Log.d(TAG, "RAPDU = " + hideData(response));

                        /// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @{
                        if (response.length() >= 4) {
                            String SW1 = response.substring(response.length()-4,
                                response.length()-2).toUpperCase();
                            String SW2 = response.substring(response.length()-2,
                                response.length()).toUpperCase();
                            if (SW1.equals("6F")) {
                                notifyEvent(ISmartcardServiceCallback.IOErrorEventType);
                            }
                        }
                        /// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @}
                    } catch (Exception ex) {
                        throw new IOException("transmit command failed");
                    }
                }
                Log.d(TAG, "internalTransmit < " + hideData(response));
                return ByteArrayConverter.hexStringToByteArray(response);
            } catch (Exception e) {
                Log.e(TAG, "Exception at internalTransmit", e);
                error.set(e);
                return null;
            }
        }

        /// M: @{
        @Override
        public byte[] getAtr() {
            Log.d(TAG, "Uicc2Terminal:getATR()");

            synchronized (mAtrLock) {
                if (mAtr != null) {
                    Log.d(TAG, "get cached atr");
                    return mAtr;
                }
            }

            try {
                if (managerEx == null || (mBinderEx != null && !mBinderEx.isBinderAlive())) {
                    mBinderEx = ServiceManager.getService("phoneEx");
                    managerEx = IMtkTelephonyEx.Stub.asInterface(mBinderEx);

                    Log.d(TAG, "getAtr(), re-get telephony manager ex = " + managerEx
                        + ", " + mBinderEx);
                }

                String atr;
                Log.d(TAG, "getATR() --> ITelephony.getIccAtr() ex");
                atr = managerEx.getIccAtr(getSubIdBySlot(mUiccNumber));
                Log.d(TAG, "atr = " + (atr == null ? "" : atr));

                synchronized (mAtrLock) {
                    if (atr != null && !atr.equals("")) {
                        mAtr = ByteArrayConverter.hexStringToByteArray(atr);
                    }
                    Log.d(TAG, "atr=" + atr  + ", mAtr=" + mAtr);

                    return mAtr;
                }
            } catch (Exception ex) {
                synchronized (mAtrLock) {
                    mAtr = null; // Reset ATR cache
                }

                ex.printStackTrace();
            }

            return null;
        }

        @Override
        public boolean isCardPresent() throws RemoteException {
            int sim_State = -1;
            try {
                sim_State = TelephonyManager.getDefault().getSimState(mUiccNumber);
            } catch (Exception ex) {
                ex.printStackTrace();
                Log.d(TAG,"Cannot getSimState from Uicc" + mUiccNumber + " TelephonyManager");

                resetSubId();
                return false;
                //throw new IOException("Cannot getSimState from Uicc" +
                //    mUiccNumber + " TelephonyManager");
            }

            Log.d(TAG, "isCardPresent(), SIM" + mUiccNumber + " sim_State:" + sim_State);

            if (sim_State == android.telephony.TelephonyManager.SIM_STATE_READY) {
                getSubIdBySlot(mUiccNumber);

                Log.d(TAG, "SIM " + mUiccNumber + " is ready");
                return true;
            }

            if (manager == null || (mBinder != null && !mBinder.isBinderAlive())) {
                try {
                    mBinder = ServiceManager.getService("phone");
                    manager = ITelephony.Stub.asInterface(mBinder);
                    /*manager = ITelephony.Stub.asInterface(ServiceManager
                            .getService("phone"));*/
                } catch (Exception ex) {
                    Log.d(TAG, "Exception for getService(phone) ");
                    ex.printStackTrace();
                }
                Log.d(TAG, "isCardPresent(), re-get telephony manager = " + manager);
            }

            if (managerEx == null || (mBinderEx != null && !mBinderEx.isBinderAlive())) {
                try {
                    mBinderEx = ServiceManager.getService("phoneEx");
                    managerEx = IMtkTelephonyEx.Stub.asInterface(mBinderEx);
                } catch (Exception ex) {
                    Log.d(TAG, "Exception for getService(phone) ");
                    ex.printStackTrace();
                }
                Log.d(TAG, "isCardPresent(), re-get telephony manager ex = " + managerEx
                    + ", " + mBinderEx);
            }

            resetSubId();
            return false;
        }
        /// M: @}

        @Override
        public byte[] simIOExchange(int fileID, String filePath, byte[] cmd, SmartcardError error)
                throws RemoteException {
            try {
                int ins;
                int p1 = cmd[2] & 0xff;
                int p2 = cmd[3] & 0xff;
                int p3 = cmd[4] & 0xff;
                switch (cmd[1]) {
                    case (byte) 0xB0:
                        ins = 176;
                        break;
                    case (byte) 0xB2:
                        ins = 178;
                        break;
                    case (byte) 0xA4:
                        ins = 192;
                        p1 = 0;
                        p2 = 0;
                        p3 = 15;
                        break;
                    default:
                        throw new IOException("Unknown SIM_IO command");
                }

                if (filePath != null && filePath.length() > 0) {
                    mCurrentSelectedFilePath = filePath;
                }

                return manager.iccExchangeSimIO(getSubIdBySlot(mUiccNumber), fileID, ins, p1,
                    p2, p3, mCurrentSelectedFilePath);
            } catch (Exception e) {
                Log.e(TAG, "Exception at simIOExchange", e);
                error.set(e);
                return null;
            }
        }

        @Override
        public String getSeStateChangedAction() {
            return ACTION_SIM_STATE_CHANGED;
        }

        /// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @{
        @Override
        public void registerCallback(ISmartcardServiceCallback cb) {
            if (cb != null) mCallbacks.register(cb);
        }

        @Override
        public void unregisterCallback(ISmartcardServiceCallback cb) {
            if (cb != null) mCallbacks.unregister(cb);
        }
        /// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @}
    }

    private void resetSubId() {
        synchronized(mSubIdLock) {
            mSubId = -1;

            Log.d(TAG, "resetSubId");
        }
    }

    private int getSubIdBySlot(int slot) {
        synchronized(mSubIdLock) {
            if (mSubId == -1) {
                int [] subId = SubscriptionManager.getSubId(slot); // May spend long time
                if (subId == null || subId.length == 0) {
                    Log.d(TAG, "MTK getSubIdBySlot error");
                    return GET_SUBID_NULL_ERROR;
                }
                mSubId = subId[0];

                Log.d(TAG, "MTK getSubIdBySlot, simId " + slot + " subId " + mSubId);
            }

            return mSubId;
        }
    }

    private String transmitSelectCommand(int channel, String aid, byte p2) throws Exception {
        if (manager != null) {
            Log.d(TAG, "internalTransmit > SELECT");
            // <ch> A4 04 <p2> <aid length> <aid>
            return manager.iccTransmitApduLogicalChannel(
                getSubIdBySlot(mUiccNumber), channel,
                channel, 0xA4, 0x04, p2 & 0xFF, aid.length()/2, aid);
        }
        return null;
    }

    /// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @{
    /**
     * This is a list of callbacks that have been registered with the service.
     */
    private final RemoteCallbackList<ISmartcardServiceCallback> mCallbacks
            = new RemoteCallbackList<ISmartcardServiceCallback>();

    /**
     * Called by the framework when the event occurs.
     *
     * @param eventType the event type generated by the framework.
     */
    private void notifyEvent(int eventType) {
        if (mHandler != null) {
            Message msg = mHandler.obtainMessage(eventType);
            mHandler.sendMessage(msg);
        }
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (mCallbacks != null) {
                // Broadcast to all clients
                synchronized(mCallbacks) {
                    final int N = mCallbacks.beginBroadcast();
                    for (int i=0; i<N; i++) {
                        try {
                            mCallbacks.getBroadcastItem(i).notifyEvent(msg.what, null);
                        } catch (RemoteException e) {
                            // The RemoteCallbackList will take care of removing
                            // the dead object for us.
                        }
                    }
                    mCallbacks.finishBroadcast();
                }
            }
        }
    };

    private String transmitGetResponseCommand(int channel, int Le) throws Exception {
        if (manager != null) {
            Log.d(TAG, "internalTransmit > GET RESPONSE");
            // <ch> C0 00 00 00(Le)
            return manager.iccTransmitApduLogicalChannel(
                getSubIdBySlot(mUiccNumber), channel,
                channel, 0xC0, 0x00, 0x00, Le, null);
        }
        return null;
    }
    /// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @}

    /// M: TS.27 15.9.3.2.2 @{
    private String hideAid(String aid) {
        if (!sDebug && aid != null) {
            StringBuilder builder = new StringBuilder();

            // AID length should within 5 to 16 (inclusive)
            if (aid.length() >= 10) {
                // keep last 3 bytes
                int keep = 6;
                for (int i=0; i < aid.length() - keep; ++i) {
                    builder.append("*");
                }
                builder.append(aid.substring(aid.length() - keep));
            }
            return builder.toString();
        }
        return aid;
    }

    private String hideData(String apdu) {
        if (!sDebug && apdu != null) {
            StringBuilder builder = new StringBuilder();

            // keep last 2 bytes (SW1SW2)
            if (apdu.length() >= 4) {
                int keep = 4;
                for (int i=0; i < apdu.length() - keep; ++i) {
                    builder.append("*");
                }
                builder.append(apdu.substring(apdu.length() - keep));
            }
            return builder.toString();
        }
        return apdu;
    }
    /// M: TS.27 15.9.3.2.2 @}

    /// M: OMAPI Test Spec. 6.5.4 ID12 for case 4 @{
    private String handleSelectResponse(int channel, String selectAIDResponse) throws Exception {
        String SW1 = "";
        String SW2 = "";
        String resSW1 = "";
        String resSW2 = "";
        StringBuilder builder = new StringBuilder();
        do {
            if (selectAIDResponse == null) {
                break;
            }

            SW1 = selectAIDResponse.substring(selectAIDResponse.length()-4,
                selectAIDResponse.length()-2).toUpperCase();
            SW2 = selectAIDResponse.substring(selectAIDResponse.length()-2,
                selectAIDResponse.length()).toUpperCase();
            if (resSW1.equals("") || resSW2.equals("")) {
                Log.d(TAG, "Set default return SW");
                resSW1 = SW1;
                resSW2 = SW2;
            }

            Log.d(TAG, "select SW(" + SW1 + SW2 + "), RAPDU = " + hideData(selectAIDResponse));

            if (selectAIDResponse.length() > 4) {
                builder.append(
                    selectAIDResponse.substring(0, selectAIDResponse.length() - 4));
            }

            /* OMAPI Test Spec. 6.5.6 - CRN10:
             * For T=0 transmission protocol when
             * 1. transmitting case-4 APDU and
             * 2. the parameter "expectDataWithWarningSw" is set to true and
             * 3. the SE returns warning SW ("62XX" or "63XX") without data and
             * 4. the SE returns an error SW for the GET RESPONSE
             *
             * the error SW returned for the GET RESPONSE shall be
             * provided to the calling Mobile Application.
             */
            if (SW1.equals("61")) {
                /* Get more data */
                selectAIDResponse =
                    transmitGetResponseCommand(channel, Integer.parseInt(SW2));
            } else if (SW1.equals("62") || SW1.equals("63")) {
                resSW1 = SW1;
                resSW2 = SW2;

                /* Specific rules for handling of status word Warning (62xx and 63xx) */
                if (selectAIDResponse.length() == 4) {
                    selectAIDResponse =
                        transmitGetResponseCommand(channel, 0);
                } else {
                    // no need to transmit GET RESPONSE command if the SW with data
                    break;
                }
            } else {
                if (!SW1.equals("90") && !SW2.equals("00")) {
                    resSW1 = SW1;
                    resSW2 = SW2;
                }

                // No need to transmit GET RESPONSE command
                break;
            }
        } while (true);

        if (builder.toString().length() != 0) {
            selectAIDResponse = builder.toString() + resSW1 + resSW2;
        } else {
            selectAIDResponse = resSW1 + resSW2;
        }
        return selectAIDResponse;
    }
    /// M: OMAPI Test Spec. 6.5.4 ID12 for case 4 @}
}
