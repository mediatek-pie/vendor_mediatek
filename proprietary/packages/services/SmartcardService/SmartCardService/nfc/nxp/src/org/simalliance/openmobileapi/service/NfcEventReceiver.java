package org.simalliance.openmobileapi.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.nfc.INfcAdapterExtras;
import android.nfc.NfcAdapter;
import android.os.RemoteException;
import android.util.Log;

import com.mediatek.config.SeRuntimeOptions;
import com.nxp.nfc.INxpNfcAdapterExtras;
import com.nxp.nfc.NxpConstants;
import com.nxp.nfc.NxpNfcAdapter;

import org.simalliance.openmobileapi.internal.ByteArrayConverter;
import org.simalliance.openmobileapi.service.security.AccessControlEnforcer;

import java.util.ArrayList;
import java.util.List;

public final class NfcEventReceiver {
    private static final String LOG_TAG = "NfcEventReceiver";

    private static final String AID_SELECTED
        = "com.android.nfc_extras.action.AID_SELECTED";

    private static final String ACTION_CHECK_X509
        = "org.simalliance.openmobileapi.service.ACTION_CHECK_X509";

    private static final String TRANSACTION_EVENT
        = "com.gsma.services.nfc.action.TRANSACTION_EVENT";

    /* Broadcast receivers */
    private BroadcastReceiver mNfcEventReceiver;

    /// Handle GSMA packages @{
    private static final String GSMA_TRANSACTION_PERMISSION
        = "com.gsma.services.nfc.permission.TRANSACTION_EVENT";

    /* Package event broadcast receiver */
    private BroadcastReceiver mPkgEventReceiver;

    /* GSMA packages */
    private List<String> mInstalledGsmaPackages = new ArrayList<>();
    /// Handle GSMA packages @}

    private final SmartcardService mService;

    public NfcEventReceiver(SmartcardService service) {
        mService = service;

        Log.v(LOG_TAG, "NfcEventReceiver() for NXP NFC chip");
    }

    public void onCreate() {
        if (SeRuntimeOptions.isGSMASupport() == false) {
            Log.v(LOG_TAG, "Do not need GSMA API");
            return;
        }

        /// Handle GSMA packages @{
        updateGsmaPackages();
        registerPkgEvent(getApplicationContext());
        /// Handle GSMA packages @}

        registerNfcEvent(getApplicationContext());
    }

    public void onDestroy() {
        if (SeRuntimeOptions.isGSMASupport() == false) {
            Log.v(LOG_TAG, "Do not need GSMA API");
            return;
        }

        /// Handle GSMA packages @{
        unregisterPkgEvent(getApplicationContext());
        /// Handle GSMA packages @}

        unregisterNfcEvent(getApplicationContext());
    }

    private Context getApplicationContext() {
        if (mService == null) {
            Log.e(LOG_TAG, "Cannot get context");
            return null;
        }
        return mService.getApplicationContext();
    }

    private PackageManager getPackageManager() {
        if (mService == null) {
            Log.e(LOG_TAG, "Cannot get PackageManager");
            return null;
        }
        return mService.getPackageManager();
    }

    private Terminal getTerminal(String seName) {
        return mService.getTerminal(seName);
    }

    private AccessControlEnforcer getAccessControlEnforcer(String seName) {
        Terminal terminal = getTerminal(seName);
        if (terminal == null) {
            Log.e(LOG_TAG, "Couldn't get terminal for " + seName);
            return null;
        }

        return terminal.getAccessControlEnforcer();
    }

    private void initializeAccessControl(String seName) {
        Terminal terminal = getTerminal(seName);
        if (terminal == null) {
            Log.e(LOG_TAG, "Couldn't get terminal for " + seName);
            return;
        }

        getTerminal(seName).initializeAccessControl(false);
    }

    private void registerNfcEvent(Context context) {
        Log.v(LOG_TAG, "registerNfcEvent()");

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AID_SELECTED);
        intentFilter.addAction(ACTION_CHECK_X509);

        mNfcEventReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();

                Log.v(LOG_TAG, "action: " + action);

                if (action.equals(ACTION_CHECK_X509)) {
                    actionCheckX509(context, intent);
                } else if (action.equals(AID_SELECTED)) {
                    actionAidSelected(context, intent);
                } else {
                    Log.v(LOG_TAG, "mNfcEventReceiver got unexpected intent: "
                        + action);
                }
            }
        };

        context.registerReceiver(mNfcEventReceiver, intentFilter);
    }

    private void unregisterNfcEvent(Context context) {
        if (mNfcEventReceiver != null) {
            Log.v(LOG_TAG, "unregister NFC event");

            if (context != null) {
                context.unregisterReceiver(mNfcEventReceiver);
            }
            mNfcEventReceiver = null;
        }
    }

    /// Handle GSMA packages @{
    private void registerPkgEvent(Context context) {
        Log.d(LOG_TAG, "registerPkgEvent");
        if(mPkgEventReceiver != null) {
            Log.d(LOG_TAG, "always registered");
            return;
        }
        mPkgEventReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(LOG_TAG, "update gsma pkg cached on : " + intent);
                updateGsmaPackages();
            }
        };

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_EXTERNAL_APPLICATIONS_AVAILABLE);
        intentFilter.addAction(Intent.ACTION_EXTERNAL_APPLICATIONS_UNAVAILABLE);
        intentFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
        intentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        intentFilter.addDataScheme("package");

        context.registerReceiver(mPkgEventReceiver, intentFilter);
    }

    private void unregisterPkgEvent(Context context) {
        Log.d(LOG_TAG, "unregisterPkgEvent");
        if (mPkgEventReceiver != null) {
            if (context != null) {
                context.unregisterReceiver(mPkgEventReceiver);
            }
            mPkgEventReceiver = null;
        }
    }

    private void updateGsmaPackages() {
        PackageManager pm = getPackageManager();
        String[] permissions = new String[] {
            GSMA_TRANSACTION_PERMISSION,
        };

        List<PackageInfo> packages = pm.getPackagesHoldingPermissions(
            permissions, PackageManager.GET_ACTIVITIES);
        if (packages == null) {
            Log.e(LOG_TAG, "failed to get gsma services");
            return;
        }

        mInstalledGsmaPackages.clear();
        for (PackageInfo pkgInfo : packages) {
            if (pkgInfo == null) {
                Log.d(LOG_TAG, "error entry of gsma packages");
                continue;
            }

            if (pkgInfo.applicationInfo != null) {
                if (pkgInfo.packageName.equals("org.simalliance.openmobileapi.service")) {
                    // do not include these packages
                    Log.d(LOG_TAG, "exclude nfc pkg : " + pkgInfo.packageName);
                    continue;
                }
            } else {
                Log.e(LOG_TAG, "fail to get pkg info :" + pkgInfo);
                continue;
            }

            if (pkgInfo.requestedPermissions != null) {
                for (String permission : pkgInfo.requestedPermissions) {
                    if (GSMA_TRANSACTION_PERMISSION.equals(permission)) {
                        // add this into GSMA packages
                        mInstalledGsmaPackages.add(pkgInfo.packageName);
                        break;
                    }
                }
            }
        }

        Log.d(LOG_TAG, "gsma packages cache updated : " + mInstalledGsmaPackages);
    }
    /// Handle GSMA packages @}

    private void actionCheckX509(Context context, Intent intent) {
        String pkg
            = intent.getStringExtra("org.simalliance.openmobileapi.service.EXTRA_PKG");
        String seName
            = intent.getStringExtra("org.simalliance.openmobileapi.service.EXTRA_SE_NAME");

        Log.v(LOG_TAG, "ACTION_CHECK_X509 seName: " + seName + ", pkg : " + pkg);

        boolean checkResult = false;

        // Get INxpNfcAdapterExtras
        INxpNfcAdapterExtras nxpExtras = getINxpNfcAdapterExtras(context);
        if (nxpExtras == null) {
            Log.e(LOG_TAG, "Couldn't get nxpExtras");
            return;
        }

        /* In normal case, applications will open session or channels for
         * accessing SEs. In this case, it only need to use the access rules
         * from SEs without session or channels. The access rules may be
         * changed without notifying device via external tools. Thus, we
         * should check access rules every time.
         */
        initializeAccessControl(seName);

        // Get AccessControlEnforcer
        AccessControlEnforcer acEnforcer = getAccessControlEnforcer(seName);
        if (acEnforcer == null) {
            Log.e(LOG_TAG, "Couldn't get AccessControlEnforcer for " + seName);
        } else {
            // isOperatorCertificatesAllowed ?
            String[] pkgNames = new String[1];
            pkgNames[0] = pkg;
            try {
                boolean[] res = acEnforcer.isOperatorCertificatesAllowed(pkgNames);
                if (res[0] == true) {
                    checkResult = true;
                }
            } catch (Exception e) {
                Log.e(LOG_TAG, "RemoteException: " + e.toString());
            }
        }

        try {
            notifyCheckCertResult(nxpExtras, pkg, checkResult);
        } catch (RemoteException e) {
            Log.e(LOG_TAG, "RemoteException: " + e.toString());
        }
    }

    private void actionAidSelected(Context context, Intent intent) {
        /// 20180111 updated @{
        byte[] aid = intent.getByteArrayExtra(NxpConstants.EXTRA_AID);
        byte[] data = intent.getByteArrayExtra(NxpConstants.EXTRA_DATA);
        String seName = intent.getStringExtra(NxpConstants.EXTRA_SOURCE);
        /// 20180111 updated @}

        Log.v(LOG_TAG, "AID_SELECTED aid: "
            + ByteArrayConverter.byteArrayToHexString(aid)
            + ", seName: " + seName);

        if ((aid == null) || (seName == null)) {
            Log.v(LOG_TAG, "Got AID_SELECTED without AID or SE Name");
            return;
        }

        /// 20180111 updated @{
        if (seName != null && seName.equals(NxpConstants.UICC_ID)) {
            seName = "SIM1";
        }
        /// 20180111 updated @}

        // Get INxpNfcAdapterExtras
        INxpNfcAdapterExtras nxpExtras = getINxpNfcAdapterExtras(context);
        if (nxpExtras == null) {
            Log.e(LOG_TAG, "Couldn't get nxpExtras");
            return;
        }

        // Get only packages which have Intentfilter for TRANSACTION_EVENT action
        StringBuffer strAid = new StringBuffer();
        for (int i = 0; i < aid.length; i++) {
            String hex = Integer.toHexString(0xFF & aid[i]);
            if (hex.length() == 1) {
                strAid.append('0');
            }
            strAid.append(hex);
        }

        Intent gsmaIntent = new Intent();
        gsmaIntent.setAction(TRANSACTION_EVENT);
        gsmaIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        gsmaIntent.setData(Uri.parse("nfc://secure:0/" + seName + "/" + strAid));

        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(gsmaIntent,
            PackageManager.MATCH_DEFAULT_ONLY | PackageManager.GET_RESOLVED_FILTER);

        Log.d(LOG_TAG, "Found resInfos = " + resInfo);

        /* In normal case, applications will open session or channels for
         * accessing SEs. In this case, it only need to use the access rules
         * from SEs without session or channels. The access rules may be
         * changed without notifying device via external tools. Thus, we
         * should check access rules every time.
         */
        initializeAccessControl(seName);

        // Get AccessControlEnforcer
        AccessControlEnforcer acEnforcer = getAccessControlEnforcer(seName);
        if (acEnforcer == null) {
            Log.e(LOG_TAG, "Couldn't get AccessControlEnforcer for "
                + seName);
        } else {
            // isNFCEventAllowed ?
            int index = 0;
            String[] packageNames = new String[resInfo.size()];
            for (ResolveInfo res : resInfo) {
                ActivityInfo activityInfo = res.activityInfo;
                if ((activityInfo != null) && (activityInfo.packageName != null)) {
                    packageNames[index++] = new String(activityInfo.packageName);
                }
            }

            boolean[] nfcEventAccessFinal = null;
            try {
                nfcEventAccessFinal = acEnforcer.isNFCEventAllowed(aid, packageNames);
            } catch (Exception e) {
                Log.e(LOG_TAG, "RemoteException: " + e.toString());
            }
            if (nfcEventAccessFinal != null) {
                for (int i = 0; i < resInfo.size(); i++) {
                    if (nfcEventAccessFinal[i]) {
                        if (packageNames[i].equals("org.simalliance.openmobileapi.service")) {
                            continue;
                        }

                        /// Handle GSMA packages @{
                        if (!mInstalledGsmaPackages.contains(packageNames[i])) {
                            Log.d(LOG_TAG, "no gsma TRANSACTION permission : " + packageNames[i]);
                            continue;
                        }
                        /// Handle GSMA packages @}

                        Intent evtIntent = new Intent();
                        evtIntent.setAction(getActionMultiEvtTransaction());
                        evtIntent.putExtra("com.android.nfc_extras.extra.AID", aid);
                        evtIntent.putExtra("com.android.nfc_extras.extra.DATA", data);
                        evtIntent.putExtra("com.android.nfc_extras.extra.SE_NAME", seName);
                        evtIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);

                        try {
                            deliverSeIntent(nxpExtras, packageNames[i], evtIntent);
                        } catch (Exception ignore) {
                            //ignore
                            Log.d(LOG_TAG, "failed to deliver se intent", ignore);
                        }
                    } else {
                        Log.d(LOG_TAG, "not allowed to send TRANSACTION to " + packageNames[i]);
                    }
                }

            }
        }
    }

    /*
     * NXP APIs for GSMA (reference only)
     */
    private INxpNfcAdapterExtras getINxpNfcAdapterExtras(Context context) {
        NfcAdapter adapter = NfcAdapter.getNfcAdapter(context);
        INfcAdapterExtras adapterExtras = adapter.getNfcAdapterExtrasInterface();
        NxpNfcAdapter nfcAdapter = NxpNfcAdapter.getNxpNfcAdapter(adapter);
        INxpNfcAdapterExtras nxpExtras
            = nfcAdapter.getNxpNfcAdapterExtrasInterface(adapterExtras);
        return nxpExtras;
    }

    private String getActionMultiEvtTransaction() {
        return NxpConstants.ACTION_MULTI_EVT_TRANSACTION;
    }

    private void notifyCheckCertResult(INxpNfcAdapterExtras nxpExtras, String pkg, boolean success)
        throws RemoteException {
        nxpExtras.notifyCheckCertResult(pkg, success);
    }

    private void deliverSeIntent(INxpNfcAdapterExtras nxpExtras, String pkg, Intent seIntent)
        throws RemoteException {
        nxpExtras.deliverSeIntent(pkg, seIntent);
    }
}
