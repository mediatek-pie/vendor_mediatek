/*
 * Copyright (C) 2011 Deutsche Telekom, A.G.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.simalliance.openmobileapi.service.security.arf.PKCS15;

import org.simalliance.openmobileapi.service.security.arf.ASN1;
import org.simalliance.openmobileapi.service.security.arf.DERParser;
import org.simalliance.openmobileapi.service.security.arf.SecureElement;
import org.simalliance.openmobileapi.service.security.arf.SecureElementException;
import org.simalliance.openmobileapi.internal.Util;

import android.util.Log;
import java.util.ArrayList;

/**
 * EF_CDF related features
 ***************************************************/
public class EFCDF extends EF{

    //public static final String TAG = "ACE ARF EF_CDF";

    /**
     * Decodes EF_CDF file
     * @param buffer ASN.1 data
     * @return Path to "Certificates" from "EF_CDF";
     *             <code>null</code> otherwise
     *
     * CDF File Structure for one cerificate
     *  T  T  T  T  T  L
     * 30             1D
     *    30          0C
     *       0C       0A
     *    30          03
     *       04       01
     *    A1          08
     *       30       06
     *          30    04
     *             04 02 <-- Path
     */
    private ArrayList<byte[]> decodeDER(byte[] buffer)
        throws PKCS15Exception,SecureElementException {
            //Log.v(TAG,"Analysing Certifcates Paths...");
            byte objectType;
            ArrayList<byte[]> CertPaths = new ArrayList<byte[]>();
            DERParser DER=new DERParser(buffer);

            while(!DER.isEndofBuffer()) {
                if (DER.parseTLV()==ASN1.TAG_Sequence) {
                    // Common Object Attributes
                    DER.parseTLV(ASN1.TAG_Sequence);
                    DER.skipTLVData();
                    // Common Data Object Attributes
                    DER.parseTLV(ASN1.TAG_Sequence);
                    DER.skipTLVData();

                    objectType=DER.parseTLV();
                    if (objectType==(byte)0xA1) {
                        DER.parseTLV(ASN1.TAG_Sequence);
                        byte[] path = DER.parsePathAttributes();
                        CertPaths.add(path);
                        // Read the cerficate file
                        //Log.v(TAG,"Loading Cerficate File...");
                        if (selectFile(path)!=APDU_SUCCESS)
                            throw new PKCS15Exception("XXX not found!");
                        mSEHandle.putCertificate(readBinary(0,Util.END));
                    } else throw new PKCS15Exception("[Parser] EFXXX Unexpected type");
                } else DER.skipTLVData();
            }
            return CertPaths;
        }

    /**
     * Constructor
     * @param secureElement SE on which ISO7816 commands are applied
     */
    public EFCDF(SecureElement handle) {
        super(handle);
    }

    /**
     * Selects and Analyses EF_CDF file
     * @param path Path of the "EF_CDF" file
     * @return Path to "Certificate" from "EF_CDF";
     *             <code>null</code> otherwise
     */
    public ArrayList<byte[]> analyseFile(byte[] path)
        throws PKCS15Exception,SecureElementException
        {
            //Log.v(TAG,"Analysing EF_CDF...");

            if (selectFile(path)!=APDU_SUCCESS)
                throw new PKCS15Exception("EF_XXX not found!");

            return decodeDER(readBinary(0,Util.END));
        }

}

