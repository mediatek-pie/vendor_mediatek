/*
 * Copyright (C) 2011, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * Contributed by: Giesecke & Devrient GmbH.
 */

package org.simalliance.openmobileapi;

import java.io.IOException;
/// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @{
import org.simalliance.openmobileapi.service.ISmartcardServiceCallback;
/// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @}
import org.simalliance.openmobileapi.service.ISmartcardServiceReader;
import org.simalliance.openmobileapi.service.ISmartcardServiceSession;
import org.simalliance.openmobileapi.service.SmartcardError;
import android.os.RemoteException;
/// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @{
import android.util.Log;

import java.util.ArrayList;
/// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @}
import java.util.Hashtable;
import java.util.Set;
import java.util.Enumeration;

/**
 * Instances of this class represent Secure Element Readers supported to this
 * device. These Readers can be physical devices or virtual devices. They can be
 * removable or not. They can contain Secure Element that can or cannot be
 * removed.
 *
 * @see <a href="http://simalliance.org">SIMalliance Open Mobile API  v3.0</a>
 */
public class Reader {

    private final String mName;
    private final SEService mService;
    private ISmartcardServiceReader mReader;
    Hashtable<Session, ISmartcardServiceSession> mSessions;

    private final Object mLock = new Object();


    Reader(SEService service, ISmartcardServiceReader reader, String name ) {
        mName = name;
        mService = service;
        mReader = reader;
        mSessions = new Hashtable<Session, ISmartcardServiceSession>();
    }

    /**
     * Return the name of this reader.
     * <ul>
     * <li>If this reader is a SIM reader, then its name must be "SIM[Slot]".</li>
     * <li>If the reader is a SD or micro SD reader, then its name must be "SD[Slot]"</li>
     * <li>If the reader is a embedded SE reader, then its name must be "eSE[Slot]"</li>
     * </ul>
     * Slot is a decimal number without leading zeros. The Numbering must start with 1
     * (e.g. SIM1, SIM2, ... or SD1, SD2, ... or eSE1, eSE2, ...).
     * The slot number “1” for a reader is optional
     * (SIM and SIM1 are both valid for the first SIM-reader,
     * but if there are two readers then the second reader must be named SIM2).
     * This applies also for other SD or SE readers.

     *
     * @return the reader name, as a String.
     */
    public String getName() {
        return mName;
    }

    /**
     * Connects to a Secure Element in this reader. <br>
     * This method prepares (initialises) the Secure Element for communication
     * before the Session object is returned (e.g. powers the Secure Element by
     * ICC ON if its not already on). There might be multiple sessions opened at
     * the same time on the same reader. The system ensures the interleaving of
     * APDUs between the respective sessions.
     *
     * @throws IOException if something went wrong with the communicating to the
     *             Secure Element or the reader.
     * @return a Session object to be used to create Channels.
     */
    public Session openSession() throws IOException {

        if (mService == null || !mService.isConnected()) {
            throw new IllegalStateException("service is not connected");
        }
        /// M: @ {
        /*
           CMCC on-line issue
         */
        if (!isSecureElementPresent()) {
            throw new IOException("Secure Element is not presented.");
        }
        /// }
        synchronized (mLock) {
            try {
                SmartcardError error = new SmartcardError();
                ISmartcardServiceSession session = mReader.openSession(error);
                if (error.isSet()) {
                    error.throwException();
                }
                return new Session(session, this);
            } catch (RemoteException e) {
                throw new IOException( e.getMessage() );
            }
        }
    }

    /**
     * Check if a Secure Element is present in this reader.
     *
     * @return <code>true</code> if the SE is present, <code>false</code> otherwise.
     */
    public boolean isSecureElementPresent() {
        if (mService == null || !mService.isConnected()) {
            throw new IllegalStateException("service is not connected");
        }

        try {
            return mReader.isSecureElementPresent();
        } catch (RemoteException e) {
            throw new IllegalStateException(e.getMessage());
        }
    }

    /**
     * Return the Secure Element service this reader is bound to.
     *
     * @return the SEService object.
     */
    public SEService getSEService() {
        return mService;
    }

    /**
     * Close all the sessions opened on this reader. All the channels opened by
     * all these sessions will be closed.
     */
    public void closeSessions() {
        if (mService == null || !mService.isConnected()) {
            throw new IllegalStateException("service is not connected");
        }
        synchronized (mLock) {
            SmartcardError error = new SmartcardError();
            /* --- Enhanced for multi-SEService Scenario --- */
            /*try {
                mReader.closeSessions(error);*/
            Enumeration sessions = mSessions.keys();
            while (sessions.hasMoreElements()) {
                Session session = (Session) sessions.nextElement();
                session.close();
            }
            /*} catch (RemoteException e) {
                throw new IllegalStateException(e.getMessage());
            }*/
        }
    }

    /// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @{
    /**
     * IOErrorEventType – an IOError occurred on the reader.
     */
    public static final int IOErrorEventType    = ISmartcardServiceCallback.IOErrorEventType;

    /**
     * SEInsertedEventType – the SE was in removed state and has been inserted in the reader.
     */
    public static final int SEInsertedEventType = ISmartcardServiceCallback.SEInsertedEventType;

    /**
     * SERemovalEventType – the SE was in inserted state and has been removed from the reader.
     */
    public static final int SERemovalEventType  = ISmartcardServiceCallback.SERemovalEventType;

    /**
     * Interface to receive event when asynchronous event occurs on the specified reader.
     */
    public interface EventCallBack {

        /**
         * Called by the framework when the event occurs.
         *
         * @param event the event instance generated by the framework.
         */
        void notify(Reader.Event event);
    }

    /**
     * An instance of that class is generated by the framework
     */
    public class Event {
        private Reader mReader;
        private int mEventType;

        public Event(Reader reader, int type) {
            mReader = reader;
            mEventType = type;
        }

        /**
         * Get the reader on which the event occurred.
         *
         * @return Return the reader on which the event occurred.
         */
        public Reader getReader() {
            return mReader;
        }

        /**
         * Get the type of the event.
         *
         * @return Return the type of the event.
         */
        public int getEventType() {
            return mEventType;
        }
    }

    public static final String TAG = "Reader";
    private final ArrayList<Reader.EventCallBack> mEventCallbackList = new ArrayList<>();

    /**
     * Register the specified callback in the set of reader callbacks. The calling
     * order is undetermined. If the callback has been already registered the method
     * does nothing.
     *
     * @param callback the event callback to be notified when an event occurs
     */
    public void registerReaderEventCallback(Reader.EventCallBack callback) {
        synchronized (mEventCallbackList) {
            if (callback == null) {
                Log.v(TAG, "The callback is null");
            } else {
                if (mEventCallbackList.contains(callback) == false) {
                    mEventCallbackList.add(callback);
                } else {
                    Log.v(TAG, "The callback has been already registered (" + callback + ")");
                }
            }
        }
    }

    /**
     * Unregister the specified callback available in the set of reader callbacks.
     *
     * @param callback the event callback to be unregistered from the callback set.
     * @return <code>true</code> if the callback has been previously registered and
     * has been unregistered, <code>false</code> if the callback has not been
     * previously registered.
     */
    public boolean unregisterReaderEventCallback(Reader.EventCallBack callback) {
        synchronized (mEventCallbackList) {
            if (callback == null) {
                Log.v(TAG, "The callback is null");
                return false;
            } else {
                return mEventCallbackList.remove(callback);
            }
        }
    }

    /**
     * Called by the framework when the event occurs.
     *
     * @param eventType the event type generated by the framework.
     */
    void notifyEvent(int eventType) {
        Log.v(TAG, "notify eventType=" + eventType + " name=" + mName + " this=" + this);
        synchronized (mEventCallbackList) {
            for (Reader.EventCallBack c : mEventCallbackList) {
                final int type = eventType;
                final Reader.EventCallBack callback = c;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final Event event = new Event(Reader.this, type);
                        callback.notify(event);
                    }
                }).start();
            }
        }
    }
    /// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @}
}
