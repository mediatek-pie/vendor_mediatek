LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/packages/services/LPPeService))
include $(CLEAR_VARS)
LOCAL_MODULE := LPPeService
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := .apk
LOCAL_MODULE_TAGS := optional
LOCAL_CERTIFICATE := platform
LOCAL_SRC_FILES := LPPeService.apk
include $(BUILD_PREBUILT)
endif
