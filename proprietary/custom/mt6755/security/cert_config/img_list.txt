[single_bin]
logo.bin=logo
lk.img=lk
boot.img=boot
recovery.img=recovery
md1arm7.img=md1arm7
md1dsp.img=md1dsp
md1img.img=md1rom
md3img.img=md3rom
odmdtbo.img=dtbo
dtbo.img=dtbo

[multi_bin]
tee.img=atf,tee
