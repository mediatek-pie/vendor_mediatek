LOCAL_PATH := $(call my-dir)

##### build libgz_gp_client #####

include $(CLEAR_VARS)

LOCAL_MODULE := libgz_gp_client
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := tee_client_api.c

LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/include/ \
    $(LOCAL_PATH)/../uree/include/ \

LOCAL_CFLAGS += -Wall -Wno-unused-parameter -Wno-unused-function -Werror
LOCAL_STATIC_LIBRARIES += libgz_uree
LOCAL_SHARED_LIBRARIES += liblog

include $(BUILD_STATIC_LIBRARY)

##### build gz_gp_test #####

include $(CLEAR_VARS)

LOCAL_MODULE := gz_gp_test
LOCAL_SRC_FILES := test/test.c
LOCAL_MODULE_TAGS := optional

LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/include \
    $(LOCAL_PATH)/../uree/include/ \

LOCAL_CFLAGS += -Wall -Wno-unused-parameter -Wno-unused-function -Werror

LOCAL_MODULE_STEM_32 := gz_gp_test32
LOCAL_MODULE_STEM_64 := $(LOCAL_MODULE)
LOCAL_MULTILIB := both

LOCAL_STATIC_LIBRARIES += libgz_gp_client \
                          libgz_uree
LOCAL_SHARED_LIBRARIES += liblog
include $(BUILD_EXECUTABLE)