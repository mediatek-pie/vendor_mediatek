LOCAL_PATH := $(call my-dir)

##### build uree_test #####

include $(CLEAR_VARS)

LOCAL_MODULE := uree_test
LOCAL_SRC_FILES := test.c
LOCAL_MODULE_TAGS := optional
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/../include \
    $(TOP)/vendor/mediatek/proprietary/external/libion_mtk/include

LOCAL_CFLAGS += -Wall -Wno-unused-parameter -Wno-unused-function -Werror

LOCAL_MODULE_STEM_32 := uree_test32
LOCAL_MODULE_STEM_64 := $(LOCAL_MODULE)
LOCAL_MULTILIB := both

LOCAL_STATIC_LIBRARIES += libgz_uree
LOCAL_SHARED_LIBRARIES += liblog
LOCAL_SHARED_LIBRARIES += libion libion_mtk

ifeq ($(TRUSTONIC_TEE_SUPPORT),yes)
LOCAL_CFLAGS += -DTRUSTONIC_TEE_ENABLED
LOCAL_SHARED_LIBRARIES += libMcClient
LOCAL_CFLAGS += -DTBASE_API_LEVEL=5
endif
include $(BUILD_EXECUTABLE)

