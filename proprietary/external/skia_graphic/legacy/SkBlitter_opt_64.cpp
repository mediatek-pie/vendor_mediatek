
/*
 * Copyright 2008 The Android Open Source Project
 *
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */
#include "SkColor.h" 
#include "SkColorPriv.h"

void GetAlpha8888_opt(const SkPMColor* SK_RESTRICT src, 
                             int width, uint8_t* SK_RESTRICT alpha) {
	 if (width <= 0) {
		 return ;
	 }


#if defined(__ARM_HAVE_NEON_COMMON) && defined(SK_CPU_LENDIAN)	
	 asm volatile (
			 "prfm		 PLDL1KEEP, [%[src]]		 \n\t"			 
			 "subs		 %[width], %[width], #8 	 \n\t"	 // width -= 8
			 "blt		 2f 						 \n\t"
			 
			 "1:									 \n\t"
			 "ld4		 {v0.8B, v1.8B, v2.8B, v3.8B}, [%[src]], #32 \n\t"	 // Load 8 pixels
			 "prfm		 PLDL1KEEP, [%[src], #256]	 \n\t"
			 "st1		 {v3.8B}, [%[alpha]], #8	 \n\t"
			 "subs		 %[width], %[width], #8 	 \n\t"
			 "bge		 1b 						 \n\t"
			 
			 "2:									 \n\t"
			 "add		 %[width], %[width], #8 	 \n\t"
			 "cmp		 %[width], #0				 \n\t"
			 "ble		 4f 						 \n\t"
			 
			 "3:									 \n\t"	 
			 "ldr		 w5, [%[src]], #4			 \n\t"	 // load one pixel
			 "lsr		 w5, w5, #24				 \n\t"
			 "strb		 w5, [%[alpha]], #1 		 \n\t"	 // Store one byte to %[alpha]
			 "subs		 %[width], %[width], #1 	 \n\t"
			 "bgt		 3b 						 \n\t"
			 
			 "4:									 \n\t"
			 :[width] "+r" (width), [src] "+r" (src), [alpha] "+r" (alpha)
			 :
			 :"memory", "x5", "v0", "v1", "v2", "v3"
			 );
#else
	 asm volatile (
			 "prfm		 PLDL1KEEP, [%[src]]		 \n\t"			 
			 "subs		 %[width], %[width], #1 	 \n\t"	 // width -= 8
			 "blt		 2f 						 \n\t"
			 
			 "1:									 \n\t"	 
			 "ldr		 w5, [%[src]], #4			 \n\t"	 // load one pixel
			 "lsr		 w5, w5, #24				 \n\t"
			 "strb		 w5, [%[alpha]], #1 		 \n\t"	 // Store one byte to %[alpha]
			 "subs		 %[width], %[width], #1 	 \n\t"
			 "bgt		 1b 						 \n\t"
			 
			 "2:									 \n\t"
			 :[width] "r+" (width), [src] "r+" (src), [alpha] "r+" (alpha)
			 :
			 :"memory", "x5"
			 );
#endif
 }


void blend32_16_row_opt(SkPMColor src_expand, uint32_t scale, 
								uint16_t device[], int width) {
	  if (width <= 0) {
		  return ;
	  }
  
#if defined(__ARM_HAVE_NEON_COMMON)	
	  asm volatile(
			  "cmp		  %[width], #0					  \n\t"   // width > 0
			  "ble		  5f							  \n\t"
			  
			  "prfm 	  PLDL1KEEP, [%[device]]		  \n\t"
			  "mov		  w9, #0x07e0					  \n\t"
			  "mov		  w8, #0xf81f					  \n\t"
			  "orr		  w9, w8, w9, lsl #16			  \n\t"   // w9 = 0x07e0f81f
			  
			  "dup		  v7.4S, w9 					  \n\t"
			  "dup		  v6.4S, %w[src_expand] 		  \n\t"
			  "dup		  v8.4S, %w[scale]				  \n\t"
			  "lsr		  w4, %w[width], #4 			  \n\t"   // w4: width/16
			  "cmp		  w4, #0						  \n\t"
			  "ble		  2f							  \n\t"
			  
			  "1:										  \n\t"
			  "ld1		  {v0.8H, v1.8H}, [%[device]]	  \n\t"   // load 16 pixels
			  "prfm 	  PLDL1KEEP, [%[device], 256]	  \n\t"
			  "subs 	  w4, w4, #1					  \n\t"
			  
			  "and		  v2.16B, v0.16B, v7.16B		  \n\t"
			  "and		  v4.16B, v1.16B, v7.16B		  \n\t"   // 0G0/R0B.......
			  "rev32	  v3.8H, v0.8H					  \n\t"
			  "rev32	  v5.8H, v1.8H					  \n\t"   // revert pixel(n) and pixel(n+1)
			  "and		  v3.16B, v3.16B, v7.16B		  \n\t"
			  "and		  v5.16B, v5.16B, v7.16B		  \n\t"   // 0G0/R0B......
			  
			  "mul		  v2.4S, v2.4S, v8.4S			  \n\t"
			  "mul		  v4.4S, v4.4S, v8.4S			  \n\t"
			  "mul		  v3.4S, v3.4S, v8.4S			  \n\t"
			  "mul		  v5.4S, v5.4S, v8.4S			  \n\t"   // dst * scale
			  "add		  v2.4S, v2.4S, v6.4S			  \n\t"
			  "add		  v4.4S, v4.4S, v6.4S			  \n\t"
			  "add		  v3.4S, v3.4S, v6.4S			  \n\t"
			  "add		  v5.4S, v5.4S, v6.4S			  \n\t"   // dst * scale + src_expand
			  "ushr 	  v2.4S, v2.4S, #5				  \n\t"
			  "ushr 	  v4.4S, v4.4S, #5				  \n\t"
			  "ushr 	  v3.4S, v3.4S, #5				  \n\t"
			  "ushr 	  v5.4S, v5.4S, #5				  \n\t"   // (dst * scale + src_expand) >> 5
			  
			  "and		  v2.16B, v2.16B, v7.16B		  \n\t"
			  "and		  v4.16B, v4.16B, v7.16B		  \n\t"
			  "and		  v3.16B, v3.16B, v7.16B		  \n\t"
			  "and		  v5.16B, v5.16B, v7.16B		  \n\t"   // 8888 to RGB565
			  "rev32	  v3.8H, v3.8H					  \n\t"
			  "rev32	  v5.8H, v5.8H					  \n\t"
			  "orr		  v0.16B, v2.16B, v3.16B		  \n\t"
			  "orr		  v1.16B, v4.16B, v5.16B		  \n\t"
			  
			  "st1		  {v0.8H, v1.8H}, [%[device]], #32	  \n\t"
			  "bne		  1b							  \n\t"
			  
			  "2:										  \n\t"
			  "and		  w4, %w[width], #0x0F			  \n\t"   // w4: width % 16
			  "cmp		  w4, #0						  \n\t"
			  "beq		  5f							  \n\t"   
			  "cmp		  w4, #8						  \n\t"
			  "blt		  3f							  \n\t"
			  
			  "ld1		  {v0.8H}, [%[device]]			  \n\t"   // Handle 8 pixel for one time
			  "and		  v2.16B, v0.16B, v7.16B		  \n\t"
			  "rev32	  v3.8H, v0.8H					  \n\t"
			  "and		  v3.16B, v3.16B, v7.16B		  \n\t"
			  
			  "mul		  v2.4S, v2.4S, v8.4S			  \n\t"
			  "mul		  v3.4S, v3.4S, v8.4S			  \n\t"   // dst * scale
			  "add		  v2.4S, v2.4S, v6.4S			  \n\t"
			  "add		  v3.4S, v3.4S, v6.4S			  \n\t"   // dst * scale + src_expand
			  "ushr 	  v2.4S, v2.4S, #5				  \n\t"
			  "ushr 	  v3.4S, v3.4S, #5				  \n\t"   // (dst * scale + src_expand) >> 5
			  
			  "and		  v2.16B, v2.16B, v7.16B		  \n\t"
			  "and		  v3.16B, v3.16B, v7.16B		  \n\t"
			  "rev32	  v3.8H, v3.8H					  \n\t"
			  "orr		  v0.16B, v2.16B, v3.16B		  \n\t"
			  "st1		  {v0.8H}, [%[device]], #16 	  \n\t"
			  
			  "3:										  \n\t"
			  "and		  w4, %w[width], #0x07			  \n\t"
			  "cmp		  w4, #0						  \n\t"
			  "beq		  5f							  \n\t"
			  
			  "4:										  \n\t"
			  "ldrh 	  w10, [%[device]]				  \n\t"   // w10: one pixel 565
			  "subs 	  w4, w4, #1					  \n\t"
			  "prfm 	  PLDL1KEEP, [%[device], #16]	  \n\t"
			  "orr		  w8, w10, w10, lsl #16 		  \n\t"
			  "and		  w10, w8, w9					  \n\t"
			  "madd 	  w8, w10, %w[scale], %w[src_expand]  \n\t"   // dst * scale + src_expand
			  "and		  w8, w9, w8, lsr #5			  \n\t"
			  "orr		  w10, w8, w8, lsr #16			  \n\t"
			  "strh 	  w10, [%[device]], #2			  \n\t"   //device += 2
			  "bne		  4b							  \n\t"
			  
			  "5:										  \n\t"
			  :[device] "+r" (device)
			  :[width] "r" (width), [src_expand] "r" (src_expand), [scale] "r" (scale)
			  :"memory", "x4", "x8", "x9", "x10", "v0", "v1", "v2", "v3", "v4", "v5", "v6", "v7", "v8"
			  );
#else	
	  asm volatile(
			  "prfm 	  PLDL1KEEP, [%[device]]		  \n\t"
			  "mov		  w9, #0x07e0					  \n\t"
			  "mov		  w8, #0xf81f					  \n\t"
			  "orr		  w9, w8, w9, lsl #16			  \n\t"
  
			  "1:										  \n\t"
			  "ldrh 	  w10, [%[device]]				  \n\t"   // w10: one 565 pixel
			  "subs 	  %[width], %[width], #1		  \n\t"
			  "prfm 	  PLDL1KEEP, [%[device], #16]	  \n\t"
			  
			  "orr		  w8, w10, w10, lsl #16 		  \n\t"   // copy uint16 to MSB16
			  "and		  w10, w8, w9					  \n\t"   // 0G0R0B
			  "madd 	  w8, w10, %w[scale], %w[src_expand]  \n\t"   // dst * scale + src_expand
			  "and		  w8, w9, w8, lsr #5			  \n\t"   // (dst * scale + src_expand)>>5
			  "orr		  w10, w8, w8, lsr #16			  \n\t"
			  "strh 	  w10, [%[device]], #2			  \n\t"   //device += 2
			  "bne		  1b							  \n\t"
				  
			  :[device] "+r" (device)
			  :[width] "r" (width), [scale] "r" (scale), [src_expand] "r" (src_expand)
			  :"memory", "x8", "x9", "x10"
			  );
#endif
}


#if defined(__ARM_HAVE_NEON_COMMON)	
void blend16_32_row_neon_opt(SkPMColor* SK_RESTRICT dst,
                                 		   const uint16_t* SK_RESTRICT src,
                                 		   int count) {
    if (count <= 0) {
		return ;
    }

	asm volatile (
			"cmp		%[count], #8					\n\t"
			"movi		v6.4H, #0xFF, lsl #8			\n\t"	// 0xFF00
			"blt		2f								\n\t"
			"movi		v5.8B, #0xFF					\n\t"	// 0xFF
			"sub		%[count], %[count], #8			\n\t"
			"prfm		PLDL1KEEP, [%[src]]				\n\t"
			
			"1: 										\n\t"	// Handle 8 pixels in one loop
			"ld1		{v0.8H}, [%[src]], #16			\n\t"	// Load 8 pixels of 565 formate
			"prfm		PLDL1KEEP, [%[src], #128]		\n\t"
			"subs		%[count], %[count], #8			\n\t"
			"shrn		v2.8B, v0.8H, #8				\n\t" 	// R: MSB5 is Red, LSB3 is 0
			"shrn		v3.8B, v0.8H, #3				\n\t" 	// G: MSB6 is Green, LSB2 is 0
			"sli		v0.8H, v0.8H, #5				\n\t" 	// B: LSB 6 is Blue, GBB(655)
			"sri		v2.8B, v2.8B, #5				\n\t" 	// R: MSB5 + MSB3 = R
			"sri		v3.8B, v3.8B, #6				\n\t" 	// G: MSB6 + MSB2 = G
			"shrn		v4.8B, v0.8H, #2				\n\t" 	// B: MSB5 + MSB3 = B
			"st4		{v2.8B, v3.8B, v4.8B, v5.8B}, [%[dst]], #32 \n\t"
			
			"bge		1b								\n\t"
			"add		%[count], %[count], #8			\n\t"
			
			
			"2: 										\n\t"
			: [src] "+r" (src), [dst] "+r" (dst), [count] "+r" (count)
			:
			: "cc", "memory","v0","v1","v2","v3","v4","v5","v6","v7","v8"
	);

	// process the rest of 7 pixels
	for (int i = (count & 7); i > 0; --i) {
		*dst = SkPixel16ToPixel32(*src);
		src += 1;
		dst += 1;
	}	
}

void S32A_Opaque_BlitRow32_neon_opt(SkPMColor* SK_RESTRICT dst,
                                const SkPMColor* SK_RESTRICT src,
                                int count, U8CPU alpha) {    
    SkASSERT(255 == alpha);

	if (count <= 0) {
		return ;
	}
	
	asm volatile (		
			"prfm	PLDL1KEEP, [%[src]]							\n\t"
			"prfm	PLDL1KEEP, [%[dst]]							\n\t"
		
			"subs	%[count], %[count], #8						\n\t"	// count -= 8
			"blt 	 .LOpaque_less_than_8						\n\t"
			
			"movi	 v14.8H, #0x01, lsl #8						\n\t"	//256
		
/*===>*/	".LOpaque_blitrow32_loop_8:							\n\t"
			
			"ld4 	{v4.8B, v5.8B, v6.8B, v7.8B}, [%[dst]]		\n\t"	// Dst: ARGB Load 4 Pixels
			"ld4 	{v0.8B, v1.8B, v2.8B, v3.8B}, [%[src]], #32 \n\t"	// Src: ARGB Load 4 Pixels
		
			"subs	 %[count], %[count], #8 					\n\t"	// count -= 8
			
			"usubw	 v15.8H, v14.8H, v3.8B						\n\t"	// 256 - Src.alpha
			
			"uxtl	 v8.8H, v4.8B								\n\t"
			"uxtl	 v9.8H, v5.8B								\n\t"
			"uxtl	 v10.8H, v6.8B								\n\t"
			"uxtl	 v11.8H, v7.8B								\n\t"	// expand dst pixel to 16-Bit
		
			"mul 	 v8.8H, v8.8H, v15.8H						\n\t"
			"mul 	 v9.8H, v9.8H, v15.8H						\n\t"
			"mul 	 v10.8H, v10.8H, v15.8H						\n\t"
			"mul 	 v11.8H, v11.8H, v15.8H 					\n\t"	// Dst * (256 - Src.alpha)
		
			"prfm	 PLDL1KEEP, [%[dst], #64]					\n\t"
			"prfm	 PLDL1KEEP, [%[src], #32]					\n\t"
		
			"shrn	 v8.8B, v8.8H, #8							\n\t"
			"shrn	 v9.8B, v9.8H, #8							\n\t"
			"shrn	 v10.8B, v10.8H, #8							\n\t"
			"shrn	 v11.8B, v11.8H, #8 						\n\t"	// Keep MSB8 as new pixel
		
			"add 	 v4.8B, v8.8B, v0.8B						\n\t"
			"add 	 v5.8B, v9.8B, v1.8B						\n\t"
			"add 	 v6.8B, v10.8B, v2.8B						\n\t"
			"add 	 v7.8B, v11.8B, v3.8B						\n\t"	// Src + Dst * (256 - Src.alpha)
		
			"st4 	 {v4.8B, v5.8B, v6.8B, v7.8B}, [%[dst]], #32	\n\t"   
		
			"bge 	.LOpaque_blitrow32_loop_8					\n\t"
		
/*===>*/	".LOpaque_less_than_8:								\n\t"
			"add 	 %[count], %[count], #8						\n\t"
			"cmp 	 %[count], #0								\n\t"
			"beq 	 .LOpaque_exit								\n\t"

			
			"mov	w10, #0x00FF 								\n\t"
			"orr	w10, w10, w10, lsl #16 						\n\t"
		
			"subs	 %[count], %[count], #2 					\n\t"	// count -= 2
			"blt 	 .LOpaque_blitrow32_single_loop				\n\t"
		
/*===>*/	".LOpaque_blitrow32_double_loop:					\n\t"

			"ldp 	 w3, w4, [%[dst]]							\n\t"	// Load 2 Dst Pixels
			"ldp 	 w5, w6, [%[src]], #8						\n\t"	// Load 2 Src Pixels
		
			"prfm	 PLDL1KEEP, [%[dst], #16]					\n\t"
			"prfm	 PLDL1KEEP, [%[src], #0]					\n\t"
		
			"orr 	 w9, w3, w4									\n\t"
			"cmp 	 w9, #0										\n\t"
			"beq 	 .LOpaque_blitrow32_loop_cond				\n\t"
		
			// First iteration
			"lsr 	 w7, w5, #24			   					\n\t"	//pix0: src.alpha
			"and 	 w8, w3, w10			   					\n\t"   //pix0: rb = (dst & mask)
			"and 	 w9, w10, w3, lsr #8			   			\n\t"   //pix0: ag = (dst >> 8) & mask
			"mov 	 w12, #256			   						\n\t"	
			"sub 	 w7, w12, w7			   					\n\t"   //pix0: 256-src.alpha
			"mul 	 w11, w8, w7			   					\n\t"   //pix0: rb * (256-src.alpha)    
			"mul 	 w3, w9, w7			   						\n\t"   //pix0: ag * (256-src.alpha)
			"and 	 w11, w10, w11, lsr #8			   			\n\t"   //pix0: (RB>>8) & mask
			"and 	 w3, w3, w10, lsl #8			   			\n\t"   //pix0: AG & ~mask
			"orr 	 w3, w3, w11			   					\n\t"   //pix0: combine rb & ag
			
			//Second iteration
			"lsr 	 w7, w6, #24			   					\n\t"   //pix1: src.alpha
			"and 	 w8, w4, w10			   					\n\t"   //pix1: rb = (dst & mask)
			"and	 w9, w10, w4, lsr #8						\n\t"	//pix1: ag = (dst>>8) & mask
			"mov 	 w12, #256			   						\n\t"
			"sub 	 w7, w12, w7			   					\n\t"   //pix1: 255-src.alpha
			"mul 	 w11, w8, w7				   				\n\t"   //pix1: RB = rb * scale
			"mul 	 w4, w9, w7 			   					\n\t"   //pix1: AG = ag * scale	 
			"and 	 w11, w10, w11, lsr #8			   			\n\t"   //pix1: (RB>>8) & mask
			"and 	 w4, w4, w10, lsl #8				   		\n\t"   //pix1: AG & ~mask
			"orr 	 w4, w4, w11			   					\n\t"   //pix1: combine RB and AG
		
			"add 	 w5, w5, w3									\n\t"			   
			"add 	 w6, w6, w4 								\n\t"	// src + dst*(256 + src.alpha)
		
/*===>*/	".LOpaque_blitrow32_loop_cond:						\n\t"
			"subs	 %[count], %[count], #2						\n\t"
			"stp 	 w5, w6, [%[dst]], #8						\n\t"
		
			"bge 	 .LOpaque_blitrow32_double_loop				\n\t"
		
/*===>*/	".LOpaque_blitrow32_single_loop:					\n\t"
			"add 	 %[count], %[count], #1						\n\t"
			"cmp 	 %[count], #0								\n\t"
			"blt 	 .LOpaque_exit								\n\t"
		
			"ldr 	 w3, [%[dst]]								\n\t"	// Load Dst one pixel
			"ldr 	 w5, [%[src]], #4							\n\t"	// Load Src one pixel
		
			"cmp 	 w3, #0 									\n\t"	// dst==0, src->dst
			"beq 	 .LOpaque_blitrow32_single_store			\n\t"
		
			"lsr 	 w7, w5, #24			   					\n\t"	// pix0: src.alpha
			"and 	 w8, w3, w10			   					\n\t"   // pix0: RB = (dst & mask)
			"and 	 w9, w10, w3, lsr #8			   			\n\t"	// pix0: AG = (dst>>8) & mask
			"mov 	 w12, #256			   						\n\t"
			"sub 	 w7, w12, w7			   					\n\t"   // pix0: 256-alpha			
			"mul 	 w8, w8, w7 			   					\n\t"   // pix0: RB = rb * scale
			"mul 	 w9, w9, w7 			   					\n\t"   // pix0: AG = ag * scale    
			"and 	 w8, w10, w8, lsr #8			   			\n\t"	// pix0: r8 = (RB>>8) & mask
			"and 	 w9, w9, w10, lsl #8			   			\n\t"	// pix0: r9 = AG & ~mask
			"orr 	 w3, w8, w9 			   					\n\t"   // pix0: combine RB and AG
		
			"add 	 w5, w5, w3 			   					\n\t"	// src + dst*(256 + src.alpha)
		
/*===>*/	".LOpaque_blitrow32_single_store:					\n\t"
			"str 	 w5, [%[dst]], #4							\n\t"
		
/*===>*/	".LOpaque_exit:										\n\t"
			: [dst] "+r" (dst), [src] "+r" (src), [count] "+r" (count)
			:
			: "cc", "memory", "x4", "x5", "x6", "x7", "x8", "x9", "x10", "x11", "x12", "v0", "v1", "v2", "v3", "v4", "v5", "v6", "v7", "v8", "v9", "v10", "v11", "v14", "v15"
	);
}
void S32A_Blend_BlitRow32_neon_opt(SkPMColor* SK_RESTRICT dst,
                         const SkPMColor* SK_RESTRICT src,
                         int count, U8CPU alpha) {
    SkASSERT(255 >= alpha);

    if (count <= 0) {
        return;
    }
	
    unsigned alpha256 = SkAlpha255To256(alpha);

	asm volatile (
			"prfm	PLDL1KEEP, [%[dst]]							\n\t"
			"prfm	PLDL1KEEP, [%[src]]							\n\t"
		
			"subs	%[count], %[count], #8						\n\t"	// count -= 8
			"blt 	.Lless_then_8								\n\t"
		
			"movi	v19.8H, #1, lsl #8							\n\t"	// 256
			"dup 	v20.8H, %w[alpha256]						\n\t"	// src_scale
		
/*===>*/	".Lblitrow32_loop_8:								\n\t"
			"ld4 	{v4.8B, v5.8B, v6.8B, v7.8B}, [%[dst]]		\n\t"	// dest: RGBA
			"ld4 	{v0.8B, v1.8B, v2.8B, v3.8B}, [%[src]], #32 \n\t"	//source: RGBA
		
			"uxtl	v8.8H, v4.8B								\n\t"	// expand r/g/b/a from 8bit to 16bit
			"uxtl	v9.8H, v5.8B								\n\t"
			"uxtl	v10.8H, v6.8B								\n\t"
			"uxtl	v11.8H, v7.8B								\n\t"	// Dst.alpha (src_scale)
		
			"uxtl	v12.8H, v0.8B								\n\t"	// expand r/g/b/a from 8bit to 16bit
			"uxtl	v13.8H, v1.8B								\n\t"
			"uxtl	v14.8H, v2.8B								\n\t"
			"uxtl	v15.8H, v3.8B								\n\t"	// Src.alhpa
		
			"mul 	v16.8H, v20.8H, v15.8H						\n\t"	
			"ushr	v16.8H, v16.8H, #8							\n\t"	
			"sub 	v16.8H, v19.8H, v16.8H						\n\t"	// dst_scale: 256 - alpha * Src.alpha/256
		
			"mul 	v8.8H, v8.8H, v16.8H						\n\t"
			"mul 	v9.8H, v9.8H, v16.8H						\n\t"
			"mul 	v10.8H, v10.8H, v16.8H						\n\t"
			"mul 	v11.8H, v11.8H, v16.8H						\n\t" 	// dst * dst_scale
		
			"mul 	v12.8H, v12.8H, v20.8H						\n\t"
			"mul 	v13.8H, v13.8H, v20.8H						\n\t"
			"mul 	v14.8H, v14.8H, v20.8H						\n\t"
			"mul 	v15.8H, v15.8H, v20.8H						\n\t"	// src * src_scale
		
			"subs	%[count], %[count], #8						\n\t"	// count -= 8
		
			"prfm	PLDL1KEEP, [%[dst], #64]					\n\t"
			"prfm	PLDL1KEEP, [%[src], #32]					\n\t"
		
			"uqadd	v12.16B, v8.16B, v12.16B					\n\t"	// ?
			"uqadd	v13.16B, v9.16B, v13.16B					\n\t"
			"uqadd	v14.16B, v10.16B, v14.16B					\n\t"
			"uqadd	v15.16B, v11.16B, v15.16B					\n\t"	// src * src_scale + dst * dst_scale
		
			"shrn	v4.8B, v12.8H, #8							\n\t"
			"shrn	v5.8B, v13.8H, #8							\n\t"
			"shrn	v6.8B, v14.8H, #8							\n\t"
			"shrn	v7.8B, v15.8H, #8							\n\t"	// all pixel / 256 
		
			"st4 	{v4.8B, v5.8B, v6.8B, v7.8B}, [%[dst]], #32	\n\t"
			
			"bge 	.Lblitrow32_loop_8							\n\t"
			
/*===>*/	".Lless_then_8:										\n\t"
		
			"add 	%[count], %[count], #8						\n\t"	// count += 8
			"cmp 	%[count], #0								\n\t"
			"beq 	.Lexit										\n\t"
		
			"mov 	w10, #0x00FF00FF							\n\t"	// mask: 0x00FF00FF
		
			"subs	%[count], %[count], #2						\n\t"	// count -= 2
			"blt 	.Lblitrow32_single_loop						\n\t"
			
/*===>*/	".Lblitrow32_double_loop:							\n\t"
		
			"ldp 	w4, w5, [%[dst]]							\n\t"	// Load Dst 2 pixels
			"ldp 	w6, w7, [%[src]], #8						\n\t"	// Load Src 2 pixels
			
			"prfm	PLDL1KEEP, [%[dst], #32]					\n\t"
			"prfm	PLDL1KEEP, [%[src]]							\n\t"
		
			//First iteration
			"lsr 	w14, w6, #24								\n\t"
			"mul 	w14, w14, %w[alpha256]						\n\t"
			"lsr 	w14, w14, #8								\n\t"	
			"mov 	w15, #256									\n\t"
			"sub 	w14, w15, w14								\n\t"	// dst_scale: 256 - alpha * Src.alpha/256
			
			"and 	w8, w6, w10 								\n\t"	// Src: -R-B
			"and 	w9, w10, w6, lsr #8 						\n\t"	// Src: -A-G
			"mul 	w11, w8, %w[alpha256]						\n\t"
			"mul 	w6, w9, %w[alpha256]						\n\t"	// src * src_scale
			"and 	w11, w10, w11, lsr #8						\n\t"	// (-R-B>>8) & mask      (MSB8)
			"and 	w6, w6, w10, lsl #8 						\n\t"	// (-A-G)       & ~mask    (MSB8)
			"orr 	w6, w6, w11 								\n\t"	// combine Src RB & AG
		
			"and 	w8, w4, w10 								\n\t"	// Dst: -R-B
			"and 	w9, w10, w4, lsr #8 						\n\t"	// Dst: -A-G
			"mul 	w11, w8, w14								\n\t"
			"mul 	w4, w9, w14 								\n\t"	// dst * dst_scale
			"and 	w11, w10, w11, lsr #8						\n\t"	// (-R-B>>8) & mask      (MSB8)
			"and 	w4, w4, w10, lsl #8 						\n\t"	// (-A-G)       & ~mask    (MSB8)
			"orr 	w4, w4, w11 								\n\t"	// combine Dst RB & AG
		
			"add 	w6, w6, w4									\n\t"	// src * src_scale + dst * dst_scale
		
			//Second iteration
			"lsr 	w14, w7, #24								\n\t"
			"mul 	w14, w14, %w[alpha256]						\n\t"
			"lsr 	w14, w14, #8								\n\t"
			"mov 	w15, #256									\n\t"
			"sub 	w14, w15, w14								\n\t"	// dst_scale: 256 - alpha * Src.alpha/256
		
			"and 	w8, w7, w10 								\n\t"	// Src: -R-B
			"and 	w9, w10, w7, lsr #8 						\n\t"	// Src: -A-G
			"mul 	w11, w8, %w[alpha256]						\n\t"
			"mul 	w7, w9, %w[alpha256]							\n\t"	// Src * src_scale
			"and 	w11, w10, w11, lsr #8						\n\t"	// (-R-B>>8) & mask      (MSB8)
			"and 	w7, w7, w10, lsl #8 						\n\t"	// (-A-G)       & ~mask    (MSB8)
			"orr 	w7, w7, w11 								\n\t"	// Combine Src RB & AG
		
			"and 	w8, w5, w10 								\n\t"	// Src: -R-B
			"and 	w9, w10, w5, lsr #8 						\n\t"	// Src: -A-G
			"mul 	w11, w8, w14								\n\t"
			"mul 	w5, w9, w14 								\n\t"	// Dst * dst_scale
			"and 	w11, w10, w11, lsr #8						\n\t"	// (-R-B>>8) & mask      (MSB8)
			"and 	w5, w5, w10, lsl #8 						\n\t"	// (-A-G)       & ~mask    (MSB8)
			"orr 	w5, w5, w11 								\n\t"	// combine Dst RB & AG
			
			"add 	w7, w7, w5									\n\t"	// src * src_scale + dst * dst_scale
		
			"stp 	w6, w7, [%[dst]], #8						\n\t"
			
			"subs	 %[count], %[count], #2						\n\t"	// count -= 2
			"bge 	 .Lblitrow32_double_loop					\n\t"
		
/*===>*/	".Lblitrow32_single_loop:							\n\t"

			"add 	%[count], %[count], #1						\n\t"
			"cmp 	%[count], #0								\n\t"
			"blt 	.Lexit										\n\t"
		
			"ldr 	w4, [%[dst]]								\n\t"	// Load one Dst pixel
			"ldr 	w6, [%[src]], #4							\n\t"	// Load one Src pixel
		
			"lsr 	w14, w6, #24								\n\t"
			"mul 	w14, %w[alpha256], w14						\n\t"
			"lsr 	w14, w14, #8								\n\t"
			"mov 	w15, #256									\n\t"	
			"sub 	w14, w15, w14								\n\t"	// dst_scale: 256 - alpha * Src.alpha/256
			
			"and 	w8, w6, w10 								\n\t"	// Src: RB
			"and 	w9, w10, w6, lsr #8 						\n\t"	// Src: AG
			"mul 	w11, w8, %w[alpha256] 						\n\t"	// RB * src_scale
			"mul 	w6, w9, %w[alpha256]						\n\t"	// AG * src_scale
			"and 	w11, w10, w11, lsr #8						\n\t"	// (RB>>8) & mask
			"and 	w6, w6, w10, lsl #8 						\n\t"	// AG  & ~ mask
			"orr 	w6, w6, w11 								\n\t"	// Combine Src RB & AG
		
			"and 	w8, w4, w10 								\n\t"	// Dst: RB
			"and 	w9, w10, w4, lsr #8 						\n\t"	// Dst: AG	
			"mul 	w11, w8, w14								\n\t"	// RB * dst_scale
			"mul 	w4, w9, w14 								\n\t"	// Dst * dst_scale
			"and 	w11, w10, w11, lsr #8						\n\t"	// (RB>>8) * mask
			"and 	w4, w4, w10, lsl #8 						\n\t"	// AG & ~mask	
			"orr 	w4, w4, w11 								\n\t"	// combine RB & AG
			
			"add 	w6, w6, w4									\n\t"	// src * src_scale + dst * dst_sacle
		
			"str 	w6, [%[dst]], #4							\n\t"
			
/*===>*/	".Lexit:											\n\t"

			: [dst] "+r" (dst), [src] "+r" (src), [count] "+r" (count), [alpha256] "+r" (alpha256)
			:
			: "cc", "memory", "x4", "x5", "x6", "x7", "x8", "x9", "x10", "x11", "x14", "x15", "v0", "v1", "v2", "v3", "v4", "v5", "v6", "v7", "v8", "v9", "v10", "v11", "v12","v13", "v14", "v15", "v16", "v19", "v20"
			);	
}
#endif

