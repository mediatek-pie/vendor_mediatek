/*
 * Copyright (c) 2010, Code Aurora Forum. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "SkFixed.h"
#include <stdlib.h>

void S32_Opaque_D32_filter_DX_shaderproc_neon(const unsigned int* image0, const unsigned int* image1,
                                        SkFixed fx, unsigned int maxX, unsigned int subY,
                                        unsigned int* colors, SkFixed dx, int count) {
	asm volatile(
			"mov		w4, #16						\n\t"	// 16
			"sub		w5, w4, %w[subY]			\n\t"	// 16-y
			"movi		v12.4H, #0x10				\n\t"	// vector: 16
			"dup		v13.8B, %w[subY]			\n\t"	// vector: y
			"dup		v14.8B, w5					\n\t"	// vector: 16-y

			"cmp		%[count], #4				\n\t"
			"blt		loop4_end					\n\t"

			"loop4:									\n\t"	// handle 4 pixels

			"asr		w4, %w[fx], #12				\n\t"	// Handle pixel0
			"asr		w5, %w[fx], #16				\n\t"	// fx integer part
			"add		%[fx], %[fx], %[dx]			\n\t"
			"and		w4, w4, #0xF				\n\t"	// subX0: fx decimal part
			"add		x7, %[image1], x5, lsl #2	\n\t"
			"add		x6, %[image0], x5, lsl #2	\n\t"			
			"dup		v8.4H, w4		 			\n\t"	//   	pix0: x
			"ld1		{v1.2S}, [x7]				\n\t"	//	pix0: [a11|a10]
			"ld1		{v0.2S}, [x6]				\n\t"	//	pix0: [a01|a00]
	
			"asr		w4, %w[fx], #12				\n\t"	// Handle pixel1
			"asr		w5, %w[fx], #16				\n\t"	// fx integer part
			"add		%[fx], %[fx], %[dx]			\n\t"
			"and		w4, w4, #0xF				\n\t"	// subX1: fx decimal part
			"add		x7, %[image1], x5, lsl #2	\n\t"
			"add		x6, %[image0], x5, lsl #2	\n\t"						
			"dup		v9.4H, w4		 			\n\t"	//   	pix1: x
			"ld1		{v3.2S}, [x7]				\n\t"	//	pix1: [a11|a10]
			"ld1		{v2.2S}, [x6]				\n\t"	//	pix1: [a01|a00]

			"asr		w4, %w[fx], #12				\n\t"	// Handle pixel2
			"asr		w5, %w[fx], #16				\n\t"	// fx integer part
			"add		%[fx], %[fx], %[dx]			\n\t"
			"and		w4, w4, #0xF				\n\t"	// subX2: fx decimal part
			"add		x7, %[image1], x5, lsl #2	\n\t"
			"add		x6, %[image0], x5, lsl #2	\n\t"			
			"dup		v10.4H, w4		 			\n\t"	//   	pix2: x
			"ld1		{v5.2S}, [x7]				\n\t"	//	pix2: [a11|a10]
			"ld1		{v4.2S}, [x6]				\n\t"	//	pix2: [a01|a00]

			"asr		w4, %w[fx], #12				\n\t"	// Handle pixel3
			"asr		w5, %w[fx], #16				\n\t"	// fx integer part
			"add		%[fx], %[fx], %[dx]			\n\t"
			"and		w4, w4, #0xF				\n\t"	// subX3: fx decimal part
			"add		x7, %[image1], x5, lsl #2	\n\t"
			"add		x6, %[image0], x5, lsl #2	\n\t"						
			"dup		v11.4H, w4 					\n\t"	//   	pix3: x
			"ld1		{v7.2S}, [x7]				\n\t"	//	pix3: [a11|a10]
			"ld1		{v6.2S}, [x6]				\n\t"	//	pix3: [a01|a00]	
			
			// -----------------------------------------//
			// filter 4 pixels
			"umull		v16.8H , v0.8B, v14.8B		\n\t"	// pix 0 : [a01|a00] * (16-y)
			"umull		v18.8H , v2.8B, v14.8B		\n\t"	// pix 1 : [a01|a00] * (16-y)
			"umull		v20.8H , v4.8B, v14.8B		\n\t"	// pix 2 : [a01|a00] * (16-y)
			"umull		v22.8H , v6.8B, v14.8B		\n\t"	// pix 3 : [a01|a00] * (16-y)
			
			"umull		v15.8H , v1.8B, v13.8B		\n\t"	// pix 0 : [a11|a10] * y
			"umull		v17.8H , v3.8B, v13.8B		\n\t"	// pix 1 : [a11|a10] * y
			"umull		v19.8H , v5.8B, v13.8B		\n\t"	// pix 2 : [a11|a10] * y
			"umull		v21.8H , v7.8B, v13.8B		\n\t"	// pix 3 : [a11|a10] * y

			"mov		D4, v16.D[1]				\n\t"	// pix 0: a01
			"mov		D5, v18.D[1]				\n\t"	// pix 1: a01
			"mov		D6, v20.D[1]				\n\t"	// pix 2: a01
			"mov		D7, v22.D[1]				\n\t"	// pix 3: a01

			"mul		v0.4H, v4.4H, v8.4H			\n\t"	// pix 0 : a01 * x
			"mul		v1.4H, v5.4H, v9.4H			\n\t"	// pix 1 : a01 * x
			"mul		v2.4H, v6.4H, v10.4H		\n\t"	// pix 2 : a01 * x
			"mul		v3.4H, v7.4H, v11.4H		\n\t"	// pix 3 : a01 * x
			
			"mov		D4, v15.D[1]				\n\t"	// pix 0: a11
			"mov		D5, v17.D[1]				\n\t"	// pix 1: a11
			"mov		D6, v19.D[1]				\n\t"	// pix 2: a11
			"mov		D7, v21.D[1]				\n\t"	// pix 3: a11

			"mla		v0.4H, v4.4H, v8.4H			\n\t"	// pix 0 : + a11 * x
			"mla		v1.4H, v5.4H, v9.4H			\n\t"	// pix 1 : + a11 * x
			"mla		v2.4H, v6.4H, v10.4H		\n\t"	// pix 2 : + a11 * x
			"mla		v3.4H, v7.4H, v11.4H		\n\t"	// pix 3 : + a11 * x
			
			"sub		v8.4H, v12.4H, v8.4H		\n\t"	// pix 0 : 16-x
			"sub		v9.4H, v12.4H, v9.4H		\n\t"	// pix 1 : 16-x
			"sub		v10.4H, v12.4H, v10.4H		\n\t"	// pix 2 : 16-x
			"sub		v11.4H, v12.4H, v11.4H		\n\t"	// pix 3 : 16-x

			"mla		v0.4H, v16.4H, v8.4H		\n\t"	// pix 0 : + a00 * (16-x)
			"mla		v1.4H, v18.4H, v9.4H		\n\t"	// pix 1 : + a00 * (16-x)
			"mla		v2.4H, v20.4H, v10.4H		\n\t"	// pix 2 : + a00 * (16-x)
			"mla		v3.4H, v22.4H, v11.4H		\n\t"	// pix 3 : + a00 * (16-x)

			"mla		v0.4H, v15.4H, v8.4H		\n\t"	// pix 0 : + a10 * (16-x)
			"mla		v1.4H, v17.4H, v9.4H		\n\t"	// pix 1 : + a10 * (16-x)
			"mla		v2.4H, v19.4H, v10.4H		\n\t"	// pix 2 : + a10 * (16-x)
			"mla		v3.4H, v21.4H, v11.4H		\n\t"	// pix 3 : + a10 * (16-x)

			"shrn		v4.8B, v0.8H, #8			\n\t"	
			"shrn		v5.8B, v1.8H, #8			\n\t"
			"shrn		v6.8B, v2.8H, #8			\n\t"
			"shrn		v7.8B, v3.8H, #8			\n\t"
			
			"st1		{v4.S}[0], [%[colors]], #4	\n\t"
			"st1		{v5.S}[0], [%[colors]], #4	\n\t"
			"st1		{v6.S}[0], [%[colors]], #4	\n\t"
			"st1		{v7.S}[0], [%[colors]], #4	\n\t"

			"sub		%[count], %[count], #4		\n\t"
			"cmp		%[count], #4				\n\t"
			"bge		loop4						\n\t"
		
			"loop4_end:								\n\t"
			
			"cmp		%[count], #0				\n\t"
			"ble		tailloop_end				\n\t"
			
			"tailloop:								\n\t"	// handle 1 pixel

			"asr		w4, %w[fx], #12				\n\t"
			"asr		w5, %w[fx], #16				\n\t"
			"add		%[fx], %[fx], %[dx]			\n\t"
			"and		w4, w4, #0xF				\n\t"
			"add		x7, %[image1], x5, lsl #2	\n\t"
			"add		x6, %[image0], x5, lsl #2	\n\t"
			"dup		v3.4H, w4		 			\n\t"
			"ld1		{v1.2S}, [x7]				\n\t"	//	pix0: [a11|a10]
			"ld1		{v0.2S}, [x6]				\n\t"	//	pix0: [a01|a00]

			"umull		v4.8H , v0.8B, v14.8B		\n\t"	// pix 0 : [a01|a00] * (16-y)
			"umull		v5.8H , v1.8B, v13.8B		\n\t"	// pix 0 : [a11|a10] * y
			"mov		D1, v4.D[1]					\n\t"	// pix 0: a01
			"mul		v0.4H, v1.4H, v3.4H			\n\t"	// pix 0 : a01 * x
			"mov		D1, v5.D[1]					\n\t"	// pix 0: a11
			"mla		v0.4H, v1.4H, v3.4H			\n\t"	// pix 0 : + a11 * x
			"sub		v3.4H, v12.4H, v3.4H		\n\t"	// pix 0 : 16 - x
			"mla		v0.4H, v4.4H, v3.4H			\n\t"	// pix 0 : + a00 * (16-x)
			"mla		v0.4H, v5.4H, v3.4H			\n\t"	// pix 0 : + a10 * (16-x)
			
			"shrn		v1.8B, v0.8H, #8			\n\t"
			"st1		{v1.S}[0], [%[colors]], #4	\n\t"

			"subs		%[count], %[count], #1		\n\t"
			"bgt		tailloop					\n\t"

			"tailloop_end:							\n\t"
			
			: [colors] "+r" (colors)
			: [image0] "r" (image0), [image1] "r" (image1), [fx] "r" (fx), [subY] "r" (subY), [dx] "r" (dx), [count] "r" (count)
			: "cc", "memory", "x4", "x5", "x6", "x7", "v0", "v1", "v2", "v3", "v4", "v5", "v6", "v7", "v8", "v9", "v10", "v11", "v12", "v13", "v14", "v15", "v16", "v17", "v18", "v19", "v20", "v21", "v22"
		);
}
