
/*
 * Copyright 2008 The Android Open Source Project
 *
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */
#include "SkColor.h"
#include "SkColorPriv.h"


void GetAlpha8888_opt(const SkPMColor* SK_RESTRICT src, 
                             int width, uint8_t* SK_RESTRICT alpha) {
	 if (width <= 0) {
		 return ;
	 }

#if defined(__ARM_HAVE_NEON_COMMON) && defined(SK_CPU_LENDIAN)	
	 asm volatile(
			 "pld		 [%[src], #0]				 \n\t"
			 "mov		 r6, %[src] 				 \n\t"
			 "mov		 r7, %[alpha]				 \n\t"
			 "mov		 r4, %[width]				 \n\t"
			 "subs		 r4, r4, #8 				 \n\t"
			 "blt		 2f 						 \n\t"
	 
			 "1:									 \n\t"
			 "vld4.8	 {d0, d1, d2, d3}, [r6]!	 \n\t"
			 "subs		 r4, r4, #8 				 \n\t"
			 "pld		 [r6, #64]					 \n\t"
			 "vst1.8	 {d3}, [r7]!				 \n\t"
			 "bge		 1b 						 \n\t"
	 
			 "2:									 \n\t"
			 "add		 r4, r4, #8 				 \n\t"
			 "cmp		 r4, #0 					 \n\t"
			 "ble		 4f 						 \n\t"
	 
			 "3:									 \n\t"
			 "ldr		 r5, [r6], #4				 \n\t"
			 "subs		 r4, r4, #1 				 \n\t"
			 "lsr		 r5, r5, #24				 \n\t"
			 "strb		 r5, [r7], #1				 \n\t"
			 "bgt		 3b 						 \n\t"
	 
			 "4:									 \n\t"
			 :
			 :[width] "r" (width), [src] "r" (src), [alpha] "r" (alpha)
			 :"memory", "r4", "r5", "r6", "r7", "d0", "d1", "d2", "d3"
			 );
#else
	 asm volatile(
			 "pld		 [%[src], #0]				 \n\t"
			 "subs		 %[width], %[width], #1 	 \n\t"
			 "blt		 2f 						 \n\t"
			 
			 "1:									 \n\t"
			 "ldr		 r5, [%[src]], #4			 \n\t"
			 "subs		 %[width], %[width], #1 	 \n\t"
			 "lsr		 r5, r5, #24				 \n\t"
			 "strb		 r5, [%[alpha]], #1 		 \n\t"
			 "bgt		 1b 						 \n\t"
			 
			 "2:									 \n\t"
			 :
			 :[width] "r" (width), [src] "r" (src), [alpha] "r" (alpha)
			 :"memory", "r5"
			 );
#endif
 }



void blend32_16_row_opt(SkPMColor src_expand, uint32_t scale, 
								uint16_t device[], int width) {
	 if (width <= 0) {
		 return ;
	 }
 
#if defined(__ARM_HAVE_NEON_COMMON)
	 asm volatile (
			 "cmp		 %[width], #0					 \n\t"	 //width> 0?
			 "ble		 5f 							 \n\t"
 
			 "pld		 [%[device], #0]				 \n\t"
			 "movw		 r9, #0xf81f					 \n\t"
			 "movt		 r9, #0x07e0					 \n\t"	 // r9 = 0x07e0f81f
 
			 "vdup.32	 q7, r9 						 \n\t"	 //q7=mask_vec
			 "vdup.32	 q6, %[src_expand]				 \n\t"
			 "vdup.32	 q8, %[scale]					 \n\t"
 
			 "movs		 r4, %[width], lsr #4			 \n\t"
			 "beq		 2f 							 \n\t"
 
 
			 "1:										 \n\t"
			 "vld1.u16	 {d0,d1,d2,d3}, [%[device]] 	 \n\t"
			 "pld		 [%[device], #256]				 \n\t"
			 "subs		 r4, r4, #1 					 \n\t"
 
			 "vand.32	 q2, q0, q7 					 \n\t"
			 "vand.32	 q4, q1, q7 					 \n\t"
			 "vrev32.16  q3, q0 						 \n\t"
			 "vrev32.16  q5, q1 						 \n\t"
			 "vand.32	 q3, q3, q7 					 \n\t"
			 "vand.32	 q5, q5, q7 					 \n\t"
 
			 "vmul.i32	 q2, q2, q8 					 \n\t"
			 "vmul.i32	 q4, q4, q8 					 \n\t"
			 "vmul.i32	 q3, q3, q8 					 \n\t"
			 "vmul.i32	 q5, q5, q8 					 \n\t"
			 "vadd.u32	 q2, q2, q6 					 \n\t"
			 "vadd.u32	 q4, q4, q6 					 \n\t"
			 "vadd.u32	 q3, q3, q6 					 \n\t"
			 "vadd.u32	 q5, q5, q6 					 \n\t"
			 "vshr.u32	 q2, q2, #5 					 \n\t"
			 "vshr.u32	 q4, q4, #5 					 \n\t"
			 "vshr.u32	 q3, q3, #5 					 \n\t"
			 "vshr.u32	 q5, q5, #5 					 \n\t"
 
			 "vand.u32	 q2, q2, q7 					 \n\t"
			 "vand.u32	 q4, q4, q7 					 \n\t"
			 "vand.u32	 q3, q3, q7 					 \n\t"
			 "vand.u32	 q5, q5, q7 					 \n\t"
			 "vrev32.16  q3, q3 						 \n\t"
			 "vrev32.16  q5, q5 						 \n\t"
			 "vorr.u16	 q0, q2, q3 					 \n\t"
			 "vorr.u16	 q1, q4, q5 					 \n\t"
 
			 "vst1.u16	 {d0,d1,d2,d3}, [%[device]]!	 \n\t"
			 "bne		   1b							 \n\t"
 
			 "2:										 \n\t"
			 "ands		 r4, %[width], #0xF 			 \n\t"
			 "beq		 5f 							 \n\t"
			 "cmp		 r4, #8 						 \n\t"
			 "blt		 3f 							 \n\t"
 
			 "vld1.u16	 {d0,d1}, [%[device]]			 \n\t"
			 "vand.32	 q2, q0, q7 					 \n\t"
			 "vrev32.16  q3, q0 						 \n\t"
			 "vand.32	 q3, q3, q7 					 \n\t"
 
			 "vmul.i32	 q2, q2, q8 					 \n\t"
			 "vmul.i32	 q3, q3, q8 					 \n\t"
			 "vadd.u32	 q2, q2, q6 					 \n\t"
			 "vadd.u32	 q3, q3, q6 					 \n\t"
			 "vshr.u32	 q2, q2, #5 					 \n\t"
			 "vshr.u32	 q3, q3, #5 					 \n\t"
 
			 "vand.u32	 q2, q2, q7 					 \n\t"
			 "vand.u32	 q3, q3, q7 					 \n\t"
			 "vrev32.16  q3, q3 						 \n\t"
			 "vorr.u16	 q0, q2, q3 					 \n\t"
 
			 "vst1.u16	 {d0, d1}, [%[device]]! 		 \n\t"
 
 
			 "3:										 \n\t"
			 "ands		 r4, %[width], #0x7 			 \n\t"
			 "beq		 5f 							 \n\t"
 
			 "4:										 \n\t"
			 "ldrh		 r10, [%[device], #0]			 \n\t"	 //r10 = current = *device
			 "subs		 r4, r4, #1 					 \n\t"	 //width--;
			 "pld		 [%[device], #8]				 \n\t"
			 "orr		 r8, r10, r10, lsl #16			 \n\t"	 //(current << 16 | current)
			 "and		 r10, r8, r9					 \n\t"	 //r10 = &mask
			 "mla		 r8, r10, %[scale], %[src_expand]	 \n\t"	//r8 = src_expand + dst_expand
			 "and		 r8, r9, r8, lsr #5 			 \n\t"	 //r8 & mask
			 "orr		 r10, r8, r8, lsr #16			 \n\t"	 //must trunk the high 16bits, if not, generate error result
			 "strh		 r10, [%[device]], #2			 \n\t"
			 "bne		 4b 							 \n\t"
 
			 "5:										 \n\t"
			 :[device] "+r" (device)
			 :[width] "r" (width), [src_expand] "r" (src_expand), [scale] "r" (scale)
			 :"memory", "r4", "r8", "r9", "r10", "d0", "d1", "d2", "d3", "d4", "d5", "d6", "d7", "d8", "d9", "d10", "d11", "d12", "d13", "d14", "d15", "d16", "d17"
			 );
#else
	 asm volatile(
			 "pld		 [%[device]]				 \n\t"
			 "mov		 r9, #0x7E0 				 \n\t"
			 "mov		 r10, #0xFF 				 \n\t"
			 "orr		 r10, r10, r10, lsl #8		 \n\t"	 //r10 = 0xFFFF
			 "bic		 r10, #0x7E0				 \n\t"
			 "orr		 r9, r10, r9, lsl #16		 \n\t"	 //r9 = mask
 
			 "1:									 \n\t"
			 "ldrh		 r10, [%[device], #0]		 \n\t"	 //r10 = current = *device
			 "subs		 %[width], %[width], #1 	 \n\t"	 //width--;
			 "pld		 [%[device], #8]			 \n\t"
			 "orr		 r8, r10, r10, lsl #16		 \n\t"	 //(current << 16 | current)
			 "and		 r10, r8, r9				 \n\t"	 //r10 = &mask
			 "mla		 r8, r10, %[scale], %[src_expand]	 \n\t"	 //r8 = src_expand + dst_expand
			 "and		 r8, r9, r8, lsr #5 		 \n\t"	 //r8 & mask
			 "orr		 r10, r8, r8, lsr #16		 \n\t"	 //must trunk the high 16bits, if not, generate error result
			 "strh		 r10, [%[device]], #2		 \n\t"
			 "bne		 1b 						 \n\t"
 
			 :[device] "+r" (device)
			 :[width] "r" (width), [scale] "r" (scale), [src_expand] "r" (src_expand)
			 :"memory", "r8", "r9", "r10"
			 );
#endif
}

#if defined(__ARM_HAVE_NEON_COMMON)	
void blend16_32_row_neon_opt(SkPMColor* SK_RESTRICT dst,
                                		   const uint16_t* SK_RESTRICT src,
                                 		   int count) {
    if (count <= 0) {
		return ;
    }

	asm volatile (
			"cmp		%[count], #8				\n\t"
		  	"vmov.u16   d6, #0xFF00					\n\t"   // Load alpha value into q8 for later use.
		  	"blt		2f							\n\t"   // if count < 0, branch to label 2
		  	"vmov.u8	d5, #0xFF 					\n\t" 			   
		  	"sub		%[count], %[count], #8		\n\t"   // count -= 8
			"pld		[%[src]]					\n\t"

		  	"1:										\n\t"   // 8 loop
		  	// Handle 8 pixels in one loop.
		  	"vld1.u16   {q0}, [%[src]]!				\n\t"   // load eight src 565 pixels
		  	"pld		[%[src], #64] 				\n\t"
		  	"subs 	  	%[count], %[count], #8		\n\t"   // count -= 8, set flag
		  	"vshrn.u16  d2, q0, #8					\n\t"	//R		  	
		  	"vshrn.u16  d3, q0, #3					\n\t" 	//G
		  	"vsli.u16   q0, q0, #5					\n\t" 	//B
		  	"vsri.u8	d2, d2, #5					\n\t"
		  	"vsri.u8	d3, d3, #6					\n\t"
		  	"vshrn.u16  d4, q0, #2					\n\t"
		  	"vst4.u8	{d2, d3, d4, d5}, [%[dst]]!	\n\t"

		  	"bge		1b							\n\t"   // loop if count >= 0
		  	"add		%[count], %[count], #8		\n\t"

		  	"2:										\n\t"   // exit of 8 loop
		  	"cmp		%[count], #4				\n\t"   //
		  	"blt		3f							\n\t"   // if count < 0, branch to label 3
		  	"sub		%[count], %[count], #4		\n\t"
		  	// Handle 4 pixels at once		  	
		  	"vld1.u16   {d0}, [%[src]]!				\n\t"   // load 4 src 565 pixels

		  	"vshl.u16   d2, d0, #5					\n\t"   // put green in the 6 high bits of d2
		  	"vshl.u16   d1, d0, #11					\n\t"   // put blue in the 5 high bits of d1
		  	"vmov.u16   d3, d6						\n\t"   // copy alpha from d6
		  	"vsri.u16   d3, d1, #8					\n\t"   // put blue below alpha in d3
		  	"vsri.u16   d3, d1, #13					\n\t"   // put 3 MSB blue below blue in d3
		  	"vsri.u16   d2, d2, #6					\n\t"   // put 2 MSB green below green in d2
		  	"vsri.u16   d2, d0, #8					\n\t"   // put red below green in d2
		  	"vsri.u16   d2, d0, #13					\n\t"   // put 3 MSB red below red in d2
		  	"vzip.16	d2, d3						\n\t"   // interleave d2 and d3
		  	"vst1.16	{d2, d3}, [%[dst]]!			\n\t"   // store d2 and d3 to dst

		  	"3:										\n\t"   // end
		  	: [src] "+r" (src), [dst] "+r" (dst), [count] "+r" (count)
		  	:
		  	: "cc", "memory","d0","d1","d2","d3","d4","d5","d6"
	);	
	
	// process the rest of 3 pixels
	for (int i = (count & 3); i > 0; --i) {
		*dst = SkPixel16ToPixel32(*src);
		src += 1;
		dst += 1;
	}	
}
#endif

