#include "SkBitmap.h"
#include "SkSize.h"
#include "SkCanvas.h"
#include "SkPaint.h"
#include "SkBitmap.h"
#include "SkStream.h"
#include "SkString.h"

#include "SkPngChunkReader.h"
#include "SkAndroidCodec.h"
#include "SkBRDAllocator.h"
#include "SkPixelRef.h"
#include "SkStream.h"
#include <memory>
#include "SkImageEncoder.h"
#include "SkMatrix.h"


extern "C" {
    #include <stdio.h>
    #include <sys/time.h>
    #include <unistd.h>
    #include <stdlib.h>
}

// Set default values for the options parameters.
int sampleSize = 1; // the paramter can resize originnal image. scale is 1: smapleSzie^2
bool onlyDecodeSize = false;
SkColorType prefColorType = kN32_SkColorType;
bool isMutable = false;
bool requireUnpremultiplied = false;
sk_sp<SkColorSpace> prefColorSpace = nullptr;

void imageDecoder_decode(char *file, SkImageInfo &decodeInfo, SkBitmap &decodingBitmap) {
    SkBitmap bm8888;
	SkString filename(file);
	std::unique_ptr<SkFILEStream> stream (new SkFILEStream(filename.c_str()));
    
    // Create the codec.
    //SkPngChunkReader peeker;
    std::unique_ptr<SkAndroidCodec> codec(SkAndroidCodec::NewFromStream(stream.release()));
    if (!codec.get()) {
        printf("SkAndroidCodec::NewFromStream returned null\n");
        return ;
    }
    printf("SkAndroidCodec::NewFromStream success!\n");

    // Determine the output size and return if the client only wants the size.
    SkISize size = codec->getSampledDimensions(sampleSize);
    // Set the decode colorType
    SkColorType decodeColorType = codec->computeOutputColorType(prefColorType);
    sk_sp<SkColorSpace> decodeColorSpace = codec->computeOutputColorSpace(
            decodeColorType, prefColorSpace);

    // Set the alpha type for the decode.
    SkAlphaType alphaType = codec->computeOutputAlphaType(requireUnpremultiplied);
    decodeInfo = SkImageInfo::Make(size.width(), size.height(), decodeColorType,
            alphaType, decodeColorSpace);
    printf("original image width = %d height = %d\n", size.width(), size.height());

    if (!decodingBitmap.setInfo(decodeInfo) ||
            !decodingBitmap.tryAllocPixels(nullptr)) {
        // SkAndroidCodec should recommend a valid SkImageInfo, so setInfo()
        // should only only fail if the calculated value for rowBytes is too
        // large.
        // tryAllocPixels() can fail due to OOM on the Java heap, OOM on the
        // native heap, or the recycled javaBitmap being too small to reuse.
        printf("decodingBitmap.tryAllocPixels failed.\n");
        return ;
    }

    // Use SkAndroidCodec to perform the decode.
    SkAndroidCodec::AndroidOptions codecOptions;
    codecOptions.fZeroInitialized = SkCodec::kNo_ZeroInitialized;
    codecOptions.fSampleSize = sampleSize;
    SkCodec::Result result = codec->getAndroidPixels(decodeInfo, decodingBitmap.getPixels(),
            decodingBitmap.rowBytes(), &codecOptions);
    switch (result) {
        case SkCodec::kInvalidParameters:
            printf("codec->getAndroidPixels() kInvalidParameters\n");
            break;
        case SkCodec::kSuccess:
            printf("codec->getAndroidPixels() kSuccess\n");
            break;
        case SkCodec::kIncompleteInput:
            printf("codec->getAndroidPixels() incompleteInput.\n");
            break;
        default:
            printf("codec->getAndroidPixels() failed.\n");
    }

}


bool dumpToPng(const SkBitmap &bm, char *outFileName) {
    bool success = false;
    SkFILEWStream file(outFileName);
    if (outFileName != nullptr)
        success = SkEncodeImage(&file,bm,SkEncodedImageFormat::kPNG,100);
    return success;
}



/*      rotate 270CW original matrix:
        [ 0    -1     h]   [X]   [X']
        [ 1     0     0] * [Y] = [Y']
        [ 0     0     1]   [1]   [1 ]

        h must be the src original height without clip
*/
void setRotate90CW(SkCanvas &canvas, SkMatrix &matrix, SkPaint &paint, int trans) {
    paint.setFilterQuality(kMedium_SkFilterQuality);
    matrix.postRotate(90, 0, 0);
    matrix.postTranslate(trans, 0);
    matrix.dump();
    canvas.setMatrix(matrix);
}

/*      rotate 90CW original matrix:
        [ 0    1     0]   [X]   [X']
        [-1    0     w] * [Y] = [Y']
        [ 0    0     1]   [1]   [1 ]
        
        w must be the src original width without clip
*/
void setRotate270CW(SkCanvas &canvas, SkMatrix &matrix, SkPaint &paint, int trans) {
    paint.setFilterQuality(kMedium_SkFilterQuality);
    matrix.postRotate(270, 0, 0);
    matrix.postTranslate(0, trans);
    matrix.dump();
    canvas.setMatrix(matrix);
}

int testRotationOptimization_withSrcClip(int argc, char **argv) {
    printf("decode image %s\n",argv[1]);
    SkImageInfo decodeInfo;
    SkBitmap decodingBitmap;
    imageDecoder_decode(argv[1], decodeInfo, decodingBitmap);

    /*now draw 2d flow*/
    SkBitmap dstbm;
    dstbm.setInfo(SkImageInfo::Make(decodeInfo.height(), decodeInfo.width(), decodeInfo.colorType(),
            decodeInfo.alphaType()));
    printf("destination bitmap width = %d height = %d\n", dstbm.width(), dstbm.height());
    if (!dstbm.tryAllocPixels(nullptr)) {
        // This should only fail on OOM.  The recyclingAllocator should have
        // enough memory since we check this before decoding using the
        // scaleCheckingAllocator.
        return printf("allocation failed for scaled bitmap");
    }
    SkCanvas canvas(dstbm);
    SkMatrix matrix;
    SkPaint paint;
    if (atoi(argv[4]) == 90) {
        printf("set rotation 90cw\n");
        setRotate90CW(canvas, matrix, paint, decodeInfo.height());
    } else if (atoi(argv[4]) == 270) {
        printf("set rotation 270cw\n");
        setRotate270CW(canvas, matrix, paint, decodeInfo.width());
    } else {
        printf("set rotation error\n");
    }

    SkRect srcRect;
    SkRect dstRect;
    if (atoi(argv[5]) == 1) {
        srcRect = SkRect::MakeLTRB(100, 0, decodeInfo.width()-500, decodeInfo.height());
        dstRect = SkRect::MakeLTRB(0, 0, decodeInfo.width()-600, decodeInfo.height());
    } else if (atoi(argv[5]) == 2) {
        srcRect = SkRect::MakeLTRB(0, 100, decodeInfo.width(), decodeInfo.height()-500);
        dstRect = SkRect::MakeLTRB(0, 0, decodeInfo.width(), decodeInfo.height()-600);
    } else {
        printf("set clip error\n");
    }
        
    printf("set clip from src.l(%f) src.t(%f) src.r(%f) src.t(%f)\n\
    to dst.l(%f) dst.t(%f) dst.r(%f) dst.t(%f)\n",
        srcRect.fLeft, srcRect.fTop, srcRect.fRight, srcRect.fBottom,
        dstRect.fLeft, dstRect.fTop, dstRect.fRight, dstRect.fBottom);
    
    /*measure interval*/
    struct  timeval start;
    struct  timeval end;
   
    unsigned  long diff;
    gettimeofday(&start,NULL);
    canvas.drawBitmapRect(decodingBitmap, srcRect, dstRect, &paint, SkCanvas::kStrict_SrcRectConstraint);
    gettimeofday(&end,NULL);
    diff = 1000000 * (end.tv_sec-start.tv_sec)+ end.tv_usec-start.tv_usec;
    printf("drawBitmapRect time consumed %ld us\n",diff);
#if 1

    printf("dump pixel raw data in canvas...!\n");
    SkFILEWStream writeStream(argv[3]);
    if (!writeStream.isValid())
        printf("invalid path to write: %s\n",argv[3]);
    writeStream.write(dstbm.getPixels(), dstbm.getSize());
    writeStream.flush();

    dumpToPng(dstbm, argv[2]);
    printf("dump pixel raw data done!\n");
#endif
    return 0;
}


int testRotationOptimization(int argc, char **argv) {
    printf("decode image %s\n",argv[1]);
    SkImageInfo decodeInfo;
    SkBitmap decodingBitmap;
    imageDecoder_decode(argv[1], decodeInfo, decodingBitmap);

    /*now draw 2d flow*/
    SkBitmap dstbm;
    dstbm.setInfo(SkImageInfo::Make(decodeInfo.height(), decodeInfo.width(),
            decodeInfo.colorType(),
            decodeInfo.alphaType()));
    printf("destination bitmap width = %d height = %d\n", dstbm.width(), dstbm.height());
    if (!dstbm.tryAllocPixels(nullptr)) {
        // This should only fail on OOM.  The recyclingAllocator should have
        // enough memory since we check this before decoding using the
        // scaleCheckingAllocator.
        return printf("allocation failed for scaled bitmap");
    }

    SkCanvas canvas(dstbm);
    SkMatrix matrix;
    SkPaint paint;
    if (atoi(argv[4]) == 90) {
        printf("set android coordinate rotation 90cw\n");
        setRotate90CW(canvas, matrix, paint, decodeInfo.height());
    } else if (atoi(argv[4]) == 270) {
        printf("set android coordinate rotation 270cw\n");
        setRotate270CW(canvas, matrix, paint, decodeInfo.width());
    }
    /*measure interval*/
    struct  timeval start;
    struct  timeval end;
   
    unsigned  long diff;
    gettimeofday(&start,NULL);
    canvas.drawBitmap(decodingBitmap, 0, 0, &paint);
    gettimeofday(&end,NULL);
    diff = 1000000 * (end.tv_sec-start.tv_sec)+ end.tv_usec-start.tv_usec;
    printf("drawBitmap no clip consumed %ld us\n",diff);
#if 1

    printf("dump pixel raw data in canvas...!\n");
    SkFILEWStream writeStream(argv[3]);
    if (!writeStream.isValid())
        printf("invalid path to write: %s\n",argv[4]);
    writeStream.write(dstbm.getPixels(), dstbm.getSize());
    writeStream.flush();
#endif

    dumpToPng(dstbm, argv[2]);
    printf("dump png done!\n");
    return 0;
}

/*
 *argv[1]: original image
 *argv[2]: dumped rotation image
 *argv[3]: dumped raw pixel data
 *argv[4]: 90 -> rotate 90CW 270-> rotate 270CW
 *argv[5]: 0:clip 1:no clip
 */
int main(int argc, char **argv) {
    if (argc != 6)
        printf("%s paramters: \n\
            *argv[1]: original image \n\
            *argv[2]: dumped rotation image \n\
            *argv[3]: dumped raw pixel data \n\
            *argv[4]: 90 -> rotate 90CW 270-> rotate 270CW \n\
            *argv[5]: 0: no clip 1: clipx 2:clipy", argv[1]);
    if (atoi(argv[5]) == 0)
        testRotationOptimization(argc, argv);
    else if (atoi(argv[5]) == 1 || atoi(argv[5]) == 2)
        testRotationOptimization_withSrcClip(argc, argv);
    else
        printf("set clip error\n");
    return 0;
}

