/*
 * ThreadSafeQueue.h
 *
 *  Created on: 2017��9��4��
 *      Author: MTK10393
 */

#ifndef SRC_THREADSAFEQUEUE_H_
#define SRC_THREADSAFEQUEUE_H_
#include <memory>
#include <queue>
#include <mutex>
#include <condition_variable>

template<typename T>
class ThreadSafeQueue {
private:
	std::mutex mut;
	std::queue<T> data_queue;
	std::condition_variable data_cond;
public:
	explicit ThreadSafeQueue() {}
	ThreadSafeQueue(const ThreadSafeQueue &other) {
		std::lock_guard<std::mutex> lk(other.mut);
		data_queue = other.data_queue;
	}

	ThreadSafeQueue &operator=(const ThreadSafeQueue &)=delete;

	void push(T new_value) {
		std::lock_guard<std::mutex> lk(mut);
		data_queue.push(new_value);
		data_cond.notify_one();
	}

	void wait_and_pop(T &value) {
		std::unique_lock<std::mutex> lk(mut);
		data_cond.wait(lk, [this]{return !data_queue.empty();});
		value = data_queue.front();
		data_queue.pop();
	}

	std::shared_ptr<T> wait_and_pop() {
		std::unique_lock<std::mutex> lk(mut);
		data_cond.wait(lk, [this]{return !data_queue.empty();});
		std::shared_ptr<T> res(std::make_shared(data_queue.front()));
		data_queue.pop();
		return res;
	}

	bool try_pop(T value) {
		std::lock_guard<std::mutex> lk(mut);
		if (data_queue.empty())
			return false;
		value = data_queue.front();
		data_queue.pop();
		return true;
	}

	std::shared_ptr<T> try_pop() {
		std::lock_guard<std::mutex> lk(mut);
		if (data_queue.empty())
			return false;
		std::shared_ptr<T> res(std::make_shared(data_queue.front()));
		data_queue.pop();
		return res;
	}

	bool empty() const {
		std::lock_guard<std::mutex> lk(mut);
		return data_queue.empty();
	}

	bool empty() {
		std::lock_guard<std::mutex> lk(mut);
		return data_queue.empty();
	}
};




#endif /* SRC_THREADSAFEQUEUE_H_ */
