#include "SkBitmap.h"
#include "SkSize.h"
#include "SkCanvas.h"
#include "SkPaint.h"
#include "SkBitmap.h"
#include "SkStream.h"
#include "SkString.h"

#include "SkPngChunkReader.h"
#include "SkAndroidCodec.h"
#include "SkBRDAllocator.h"
#include "SkPixelRef.h"
#include "SkStream.h"
#include <memory>
#include "SkImageEncoder.h"
#include "SkMatrix.h"


extern "C" {
    #include <stdio.h>
    #include <sys/time.h>
    #include <unistd.h>
    #include <stdlib.h>
}

// Set default values for the options parameters.
int sampleSize = 1; // the paramter can resize originnal image. scale is 1: smapleSzie^2
bool onlyDecodeSize = false;
SkColorType prefColorType = kN32_SkColorType;
bool isMutable = false;
bool requireUnpremultiplied = true;


void decodeFile(char *file, SkImageInfo &decodeInfo, SkBitmap &decodingBitmap) {
    SkBitmap bm8888;
	SkString filename(file);
	std::unique_ptr<SkFILEStream> stream (new SkFILEStream(filename.c_str()));

    // Create the codec.
    //SkPngChunkReader peeker;
    std::unique_ptr<SkAndroidCodec> codec(SkAndroidCodec::NewFromStream(stream.release()));
    if (!codec.get()) {
        printf("SkAndroidCodec::NewFromStream returned null\n");
        return ;
    }
    printf("SkAndroidCodec::NewFromStream success!\n");

    // Determine the output size and return if the client only wants the size.
    SkISize size = codec->getSampledDimensions(sampleSize);

    // Set the decode colorType.  This is necessary because we can't always support
    // the requested colorType.
    SkColorType decodeColorType = codec->computeOutputColorType(prefColorType);
    // Set the alpha type for the decode.
    SkAlphaType alphaType = codec->computeOutputAlphaType(requireUnpremultiplied);
    decodeInfo = SkImageInfo::Make(size.width(), size.height(), decodeColorType,
            alphaType);
    printf("original image width = %d height = %d\n", size.width(), size.height());

    // Construct a color table for the decode if necessary
    SkAutoTUnref<SkColorTable> colorTable(nullptr);
    SkPMColor* colorPtr = nullptr;
    int* colorCount = nullptr;

    if (!decodingBitmap.setInfo(decodeInfo) ||
            !decodingBitmap.tryAllocPixels(nullptr, colorTable)) {
        // SkAndroidCodec should recommend a valid SkImageInfo, so setInfo()
        // should only only fail if the calculated value for rowBytes is too
        // large.
        // tryAllocPixels() can fail due to OOM on the Java heap, OOM on the
        // native heap, or the recycled javaBitmap being too small to reuse.
        printf("decodingBitmap.tryAllocPixels failed.\n");
        return ;
    }

    // Use SkAndroidCodec to perform the decode.
    SkAndroidCodec::AndroidOptions codecOptions;
    codecOptions.fZeroInitialized = SkCodec::kNo_ZeroInitialized;
    codecOptions.fColorPtr = colorPtr;
    codecOptions.fColorCount = colorCount;
    codecOptions.fSampleSize = sampleSize;
    SkCodec::Result result = codec->getAndroidPixels(decodeInfo, decodingBitmap.getPixels(),
            decodingBitmap.rowBytes(), &codecOptions);
    switch (result) {
        case SkCodec::kInvalidParameters:
            printf("codec->getAndroidPixels() kInvalidParameters\n");
            break;
        case SkCodec::kSuccess:
            printf("codec->getAndroidPixels() kSuccess\n");
            break;
        case SkCodec::kIncompleteInput:
            printf("codec->getAndroidPixels() incompleteInput.\n");
            break;
        default:
            printf("codec->getAndroidPixels() failed.\n");
    }

}


bool dumpToPng(const SkBitmap &bm, char *outFileName) {
    bool success = false;
    if (outFileName != nullptr)
        success = SkImageEncoder::EncodeFile(outFileName,bm,SkImageEncoder::kPNG_Type,100);
    return success;
}

