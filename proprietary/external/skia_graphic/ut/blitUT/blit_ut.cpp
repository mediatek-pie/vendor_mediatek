#include "SkBitmap.h"
#include "SkSize.h"
#include "SkCanvas.h"
#include "SkPaint.h"
#include "SkBitmap.h"
#include "SkStream.h"
#include "SkString.h"

#include "SkPngChunkReader.h"
#include "SkAndroidCodec.h"
#include "SkBRDAllocator.h"
#include "SkPixelRef.h"
#include "SkStream.h"
#include <memory>
#include "SkImageEncoder.h"
#include "SkMatrix.h"
#include "SkXfermode.h"


extern "C" {
    #include <stdio.h>
    #include <sys/time.h>
    #include <unistd.h>
    #include <stdlib.h>
}

#include "blitRow32Case.h"
#include "asyncTask.h"
#include "ThreadPool.h"

extern void decodeFile(char *file, SkImageInfo &decodeInfo, SkBitmap &decodingBitmap);
extern bool dumpToPng(const SkBitmap &bm, char *outFileName);
void dumpToPixels(char *file, SkBitmap &dstbm) {
    printf("dump pixel raw data in canvas...!\n");
    SkFILEWStream writeStream(file);
    if (!writeStream.isValid())
        printf("invalid path to write: %s\n",file);
    writeStream.write(dstbm.getPixels(), dstbm.getSize());
    writeStream.flush();
}

void changeAlpha(SkBitmap &bitmap) {
    unsigned int *pix;
    unsigned char alpha;
    for (int j = 0; j < bitmap.height(); j++) {
        for (int i = 0; i < bitmap.width(); i++) {
            pix = bitmap.getAddr32(i, j);
            *pix = *pix & ((1 << SK_BGRA_A32_SHIFT) - 1);
            alpha = i % 0xff;
            *pix = *pix | (alpha << SK_BGRA_A32_SHIFT);
        }
    }
}

void drawAndDump(SkImageInfo &decodeInfo, SkBitmap &decodingBitmap, char *file) {
    /*now draw 2d flow*/
    SkBitmap dstbm;
    dstbm.setInfo(SkImageInfo::Make(decodeInfo.width(), decodeInfo.height(),
            decodeInfo.colorType(),
            decodeInfo.alphaType()));
    printf("destination bitmap width = %d height = %d\n", dstbm.width(), dstbm.height());
    if (!dstbm.tryAllocPixels(nullptr, nullptr)) {
        // This should only fail on OOM.  The recyclingAllocator should have
        // enough memory since we check this before decoding using the
        // scaleCheckingAllocator.
        printf("allocation failed for scaled bitmap");
        return;
    }

    SkCanvas canvas(dstbm);
    SkPaint paint;

    paint.setColor(0x8000);
    //paint.setAntiAlias(true);
    //paint.setXfermodeMode(SkXfermode::kDst_Mode);
    
    /*measure interval*/
    struct  timeval start;
    struct  timeval end;
   
    unsigned  long diff;
    gettimeofday(&start,NULL);
    canvas.drawBitmap(decodingBitmap, 0, 0, &paint);
    gettimeofday(&end,NULL);
    diff = 1000000 * (end.tv_sec-start.tv_sec)+ end.tv_usec-start.tv_usec;
    printf("drawBitmap no clip consumed %ld us\n",diff);

    dumpToPng(dstbm, file);
    printf("dump png done!\n");

}

void drawXfermode(SkBitmap &bm, SkImageInfo &decodeInfo, SkBitmap &decodingBitmap) {
    bm.setInfo(SkImageInfo::Make(decodeInfo.width(), decodeInfo.height(),
            decodeInfo.colorType(),
            decodeInfo.alphaType()));
    if (!bm.tryAllocPixels(nullptr, nullptr)) {
        // This should only fail on OOM.  The recyclingAllocator should have
        // enough memory since we check this before decoding using the
        // scaleCheckingAllocator.
        printf("allocation failed for scaled bitmap");
        return;
    }

    SkPaint paint;
    SkCanvas canvas(bm);
    canvas.drawBitmap(decodingBitmap, 0, 0, &paint);
    //paint.setAlpha(0x80);
    //canvas.drawColor(0xff008000, SkXfermode::Mode::kDarken_Mode);
    canvas.drawColor(0x80008000);
}

void func() {printf("hello\n");}

#include "test_thread.h"

int main(int argc, char **argv) {
    /*1 decode*/
    #if 0
    printf("decode image %s\n",argv[1]);
    SkImageInfo decodeInfo;
    SkBitmap decodingBitmap;
    decodeFile(argv[1], decodeInfo, decodingBitmap);
    changeAlpha(decodingBitmap);
    //dumpToPixels(argv[2], decodingBitmap);
    //drawAndDump(decodeInfo, decodingBitmap, argv[3]);
    SkBitmap dstbm;
    //blitRectTest(decodeInfo, decodingBitmap, dstbm);
    //MtkAsyncTask asyncTask;
    //asyncTask.doAsyncTask(decodeInfo, decodingBitmap, dstbm);
    drawXfermode(dstbm, decodeInfo, decodingBitmap);
    dumpToPng(dstbm, argv[2]);
    #endif
    my_task task;
    for (int i = 0; i < 5; i++)
        task.callAsync(i, i);
    
    printf("done\n");
    return 0;
}
