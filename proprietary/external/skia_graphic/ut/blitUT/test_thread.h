
#include <thread>
#include <iostream>
#include <vector>

class my_task {
private:
	std::vector<std::thread> threads;
public:
	my_task() { std::cout << __FUNCTION__ << "\n"; }
	//my_task(const my_task& other) { std::cout << __FUNCTION__ << "\n"; }
	my_task(const my_task& other) = delete;
	my_task(const my_task&& other) { std::cout << __FUNCTION__ << "\n"; }
	~my_task() { 
		std::cout << __FUNCTION__ << " thread size: " << threads.size() << "\n";
		for (int i = 0; i < threads.size(); i++) {
			threads[i].join();
		}
	}

	void call(int a, int b) {
		std::cout << __FUNCTION__;
		std::cout << a << " " << b << "\n";
	}

	void callAsync(int a, int b) {
		std::cout << __FUNCTION__ << "\n";
		std::thread t(&my_task::call, this, a, b);
		threads.push_back(std::move(t));
	}
};