#include "SkPictureRecorder.h"
#include "SkCanvas.h"
#include "SkBitmap.h"
#include "SkSize.h"
#include "SkCanvas.h"
#include "SkPaint.h"
#include "SkBitmap.h"
#include "SkStream.h"
#include "SkString.h"

#include "SkPngChunkReader.h"
#include "SkAndroidCodec.h"
#include "SkBRDAllocator.h"
#include "SkPixelRef.h"
#include "SkStream.h"
#include <memory>
#include "SkImageEncoder.h"
#include "SkMatrix.h"

extern "C" {
    #include <stdio.h>
    #include <sys/time.h>
    #include <unistd.h>
    #include <stdlib.h>
}


static SkPicture* make_picture() {
    SkPictureRecorder rec;
    SkCanvas* canvas = rec.beginRecording(100, 100);

    SkPaint paint;
    paint.setAntiAlias(true);
    SkPath path;

    paint.setColor(0x800000FF);
    canvas->drawRect(SkRect::MakeWH(100, 100), paint);

    paint.setColor(0x80FF0000);
    path.moveTo(0, 0); path.lineTo(100, 0); path.lineTo(100, 100);
    canvas->drawPath(path, paint);
    
    paint.setColor(0x8000FF00);
    path.reset(); path.moveTo(0, 0); path.lineTo(100, 0); path.lineTo(0, 100);
    canvas->drawPath(path, paint);

    paint.setColor(0x80FFFFFF);
    paint.setXfermodeMode(SkXfermode::kPlus_Mode);
    canvas->drawRect(SkRect::MakeXYWH(25, 25, 50, 50), paint);

    return rec.endRecording();
}

bool dumpToPng(const SkBitmap &bm, char *outFileName) {
    bool success = false;
    if (outFileName != nullptr)
        success = SkImageEncoder::EncodeFile(outFileName,bm,SkImageEncoder::kPNG_Type,100);
    return success;
}

int main() {
    SkBitmap bmp;
    bmp.setInfo(SkImageInfo::Make(300, 300,
            kRGBA_8888_SkColorType,
            kOpaque_SkAlphaType));
    printf("canvas width = %d height = %d\n", bmp.width(), bmp.height());
    if (!bmp.tryAllocPixels(nullptr, nullptr)) {
        printf("allocation failed for scaled bitmap");
        return 1;
    }
    SkCanvas canvas(bmp);
    SkPicture* pic = make_picture();
    canvas->drawPicture(pic);
    printf("finish draw picture\n");
    dumpToPng(bmp, "1.png");
    return 0;
}
