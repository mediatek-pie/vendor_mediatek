extern "C" {
#include <stdio.h>
#include <pthread.h>
}
#include "APthread.h"
#include "CoWorker.h"
#include <vector>
#define TOSTRING(x) (#x)
using namespace std;


class JobInCoworker: public Runnable {
public:
	JobInCoworker() { c = count++; }
	void run() {
		printf("JobInCoworker %s(): thread[%lu] doing job [%u]\n", __FUNCTION__, pthread_self(), c);
		for (int i = 0; i < 0xffff; i++)
			;
	}
	static int count;
private:
	int c;
};
int JobInCoworker::count = 0;

int run_with_coworker = 0;
int run_without_coworker = 0;
int test_times = 100;
int run_with_coworker_all = 0;
int run_without_coworker_all = 0;

class CoworkerPolicyTask: public Runnable {
public:
	int policy(){
		const int JOB_NUM = 4;
		JobInCoworker mj[JOB_NUM];

		queue<Runnable *> jobq;
		for (int i = 0; i < JOB_NUM; i++) {
			jobq.push(&mj[i]);
		}
		CoWorker *cw = CoWorker::getInstance();
		//printf("cw = %p\n", cw);
		if (cw->requestWorker(jobq.size())) {
			//printf("thread[%lu] requestWorker success\n", pthread_self());
		}
		if (cw->dispatchJobs(&jobq)) {
			//printf("thread[%lu] can dispatch jobs\n", pthread_self());
			cw->startAndWaitDone();
			run_with_coworker++;
		} else {
			//printf("thread[%lu] cannot use coworker!\n", pthread_self());
			run_without_coworker++;
		}
		return 0;
	}

	void run() {
		//printf("===%s coworker in thread[%u]===.\n", __FUNCTION__, pthread_self());
		policy();
	}
};

class TestReuseCoWoker {
public:
	/*APthread number*/
	static const int snum = 10;
	TestReuseCoWoker() {
		for (int i = 0; i < snum; i++) {
			ftv.push_back(new APthread());
		}
	}
	/*recycle APthread*/
	~TestReuseCoWoker() {
		APthread *tmp;
		for (int i = 0; i < snum; i++) {
			delete ftv[i];
		}
		ftv.clear();
	}
	/*submit job array*/
	void submitJobs(Runnable *r) {
		for (int i = 0; i < snum; i++) {
			ftv[i]->createPthread();
			ftv[i]->setRunnable(r + i);
		}
	}

	void startJobsInCoworker() {
		for (int i = 0; i < snum; i++) {
			ftv[i]->start();
		}
		for (int i = 0; i < snum; i++) {
			ftv[i]->waitUtilRunnableDone();
		}
	}
	int test() {
		printf("****************************submitJob******************************\n");
		CoworkerPolicyTask cpt[TestReuseCoWoker::snum];
		submitJobs(cpt);
		printf("****************************startTest******************************\n");
		startJobsInCoworker();
		printf("***************run with coworker thread times = %u*****************\n", run_with_coworker);
		printf("**************run without coworker thread times = %u***************\n", run_without_coworker);
		run_with_coworker_all += run_with_coworker;
		run_without_coworker_all += run_without_coworker;
		run_with_coworker = 0;
		run_without_coworker = 0;
		return 0;
	}
private:
	vector<APthread *> ftv;
};

int main() {
	printf("main[%lu]...\n", pthread_self());
	TestReuseCoWoker trc;
	for(int i = 0; i < test_times; i++)
		trc.test();
	printf("***************run with coworker thread times in total = %u*****************\n", run_with_coworker_all);
	printf("**************run without coworker thread times in total = %u***************\n", run_without_coworker_all);
	printf("***********************finish and recycle**************************\n");
	return 0;
}
