
/*******************************************************************************
 *
 * Filename:
 * ---------
 * AudioStream.h
 *
 * Project:
 * --------
 *   Android
 *
 * Description:
 * ------------
 *   The class to get uplink and downlink PCM data.
 *
 * Author:
 * -------
 *   Donglei Ji(mtk80823)
 *
 *******************************************************************************/

 #ifndef _AUDIO_STREAM_H_
 #define _AUDIO_STREAM_H_

#include<utils/RefBase.h>
#include <sys/types.h>

#include<system/audio.h>
#include<media/stagefright/AudioSource.h>
#include<media/stagefright/MediaBuffer.h>
#include <media/AudioSystem.h>

using namespace android;

enum {
    kKeyDLTSS   = 1, //int64_t
    kKeyDLTSNS
};

#define BUFFERS_NUM_MAX 5

enum voice_recorder_state {
    VOICE_RECORDER_UNINITIALIZE = -1,
    VOICE_RECORDER_INITIALIZED,
    VOICE_RECORDER_STARTED
};

class AudioStream: virtual public RefBase
{
public:
    AudioStream(audio_source_t inputSource=AUDIO_SOURCE_UNPROCESSED, uint32_t sampleRate=16000, uint32_t channelCount=1);
    ~AudioStream();

    status_t initCheck();
    int32_t latency();
    status_t start();
    status_t stop();
    status_t readPCM(short *pULBuf1, short *pULBuf2, short *pDLBuf, uint32_t size);
    status_t reset();
    int16_t getMaxAmplitude();
    // downlink stream read thread
    void threadFunc();

private:
    sp<AudioSource> m_pAudioSource;
    voice_recorder_state m_RecorderState;

    List<uint32_t> m_DLBufReaded;
    List<uint32_t> m_DLBufFree;
    //MediaBuffer *m_pULBuf;
    MediaBufferBase *m_pULBuf;
    //MediaBuffer *m_pDLBuf[BUFFERS_NUM_MAX];
    //MediaBufferBase *m_pDLBuf[BUFFERS_NUM_MAX];
   
    bool m_bStarted;
    bool m_bNoDLPath;
    bool m_bDLStream;
    bool m_DLStandby;

    audio_source_t m_InputSource;
    uint32_t m_SampleRate;
    uint32_t m_ChannelCount;
    
    int m_DropFrameCnt;
    int m_ULReadCnt;
    int m_ULDataLen;

    pthread_t m_ThreadId;
    Condition m_WorkState;
    Mutex m_Lock;
    Mutex m_DLBufLock;
    Mutex m_ULBufLock;
    
    struct timespec mULCurTime;
    struct timespec mULTime;
    struct timespec mDLStartTime;
    struct timespec mDLCurTime;

    // release queued the downlink buffers
    void releaseQueuedDLBufs();
    void startDLPath();
    void stopDLPath();
    void updateTime(int length, struct timespec *time);
    void dropData();
    void extractULData(short *pULBuf1, short *pULBuf2, short *data, int len);
};
 #endif

