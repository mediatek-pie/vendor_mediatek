#ifndef AURISYS_LVVE_TYPE_H
#define AURISYS_LVVE_TYPE_H

#include <stdint.h>

#include "LVVE.h"

#ifdef __cplusplus
extern "C" {
#endif



/* demo lib data structures */
typedef struct {
    //uint32_t param1[16];
    //uint16_t param2[32];
    // LVVE
    LVM_CHAR LVVE_Tx_Preset_Buffer[LVVE_TX_PRESET_LENGTH];
	LVM_CHAR LVVE_Rx_Preset_Buffer[LVVE_RX_PRESET_LENGTH];
} lvve_sph_param_t;

typedef struct {
    uint32_t frame_index;

    int8_t process_type; // 0: uplink, 1: downlink
    char pcm_debug_buf[1280];

    int16_t  analog_gain;
    int16_t  digital_gain;
    int16_t  mute_on;
    int16_t  enhance_on;
} lvve_epl_buf_t;

typedef struct {
    arsi_task_config_t task_config;
    arsi_lib_config_t  lib_config;

    debug_log_fp_t print_log;
    lvve_sph_param_t sph_param;

    uint32_t tmp_buf_size;
    uint32_t my_private_var;

    int16_t  ul_analog_gain;
    int16_t  ul_digital_gain;
    int16_t  dl_analog_gain;
    int16_t  dl_digital_gain;

    int16_t  b_ul_mute_on;
    int16_t  b_dl_mute_on;

    int16_t  b_ul_enhance_on;
    int16_t  b_dl_enhance_on;

    uint32_t value_at_addr_0x1234;

    uint32_t frame_index;
	// LVVE
	LVM_CHAR **avc_message_ptr;
	LVM_INT32 avc_message_size;
	LVVE_Tx_Handle_t            hInstance_Tx;
	LVVE_Rx_Handle_t            hInstance_Rx;
} lvve_lib_handle_t;



#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* end of AURISYS_LVVE_TYPE_H */

