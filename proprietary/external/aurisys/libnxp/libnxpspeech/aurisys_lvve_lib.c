#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <wrapped_audio.h>
#include <audio_task.h>
#include <arsi_call_type.h>

#include <arsi_type.h>
#include <arsi_api.h>
#include <arsi_library_entry_points.h> // declared library entry point

#include <aurisys_lvve_lib_private_type.h>

#include "LVVE.h"

#ifdef __cplusplus
extern "C" {
#endif


status_t lvve_arsi_get_lib_version(string_buf_t *version_buf)
{
    if (version_buf == NULL) {
        return BAD_VALUE;
    }

    return UNKNOWN_ERROR;
}


status_t lvve_arsi_load_param(
    const string_buf_t       *product_info,
    const string_buf_t       *param_file_path,
    const debug_log_fp_t      debug_log_fp)
{
    if (product_info == NULL ||
        param_file_path == NULL ||
        debug_log_fp == NULL) {
        return BAD_VALUE;
    }

    return UNKNOWN_ERROR;
}


status_t lvve_arsi_query_param_buf_size_by_custom_info(
    const arsi_task_config_t *p_arsi_task_config,
    const arsi_lib_config_t  *p_arsi_lib_config,
    const string_buf_t       *product_info,
    const string_buf_t       *param_file_path,
    const string_buf_t       *custom_info,
    uint32_t                 *p_param_buf_size,
    const debug_log_fp_t      debug_log_fp)
{
    if (p_arsi_task_config == NULL ||
        p_arsi_lib_config == NULL ||
        product_info == NULL ||
        param_file_path == NULL ||
        p_param_buf_size == NULL ||
        debug_log_fp == NULL) {
        return BAD_VALUE;
    }

    if (p_arsi_task_config->task_scene == TASK_SCENE_PHONE_CALL) {
        *p_param_buf_size = sizeof(lvve_sph_param_t) * 3;
    } else if (p_arsi_task_config->task_scene == TASK_SCENE_VOIP) {
        *p_param_buf_size = sizeof(lvve_sph_param_t) * 2;
    } else {
        *p_param_buf_size = 0;
        return BAD_VALUE;
    }

    debug_log_fp("%s(), get param buf size %u for enh mode %s from file %s\n",
                 __func__, *p_param_buf_size, custom_info->p_string, param_file_path->p_string);

    return NO_ERROR;
}


status_t lvve_arsi_parsing_param_file_by_custom_info(
    const arsi_task_config_t *p_arsi_task_config,
    const arsi_lib_config_t  *p_arsi_lib_config,
    const string_buf_t       *product_info,
    const string_buf_t       *param_file_path,
    const string_buf_t       *custom_info,
    data_buf_t               *p_param_buf,
    const debug_log_fp_t      debug_log_fp)
{
    lvve_sph_param_t *p_sph_param = NULL;

    char full_path[256];

    // LVVE
    FILE *fp_bin;
    uint32_t bin_size;
    size_t result;

    if (p_arsi_task_config == NULL ||
        p_arsi_lib_config == NULL ||
        product_info == NULL ||
        param_file_path == NULL ||
        p_param_buf == NULL ||
        debug_log_fp == NULL) {
        return BAD_VALUE;
    }

    if (p_arsi_task_config->task_scene == TASK_SCENE_PHONE_CALL) {
        snprintf(full_path, 256, "%s/Call.bin", param_file_path->p_string);
    } else if (p_arsi_task_config->task_scene == TASK_SCENE_VOIP) {
        snprintf(full_path, 256, "%s/VoIP.bin", param_file_path->p_string);
    } else {
        return BAD_VALUE;
    }

    p_sph_param = (lvve_sph_param_t *)p_param_buf->p_buffer;
    debug_log_fp("%s(), full_path %s, custom_info = %s, p_sph_param: %p, memory_size: %u, sizeof(lvve_sph_param_t) = %lu\n",
                 __func__, full_path, custom_info->p_string,
                 p_sph_param, p_param_buf->memory_size, sizeof(lvve_sph_param_t));


    // should get param from param_file_path
    // LVVE
#if 1
    fp_bin = fopen(full_path, "rb");
    if (fp_bin != NULL) {
        debug_log_fp("The file %s was opened\n", full_path);
    } else {
        debug_log_fp(" The file %s was not opened\n", full_path);
        return FAILED_TRANSACTION;
    }

    fseek(fp_bin, 0, SEEK_END);
    bin_size = ftell(fp_bin);
    rewind(fp_bin);

    if (bin_size > p_param_buf->memory_size) {
        debug_log_fp("bin_size: %u, memory_size: %u, sizeof(lvve_sph_param_t) = %lu\n",
                     bin_size, p_param_buf->memory_size, sizeof(lvve_sph_param_t));
        fclose(fp_bin);
        return NO_MEMORY;
    }

    result = fread(p_param_buf->p_buffer, 1, bin_size, fp_bin);
    if (result != bin_size) {
        debug_log_fp(" The file %s read error, result=%d, bin_size=%d\n", full_path, result, bin_size);
        fclose(fp_bin);
        return FAILED_TRANSACTION;
    }
    fclose(fp_bin);
#endif

    p_param_buf->data_size = bin_size;

    return NO_ERROR;
}


void lvve_arsi_assign_lib_fp(AurisysLibInterface *lib)
{
    lib->arsi_get_lib_version = lvve_arsi_get_lib_version;
    lib->arsi_query_working_buf_size = NULL;
    lib->arsi_create_handler = NULL;
    lib->arsi_query_max_debug_dump_buf_size = NULL;
    lib->arsi_process_ul_buf = NULL;
    lib->arsi_process_dl_buf = NULL;
    lib->arsi_reset_handler = NULL;
    lib->arsi_destroy_handler = NULL;
    lib->arsi_update_device = NULL;
    lib->arsi_update_param = NULL;
    lib->arsi_query_param_buf_size = NULL;
    lib->arsi_parsing_param_file = NULL;
    lib->arsi_set_addr_value = NULL;
    lib->arsi_get_addr_value = NULL;
    lib->arsi_set_key_value_pair = NULL;
    lib->arsi_get_key_value_pair = NULL;
    lib->arsi_set_ul_digital_gain = NULL;
    lib->arsi_set_dl_digital_gain = NULL;
    lib->arsi_set_ul_mute = NULL;
    lib->arsi_set_dl_mute = NULL;
    lib->arsi_set_ul_enhance = NULL;
    lib->arsi_set_dl_enhance = NULL;
    lib->arsi_set_debug_log_fp = NULL;
    lib->arsi_query_process_unit_bytes = NULL;
    lib->arsi_load_param = lvve_arsi_load_param;
    lib->arsi_query_param_buf_size_by_custom_info = lvve_arsi_query_param_buf_size_by_custom_info;
    lib->arsi_parsing_param_file_by_custom_info = lvve_arsi_parsing_param_file_by_custom_info;
}


/* ONLY for dynamic link, like libXXX.so in HAL --> */
void dynamic_link_arsi_assign_lib_fp(AurisysLibInterface *lib)
{
    lvve_arsi_assign_lib_fp(lib); /* like */
}
/* <-- ONLY for dynamic link, like libXXX.so in HAL */



#ifdef __cplusplus
}  /* extern "C" */
#endif

