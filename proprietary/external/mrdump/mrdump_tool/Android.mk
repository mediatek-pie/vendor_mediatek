LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_SRC_FILES := \
	mrdump_tool.c \
	mrdump_support_ext4_fiemap.c \
	mrdump_support_ext4.c \
	mrdump_support_f2fs.c

LOCAL_C_INCLUDES := \
	vendor/mediatek/proprietary/external/aee/mrdump \
	vendor/mediatek/proprietary/external/libsysenv

LOCAL_MODULE := mrdump_tool
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
LOCAL_MODULE_TAGS := optional
LOCAL_SHARED_LIBRARIES := libcutils libmrdumpv libz liblog libsysenv
include $(MTK_EXECUTABLE)
