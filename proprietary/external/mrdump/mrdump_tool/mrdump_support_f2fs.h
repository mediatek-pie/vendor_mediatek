#if !defined(__MRDUMP_SUPPORT_F2FS_H__)
#define __MRDUMP_SUPPORT_F2FS_H__

#include "mrdump_support_ext4.h"

/* Map size for ZeroPage */
#define F2FS_MAPSIZE (1024*1024*8)

/* for IOCTL */
#define F2FS_IOCTL_MAGIC        0xf5
#define F2FS_IOC_SET_PIN_FILE   _IOW(F2FS_IOCTL_MAGIC, 13, __u32)
#define F2FS_IOC_GET_PIN_FILE   _IOR(F2FS_IOCTL_MAGIC, 14, __u32)


/* Function Prototypes */
bool mount_as_f2fs(const char *mountp);
bool f2fs_fallocate(const char *allocfile, uint64_t allocsize);

#endif /* __MRDUMP_SUPPORT_F2FS_H__ */
