LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)
# Add for MTK HDMI
ifeq ($(strip $(MTK_HDMI_SUPPORT)), yes)
  LOCAL_CFLAGS += -DMTK_HDMI_SUPPORT
endif
ifeq ($(strip $(MTK_HDMI_HDCP_SUPPORT)), yes)
  LOCAL_CFLAGS += -DMTK_HDMI_HDCP_SUPPORT
endif
ifeq ($(strip $(MTK_INTERNAL_HDMI_SUPPORT)), yes)
  LOCAL_CFLAGS += -DMTK_INTERNAL_HDMI_SUPPORT
endif
# Add for MTK HDMI end
LOCAL_SRC_FILES:= \
    main_hdmi.cpp

LOCAL_SHARED_LIBRARIES := \
    libutils \
    libbinder \
    liblog \
    libhdmi \
    libhdmiservice

LOCAL_C_INCLUDES += \
      $(TOP)/vendor/mediatek/proprietary/external/hdmi/include

LOCAL_MODULE:= hdmi
#LOCAL_PROPRIETARY_MODULE :=true
LOCAL_MODULE_OWNER := mtk

include $(BUILD_EXECUTABLE)
