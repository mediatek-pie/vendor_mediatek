/* MediaTek Inc. (C) 2016. All rights reserved.
 *
 * Copyright Statement:
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 */

/*
* Description:
*   This is used to debug audio hal and driver by set audio register.
*/


#define LOG_TAG "AudioRegSetting"

typedef unsigned int UINT32;
typedef unsigned short  UINT16;

#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <signal.h>
#include <binder/IPCThreadState.h>
#include <binder/MemoryBase.h>
#include "AudioIoctl.h"

using namespace android;

static char const *const kAudioDeviceName = "/dev/eac";

static void setAudSysReg(int input, int mFd) {
    Register_Control registerData;
    memset((void *)&registerData, 0, sizeof(registerData));
    printf("please enter offset with hex \n");
    int returnValue = 0;
    returnValue = scanf("%x", &input);
    if (returnValue) {
        registerData.offset = input;
        printf("offset = %x\n", registerData.offset);
        printf("please enter value with hex mask = 0xffffffff\n");
        returnValue = scanf("%x", &input);
        if (returnValue) {
            registerData.value = input;
            registerData.mask = 0xffffffff;
            printf("value = %x\n", registerData.value);
            ::ioctl(mFd, SET_AUDSYS_REG, &registerData);
        }
    }
}

static void setAnaAfeReg(int input, int mFd) {
    Register_Control registerData;
    memset((void *)&registerData, 0, sizeof(registerData));
    printf("please enter offset with hex \n");
    int returnValue = 0;
    returnValue = scanf("%x", &input);
    if (returnValue) {
        registerData.offset = input;
        printf("offset = %x\n", registerData.offset);
        printf("please enter value with hex mask = 0xffffffff\n");
        returnValue = scanf("%x", &input);
        if (returnValue) {
            registerData.value = input;
            registerData.mask = 0xffffffff;
            printf("value = %x\n", registerData.value);
            ::ioctl(mFd, SET_ANAAFE_REG, &registerData);
        }
    }
}

static void turnOnSpeaker(bool bEnable, int mFd) {
    if (bEnable) {
        printf("Turn on Speaker \n");
        ::ioctl(mFd, SET_SPEAKER_ON, 1);
    } else {
        printf("Turn off Speaker \n");
        ::ioctl(mFd, SET_SPEAKER_OFF, 0);
    }
}

int main() {
    char str [80];
    int input;
    int exit = 0;
    unsigned int output;
    int fileHandle = 0;
    int returnValue = 0;
    fileHandle = ::open(kAudioDeviceName, O_RDWR);
    printf("open audio drvier mdf = %d\n", fileHandle);
    if (fileHandle <= 0) {
        printf("open audio driver error , return");
    }

    while (!exit) {
        printf("please enter audio ioctl command in hex or ffff to exit \n");
        printf("00:SET_AUDSYS_REG 01:GET_AUDSYS_REG 02:SET_ANAAFE_REG 03:GET_ANAAFE_REG other refernce ioctl code \n");
        returnValue = scanf("%x", &input);
        if (returnValue == 0) {
            printf("enter noting , please re keyin again\n");
            continue;
        }
        switch (input) {
        case 0x00: {
            printf("SET_AUDSYS_REG \n");
            setAudSysReg(input, fileHandle);
            break;
        }
        case 0x01: {
            printf("GET_AUDSYS_REG \n");
            setAudSysReg(input, fileHandle);
            break;
        }
        case 0x02: {
            printf("SET_ANAAFE_REG \n");
            setAnaAfeReg(input, fileHandle);
            break;
        }
        case 0x03: {
            printf("GET_ANAAFE_REG \n");
            setAnaAfeReg(input, fileHandle);
            break;
        }
        case 0x13: {
            printf("Set SPEAKER_ON \n");
            turnOnSpeaker(true, fileHandle);
            break;
        }
        case 0x14: {
            printf("Set SPEAKER_OFF \n");
            turnOnSpeaker(false, fileHandle);
            break;
        }
        case 0x20: {
            printf("OPEN_DL1_STREAM \n");
            ::ioctl(fileHandle, OPEN_DL1_STREAM, 0);
            break;
        }
        case 0x21: {
            printf("CLOSE_DL1_STREAM \n");
            ::ioctl(fileHandle, CLOSE_DL1_STREAM, 0);
            break;
        }
        case 0x22: {
            printf("INIT_DL1_STREAM \n");
            printf("please enter INIT_DL1_STREAM buffer size with hex \n");
            returnValue = scanf("%x", &input);
            if (returnValue) {
                ::ioctl(fileHandle, INIT_DL1_STREAM, input);
            }
            break;
        }
        case 0x23: {
            printf("START_DL1_STREAM \n");
            ::ioctl(fileHandle, START_DL1_STREAM, 0);
            break;
        }
        case 0x24: {
            printf("STANDBY_DL1_STREAM \n");
            ::ioctl(fileHandle, STANDBY_DL1_STREAM, 0);
            break;
        }
        case 0x25: {
            printf("SET_DL1_AFE_BUFFER \n");
            printf("please enter SET_DL1_AFE_BUFFER size with hex \n");
            returnValue = scanf("%x", &input);
            if (returnValue) {
                ::ioctl(fileHandle, SET_DL1_AFE_BUFFER, input);
            }
            break;
        }
        case 0x26: {
            printf("SET_DL1_SLAVE_MODE \n");
            printf("please enter SET_DL1_SLAVE_MODE with hex \n");
            returnValue = scanf("%x", &input);
            if (returnValue) {
                ::ioctl(fileHandle, SET_DL1_SLAVE_MODE, input);
            }
            break;
        }
        case 0x27: {
            printf("GET_DL1_SLAVE_MODE \n");
            ::ioctl(fileHandle, GET_DL1_SLAVE_MODE, &output);
            printf("GET_DL1_SLAVE_MODE output = %x\n", output);
            break;
        }
        case 0x30: {
            printf("OPEN_I2S_INPUT_STREAM \n");
            printf("please enter OPEN_I2S_INPUT_STREAM buffer size with hex \n");
            returnValue = scanf("%x", &input);
            if (returnValue) {
                ::ioctl(fileHandle, OPEN_I2S_INPUT_STREAM, input);
            }
            break;
        }
        case 0x31: {
            printf("CLOSE_I2S_INPUT_STREAM \n");
            ::ioctl(fileHandle, CLOSE_I2S_INPUT_STREAM, 0);
            break;
        }
        case 0x33: {
            printf("START_I2S_INPUT_STREAM \n");
            ::ioctl(fileHandle, START_I2S_INPUT_STREAM, 0);
            break;
        }
        case 0x34: {
            printf("STANDBY_I2S_INPUT_STREAM \n");
            ::ioctl(fileHandle, STANDBY_I2S_INPUT_STREAM, 0);
            break;
        }
        case 0x35: {
            printf("START_I2S_INPUT_STREAM \n");
            ::ioctl(fileHandle, START_I2S_INPUT_STREAM, 0);
            break;
        }
        case 0x36: {
            printf("SET_I2S_Output_BUFFER \n");
            ::ioctl(fileHandle, SET_I2S_Output_BUFFER, 0);
            break;
        }
        case 0x51: {
            printf("AUD_SET_CLOCK \n");
            printf("please enter AUD_SET_CLOCK enable 0 : disable 1 : enable \n");
            returnValue = scanf("%x", &input);
            if (returnValue) {
                ::ioctl(fileHandle, AUD_SET_CLOCK, input);
            }
            break;
        }
        case 0x52: {
            printf("AUD_SET_26MCLOCK \n");
            printf("please enter AUD_SET_26MCLOCK enable 0 : disable 1 : enable \n");
            returnValue = scanf("%x", &input);
            if (returnValue) {
                ::ioctl(fileHandle, AUD_SET_26MCLOCK, input);
            }
            break;
        }
        case 0x53: {
            printf("AUD_SET_ADC_CLOCK \n");
            printf("please enter AUD_SET_ADC_CLOCK enable 0 : disable 1 : enable \n");
            returnValue = scanf("%x", &input);
            if (returnValue) {
                ::ioctl(fileHandle, AUD_SET_ADC_CLOCK, input);
            }
            break;
        }
        case 0x54: {
            printf("AUD_SET_I2S_CLOCK \n");
            printf("please enter AUD_SET_I2S_CLOCK enable 0 : disable 1 : enable \n");
            returnValue = scanf("%x", &input);
            if (returnValue) {
                ::ioctl(fileHandle, AUD_SET_I2S_CLOCK, input);
            }
            break;
        }
        case 0x60: {
            printf("AUDDRV_RESET_BT_FM_GPIO \n");
            ::ioctl(fileHandle, AUDDRV_RESET_BT_FM_GPIO, 0);
            break;
        }
        case 0x61: {
            printf("AUDDRV_SET_BT_PCM_GPIO \n");
            ::ioctl(fileHandle, AUDDRV_SET_BT_PCM_GPIO, 0);
            break;
        }
        case 0x62: {
            printf("AUDDRV_SET_FM_I2S_GPIO \n");
            ::ioctl(fileHandle, AUDDRV_SET_FM_I2S_GPIO, 0);
            break;
        }
        case 0x63: {
            printf("AUDDRV_MT6573_CHIP_VER \n");
            ::ioctl(fileHandle, AUDDRV_MT6573_CHIP_VER, 0);
            break;
        }
        case 0x81: {
            printf("AUDDRV_START_DAI_OUTPUT \n");
            ::ioctl(fileHandle, AUDDRV_START_DAI_OUTPUT, 0);
            break;
        }
        case 0x82: {
            printf("AUDDRV_STOP_DAI_OUTPUT \n");
            ::ioctl(fileHandle, AUDDRV_STOP_DAI_OUTPUT, 0);
            break;
        }
        case 0xFD: {
            printf("AUDDRV_LOG_PRINT \n");
            ::ioctl(fileHandle, AUDDRV_LOG_PRINT, 0);
            break;
        }
        case 0xFE: {
            printf("AUDDRV_ASSERT_IOCTL \n");
            ::ioctl(fileHandle, AUDDRV_ASSERT_IOCTL, 0);
            break;
        }
        case 0xFF: {
            printf("AUDDRV_BEE_IOCTL \n");
            ::ioctl(fileHandle, AUDDRV_BEE_IOCTL, 0);
            break;
        }
        case 0xffff: {
            printf("0xffff exit!!! \n");
            exit = true;
            break;
        }
        default: {
            printf("not support such command please enter again\n");
        }
        }
    }
    return 0;
}


