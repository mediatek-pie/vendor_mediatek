#include <netlink/genl/genl.h>
#include <netlink/genl/family.h>
#include <netlink/genl/ctrl.h>
#include <unistd.h>
#include "event_loop.h"
#include "nl80211.h"
//#include <sys/wait.h>
//#include <sys/types.h>
struct nl80211_cfg {
	struct nl_cb *nl_cb;
	struct nl_sock *nl;
	struct nl_sock *nl_event;
	s32 rtm_sock;
	s32 nl80211_id;
	s32 ifindex;
	struct wifi_callback_funcs agps_cb;
};

struct family_data {
	const s8 *group;
	s32 id;
};

struct driver_ap_info {
	u8 bssid[6];
    s16 ap_rssi; //-127..128
    u16 ap_channel;   //0..256
    wifi2agps_ap_device_type_e phy_type;
};

struct driver_ap_list {
	u8 num;
	struct driver_ap_info ap_info[32];
};

struct rtm_attr {
	unsigned short len;
	unsigned short type;
};

static struct nl80211_cfg nl_cfg;
static int android_genl_ctrl_resolve(struct nl_sock *handle, const s8 *name);

static int tranlate_to_android_level(u32 level)
{
        if (level == MSG_ERROR)
                return ANDROID_LOG_ERROR;
        if (level == MSG_WARNING)
                return ANDROID_LOG_WARN;
        if (level == MSG_INFO)
                return ANDROID_LOG_INFO;
        return ANDROID_LOG_DEBUG;
}

void wifi2agps_log(u32 level, s8* fmt, ...) {
        va_list ap;

        va_start(ap, fmt);
        __android_log_vprint(tranlate_to_android_level(level),
                                     "wlan-assistant", fmt, ap);
        va_end(ap);
}

static struct nl_sock * nl_create_handle(struct nl_cb *cb, const s8 *dbg)
{
	struct nl_sock *handle;

	handle = nl_socket_alloc_cb(cb);
	if (handle == NULL) {
		wifi2agps_log(MSG_ERROR, "nl80211: Failed to allocate netlink "
			   "callbacks (%s)", dbg);
		return NULL;
	}

	if (genl_connect(handle)) {
		wifi2agps_log(MSG_ERROR, "nl80211: Failed to connect to generic "
			   "netlink (%s)", dbg);
		nl_socket_free(handle);
		return NULL;
	}

	return handle;
}

static void nl_destroy_handles(struct nl_sock **handle)
{
	if (*handle == NULL)
		return;
	nl_socket_free(*handle);
	*handle = NULL;
}

static void nl80211_event_receive(void *eloop_ctx,
					     void *handle)
{
	nl_recvmsgs(handle, (struct nl_cb*)eloop_ctx);
}

static int android_genl_ctrl_resolve(struct nl_sock *handle,
				     const s8 *name)
{
	struct nl_cache *cache = NULL;
	struct genl_family *nl80211 = NULL;
	int id = -1;

	if (genl_ctrl_alloc_cache(handle, &cache) < 0) {
		wifi2agps_log(MSG_ERROR, "nl80211: Failed to allocate generic "
			   "netlink cache");
		goto fail;
	}

	nl80211 = genl_ctrl_search_by_name(cache, name);
	if (nl80211 == NULL)
		goto fail;

	id = genl_family_get_id(nl80211);

fail:
	if (nl80211)
		genl_family_put(nl80211);
	if (cache)
		nl_cache_free(cache);

	return id;
}


/* nl80211_init_nl: initialize the netlink interface to nl80211, to listen the multicast
	event from cfg80211 in driver */
static int nl80211_init_nl(struct nl80211_cfg *nl_cfg)
{
	nl_cfg->ifindex = -1;
	nl_cfg->nl_cb = nl_cb_alloc(NL_CB_DEFAULT);
	if (nl_cfg->nl_cb == NULL) {
		wifi2agps_log(MSG_ERROR, "nl80211: Failed to allocate netlink "
			   "callbacks");
		return -1;
	}

	nl_cfg->nl = nl_create_handle(nl_cfg->nl_cb, "nl");
	if (nl_cfg->nl == NULL)
		goto err;

	nl_cfg->nl80211_id = android_genl_ctrl_resolve(nl_cfg->nl, "nl80211");
	if (nl_cfg->nl80211_id < 0) {
		wifi2agps_log(MSG_ERROR, "nl80211: 'nl80211' generic netlink not "
			   "found");
		goto err;
	}

	nl_cfg->nl_event = nl_create_handle(nl_cfg->nl_cb, "event");
	if (nl_cfg->nl_event == NULL)
		goto err;

	event_loop_add_event(nl_socket_get_fd(nl_cfg->nl_event), nl_cfg->nl_cb,
				 nl_cfg->nl_event, nl80211_event_receive);

	return 0;

err:
	nl_destroy_handles(&nl_cfg->nl_event);
	nl_destroy_handles(&nl_cfg->nl);
	nl_cb_put(nl_cfg->nl_cb);
	nl_cfg->nl_cb = NULL;
	return -1;
}

int wifi2agps_init(struct wifi_callback_funcs *callback) {
	memcpy(&nl_cfg.agps_cb, callback, sizeof(struct wifi_callback_funcs));
	if (nl80211_init_nl(&nl_cfg) < 0)
		return -1;

	return 0;
}

int wifi2agps_deinit() {
	event_loop_terminate();
	return 0;
}
