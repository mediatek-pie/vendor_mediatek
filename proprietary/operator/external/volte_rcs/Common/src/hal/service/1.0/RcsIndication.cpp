#include "RcsIndication.h"

namespace vendor {
namespace mediatek {
namespace hardware {
namespace rcs {
namespace V1_0 {
namespace implementation {

// Methods from ::vendor::mediatek::hardware::rcs::V1_0::IRcsIndication follow.
Return<void> RcsIndication::readEvent(const hidl_vec<int8_t>& data, int32_t offset, int32_t length) {
    // TODO implement
    return Void();
}


// Methods from ::android::hidl::base::V1_0::IBase follow.

IRcsIndication* HIDL_FETCH_IRcsIndication(const char* /* name */) {
    return new RcsIndication();
}

}  // namespace implementation
}  // namespace V1_0
}  // namespace rcs
}  // namespace hardware
}  // namespace mediatek
}  // namespace vendor
