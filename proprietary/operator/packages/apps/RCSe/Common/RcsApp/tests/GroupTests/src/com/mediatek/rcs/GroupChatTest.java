package com.mediatek.rcs;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;
import android.app.Instrumentation;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.os.Message;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.Contacts.Data;
import android.provider.ContactsContract.RawContacts;
import android.provider.MediaStore;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mediatek.rcse.activities.ChatScreenActivity;
import com.mediatek.rcse.activities.PluginProxyActivity;
import com.mediatek.rcse.activities.SelectContactsActivity;
import com.mediatek.rcse.api.Logger;
import com.mediatek.rcse.api.Participant;
import com.mediatek.rcse.interfaces.ChatController;
import com.mediatek.rcse.mvc.ControllerImpl;
import com.mediatek.rcse.plugin.message.IpMessageConsts.SelectContactType;
import com.mediatek.rcse.service.MediatekFactory;
import com.mediatek.rcse.service.PluginApiManager;
import com.mediatek.rcs.R;
import com.mediatek.rcse.plugin.message.PluginGroupChatActivity;
import com.mediatek.rcse.api.Participant;   


import com.gsma.services.rcs.JoynServiceConfiguration;

import com.jayway.android.robotium.solo.Solo;

/**
 * The Class GroupChatTest.
 */
public class GroupChatTest extends
    ActivityInstrumentationTestCase2<ChatScreenActivity> {

  /** The solo. */
  private Solo solo;
  //private UiDevice mDevice;
  /** The m instrumentation. */
  protected Instrumentation mInstrumentation;
  
  /** The m context. */
  protected Context mContext;

  /** The Constant TAG. */
  public static final String TAG = "GroupChatTest"; 
  
  /** The Constant KEY_ADD_CONTACTS. */
  public static final String KEY_ADD_CONTACTS = "addContacts";
  
  /** The Constant VALUE_ADD_CONTACTS. */
  public static final String VALUE_ADD_CONTACTS = "fromChatMainActivity";
  
  /** The Constant MULTIPLE_FILE_URI. */
  public static final String MULTIPLE_FILE_URI = "rcs_multiple_file_uri";
  
  /**
   * The Class ChatMode.
   */
  public static final class ChatMode {
      
      /** The Constant JOYN. */
      public static final int JOYN = 1;
      
      /** The Constant XMS. */
      public static final int XMS  = 2;
  }
  /**
   * Defined as the select contacts action.
   */
  public static final String SELECT_PLUGIN_CONTACT_ACTION =
          "com.mediatek.rcse.action.PluginSelectContact";
  
  /** The Constant MAX_NUMBER_OF_SCROLLS. */
  public static final int MAX_NUMBER_OF_SCROLLS = 100; 
  
  /** The participants. */
  ArrayList<Participant> participants = null;
  
  /**
   * Instantiates a new group chat test.
   */
  public GroupChatTest() {
    super(ChatScreenActivity.class);
  }

  /* (non-Javadoc)
   * @see android.test.ActivityInstrumentationTestCase2#setUp()
   */
  public void setUp() throws Exception {
    solo = new Solo(getInstrumentation(), getActivity());
    //mDevice = UiDevice.getInstance();
    Log.d(TAG, "setUp entry ");
    mInstrumentation = getInstrumentation();
    mContext = mInstrumentation.getTargetContext();
    boolean isRegistered = JoynServiceConfiguration.isServiceActivated(getActivity());
    if(!isRegistered) {
        Log.d(TAG, "setUp RCS Not registered, tear Down ");
        assertTrue("RCS Not registered", false);
    }
    Participant firstContact = new Participant("+14259456689",
            "1425");
    Participant secondContact = new Participant("+14252330593",
            "1426");
    participants = new ArrayList<Participant>();
    participants.add(firstContact);
    participants.add(secondContact);
    Intent chat = new Intent();
    chat.putParcelableArrayListExtra(Participant.KEY_PARTICIPANT_LIST,
            participants);
    createGroupChatWindow(chat,2);
    solo.sleep(2000);
  }

  /* (non-Javadoc)
   * @see android.test.ActivityInstrumentationTestCase2#tearDown()
   */
  @Override
  public void tearDown() throws Exception {
      Log.d(TAG, "tearDown entry ");
      solo.finishOpenedActivities();
  }
  
  /**
   * Creates the group chat window.
   *
   * @param intent the intent
   * @param size the size
   */
  private void createGroupChatWindow(Intent intent, int size) {
      Log.d(TAG, "createGroupChatWindow()");
      intent.putExtra(KEY_ADD_CONTACTS, VALUE_ADD_CONTACTS);     
      intent.setAction(PluginGroupChatActivity.ACTION);     
      intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      mContext.startActivity(intent);
  }
  
  /**
   * Test001_ create group chat subject.
   */
  public void test001_CreateGroupChatSubject() {
      solo.sleep(2000);
      Log.d(TAG, "testCreateGroupChat entry ");
      //solo.assertCurrentActivity("wrong activity", PluginGroupChatActivity.class);
      final EditText subjectEditText = (EditText)solo.getView(com.mediatek.rcs.R.id.inputEditText);
      assertTrue("Subject Box not found", subjectEditText != null);
      
      //Enter subject in group subject dialog
      
      try {
        runTestOnUiThread(new Runnable() {
              @Override
              public void run() {
                  subjectEditText.setText("TestGroup");
             }
         });
    } catch (Throwable e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
      //Button okButton = (Button)solo.getView(com.mediatek.rcs.R.id.button1);
      TextView oktext = (TextView)solo.getText("Ok");
      assertTrue("OK Button not found", oktext != null);
      // CLick on ok button
      solo.clickOnView(oktext);
      solo.sleep(2000);
  }
  
    /**
     * Creates the group subject.
     */
    private void createGroupSubject() {
        Log.d(TAG, "createGroupSubject entry ");
        // solo.assertCurrentActivity("wrong activity",
        // PluginGroupChatActivity.class);
        final EditText subjectEditText = (EditText) solo
                .getView(com.mediatek.rcs.R.id.inputEditText);
        assertTrue("Subject Box not found", subjectEditText != null);
        // Enter subject in group subject dialog
        try {
            runTestOnUiThread(new Runnable() {
                @Override
                public void run() {
                    subjectEditText.setText("TestGroup");
                }
            });
        } catch (Throwable e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        TextView oktext = (TextView) solo.getText("Ok");
        assertTrue("OK Button not found", oktext != null);
        // CLick on ok button
        solo.clickOnView(oktext);
        solo.sleep(2000);

    }
  
  
  /**
   * Test002_ send group chat first message.
   */
  public void test002_SendGroupChatFirstMessage() {
      Log.d(TAG, "testSendGroupChatFirstMessage entry ");
      createGroupSubject();
      //solo.assertCurrentActivity("wrong activity", PluginGroupChatActivity.class);
      
      final EditText chatEditText = (EditText)solo.getView("et_chat_message");
      assertTrue("Chat Edit Text Box not found", chatEditText != null);
      try {
        runTestOnUiThread(new Runnable() {
              @Override
              public void run() {
                  //chatEditText.getText().clear();
                  //chatEditText.setText("TestMessage1"); 
                  try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                  chatEditText.setVisibility(View.VISIBLE);
                  chatEditText.setFocusable(true);
                  chatEditText.setClickable(true);
                  chatEditText.setEnabled(true);
                  solo.clickOnView(chatEditText);
                  chatEditText.setText("TestMessage1"); 
                  Log.d(TAG, "Send TestMessage1");
             }
         });
    } catch (Throwable e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
        Log.d(TAG, "Send TestMessage1 Exception");
    }
      Log.d(TAG, "testSendGroupChatFirstMessage chatEditText " + chatEditText);
      //Enter subject in group subject dialog
      solo.sleep(2000);
      ImageButton sendButton = (ImageButton)solo.getView(com.mediatek.rcs.R.id.btn_chat_send);
      assertTrue("Send Button not found", sendButton != null);
      // CLick on send button
      solo.clickOnView(sendButton);
      solo.sleep(2000);
      CheckFirstMessageDisplay();
  }
  
  /**
   * Check first message displays on UI or not.
   */
  public void CheckFirstMessageDisplay() {
      Log.d(TAG, "testCheckFirstMessageDisplay entry ");      
      //Check if first message shows correctly on chatscreen 
      TextView firstMessage = (TextView)solo.getView(com.mediatek.rcs.R.id.chat_text_display);
      assertTrue("firstMessage not display on chatwindow", firstMessage != null && firstMessage.getText() == "TestMessage1");
  }
  
  /**
   * Test003_ group subject display correctly.
   */
  public void test003_GroupSubjectDisplay() {
      solo.sleep(2000);
      createGroupSubject();
      Log.d(TAG, "testGroupSubjectDisplay entry ");
      //solo.assertCurrentActivity("wrong activity", PluginGroupChatActivity.class);
      // Check if subject is displaying correctly
      TextView groupSubjectTitle = (TextView)solo.getView(com.mediatek.rcs.R.id.peer_name);
      assertTrue("groupSubjectTitle not display on chatwindow", groupSubjectTitle != null && groupSubjectTitle.getText().equals("TestGroup"));
  }
  
  /**
   * Test004_ number of activeparticipants count correct or not.
   */
  public void test004_NumberOfActiveparticipants() {
      solo.sleep(2000);
      createGroupSubject();
      Log.d(TAG, "testNumberOfActiveparticipants entry ");
      //solo.assertCurrentActivity("wrong activity", PluginGroupChatActivity.class);
      // Test Number of active particpants should be more than 1
      
      TextView activeCount = (TextView)solo.getView(com.mediatek.rcs.R.id.peer_number);
      String activeCountParticipants = (String) activeCount.getText().subSequence(1, 1);
      assertTrue("Number of active participants is zero", activeCount != null && !activeCountParticipants.equals("0"));
  }
  
  /**
   * Test005_initiate photo to group chat.
   */
  public void test005_initiatePhoto() {
      solo.sleep(2000);
      createGroupSubject();
      Log.d(TAG, "testNumberOfActiveparticipants entry ");
      ImageButton attachIcon = (ImageButton)solo.getView(com.mediatek.rcs.R.id.btn_chat_add);
      assertTrue("Attach Button not found", attachIcon != null);
      solo.clickOnView(attachIcon);
      solo.sleep(2000);      
      solo.clickOnView(attachIcon);
      
      // Select photo from gallery
      
      String[] projection = { MediaStore.Images.Media._ID };
      String selection = "";
      String imagePath = null;
      String [] selectionArgs = null;
      Cursor mImageCursor = getActivity().getContentResolver().query( MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI,
                                   projection, selection, selectionArgs, null );
        if (mImageCursor != null && mImageCursor.moveToFirst()) {
            int column_index = mImageCursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            imagePath = mImageCursor.getString(column_index);
        }
        mImageCursor.close();
        Log.d(TAG, "test005_initiatePhoto entry imagePath" + imagePath);
        solo.sleep(2000);
        // Start group chat file transfer
        ArrayList<String> files = new ArrayList<String>();
        if(imagePath != null) {
            files.add(imagePath);
        }
        else { 
            files.add(createDummyFile());
        }
        startGroupFileTransfer(participants, files);
        solo.sleep(4000);
        createGroupSubject();
        solo.sleep(2000);
        Log.d(TAG, "testNumberOfActiveparticipants entry ");
        // Check if file visible on UI
        View fileTransferView = solo.getView(com.mediatek.rcs.R.id.file_transfer_view);
        assertTrue("fileTransferView not found", fileTransferView != null);
  }
    
  /**
   * Test006_add participants to group chat.
   */
  public void test006_addParticipantsToGroupChat() {
      solo.sleep(2000);
      createGroupSubject();
      Log.d(TAG, "test006_addParticipantsToGroupChat entry ");
      View addIcon = solo.getView(com.mediatek.rcs.R.id.menu_add_contact);
      assertTrue("Add Button not found", addIcon != null);
      solo.clickOnView(addIcon);
      solo.sleep(2000); 
      
      
      
  }  
    
  /**
   * Creates the dummy file.
   *
   * @return the string
   */
  private String createDummyFile() {
      String name = "AtTestFileGroup.txt";
      String directory = Environment.getExternalStorageDirectory().toString();
      String finalName = directory + name;
      Log.d(TAG, "createDummyFile filename is: " + directory + name); 
      File testFile = new File(directory, name);
      BufferedOutputStream testFileStream = null;
      try {
              testFileStream = new BufferedOutputStream(new FileOutputStream(testFile));
      }catch(Exception e){
              Log.d(TAG, "testSendMessage fail file not found");
              e.printStackTrace();
      }
      
      String fileContent = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
      byte[] b = fileContent.getBytes();
      try {
       testFileStream.write(b, 0, b.length);
       testFileStream.flush();
       testFileStream.close();
      }catch(Exception e){
              Log.d(TAG, "testSendMessage fail "); 
              e.printStackTrace();
              
      }    
      return finalName;
  }
  
  /**
   * Start group file transfer.
   *
   * @param participants the participants
   * @param files the files
   */
  private void startGroupFileTransfer(ArrayList<Participant> participants,
          ArrayList<String> files) {
      Log.d(TAG, "startGroupFileTransfer() entry with participants is "
              + participants + "" + " and files is " + files);
      Log.d(TAG, "startGroupFileTransfer()-The files is " + files);
      if (files != null) {
          Intent intent = new Intent();
          intent.setAction(PluginGroupChatActivity.ACTION);
                      intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                      intent.putParcelableArrayListExtra(
                              "participantList", participants);
                      intent.putExtra(KEY_ADD_CONTACTS,
                              VALUE_ADD_CONTACTS);
                      intent.putExtra(MULTIPLE_FILE_URI,
                              files);
                      intent.putExtra("isChatGroup", true);
                      intent.putExtra("ftfromshare", true);
                      intent.putExtra("chatmode", ChatMode.JOYN);
          try {
              getActivity().startActivity(intent);
          } catch (android.content.ActivityNotFoundException e) {
              e.printStackTrace();
          }
      }     
  }  
 
}