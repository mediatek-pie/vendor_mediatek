/*******************************************************************************
 * Software Name : RCS IMS Stack
 *
 * Copyright (C) 2010 France Telecom S.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.mediatek.presence.provisioning;

import android.content.Context;
import android.telephony.TelephonyManager;

import com.mediatek.ims.rcsua.AcsEventCallback;
import com.mediatek.ims.rcsua.RcsUaService;

import com.mediatek.presence.provider.settings.RcsSettings;
import com.mediatek.presence.provider.settings.RcsSettingsData;
import com.mediatek.presence.utils.logger.Logger;


public class AcsProvisioningController {

    private Context mContext;
    private Logger logger = Logger.getLogger(
            AcsProvisioningController.class.getSimpleName());
    private RcsUaService mRcsUaService;
    private UceEventCallback mUceEventCallback;

    private static final String KEY_PUBLUSH_THROTTLE = "source-throttlepublish";
    private static final String KEY_SUBSCRIBE_EXPIRY = "pollingPeriod";
    private static final String KEY_NON_RCS_CAPABILITY_INFO_EXPIRY = "nonRCScapInfoExpiry";
    private static final String KEY_CAPABILITY_INFO_EXPIRY = "capInfoExpiry";

    public AcsProvisioningController(Context context) {
        logger.debug("Constructor");
        mContext = context;
        mRcsUaService = RcsUaService.startService(context, mRcsUaServiceCallback, null);
    }

    public void initDataBase() {
        logger.debug("initDataBase");
        TelephonyManager tm = (TelephonyManager) mContext.getSystemService(
                    Context.TELEPHONY_SERVICE);

            String impi = tm.getIsimImpi();
            String[] impu = tm.getIsimImpu();
            String domain = tm.getIsimDomain();
            String MSISDN="";
            if (impi == null || impu == null || domain == null) {
                logger.error("impi == null || impu == null || domain == null");
                return;
            }
            try {
                MSISDN = extractUserNamePart(impu[0]);
            } catch (Exception e) {
                MSISDN="";
                logger.error("updateSIMDetailsinDB is MSISDN null");
                e.printStackTrace();
            }
            if(MSISDN == null) {
                MSISDN="";
            }
            logger.error("setDefaultProvisioningValue for AT&T: impu[0]: " + impu[0] +
                    ", impi: " + impi + ", domain: " + domain + ", msisdn: " + MSISDN);
            for (int i = 0; i < impu.length; i++) {
                logger.error("impu[" + i + "]:" + impu[i]);
            }

            RcsSettings.getInstance().setUserProfileImsUserName_full(impu[0]);
            RcsSettings.getInstance().setUserProfileImsPrivateId(impi);
            RcsSettings.getInstance().setUserProfileImsDomain(domain);
            RcsSettings.getInstance().setUserProfileImsDisplayName(MSISDN);
            RcsSettings.getInstance().setUserProfileImsUserName(MSISDN);

            //SERVICE CHANGES
            RcsSettings.getInstance().writeParameter(RcsSettingsData.CAPABILITY_SOCIAL_PRESENCE, Boolean.toString(true));
            RcsSettings.getInstance().writeParameter(RcsSettingsData.CAPABILITY_PRESENCE_DISCOVERY, Boolean.toString(true));
            RcsSettings.getInstance().writeParameter(RcsSettingsData.AUTO_ACCEPT_CHAT,RcsSettingsData.TRUE);
            RcsSettings.getInstance().writeParameter(RcsSettingsData.AUTO_ACCEPT_GROUP_CHAT,RcsSettingsData.TRUE);
            RcsSettings.getInstance().writeParameter(RcsSettingsData.PERMANENT_STATE_MODE,RcsSettingsData.FALSE);
            RcsSettings.getInstance().writeParameter(RcsSettingsData.IMS_AUTHENT_PROCEDURE_WIFI, RcsSettingsData.AKA_AUTHENT);
            RcsSettings.getInstance().writeParameter(RcsSettingsData.IMS_AUTHENT_PROCEDURE_MOBILE, RcsSettingsData.AKA_AUTHENT);
            RcsSettings.getInstance().writeParameter(RcsSettingsData.RCS_VOLTE_SINGLE_REGISTRATION, "1");
            RcsSettings.getInstance().setCPMSupported(true);
            RcsSettings.getInstance().setServicePermissionState(true);
            //CAPABILITY INFO EXPIRY shall be 6 hours
            // NON RCS CAPABILITY INFO EXPIRY shall be 72 hours
            logger.debug("chage expiry time to 21600");
            RcsSettings.getInstance().writeParameter(
                    RcsSettingsData.CAPABILITY_EXPIRY_TIMEOUT, "21600");
            //Configure RCS related capabilities
            RcsSettings.getInstance().writeParameter(
                    RcsSettingsData.CAPABILITY_CS_VIDEO, RcsSettingsData.FALSE);
            RcsSettings.getInstance().writeParameter(
                    RcsSettingsData.CAPABILITY_IMAGE_SHARING, RcsSettingsData.FALSE);
            RcsSettings.getInstance().writeParameter(
                    RcsSettingsData.CAPABILITY_VIDEO_SHARING, RcsSettingsData.FALSE);
            RcsSettings.getInstance().writeParameter(
                    RcsSettingsData.CAPABILITY_IP_VOICE_CALL, RcsSettingsData.FALSE);
            RcsSettings.getInstance().writeParameter(
                    RcsSettingsData.CAPABILITY_IP_VIDEO_CALL, RcsSettingsData.FALSE);
            RcsSettings.getInstance().writeParameter(
                    RcsSettingsData.CAPABILITY_IM_SESSION, RcsSettingsData.FALSE);
            RcsSettings.getInstance().writeParameter(
                    RcsSettingsData.CAPABILITY_IM_GROUP_SESSION, RcsSettingsData.FALSE);
            RcsSettings.getInstance().writeParameter(
                    RcsSettingsData.CAPABILITY_FILE_TRANSFER, RcsSettingsData.FALSE);
            RcsSettings.getInstance().writeParameter(
                    RcsSettingsData.CAPABILITY_FILE_TRANSFER_HTTP, RcsSettingsData.FALSE);
            RcsSettings.getInstance().writeParameter(
                    RcsSettingsData.CAPABILITY_SOCIAL_PRESENCE, RcsSettingsData.FALSE);
            RcsSettings.getInstance().writeParameter(
                    RcsSettingsData.CAPABILITY_GEOLOCATION_PUSH, RcsSettingsData.FALSE);
            RcsSettings.getInstance().writeParameter(
                    RcsSettingsData.CAPABILITY_FILE_TRANSFER_THUMBNAIL, RcsSettingsData.FALSE);
            RcsSettings.getInstance().writeParameter(
                    RcsSettingsData.CAPABILITY_GROUP_CHAT_SF, RcsSettingsData.FALSE);
            RcsSettings.getInstance().writeParameter(
                    RcsSettingsData.CAPABILITY_FILE_TRANSFER_SF, RcsSettingsData.FALSE);
            RcsSettings.getInstance().writeParameter(
                    RcsSettingsData.CAPABILITY_SMSOverIP, RcsSettingsData.FALSE);
            RcsSettings.getInstance().writeParameter(
                    RcsSettingsData.CAPABILITY_ICSI_MMTEL, RcsSettingsData.FALSE);
            RcsSettings.getInstance().writeParameter(
                    RcsSettingsData.CAPABILITY_ICSI_EMERGENCY, RcsSettingsData.FALSE);
            RcsSettings.getInstance().writeParameter(
                    RcsSettingsData.STANDALONE_MSG_SUPPORT, RcsSettingsData.FALSE);
    }

    public class UceEventCallback extends AcsEventCallback {

        public UceEventCallback(){}

        public void onConfigurationStatusChanged(boolean valid, int version) {
            logger.debug("onConfigurationStatusChanged: valid: " + valid + " version: " + version);
            if (valid == true) {
                 updateProvisionValues();
            }
        }

        public void onAcsConnected() {
             logger.debug("onAcsConnected");
             updateProvisionValues();
        }

        public void onAcsDisconnected() {
            logger.debug("onAcsDisconnected");
        }
    }


    private RcsUaService.Callback mRcsUaServiceCallback = new RcsUaService.Callback() {
            @Override
            public void serviceConnected(RcsUaService service) {
                logger.debug("serviceConnected:" + service);
                if (RcsUaService.isAcsAvailable(mContext)) {
                    if (mUceEventCallback == null) {
                        logger.debug("register callback of AcsEvents");
                        mUceEventCallback = new UceEventCallback();
                        mRcsUaService.registerAcsEventCallback(mUceEventCallback);
                    } else {
                        logger.error("Callback has not be initialized");
                    }
                } else {
                    logger.error("ACS is not available");
                }
            }
            @Override
            public void serviceDisconnected(RcsUaService service) {
                logger.debug("serviceDisconnected:" + service);

                if (mUceEventCallback != null) {
                    mUceEventCallback = null;
                }

                if (mRcsUaService != null) {
                    mRcsUaService = null;
                }
            }
        };

    private void updateProvisionValues() {
        int publishThrottle = mRcsUaService.getAcsConfigInt(KEY_PUBLUSH_THROTTLE);
        logger.debug("publishThrottle: " + publishThrottle);
        if (publishThrottle > 0) {
                RcsSettings.getInstance().writeParameter(RcsSettingsData.SOURCE_THROTTLE_PUBLISH, Integer.toString(publishThrottle));
        }

        int subscribeExpire = mRcsUaService.getAcsConfigInt(KEY_SUBSCRIBE_EXPIRY);
        logger.debug("subscribeThrottle: " + subscribeExpire);
        if (subscribeExpire > 0) {
                RcsSettings.getInstance().writeParameter(RcsSettingsData.SUBSCRIBE_EXPIRE_PERIOD, Integer.toString(subscribeExpire));
        }

        int nonRcsExpiry = mRcsUaService.getAcsConfigInt(KEY_NON_RCS_CAPABILITY_INFO_EXPIRY);
        logger.debug("nonRcsExpiry: " + nonRcsExpiry);
        if (nonRcsExpiry > 0) {
            RcsSettings.getInstance().writeParameter(
                RcsSettingsData.NON_RCS_CAPABILITY_EXPIRY_TIMEOUT, Integer.toString(nonRcsExpiry));
        }

        int rcsExpiry = mRcsUaService.getAcsConfigInt(KEY_CAPABILITY_INFO_EXPIRY);
        logger.debug("rcsExpiry: " + rcsExpiry);
        if (rcsExpiry > 0) {
            RcsSettings.getInstance().writeParameter(
                RcsSettingsData.CAPABILITY_EXPIRY_TIMEOUT, Integer.toString(rcsExpiry));
        }
    }

    private  String extractUserNamePart(String uri) {
        if ((uri == null) || (uri.trim().length() == 0)) {
            return "";
        }

        try {
            uri = uri.trim();
            int index1 = uri.indexOf("sip:");
            if (index1 != -1) {
                int index2 = uri.indexOf("@", index1);
                String result = uri.substring(index1+4, index2);
                return result;
            } else {
                return uri;
            }
        } catch(Exception e) {
            return "";
        }
    }
}
