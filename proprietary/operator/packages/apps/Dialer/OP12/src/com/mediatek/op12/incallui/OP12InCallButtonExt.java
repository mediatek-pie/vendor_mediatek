package com.mediatek.op12.incallui;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.mediatek.incallui.ext.DefaultInCallButtonExt;
import com.mediatek.op12.dialer.R;

import mediatek.telecom.MtkTelecomManager;

/**
 * Plug in implementation for OP12 InCallButton interfaces.
 */
public class OP12InCallButtonExt extends DefaultInCallButtonExt {
    private static final String TAG = "OP12InCallButtonExt";
    private Context mContext;

    /** Constructor.
     * @param context context
     */
    public OP12InCallButtonExt(Context context) {
        mContext = context;
    }

    /**
     * Check if one way video call is supported
     * @param call call object
     */
    @Override
    public boolean isOneWayVideoSupportedForCall(Object call) {
      Log.d(TAG, "isOneWayVideoSupportedForCall entry");
      return true;
    }

    @Override
    public boolean isOneWayVideoSupported() {
      Log.d(TAG, "isOneWayVideoSupported true");
      return true;
    }

    @Override
    public void showToastForGTT(String event, Bundle extra) {
        if (extra == null || !event.equals(MtkTelecomManager.EVENT_GTT_MODE_CHANGED)) {
           return;
        }
        boolean gttLocal = extra.getBoolean(MtkTelecomManager.EXTRA_GTT_MODE_LOCAL);
        boolean gttRemote = extra.getBoolean(MtkTelecomManager.EXTRA_GTT_MODE_REMOTE);
        Log.d(TAG, "showToastForGTT gttLocal = " + gttLocal + " gttRemote = " + gttRemote);
        if (gttRemote && !gttLocal) {
            Log.d(TAG, "gtt local if off and gtt remote if on");
            Toast.makeText(mContext, mContext.getString(R.string.open_gtt),
                    Toast.LENGTH_LONG).show();
        } else if (!gttRemote && gttLocal) {
            Log.d(TAG, "gtt local if ON and gtt remote if off");
            Toast.makeText(mContext, mContext.getString(R.string.gtt_not_allowed),
                    Toast.LENGTH_LONG).show();
        }
    }
}