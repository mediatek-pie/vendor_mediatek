package com.mediatek.op12.dialer;

import android.content.Context;

import com.mediatek.dialer.ext.ICallDetailExtension;
import com.mediatek.dialer.ext.ICallLogExtension;
import com.mediatek.dialer.ext.OpDialerCustomizationFactoryBase;

public class Op12DialerCustomizationFactory extends OpDialerCustomizationFactoryBase {
    private Context mContext;

    public Op12DialerCustomizationFactory(Context context) {
        mContext = context;
    }

    public ICallLogExtension makeCallLogExt() {
        return new Op12CallLogExtension(mContext);
    }

    public ICallDetailExtension makeCallDetailExt() {
        return new Op12CallDetailExtension(mContext);
    }
}
