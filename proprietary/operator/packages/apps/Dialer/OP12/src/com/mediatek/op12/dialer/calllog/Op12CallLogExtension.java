package com.mediatek.op12.dialer;


import java.util.List;
import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.PorterDuff;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.CallLog.Calls;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.telephony.SubscriptionManager;
import android.util.Log;
import android.widget.HorizontalScrollView;
import android.widget.TextView;
import android.view.View;

import com.android.dialer.app.calllog.CallLogActivity;
import com.android.dialer.app.calllog.CallLogFragment;
import com.android.dialer.app.calllog.CallLogListItemViewHolder;
import com.android.dialer.compat.AppCompatConstants;
import com.android.dialer.calllogutils.CallTypeIconsView;
import com.android.ims.ImsConfig;
import com.android.ims.ImsException;
import com.android.ims.ImsManager;
import com.mediatek.dialer.ext.DefaultCallLogExtension;
import com.mediatek.dialer.util.DialerFeatureOptions;
import com.mediatek.dialer.calllog.CallLogMultipleDeleteFragment;
import com.mediatek.op12.dialer.VideoItemsManager;

public class Op12CallLogExtension extends DefaultCallLogExtension {
    private static final String TAG = "Op12CallLogExtension";
    private Context mContext;
    private VideoItemsManager mVideoItemsManager;

    public Op12CallLogExtension(Context context) {
        Log.d(TAG, "Op12CalllogExtension, context = " + context);
        mVideoItemsManager = VideoItemsManager.getInstance();
        mContext = context;
    }

    @Override
    public void onCreate(Fragment fragment, Bundle bundle) {
        Log.d(TAG, "onCreate, fragment = " + fragment);
        mVideoItemsManager.createCallLogItemsController(fragment);
    }

    @Override
    public void onDestroy(Fragment fragment) {
        Log.d(TAG, "onDestroy, fragment = " + fragment);
        mVideoItemsManager.destroyCallLogItemsController(fragment);
    }
    @Override
    public void showActions(Object obj, boolean show) {
        Log.d(TAG, "showActions, show = " + show);
        if (obj instanceof CallLogListItemViewHolder) {
            CallLogListItemViewHolder holder = (CallLogListItemViewHolder) obj;
            mVideoItemsManager.showActions(holder, show);
        }
    }

    @Override
    public void onAttachedToWindow(Fragment fragment, Object obj) {
        if (fragment == null) {
            Log.d(TAG, "onAttachedToWindow, fragment is null");
            return;
        }

        if (fragment instanceof CallLogMultipleDeleteFragment) {
            Log.d(TAG, "onAttachedToWindow, incorrect fragment");
            return;
        }

        if (obj instanceof CallLogListItemViewHolder) {
            CallLogListItemViewHolder holder = (CallLogListItemViewHolder) obj;
            mVideoItemsManager.onAttachedToController(fragment, holder);
        }
    }

    @Override
    public void onDetachedFromWindow(Object obj) {
        Log.d(TAG, "onDetachedFromWindow");
        if (obj instanceof CallLogListItemViewHolder) {
            CallLogListItemViewHolder holder = (CallLogListItemViewHolder) obj;
            mVideoItemsManager.onDetachedFromController(holder);
        }
    }
}
