package com.mediatek.op08.dialer;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.util.Log;
import android.view.View;

import com.android.dialer.app.DialtactsActivity;
import com.android.dialer.app.calllog.CallLogListItemViewHolder;
import com.android.dialer.calldetails.CallDetailsActivity;

import com.mediatek.op08.dialer.calllog.CallDetailItemsController;
import com.mediatek.op08.dialer.calllog.CallLogItemsController;

import java.util.HashMap;
import java.util.Map;

public class VideoItemsManager {
    private static final String TAG = "VideoItemsManager";

    private volatile static VideoItemsManager sVideoItemsManager;

    private Map<Fragment, CallLogItemsController> mCallLogFragmentMap
            = new HashMap<Fragment, CallLogItemsController>();

    private Map<Activity, CallDetailItemsController> mCallDetailsActivityMap
            = new HashMap<Activity, CallDetailItemsController>();

    public static VideoItemsManager getInstance() {
        if (sVideoItemsManager == null) {
            synchronized (VideoItemsManager.class) {
                if (sVideoItemsManager == null) {
                    sVideoItemsManager = new VideoItemsManager();
                }
            }
        }
        return sVideoItemsManager;
    }

    public CallDetailItemsController createCallDetailsController(
            Activity activity) {
        Log.d(TAG, "initByActivity, activity = " + activity);
        CallDetailItemsController controller = getControllerByActivity(activity);
        if (controller != null) {
            Log.d(TAG, "initByActivity, already inited.");
            return controller;
        }

        if (activity instanceof CallDetailsActivity) {
            controller = new CallDetailItemsController(activity);
            Log.d(TAG, "initByActivity, host is CallDetailsActivity");
        }

        if (controller != null) {
            mCallDetailsActivityMap.put(activity, controller);
        }
        return controller;
    }

    public void destroyCallDetailsController(Activity activity) {
        Log.d(TAG, "deInitByActivity, activity = " + activity);
        CallDetailItemsController controller = getControllerByActivity(activity);
        if (controller != null) {
            controller.clear();
            mCallDetailsActivityMap.remove(activity);
        }
    }

    public CallLogItemsController createCallLogItemsController(Fragment fragment) {
        Log.d(TAG, "createCallLogItemsController, fragment = " + fragment);
        CallLogItemsController callLogItemsController = getControllerByFragment(fragment);
        if (callLogItemsController != null) {
            return callLogItemsController;
        }

        CallLogItemsController controller = new CallLogItemsController(fragment);
        mCallLogFragmentMap.put(fragment, controller);
        return controller;
    }

    public void destroyCallLogItemsController(Fragment fragment) {
        Log.d(TAG, "destroyCallLogItemsController, fragment = " + fragment);
        CallLogItemsController controller = getControllerByFragment(fragment);
        if (controller != null) {
            controller.clear();
            mCallLogFragmentMap.remove(fragment);
        }
    }

    public CallLogItemsController getControllerByFragment(Fragment fragment) {
        return mCallLogFragmentMap.get(fragment);
    }

    public CallDetailItemsController getControllerByActivity(Activity activity) {
        return mCallDetailsActivityMap.get(activity);
    }

    public void onAttachedToController(Fragment fragment,
            CallLogListItemViewHolder holder) {
        Log.d(TAG, "onAttachedToController, fragment = " + fragment);
        setFragmenTag(fragment, holder);
        CallLogItemsController controller = getControllerByFragment(fragment);
        if (controller == null) {
            controller = createCallLogItemsController(fragment);
        }

        controller.addCallLogViewHolder(holder);
    }

    public void onDetachedFromController(CallLogListItemViewHolder holder) {
        Fragment fragment = getFragmentTag(holder);
        Log.d(TAG, "onDetachedFromController, fragment = " + fragment);
        if (fragment == null) {
            return;
        }

        CallLogItemsController controller = getControllerByFragment(fragment);
        if (controller != null) {
            controller.removeCallLogViewHolder(holder);
        }
        clearFragmentTag(holder);
    }

    public void showActions(CallLogListItemViewHolder holder, boolean show) {
        Fragment fragment = getFragmentTag(holder);
        Log.d(TAG, "showActions, fragment = " + fragment);
        if (fragment == null) {
            Log.d(TAG, "showActions, fragment == null return");
            return;
        }

        CallLogItemsController controller = getControllerByFragment(fragment);
        if (controller == null) {
            controller = createCallLogItemsController(fragment);
        }
        controller.showActions(holder, show);
    }

    public void customizeVideoItem(View view, int type, String number) {
        Activity activity = getActivityByView(view);
        Log.d(TAG, "customizeVideoItem, activity = " + activity);

        if (activity != null) {
            CallDetailItemsController controller = getControllerByActivity(activity);
            if (controller == null) {
                controller = createCallDetailsController(activity);
            }

            controller.customizeVideoItem(view, type, number);
        }
    }

    private Activity getActivityByView(View view) {
        Context cnx = view.getContext();

        if (cnx instanceof Activity) {
            return (Activity) cnx;
        }
        return null;
    }

    private void setFragmenTag(Fragment fragment,
            CallLogListItemViewHolder holder) {
        if (holder.rootView != null) {
            holder.rootView.setTag(fragment);
            Log.d(TAG, "setFragmenTag");
        }
    }

    private void clearFragmentTag(CallLogListItemViewHolder holder) {
        if (holder.rootView != null) {
            holder.rootView.setTag(null);
            Log.d(TAG, "clearFragmentTag");
        }
    }

    private Fragment getFragmentTag(CallLogListItemViewHolder holder) {
        if (holder.rootView != null) {
            return (Fragment) holder.rootView.getTag();
        }
        return null;
    }
}
