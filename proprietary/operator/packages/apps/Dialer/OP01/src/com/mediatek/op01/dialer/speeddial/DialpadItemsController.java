package com.mediatek.op01.dialer.speeddial;

import android.app.Activity;

import com.mediatek.op01.dialer.ItemsController;

public class DialpadItemsController extends ItemsController{

    public DialpadItemsController(Activity activity) {
        super(activity);
    }

    @Override
    protected boolean isTypeVideo(int type) {
        return (type == SHORTCUT_MAKE_VIDEO_CALL);
    }
}