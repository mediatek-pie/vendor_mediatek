package com.mediatek.op01.dialer.calllog;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.dialer.app.calllog.CallLogListItemViewHolder;
import com.mediatek.dialer.calllog.CallLogMultipleDeleteFragment;
import com.mediatek.dialer.ext.DefaultCallLogExtension;
import com.mediatek.op01.dialer.VideoItemsManager;

public class Op01CallLogExtension extends DefaultCallLogExtension {
    private static final String TAG = "Op01CallLogExtension";
    VideoItemsManager mVideoItemsManager;

    /**
     * constructor
     * @param context the current context
     */
    public Op01CallLogExtension(Context context) {
        mVideoItemsManager = VideoItemsManager.getInstance();
    }

    @Override
    public void onCreate(Fragment fragment, Bundle bundle) {
        mVideoItemsManager.initByFragment(fragment);
    }

    @Override
    public void onDestroy(Fragment fragment) {
        mVideoItemsManager.deInitByFragment(fragment);
    }

    /**
     * for op01/op02/op09
     * plug-in to create account info in calllog fragment
     * @param fragment the current fragment
     * @param view the current view
     */
    @Override
    public void onViewCreated(Fragment fragment, View view) {
        mVideoItemsManager.initByFragment(fragment);
    }

    @Override
    public void showActions(Object obj, boolean show) {
        Log.d(TAG, "showActions, show = " + show);
        if (obj instanceof CallLogListItemViewHolder) {
            CallLogListItemViewHolder holder = (CallLogListItemViewHolder) obj;
            mVideoItemsManager.showActions(holder, show);
        }
    }

    @Override
    public void onAttachedToWindow(Fragment fragment, Object obj) {
        if (fragment == null) {
            Log.d(TAG, "onAttachedToWindow, fragment is null");
            return;
        }

        if (fragment instanceof CallLogMultipleDeleteFragment) {
            Log.d(TAG, "onAttachedToWindow, incorrect fragment");
            return;
        }

        if (obj instanceof CallLogListItemViewHolder) {
            CallLogListItemViewHolder holder = (CallLogListItemViewHolder) obj;
            mVideoItemsManager.onAttachedToController(fragment, holder);
        }
    }

    @Override
    public void onDetachedFromWindow(Object obj) {
        Log.d(TAG, "onDetachedFromWindow");
        if (obj instanceof CallLogListItemViewHolder) {
            CallLogListItemViewHolder holder = (CallLogListItemViewHolder) obj;
            mVideoItemsManager.onDetachedFromController(holder);
        }
    }

    @Override
    public boolean isNeedPreShowPrimaryUi() {
        return false;
    }
}
