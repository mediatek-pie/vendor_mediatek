package com.mediatek.op01.dialer.calllog;

import android.app.Activity;
import android.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;

import com.android.dialer.app.calllog.CallLogListItemViewHolder;
import com.mediatek.op01.dialer.ItemsController;
import com.mediatek.op01.dialer.PhoneStateUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CallLogItemsController
    extends ItemsController{
    private static final String TAG = "CallLogItemsController";

    private Fragment mFragment;
    private List<CallLogListItemViewHolder> mArrayListItems =
            new ArrayList<CallLogListItemViewHolder>();

    private CallLogListItemViewHolder mCallLogViewHolder;

    public CallLogItemsController(Fragment fragment) {
        super(fragment);
        mFragment = fragment;
    }

    @Override
    protected boolean isTypeVideo(int type) {
        return (type == SHORTCUT_MAKE_VIDEO_CALL);
    }

    @Override
    public void reInitInfo(Fragment fragment) {
        if (mActivity != null) {
            Log.d(TAG, "reInitInfo, activity is not null");
            return;
        }

        super.reInitInfo(mFragment);
    }

    @Override
    public void addCallLogViewHolder(CallLogListItemViewHolder holder) {
        if (PhoneStateUtils.getPhoneState() != TelephonyManager.CALL_STATE_IDLE) {
            holder.primaryActionButtonView.setVisibility(View.GONE);
        }
        mArrayListItems.add(holder);
    }

    @Override
    public void removeCallLogViewHolder(CallLogListItemViewHolder holder) {
        Iterator<CallLogListItemViewHolder> it = mArrayListItems.iterator();
        while(it.hasNext()) {
            CallLogListItemViewHolder itHolder = (CallLogListItemViewHolder) it.next();
            if (itHolder== holder) {
                Log.d(TAG, "onDetachedFromWindow, remove the weak reference");
                it.remove();
            }
        }
    }

    @Override
    protected void updateCallListViewHolders(int state) {
        boolean isVisible = (state == TelephonyManager.CALL_STATE_IDLE);
        Log.d(TAG, "updateCallListViewHolders, isVisible = " + isVisible);

        Iterator<CallLogListItemViewHolder> it = mArrayListItems.iterator();
        while(it.hasNext()) {
            CallLogListItemViewHolder holder = (CallLogListItemViewHolder) it.next();
            Log.d(TAG, "updateCallListViewHolders, callbackAction = " + holder.callbackAction);
            if (holder != null
                && holder.callbackAction == CALLBACK_TYPE_IMS_VIDEO
                && holder.primaryActionButtonView != null) {
                if (isVisible) {
                    holder.primaryActionButtonView.setVisibility(View.VISIBLE);
                } else {
                    holder.primaryActionButtonView.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    public void showAction(CallLogListItemViewHolder holder, boolean show) {
        if (holder != null && holder.confCallNumbers != null) {
            Log.d(TAG, "showActions, conference calllog.");
            return;
        }

        if (show) {
            mCallLogViewHolder = holder;
        } else {
            if (mCallLogViewHolder != null) {
                if (mCallLogViewHolder == holder) {
                    mCallLogViewHolder = null;
                    super.customizeVideoItem(null, SHORTCUT_MAKE_VIDEO_CALL);
                }
            }
        }

        boolean isIdle = (PhoneStateUtils.getPhoneState() == TelephonyManager.CALL_STATE_IDLE);
        customizeActionVisibility(holder, show);

        Log.d(TAG, "showActions, callbackAction = " + holder.callbackAction);
        if (!isIdle && holder.callbackAction == CALLBACK_TYPE_IMS_VIDEO) {
            holder.primaryActionButtonView.setVisibility(View.GONE);
        }
    }

    private void customizeActionVisibility(CallLogListItemViewHolder holder, boolean show) {
        if (!show) {
            return;
        }

        if (holder == null || holder.videoCallButtonView == null) {
            return;
        }

        if (holder.callbackAction != CALLBACK_TYPE_VOICE) {
            return;
        }

        if (holder.videoCallButtonView.getVisibility() == View.VISIBLE) {
            super.customizeVideoItem(holder.videoCallButtonView, SHORTCUT_MAKE_VIDEO_CALL);
            Log.d(TAG, "customizeActionVisibility, set visibility as VISIBLE.");
        }
    }

    @Override
    public void clear() {
        super.clear();
        mFragment = null;
        mCallLogViewHolder = null;
        mArrayListItems.clear();
        mArrayListItems = null;
    }
}
