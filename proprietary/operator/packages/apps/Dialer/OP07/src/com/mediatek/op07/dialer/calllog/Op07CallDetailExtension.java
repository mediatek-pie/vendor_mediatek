package com.mediatek.op07.dialer.calllog;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.dialer.calldetails.CallDetailsActivity;

import com.mediatek.dialer.ext.DefaultCallDetailExtension;
import com.mediatek.op07.dialer.VideoItemsManager;

public class Op07CallDetailExtension extends DefaultCallDetailExtension {
    private static final String TAG = "Op07CallDetailExtension";

    private Context mPluginContext = null;
    private VideoItemsManager mVideoItemsManager;

    /**
     * constructor
     * @param context the current context
     */
    public Op07CallDetailExtension(Context context) {
        mVideoItemsManager = VideoItemsManager.getInstance();
        mPluginContext = context;
    }

    /**
     * for op01
     * @param set actionbar
     */
    @Override
    public void onCreate(Object obj, Object tool) {
        if (obj instanceof CallDetailsActivity) {
            CallDetailsActivity activity = (CallDetailsActivity) obj;
            mVideoItemsManager.createCallDetailsController(activity);
        }
    }

    @Override
    public void onDestroy(Object obj) {
        if (obj instanceof CallDetailsActivity) {
            CallDetailsActivity activity = (CallDetailsActivity) obj;
            mVideoItemsManager.destroyCallDetailsController(activity);
        }
    }

    @Override
    public void setCallbackAction(ImageView callbackButton, int action, String number) {
        mVideoItemsManager.customizeVideoItem(callbackButton, action, number);
    }
}
