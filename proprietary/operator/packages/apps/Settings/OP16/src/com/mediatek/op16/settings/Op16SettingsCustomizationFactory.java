package com.mediatek.op16.settings;

import android.content.Context;

import com.mediatek.settings.ext.IWfcSettingsExt;
import com.mediatek.settings.ext.OpSettingsCustomizationFactoryBase;

public class Op16SettingsCustomizationFactory extends OpSettingsCustomizationFactoryBase {
    private Context mContext;

    public Op16SettingsCustomizationFactory(Context context) {
        super(context);
        mContext = context;
    }

    public IWfcSettingsExt makeWfcSettingsExt() {
        return new Op16WfcSettingsExt(mContext);
    }
}
