/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
package com.mediatek.op18.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiConfiguration;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class OP18WifiTetherSettingsUtil {
    private static final String TAG = "OP18WifiTetherSettingsUtil";
    private static final String IS_CHECKED = "checkboxstatus";
    private static final String CHANNEL_INDEX = "channelindex";
    private static int AUTO_SELECT = 0;
    private static final int AP_BAND_2GHZ = 0;
    private static final int AP_BAND_5GHZ = 1;

    private Context mContext;
    private CheckBox mAdvanceOption;
    private Spinner mSpinner;
    private TextView mTextView;
    private ArrayAdapter <CharSequence> mChannelAdapter;
    private int indexToBandArr[] = {0, 36, 40, 44, 48, 149, 153, 157, 161, 165};

    private static int mChannelIndex = AUTO_SELECT;
    private boolean mInit = false;

    /**
     * Plugin constructor.
     * @param context hostapp context
     */
    public OP18WifiTetherSettingsUtil(Context context) {
        mContext = context;
    }

    /**
     * Customize Wifi ap dialog view to add apChannel selection option.
     * @param context The parent context
     * @param view parent layout view
     * @param config wificonfiguration object
     */
    public void customizeView(Context context, View view, WifiConfiguration config) {

        mContext.setTheme(android.R.style.Theme_Material_Settings);

        View mView = (LinearLayout) LayoutInflater.from(mContext)
                            .inflate(R.layout.wifi_channels, null);

        mSpinner = ((Spinner) mView.findViewById(R.id.choose_channel));
        mAdvanceOption = ((CheckBox) mView.findViewById(R.id.show_apchannel_option));
        mTextView = ((TextView) mView.findViewById(R.id.show_ap_channels));
        Log.i(TAG, "mAdvanceOption " + mAdvanceOption + ", config.apChannel" +
                config.apChannel + "config.apBand = " + config.apBand);

        setChannelAdapter(config.apBand);
        mChannelIndex = getIndexForApChannel(config);

        if (load(IS_CHECKED)) {
            mAdvanceOption.setChecked(true);
            mSpinner.setVisibility(View.VISIBLE);
            mTextView.setVisibility(View.VISIBLE);
            mSpinner.setSelection(mChannelIndex);
        }

        mAdvanceOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, String.format("checkbox onClick," +
                    "isSelected: %s, identityHashCode: %s", mAdvanceOption.isChecked(),
                     System.identityHashCode(mAdvanceOption)));
                save(mAdvanceOption.isChecked(), IS_CHECKED);
                if (((CheckBox) mAdvanceOption).isChecked()) {
                    Log.i(TAG, "setVisibility true");
                    mSpinner.setVisibility(View.VISIBLE);
                    mTextView.setVisibility(View.VISIBLE);
                    mSpinner.setSelection(AUTO_SELECT);
                } else {
                    Log.i(TAG, "setVisibility false");
                    mSpinner.setVisibility(View.GONE);
                    mTextView.setVisibility(View.GONE);
                    mChannelIndex = AUTO_SELECT;
                }
            }
        });

        mSpinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position,
                            long id) {
                        if (!mInit) {
                            mInit = false;
                            mChannelIndex = position;
                            Log.i(TAG, "onItemSelected apChannelIndex : " + mChannelIndex);
                            mSpinner.setSelection(mChannelIndex);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                }
        );
        ((ViewGroup) (((ViewGroup) view).getChildAt(0))).addView(mView);
    }

    /**
     * Update wifiConfiguration with selected apChannel information.
     * @param config wificonfiguration object
     */
    public void updateConfig(WifiConfiguration config) {
        int apChannel = AUTO_SELECT;
        apChannel = mChannelIndex;
        if (config.apBand == AP_BAND_5GHZ) {
            apChannel = indexToBandArr[mChannelIndex];
        }
        Log.i(TAG, "setBroadcastChannelConfig: " + "apBand = " + config.apBand
                + "," + mChannelIndex + ", apChannel =" + apChannel);
        config.apChannel = apChannel;
    }

    /**
     * Set update ApChannel spinner when band is changed.
     * @param apBand selected AP band
     * @param needToSet this is to check if different band is selected
     */
    public void setApChannel(int apBand, boolean needToSet) {
        Log.i(TAG, "setApChannel for band: " + apBand + "needToSet: " + needToSet);
        if (needToSet) {
            mInit =  true;
            setChannelAdapter(apBand);
            mSpinner.setSelection(AUTO_SELECT);
            mChannelIndex = AUTO_SELECT;
            mInit = false;
        }
    }

    private void setChannelAdapter(int apBand) {
        Log.i(TAG, "setChannelAdapter for apBand = " + apBand);
        if (apBand == AP_BAND_2GHZ) {
            mChannelAdapter  = ArrayAdapter.createFromResource(mContext,
                        R.array.wifi_channel_config_2_4g, android.R.layout.simple_spinner_item);
        } else {
            mChannelAdapter  = ArrayAdapter.createFromResource(mContext,
                        R.array.wifi_channel_config_5g,
                        android.R.layout.simple_spinner_item);
        }
        mChannelAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(mChannelAdapter);
    }

    private int getIndexForApChannel(WifiConfiguration config) {
        int index = AUTO_SELECT;
        if (config.apBand == AP_BAND_2GHZ) {
            index = config.apChannel;
        }
        switch (config.apChannel) {
            case 36:
                index = 1;
                break;
            case 40:
                index = 2;
                break;
            case 44:
                index = 3;
                break;
            case 48:
                index = 4;
                break;
            case 149:
                index = 5;
                break;
            case 153:
                index = 6;
                break;
            case 157:
                index = 7;
                break;
            case 161:
                index = 8;
                break;
            case 165:
                index = 9;
                break;
            default:
                break;
        }
        Log.i(TAG, "getIndexForApChannel apChannel =" + config.apChannel +
                ", index = " + index);
        return index;
    }

    private void save(final boolean val , String key) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                key, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, val);
        editor.commit();
    }

    private boolean load(String key) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                key, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(key, false);
    }
}
