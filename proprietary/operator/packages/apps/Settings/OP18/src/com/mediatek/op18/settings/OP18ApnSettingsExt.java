/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2014. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op18.settings;

import android.content.Context;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.util.Log;

import com.android.internal.telephony.PhoneConstants;

import com.mediatek.internal.telephony.MtkSubscriptionManager;

import com.mediatek.internal.telephony.RadioCapabilitySwitchUtil;
import com.mediatek.settings.ext.DefaultApnSettingsExt;
import com.mediatek.telephony.MtkTelephonyManagerEx;

/**
 * Plugin for APN Settings.
 */
public class OP18ApnSettingsExt extends DefaultApnSettingsExt {

    private static final String TAG = "OP18ApnSettingsExt";
    private static final String KEY_TETHERED_IPV6 = "tethered_ipv6";
    public static final String SIM_IMPI = "simImpi";
    private Context mContext;
    private static final String TYPE_IA_IMS = PhoneConstants.APN_TYPE_IA + ","
            + PhoneConstants.APN_TYPE_IMS;

    /**
     * Constructor of plugin.
     * @param context context
     */
    public OP18ApnSettingsExt(Context context) {
        super();
        mContext = context;
    }

    /**
     * To get mcc&mnc from IMPI, see TelephonyManagerEx.getIsimImpi().
     * @param defaultValue default value
     * @param phoneId phoneId used to get IMPI.
     * @return MccMnc from IMPI or Default
     */
    @Override
    public String getOperatorNumericFromImpi(String defaultValue, int phoneId) {
        final String mccTag = "mcc";
        final String mncTag = "mnc";
        final int mccLength = 3;
        final int mncLength = 3;
        boolean isImsSupportBySim = false;
        Log.d(TAG, "getOperatorNumbericFromImpi got default mccmnc: " + defaultValue);
        String[] imsMccMncList = mContext.getResources().getStringArray(R.array.ims_mcc_mnc_list);
        String impi = null;
        int masterPhoneId = RadioCapabilitySwitchUtil.getMainCapabilityPhoneId();
        Log.d(TAG, "Impi requested by phoneId: " + phoneId);
        Log.d(TAG, "masterPhoneId:" + masterPhoneId);
       /* if (masterPhoneId != phoneId) {
            Log.d(TAG, "Request from Secondry Sim So Returning default mccmnc: " + defaultValue);
            return defaultValue;
        }*/
        impi = MtkTelephonyManagerEx.getDefault()
                .getIsimImpi(MtkSubscriptionManager.getSubIdUsingPhoneId(phoneId));
        if (impi == null  || impi.equals("")) {
            Log.d(TAG, "Returning default mccmnc: " + defaultValue);
            return defaultValue;
        }
        /*try {
            impi = Settings.Global.getString(mContext.getContentResolver(), SIM_IMPI);
        } catch (RuntimeException e) {
            Log.e(TAG, "exception:" + e);
            Log.d(TAG, "Returning default mccmnc: " + defaultValue);
            return defaultValue;
        }*/
        int mccPosition = impi.indexOf(mccTag);
        int mncPosition = impi.indexOf(mncTag);
        if (mccPosition == -1 || mncPosition == -1) {
            Log.d(TAG, "Returning default mccmnc: " + defaultValue);
            return defaultValue;
        }
        String masterMccMnc = impi.substring(mccPosition + mccTag.length(), mccPosition
                + mccTag.length() + mccLength) + impi.substring(mncPosition + mncTag.length(),
                mncPosition + mncTag.length() + mncLength);
        Log.d(TAG, "master MccMnc: " + masterMccMnc);
        if (masterMccMnc == null || masterMccMnc.equals("")) {
            Log.d(TAG, "Returning default mccmnc: " + defaultValue);
            return defaultValue;
        }
        for (String mccMnc : imsMccMncList) {
            if (masterMccMnc.equals(mccMnc)) {
                Log.d(TAG, "mccMnc matched:" + mccMnc);
                isImsSupportBySim = true;
                break;
            }
        }
        Log.d(TAG, "isImsSupportBySim:" + isImsSupportBySim);
        if (isImsSupportBySim) {
            Log.d(TAG, "Returning mccmnc from IMPI: " + masterMccMnc);
            return masterMccMnc;
        } else {
          Log.d(TAG, "Returning default mccmnc: " + defaultValue);
          return defaultValue;
        }
    }

    /** Customize tether APN settings.
     * @param root PreferenceScreen
     * @return
     */
    @Override
    public void customizeTetherApnSettings(PreferenceScreen root) {
        if (root != null) {
            Preference tetherProtocolPreference
                    = (Preference) root.findPreference(KEY_TETHERED_IPV6);
            if (tetherProtocolPreference != null) {
                root.removePreference(tetherProtocolPreference);
                Log.d(TAG, "removed tethering preference");
            }
        }
    }
}
