/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op18.settings;

import android.content.Context;

import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.DropDownPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.util.Log;

import com.android.settings.wifi.tether.WifiTetherBasePreferenceController;
import com.android.settings.wifi.tether.WifiTetherBasePreferenceController
        .OnTetherConfigUpdateListener;

public class OP18WifiTetherPreferenceController extends WifiTetherBasePreferenceController {

    private String TAG = "OP18WifiTetherPreferenceController";

    private static final String KEY_ADVANCE_OPTION = "advance_option";
    private static final String KEY_BROADCAST_CHANNEL = "broadcast_channel";
    private static final String KEY_AP_BAND = "wifi_tether_network_ap_band";
    private Context mContext;
    private Context mOP18Context;

    private final String AUTO_SELECT = "Auto-select";
    private static final int AP_BAND_2GHZ = 0;
    private static final int AP_BAND_5GHZ = 1;
    private static final int AP_BAND_2_5GHZ = -1;

    private WifiManager mWifiManager;
    private WifiConfiguration mWifiConfig;

    private CheckBoxPreference mAdvanceOptionPref;
    private DropDownPreference mBroadcastChannelPref;
    private PreferenceScreen mPrefScreen;
    private static int mBandIndex = 0;
    private static int mChannelValue = 0;

    public OP18WifiTetherPreferenceController(Context context, Context op18Context,
            OnTetherConfigUpdateListener listener) {
        super(context, listener);
        mContext = context;
        mOP18Context = op18Context;
        mWifiManager = ((WifiManager) mContext.getSystemService(Context.WIFI_SERVICE));
        Log.i(TAG, "mContext = " + mContext + ", mOP18Context = " + mOP18Context);
    }

    /**
     * Displays preference in this controller.
     */
    public void displayPreference(PreferenceScreen screen) {
        Log.i(TAG, "displayPreference");
        if (screen.findPreference(KEY_BROADCAST_CHANNEL) == null) {
            initPreference(screen);
        }
    }

    private void initPreference(PreferenceScreen screen) {
        Log.i(TAG, "initPreference");
        mPrefScreen = screen;
        mWifiConfig = mWifiManager.getWifiApConfiguration();
        if (mWifiConfig != null) {
            mBandIndex = mWifiConfig.apBand;
            mChannelValue = mWifiConfig.apChannel;
            Log.i(TAG, "mBandIndex " + mBandIndex + ", mWifiConfig.apChannel = "
                    + mWifiConfig.apChannel);
        }

        mBroadcastChannelPref = new DropDownPreference(screen.getPreferenceManager()
                .getContext());
        mBroadcastChannelPref.setKey(KEY_BROADCAST_CHANNEL);
        mBroadcastChannelPref.setTitle(mOP18Context.getString(R.string.wifi_ap_channels));
        mBroadcastChannelPref.setValue(String.valueOf(mChannelValue));
        if (mBandIndex == AP_BAND_2GHZ) {
            mBroadcastChannelPref.setEntries(mOP18Context.getResources()
                .getTextArray(R.array.wifi_channel_config_2_4g));
            mBroadcastChannelPref.setEntryValues(mOP18Context.getResources()
                .getTextArray(R.array.wifi_channel_config_2_4g_values));
        }
        else if (mBandIndex == AP_BAND_5GHZ) {
            mBroadcastChannelPref.setEntries(mOP18Context.getResources()
                .getTextArray(R.array.wifi_channel_config_5g));
            mBroadcastChannelPref.setEntryValues(mOP18Context.getResources()
                .getTextArray(R.array.wifi_channel_config_5g_values));
        }
        if (mChannelValue == 0) {
            mBroadcastChannelPref.setSummary(AUTO_SELECT);
        } else {
            mBroadcastChannelPref.setSummary(String.valueOf(mChannelValue));
        }

        /// M: [ALPS04057600] DropDown Pref should be enable for both bands @{
        mBroadcastChannelPref.setVisible(true);
        ///@}
        mBroadcastChannelPref.setOnPreferenceChangeListener(this);
        screen.addPreference(mBroadcastChannelPref);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        Log.d(TAG, "onPreferenceChange: newValue" + newValue.toString());
        updateChannelDisplay((DropDownPreference) preference,
                Integer.parseInt(newValue.toString()));
        return true;
    }

    public void onPrefChangeNotifyController(String pref_key, Object newValue) {
        int bandIndex = Integer.parseInt(newValue.toString());
        Log.d(TAG, "onPrefChangeNotifyController: pref_key" + pref_key + ", bandIndex: " +
                bandIndex);
        /// M: [ALPS04057600] DropDown Pref should be visible for both bands @{
        resetChannelPreference(true);
        ///@}
        if (mPrefScreen != null) {
            if (bandIndex == AP_BAND_2GHZ) {
                mBroadcastChannelPref.setEntries(mOP18Context.getResources()
                    .getTextArray(R.array.wifi_channel_config_2_4g));
                mBroadcastChannelPref.setEntryValues(mOP18Context.getResources()
                    .getTextArray(R.array.wifi_channel_config_2_4g_values));
            }
            else if (bandIndex == AP_BAND_5GHZ || bandIndex == AP_BAND_2_5GHZ) {
                mBroadcastChannelPref.setEntries(mOP18Context.getResources()
                    .getTextArray(R.array.wifi_channel_config_5g));
                mBroadcastChannelPref.setEntryValues(mOP18Context.getResources()
                    .getTextArray(R.array.wifi_channel_config_5g_values));
            }
        }
    }

    private void resetChannelPreference(boolean visible) {
        mChannelValue = 0;
        mBroadcastChannelPref.setValue(String.valueOf(mChannelValue));
        mBroadcastChannelPref.setSummary(AUTO_SELECT);
        mBroadcastChannelPref.setVisible(visible);
    }

    private void updateChannelDisplay(DropDownPreference preference, int channel) {
        Log.d(TAG, "updateChannelDisplay : for channel = " + channel);
        if (channel != 0) {
            mBroadcastChannelPref.setSummary(String.valueOf(channel));
            mWifiConfig = mWifiManager.getWifiApConfiguration();
            if (mWifiConfig != null) {
                mWifiConfig.apChannel = channel;
            }
            mWifiManager.setWifiApConfiguration(mWifiConfig);
        } else {
            mBroadcastChannelPref.setSummary(AUTO_SELECT);
        }
        mBroadcastChannelPref.setValue(String.valueOf(channel));
        mChannelValue = channel;
    }

    @Override
    public String getPreferenceKey() {
        return KEY_BROADCAST_CHANNEL;
    }

    @Override
    public boolean isAvailable() {
        return false;
    }

    @Override
    public void updateDisplay() {
        Log.d(TAG, "updateDisplay");
    }
}
