package com.mediatek.op06.settings;

import android.content.Context;
import android.os.PersistableBundle;
import android.support.v7.preference.ListPreference;
import android.telephony.CarrierConfigManager;
import android.telephony.SubscriptionManager;
import android.util.Log;

import mediatek.telephony.MtkCarrierConfigManager;

/**
 * Class to support operator customizations for WFC settings.
 */
public class Op06WfcSettings {

    private static final String TAG = "Op06WfcSettings";
    private static final int WFC_USER_MODE_CHANGE_TYPE = 1;

    private static Op06WfcSettings sWfcSettings = null;
    private Context mPluginContext;
    private WfcSwitchController mController;

    private Op06WfcSettings(Context context) {
       mPluginContext = context;
       mController = WfcSwitchController.getInstance(context);
    }

    /** Returns instance of Op06WfcSettings.
         * @param context context
         * @return Op06WfcSettings
         */
    public static Op06WfcSettings getInstance(Context context) {

        if (sWfcSettings == null) {
            sWfcSettings = new Op06WfcSettings(context);
        }
        return sWfcSettings;
    }

    /** Customize WFC pref as per operator requirement,
         * i.e. add WFC switch instead of AOSP pref screen for UK & DE
         * @param context context
         * @param preferenceScreen preferenceScreen
         * @return
         */
    public void customizedWfcPreference(Context context, Object preferenceScreen) {
        mController.customizedWfcPreference(context, preferenceScreen);
    }

     /** Customize WFC mode by adding wifi-only option for selected countries
        * @param wfcModePref wfcModePref
        * @param wfcEnabled wfcEnabled
        * @return
        */
    public void updateWfcModePreference(ListPreference wfcModePref, boolean wfcEnabled) {
        /* If switch is off, do not update. It will be updated when switch will be turned ON */
        if (!wfcEnabled) {
            return;
        }
        int settingsUxType = -1;
        CarrierConfigManager configMgr = (CarrierConfigManager) mPluginContext
                .getSystemService(Context.CARRIER_CONFIG_SERVICE);
        PersistableBundle b =
                configMgr.getConfigForSubId(SubscriptionManager.getDefaultDataSubscriptionId());
        if (b != null) {
            settingsUxType = b.getInt(MtkCarrierConfigManager.MTK_KEY_WFC_SETTINGS_UX_TYPE_INT);
        }
        Log.d(TAG, "settingsUxType:" + settingsUxType);
        // TODO: how to implement this
        // Dismiss Dialog if showing
        //Dialog d = wfcModePref.getDialog();
        //if (d != null && d.isShowing()) {
        //    d.dismiss();
        //}
        CharSequence[] choices;
        CharSequence[] values;
        if (settingsUxType == WFC_USER_MODE_CHANGE_TYPE) {
            choices = mPluginContext.getResources()
                    .getStringArray(R.array.wifi_calling_mode_choices);
            values = mPluginContext.getResources()
                    .getStringArray(R.array.wifi_calling_mode_values);
        } else {
            choices = mPluginContext.getResources()
                    .getStringArray(R.array.aosp_wifi_calling_mode_choices);
            values = mPluginContext.getResources()
                    .getStringArray(R.array.aosp_wifi_calling_mode_values);
        }
        wfcModePref.setEntries(choices);
        wfcModePref.setEntryValues(values);
    }

    /** Returns instance of OP18WfcSettings.
     * @param preferenceScreen preferenceScreen of WFC settings
     * @return
     */
    public void removeWfcPreference(Object preferenceScreen) {
        mController.removeWfcPreference(preferenceScreen);
    }
}
