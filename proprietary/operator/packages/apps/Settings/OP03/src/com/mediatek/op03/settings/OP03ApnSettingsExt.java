package com.mediatek.op03.settings;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Telephony;
import android.support.v7.preference.Preference;
import android.util.Log;

import com.mediatek.settings.ext.DefaultApnSettingsExt;

/** OP03 Plugin implemenation of ApnSettings.
 */
public class OP03ApnSettingsExt extends DefaultApnSettingsExt {

    private static final String TAG = "OP03ApnSettingsExt";
    private static final String TETHER_TYPE = "tethering";
    private static final String APN_TYPE = "apn_type";

    private static final String ORANGE_CARD_1 = "20801";
    private static final String ORANGE_CARD_2 = "23430";
    private static final String ORANGE_CARD_3 = "23431";
    private static final String ORANGE_CARD_4 = "23432";
    private static final String ORANGE_CARD_5 = "23433";
    private static final String ORANGE_CARD_6 = "21403";

    private Context mContext;

    /**
     * Constructor.
     * @param context context
     */
    public OP03ApnSettingsExt(Context context) {
        super();
        mContext = context;
    }

    /**
     * Decide whether to allow user to edit pre-configured apns.
     * @param type apn type
     * @param apn apn
     * @param numeric mcc+mnc
     * @param sourcetype sourceType
     * @return boolean
     */
    @Override
    public boolean isAllowEditPresetApn(String type , String apn, String numeric, int sourcetype) {
        Log.d("@M_" + TAG, "isAllowEditPresetApn");
        boolean isAllowEdit = true;
        isAllowEdit = !TETHER_TYPE.equals(type);
        return isAllowEdit && !isOrangeCard(numeric) || sourcetype != 0;
    }

    /**
     * Decide whether given apnType is selectable or not.
     * @param type Apn type
     * @return boolean
     */
    @Override
    public boolean isSelectable(String type) {
        boolean isSelect = false;
        if (TETHER_TYPE.equals(type)) {
            isSelect = false; //mIsTetherApn;
        } else {
            isSelect = super.isSelectable(type);
        }
        return isSelect;
    }

    /**
     * set APN type preference state (enable or disable), called at update UI in ApnEditor.
     * @param preference The preference to set state
     * @param apnType apnType
     */
    @Override
    public void setApnTypePreferenceState(Preference preference, String apnType) {
        if (TETHER_TYPE.equals(apnType)) {
            Log.d("@M_" + TAG, "setPreferenceState:false");
            preference.setEnabled(false);
        }
    }

    /**.
     * Called when ApnEditor insert a new APN item
     * @param defaultUri the Uri host used
     * @param context context
     * @param intent intent
     * @return inserted Uri, return a customized one if necessary or just return the default Uri
     */
    @Override
    public Uri getUriFromIntent(Uri defaultUri, Context context, Intent intent) {
       Log.d("@M_" + TAG, "getUriFromIntent , intent = " + intent);
       ContentValues value = new ContentValues();
       String strType = intent.getStringExtra(APN_TYPE);
       if ((strType != null) && (strType.equals(TETHER_TYPE))) {
           value.put(Telephony.Carriers.TYPE, TETHER_TYPE);
       }
       return context.getContentResolver().insert(intent.getData(), value);
    }

    /**.
     * Called when host forge the APN type array
     * @param defaultApnArray the default array host used
     * @param context context
     * @param apnType apnType
     * @return a customized array if necessary, or just return the defaultApnArray
     */
    @Override
    public String[] getApnTypeArray(String[] defaultApnArray, Context context, String apnType) {
       Log.d("@M_" + TAG, "getApnTypeArray : orange array");
       String[] resArray = null;
       boolean isTether = TETHER_TYPE.equals(apnType);
       if (isTether) {
            resArray = mContext.getResources().getStringArray(R.array
                    .apn_type_orange_tethering_only);
       } else {
           resArray = mContext.getResources().getStringArray(R.array.apn_type_orange);
       }
       return resArray;
    }

    private boolean isOrangeCard(String numeric) {
        return (ORANGE_CARD_1.equals(numeric) || ORANGE_CARD_5.equals(numeric)
                || ORANGE_CARD_6.equals(numeric));
    }

    private boolean isOrangeCardForTether(String numeric) {
        return (ORANGE_CARD_1.equals(numeric) || ORANGE_CARD_2.equals(numeric)
                || ORANGE_CARD_3.equals(numeric) || ORANGE_CARD_4.equals(numeric));
    }
}

