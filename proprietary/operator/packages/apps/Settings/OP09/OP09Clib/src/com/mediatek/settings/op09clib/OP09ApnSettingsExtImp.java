/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.settings.op09clib;

import android.content.Context;
import android.os.SystemProperties;
import android.provider.Settings;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.EditTextPreference;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.android.internal.telephony.PhoneConstants;
import com.android.ims.ImsManager;
import com.android.settings.network.ApnPreference;

import com.mediatek.ims.internal.MtkImsManager;
import com.mediatek.settings.ext.DefaultApnSettingsExt;
import com.mediatek.telephony.MtkTelephonyManagerEx;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * APN CT feature..
 */
public class OP09ApnSettingsExtImp extends DefaultApnSettingsExt {
    private static final String TAG = "OP09ClibApnSettingsExtImp";
    private Context mContext;
    private static final String CT_NUMERIC_1 = "46011";
    private static final String CT_NUMERIC_2 = "46003";
    private static final String CT_NUMERIC_3 = "45400";
    private static final String CT_NUMERIC_4 = "45431";
    private static final String CT_NUMERIC_5 = "46001";
    private static final String CT_NUMERIC_6 = "45403";
    private static final String CT_NUMERIC_7 = "45404";

    private static final String CT_NUMERIC_MVNO_3 = "CTExcel";
    private static final String CT_NUMERIC_MVNO_4 = "中国电信";
    private static final String CT_NUMERIC_MVNO_5 = "China Telecom";

    private static final int SOURCE_TYPE_DEFAULT = 0;
    private static final String IMS_TYPE = "IMS";
    private static final String CTNET_TYPE = "ctnet";
    private static final String CTWAP_TYPE = "ctwap";

    private static final String[] CT_NUMERIC = { "45502", "45507", "46003", "46011", "46012",
    "46013", "20404" };
    public static final String ENHANCED_4G_MODE_ENABLED_SIM2 = "volte_vt_enabled_sim2";

    /**
    * Constructor method.
    * @param context plugin context
    */
    public OP09ApnSettingsExtImp(Context context) {
        super();
        mContext = context;
    }

    /**
     * Customize the unselected APN.
     * @param type apn="IMS", apn="ctnet" apn="ctwap".
     * @param mnoApnList the unselectable mno apn list.
     * @param mvnoApnList the unselectable mvno apn list.
     * @param subId the subscription ID.
     */
    @Override
    public void customizeUnselectableApn(String type,
            String mvnoType,
            String mvnoMatchData,
            Object mnoApnListObject,
            Object mvnoApnListObject,
            int subId) {
        ArrayList<ApnPreference> mnoApnList = (ArrayList<ApnPreference>)mnoApnListObject;
        ArrayList<ApnPreference> mvnoApnList = (ArrayList<ApnPreference>)mvnoApnListObject;
        MtkTelephonyManagerEx telephonyManagerEx = MtkTelephonyManagerEx.getDefault();
        String values[] = telephonyManagerEx.getSupportCardType(
                SubscriptionManager.getSlotIndex(subId));
        Log.d(TAG, "cardType = " + Arrays.toString(values));

        TelephonyManager tm
            = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        /// isCtCard will be true.
        boolean isCtCard = false;
        if (values != null) {
            for (int i = 0; i < values.length; i++) {
                if ("RUIM".equals(values[i])) {
                    isCtCard = true;
                    break;
                } else if ("CSIM".equals(values[i])) {
                    isCtCard = true;
                    break;
                }
            }
        }
        ///simMccMnc means is CT card, 46011 means is CT4G card,46003 means is CT3G card.
        ///networkType means ps data, when is NETWORK_TYPE_LTE or NETWORK_TYPE_EHRPD,
        ///dataMccMnc will be true.
        String simMccMnc = tm.getSimOperator(subId);
        int networkType = tm.getNetworkType(subId);
        boolean dataMccMnc = false;
        if (simMccMnc != null && (isCtSim(simMccMnc))) {
            if (TelephonyManager.NETWORK_TYPE_LTE == networkType
                    || TelephonyManager.NETWORK_TYPE_EHRPD == networkType ) {
                dataMccMnc = true;
            }
        }

        boolean isMmsSupltype = false;
        if (type != null) {
            if (type.equals(CTWAP_TYPE)) {
                isMmsSupltype = true;
            }
        }

        /// for CT volte feature, when volte button off, remove ims apn.
        boolean isIMsType = false;
        int slotId = SubscriptionManager.getSlotIndex(subId);
        boolean enh4glteMode = MtkImsManager.isEnhanced4gLteModeSettingEnabledByUser(
                mContext, slotId) &&
                MtkImsManager.isNonTtyOrTtyOnVolteEnabled(mContext, slotId);
        int isChecked = Settings.Global.getInt(mContext.getContentResolver(),
                Settings.Global.ENHANCED_4G_MODE_ENABLED, 0);
        if (SystemProperties.getInt("persist.vendor.mims_support", 1) > 1) {
            if (PhoneConstants.SIM_ID_1 == slotId) {

            } else if (PhoneConstants.SIM_ID_2 == slotId) {
                isChecked = Settings.Global.getInt(mContext.getContentResolver(),
                        ENHANCED_4G_MODE_ENABLED_SIM2, 0);
            }
        }
        int mainPhoneId = SubscriptionManager.INVALID_PHONE_INDEX;
        String curr3GSim = SystemProperties.get("persist.vendor.radio.simswitch", "");
        Log.d(TAG, "current 3G Sim = " + curr3GSim);
        if (!TextUtils.isEmpty(curr3GSim)) {
            int curr3GPhoneId = Integer.parseInt(curr3GSim);
            mainPhoneId = curr3GPhoneId - 1;
        }
        ImsManager imsMgr = ImsManager.getInstance(mContext, slotId);
        boolean isVolteSupport
                = imsMgr.isVolteEnabledByPlatform();
        if (simMccMnc != null
                && isCtCard
                && IMS_TYPE.equals(type)) {
            /// multiple ims, if volte is select, IMS need show.
            if (SystemProperties.getInt("persist.vendor.mims_support", 1) > 1) {
                if (!enh4glteMode || !isVolteSupport) {
                    isIMsType = true;
                }
            /// if not multiple ims, if volte is select and is mainPhoneId,IMS need show
            } else {
                if (!enh4glteMode || mainPhoneId != slotId || !isVolteSupport) {
                    isIMsType = true;
                }
            }
        }

        if ((isCtCard && isMmsSupltype)
                || (isCtCard && isIMsType)) {
            if (null != mvnoMatchData && mvnoMatchData.length() > 0) {
                if (mvnoApnList != null && mvnoApnList.size() > 0) {
                    mvnoApnList.remove(mvnoApnList.size() - 1);
                }
            } else {
                if (mnoApnList != null && mnoApnList.size() > 0) {
                    mnoApnList.remove(mnoApnList.size() - 1);
                }
            }
        }

        Log.d(TAG, "simMccMnc = " + simMccMnc
                + " dataMccMnc = " + dataMccMnc
                + " networkType = " + networkType
                + " isMmsSupltype = " + isMmsSupltype
                + " isIMsType = " + isIMsType
                + " type = " + type
                + " isChecked = " + isChecked
                + " enh4glteMode = " + enh4glteMode
                + " isVolteSupport = " + isVolteSupport
                + " subId = " + subId
                + " slotId = " + slotId
                + " mainPhoneId = " + mainPhoneId
                + " curr3GSim = " + curr3GSim
                + " mvnoType = " + mvnoType
                + " mvnoMatchData = " + mvnoMatchData);
    }

    /**
     * Update the customized status(enable , disable).
     * Called at update screen status
     * @param subId sub id
     * @param sourceType 0 means google default apn.
     * @param root PPP's parent
     * @param apnType apnType="IMS", apn="ctnet" apn="ctwap".
     */
    @Override
    public void updateFieldsStatus(int subId, int sourceType,
            PreferenceScreen root, String apnType) {
        
        Log.d(TAG, "updateFieldsStatus subId=" + subId
                + " sourceType = " + sourceType
                + " apnType = " + apnType);

        if (sourceType != SOURCE_TYPE_DEFAULT) {
            return;
        }
        TelephonyManager tm =
            (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        String simMccMnc = tm.getSimOperator(subId);
        MtkTelephonyManagerEx telephonyManagerEx = MtkTelephonyManagerEx.getDefault();
        String values[] = telephonyManagerEx.getSupportCardType(
                SubscriptionManager.getSlotIndex(subId));
        Log.d(TAG, "cardType = " + Arrays.toString(values));
        boolean isCtCard = false;
        if (values != null) {
            for (int i = 0; i < values.length; i++) {
                if ("RUIM".equals(values[i])) {
                    isCtCard = true;
                    break;
                } else if ("CSIM".equals(values[i])) {
                    isCtCard = true;
                    break;
                }
            }
        }

        boolean isMvnoCtCard = false;
        if (null != root.findPreference("mvno_match_data")) {
            EditTextPreference mvno_match_data
                = ((EditTextPreference)root.findPreference("mvno_match_data"));
            String mvno_text = mvno_match_data.getText();
            String mvno_summary = mvno_match_data.getSummary().toString();
            Log.d(TAG, "mvno_match_data false != null"
                    + " mvno_text = " + mvno_text
                    + " mvno_summary = " + mvno_summary
                    + " simMccMnc = " + simMccMnc);
            if (CT_NUMERIC_3.equals(simMccMnc)
                    && (CT_NUMERIC_MVNO_3.equals(mvno_text)
                            || CT_NUMERIC_MVNO_3.equals(mvno_summary))) {
                isMvnoCtCard = true;
            }
            if (CT_NUMERIC_4.equals(simMccMnc)
                    && (CT_NUMERIC_MVNO_3.equals(mvno_text)
                            || CT_NUMERIC_MVNO_3.equals(mvno_summary))) {
                isMvnoCtCard = true;
            }
            if (CT_NUMERIC_4.equals(simMccMnc)
                    && (CT_NUMERIC_MVNO_4.equals(mvno_text)
                            || CT_NUMERIC_MVNO_4.equals(mvno_summary))) {
                isMvnoCtCard = true;
            }
            if (CT_NUMERIC_5.equals(simMccMnc)
                    && (CT_NUMERIC_MVNO_5.equals(mvno_text)
                            || CT_NUMERIC_MVNO_5.equals(mvno_summary))) {
                isMvnoCtCard = true;
            }
            if (CT_NUMERIC_6.equals(simMccMnc)
                    && (CT_NUMERIC_MVNO_4.equals(mvno_text)
                            || CT_NUMERIC_MVNO_4.equals(mvno_summary))) {
                isMvnoCtCard = true;
            }
            if (CT_NUMERIC_7.equals(simMccMnc)
                    && (CT_NUMERIC_MVNO_4.equals(mvno_text)
                            || CT_NUMERIC_MVNO_4.equals(mvno_summary))) {
                isMvnoCtCard = true;
            }
        }

        Log.d(TAG, "updateFieldsStatus isCtCard=" + isCtCard + " isMvnoCtCard= " + isMvnoCtCard);

        boolean isImsType = false;
        int slotId = SubscriptionManager.getSlotIndex(subId);
        ImsManager imsMgr = ImsManager.getInstance(mContext, slotId);
        boolean isVolteSupport
                = imsMgr.isVolteEnabledByPlatform();
        if (isVolteSupport) {
            if (apnType != null) {
                if (apnType.equals(IMS_TYPE)) {
                    isImsType = true;
                }
            }
        }
        //CT net can not editor.
        boolean isCtnetType = false;
        if (apnType != null) {
            if (apnType.equals(CTNET_TYPE)) {
                isCtnetType = true;
            }
        }
        Log.d(TAG, "updateFieldsStatus isCtCard = " + isCtCard
                + " isMvnoCtCard = " + isMvnoCtCard
                + " isImsType = " + isImsType
                + " isCtnetType = " + isCtnetType);

        if ((isCtCard || isMvnoCtCard) && (isImsType || isCtnetType)) {
            int count = root.getPreferenceCount();
            for (int i = 0; i < count; i++) {
                root.getPreference(i).setEnabled(false);
            }
        }
    }

    public static boolean isCtSim(String numeric) {
        boolean ctSim = false;
        for (String ct : CT_NUMERIC) {
            if (ct.equals(numeric)) {
                ctSim = true;
                break;
            }
        }
        return ctSim;
    }
}
