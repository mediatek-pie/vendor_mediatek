package com.mediatek.op12.settings;

import android.content.Context;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.util.Log;

import com.mediatek.settings.ext.DefaultWfcSettingsExt;

/**
 * Plugin implementation for WFC Settings.
 */

public class Op12WfcSettingsExt extends DefaultWfcSettingsExt {

    private static final String TAG = "Op12WfcSettingsExt";
    private static final String KEY_WFC_SETTINGS = "wifi_calling_settings";
    private static final String BUTTON_WFC_MODE = "wifi_calling_mode";

    private Context mContext;

    public Op12WfcSettingsExt(Context context) {
        mContext = context;
    }

     /** Customize wfc setting.
     * @param context context
     * @param preferenceScreen preferenceScreen
     * @return
     */
    @Override
    public void customizedWfcPreference(Context context, PreferenceScreen preferenceScreen) {
        Log.d(TAG, "customizedWfcPreference in Settings");
        Preference wfcPreference = preferenceScreen.findPreference(KEY_WFC_SETTINGS);
        if (wfcPreference != null) {
            preferenceScreen.removePreference(wfcPreference);
            Log.d(TAG, "removeWifiCallingPreference: WFC button removed in Settings");
        }
    }
}
