package com.mediatek.op03.wallpaper;

import android.content.Context;
import android.content.res.Resources;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.OperationCanceledException;
import android.graphics.drawable.Drawable;
import android.util.Log;


import com.mediatek.common.wallpaper.DefaultWallpaperPlugin;
import com.mediatek.op03.wallpaper.R;

public class Op03WallpaperPluginExt /*extends ContextWrapper */extends DefaultWallpaperPlugin {

    private static final String TAG = "Op03WallpaperPluginExt";
    private Context mContextWallpaperMgr = null;
    private Context mContext = null;

    public Op03WallpaperPluginExt(Context context) {
        super(context);
        mContext = context;
        if (context == null) {
		  Log.d(TAG, "Op03WallpaperPluginExt: cntx null");
        } else {
	      Log.d(TAG, "Op03WallpaperPluginExt: input parameter context valid");
        }
    }

    public Resources getPluginResources(Context context) {
        mContextWallpaperMgr = context;
            return mContext.getResources();
    }

    public int getPluginDefaultImage() {
        int imageID = 0;
        //return mContext.getResources().getDrawable(R.drawable.default_wallpaper);
        imageID = R.drawable.default_wallpaper;
        //Log.d(TAG,"getPluginDefaultImage: image_file_name used is default_wallpaper"+" "+imageID);
        return imageID;
    }

}
