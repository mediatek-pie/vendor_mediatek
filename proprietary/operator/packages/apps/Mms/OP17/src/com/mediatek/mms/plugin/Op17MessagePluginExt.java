package com.mediatek.mms.plugin;

import android.content.Context;

import com.mediatek.mms.ext.DefaultOpMessagePluginExt;
import com.mediatek.mms.ext.IOpMmsConfigExt;

public class Op17MessagePluginExt extends DefaultOpMessagePluginExt {

    private Context mContext;
    public Op17MessagePluginExt(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    public IOpMmsConfigExt getOpMmsConfigExt() {
        return new Op17MmsConfigExt();
    }

}
