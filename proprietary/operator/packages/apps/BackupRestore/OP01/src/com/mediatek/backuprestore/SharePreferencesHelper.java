package com.mediatek.backuprestore.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

public class SharePreferencesHelper {
    private static SharedPreferences sSharedPreferences;

    public static SharedPreferences getInstance(Context context) {
        if (sSharedPreferences == null) {
            sSharedPreferences = context.getApplicationContext()
                    .getSharedPreferences(Constants.SETTINGINFO, Activity.MODE_PRIVATE);
        }
        return sSharedPreferences;
    }

    public static void setNoticeStatus(Context context, boolean status, String key) {
        SharedPreferences.Editor editor = getInstance(context).edit();
        editor.putBoolean(key, status);
        editor.commit();
    }

    public static boolean getNoticeStatus(Context context, String key) {
        return getInstance(context).getBoolean(key, false);
    }

    public static void setBackupPath(Context context, Uri path, String key) {
	    SharedPreferences.Editor editor = getInstance(context).edit();
	    editor.putString(key, path.toString());
	    editor.commit();
   }
	
    public static Uri getBackupPath(Context context, String key) {
        String path = getInstance(context).getString(key, null);
		if (path != null) {
 			return Uri.parse(path);
		} else {
			return null;
		}
    }
}
