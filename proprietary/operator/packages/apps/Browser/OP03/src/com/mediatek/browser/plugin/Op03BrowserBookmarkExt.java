package com.mediatek.browser.plugin;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mediatek.browser.ext.DefaultBrowserBookmarkExt;


//@PluginImpl(interfaceName="com.mediatek.browser.ext.IBrowserBookmarkExt")
public class Op03BrowserBookmarkExt extends DefaultBrowserBookmarkExt {

    private static final String TAG = "Op03BrowserBookmarkExt";
    private Context mContext = null;

    public Op03BrowserBookmarkExt(Context context) {
        super();
        mContext = context;
    }

    public int addDefaultBookmarksForCustomer(SQLiteDatabase db) {
        Log.i(TAG, "return from addDefaultBookmarksForCustomer" + " --OP03 implement");
        return 0;
    }
}
