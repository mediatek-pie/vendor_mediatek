package com.mediatek.browser.plugin;

import android.content.Context;

import com.mediatek.browser.ext.IBrowserBookmarkExt;
import com.mediatek.browser.ext.IBrowserRegionalPhoneExt;
import com.mediatek.browser.ext.IBrowserSettingExt;
import com.mediatek.browser.ext.OpBrowserCustomizationFactoryBase;

public class Op03BrowserCustomizationFactory extends OpBrowserCustomizationFactoryBase {
    private Context mContext;

    public Op03BrowserCustomizationFactory(Context context) {
        mContext = context;
    }

    @Override
    public IBrowserBookmarkExt makeBrowserBookmarkExt() {
        android.util.Log.d("Op03BrowserCustomizationFactory", "new plugin framework op03!");
        return new Op03BrowserBookmarkExt(mContext);
    }
    @Override
    public IBrowserSettingExt makeBrowserSettingExt() {
        return new Op03BrowserSettingExt();
    }
@Override
    public IBrowserRegionalPhoneExt makeBrowserRegionalPhoneExt() {
        return new OP03BrowserDefaultBookmarksExt(mContext);
    }
}