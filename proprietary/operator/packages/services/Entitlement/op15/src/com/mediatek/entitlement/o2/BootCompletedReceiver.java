package com.mediatek.entitlement.o2;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;

public class BootCompletedReceiver extends BroadcastReceiver {
    private static final String TAG = "[Entitlement]BootCompletedReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            Log.d(TAG, "BOOT_COMPLETED: start entitlement check");
            Intent i = new Intent(context, EntitlementService.class);
            i.setAction(EntitlementService.ACTION_ENTITLEMENT_CHECK);
            context.startService(i);
        } else if (intent.getAction().equals(
               "android.intent.action.ACTION_SUBINFO_RECORD_UPDATED")) {
            Log.d(TAG, "Sim Record Updated : Trigger Service");
            Intent i = new Intent(context, EntitlementService.class);
            i.setAction(EntitlementService.ACTION_SUBINFO_RECORD_UPDATED);
            i.putExtras(intent);
            context.startService(i);
        }
    }
}
