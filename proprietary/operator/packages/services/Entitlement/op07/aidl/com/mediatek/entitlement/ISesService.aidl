package com.mediatek.entitlement;

import com.mediatek.entitlement.ISesServiceListener;

interface ISesService {

    /**
     * Called to start Entilement Check. Device activation is also done for first time.
     * The service will decide what to do by current state.
     *
     * @param service the service name to entitle. e.g. 'VoWiFi'.
     * @param retryTimer caller can indicate the timer for service to retry.
     *         This is meaningless if retryTimes equal to 0.
     * @param retryTimes caller can indicate the retry times for service to retry.
     *         0 means no need to retry
     */
    void startEntitlementCheck(in String service, in int retryTimer, in int retryTimes);

    /**
     * Called to cancel Entilement Check.
     * The service will return to the original state. (before startEntitlementCheck)
     *
     * @param service the service name to entitle. e.g. 'VoWiFi'.
     */
    void stopEntitlementCheck(in String service);

    /**
     * Called to update Location and T&C information.
     * The service will query latest URL/post-data and callback onWebsheetPost again.
     *
     * @param service the service name to entitle. e.g. 'VoWiFi'.
     */
    void updateLocationAndTc(in String service);

    /**
     * Called to get the entitlement state currently.
     *
     * @param service the service name to entitle. e.g. 'VoWiFi'.
     */
    String getCurrentEntitlementState(in String service);

    /**
     * Get the last one error code.
     *
     * @return the last one error code.
     */
    int getLastErrorCode();

    /**
     * Register listener for any SES event.
     *
     * @param listener the instance of ISesServiceStateListener
     */
    void registerListener(in ISesServiceListener listener);

    /**
     * Un-register listener from service.
     *
     * @param listener the instance of ISesServiceStateListener
     */
    void unregisterListener(in ISesServiceListener listener);

    /**
     * Deactivate a service. For Wi-Fi calling, the managePushToken REMOVE
     * procedure will be performed.
     *
     * @param service the service name to entitle. e.g. 'VoWiFi'.
     */
    void deactivateService(in String service);
}
