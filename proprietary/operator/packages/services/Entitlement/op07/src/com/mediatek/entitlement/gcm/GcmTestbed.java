package com.mediatek.entitlement.gcm;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class GcmTestbed extends Activity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "Entitlement-GcmTestbed";

    private ProgressBar mRegistrationProgressBar;
    private TextView mInformationTextView;

    private static final String BROADCAST_PUSH_NOTIFICATION = "com.mediatek.entitlement.gcm.PushNotification";

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BROADCAST_PUSH_NOTIFICATION.equals(action)) {
                sendNotification(intent.getExtras());
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.mediatek.entitlement.R.layout.gcm_main);

        mRegistrationProgressBar = (ProgressBar) findViewById(com.mediatek.entitlement.R.id.registrationProgressBar);
        mInformationTextView = (TextView) findViewById(com.mediatek.entitlement.R.id.informationTextView);

        final IntentFilter filter = new IntentFilter();
                filter.addAction(BROADCAST_PUSH_NOTIFICATION);
                registerReceiver(mBroadcastReceiver, filter);

        if (checkPlayServices()) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final String token = GcmHandler.getInstance().registerGcmToken(GcmTestbed.this);

                    runOnUiThread(new Runnable() {
                        public void run() {
                            mRegistrationProgressBar.setVisibility(ProgressBar.GONE);
                            if (token != null) {
                                mInformationTextView.setText("Token retrieved and sent to server! You can now use gcmsender to send downstream messages to this app.");
                            } else {
                                mInformationTextView.setText("An error occurred while either fetching the InstanceID token, sending the fetched token to the server or subscribing to the PubSub topic. Please try running the sample again.");
                            }
                        }
                    });
                }
            }).start();
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.d(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }

        return true;
    }

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(Bundle data) {
        if (data == null) {
            Log.d(TAG, "sendNotification: null data");
            return;
        }

        String message = data.getString("message", "");
        Log.d(TAG, "sendNotification: message=" + message);

        Intent intent = new Intent(this, GcmTestbed.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, // Request code
                intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setContentTitle("GCM Message")
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setSmallIcon(android.R.drawable.stat_sys_warning);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(
            0, // ID of notification
            notificationBuilder.build());
    }
}
