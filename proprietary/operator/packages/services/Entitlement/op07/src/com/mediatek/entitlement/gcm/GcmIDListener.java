package com.mediatek.entitlement.gcm;

import com.google.android.gms.iid.InstanceIDListenerService;

public class GcmIDListener extends InstanceIDListenerService {

    private static final String TAG = "Entitlement-gcm";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. This call is initiated by the
     * InstanceID provider.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Fetch updated Instance ID token and notify it.
        GcmHandler.getInstance().registerGcmToken(getApplicationContext());
    }
    // [END refresh_token]
}
