/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op.phone.plugin.gtt;

import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.provider.Settings;
import android.telecom.TelecomManager;
import android.util.Log;
import android.widget.Toast;

import com.mediatek.op.phone.plugin.OpTelephonyUtils;
import com.mediatek.op.phone.plugin.R;
import com.mediatek.phone.ext.DefaultGttInfoExt;

import java.util.Set;

public class OpGttInfoExt extends DefaultGttInfoExt {
    private static final String TAG = "OpGttInfoExt";
    private static final int TTY_MODE_OFF = 0;
    private static final int TTY_MODE_FULL = 1;
    private static final int TTY_MODE_HCO = 2;
    private static final int TTY_MODE_VCO = 3;
    private static final String PREFERRED_TTY_MODE = "preferred_tty_mode";

    private Context mContext;

    public OpGttInfoExt(Context context) {
        mContext = context;
    }

    public void writeSettingsAndShowToast(int ttyMode) {
        if (!OpTelephonyUtils.isGttOn()) {
            return;
        }
        Log.v(TAG, "writeSettingsAndShowToast ttyMode = " + ttyMode);
        Toast.makeText(mContext,
                mContext.getResources()
                        .getString(R.string.gtt_toast_for_setting_failed),
                Toast.LENGTH_SHORT).show();
        Settings.Secure.putInt(
                mContext.getContentResolver(),
                PREFERRED_TTY_MODE,
                ttyMode);
    }

    public void updateAudioTtyMode(int currentMode) {
        if (!OpTelephonyUtils.isGttOn()) {
            return;
        }
        String audioTtyMode;
        switch (currentMode) {
            case TTY_MODE_FULL:
                audioTtyMode = "tty_full";
                break;
            case TTY_MODE_VCO:
                audioTtyMode = "tty_vco";
                break;
            case TTY_MODE_HCO:
                audioTtyMode = "tty_hco";
                break;
            case TTY_MODE_OFF:
            default:
                audioTtyMode = "tty_off";
                break;
        }
        Log.v(TAG, "updateAudioTtyMode " + audioTtyMode);

        AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setParameters("tty_mode=" + audioTtyMode);
    }
}

