/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op07.phone;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.util.Log;
import android.widget.Toast;

import com.android.internal.telephony.PhoneFactory;

import com.mediatek.internal.telephony.FemtoCellInfo;
import com.mediatek.internal.telephony.MtkGsmCdmaPhone;

import java.util.ArrayList;

/**
 * Femtocell selection handling.
 */
public class ManualFemtoCellSelection extends Preference {
    private static final String LOG_TAG = "OP07ManualFemtoCellSelection";

    private final int FEMTO_SELECT_STATE_NONE = 0;
    private final int FEMTO_SELECT_STATE_FETCHING = 1;
    private final int FEMTO_SELECT_STATE_SELECTING = 2;
    private final int FEMTO_SELECT_STATE_DONE = 3;
    private final int FEMTO_SELECT_STATE_FAILED = 4;

    private final Context mContext;
    private final Context mHostContext;
    private int mPhoneId;
    private MyHandler mHandler;
    private ProgressDialog mDialog = null;
    private ArrayList<FemtoCellInfo> mFemtoList = null;
    private int mFemtoCount = 0;
    private int mFemtoIndex = 0;
    private int mFemtoSelectState = FEMTO_SELECT_STATE_NONE;

    /**
     * Default constructor for Femtocell selection.
     * @param context context of plugin app
     * @param prefSet host app prefernce screen
     */
    public ManualFemtoCellSelection(Context context, PreferenceCategory prefSet) {
        super(prefSet.getContext());
        mPhoneId = PhoneFactory.getDefaultPhone().getPhoneId();
        mHandler = new MyHandler();
        mContext = context;
        mHostContext = prefSet.getContext();
    }

    private void showProgressDialog() {
        // TODO Auto-generated method stub
        Log.d(LOG_TAG, "showProgressDialog");
        mDialog = new ProgressDialog(mHostContext);
        mDialog.setMessage(mContext.getText(R.string.fetching_femtocell_list));
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Log.d(LOG_TAG, "onDismiss, femtoSelectState=" + mFemtoSelectState);
                if (mFemtoSelectState == FEMTO_SELECT_STATE_FETCHING) {
                    MtkGsmCdmaPhone phone = (MtkGsmCdmaPhone)
                            PhoneFactory.getPhone(mPhoneId);
                    phone.abortFemtoCellList(
                            mHandler.obtainMessage(MyHandler.MESSAGE_ABORT_FEMTOCELL_LIST));
                }
                mFemtoSelectState = FEMTO_SELECT_STATE_NONE;
                if (mDialog != null) {
                    mDialog = null;
                }
            }
        });
        mDialog.setIndeterminate(true);
        mDialog.show();
    }

    @Override
    protected void onClick() {
        if (mFemtoSelectState == FEMTO_SELECT_STATE_NONE) {
            Log.d(LOG_TAG, "onClick");
            showProgressDialog();
            MtkGsmCdmaPhone phone = (MtkGsmCdmaPhone) PhoneFactory.getPhone(mPhoneId);
            phone.getFemtoCellList(
                    mHandler.obtainMessage(MyHandler.MESSAGE_GET_FEMTOCELL_LIST));
            mFemtoSelectState = FEMTO_SELECT_STATE_FETCHING;
        } else {
            Log.d(LOG_TAG, "onClick is continuous, femtoSelectState="
                    + mFemtoSelectState);
        }
    }

    /**
     * Handler class to handle Femtocell selection.
     */
    private class MyHandler extends Handler {

        private static final int MESSAGE_GET_FEMTOCELL_LIST = 0;
        private static final int MESSAGE_SELECT_FEMTOCELL = 1;
        private static final int MESSAGE_ABORT_FEMTOCELL_LIST = 2;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_GET_FEMTOCELL_LIST:
                    handleGetFemtocellList(msg);
                    break;

                case MESSAGE_SELECT_FEMTOCELL:
                    handleSelectFemtoCell(msg);
                    break;

                case MESSAGE_ABORT_FEMTOCELL_LIST:
                    handleAbortFemtocellList(msg);
                    break;

                default:
                    break;
            }
        }

        private void handleGetFemtocellList(Message msg) {
            Log.d(LOG_TAG, "handleGetFemtocellList, femtoSelectState="
                    + mFemtoSelectState);
            if (mFemtoSelectState == FEMTO_SELECT_STATE_FETCHING) {
                AsyncResult ar = (AsyncResult) msg.obj;
                if (ar.exception != null) {
                    Log.d(LOG_TAG, "handleGetFemtocellList, exception=" + ar.exception);
                    showFailToast();
                } else {
                    mFemtoList = (ArrayList<FemtoCellInfo>) ar.result;
                    mFemtoCount = mFemtoList != null ? mFemtoList.size() : 0;
                    mFemtoIndex = 0;
                    Log.d(LOG_TAG, "handleGetFemtocellList, femtoCount=" + mFemtoCount);
                    if (mFemtoCount > 0) {
                        mFemtoSelectState = FEMTO_SELECT_STATE_SELECTING;
                        startFemtocellSelection();
                    } else {
                        showFailToast();
                    }
                }
            }
        }

        private void startFemtocellSelection() {
            Log.d(LOG_TAG, "startFemtocellSelection, femtoSelectState="
                    + mFemtoSelectState + ", femtoIndex=" + mFemtoIndex);
            if (mFemtoSelectState == FEMTO_SELECT_STATE_SELECTING) {
                if (mFemtoIndex >= mFemtoCount) {
                    showFailToast();
                    return;
                }

                FemtoCellInfo femtoCell = mFemtoList.get(mFemtoIndex);
                MtkGsmCdmaPhone phone = (MtkGsmCdmaPhone)
                        PhoneFactory.getPhone(mPhoneId);
                phone.selectFemtoCell(femtoCell,
                        mHandler.obtainMessage(MyHandler.MESSAGE_SELECT_FEMTOCELL));
            }
        }

        private void handleSelectFemtoCell(Message msg) {
            Log.d(LOG_TAG, "handleSelectFemtoCell, femtoSelectState="
                    + mFemtoSelectState + ", femtoIndex=" + mFemtoIndex);
            if (mFemtoSelectState == FEMTO_SELECT_STATE_SELECTING) {
                AsyncResult ar = (AsyncResult) msg.obj;
                if (ar.exception != null) {
                    Log.d(LOG_TAG, "handleSelectFemtoCell, femtoIndex=" + mFemtoIndex
                            + ", exception=" + ar.exception);
                    mFemtoIndex++;
                    startFemtocellSelection();
                } else {
                    Log.d(LOG_TAG, "handleSelectFemtoCell sussess, femtoIndex=" + mFemtoIndex);
                    showPassToast();
                }
            }
        }

        private void handleAbortFemtocellList(Message msg) {
            Log.d(LOG_TAG, "handleAbortFemtocellList");
        }
    }

    private void showFailToast() {
        mFemtoSelectState = FEMTO_SELECT_STATE_FAILED;
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
        Toast.makeText(mHostContext,
                mContext.getString(R.string.femto_fail), Toast.LENGTH_LONG).show();
    }

    private void showPassToast() {
        mFemtoSelectState = FEMTO_SELECT_STATE_DONE;
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
        Toast.makeText(mHostContext,
                mContext.getString(R.string.femto_pass), Toast.LENGTH_LONG).show();
    }

    public void onDestroy() {
        Log.d(LOG_TAG, "onDestroy");
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }
}
