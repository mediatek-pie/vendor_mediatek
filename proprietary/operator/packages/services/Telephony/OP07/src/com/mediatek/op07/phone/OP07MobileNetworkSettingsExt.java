/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op07.phone;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.telecom.TelecomManager;
import android.telephony.PhoneStateListener;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.ims.ImsManager;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneFactory;

import com.mediatek.internal.telephony.MtkGsmCdmaPhone;
import com.mediatek.internal.telephony.ratconfiguration.RatConfiguration;
import com.mediatek.phone.ext.DefaultMobileNetworkSettingsExt;

/**
 * Plugin implementation for OP07.
 */
public class OP07MobileNetworkSettingsExt extends DefaultMobileNetworkSettingsExt {
    private static final String TAG = "OP07MobileNetworkSettingsExt";

    private static final String AOSP_KEY_WFC_SETTINGS = "wifi_calling_key";

    private static final String BUTTON_DISABLE_2G = "button_diable_2g";
    private static final String BUTTON_MOBILE_DATA = "mobile_data_enable";
    private static final String BUTTON_DATA_ROAMING = "button_roaming_key";
    private static final String BUTTON_PREFERED_NETWORK_MODE = "preferred_network_mode_key";
    private static final String BUTTON_ENABLED_NETWORKS = "enabled_networks_key";
    private static final String BUTTON_PLMN_LIST = "button_plmn_key";
    private static final String BUTTON_NETWORK_MODE_LTE = "button_network_mode_LTE_key";
    private static final String BUTTON_OPERATOR_SELECTION_EXPAND = "button_carrier_sel_key";

    private Context mContext;
    private SwitchPreference mDisable2GPreference;
    private ManualFemtoCellSelection mFemtoCellPreference = null;
    private AlertDialog mDialog;
    private int mSubId = SubscriptionManager.INVALID_SUBSCRIPTION_ID;
    private TelephonyManager mTelephonyManager;
    private boolean mListeningPhoneState = false;
    private boolean mIsDisable2G = false;
    private boolean mIsGetDisable2GState = false;
    private boolean mIsSetDisable2GState = false;
    private Handler mHandler;

    private class MyHandler extends Handler {
        private static final int MSG_GET_DISABLE_2G = 1;
        private static final int MSG_SET_DISABLE_2G = 2;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_GET_DISABLE_2G:
                    handleGetDisable2G(msg);
                    break;

                case MSG_SET_DISABLE_2G:
                    handleSetDisable2G(msg);
                    break;

                default:
                    break;
            }
        }
    }

    public OP07MobileNetworkSettingsExt(Context context) {
        mContext = context;
        mHandler = new MyHandler();
    }

    private PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            Log.d(TAG, "onCallStateChanged, state=" + state);

            mDisable2GPreference.setEnabled(!isHandleDisable2G()
                    && state == TelephonyManager.CALL_STATE_IDLE);
            mDisable2GPreference.setChecked(isSettingOn());

            if (state == TelephonyManager.CALL_STATE_RINGING
                    || state == TelephonyManager.CALL_STATE_OFFHOOK) {
                if (mDialog != null && mDialog.isShowing()) {
                    mDialog.dismiss();
                }
            }
        }
    };

    public void customizeBasicMobileNetworkSettings(
            PreferenceScreen prefSet, int subId) {
        mSubId = subId;
        int phoneId = SubscriptionManager.getPhoneId(subId);
        boolean isVtEnabled = ImsManager.getInstance(mContext, phoneId)
                .isVtEnabledByPlatform();

        Log.d(TAG, "customizeBasicMobileNetworkSettings, subId=" + subId
                + ", phoneId=" + phoneId + ", isVtEnabled=" + isVtEnabled);

        Preference mobileDataPref = prefSet.findPreference(BUTTON_MOBILE_DATA);
        if (mobileDataPref == null) {
            mobileDataPref = prefSet.findPreference(BUTTON_MOBILE_DATA + subId);
        }
        if (mobileDataPref != null) {
            String title = mContext.getString(R.string.mobile_data);
            String summary;
            if (isVtEnabled) {
                summary = mContext.getString(
                        R.string.mobile_data_summary_when_vilte_on);
            } else {
                summary = mContext.getString(
                        R.string.mobile_data_summary_default);
            }
            mobileDataPref.setTitle(title);
            mobileDataPref.setSummary(summary);
            Log.d(TAG, "change mobile data title=" + title + ", summary=" + summary);
        }

        SwitchPreference dataRoamingPref = (SwitchPreference)
                prefSet.findPreference(BUTTON_DATA_ROAMING);
        if (dataRoamingPref != null) {
            String title = mContext.getString(R.string.data_roaming_title);
            String summary;
            if (isVtEnabled) {
                summary = mContext.getString(
                        R.string.data_roaming_warning_vilte_capable);
            } else {
                summary = mContext.getString(R.string.data_roaming_warning);
            }
            dataRoamingPref.setTitle(title);
            dataRoamingPref.setSummary(summary);
            dataRoamingPref.setSummaryOn(summary);
            dataRoamingPref.setSummaryOff(summary);
            Log.d(TAG, "change data roaming title=" + title + ", summary=" + summary);
        }
    }

    public void initOtherMobileNetworkSettings(PreferenceScreen prefSet, int subId) {
        mSubId = subId;
        int phoneId = SubscriptionManager.getPhoneId(subId);

        Log.d(TAG, "initOtherMobileNetworkSettings, subId=" + subId
                + ", phoneId=" + phoneId);

        customizeFeatureForOperator(prefSet);

        Phone phone = PhoneFactory.getPhone(phoneId);
        if (phone instanceof MtkGsmCdmaPhone) {
            addPreference(prefSet);
            ((MtkGsmCdmaPhone) phone).getDisable2G(
                    mHandler.obtainMessage(MyHandler.MSG_GET_DISABLE_2G));
            mIsGetDisable2GState = true;
        } else {
            Log.e(TAG, "initOtherMobileNetworkSettings, phone is not MtkGsmCdmaPhone.");
        }
    }

    public boolean onPreferenceTreeClick(PreferenceScreen prefSet,
            Preference preference) {
        Preference buttonDisable2G = (SwitchPreference)
                prefSet.findPreference(BUTTON_DISABLE_2G);
        Preference femtoPref = prefSet.findPreference(
                OP07NetworkSettingExt.OP07_BUTTTON_MANUAL_FEMTOCELL);
        if (preference == buttonDisable2G) {
            Log.d(TAG, "onPreferenceTreeClick, buttonDisable2G");
            mDisable2GPreference = (SwitchPreference) buttonDisable2G;
            mDisable2GPreference.setEnabled(false);
            boolean checked = mDisable2GPreference.isChecked();
            if (checked) {
                Log.d(TAG, "Show alert dialog for disable 2G.");
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(preference.getContext());
                builder.setTitle(
                        mContext.getString(R.string.disable_2g_Alert_title));
                builder.setMessage(
                        mContext.getString(R.string.disable_2g_menu_on_alert));
                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        Log.d(TAG, "onCancel");
                        mDisable2GPreference.setChecked(false);
                    }
                });
                builder.setOnDismissListener(new DialogInterface.OnDismissListener () {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        Log.d(TAG, "onDismiss");
                        mDisable2GPreference.setChecked(isSettingOn());
                        mDisable2GPreference.setEnabled(getDisable2GEnabledState());
                    }
                });
                builder.setPositiveButton(
                        mContext.getString(R.string.disable_2g_alert_ok),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.d(TAG, "onClick, OK.");
                                handleSwitchAction(true);
                            }
                        });
                builder.setNegativeButton(
                        mContext.getString(R.string.disable_2g_alert_cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.d(TAG, "onClick, Cancel.");
                                mDisable2GPreference.setChecked(false);
                            }
                        });
                mDialog = builder.show();
            } else {
                boolean result = handleSwitchAction(false);
                if (!result) {
                    mDisable2GPreference.setEnabled(getDisable2GEnabledState());
                }
            }
            return true;
        } else if (preference == femtoPref) {
            Log.d(TAG, "onPreferenceTreeClick, buttonFemtocell");
            mFemtoCellPreference = (ManualFemtoCellSelection) femtoPref;
            return true;
        }
        return false;
    }

    private void addPreference(PreferenceScreen prefSet) {
        // Add disable 2G item when LTE is supported.
        boolean isLteSupported = RatConfiguration.isLteFddSupported()
                || RatConfiguration.isLteTddSupported();
        Log.d(TAG, "addPreference, isLteSupported=" + isLteSupported);
        if (isLteSupported) {
            mDisable2GPreference = (SwitchPreference)
                    prefSet.findPreference(BUTTON_DISABLE_2G);
            if (mDisable2GPreference == null) {
                SwitchPreference buttonDisable2G = new SwitchPreference(
                        prefSet.getContext());
                mDisable2GPreference = buttonDisable2G;
                buttonDisable2G.setKey(BUTTON_DISABLE_2G);
                buttonDisable2G.setTitle(mContext.getString(
                        R.string.network_button_disable_2g));
                buttonDisable2G.setSummary(mContext.getString(
                        R.string.network_button_disable_2g_summary));
                buttonDisable2G.setChecked(isSettingOn());
                prefSet.addPreference(buttonDisable2G);
                mTelephonyManager = TelephonyManager.from(mContext)
                        .createForSubscriptionId(mSubId);
                mTelephonyManager.listen(mPhoneStateListener,
                        PhoneStateListener.LISTEN_CALL_STATE);
                mListeningPhoneState = true;
            }
            mDisable2GPreference.setEnabled(false);
        }
    }

    public void unRegister() {
        Log.d(TAG, "unRegister");
        if (mListeningPhoneState) {
            mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_NONE);
        }
        if (mFemtoCellPreference != null) {
            mFemtoCellPreference.onDestroy();
            mFemtoCellPreference = null;
        }
    }

    public void customizeEnhanced4GLteSwitchPreference(
            PreferenceScreen prefSet, SwitchPreference switchPreference) {
        if (switchPreference != null) {
            String title = mContext.getString(R.string.volte_switch_title);
            String summary = mContext.getString(R.string.volte_switch_summary);
            switchPreference.setTitle(title);
            switchPreference.setSummary(summary);
            Log.d(TAG, "customizeEnhanced4GLteSwitchPreference, change title="
                    + title + ", summary=" + summary);
        }
    }

    private boolean handleSwitchAction(boolean switchValue) {
        Log.d(TAG, "handleSwitchAction, value=" + switchValue);
        boolean result = true;
        int phoneId = SubscriptionManager.getPhoneId(mSubId);
        Phone phone = PhoneFactory.getPhone(phoneId);
        if (phone instanceof MtkGsmCdmaPhone) {
            ((MtkGsmCdmaPhone) phone).setDisable2G(switchValue,
                    mHandler.obtainMessage(MyHandler.MSG_SET_DISABLE_2G));
            mIsDisable2G = switchValue;
            mIsSetDisable2GState = true;
        } else {
            Log.e(TAG, "handleSwitchAction, phone is not MtkGsmCdmaPhone.");
            result = false;
        }
        mDisable2GPreference.setChecked(mIsDisable2G);
        return result;
    }

    private boolean isSettingOn() {
        Log.d(TAG, "isSettingOn, isDisable2G=" + mIsDisable2G);
        return mIsDisable2G;
    }

    public void customizeDataRoamingAlertDialog(
            AlertDialog.Builder builder, int subId) {
        int phoneId = SubscriptionManager.getPhoneId(subId);
        boolean isVtEnabled = ImsManager.getInstance(mContext, phoneId)
                .isVtEnabledByPlatform();

        Log.d(TAG, "customizeDataRoamingAlertDialog, subId=" + subId
                + ", phoneId=" + phoneId + ", isVtEnabled=" + isVtEnabled);

        String summary;
        if (isVtEnabled) {
            summary = mContext.getString(
                    R.string.data_roaming_warning_vilte_capable);
        } else {
            summary = mContext.getString(R.string.data_roaming_warning);
        }
        builder.setMessage(summary);
        Log.d(TAG, "dataRoamingAlert summary=" + summary);
    }

    private void customizeFeatureForOperator(PreferenceScreen prefSet) {
        Log.d(TAG, "customizeFeatureForOperator");

        removePreference(prefSet, BUTTON_PREFERED_NETWORK_MODE);
        removePreference(prefSet, BUTTON_ENABLED_NETWORKS);
        removePreference(prefSet, BUTTON_PLMN_LIST);
        removePreference(prefSet, BUTTON_NETWORK_MODE_LTE);

        int phoneId = SubscriptionManager.getPhoneId(mSubId);
        Phone phone = PhoneFactory.getPhone(phoneId);
        if (phone != null && !phone.isCspPlmnEnabled()) {
            Log.d(TAG, "Remove operator selection item"
                    + " when CSP PLMN is not enabled.");
            removePreference(prefSet, BUTTON_OPERATOR_SELECTION_EXPAND);
        }
    }

    private void removePreference(PreferenceScreen prefSet, String key) {
        Preference pref = prefSet.findPreference(key);
        if (pref != null) {
            prefSet.removePreference(pref);
        }
    }

    private boolean isHandleDisable2G() {
        Log.d(TAG, "isHandleDisable2G, getState=" + mIsGetDisable2GState
                + ", setState=" + mIsSetDisable2GState);
        return mIsGetDisable2GState || mIsSetDisable2GState;
    }

    private boolean getDisable2GEnabledState() {
        boolean inCall = TelecomManager.from(mContext).isInCall();
        Log.d(TAG, "getDisable2GEnabledState, inCall=" + inCall
                + ", getState=" + mIsGetDisable2GState
                + ", setState=" + mIsSetDisable2GState);
        return !inCall && !mIsGetDisable2GState && !mIsSetDisable2GState;
    }

    private void handleGetDisable2G(Message msg) {
        AsyncResult ar = (AsyncResult) msg.obj;
        if (ar.exception != null) {
            Log.e(TAG, "handleGetDisable2G, isDisable2G=" + mIsDisable2G
                    + ", exception=" + ar.exception);
        } else {
            int[] mode = (int[]) ar.result;
            if (mode != null && mode.length > 0) {
                if (mode[0] == 0) {
                    mIsDisable2G = false;
                } else if (mode[0] == 1) {
                    mIsDisable2G = true;
                }
                Log.d(TAG, "handleGetDisable2G, isDisable2G=" + mIsDisable2G
                        + ", mode=" + mode[0]);
            } else {
                Log.e(TAG, "handleGetDisable2G, isDisable2G=" + mIsDisable2G
                        + ", mode is null.");
            }
        }

        mIsGetDisable2GState = false;

        if (mDisable2GPreference != null) {
            mDisable2GPreference.setChecked(mIsDisable2G);
            mDisable2GPreference.setEnabled(getDisable2GEnabledState());
        }
    }

    private void handleSetDisable2G(Message msg) {
        AsyncResult ar = (AsyncResult) msg.obj;
        if (ar.exception != null) {
            mIsDisable2G = !mIsDisable2G;
            Log.e(TAG, "handleSetDisable2G, isDisable2G=" + mIsDisable2G
                    + ", exception=" + ar.exception);
        } else {
            Log.d(TAG, "handleSetDisable2G, isDisable2G=" + mIsDisable2G
                    + ", success.");
        }

        mIsSetDisable2GState = false;

        if (mDisable2GPreference != null) {
            mDisable2GPreference.setChecked(mIsDisable2G);
            mDisable2GPreference.setEnabled(getDisable2GEnabledState());
        }
    }

   /** Customize WFC pref as per operator requirement, on basis of MCC+MNC
    * Called from WirelessSettings
    * @param context context
    * @param preferenceScreen preferenceScreen
    * @param callingCategory calling Category
    * @return
    */
    @Override
    public void customizeWfcPreference(Context context, PreferenceScreen preferenceScreen,
                        PreferenceCategory callingCategory, int phoneId) {

        if (isEntitlementEnabled()) {
            Preference wfcSettingsPreference = (Preference) callingCategory
                .findPreference(AOSP_KEY_WFC_SETTINGS);
            Log.d(TAG, "remove wfc preference:" + wfcSettingsPreference);
            if (callingCategory != null && wfcSettingsPreference != null) {
                callingCategory.removePreference(wfcSettingsPreference);
            }
        }
    }

    public static boolean isEntitlementEnabled() {
        boolean isEntitlementEnabled = (1 == SystemProperties.getInt
                ("persist.vendor.entitlement_enabled", 1) ? true : false);
        Log.d(TAG, "isEntitlementEnabled:" + isEntitlementEnabled);

        return isEntitlementEnabled;

    }
}
