/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op06.phone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.telephony.CarrierConfigManager;
import android.util.Log;

import com.android.ims.ImsManager;
import com.mediatek.ims.config.ImsConfigContract;
import com.mediatek.internal.telephony.MtkSubscriptionManager;
import com.mediatek.phone.ext.DefaultMobileNetworkSettingsExt;


/**
 * Plugin implementation for WFC Settings plugin
 */
public class Op06MobileNetworkSettingsExt extends DefaultMobileNetworkSettingsExt {
    private static final String TAG = "Op06MobileNetworkSettingsExt";

    private Context mContext;
    private PreferenceScreen mPrefScreen;
    private OP06WfcSettings mWfcSettings;

    /** Constructor.
     * @param context context
     */
    public Op06MobileNetworkSettingsExt(Context context) {
        mContext = context;
        mWfcSettings = OP06WfcSettings.getInstance(mContext);
    }
   /**
     * Updating entry and entry values of list preference.
     * @param prefSet preference screen
     * @param subId phone id
     */
    @Override
    public void initOtherMobileNetworkSettings(PreferenceScreen prefSet, int subId) {
        mPrefScreen = prefSet;
    }

    /**
     * Customize the WFC setting preference.
     * @param context the context
     * @param preferenceScreen the WFC setting's preference screen
     * @param callingCategory the callingCategory
     * @param phoneId the WFC setting's phone id
     */
    @Override
    public void customizeWfcPreference(Context context,  PreferenceScreen preferenceScreen,
            PreferenceCategory callingCategory, int phoneId) {
        Log.d(TAG, "customizeWfcPreference");
        if (ImsManager.isWfcEnabledByPlatform(context)
            && WifiCallingUtils.isWifiCallingProvisioned(context, phoneId)) {
            Log.d(TAG, "Do the customzation");
            mWfcSettings.customizedWfcPreference(context, preferenceScreen, callingCategory,
                    phoneId);
        } else {
            Log.d(TAG, "isWfcEnabledByPlatform/isWifiCallingProvisioned not supported");
            mWfcSettings.removeWfcPreference(preferenceScreen);
        }
    }

    /**
     * @param context Context
     * @param phoneId phoneId
     * @return
     * Customize boolean whether provisioned or not
     */
    @Override
    public boolean isWfcProvisioned(Context context, int phoneId) {
        if (!WifiCallingUtils.isImsProvSupported(context,
                        MtkSubscriptionManager.getSubIdUsingPhoneId(phoneId))) {
            Log.d(TAG, "Ims prov feature not supported, phoneId:" + phoneId);
            return super.isWfcProvisioned(context, phoneId);
        }
         boolean isWifiCallingProvisioned = WifiCallingUtils
                .isWifiCallingProvisioned(context, phoneId);
        if (!isWifiCallingProvisioned) {
            if (mPrefScreen != null) {
                Log.d(TAG, "WFC is not provisioned. remove WFC Pref");
                mWfcSettings.removeWfcPreference(mPrefScreen);
            }
        }
        return isWifiCallingProvisioned;
    }
}
