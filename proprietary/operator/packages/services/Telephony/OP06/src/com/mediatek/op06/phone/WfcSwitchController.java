package com.mediatek.op06.phone;

import android.content.Context;
import android.os.PersistableBundle;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.telephony.CarrierConfigManager;
import android.telephony.SubscriptionManager;
import android.util.Log;
import android.widget.Toast;

import com.android.ims.ImsConfig;
import com.android.ims.ImsException;
import com.android.ims.ImsManager;

import com.mediatek.ims.internal.MtkImsManager;
import com.mediatek.ims.internal.MtkImsManagerEx;
import com.mediatek.internal.telephony.MtkPhoneConstants;
import com.mediatek.internal.telephony.RadioCapabilitySwitchUtil;

import mediatek.telephony.MtkCarrierConfigManager;

/**
 * Class to support operator customizations for WFC settings.
 */
public class WfcSwitchController {

    private static final String TAG = "Op06WfcSwitchController";
    private static final String AOSP_SETTING_WFC_PREFERENCE = "wifi_calling_settings";
    private static final String AOSP_CALL_SETTING_WFC_PREFERENCE
            = "button_wifi_calling_settings_key";
    private static final String OP06_WFC_PREFERENCE = "op06_wfc_preference_key";
    private static final int WFC_PREF_CHANGE_TYPE = 0;

    private static WfcSwitchController sController = null;

    private Context mPluginContext;
    private Context mAppContext;
    private int mPhoneId = 0;
    private Preference mAospWfcPreference;
    private PreferenceCategory mCallingCategory = null;
    private SwitchPreference mWfcSwitchSettings = null;
    private android.preference.SwitchPreference mWfcSwitchCallSettings = null;
    private PreferenceScreen mPreferenceScreenSettings = null;
    private PreferenceScreen mPreferenceScreenCallSettings = null;

    private WfcSwitchController(Context context) {
       mPluginContext = context;
    }

    /** Returns instance of WfcSwitchController.
     * @param context context
     * @return WfcSettings
     */
    public static WfcSwitchController getInstance(Context context) {

        if (sController == null) {
            sController = new WfcSwitchController(context);
        }
        return sController;
    }

    /**
     * Customize the WFC setting preference.
     * @param context the context
     * @param preferenceScreen the WFC setting's preference screen
     * @param phoneId the WFC setting's phone id
     */
    public void customizedWfcPreference(Context context, PreferenceScreen preferenceScreen,
            int phoneId) {
        customizedWfcPreference(context, preferenceScreen, null, phoneId);
    }

    /**
     * Customize the WFC setting preference.
     * @param context the context
     * @param preferenceScreen the WFC setting's preference screen
     * @param callingCategory the callingCategory
     * @param phoneId the WFC setting's phone id
     */
    public void customizedWfcPreference(Context context, PreferenceScreen preferenceScreen,
                        PreferenceCategory callingCategory, int phoneId) {
        mAppContext = context;
        mPhoneId = phoneId;
        mPreferenceScreenSettings = preferenceScreen;
        mCallingCategory = callingCategory;
        Preference wfcPreferenceSettings = null;
        int settingsUxType = -1;
        int defaultWfcMode = ImsConfig.WfcModeFeatureValueConstants.CELLULAR_PREFERRED;
        CarrierConfigManager configMgr = (CarrierConfigManager) mPluginContext
                .getSystemService(Context.CARRIER_CONFIG_SERVICE);
        PersistableBundle b =
                configMgr.getConfigForSubId(SubscriptionManager.getDefaultDataSubscriptionId());
        if (b != null) {
             settingsUxType = b.getInt(MtkCarrierConfigManager.MTK_KEY_WFC_SETTINGS_UX_TYPE_INT);
             defaultWfcMode = b.getInt(MtkCarrierConfigManager.MTK_KEY_DEFAULT_WFC_MODE_INT);
        }

        Log.d(TAG, "settingsUxType:" + settingsUxType);
        Log.d(TAG, "defaultWfcMode:" + defaultWfcMode);
        Log.d(TAG, "customizedWfcPreference with category");
        if (callingCategory != null) {
            //Adding preference in Network Settings
            mWfcSwitchSettings = (SwitchPreference) callingCategory
                            .findPreference(OP06_WFC_PREFERENCE);
            Log.d(TAG, "Plugin wfc switch: " + mWfcSwitchSettings);

            wfcPreferenceSettings =
                        (android.preference.Preference) callingCategory
                        .findPreference(AOSP_SETTING_WFC_PREFERENCE);
            Log.d(TAG, "AOSP wfcPreference: " + wfcPreferenceSettings);
        } else {
            //Adding preference in Call Settings
            mWfcSwitchSettings = (SwitchPreference) preferenceScreen
                    .findPreference(OP06_WFC_PREFERENCE);
            Log.d(TAG, "Plugin mWfcSwitch in CallSettings: " + mWfcSwitchSettings);

            wfcPreferenceSettings =
                    (android.preference.Preference) preferenceScreen
                    .findPreference(AOSP_CALL_SETTING_WFC_PREFERENCE);
            Log.d(TAG, "AOSP wfcPreference in CallSettings: " + wfcPreferenceSettings);
        }


        CharSequence title = null;
        int order = 0;
        if (wfcPreferenceSettings != null) {
            if (settingsUxType == WFC_PREF_CHANGE_TYPE) {
                title = wfcPreferenceSettings.getTitle();
                order = wfcPreferenceSettings.getOrder();
                if (callingCategory != null) {
                    callingCategory.removePreference(wfcPreferenceSettings);
                } else {
                    mPreferenceScreenSettings.removePreference(wfcPreferenceSettings);
                }
                checkAndAddWfcSwitchForSettings(title, order);
                updateWfcSwitchState();
            }
        }

    }


    private void checkAndAddWfcSwitchForSettings(CharSequence title, int order) {
        Log.d(TAG, "checkAndAddWfcSwitchForSettings ");
        if (mWfcSwitchSettings == null) {
                mWfcSwitchSettings = new SwitchPreference(mAppContext);
                mWfcSwitchSettings
                        .setOnPreferenceChangeListener(new WfcSwitchListenerForSettings());
                mWfcSwitchSettings.setKey(OP06_WFC_PREFERENCE);
                mWfcSwitchSettings.setTitle(title);
                mWfcSwitchSettings.setOrder(order);
            if (mCallingCategory != null) {
                Log.d(TAG, "addPreference for non null category");
                mCallingCategory.addPreference(mWfcSwitchSettings);
            } else {
                Log.d(TAG, "addPreference for null call settings preference");
                mPreferenceScreenSettings.addPreference(mWfcSwitchSettings);
            }
            MtkImsManager.setWfcMode(mAppContext,
               ImsConfig.WfcModeFeatureValueConstants.CELLULAR_PREFERRED, mPhoneId);
        }
    }

    /** Updates wfc switch.
      * @return
      */
    public void updateWfcSwitchState() {
        if (mWfcSwitchCallSettings != null) {
            mWfcSwitchCallSettings.setChecked(MtkImsManager.isWfcEnabledByUser(mPluginContext,
                                mPhoneId));
        } else if (mWfcSwitchSettings != null) {
            mWfcSwitchSettings.setChecked(MtkImsManager.isWfcEnabledByUser(mPluginContext,
                                mPhoneId));
        }
    }


    private boolean isInSwitchProcess() {
        int imsState = MtkPhoneConstants.IMS_STATE_DISABLED;
        try {
         imsState = MtkImsManagerEx.getInstance().getImsState(RadioCapabilitySwitchUtil
                         .getMainCapabilityPhoneId());
        } catch (ImsException e) {
           return false;
        }
        Log.d(TAG, "isInSwitchProcess , imsState = " + imsState);
        return imsState == MtkPhoneConstants.IMS_STATE_DISABLING
                || imsState == MtkPhoneConstants.IMS_STATE_ENABLING;
    }


    /** Removes wfc preference.
     * @param preferenceScreen preferenceScreen
     * @return
     */
    public void removeWfcPreference(Object preferenceScreen) {
        if (mPreferenceScreenCallSettings != null && mWfcSwitchCallSettings != null) {
            mPreferenceScreenCallSettings.removePreference(mWfcSwitchCallSettings);
        }
        if (mCallingCategory != null && mWfcSwitchSettings != null) {
            mCallingCategory.removePreference(mWfcSwitchSettings);
        }
    }

    /**
     * Class for WFC switch listener.
     */
    private class WfcSwitchListenerForSettings implements
            Preference.OnPreferenceChangeListener {

        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            boolean isChecked = ((Boolean) newValue).booleanValue();
            MtkImsManager.setWfcSetting(mPluginContext, isChecked, mPhoneId);
            return true;
        }
    }

    private class WfcSwitchListenerForCallSettings implements
            android.preference.Preference.OnPreferenceChangeListener {

        @Override
        public boolean onPreferenceChange(android.preference.Preference preference,
                Object newValue) {
            boolean isChecked = ((Boolean) newValue).booleanValue();
            MtkImsManager.setWfcSetting(mPluginContext, isChecked, mPhoneId);
            return true;
        }
    }
}
