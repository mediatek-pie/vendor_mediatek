/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2014. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op.net.ims;

import android.util.Log;
import android.telephony.ims.ImsCallProfile;
import android.telephony.ims.ImsStreamMediaProfile;

import com.android.ims.ImsCall;

import com.mediatek.ims.MtkImsCall;
import com.mediatek.ims.op.OpImsCallBase;

import com.mediatek.ims.internal.MtkImsCallSession;

import mediatek.telecom.MtkConnection;

public class OpImsCallImpl extends OpImsCallBase {
    private static final String TAG = "OpImsCallImpl";

    @Override
    public void callSessionTextCapabilityChanged(ImsCall.Listener listener, MtkImsCall mtkImsCall,
            int localCapability, int remoteCapability,
            int localTextStatus, int realRemoteCapability) {
        if (listener != null && listener instanceof MtkImsCall.Listener) {
            try {
                ((MtkImsCall.Listener)listener).onTextCapabilityChanged(mtkImsCall,
                    localCapability, remoteCapability, localTextStatus, realRemoteCapability);
            } catch (Throwable t) {
                Log.d(TAG, "callSessionTextCapabilityChanged :: ", t);
            }
        }
    }

    @Override
    public void callSessionRttEventReceived(ImsCall.Listener listener, MtkImsCall mtkImsCall,
                int event) {
        if (listener != null && listener instanceof MtkImsCall.Listener) {
            try {
                ((MtkImsCall.Listener)listener).onRttEventReceived(mtkImsCall, event);
            } catch (Throwable t) {
                Log.d(TAG, "callSessionRttEventReceived :: ", t);
            }
        }
    }

    @Override
    public void sendRttDowngradeRequest(ImsCallProfile callProfile, MtkImsCallSession session) {
        if (!callProfile.mMediaProfile.isRttCall()) {
            Log.d(TAG, "sendRttDowngradeRequest::Already non RTT call, ignoring.");
            return;
        }

        ImsCallProfile requestedProfile = new ImsCallProfile();
        requestedProfile.mMediaProfile.setRttMode(ImsStreamMediaProfile.RTT_MODE_DISABLED);
        session.sendRttModifyRequest(requestedProfile);
        Log.d(TAG, "sendRttDowngradeRequest");
    }

    @Override
    public void setRttMode(int mode, ImsCallProfile callProfile) {
        if (callProfile == null) {
            Log.d(TAG, "setRttMode return");
            return;
        }
        if (mode == 1) {
            callProfile.mMediaProfile.setRttMode(ImsStreamMediaProfile.RTT_MODE_FULL);
        } else {
            callProfile.mMediaProfile.setRttMode(ImsStreamMediaProfile.RTT_MODE_DISABLED);
        }
        Log.d(TAG, "setRttMode");
    }

    @Override
    public void notifyRttDowngradeEvent(ImsCall.Listener listener, MtkImsCall mtkImsCall) {

        if (listener == null ) return;
        Log.d(TAG, "notifyRttDowngradeEvent");

        ImsCallProfile callProfile = mtkImsCall.getCallProfile();

        if (callProfile != null) {
            callProfile.mMediaProfile.setRttMode(ImsStreamMediaProfile.RTT_MODE_DISABLED);
        } else {
            Log.d(TAG, "notifyRttDowngradeEvent, set rtt mode disable fail");
        }

        try {
            listener.onRttModifyResponseReceived(mtkImsCall,
                mediatek.telecom.MtkConnection.MtkRttModifyStatus.SESSION_DOWNGRADED_BY_REMOTE);
        } catch (Throwable t) {
            Log.d(TAG, "notifyRttDowngradeEvent:: send downgrade notify fail ", t);
        }
    }
}
