package com.mediatek.op02.telephony;

import android.content.Context;
import android.content.Intent;

import android.text.TextUtils;

import com.mediatek.internal.telephony.datasub.DataSubConstants;
import com.mediatek.internal.telephony.datasub.DataSubSelector;
import com.mediatek.internal.telephony.datasub.DataSubSelectorUtil;
import com.mediatek.internal.telephony.datasub.ISimSwitchForDSSExt;
import com.mediatek.internal.telephony.datasub.CapabilitySwitch;

import static com.mediatek.internal.telephony.datasub.DataSubConstants.SIM_SWITCH_UNKNOWN;

public class Op02SimSwitchForDSSExt implements ISimSwitchForDSSExt {

    public Op02SimSwitchForDSSExt(Context context) {
    }

    public void init(DataSubSelector dataSubSelector) {
    }

    public boolean checkCapSwitch(int policy) {
        return false;
    }

    public boolean checkCapSwitch() {
        return false;
    }

    public int isNeedSimSwitch() {
        return DataSubConstants.SIM_SWITCH_UNKNOWN;
    }
}