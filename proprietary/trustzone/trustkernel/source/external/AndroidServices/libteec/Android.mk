LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := libteec
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES

LOCAL_VENDOR_MODULE := true
LOCAL_MODULE_OWNER := trustkernel

LOCAL_SRC_FILES_arm := ../../../client/libs/armeabi-v7a/libteec.so
LOCAL_SRC_FILES_arm64 := ../../../client/libs/arm64-v8a/libteec.so

LOCAL_MODULE_SUFFIX := .so
LOCAL_MULTILIB := both

LOCAL_EXPORT_C_INCLUDE_DIRS := \
		$(LOCAL_PATH)/include

LOCAL_SHARED_LIBRARIES := liblog

include $(BUILD_PREBUILT)

include $(CLEAR_VARS)

LOCAL_MODULE := teed
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := EXECUTABLES

LOCAL_VENDOR_MODULE := true
LOCAL_MODULE_OWNER := trustkernel

LOCAL_SRC_FILES_arm := ../../../client/libs/armeabi-v7a/teed
LOCAL_SRC_FILES_arm64 := ../../../client/libs/arm64-v8a/teed

LOCAL_SHARED_LIBRARIES := \
	libkphproxy \
	libpl \
	liblog

LOCAL_INIT_RC := ../../../bsp/platform/common/scripts/trustkernel.rc

include $(BUILD_PREBUILT)
