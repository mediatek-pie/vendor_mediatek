LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := libalipay_tz
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_VENDOR_MODULE := true
LOCAL_MODULE_OWNER := trustkernel

LOCAL_SRC_FILES_arm := ../../../ca/libs/armeabi-v7a/libalipay_tz.so
LOCAL_SRC_FILES_arm64 := ../../../ca/libs/arm64-v8a/libalipay_tz.so

LOCAL_MODULE_SUFFIX := .so
LOCAL_MULTILIB := both

include $(BUILD_PREBUILT)

include $(CLEAR_VARS)

# for trustkernel/ifaa test
LOCAL_MODULE := libteeclientjni
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES

LOCAL_VENDOR_MODULE := true
LOCAL_MODULE_OWNER := trustkernel

LOCAL_SRC_FILES_arm := ../../../ca/libs/armeabi-v7a/libteeclientjni.so
LOCAL_SRC_FILES_arm64 := ../../../ca/libs/arm64-v8a/libteeclientjni.so

LOCAL_MODULE_SUFFIX := .so
LOCAL_MULTILIB := both

LOCAL_SHARED_LIBRARIES := libalipay_tz

include $(BUILD_PREBUILT)
