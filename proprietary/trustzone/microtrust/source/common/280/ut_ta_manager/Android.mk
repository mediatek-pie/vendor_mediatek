LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := UtTAManager
LOCAL_CERTIFICATE := platform
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_SRC_FILES := ut_ta_manager_service.apk
#LOCAL_PROGUARD_ENABLED := disabled
LOCAL_PROGUARD_FLAG_FILES := proguard.cfg
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_OWNER := mtk
include $(BUILD_PREBUILT)

#----------------------------------------------------------------------------
include $(CLEAR_VARS)
LOCAL_MODULE := lib_ut_ta_manager
LOCAL_MODULE_CLASS := JAVA_LIBRARIES
LOCAL_STATIC_JAVA_LIBRARIES := lib_ut_ta_manager_classes
#LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_OWNER := mtk
include $(BUILD_JAVA_LIBRARY)

include $(CLEAR_VARS)
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := lib_ut_ta_manager_classes:lib_ut_ta_manager.jar
include $(BUILD_MULTI_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE = com.beanpod.tamanager.xml
LOCAL_MODULE_CLASS = ETC
LOCAL_MODULE_PATH = $(TARGET_OUT_VENDOR)/etc/permissions
LOCAL_SRC_FILES = $(LOCAL_MODULE)
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_OWNER := mtk
include $(BUILD_PREBUILT)
