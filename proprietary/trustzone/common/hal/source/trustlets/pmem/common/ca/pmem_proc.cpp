/*
 * Copyright (c) 2014 - 2016 MediaTek Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <gtest/gtest.h>

#include "pmem_common.h"
#include "pmem_lib.h"
#include "pmem_test_cfg.h"

class PmemProcSingleTest : public ::testing::Test {
  protected:
    void SetUp() {}
    void TearDown() {}
};

class PmemProcBaseTest : public ::testing::Test {
  public:
    static void SetUpTestCase() {}
    static void TearDownTestCase() {}
};

class PmemProcAllocTest : public PmemProcBaseTest,
                          public ::testing::WithParamInterface<AllocParameters> {};

class PmemProcSaturationTest : public PmemProcBaseTest,
                               public ::testing::WithParamInterface<AllocParameters> {};

class PmemProcBoundaryTest : public PmemProcBaseTest,
                             public ::testing::WithParamInterface<AllocParameters> {};

class PmemProcAlignmentTest : public PmemProcBaseTest,
                              public ::testing::WithParamInterface<AllocParameters> {};

/****************************************************************************
 ** pmem device open/close test
 ****************************************************************************/
TEST_F(PmemProcSingleTest, pmem_device_open_Success) {
    int pmem_fd;

    ASSERT_TRUE((pmem_fd = pmem_open()) >= 0);
    ASSERT_EQ(0, pmem_close(pmem_fd));
};

/****************************************************************************
 ** pmem allocate/free test (unsupported size)
 ****************************************************************************/
TEST_F(PmemProcSingleTest, pmem_alloc_unsupported_size_Success) {
    int pmem_fd;
    uint32_t handle;

    ASSERT_TRUE((pmem_fd = pmem_open()) >= 0);
    ASSERT_NE(0, pmem_alloc(pmem_fd, SIZE_512B, 0, 0, &handle));
    ASSERT_EQ(0, pmem_close(pmem_fd));
}

/****************************************************************************
 ** pmem allocate/free test
 ****************************************************************************/
TEST_P(PmemProcAllocTest, pmem_alloc_Success) {
    int pmem_fd;
    uint32_t handle;

    if ((GetParam().size < PMEM_MIN_CHUNK_SIZE) || (GetParam().size > PMEM_MAX_CHUNK_SIZE)) {
        return;
    }

    ASSERT_TRUE((pmem_fd = pmem_open()) >= 0);
    ASSERT_EQ(
        0, pmem_alloc(pmem_fd, GetParam().size, GetParam().alignment, GetParam().flags, &handle));
    ASSERT_NE(0U, handle);
    ASSERT_EQ(0, pmem_free(pmem_fd, handle));
    ASSERT_EQ(0, pmem_close(pmem_fd));
}

INSTANTIATE_TEST_CASE_P(PMEM_ALLOC, PmemProcAllocTest, ::testing::ValuesIn(alloc_test_params));

INSTANTIATE_TEST_CASE_P(PMEM_ALLOC_ZERO, PmemProcAllocTest,
                        ::testing::ValuesIn(alloc_zero_test_params));

INSTANTIATE_TEST_CASE_P(PMEM_ALLOC_ALIGNED, PmemProcAllocTest,
                        ::testing::ValuesIn(alloc_aligned_test_params));

INSTANTIATE_TEST_CASE_P(PMEM_ALLOC_ZERO_ALIGNED, PmemProcAllocTest,
                        ::testing::ValuesIn(alloc_zero_aligned_test_params));

/****************************************************************************
 ** pmem allocate/free saturation test
 ****************************************************************************/
TEST_P(PmemProcSaturationTest, pmem_alloc_saturation_Success) {
    int pmem_fd, ret;
    int max_items = PMEM_MAX_SIZE / GetParam().size;
    uint32_t* p_handle_list = NULL;

    if ((GetParam().size < PMEM_MIN_CHUNK_SIZE) || (GetParam().size > PMEM_MAX_CHUNK_SIZE)) {
        return;
    }

    p_handle_list = (uint32_t*)malloc(sizeof(uint32_t) * max_items);
    ASSERT_TRUE(NULL != p_handle_list);

    ASSERT_TRUE((pmem_fd = pmem_open()) >= 0);

    for (int i = 0; i < max_items; i++) {
        ret = pmem_alloc(pmem_fd, GetParam().size, GetParam().alignment, GetParam().flags,
                         &p_handle_list[i]);
        ASSERT_EQ(0, ret);
        ASSERT_NE(0U, p_handle_list[i]);
    }

    for (int i = (max_items - 1); i >= 0; i--) {
        ret = pmem_free(pmem_fd, p_handle_list[i]);
        ASSERT_EQ(0, ret);
    }

    ASSERT_EQ(0, pmem_close(pmem_fd));

    free(p_handle_list);
}

INSTANTIATE_TEST_CASE_P(PMEM_ALLOC, PmemProcSaturationTest, ::testing::ValuesIn(alloc_test_params));

INSTANTIATE_TEST_CASE_P(PMEM_ALLOC_ZERO, PmemProcSaturationTest,
                        ::testing::ValuesIn(alloc_zero_test_params));

INSTANTIATE_TEST_CASE_P(PMEM_ALLOC_ALIGNED, PmemProcSaturationTest,
                        ::testing::ValuesIn(alloc_aligned_test_params));

INSTANTIATE_TEST_CASE_P(PMEM_ALLOC_ZERO_ALIGNED, PmemProcSaturationTest,
                        ::testing::ValuesIn(alloc_zero_aligned_test_params));

/****************************************************************************
 ** pmem allocate/free boundary test
 ****************************************************************************/
TEST_P(PmemProcBoundaryTest, pmem_alloc_boundary_Success) {
    int pmem_fd, ret;
    int max_items = PMEM_MAX_SIZE / GetParam().size;
    uint32_t* p_handle_list = NULL;
    uint32_t extra_handle = 0;

    if ((GetParam().size < PMEM_MIN_CHUNK_SIZE) || (GetParam().size > PMEM_MAX_CHUNK_SIZE)) {
        return;
    }

    p_handle_list = (uint32_t*)malloc(sizeof(uint32_t) * max_items);
    ASSERT_TRUE(NULL != p_handle_list);

    ASSERT_TRUE((pmem_fd = pmem_open()) >= 0);

    for (int i = 0; i < max_items; i++) {
        ret = pmem_alloc(pmem_fd, GetParam().size, GetParam().alignment, GetParam().flags,
                         &p_handle_list[i]);
        ASSERT_EQ(0, ret);
        ASSERT_NE(0U, p_handle_list[i]);
    }

    /* one more allocation, and expect the allocation is failed */
    ret = pmem_alloc(pmem_fd, SIZE_1K, SIZE_1K, 0, &extra_handle);
    ASSERT_NE(0, ret);

    for (int i = (max_items - 1); i >= 0; i--) {
        ret = pmem_free(pmem_fd, p_handle_list[i]);
        ASSERT_EQ(0, ret);
    }

    ASSERT_EQ(0, pmem_close(pmem_fd));

    free(p_handle_list);
}

INSTANTIATE_TEST_CASE_P(PMEM_ALLOC, PmemProcBoundaryTest, ::testing::ValuesIn(alloc_test_params));

INSTANTIATE_TEST_CASE_P(PMEM_ALLOC_ZERO, PmemProcBoundaryTest,
                        ::testing::ValuesIn(alloc_zero_test_params));

INSTANTIATE_TEST_CASE_P(PMEM_ALLOC_ALIGNED, PmemProcBoundaryTest,
                        ::testing::ValuesIn(alloc_aligned_test_params));

INSTANTIATE_TEST_CASE_P(PMEM_ALLOC_ZERO_ALIGNED, PmemProcBoundaryTest,
                        ::testing::ValuesIn(alloc_zero_aligned_test_params));

/****************************************************************************
 ** pmem allocate/free alignment test (size != alignment)
 ****************************************************************************/
TEST_P(PmemProcAlignmentTest, pmem_alloc_smaller_align_size_Success) {
    int pmem_fd, ret;
    uint32_t handle;

    if ((GetParam().size < PMEM_MIN_CHUNK_SIZE) || (GetParam().size > PMEM_MAX_CHUNK_SIZE)) {
        return;
    }

    ASSERT_TRUE((pmem_fd = pmem_open()) >= 0);

    /* alignment is less than size, we expect the allocation is failed */
    ASSERT_NE(0,
              pmem_alloc(pmem_fd, GetParam().size, GetParam().size / 2, GetParam().flags, &handle));

    /* alignment is larger than size, we expect the allocation is success */
    ASSERT_EQ(0,
              pmem_alloc(pmem_fd, GetParam().size, GetParam().size * 2, GetParam().flags, &handle));
    ASSERT_NE(0U, handle);
    ASSERT_EQ(0, pmem_free(pmem_fd, handle));

    /* alignment is zero, we expect the allocation is success */
    ASSERT_EQ(0, pmem_alloc(pmem_fd, GetParam().size, 0, GetParam().flags, &handle));
    ASSERT_NE(0U, handle);
    ASSERT_EQ(0, pmem_free(pmem_fd, handle));

    ASSERT_EQ(0, pmem_close(pmem_fd));
}

INSTANTIATE_TEST_CASE_P(PMEM_ALLOC_UNALIGNED, PmemProcAlignmentTest,
                        ::testing::ValuesIn(alloc_test_params));
