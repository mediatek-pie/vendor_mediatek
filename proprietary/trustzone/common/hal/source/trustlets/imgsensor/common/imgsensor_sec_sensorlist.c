/*
 * Copyright (C) 2018 MediaTek Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See http://www.gnu.org/licenses/gpl-2.0.html for more details.
 */

#include "imgsensor_sec_sensorlist.h"

struct IMGSENSOR_SEC_SENSOR_LIST imgsensor_sec_sensor_list[MAX_NUM_OF_SUPPORT_SENSOR] = {

/*SONY*/

/*SAMSUNG*/
#ifdef S5K4E6_MIPI_RAW
	{S5K4E6_SENSOR_ID, S5K4E6_MIPI_RAW_SensorInit},
#endif

/*OmniVision*/

/*  ADD sensor driver before this line */
	{0, NULL}, /* end of list */
};


