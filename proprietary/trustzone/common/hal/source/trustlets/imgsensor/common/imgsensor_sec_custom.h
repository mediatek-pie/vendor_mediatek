/*
 * Copyright (C) 2017 MediaTek Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See http://www.gnu.org/licenses/gpl-2.0.html for more details.
 */

/*Extern api to call Hardware Definition*/
int imgsensor_query_SEC_I2C_BUS(void);
int imgsensor_query_SEC_CSI(void);

/*Implement Hardware Definition*/
enum I2C_BUS_NUMBER {
	I2C_BUS_0 = 0x0,
	I2C_BUS_1 = 0x1,
	I2C_BUS_2 = 0x2,
	I2C_BUS_3 = 0x3,
	I2C_BUS_4 = 0x4,
	I2C_BUS_5 = 0x5,
	I2C_BUS_6 = 0x6,

};

/*follow top_mux ctrl list*/
enum CSI_NUM {
	CSI0  = 0x0,//seninf1
	CSI0A = 0x0,//seninf1
	CSI0B = 0x1,//seninf2
	CSI1  = 0x2,//seninf3
	CSI2  = 0x4,//seninf5
};



