/*
 * Copyright Statement:

 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.

 * MediaTek Inc. (C) 2019. All rights reserved.

 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 */

#ifndef __MTK_TEE_TLSTD_H__
#define __MTK_TEE_TLSTD_H__


#if defined(__TRUSTONIC_TEE__)
//==============================================================================
// __TRUSTONIC_TEE__
//==============================================================================

#pragma message ( "TRUSTONIC_TEE" )
#include "tlStd.h"
#include "tee_error.h"

#elif defined(__MICROTRUST_TEE__)
//==============================================================================
// __MICROTRUST_TEE__
//==============================================================================

#pragma message ( "MICROTRUST_TEE" )

#define DECLARE_TRUSTLET_MAIN_STACK(_size_)
#define DECLARE_TRUSTED_APPLICATION_MAIN_STACK  DECLARE_TRUSTLET_MAIN_STACK

#ifndef EOL
    #define EOL "\n"
#endif

#if !defined(TRUE)
    #define TRUE    (1==1)
#endif // !defined(TRUE)

#if !defined(FALSE)
    #define FALSE   (1!=1)
#endif // !defined(TRUE)

#ifndef NULL
#  ifdef __cplusplus
#     define NULL  0
#  else
#     define NULL  ((void *)0)
#  endif
#endif

#define IN
#define OUT

//#define TLAPI_OK                            0x00000000
#define TEE_ALLOCATION_HINT_ZEROED           0x00000000
//#define TEE_MALLOC_FILL_ZERO                 TEE_ALLOCATION_HINT_ZEROED


#define TEE_DbgPrintLnf(...)      do{TEE_DbgPrintf(__VA_ARGS__);TEE_DbgPrintf(EOL);}while(FALSE)

#elif defined(__TRUSTY_TEE__)
//==============================================================================
// __TRUSTY_TEE__
//==============================================================================

#pragma message ( "TRUSTY_TEE" )

#elif defined(__BLOWFISH_TEE__)
//==============================================================================
// __BLOWFISH_TEE__
//==============================================================================

#pragma message ( "BLOWFISH_TEE" )

#include <stdint.h>
#include <stdio.h>

//==============================================================================
// C/C++ compatibility
//==============================================================================

#if defined(__cplusplus)

    #define _EXTERN_C                extern "C"
    #define _BEGIN_EXTERN_C          extern "C" {
    #define _END_EXTERN_C            }

#else

    #define _EXTERN_C
    #define _BEGIN_EXTERN_C
    #define _END_EXTERN_C

#endif // defined(__cplusplus)

#define _NORETURN           __attribute__((noreturn))
#define _TLAPI_EXTERN_C     _EXTERN_C
#define _TLAPI_NORETURN     _NORETURN

#if !defined(TRUE)
    #define TRUE    (1==1)
#endif // !defined(TRUE)

#if !defined(FALSE)
    #define FALSE   (1!=1)
#endif // !defined(TRUE)

#ifndef NULL
#  ifdef __cplusplus
#     define NULL  0
#  else
#     define NULL  ((void *)0)
#  endif
#endif

typedef void        *void_ptr;  /**< a pointer to anything. */
typedef void_ptr    addr_t;     /**< an address, can be physical or virtual */
typedef uint32_t    bool_t;     /**< boolean data type. */

#define DECLARE_TRUSTLET_MAIN_STACK(_size_)
#define DECLARE_TRUSTED_APPLICATION_MAIN_STACK  DECLARE_TRUSTLET_MAIN_STACK

#ifndef EOL
    #define EOL "\n"
#endif

#define IN
#define OUT

#define TEE_ALLOCATION_HINT_ZEROED           0x00000000
//#define TEE_MALLOC_FILL_ZERO                 TEE_ALLOCATION_HINT_ZEROED

//#define TEE_LogPrintf printf
//#define TEE_DbgPrintf msee_ta_printf
//#define TEE_DbgPrintLnf(...)      do{TEE_DbgPrintf(__VA_ARGS__);TEE_DbgPrintf(EOL);}while(FALSE)

#else
#error "NO TEE SUPPORT defined!!!"

#endif


#endif /* __MTK_TEE_TLSTD_H__ */
