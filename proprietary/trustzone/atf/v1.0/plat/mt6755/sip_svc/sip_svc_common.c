/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2014. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include <arch.h>
#include <arch_helpers.h>
#include <assert.h>
#include <bl_common.h>	/* NON_SECURE */
#include <context_mgmt.h>	/* cpu_context_t */
#include <runtime_svc.h>
#include <debug.h>
#include <sip_svc.h>
#include <sip_error.h>
#include <platform.h>
#include <mmio.h>
#include <console.h> //set_uart_flag(), clear_uart_flag();
#include "plat_private.h"   //for atf_arg_t_ptr
#include "sip_private.h"
#include "mt_cpuxgpt.h"
#include <log.h>

#include <emi_drv.h>
#include <rpmb_hmac.h>
#ifdef SECURE_DEINT_SUPPORT
#include <eint.h>
#endif
#include <gpio.h>
#include <rng.h>
#include <md.h>

/*******************************************************************************
 * SIP top level handler for servicing SMCs.
 ******************************************************************************/

static struct kernel_info k_info;

static void save_kernel_info(uint64_t pc, uint64_t r0, uint64_t r1,
								uint64_t k32_64)
{
	k_info.k32_64 = k32_64;
	k_info.pc = pc;

	if (k32_64 == LINUX_KERNEL_32) {
		/* for 32 bits kernel */
		k_info.r0 = 0;
		k_info.r1 = r0; /* machtype */
		k_info.r2 = r1; /* tags */
	} else {
		/* for 64 bits kernel */
		k_info.r0 = r0;
		k_info.r1 = r1;
	}
}

uint64_t get_kernel_k32_64(void)
{
	return k_info.k32_64;
}

uint64_t get_kernel_info_pc(void)
{
	return k_info.pc;
}

uint64_t get_kernel_info_r0(void)
{
	return k_info.r0;
}

uint64_t get_kernel_info_r1(void)
{
	return k_info.r1;
}
uint64_t get_kernel_info_r2(void)
{
	return k_info.r2;
}

extern void bl31_prepare_kernel_entry(uint64_t k32_64);
extern void el3_exit(void);
extern int start_devapc(void);
extern void clear_sramrom_violation(void);
extern unsigned long g_dormant_log_base;

/*******************************************************************************
 * SMC Call for Kernel MCUSYS register write
 ******************************************************************************/

static uint64_t mcusys_write_count;
static uint64_t sip_mcusys_write(unsigned int reg_addr, unsigned int reg_value)
{
	if ((reg_addr & 0xFFFF0000) != (MCUCFG_BASE & 0xFFFF0000))
		return SIP_SVC_E_INVALID_Range;

	/* Perform range check */
	if ((reg_addr >= MP0_MISC_CONFIG0 && reg_addr <= MP0_MISC_CONFIG9) ||
	    (reg_addr >= MP1_MISC_CONFIG0 && reg_addr <= MP1_MISC_CONFIG9)) {
		return SIP_SVC_E_PERMISSION_DENY;
	}

	if (check_cpuxgpt_write_permission(reg_addr, reg_value) == 0) {
		/* Not allow to clean enable bit[0], Force to set bit[0] as 1 */
		reg_value |= 0x1;
	}

	mmio_write_32(reg_addr, reg_value);
	dsb();

	mcusys_write_count++;

	return SIP_SVC_E_SUCCESS;
}

/*******************************************************************************
 * SMC call from LK to recieve the root of trust info
 ******************************************************************************/
#define ROOT_OF_TRUST_SIZE 48
#define COMPILE_ASSERT(cond, msg)    typedef char msg[(cond) ? 1 : -1] __unused

struct root_of_trust_info_t {
	unsigned char pubk_hash[32];
	int device_lock_state;
	int verify_boot_state; /* green:0x0, orange:0x1, yellow:0x2, red: 0x3 */
	unsigned char os_version[4];
	unsigned char reserved[4];
};

static struct root_of_trust_info_t g_rot_info = {{0} };
static uint32_t *p_root_of_trust_info = (uint32_t *)(uintptr_t) &g_rot_info;
static uint8_t rot_write_enable = 1;
static uint8_t rot_smc_count;

static uint64_t sip_save_root_of_trust_info(uint32_t arg0, uint32_t arg1,
	uint32_t arg2, uint32_t arg3)
{
	COMPILE_ASSERT(sizeof(struct root_of_trust_info_t) == ROOT_OF_TRUST_SIZE, RoT_wrong_size);

	INFO("ROT : 0x%x, 0x%x, 0x%x, 0x%x\n", arg0, arg1, arg2, arg3);

	if (!rot_write_enable)
		return SIP_SVC_E_PERMISSION_DENY;

	rot_smc_count++;
	/* currently, only 3 smc calls will be fired from LK */
	if (rot_smc_count >= 3)
		rot_write_enable = 0;

	*p_root_of_trust_info = arg0;
	p_root_of_trust_info++;
	*p_root_of_trust_info = arg1;
	p_root_of_trust_info++;
	*p_root_of_trust_info = arg2;
	p_root_of_trust_info++;
	*p_root_of_trust_info = arg3;
	p_root_of_trust_info++;

	return SIP_SVC_E_SUCCESS;
}

/*******************************************************************************
 * SMC call from LK TEE OS to retrieve the root of trust info
 ******************************************************************************/
static uint32_t *p_root_of_trust_info_head = (uint32_t *)(uintptr_t) &g_rot_info;
static uint32_t *p_root_of_trust_info_pos = (uint32_t *)(uintptr_t) &g_rot_info;
static uint64_t sip_get_root_of_trust_info(uint32_t *arg0, uint32_t *arg1,
											uint32_t *arg2, uint32_t *arg3)
{
	COMPILE_ASSERT(sizeof(struct root_of_trust_info_t) == ROOT_OF_TRUST_SIZE, RoT_wrong_size);

	if (rot_write_enable)
		return SIP_SVC_E_PERMISSION_DENY;

	/* we use the same SMC call ID for getting whole structure data
	 * therefore, reset the pos if it reaches the end
	 */
	if (4 * (p_root_of_trust_info_pos - p_root_of_trust_info_head) >= sizeof(struct root_of_trust_info_t))
		p_root_of_trust_info_pos = p_root_of_trust_info_head;

	/* get 16 bytes each time */
	*arg0 = *p_root_of_trust_info_pos;
	p_root_of_trust_info_pos++;
	*arg1 = *p_root_of_trust_info_pos;
	p_root_of_trust_info_pos++;
	*arg2 = *p_root_of_trust_info_pos;
	p_root_of_trust_info_pos++;
	*arg3 = *p_root_of_trust_info_pos;
	p_root_of_trust_info_pos++;

	INFO("ROT RET: 0x%x, 0x%x, 0x%x, 0x%x\n", *arg0, *arg1, *arg2, *arg3);

	return SIP_SVC_E_SUCCESS;
}

/*******************************************************************************
 * SIP top level handler for servicing SMCs.
 ******************************************************************************/
uint64_t sip_smc_handler(uint32_t smc_fid,
							uint64_t x1,
							uint64_t x2,
							uint64_t x3,
							uint64_t x4,
							void *cookie,
							void *handle,
							uint64_t flags)
{
	uint64_t rc = 0;
	uint32_t ns;
	atf_arg_t_ptr teearg = &gteearg;
	uint32_t atf_crash_log_addr;
	uint32_t atf_crash_log_size;
	uint32_t atf_crash_flag_addr;
	uint32_t *atf_crash_flag_addr_ptr;
	uint32_t rnd_val = 0;


	cpu_context_t *ns_cpu_context;
	uint64_t spsr = 0;

	/* Determine which security state this SMC originated from */
	ns = is_caller_non_secure(flags);
	set_uart_flag();
	if (!ns) {
		/* SiP SMC service secure world's call */
		switch (smc_fid) {
		case MTK_SIP_TBASE_HWUID_AARCH32: {
			SMC_RET4(handle, teearg->hwuid[0], teearg->hwuid[1],
						teearg->hwuid[2], teearg->hwuid[3]);
			break;
		}
		case MTK_SIP_GET_ROOT_OF_TRUST_AARCH32:
		case MTK_SIP_GET_ROOT_OF_TRUST_AARCH64: {
			uint32_t arg0, arg1, arg2, arg3;
			/* only allow from TEE which is secure */
			if (rot_write_enable)
				SMC_RET1(handle, SMC_UNK);
			rc = sip_get_root_of_trust_info(&arg0, &arg1, &arg2, &arg3);
			if (rc == SIP_SVC_E_SUCCESS)
				SMC_RET4(handle, arg0, arg1, arg2, arg3);
			break;
			}	/* case MTK_SIP_GET_ROOT_OF_TRUST_AARCH64 */
#ifdef SECURE_DEINT_SUPPORT
		case MTK_SIP_TEE_SEC_DEINT_CONFIGURE_AARCH32:
		case MTK_SIP_TEE_SEC_DEINT_CONFIGURE_AARCH64:
			rc = mt_eint_set_secure_deint(x1, x2);
			break;
		case MTK_SIP_TEE_SEC_DEINT_RELEASE_AARCH32:
		case MTK_SIP_TEE_SEC_DEINT_RELEASE_AARCH64:
			rc = mt_eint_clr_deint(x1);
			break;
#endif


		default:
			rc = SMC_UNK;
			ERROR("%s: unhandled Sec SMC (0x%x)\n", __func__, smc_fid);
		}	/* switch (smc_fid) { */
	} else {	/*	if (!ns) */
		ns_cpu_context = (cpu_context_t *) cm_get_context_by_mpidr(read_mpidr(), NON_SECURE);
		spsr = SMC_GET_EL3(ns_cpu_context, CTX_SPSR_EL3);
		if (mtk_lk_stage == 1) {
			if (GET_RW(spsr) == MODE_RW_64) {
			/* INFO("originated from LK 64\n"); */
				switch (smc_fid) {

				default:
					rc = SMC_UNK;
					ERROR("%s: unknown 64b LK SMC(0x%x)\n", __func__, smc_fid);
				}
			} else if ((GET_RW(spsr) == MODE_RW_32) &&
				((spsr & MODE32_MASK) == MODE32_svc)) {
				switch (smc_fid) {
				case MTK_SIP_LK_ROOT_OF_TRUST_AARCH32:
					/* only allow from LK which is non-secure */
					if (!rot_write_enable)
						SMC_RET1(handle, SMC_UNK);
					rc = sip_save_root_of_trust_info(x1, x2, x3, x4);
					break;
				case MTK_SIP_KERNEL_BOOT_AARCH32:
					set_uart_flag();
					printf("save kernel info\n");
					save_kernel_info(x1, x2, x3, x4);
					bl31_prepare_kernel_entry(x4);
					printf("EMI MPUS=0x%x; MPUT=0x%x\n",
							mmio_read_32(0x102031F0), mmio_read_32(0x102031F8));
					printf("el3_exit\n");
					clear_uart_flag();
					SMC_RET0(handle);
					break;
				case MTK_SIP_LK_DAPC_INIT_AARCH32:
					/* Setup DAPC in ATF */
					rc = start_devapc();
					break;
				case MTK_SIP_LK_MD_REG_WRITE_AARCH32:
					sip_write_md_regs((uint32_t)x1, (uint32_t)x2, (uint32_t)x3, (uint32_t)x4);
					break;
				case MTK_SIP_LK_RPMB_INIT_AARCH32:
					/* create shared memory for rpmb atf module */
					rc = rpmb_init();
					break;
				case MTK_SIP_LK_RPMB_UNINIT_AARCH32:
					/* mark leaving lk and release resources. */
					rc = rpmb_uninit();
					break;
				case MTK_SIP_LK_RPMB_HMAC_AARCH32:
					/* rpmb hmac calculation module */
					rc = rpmb_hmac(x1, x2);
					break;
				case MTK_SIP_LK_GET_RND_AARCH32:
					rc = plat_get_rnd(&rnd_val);
					SMC_RET2(handle, rc, rnd_val);
					break;
				case MTK_SIP_LK_DUMP_ATF_LOG_INFO_AARCH32:
					set_uart_flag();
					atf_crash_log_addr = mt_log_get_crash_log_addr();
					atf_crash_log_size = mt_log_get_crash_log_size();
					atf_crash_flag_addr_ptr = mt_log_get_crash_flag_addr();
					atf_crash_flag_addr = (uint64_t)atf_crash_flag_addr_ptr & 0xffffffff;

					printf("LK Dump\n");
					printf("buf addr:0x%x ,", atf_crash_log_addr);
					printf("buf size:%u ,", atf_crash_log_size);
					printf("flag addr:0x%x ,", atf_crash_flag_addr);
					printf("flag:0x%x\n", *atf_crash_flag_addr_ptr);
					clear_uart_flag();
					SMC_RET3(handle, atf_crash_log_addr, atf_crash_log_size, atf_crash_flag_addr);
					break;
				case MTK_SIP_KERNEL_EMIMPU_SET_AARCH32:
					rc = sip_emimpu_set_region_protection(x1, x2, x3);
					break;

				default:
					rc = SMC_UNK;
					ERROR("%s: unknown 32b LK SMC(0x%x)\n", __func__, smc_fid);
				}
			} else {
				rc = SMC_UNK;
				ERROR("%s: unknown LK SMC(0x%x)\n", __func__, smc_fid);
			}
		} else if (mtk_lk_stage == 0) {
			if (GET_RW(spsr) == MODE_RW_64) {
				/* INFO("originated from kernel 64\n"); */
				switch (smc_fid) {
				case MTK_SIP_KERNEL_MCUSYS_WRITE_AARCH32:
				case MTK_SIP_KERNEL_MCUSYS_WRITE_AARCH64:
					rc = sip_mcusys_write(x1, x2);
					break;
				case MTK_SIP_KERNEL_MCUSYS_ACCESS_COUNT_AARCH32:
				case MTK_SIP_KERNEL_MCUSYS_ACCESS_COUNT_AARCH64:
					rc = mcusys_write_count;
					break;
				case MTK_SIP_KERNEL_DAPC_INIT_AARCH32:
				case MTK_SIP_KERNEL_DAPC_INIT_AARCH64:
					/* Setup DAPC in ATF */
					start_devapc();
					break;
				case MTK_SIP_KERNEL_DAPC_CLEAR_SRAMROM_VIO_AARCH32:
				case MTK_SIP_KERNEL_DAPC_CLEAR_SRAMROM_VIO_AARCH64:

					/* Clear SRAMROM violation in ATF */
					clear_sramrom_violation();
					break;
				case MTK_SIP_KERNEL_EMIMPU_WRITE_AARCH32:
				case MTK_SIP_KERNEL_EMIMPU_WRITE_AARCH64:
					rc = sip_emimpu_write(x1, x2);
					break;
				case MTK_SIP_KERNEL_EMIMPU_READ_AARCH32:
				case MTK_SIP_KERNEL_EMIMPU_READ_AARCH64:
					rc = (uint64_t)sip_emimpu_read(x1);
					break;
				case MTK_SIP_KERNEL_EMIMPU_SET_AARCH32:
				case MTK_SIP_KERNEL_EMIMPU_SET_AARCH64:
					rc = sip_emimpu_set_region_protection(x1, x2, x3);
					break;
				case MTK_SIP_KERNEL_GIC_DUMP_AARCH32:
				case MTK_SIP_KERNEL_GIC_DUMP_AARCH64:
					rc = mt_irq_dump_status(x1);
					break;
				case MTK_SIP_KERNEL_WDT_AARCH32:
				case MTK_SIP_KERNEL_WDT_AARCH64:
					wdt_kernel_cb_addr = x1;
					printf("MTK_SIP_KERNEL_WDT : 0x%lx\n", wdt_kernel_cb_addr);
					printf("teearg->atf_aee_debug_buf_start : 0x%x\n",
						   teearg->atf_aee_debug_buf_start);
					rc = teearg->atf_aee_debug_buf_start;
					break;
				case MTK_SIP_KERNEL_MSG_AARCH32:
				case MTK_SIP_KERNEL_MSG_AARCH64:
					if (x1 == 0x0 && x2 == 2) /* set */
						g_dormant_log_base = x3;

					rc = SIP_SVC_E_SUCCESS;
					break;
				case MTK_SIP_KERNEL_SECURE_IRQ_MIGRATE:
					rc = mt_irq_migrate(x1, x2);
					break;
				case MTK_SIP_KERNEL_TIME_SYNC_AARCH32:
				case MTK_SIP_KERNEL_TIME_SYNC_AARCH64:
					printf("kernel time sync 0x%16lx 0x%16lx atf: 0x%16lx\n",
							x1, x2, atf_sched_clock());
					/* in arch32, high 32 bits is stored in x2 and this would be 0 in arch64 */
					atf_sched_clock_init(atf_sched_clock() - (x1 + (x2 << 32)), 0);
					MT_LOG_KTIME_SET();
					rc = SIP_SVC_E_SUCCESS;
					break;
				case MTK_SIP_KERNEL_GPIO_WRITE_AARCH64:
					/*INFO("SIP_KGPIO: 0x%lx 0x%lx atf: 0\n", x1, x2);*/
					/* in arch32, high 32 bits is stored in x2 and this would be 0 in arch64 */
					if (x1 <= 0x6f0)
						GPIO_write(x1, x2);
					rc = SIP_SVC_E_SUCCESS;
					break;
				case MTK_SIP_KERNEL_GPIO_READ_AARCH64:
					/*INFO("SIP_KGPIO: 0x%lx 0x%lx atf: 0\n", x1, x2);*/
					/* in arch32, high 32 bits is stored in x2 and this would be 0 in arch64 */
					if (x1 <= 0x6f0)
						rc = GPIO_read(x1);
					break;

				default:
					rc = SMC_UNK;
					ERROR("%s: unknown 64B kernel SMC(0x%x)\n", __func__, smc_fid);
				}
			} else if ((GET_RW(spsr) == MODE_RW_32) &&
				((spsr & MODE32_MASK) == MODE32_svc)) {
					/* INFO("originated from kernel 32\n"); */
				switch (smc_fid) {
				case MTK_SIP_KERNEL_MCUSYS_WRITE_AARCH32:
					rc = sip_mcusys_write(x1, x2);
					break;
				case MTK_SIP_KERNEL_MCUSYS_ACCESS_COUNT_AARCH32:
					rc = mcusys_write_count;
					break;
				case MTK_SIP_KERNEL_DAPC_INIT_AARCH32:
					/* Setup DAPC in ATF */
					start_devapc();
					break;
				case MTK_SIP_KERNEL_DAPC_CLEAR_SRAMROM_VIO_AARCH32:
					/* Clear SRAMROM violation in ATF */
					clear_sramrom_violation();
					break;
				case MTK_SIP_KERNEL_EMIMPU_WRITE_AARCH32:
					rc = sip_emimpu_write(x1, x2);
					break;
				case MTK_SIP_KERNEL_EMIMPU_READ_AARCH32:
					rc = (uint64_t)sip_emimpu_read(x1);
					break;
				case MTK_SIP_KERNEL_EMIMPU_SET_AARCH32:
					rc = sip_emimpu_set_region_protection(x1, x2, x3);
					break;
				case MTK_SIP_KERNEL_GIC_DUMP_AARCH32:
					rc = mt_irq_dump_status(x1);
					break;
				case MTK_SIP_KERNEL_WDT_AARCH32:
					wdt_kernel_cb_addr = x1;
					printf("MTK_SIP_KERNEL_WDT : 0x%lx\n", wdt_kernel_cb_addr);
					printf("teearg->atf_aee_debug_buf_start : 0x%x\n",
						   teearg->atf_aee_debug_buf_start);
					rc = teearg->atf_aee_debug_buf_start;
					break;
				case MTK_SIP_KERNEL_MSG_AARCH32:
					if (x1 == 0x0 && x2 == 2) /* set */
						g_dormant_log_base = x3;
					rc = SIP_SVC_E_SUCCESS;
					break;
				case MTK_SIP_KERNEL_SECURE_IRQ_MIGRATE:
					rc = mt_irq_migrate(x1, x2);
					break;
				case MTK_SIP_KERNEL_TIME_SYNC_AARCH32:
					printf("kernel time sync 0x%16lx 0x%16lx atf: 0x%16lx\n",
							x1, x2, atf_sched_clock());
					/* in arch32, high 32 bits is stored in x2 and this would be 0 in arch64 */
					atf_sched_clock_init(atf_sched_clock() - (x1 + (x2 << 32)), 0);
					MT_LOG_KTIME_SET();
					rc = SIP_SVC_E_SUCCESS;
					break;
				case MTK_SIP_KERNEL_GPIO_WRITE_AARCH32:
					/*INFO("SIP_KGPIO: 0x%lx 0x%lx atf: 0\n", x1, x2);*/
					/* in arch32, high 32 bits is stored in x2 and this would be 0 in arch64 */
					if (x1 <= 0x6f0)
						GPIO_write(x1, x2);
					rc = SIP_SVC_E_SUCCESS;
					break;
				case MTK_SIP_KERNEL_GPIO_READ_AARCH32:
					/*INFO("SIP_KGPIO: 0x%lx 0x%lx atf: 0\n", x1, x2);*/
					/* in arch32, high 32 bits is stored in x2 and this would be 0 in arch64 */
					if (x1 <= 0x6f0)
						rc = GPIO_read(x1);
					break;

				default:
					rc = SMC_UNK;
					ERROR("%s: unknown 32B kernel SMC(0x%x)\n", __func__, smc_fid);
				}
			} else {
				rc = SMC_UNK;
				ERROR("%s: unknown runtime SMC(0x%x)\n", __func__, smc_fid);
			}
		} else {
			rc = SMC_UNK;
			ERROR("%s: unknown stage SMC(0x%x)\n", __func__, smc_fid);
		}
	}
	clear_uart_flag();

	SMC_RET1(handle, rc);
}

