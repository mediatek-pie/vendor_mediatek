//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mtkJava

// This file contains the module types for compiling Java for Android, and converts the properties
// into the flags and filenames necessary to pass to the Module.  The final creation of the rules
// is handled in builder.go

import (
	"path/filepath"
	"strconv"
	"strings"

	"github.com/google/blueprint"
	"github.com/google/blueprint/proptools"

	"android/soong/android"
	"android/soong/java/config"
	"android/soong/java"
)

// Module contains the properties and members used by all java module types
type Module struct {
	android.ModuleBase
	android.DefaultableModuleBase

	properties       java.CompilerProperties
	protoProperties  android.ProtoProperties
	deviceProperties java.CompilerDeviceProperties

	// header jar file suitable for inserting into the bootclasspath/classpath of another compile
	headerJarFile android.Path

	// full implementation jar file suitable for static dependency of another module compile
	implementationJarFile android.Path

	// output file containing classes.dex
	dexJarFile android.Path

	// output file containing uninstrumented classes that will be instrumented by jacoco
	jacocoReportClassesFile android.Path

	// output file containing mapping of obfuscated names
	proguardDictionary android.Path

	// output file suitable for installing or running
	outputFile android.Path

	exportAidlIncludeDirs android.Paths

	logtagsSrcs android.Paths

	// installed file for binary dependency
	installFile android.Path

	// list of .java files and srcjars that was passed to javac
	compiledJavaSrcs android.Paths
	compiledSrcJars  android.Paths

	// list of extra progurad flag files
	extraProguardFlagFiles android.Paths
}

type Dependency interface {
	HeaderJars() android.Paths
	ImplementationJars() android.Paths
	AidlIncludeDirs() android.Paths
}

type dependencyTag struct {
	blueprint.BaseDependencyTag
	name string
}

var (
	staticLibTag     = dependencyTag{name: "staticlib"}
	libTag           = dependencyTag{name: "javalib"}
	bootClasspathTag = dependencyTag{name: "bootclasspath"}
	systemModulesTag = dependencyTag{name: "system modules"}
	frameworkResTag  = dependencyTag{name: "framework-res"}
	kotlinStdlibTag  = dependencyTag{name: "kotlin-stdlib"}
	proguardRaiseTag = dependencyTag{name: "proguard-raise"}
)

type sdkDep struct {
	useModule, useFiles, useDefaultLibs, invalidVersion bool

	module        string
	systemModules string

	jar  android.Path
	aidl android.Path
}

func sdkStringToNumber(ctx android.BaseContext, v string) int {
	switch v {
	case "", "current", "system_current", "test_current", "core_current":
		return android.FutureApiLevel
	default:
		if i, err := strconv.Atoi(android.GetNumericSdkVersion(v)); err != nil {
			ctx.PropertyErrorf("sdk_version", "invalid sdk version")
			return -1
		} else {
			return i
		}
	}
}

func (j *Module) shouldInstrument(ctx android.BaseContext) bool {
	return j.properties.Instrument && ctx.Config().IsEnvTrue("EMMA_INSTRUMENT")
}

func decodeSdkDep(ctx android.BaseContext, v string) sdkDep {
	i := sdkStringToNumber(ctx, v)
	if i == -1 {
		// Invalid sdk version, error handled by sdkStringToNumber.
		return sdkDep{}
	}

	// Ensures that the specificed system SDK version is one of BOARD_SYSTEMSDK_VERSIONS (for vendor apks)
	// or PRODUCT_SYSTEMSDK_VERSIONS (for other apks or when BOARD_SYSTEMSDK_VERSIONS is not set)
	if strings.HasPrefix(v, "system_") && i != android.FutureApiLevel {
		allowed_versions := ctx.DeviceConfig().PlatformSystemSdkVersions()
		if ctx.DeviceSpecific() || ctx.SocSpecific() {
			if len(ctx.DeviceConfig().SystemSdkVersions()) > 0 {
				allowed_versions = ctx.DeviceConfig().SystemSdkVersions()
			}
		}
		version := strings.TrimPrefix(v, "system_")
		if len(allowed_versions) > 0 && !android.InList(version, allowed_versions) {
			ctx.PropertyErrorf("sdk_version", "incompatible sdk version %q. System SDK version should be one of %q",
				v, allowed_versions)
		}
	}

	toFile := func(v string) sdkDep {
		isCore := strings.HasPrefix(v, "core_")
		if isCore {
			v = strings.TrimPrefix(v, "core_")
		}
		dir := filepath.Join("prebuilts/sdk", v)
		jar := filepath.Join(dir, "android.jar")
		if isCore {
			jar = filepath.Join(dir, "core.jar")
		}
		aidl := filepath.Join(dir, "framework.aidl")
		jarPath := android.ExistentPathForSource(ctx, jar)
		aidlPath := android.ExistentPathForSource(ctx, aidl)

		if (!jarPath.Valid() || !aidlPath.Valid()) && ctx.Config().AllowMissingDependencies() {
			return sdkDep{
				invalidVersion: true,
				module:         "sdk_v" + v,
			}
		}

		if !jarPath.Valid() {
			ctx.PropertyErrorf("sdk_version", "invalid sdk version %q, %q does not exist", v, jar)
			return sdkDep{}
		}

		if !aidlPath.Valid() {
			ctx.PropertyErrorf("sdk_version", "invalid sdk version %q, %q does not exist", v, aidl)
			return sdkDep{}
		}

		return sdkDep{
			useFiles: true,
			jar:      jarPath.Path(),
			aidl:     aidlPath.Path(),
		}
	}

	//toModule := func(m string) sdkDep {
	//	return sdkDep{
	//		useModule:     true,
	//		module:        m,
	//		systemModules: m + "_system_modules",
	//	}
	//}

	if ctx.Config().UnbundledBuild() && v != "" {
		return toFile(v)
	}

	switch v {
	case "":
		return sdkDep{
			useDefaultLibs: true,
		}
	// TODO(ccross): re-enable these once we generate stubs, until then
	// use the stubs in prebuilts/sdk/*current
	//case "current":
	//	return toModule("android_stubs_current")
	//case "system_current":
	//	return toModule("android_system_stubs_current")
	//case "test_current":
	//	return toModule("android_test_stubs_current")
	default:
		return toFile(v)
	}
}

func (j *Module) deps(ctx android.BottomUpMutatorContext) {
	if ctx.Device() {
		if !proptools.Bool(j.properties.No_standard_libs) {
			sdkDep := decodeSdkDep(ctx, proptools.String(j.deviceProperties.Sdk_version))
			if sdkDep.useDefaultLibs {
				ctx.AddDependency(ctx.Module(), bootClasspathTag, config.DefaultBootclasspathLibraries...)
				if ctx.Config().TargetOpenJDK9() {
					ctx.AddDependency(ctx.Module(), systemModulesTag, config.DefaultSystemModules)
				}
				if !proptools.Bool(j.properties.No_framework_libs) {
					ctx.AddDependency(ctx.Module(), libTag, config.DefaultLibraries...)
				}
			} else if sdkDep.useModule {
				if ctx.Config().TargetOpenJDK9() {
					ctx.AddDependency(ctx.Module(), systemModulesTag, sdkDep.systemModules)
				}
				ctx.AddDependency(ctx.Module(), bootClasspathTag, sdkDep.module)
				if proptools.Bool(j.deviceProperties.Optimize.Enabled) {
					ctx.AddDependency(ctx.Module(), proguardRaiseTag, config.DefaultBootclasspathLibraries...)
					ctx.AddDependency(ctx.Module(), proguardRaiseTag, config.DefaultLibraries...)
				}
			}
		} else if j.deviceProperties.System_modules == nil {
			ctx.PropertyErrorf("no_standard_libs",
				"system_modules is required to be set when no_standard_libs is true, did you mean no_framework_libs?")
		} else if *j.deviceProperties.System_modules != "none" && ctx.Config().TargetOpenJDK9() {
			ctx.AddDependency(ctx.Module(), systemModulesTag, *j.deviceProperties.System_modules)
		}
		if ctx.ModuleName() == "framework" {
			ctx.AddDependency(ctx.Module(), frameworkResTag, "framework-res")
		}
	}

	ctx.AddDependency(ctx.Module(), libTag, j.properties.Libs...)
	ctx.AddDependency(ctx.Module(), staticLibTag, j.properties.Static_libs...)
	ctx.AddDependency(ctx.Module(), libTag, j.properties.Annotation_processors...)

	android.ExtractSourcesDeps(ctx, j.properties.Srcs)
	android.ExtractSourcesDeps(ctx, j.properties.Exclude_srcs)
	android.ExtractSourcesDeps(ctx, j.properties.Java_resources)
	android.ExtractSourceDeps(ctx, j.properties.Manifest)
}


func shardPaths(paths android.Paths, shardSize int) []android.Paths {
	ret := make([]android.Paths, 0, (len(paths)+shardSize-1)/shardSize)
	for len(paths) > shardSize {
		ret = append(ret, paths[0:shardSize])
		paths = paths[shardSize:]
	}
	if len(paths) > 0 {
		ret = append(ret, paths)
	}
	return ret
}

func (j *Module) HeaderJars() android.Paths {
	return android.Paths{j.headerJarFile}
}

func (j *Module) ImplementationJars() android.Paths {
	return android.Paths{j.implementationJarFile}
}

func (j *Module) AidlIncludeDirs() android.Paths {
	return j.exportAidlIncludeDirs
}

