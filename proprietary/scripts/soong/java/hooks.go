package mtkJava

import (
	"reflect"
	"strings"

	"android/soong/android"
	"android/soong/java"
	"android/soong/android/mediatek"
)

func init() {
	android.RegisterModuleType("mtk_java_defaults", mtkJavaDefaultsFactory)
}

func mtkJavaLoadHook(ctx android.LoadHookContext, c interface{}) {
	customStruct := reflect.ValueOf(c).Elem().FieldByName("Mediatek_variables")
	for j := 0; j < customStruct.NumField(); j++ {
		variableStruct := customStruct.Field(j)
		if variableStruct.Kind() == reflect.Ptr {
			variableStruct = variableStruct.Elem()
		}
		if variableStruct.Kind() == reflect.Struct {
			variableName := customStruct.Type().Field(j).Name
			featureName := strings.ToUpper(variableName)
			featureBool := true
			if strings.HasPrefix(variableName, "Not_") {
				featureBool = false
				featureName = strings.ToUpper(strings.TrimLeft(variableName, "Not_"))
			}
			featureValue := mediatek.GetFeature(featureName)
			if ((featureBool == true) && (featureValue == "yes" || featureValue == "true")) ||
				((featureBool == false) && (featureValue != "yes") && (featureValue != "true")) {
				ctx.AppendProperties(variableStruct.Addr().Interface())
			}
		}
	}
	return
}

func mtkJavaDefaultsFactory() android.Module {
	module := java.DefaultsFactory()
	c := mediatek.InitJavaProperties().Interface()
	module.AddProperties(c)
	android.AddLoadHook(module, func(ctx android.LoadHookContext) { mtkJavaLoadHook(ctx, c) })
	return module
}
