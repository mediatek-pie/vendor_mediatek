package mtkCc

import (
	"reflect"
	"regexp"
	"strings"

	"android/soong/android"
	"android/soong/android/mediatek"
)

func mtkCcLoadHook(ctx android.LoadHookContext, c interface{}) {
	customStruct := reflect.ValueOf(c).Elem().FieldByName("Mediatek_variables")
	for j := 0; j < customStruct.NumField(); j++ {
		variableStruct := customStruct.Field(j)
		if variableStruct.Kind() == reflect.Ptr {
			variableStruct = variableStruct.Elem()
		}
		if variableStruct.Kind() == reflect.Struct {
			variableName := customStruct.Type().Field(j).Name
			featureName := strings.ToUpper(variableName)
			featureBool := true
			if strings.HasPrefix(variableName, "Not_") {
				featureBool = false
				featureName = strings.ToUpper(strings.TrimLeft(variableName, "Not_"))
			}
			featureValue := mediatek.GetFeature(featureName)
			if ((featureBool == true) && (featureValue == "yes" || featureValue == "true")) ||
				((featureBool == false) && (featureValue != "yes") && (featureValue != "true")) {
				ctx.AppendProperties(variableStruct.Addr().Interface())
			}
		}
	}
	return
}

func checkPlatformConditional(conditional string) (bool, string) {
	if conditional == "" {
		return false, ""
	}
	var disabled bool
	var prefix string
	pattern := regexp.MustCompile(`([0-9A-Z_]+)(==)(.*)`)
	if matches := pattern.FindStringSubmatch(conditional); matches != nil {
		var value string
		if matches[1] == "MTK_PLATFORM_DIR" {
			value = strings.ToLower(mediatek.GetFeature("MTK_PLATFORM"))
		} else {
			value = mediatek.GetFeature(matches[1])
		}
		if (matches[2] == "==") && (matches[3] == value) {
		} else if (matches[2] == "!=") && (matches[3] != value) {
		} else {
			disabled = true
			prefix = matches[3] + "_"
		}
	}
	return disabled, prefix
}
