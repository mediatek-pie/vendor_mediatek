package mtkCc

import (
	"android/soong/android"
	"android/soong/cc"
	"android/soong/android/mediatek"
)

func init() {
	android.RegisterModuleType("mtk_cc_defaults", mtkCcDefaultsFactory)
	android.RegisterModuleType("mtk_cc_prebuilt_defaults", mtkCcPrebuiltDefaultsFactory)
	android.RegisterModuleType("mtk_global_defaults", mtkGlobalDefaultsFactory)
}

type UnusedProperties struct {
	Mediatek_namespace struct {
		Platform_conditional string
	}
}

type Defaults struct {
	android.ModuleBase
	android.DefaultsModuleBase
	unused UnusedProperties
}

func (d *Defaults) GenerateAndroidBuildActions(ctx android.ModuleContext) {
}

func (d *Defaults) DepsMutator(ctx android.BottomUpMutatorContext) {
}

func (d *Defaults) Name() string {
	name := d.ModuleBase.Name()
	if disabled, prefix := checkPlatformConditional(d.unused.Mediatek_namespace.Platform_conditional); disabled {
		name = prefix + name
	}
	return name
}

func DefaultsFactory(props ...interface{}) android.Module {
	module := &Defaults{}

	module.AddProperties(props...)
	module.AddProperties(
		&cc.BaseProperties{},
		&cc.VendorProperties{},
		&cc.BaseCompilerProperties{},
		&cc.BaseLinkerProperties{},
		&cc.LibraryProperties{},
		&cc.FlagExporterProperties{},
		&cc.BinaryLinkerProperties{},
		&cc.TestProperties{},
		&cc.TestBinaryProperties{},
		&cc.UnusedProperties{},
		&cc.StlProperties{},
		&cc.SanitizeProperties{},
		&cc.StripProperties{},
		&cc.InstallerProperties{},
		&cc.TidyProperties{},
		&cc.CoverageProperties{},
		&cc.SAbiProperties{},
		&cc.VndkProperties{},
		&cc.LTOProperties{},
		&cc.PgoProperties{},
		&android.ProtoProperties{},
	)
	module.AddProperties(&module.unused)

	android.InitDefaultsModule(module)

	return module
}

func mtkCcDefaultsFactory() android.Module {
	module := DefaultsFactory()
	c := mediatek.InitVariableProperties().Interface()
	module.AddProperties(c)
	android.AddLoadHook(module, func(ctx android.LoadHookContext) { mtkCcLoadHook(ctx, c) })
	return module
}

type PrebuiltDefaults struct {
	android.ModuleBase
	android.DefaultsModuleBase
	unused UnusedProperties
}

func (d *PrebuiltDefaults) GenerateAndroidBuildActions(ctx android.ModuleContext) {
}

func (d *PrebuiltDefaults) DepsMutator(ctx android.BottomUpMutatorContext) {
}

func (d *PrebuiltDefaults) Name() string {
	name := d.ModuleBase.Name()
	if disabled, prefix := checkPlatformConditional(d.unused.Mediatek_namespace.Platform_conditional); disabled {
		name = prefix + name
	}
	return name
}

func PrebuiltDefaultsFactory(props ...interface{}) android.Module {
	module := &PrebuiltDefaults{}

	module.AddProperties(props...)
	module.AddProperties(
		&cc.BaseProperties{},
		//&cc.BaseCompilerProperties{},
		&cc.BaseLinkerProperties{},
		&cc.LibraryProperties{},
		&cc.FlagExporterProperties{},
		&cc.BinaryLinkerProperties{},
		&cc.TestProperties{},
		&cc.TestBinaryProperties{},
		&cc.UnusedProperties{},
		&cc.StlProperties{},
		&cc.SanitizeProperties{},
		&cc.StripProperties{},
		&cc.InstallerProperties{},
		&cc.TidyProperties{},
		&cc.CoverageProperties{},
		&cc.SAbiProperties{},
		&cc.VndkProperties{},
	)
	module.AddProperties(&module.unused)
	var p android.PrebuiltProperties
	module.AddProperties(&p)

	android.InitDefaultsModule(module)

	return module
}

func mtkCcPrebuiltDefaultsFactory() android.Module {
	module := PrebuiltDefaultsFactory()
	c := mediatek.InitVariableProperties().Interface()
	module.AddProperties(c)
	android.AddLoadHook(module, func(ctx android.LoadHookContext) { mtkCcLoadHook(ctx, c) })
	return module
}

func mtkGlobalDefaultsFactory() android.Module {
	module := cc.DefaultsFactory()
	android.AddLoadHook(module, mtkGlobalDefaults)
	return module
}

