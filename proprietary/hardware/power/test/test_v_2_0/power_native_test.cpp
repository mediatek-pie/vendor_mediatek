#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#define LOG_TAG "PowerTest"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sched.h>
#include <fcntl.h>
#include <errno.h>
#include <dlfcn.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <cutils/log.h>
#include <cutils/properties.h>
#include <cutils/sockets.h>
#include <utils/String16.h>
#include <binder/IServiceManager.h>
#include <binder/IPCThreadState.h>
#include <binder/Parcel.h>

#include <vendor/mediatek/hardware/power/2.0/IPower.h>
#include <vendor/mediatek/hardware/power/2.0/types.h>

using namespace vendor::mediatek::hardware::power::V2_0;

//namespace android {

enum {
    CMD_POWER_HINT = 1,
    CMD_CUS_POWER_HINT,
    CMD_QUERY_INFO,
    CMD_SCN_REG,
    CMD_SCN_CONFIG,
    CMD_SCN_UNREG,
    CMD_SCN_ENABLE,
    CMD_SCN_DISABLE,
    CMD_SCN_ULTRA_CFG,
    CMD_SCN_BINDER,
    CMD_SOCKET_TEST,
    CMD_GPU_FREQ_UT, // GPU freq unit test
};

#define _TCP_BIND_ADDR_ "com.mediatek.powerhald"

int rand31(){
    // RAND31_MAX = 2147483647
    return ( (rand() << 16) | (rand() << 1) | (rand() & 1) );
}

int unit_test(void)
{
    int vRet, rTmp, fd = -1;
    char buf[1024];

    vRet = socket_local_client(_TCP_BIND_ADDR_, ANDROID_SOCKET_NAMESPACE_ABSTRACT, SOCK_STREAM);

    rTmp = rand31();

    if (vRet < 0)
       ALOGI("Connect to %s failed.\n", _TCP_BIND_ADDR_);
    else
        fd = vRet;

    vRet = send(fd, buf, 1024, MSG_NOSIGNAL);
    ALOGI("send vRet:%d, rTmp:%d", vRet, rTmp);

    if(fd >= 0)
        close(fd);

    return 0;
}

int unit_test_gpu(android::sp<IPower> gPowerHal)
{
    int handle_a = -1, handle_b = -1;

    handle_a = gPowerHal->scnReg();
    handle_b = gPowerHal->scnReg();

    /* config */
    gPowerHal->scnConfig(handle_a, MtkPowerCmd::CMD_SET_GPU_FREQ_MIN, 0, 0, 0, 0); // A min: opp 0
    gPowerHal->scnConfig(handle_b, MtkPowerCmd::CMD_SET_GPU_FREQ_MAX, 1, 0, 0, 0); // B max: opp 1

    /* case 1 */
    gPowerHal->scnEnable(handle_a, 100000);
    sleep(5);
    gPowerHal->scnDisable(handle_a);
    sleep(5);

    /* case 2 */
    gPowerHal->scnEnable(handle_b, 100000);
    sleep(5);
    gPowerHal->scnDisable(handle_b);
    sleep(5);

    /* case 3 */
    gPowerHal->scnEnable(handle_a, 100000);
    sleep(5);
    gPowerHal->scnEnable(handle_b, 100000);
    sleep(5);

    gPowerHal->scnDisable(handle_a);
    sleep(5);
    gPowerHal->scnDisable(handle_b);
    sleep(5);

    /* case 4 */
    gPowerHal->scnEnable(handle_b, 100000);
    sleep(5);
    gPowerHal->scnEnable(handle_a, 100000);
    sleep(5);

    gPowerHal->scnDisable(handle_b);
    sleep(5);
    gPowerHal->scnDisable(handle_a);
    sleep(5);

    return 0;
}

int power_hal_mgr_service_test(int method, int hdl, int flexcmd, int p1, int p2, int p3, int p4)
{
    int err = 0, handle = 0;
    android::Parcel data;
    android::Parcel reply;
    android::sp<android::IBinder> mPowerHalMgrBinder = nullptr;

    android::sp<android::IServiceManager> sm(android::defaultServiceManager());
    mPowerHalMgrBinder = sm->checkService(android::String16("power_hal_mgr_service"));
    if (mPowerHalMgrBinder == nullptr) {
        ALOGE("[power_hal_mgr_service_test] null binder");
        return -1;
    }

    if (mPowerHalMgrBinder != nullptr) {
        data.writeInterfaceToken(android::String16("com.mediatek.powerhalmgr.IPowerHalMgr"));
        data.writeStrongBinder(NULL);

        switch (method) {
            case 1:
                ALOGE("[power_hal_mgr_service_test] case1");
                mPowerHalMgrBinder->transact(1, data, &reply);
                break;
            case 2:
                ALOGE("[power_hal_mgr_service_test] case2");
                data.writeInt32(hdl);
                data.writeInt32(flexcmd);
                data.writeInt32(p1);
                data.writeInt32(p2);
                data.writeInt32(p3);
                data.writeInt32(p4);

                mPowerHalMgrBinder->transact(2, data, &reply);
                break;
            case 3:
                ALOGE("[power_hal_mgr_service_test] case3");
                data.writeInt32(hdl);

                mPowerHalMgrBinder->transact(3, data, &reply);
                break;
            case 4:
                ALOGE("[power_hal_mgr_service_test] case4");
                data.writeInt32(hdl);
                /*--user cmd as timeout--*/
                data.writeInt32(flexcmd);

                mPowerHalMgrBinder->transact(4, data, &reply);
                break;
            case 5:
                ALOGE("[power_hal_mgr_service_test] case5");
                data.writeInt32(hdl);

                mPowerHalMgrBinder->transact(5, data, &reply);
                break;
            case 6:
                ALOGE("[power_hal_mgr_service_test] case6");
                data.writeInt32(hdl);
                data.writeInt32(flexcmd);
                data.writeInt32(p1);
                data.writeInt32(p2);
                data.writeInt32(p3);
                data.writeInt32(p4);

                mPowerHalMgrBinder->transact(6, data, &reply);
                break;
            default:
                break;
        }
             
        if (method == 1) {
            err = reply.readExceptionCode();
            if(err < 0) {
               ALOGE("[power_hal_mgr_service_test] exception %d", err);
               return -1;
            }

            handle = reply.readInt32();
            return handle;
        }
    }

    return 0;
}

static void usage(char *cmd);

int main(int argc, char* argv[])
{
    int command=0, hint=0, timeout=0, data=0;
    int flexcmd=0, p1=0, p2=0, p3=0, p4=0;
    int handle = -1, method = 0;
    android::sp<IPower> gPowerHal;

    if(argc < 2) {
        usage(argv[0]);
        return 0;
    }

    gPowerHal = IPower::getService();
    if (gPowerHal == nullptr)
        return -1;

    command = atoi(argv[1]);
    //printf("argc:%d, command:%d\n", argc, command);
    switch(command) {
        case CMD_SCN_REG:
        case CMD_GPU_FREQ_UT:
            if(argc!=2) {
                usage(argv[0]);
                return -1;
            }
            break;

        case CMD_SCN_UNREG:
        case CMD_SCN_DISABLE:
        case CMD_SOCKET_TEST:
            if(argc!=3) {
                usage(argv[0]);
                return -1;
            }
            break;

        case CMD_POWER_HINT:
        case CMD_CUS_POWER_HINT:
        case CMD_QUERY_INFO:
        case CMD_SCN_ENABLE:
            if(argc!=4) {
                usage(argv[0]);
                return -1;
            }
            break;

        case CMD_SCN_CONFIG:
        case CMD_SCN_ULTRA_CFG:
            if(argc!=8) {
                usage(argv[0]);
                return -1;
            }
            break;

        case CMD_SCN_BINDER:
            if(argc!=9) {
                usage(argv[0]);
                return -1;
            }
            break;

        default:
            usage(argv[0]);
            return -1;
    }

    if(command == CMD_POWER_HINT || command == CMD_CUS_POWER_HINT) {
        hint = atoi(argv[2]);
        data = atoi(argv[3]);
    }
    else if(command == CMD_QUERY_INFO) {
        flexcmd = atoi(argv[2]);
        p1 = atoi(argv[3]);
    }
    else if(command == CMD_SCN_UNREG || command == CMD_SCN_DISABLE) {
        handle = atoi(argv[2]);
    }
    else if(command == CMD_SCN_ENABLE) {
        handle = atoi(argv[2]);
        timeout = atoi(argv[3]);
    }
    else if(command == CMD_SCN_CONFIG || command == CMD_SCN_ULTRA_CFG) {
        handle = atoi(argv[2]);
        flexcmd = atoi(argv[3]);
        p1 = atoi(argv[4]);
        p2 = atoi(argv[5]);
        p3 = atoi(argv[6]);
        p4 = atoi(argv[7]);
    }
    else if(command == CMD_SOCKET_TEST) {
        flexcmd = atoi(argv[2]);
    }
    else if(command == CMD_SCN_BINDER) {
        method = atoi(argv[2]);
        handle = atoi(argv[3]);
        /*-- method = 2 = scnConfig--*/
        flexcmd = atoi(argv[4]);
        p1 = atoi(argv[5]);
        p2 = atoi(argv[6]);
        p3 = atoi(argv[7]);
        p4 = atoi(argv[8]);
    }

    /* command */
    if(command == CMD_POWER_HINT)
        gPowerHal->mtkPowerHint((enum MtkPowerHint)hint, data);
    else if(command == CMD_CUS_POWER_HINT)
        gPowerHal->mtkCusPowerHint(hint, data);
    else if(command == CMD_QUERY_INFO) {
        data = gPowerHal->querySysInfo((enum MtkQueryCmd)flexcmd, p1);
        printf("data:%d\n", data);
    }
    else if(command == CMD_SCN_REG) {
        handle = gPowerHal->scnReg();
        printf("handle:%d\n", handle);
    }
    else if(command == CMD_SCN_CONFIG)
        gPowerHal->scnConfig(handle, (enum MtkPowerCmd)flexcmd, p1, p2, p3, p4);
    else if(command == CMD_SCN_UNREG)
        gPowerHal->scnUnreg(handle);
    else if(command == CMD_SCN_ENABLE)
        gPowerHal->scnEnable(handle, timeout);
    else if(command == CMD_SCN_DISABLE)
        gPowerHal->scnDisable(handle);
    else if(command == CMD_SCN_ULTRA_CFG)
        gPowerHal->scnUltraCfg(handle, flexcmd, p1, p2, p3, p4);
    else if(command == CMD_SCN_BINDER) {
        if (method > 0 && method < 7) {
            handle = power_hal_mgr_service_test(method, handle, flexcmd, p1, p2, p3, p4);
            if (method == 1)
                printf("handle:%d\n", handle);
        } else
           printf("method must gt 0 and lt 7 \n");
    }
    else if(command == CMD_SOCKET_TEST) {

    }
    else if(command == CMD_GPU_FREQ_UT) {
        unit_test_gpu(gPowerHal);
    }

    return 0;
}

static void usage(char *cmd) {
    fprintf(stderr, "\nUsage: %s command scenario\n"
                    "    command\n"
                    "        1: MTK power hint\n"
                    "        2: MTK cus power hint\n"
                    "        3: query info\n"
                    "        4: scn register\n"
                    "        5: scn config\n"
                    "        6: scn unregister\n"
                    "        7: scn enable\n"
                    "        8: scn disable\n"
                    "        9: scn ultacfg\n"
                    "        10: service test\n"
                    "        11: socket test\n"
                    "        12: gpu freq test\n", cmd);
}

//} // namespace
