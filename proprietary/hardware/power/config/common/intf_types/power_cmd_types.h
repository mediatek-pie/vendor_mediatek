
/*
 * command table extend
 */

using namespace vendor::mediatek::hardware::power::V2_0;

enum class MtkPowerCmdInternal : uint32_t {
   CMD_SET_FROM_JUMP = (int)MtkPowerCmd::CMD_SET_END_JUMP, /* 100 */
   CMD_SET_PPM_USERLIMIT_BOOST,                            /* 101 */
};
