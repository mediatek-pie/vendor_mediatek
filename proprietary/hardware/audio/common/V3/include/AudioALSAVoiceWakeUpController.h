#ifndef ANDROID_ALSA_AUDIO_VOICE_WAKE_UP_CONTROLLER_H
#define ANDROID_ALSA_AUDIO_VOICE_WAKE_UP_CONTROLLER_H

#include <tinyalsa/asoundlib.h>

#include "AudioType.h"
#include "AudioParamParser.h"
#include <AudioLock.h>

#include "AudioALSACaptureHandlerBase.h"
#include "AudioALSACaptureHandlerVOW.h"


namespace android {
//class AudioALSACaptureDataProviderBase;

class AudioALSADeviceConfigManager;
class AudioALSACaptureDataProviderVOW;

class AudioALSAVoiceWakeUpController {
public:
    virtual ~AudioALSAVoiceWakeUpController();

    static AudioALSAVoiceWakeUpController *getInstance();

    virtual status_t setVoiceWakeUpEnable(const bool enable);
    virtual bool     getVoiceWakeUpEnable();

    virtual status_t updateDeviceInfoForVoiceWakeUp();

    virtual status_t updateVOWCustParam();
    virtual bool    getVoiceWakeUpStateFromKernel();
    virtual unsigned int getVOWMicType();

    virtual status_t SeamlessRecordEnable();

#ifdef MTK_VOW_BARGE_IN_SUPPORT
    bool updateSpeakerPlaybackStatus(bool isSpeakerPlaying);
#endif

protected:
    AudioALSAVoiceWakeUpController();

    virtual status_t updateParamToKernel();
#ifdef MTK_VOW_BARGE_IN_SUPPORT
    virtual bool setBargeInEnable(const bool enable);
#endif

private:
    /**
     * singleton pattern
     */
    static AudioALSAVoiceWakeUpController *mAudioALSAVoiceWakeUpController;

    AudioALSACaptureDataProviderVOW *mVOWCaptureDataProvider;
    AudioALSACaptureHandlerBase *mCaptureHandler;
    stream_attribute_t *stream_attribute_target;
    status_t setVoiceWakeUpDebugDumpEnable(const bool enable);
    bool mDebug_Enable;

    struct mixer *mMixer; // TODO(Harvey): move it to AudioALSAHardwareResourceManager later
    struct pcm *mPcm;

    AudioLock mLock;

    bool mEnable;
#ifdef MTK_VOW_BARGE_IN_SUPPORT
    bool mBargeInEnable;
    struct pcm *mBargeInPcm;
#endif

    bool mIsUseHeadsetMic;
    AudioALSAHardwareResourceManager *mHardwareResourceManager;
    bool mIsNeedToUpdateParamToKernel;


    uint32_t mHandsetMicMode;
    uint32_t mHeadsetMicMode;
    AudioALSADeviceConfigManager *mDeviceConfigManager;
    AppHandle *mAppHandle;

    static void *dumyReadThread(void *arg);
    pthread_t hDumyReadThread;
    bool mDumpReadStart;
    AudioLock mDebugDumpLock;
    int mFd_dnn;
    AudioLock mSeamlessLock;
    int mFd_vow;
#ifdef MTK_VOW_BARGE_IN_SUPPORT
    bool mIsSpeakerPlaying;
    struct pcm_config mBargeInConfig;
#endif
};

} // end namespace android

#endif // end of ANDROID_ALSA_AUDIO_VOICE_WAKE_UP_CONTROLLER_H
