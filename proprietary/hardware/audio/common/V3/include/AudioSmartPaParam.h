#ifndef ANDROID_AUDIO_SMART_PA_PARAM_H
#define ANDROID_AUDIO_SMART_PA_PARAM_H


#include "AudioLock.h"
#include "AudioType.h"
#include "AudioALSADriverUtility.h"
#include <arsi_type.h>
#define SMARTPA_PARAM_LENGTH (256)

/* string should comapre with prefix + vendor + suffix*/
const char smartpa_aurisys_set_param_prefix[] = "AURISYS_SET_PARAM,DSP,PLAYBACK,SMARTPA";
const char smartpa_aurisys_get_param_prefix[] = "AURISYS_GET_PARAM,DSP,PLAYBACK,SMARTPA";

namespace android {

class AudioSmartPaParam {

public:
    ~AudioSmartPaParam();

    int setParameter(const char *keyValuePair);
    char *getParameter(const char *key);


    /**
     * get instance's pointer
     */
    static AudioSmartPaParam *getInstance(void);

    int SetArsiTaskConfig(const arsi_task_config_t * ArsiTaskConfig);
    int Setlibconfig(const arsi_lib_config_t * mArsiLibConfig);
    int SetSmartpaParam(int mode);
    int setParamFilePath(const char *str);

protected:

    AudioSmartPaParam();

private:
    static AudioSmartPaParam *mAudioSmartPaParam;
    void parseSetParameterStr(const char *inputStr, char **outputStr,
                              const int paramindex);
    bool checkParameter(int &paramindex, int &direction, const char *keyValuePair);
    int getsetParameterPrefixlength(int paramindex);
    int getgetParameterPrefixlength(int paramindex);
    int setProductName(const char *str);
    char *getParamFilePath(void);
    char *getProductName(void);
    int getDefalutParamFilePath(void);
    int getDefaultProductName(void);


    void initArsiTaskConfig(void);
    void initlibconfig();

    static const int paramlength = SMARTPA_PARAM_LENGTH;
    static const int pDlsamplerate = 48000;
    char mSmartParamFilePath[paramlength];
    char mPhoneProductName[paramlength];

    /* mdoe to set parameters*/
    int mSmartpaMode;

    arsi_task_config_t *mArsiTaskConfig;
    arsi_lib_config_t *mArsiLibConfig;

    data_buf_t      *mParamBuf;

    void *mAurisysLib;

    bool mEnableLibLogHAL;
    char mSvalue[SMARTPA_PARAM_LENGTH];

};

}
#endif
