#ifndef AUDIO_MESSENGER_IPI_H
#define AUDIO_MESSENGER_IPI_H

#include <stdint.h>
#include <stdbool.h>


#ifdef __cplusplus
extern "C" {
#endif


/*
 * =============================================================================
 *                     MACRO
 * =============================================================================
 */

#define MAX_IPI_MSG_BUF_SIZE     (272) /* SHARE_BUF_SIZE - 16 */
#define IPI_MSG_HEADER_SIZE      (16)
#define MAX_IPI_MSG_PAYLOAD_SIZE (MAX_IPI_MSG_BUF_SIZE - IPI_MSG_HEADER_SIZE)

#define IPI_MSG_MAGIC_NUMBER     (0x8888)


/*
 * =============================================================================
 *                     typedef
 * =============================================================================
 */

enum { /* audio_ipi_msg_source_layer_t */
    AUDIO_IPI_LAYER_FROM_HAL,
    AUDIO_IPI_LAYER_FROM_KERNEL,
    AUDIO_IPI_LAYER_FROM_DSP,

    AUDIO_IPI_LAYER_FROM_SIZE,
    AUDIO_IPI_LAYER_FROM_MAX = 15 /* 4-bit only */
};

enum { /* audio_ipi_msg_target_layer_t */
    AUDIO_IPI_LAYER_TO_HAL,
    AUDIO_IPI_LAYER_TO_KERNEL,
    AUDIO_IPI_LAYER_TO_DSP,

    AUDIO_IPI_LAYER_TO_SIZE,
    AUDIO_IPI_LAYER_TO_MAX = 15 /* 4-bit only */
};

enum { /* audio_ipi_msg_data_t */
    AUDIO_IPI_MSG_ONLY,
    AUDIO_IPI_PAYLOAD,
    AUDIO_IPI_DMA,

    AUDIO_IPI_TYPE_SIZE
};

enum { /* audio_ipi_msg_ack_t */
    /* bypass ack, but still send to audio queue */
    AUDIO_IPI_MSG_BYPASS_ACK    = 0,

    /* need ack, and block in audio queue until ack back */
    AUDIO_IPI_MSG_NEED_ACK      = 1,
    AUDIO_IPI_MSG_ACK_BACK      = 2,

    /* bypass audio queue, but still send to ipc queue */
    AUDIO_IPI_MSG_DIRECT_SEND   = 3,

    AUDIO_IPI_MSG_CANCELED      = 8
};

enum { /* adsp_feature_id */
    SYSTEM_FEATURE_ID             = 0,
    AURISYS_FEATURE_ID            = 10,
    AUDIO_CONTROLLER_FEATURE_ID,
    AUDIO_DUMP_FEATURE_ID         = 20,
    PRIMARY_FEATURE_ID,
    DEEPBUF_FEATURE_ID,
    OFFLOAD_FEATURE_ID,
    AUDIO_PLAYBACK_FEATURE_ID,
    EFFECT_HIGH_FEATURE_ID,
    EFFECT_MEDIUM_FEATURE_ID,
    EFFECT_LOW_FEATURE_ID,
    A2DP_PLAYBACK_FEATURE_ID,
    SPK_PROTECT_FEATURE_ID,
    VOICE_CALL_FEATURE_ID,
    VOIP_FEATURE_ID,
    CAPTURE_UL1_FEATURE_ID,
    ADSP_NUM_FEATURE_ID,
};

/*
 * =============================================================================
 *                     struct definition
 * =============================================================================
 */

struct aud_data_t {
    uint32_t memory_size;           /* buffer size (memory) */
    uint32_t data_size;             /* 0 <= data_size <= memory_size */
    union {
        void    *addr;          /* memory address */
        unsigned long addr_val; /* the value of address */

        uint32_t dummy[2];      /* work between 32/64 bit environment */
    };
};

struct ipi_msg_dma_info_t {
    struct aud_data_t reserve;      /* TODO: remove later */

    struct aud_data_t hal_buf;      /* source data buffer */

    uint32_t rw_idx;                /* region r/w index */
    uint32_t data_size;             /* region data size */

    struct aud_data_t wb_dram;      /* allow target to write data back */
};
#define IPI_MSG_DMA_INFO_SIZE (sizeof(struct ipi_msg_dma_info_t))


struct ipi_msg_t {
    /* header: 16 bytes */
    uint16_t magic;             /* IPI_MSG_MAGIC_NUMBER */
    uint8_t  task_scene;        /* see task_scene_t */
    uint8_t  source_layer: 4;   /* see audio_ipi_msg_source_layer_t */
    uint8_t  target_layer: 4;   /* see audio_ipi_msg_target_layer_t */

    uint8_t  data_type;         /* see audio_ipi_msg_data_t */
    uint8_t  ack_type;          /* see audio_ipi_msg_ack_t */
    uint16_t msg_id;            /* defined by user */

    union {
        uint32_t param1;
        uint32_t payload_size;  /* payload */
        uint32_t scp_ret;
    };

    uint32_t param2;

    /* data: 256 bytes */
    union {
        char payload[MAX_IPI_MSG_PAYLOAD_SIZE]; /* payload only */
        struct ipi_msg_dma_info_t dma_info;     /* dma only */
        char *dma_addr;  /* TODO: remove later */
    };

};


typedef void (*audio_ipi_dma_cbk_t)(
    struct ipi_msg_t *msg,
    void *buf,
    uint32_t size,
    void *arg);



/*
 * =============================================================================
 *                     public functions
 * =============================================================================
 */

void audio_messenger_ipi_init(void);
void audio_messenger_ipi_deinit(void);



void print_msg_info(
    const char *func_name,
    const char *description,
    const struct ipi_msg_t *p_ipi_msg);


int audio_send_ipi_msg(
    struct ipi_msg_t *p_ipi_msg,
    uint8_t task_scene, /* task_scene_t */
    uint8_t target_layer, /* audio_ipi_msg_target_layer_t */
    uint8_t data_type, /* audio_ipi_msg_data_t */
    uint8_t ack_type, /* audio_ipi_msg_ack_t */
    uint16_t msg_id,
    uint32_t param1, /* data_size for payload & dma */
    uint32_t param2,
    void    *data_buffer); /* buffer for payload & dma */



void audio_ipi_dma_cbk_register(
    const uint8_t task_scene,
    const uint32_t a2dSize,
    const uint32_t d2aSize,
    audio_ipi_dma_cbk_t cbk,
    void *arg);

void audio_ipi_dma_cbk_deregister(const uint8_t task_scene);



void audio_load_task_scene(const uint8_t task_scene);

void adsp_register_feature(const uint16_t feature_id);
void adsp_deregister_feature(const uint16_t feature_id);



#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* end of AUDIO_MESSENGER_IPI_H */

