/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _V2_0_STREAMID_H_
#define _V2_0_STREAMID_H_

#include <mtkcam3/pipeline/hwnode/StreamId.h>

namespace NSCam {
namespace security {
namespace V2_0 {

/******************************************************************************
 * HALv3 streamId table
 *      value          external/internal of pipeline        description
 *        -1                    external                     NO_STREAM
 *     0 ~ 2^31-1               external                    fwk/client id
 *       2^31                   external                 application's control
 *  2^31+1 ~ 2^32-1             external                preserved for future use
 *   2^32 ~ 2^63-1              internal                    hal's streams
 ******************************************************************************/
 /******************************************************************************
  * Legacy Pipeline Stream ID
 *******************************************************************************/
enum : int64_t
{
    // FIXME: should merge into mtkcam3/pipeline/hwnode/StreamId.h to
    //        avoid stream ID confliction
    //========================== hal's streams =================================
    eSTREAMID_BEGIN_OF_PIPE_PLACEHOLDER = eSTREAMID_BEGIN_OF_PIPE,

    eSTREAMID_META_HAL_BEGIN = (eSTREAMID_END_OF_PIPE - 0x1000UL),

    eSTREAMID_IMAGE_PIPE_YUV_FR,
    eSTREAMID_META_HAL_FULL, // Full HAL Result

    eSTREAMID_META_APP_BEGIN = (eSTREAMID_END_OF_PIPE - 0x2000UL),

    eSTREAMID_META_APP_FULL, // Full APP Result

    eSTREAMID_END_OF_PIPE_PLACEHOLDER = eSTREAMID_END_OF_PIPE
    //========================== hal's streams =================================
};

} // namespace V2_0
} // namespace security
} // namespace NSCam

#endif // _V2_0_STREAMID_H_
