ifeq ($(strip $(MTK_CAM_SECURITY_SUPPORT)), yes)

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

-include $(MTK_PATH_SOURCE)/hardware/mtkcam/mtkcam.mk

LOCAL_MODULE := libmtkcam_security
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

# A workaround for Android.mk dependency that isn't created automatically.
# (e.g. Android.mk is updated but not been rebuilt.)
LOCAL_ADDITIONAL_DEPENDENCIES := $(LOCAL_PATH)/Android.mk

MTK_PATH_CAM := $(MTK_PATH_SOURCE)/hardware/mtkcam
MTK_PATH_CAM3 := $(MTK_PATH_SOURCE)/hardware/mtkcam3

LOCAL_SRC_FILES := \
	SecureCamera.cpp \
	processor/ResultProcessor.cpp \
	processor/TimestampProcessor.cpp \
	buffer/BufferPool.cpp \
	buffer/CallbackBufferPool.cpp \
	buffer/StreamBufferProvider.cpp \
	buffer/BufferCallbackHandler.cpp \
	../utils/IonHelper.cpp \
	../utils/BufferQueue.cpp \
	../utils/StreamInfoHelper.cpp \
	RgbPath.cpp

LOCAL_HEADER_LIBRARIES := libhardware_headers

LOCAL_C_INCLUDES := \
	system/media/camera/include \
	$(MTK_PATH_CAM)/include \
	$(MTK_PATH_CAM3)/include

LOCAL_SHARED_LIBRARIES := \
	liblog \
	libutils \
	libcutils \
	libmtkcam_hwutils \
	libmtkcam_stdutils \
	libmtkcam_fwkutils \
	libmtkcam_streamutils \
	libmtkcam_metastore \
	libmtkcam_metadata \
	libmtkcam_pipeline \
	libmtkcam_hwnode \
	libmtkcam_imgbuf \
	libmtkcam_modulehelper \
	libcam.halsensor \
	libimageio

LOCAL_CFLAGS += $(MTKCAM_CFLAGS) -DLOG_TAG=\"MtkCam/Security\" -DUSE_SYSTRACE

# MTK extension of ION memory allocator
ifeq ($(MTK_ION_SUPPORT), yes)
LOCAL_CFLAGS += -DUSING_MTK_ION
LOCAL_C_INCLUDES += \
	system/core/libion/include \
	$(MTK_PATH_SOURCE)/external/libion_mtk/include
LOCAL_SHARED_LIBRARIES += \
	libion \
	libion_mtk
endif

# MTK TEE (GenieZone)
ifeq ($(strip $(MTK_ENABLE_GENIEZONE)), yes)
$(info MTK TEE (GenieZone) is enabled)
LOCAL_CFLAGS += -DUSING_MTK_TEE
LOCAL_C_INCLUDES += \
	$(MTK_PATH_SOURCE)/geniezone/external/uree/include
LOCAL_STATIC_LIBRARIES += libgz_uree
endif

include $(MTK_SHARED_LIBRARY)

include $(call first-makefiles-under,$(LOCAL_PATH))

endif # MTK_CAM_SECURITY_SUPPORT
