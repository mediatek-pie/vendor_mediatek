/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _V2_0_BUFFER_IBUFFERPOOL_H_
#define _V2_0_BUFFER_IBUFFERPOOL_H_

#include <string>

#include <utils/RefBase.h>
#include <mtkcam/utils/imgbuf/IImageBuffer.h>

#include <mtkcam3/pipeline/utils/streambuf/StreamBuffers.h>

namespace NSCam {
namespace security {
namespace V2_0 {

// ------------------------------------------------------------------------

using NSCam::v3::StreamId_T;

// ------------------------------------------------------------------------

class IBufferPool : public virtual android::RefBase
{
public:
    /**
     * Try to acquire a buffer from the pool.
     *
     * @param[in] szCallerName: a null-terminated string for a caller name.
     *
     * @param[out] rpBuffer: a reference to a newly acquired buffer.
     *
     * @return 0 indicates success; non-zero indicates an error code.
     */
    virtual MERROR acquireFromPool(
            const std::string& callerName, int32_t requestNo,
            android::sp<IImageBufferHeap>& bufferHeap,
            uint32_t& transform) = 0;

    /**
     * Release a buffer to the pool.
     *
     * @param[in] szCallerName: a null-terminated string for a caller name.
     *
     * @param[in] pBuffer: a buffer to release.
     *
     * @return
     *      0 indicates success; non-zero indicates an error code.
     */
    virtual MERROR releaseToPool(
            const std::string& callerName, int32_t requestNo,
            const android::sp<IImageBufferHeap>& bufferHeap,
            uint64_t timestamp, bool invalidBuffer) = 0;

    struct Listener : public virtual android::RefBase
    {
        virtual ~Listener() = default;

        virtual void onBufferAcquired(
                const uint32_t requestNo, const StreamId_T streamId) = 0;

        virtual bool onBufferReleased(
                const uint32_t requestNo, const StreamId_T streamId,
                const bool errorBuffer,
                const android::sp<NSCam::IImageBufferHeap>& bufferHeap) = 0;
    }; // class Listener
    virtual void setListener(const android::wp<Listener>& listener) = 0;

    /**
     * Runtime update buffer count. Not all buffer pool support this function.
     *
     * @param[in] szCallerName: a null-terminated string for a caller name.
     *
     * @param[in] maxBufferCount: max buffer number for pool.
     *
     * @return
     *      0 indicates success; non-zero indicates an error code.
     */
    virtual MERROR updateBufferCount(
            const std::string& /*callerName*/, size_t /*maxBufferCount*/)
    { return android::INVALID_OPERATION; }

    /**
     * Pool name.
     */
    virtual std::string poolName() const = 0;

    /**
     * Dump information for debug.
     */
    virtual void dumpPool() const = 0;
};

} // namespace V2_0
} // namespace security
} // namespace NSCam

#endif // _V2_0_BUFFER_IBUFFERPOOL_H_
