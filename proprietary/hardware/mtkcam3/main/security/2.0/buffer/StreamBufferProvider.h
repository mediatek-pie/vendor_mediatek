/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _V2_0_BUFFER_STREAMBUFFERPROVIDER_H_
#define _V2_0_BUFFER_STREAMBUFFERPROVIDER_H_

#include <mtkcam3/pipeline/stream/IStreamInfo.h>
#include <mtkcam3/pipeline/utils/streambuf/StreamBufferProvider.h>

#include "processor/TimestampProcessor.h"
#include "IBufferPool.h"

#include <mutex>
#include <vector>
#include <unordered_map>

namespace NSCam {
namespace security {
namespace V2_0 {

// ---------------------------------------------------------------------------

using NSCam::v3::IImageStreamInfo;
using NSCam::v3::Utils::HalImageStreamBuffer;
using NSCam::v3::Utils::IStreamBufferProvider;

// ---------------------------------------------------------------------------

class StreamBufferProvider final
    : public IStreamBufferProvider
    , public ITimestampCallback
{
public:
    StreamBufferProvider(bool needTimestamp = false);
    ~StreamBufferProvider() = default;

    // interface of NSCam::v3::Utils::IStreamBufferProvider
    MERROR dequeStreamBuffer(
            const uint32_t requestNo,
            const android::sp<IImageStreamInfo> streamInfo,
            android::sp<HalImageStreamBuffer>& streamBuffer) override;

    MERROR enqueStreamBuffer(
            const android::sp<IImageStreamInfo> streamInfo,
            android::sp<HalImageStreamBuffer> streamBuffer,
            uint32_t bufferStatus) override;

    // interface of NSCam::security::V2_0::ITimestampCallback
    void doTimestampCallback(
            const uint32_t requestNo,
            const bool errorResult,
            const int64_t timestamp) override;

    void onFrameEnd(uint32_t requestNo) override;

    /**
     * Runtime update buffer count. Not all buffer pool support this function.
     *
     * @param[in] szCallerName: a null-terminated string for a caller name.
     *
     * @param[in] rMaxBufferNumber: max buffer number for pool.
     *
     * @return
     *      0 indicates success; non-zero indicates an error code.
     */
    MERROR updateBufferCount(
            const std::string& callerName, size_t maxBufferCount);

    /**
     * Set image stream info.
     *
     * @param[in] streamInfo : IImageStreamInfo.
     *
     * @return
     *      0 indicates success; otherwise failure.
     */
    android::status_t setImageStreamInfo(
            const android::sp<IImageStreamInfo>& streamInfo);

    /**
     * Query image stream info.
     *
     * @return
     *      android::sp<IImageStreamInfo>
     */
    android::sp<IImageStreamInfo> queryImageStreamInfo();

    /**
     * Set buffer pool to buffer provider.
     *
     * @param[in] pBufProvider : buffer pool.
     *
     * @return
     *      0 indicates success; otherwise failure.
     */
    android::status_t setBufferPool(
            const android::sp<IBufferPool>& bufferPool);

    /**
     * Return all buffer to pool.
     *
     * @return
     *      0 indicates success; otherwise failure.
     */
    void flush();

    // interface of android::RefBase
    void onLastStrongRef(const void* /*id*/);

private:
    mutable std::mutex mBufferPoolLock;
    android::sp<IBufferPool> mBufferPool;

    mutable std::mutex mImageStreamInfoLock;
    android::sp<IImageStreamInfo> mImageStreamInfo;
    std::string mStreamName;

    mutable std::mutex mBufferMapLock;
    struct Buffer {
        uint32_t requestNo;
        android::sp<IImageBufferHeap> heap;
    };
    std::unordered_map<HalImageStreamBuffer*, Buffer> mBufferMap;

    mutable std::mutex mResultSetLock;
    struct ResultSet
    {
        uint32_t requestNo;
        android::sp<IImageBufferHeap> heap;
        bool error;
        int64_t timestamp;
    };
    std::unordered_map<uint32_t, ResultSet> mResultSetMap;

    mutable std::mutex mFrameEndLock;
    std::vector<uint32_t> mFrameEnd;

    const bool kNeedTimestamp;

    void handleResult(const std::vector<ResultSet>& resultSet);

    android::status_t handleReturn();
}; // class StreamBufferProvider

// ---------------------------------------------------------------------------

struct StreamBufferProviderBuilder
{
    /**
     *
     * @param[in] streamInfo : ImageStreamInfo
     * @param[in] pool : user-provided buffer pool; if not provided,
     *                   a built-in one will be allocated
     * @param[out] android::sp<StreamBufferProvider>: StreamBufferProvider instance
     */
    static android::sp<StreamBufferProvider> create(
            const android::sp<IImageStreamInfo>& streamInfo,
            const android::sp<IBufferPool>& pool,
            bool needTimeStamp = true);
}; // struct StreamBufferProviderBuilder

} // namespace V2_0
} // namespace security
} // namespace NSCam

#endif // _V2_0_BUFFER_STREAMBUFFERPROVIDER_H_
