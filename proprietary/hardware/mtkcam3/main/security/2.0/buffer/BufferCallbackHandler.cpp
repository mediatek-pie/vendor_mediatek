/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define DEBUG_LOG_TAG "BufferCallbackHandler"

#include "BufferCallbackHandler.h"

#include "../StreamId.h"

#include <mtkcam3/main/security/utils/Debug.h>

using namespace android;
using namespace NSCam;
using namespace NSCam::security::V2_0;
using namespace NSCam::security::utils;

// ---------------------------------------------------------------------------

BufferCallbackHandler::BufferCallbackHandler(unsigned int sensorID)
    : kSensorId(sensorID)
{
};

void BufferCallbackHandler::onBufferAcquired(
        const uint32_t requestNo, const StreamId_T streamId)
{
    SEC_LOGD("[%d] requestNo:%u streamId:%d", kSensorId, requestNo, streamId);

    std::lock_guard<std::mutex> _l(mRequestOrderMapLock);

    ssize_t index = mRequestOrderMap.indexOfKey(requestNo);
    if (index < 0)
    {
        Vector<Buffer_T> v;
        v.push_back(Buffer_T{requestNo, streamId, false, false, nullptr});
        mRequestOrderMap.add(requestNo, v);
    }
    else
    {
        mRequestOrderMap.editValueFor(requestNo).push_back(Buffer_T{requestNo, streamId, false, false, nullptr});
    }
}

bool BufferCallbackHandler::onBufferReleased(
        const uint32_t requestNo, const StreamId_T streamId,
        const bool errorBuffer,
        const android::sp<NSCam::IImageBufferHeap>& bufferHeap)
{
    SEC_LOGD("[%d] requestNo:%u streamId:%d errorBuffer:%d bufferHeap:%" PRIxPTR,
            kSensorId, requestNo, streamId, errorBuffer, bufferHeap.get());

    std::lock_guard<std::mutex> _l(mRequestOrderMapLock);
    size_t i = 0;

    /* check RequestNO */
    if (mRequestOrderMap.indexOfKey(requestNo) < 0)
    {
        SEC_LOGE("[%d] no find requestNo. Should not happen.", kSensorId);
        return false;
    }

    /* Process the return buffer */
    size_t size     = mRequestOrderMap.editValueFor(requestNo).size();
    bool bKeepBuf = (streamId == eSTREAMID_IMAGE_PIPE_YUV_JPEG_00 && size > 1); // must KEEP jpeg buffer until all other buffers are released
    sp<IImageCallback> pCb = mCallback.promote();
    if (pCb == nullptr)
    {
        SEC_LOGE("[%d] streamID(%d) callback is NULL! can't callback buffer!!", kSensorId, streamId);
        return false;
    }
    //
    for (i = 0 ; i < size ; i++)
    {
        Buffer_T* buf = &mRequestOrderMap.editValueFor(requestNo).editItemAt(i);
        if(buf == 0)
        {
            SEC_LOGE("[%d] buf is NULL. Should not happen.", kSensorId);
            return false;
        }
        if (buf->streamId == streamId && !buf->isReturned)
        {
            buf->buffer = bufferHeap->createImageBuffer();
            buf->isError = errorBuffer;

            /* Keep buffer if it is Jpeg and some non-Jpeg buffers aren't received */
            if (bKeepBuf)
            {
                buf->isReturned = false;
            }
            else
            {
                /* callback current buffer */
                if(pCb != NULL)
                {
                    pCb->onResultReceived(buf->requestNo, buf->streamId, buf->isError, buf->buffer);
                    SEC_LOGD("[%d] onResultReceived(%d,%#" PRIxPTR ",%d,%p)", kSensorId,
                        buf->requestNo, buf->streamId, buf->isError, buf->buffer.get());
                }
                else
                {
                    SEC_LOGW("callback.promote() == NULL, [%d] onResultReceived(%d,%#" PRIxPTR ",%d,%p)", kSensorId,
                        buf->requestNo, buf->streamId, buf->isError, buf->buffer.get());
                }
                buf->isReturned = true;
            }
            break;
        }
    }

    if (i == size)
    {
        SEC_LOGE("[%d] Can't find streamID(%d). Should not happen.", kSensorId, streamId);
        return false;
    }

    /* check Jpeg buffer, callback if the other buffer is for Jpeg */
    if (!bKeepBuf && size == 2)
    {
        for (i = 0 ; i < size ; i++)
        {
            Buffer_T* buf = &mRequestOrderMap.editValueFor(requestNo).editItemAt(i);
            if (!buf->isReturned && buf->buffer != NULL && buf->streamId == eSTREAMID_IMAGE_PIPE_YUV_JPEG_00)
            {
                //callback Jpeg at last
                pCb->onResultReceived(buf->requestNo, buf->streamId, buf->isError, buf->buffer);
                SEC_LOGD("[%d] onResultReceived(%d,%#" PRIxPTR ",%d,%p)", kSensorId,
                    buf->requestNo, buf->streamId, buf->isError, buf->buffer.get());
                buf->isReturned = true;
            }
        }
    }

    /* remove returned item for this RequestNo, remove this RequestNO from mRequestOrderMap if no item insides */
    Vector<Buffer_T>::iterator it;
    for (it = mRequestOrderMap.editValueFor(requestNo).begin() ; it != mRequestOrderMap.editValueFor(requestNo).end() ;)
    {
      if (it->isReturned)
        it = mRequestOrderMap.editValueFor(requestNo).erase(it);
      else
        it++;
    }
    if (mRequestOrderMap.editValueFor(requestNo).size() == 0) mRequestOrderMap.removeItem(requestNo);

    return true;
}

void BufferCallbackHandler::addBufferPool(
        const android::sp<CallbackBufferPool>& pool)
{
    if (pool == nullptr)
    {
        SEC_LOGE("[%d] invalid pool", kSensorId);
        return;
    }

    pool->setListener(this);
    android::sp<IImageStreamInfo> streamInfo = pool->getStreamInfo();
    if (CC_UNLIKELY(streamInfo == nullptr))
    {
        SEC_LOGE("[%d] Pool has no StreamInfo!", kSensorId);
        return;
    }

    mvBufferPool.add(
            streamInfo->getStreamId(), Info_T{streamInfo->getStreamId(), pool});
}

MVOID
BufferCallbackHandler::
onLastStrongRef(const void* /*id*/)
{
    AutoLog();

    CAM_TRACE_NAME("BufferCallbackHandler:onLastStrongRef (flush pool)");
    for (size_t i = 0; i < mvBufferPool.size(); i++)
    {
#if MTKCAM_HAVE_COMMON_CAPTURE_PIPELINE_SUPPORT
        // for reusable,
        // buffer pool will free by owner or no owner to keep the pool sp
        SEC_LOGW("for reusable, buffer pool for streamId(0x%#" PRIxPTR ") should free by owner",
                mvBufferPool.editValueAt(i).pool->getStreamInfo()->getStreamId());
#else
        mvBufferPool.editValueAt(i).pool->flush();
#endif
    }
}
