/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define DEBUG_LOG_TAG "DBQ"

#include <ion/ion.h>
#include <ion.h>
#include <linux/mtk_ion.h>
#include <linux/ion_drv.h>

#include <unistd.h>

#include <sys/mman.h>
#include <linux/mman-proprietary.h>

#include <mtkcam/utils/std/Sync.h>
#include <mtkcam3/main/security/utils/BufferQueue.h>
#include <mtkcam3/main/security/utils/Debug.h>

#include <chrono>

using namespace NSCam::Utils::Sync;
using namespace NSCam::security::utils;

// ---------------------------------------------------------------------------

// timeout if below 10 FPS
static constexpr std::chrono::duration<int64_t, std::milli>
    kDequeueBufferTimeout = std::chrono::milliseconds(100);

static const int kIONNonCachable = 0;
static const int kIONCachable = (ION_FLAG_CACHED | ION_FLAG_CACHED_NEEDS_SYNC);

// ---------------------------------------------------------------------------

static SECHAND getSecureHandle(const int ionDevFD, const ion_user_handle_t& handle)
{
    struct ion_sys_data sys_data;
    sys_data.sys_cmd = ION_SYS_GET_PHYS;
    sys_data.get_phys_param.handle = handle;
    if (ion_custom_ioctl(ionDevFD, ION_CMD_SYSTEM, &sys_data))
    {
        SEC_LOGE("ion_custom_ioctl failed to get secure handle");
        return 0;
    }

    SEC_LOGI("Secure memory allocated: handle(0x%x) size(%lu)",
            sys_data.get_phys_param.phy_addr, sys_data.get_phys_param.len);

    return sys_data.get_phys_param.phy_addr;
}

// TODO: encapsulates ION operations into class level
static status_t createIonBuffer(
        const int ionDevFD, std::mutex& lock,
        const bool isSecure,
        const size_t data_size, const size_t align,
        ion_user_handle_t& handle, SECHAND& sec_handle,
        BufferQueue::CacheType cache_mechanism,
        int& ion_fd, void*& va)
{
    AutoLog();

    std::lock_guard<std::mutex> _l(lock);

    status_t err = NO_ERROR;

    // allocate buffer from multimedia heap
    // TODO: need cache sync
    const auto isCachable =
        (cache_mechanism == BufferQueue::CacheType::CACHEABLE);
    if (ion_alloc(ionDevFD, data_size, align,
                isSecure ? ION_HEAP_MULTIMEDIA_SEC_MASK : ION_HEAP_MULTIMEDIA_MASK,
                isCachable ? kIONCachable : kIONNonCachable,
                &handle))
    {
        SEC_LOGE("ion_alloc failed: ionDevFD(%d), BufSize(%zu) secure(%d)",
                ionDevFD, data_size, isSecure);
        return NO_INIT;
    }
    SEC_LOGD("ion_alloc done: %s %s buffer",
            isCachable ? "cachable" : "non-cachable",
            isSecure ? "secure" : "normal");

    // obtain a file descriptor that can pass to other clients
    if (ion_share(ionDevFD, handle, &ion_fd))
    {
        SEC_LOGE("ion_share failed: ionDevFD(%d), BufSize(%zu)," \
                " ionHandle(%d)",
                ionDevFD, data_size, handle);
        return INVALID_OPERATION;
    }

    if (isSecure == false)
    {
        // map virtual address of ION buffer
        va = ion_mmap(
                ionDevFD, nullptr, data_size,
                (PROT_READ | PROT_WRITE | PROT_NOCACHE),
                MAP_SHARED, ion_fd, 0);

        if (va != 0)
            SEC_LOGD("ion_mmap: devFD(%d) FD(%d) va(0x%" PRIxPTR ") size(%zu)",
                    ionDevFD, ion_fd, reinterpret_cast<uintptr_t>(va), data_size);
        else
        {
            SEC_LOGE("ion_mmap failed: invalid virutal address");
            return INVALID_OPERATION;
        }
    }
    else
    {
        // query secure handle via ION handle
        sec_handle = getSecureHandle(ionDevFD, handle);
        if (CC_UNLIKELY(sec_handle == 0))
        {
            SEC_LOGE("get secure handle failed");
            return BAD_VALUE;
        }
    }

    return err;
}

static status_t destroyIonBuffer(
        const int ionDevFD, std::mutex& lock,
        const bool secure,
        const size_t data_size,
        ion_user_handle_t& handle, int& ion_fd, void*& va)
{
    AutoLog();

    std::lock_guard<std::mutex> _l(lock);

    status_t err = NO_ERROR;

    // clear content from allocated ION buffer and
    // unmap virtual address of ION buffer
    if (secure == false)
    {
        memset(va, 0, data_size);
        ion_munmap(ionDevFD, va, data_size);
    }

    // close share fd of ION buffer
    if (ion_fd >= 0)
    {
        if (ion_share_close(ionDevFD, ion_fd))
            SEC_LOGE("ion_share_close failed");
        ion_fd = -1;
    }

    // free ION buffer handle
    if (ion_free(ionDevFD, handle))
        SEC_LOGE("ion_free failed");
    handle = 0;

    return err;
}

static inline status_t handleSecureBuffer(
        bool __unused is_secure,
        ion_user_handle_t __unused hand, SECHAND* __unused sec_handle)
{
    AutoLog();
/*
    *sec_handle = 0;
    if (is_secure)
    {
        SECHAND tmp_sec_handle;
        int err = getSecureHwcBuf(hand, &tmp_sec_handle);
        if (GRALLOC_EXTRA_OK != err)
        {
            SVPLOGE("handleSecureBuffer", "Failed to allocate secure memory");
            return -EINVAL;
        }
        else
        {
            *sec_handle = tmp_sec_handle;
        }
    }
    else
    {
        freeSecureHwcBuf(hand);
    }
    */
    return NO_ERROR;
}

static void protectedCloseImpl(
        const int32_t& __unused fd, const char* __unused str,
        const int32_t& __unused line)
{
    AutoLog();
#ifdef FENCE_DEBUG
    android::CallStack stack;
    stack.update();
    SEC_LOGW("close fd %d backtrace: %s", fd, stack.toString().string());

    SEC_LOGW("close fd %d(%s) line(%d)", fd, str, line);
#endif
    ::close(fd);
}
#define protectedClose(fd) protectedCloseImpl(fd, __func__, __LINE__)

// ---------------------------------------------------------------------------

BufferQueue::BufferQueue(int maxBuffers)
    : m_is_synchronous(true)
    , m_dequeue_block(true)
    , m_frame_counter(0)
    , m_last_acquire_idx(INVALID_BUFFER_SLOT)
    , m_page_size(sysconf(_SC_PAGESIZE))
{
    AutoLog();

    m_buffer_count = maxBuffers;

    // assure no "array-index out of bounds" exception
    SEC_LOGA_IF(m_buffer_count > NUM_BUFFER_SLOTS,
        "maximum buffer count(%d) exceeds NUM_BUFFER_SLOTS(%d)",
        m_buffer_count, NUM_BUFFER_SLOTS);

    // open ION device
    mIonDevFd.first = mt_ion_open(DEBUG_LOG_TAG);
    if (mIonDevFd.first < 0)
    {
        SEC_LOGE("open Ion device failed!");
        mIonDevFd.first = -1;
    }
}

BufferQueue::~BufferQueue()
{
    AutoLog();

    for (int i = 0; i < m_buffer_count; i++)
    {
        BufferSlot* slot = &m_slots[i];

        if (0 == slot->handle) continue;

        if (slot->release_fence != -1) protectedClose(slot->release_fence);
        if (slot->acquire_fence != -1) protectedClose(slot->acquire_fence);

        if (NO_ERROR != destroyIonBuffer(
                    mIonDevFd.first, mIonDevFd.second, slot->secure,
                    slot->data_size, slot->handle, slot->ion_fd,
                    reinterpret_cast<void*&>(slot->va)))
        {
            SEC_LOGE("Failed to free memory size(%zu)", slot->data_size);
        }
    }

    m_producer_listener = nullptr;
    m_consumer_listener = nullptr;

    // close ION device
    {
        std::lock_guard<std::mutex> _l(mIonDevFd.second);
        if (mIonDevFd.first != -1)
        {
            ion_close(mIonDevFd.first);
            mIonDevFd.first = -1;
        }
    }
}

status_t BufferQueue::cacheSync(
        const struct Buffer& buffer, const SyncType type)
{
    AutoLog();

    // skip cache sync if non-cacheable
    {
        std::lock_guard<std::mutex> l(m_mutex);
        if (CC_UNLIKELY(m_buffer_param.cache_mechanism == CacheType::NON_CACHEABLE))
        {
            SEC_LOGW("Non-cacheable and no need to do cache sync");
            return NO_ERROR;
        }
    }

    std::lock_guard<std::mutex> _l(mIonDevFd.second);
    if (CC_UNLIKELY(mIonDevFd.first == -1))
    {
        SEC_LOGE("Invalid Ion device");
        return NO_INIT;
    }

    if (CC_UNLIKELY(buffer.handle == 0))
    {
        SEC_LOGE("Invalid Ion user handle");
        return BAD_VALUE;
    }

    // cache sync by flush/invalidate all
    struct ion_sys_data sys_data;
    sys_data.sys_cmd                    = ION_SYS_CACHE_SYNC;
    sys_data.cache_sync_param.handle    = buffer.handle;
    sys_data.cache_sync_param.sync_type =
        [&type]()
        {
            if (type == SyncType::FLUSH)
                return ION_CACHE_FLUSH_ALL;
            else
                return ION_CACHE_INVALID_ALL;
        }();
    if (ion_custom_ioctl(mIonDevFd.first, ION_CMD_SYSTEM, &sys_data))
    {
        SEC_LOGE("Failed to flush cache through ION: %s", strerror(errno));
        return -errno;
    }

    return NO_ERROR;
}

status_t BufferQueue::setBufferParam(const BufferParam&& param)
{
    AutoLog();

    std::lock_guard<std::mutex> l(m_mutex);

    m_buffer_param = std::move(param);

    return NO_ERROR;
}

status_t BufferQueue::reallocateLocked(int idx, bool is_secure)
{
    BufferSlot* slot = &m_slots[idx];

    slot->data_pitch = m_buffer_param.pitch;
    slot->data_format = m_buffer_param.format;

    // allocate new buffer
    if (slot->handle == 0)
    {
        slot->data_size = m_buffer_param.size;
        slot->secure = is_secure;
        if (NO_ERROR != createIonBuffer(
                    mIonDevFd.first, mIonDevFd.second, slot->secure,
                    slot->data_size, m_page_size, slot->handle, slot->sec_handle,
                    m_buffer_param.cache_mechanism,
                    slot->ion_fd, reinterpret_cast<void*&>(slot->va)))
        {
            SEC_LOGE("Failed to allocate memory size(%zu)", slot->data_size);
            slot->handle = 0;
            slot->data_size = 0;
            return -EINVAL;
        }

        SEC_LOGD("Alloc Slot(%d): ion handle(%d) secure(%d) sec_handle(0x%x) FD(%d)",
                idx, slot->handle, slot->secure, slot->sec_handle, slot->ion_fd);
    }
    return NO_ERROR;
}

status_t BufferQueue::drainQueueLocked(std::unique_lock<std::mutex>& lock)
{
    while (m_is_synchronous && !m_queue.empty())
    {
        m_dequeue_condition.wait(lock);
    }

    return NO_ERROR;
}

status_t BufferQueue::dequeueBuffer(
        Buffer* buffer, bool async, bool is_secure)
{
    AutoLog();
    std::unique_lock<std::mutex> l(m_mutex);

    int found = INVALID_BUFFER_SLOT;
    bool tryAgain = true;
    while (tryAgain)
    {
        found = INVALID_BUFFER_SLOT;
        for (int i = 0; i < m_buffer_count; i++)
        {
            const int state = m_slots[i].state;
            if (state == BufferSlot::FREE)
            {
                // return the oldest of the free buffers to avoid
                // stalling the producer if possible.
                if (found < 0)
                {
                    found = i;
                }
                else if (m_slots[i].frame_num < m_slots[found].frame_num)
                {
                    found = i;
                }
            }

            SEC_LOGD("slot %d state(%d) FD(%d) num(%llu)",
                    i, state, m_slots[i].ion_fd, m_slots[i].frame_num);
        }

        // if no buffer is found, wait for a buffer to be released
        tryAgain = (found == INVALID_BUFFER_SLOT);
        if (tryAgain)
        {
            if (CC_LIKELY(m_buffer_param.dequeue_block))
            {
                SEC_LOGV("dequeueBuffer: wait for available buffer...");
                const std::cv_status res =
                    m_dequeue_condition.wait_for(l, kDequeueBufferTimeout);

                if (CC_LIKELY(res != std::cv_status::timeout))
                    SEC_LOGV("dequeueBuffer: wake up to find available buffer");
                else
                    SEC_LOGW("dequeueBuffer: cannot find available buffer");
            }
            else
            {
                SEC_LOGE("dequeueBuffer: cannot find available buffer, exit...");
                return -EBUSY;
            }
        }
    }

    const int idx = found;

    reallocateLocked(idx, is_secure);

    // TODO: add handleSecureBuffer here

    // buffer is now in DEQUEUED state
    m_slots[idx].state = BufferSlot::DEQUEUED;

    buffer->handle           = m_slots[idx].handle;
    buffer->ion_fd           = m_slots[idx].ion_fd;
    buffer->sec_handle       = m_slots[idx].sec_handle;
    buffer->data_size        = m_slots[idx].data_size;
    buffer->data_pitch       = m_slots[idx].data_pitch;
    buffer->data_format      = m_slots[idx].data_format;
    buffer->frame_num        = m_slots[idx].frame_num;
    buffer->secure           = m_slots[idx].secure;
    buffer->release_fence    = m_slots[idx].release_fence;
    buffer->va               = m_slots[idx].va;
    buffer->index            = idx;

    m_slots[idx].release_fence = -1;

    SEC_LOGD("dequeueBuffer (idx=%d, fence=%d) (handle=%d, ion=%d)",
            idx, buffer->release_fence, buffer->handle, buffer->ion_fd);

    // wait release fence
    if (!async)
    {
        sp<IFence> release_fence(IFence::create(buffer->release_fence));
        release_fence->wait(1000);
        buffer->release_fence = -1;
    }

    return NO_ERROR;
}

status_t BufferQueue::queueBuffer(
        Buffer* buffer, const void* opaqueMessage)
{
    AutoLog();

    std::shared_ptr<ConsumerListener> listener;

    {
        std::lock_guard<std::mutex> l(m_mutex);

        const int idx = buffer->index;

        if (idx < 0 || idx >= m_buffer_count)
        {
            SEC_LOGE("queueBuffer: slot index out of range [0, %d): %d",
                     m_buffer_count, idx);
            return -EINVAL;
        }
        else if (m_slots[idx].state != BufferSlot::DEQUEUED)
        {
            SEC_LOGE("queueBuffer: slot %d is not owned by the client "
                     "(state=%d)", idx, m_slots[idx].state);
            return -EINVAL;
        }

        // if queue not empty, means consumer is slower than producer
        // * in sync mode, may cause lag (but size 1 should be OK for triple buffer)
        // * in async mode, frame drop
        //bool dump_fifo = false;
        bool dump_fifo = false;
        if (true == m_is_synchronous)
        {
            // fifo depth 1 is ok for multiple buffer, but 2 would cause lag
            if (1 < m_queue.size())
            {
                SEC_LOGW("queued:%zu (lag)", m_queue.size());
                dump_fifo = true;
            }
        }
        else
        {
            // frame drop is fifo is not empty
            if (0 < m_queue.size())
            {
                SEC_LOGD("queued:%zu (drop frame)", m_queue.size());
                dump_fifo = true;
            }
        }

        // dump current fifo data, and the new coming one
        if (true == dump_fifo)
        {
            // print from the oldest to the latest queued buffers
            const BufferSlot *slot = nullptr;

            for (auto i : m_queue)
            {
                slot = &(m_slots[i]);
                SEC_LOGD(" [idx:%d] handle:%d", i, slot->handle);
            }

            SEC_LOGD("NEW [idx:%d] handle:%d", idx, m_slots[idx].handle);
        }

        if (m_is_synchronous)
        {
            // In synchronous mode we queue all buffers in a FIFO
            m_queue.push_back(idx);

            listener = m_consumer_listener;
        }
        else
        {
            // In asynchronous mode we only keep the most recent buffer.
            if (m_queue.empty())
            {
                m_queue.push_back(idx);
            }
            else
            {
                auto front(m_queue.begin());
                // buffer currently queued is freed
                m_slots[*front].state = BufferSlot::FREE;
                // and we record the new buffer index in the queued list
                *front = idx;
            }

            listener = m_consumer_listener;
        }

        m_slots[idx].handle               = buffer->handle;
        m_slots[idx].state                = BufferSlot::QUEUED;
        m_slots[idx].frame_num            = (++m_frame_counter);
        m_slots[idx].secure               = buffer->secure;
        m_slots[idx].sequence             = buffer->sequence;
        m_slots[idx].acquire_fence        = buffer->acquire_fence;

        SEC_LOGD("queueBuffer (idx=%d, fence=%d)",
                idx, m_slots[idx].acquire_fence);

        m_dequeue_condition.notify_all();
    }

    if (listener != nullptr) listener->onBufferQueued(opaqueMessage);

    return NO_ERROR;
}

status_t BufferQueue::cancelBuffer(int index, bool isDropped)
{
    AutoLog();

    std::lock_guard<std::mutex> l(m_mutex);

    if (index == INVALID_BUFFER_SLOT) return -EINVAL;

    if (index < 0 || index >= m_buffer_count)
    {
        SEC_LOGE("cancelBuffer: slot index out of range [0, %d]: %d",
                m_buffer_count, index);
        return -EINVAL;
    }
    else if ((m_slots[index].state != BufferSlot::DEQUEUED))
    {
        SEC_LOGE("cancelBuffer: slot %d is not owned by the client (state=%d)",
                index, m_slots[index].state);
        return -EINVAL;
    }

    SEC_LOGD("cancelBuffer (%d) isDropped(%d)", index, isDropped);

    m_slots[index].state = BufferSlot::FREE;
    m_slots[index].frame_num = 0;
    m_slots[index].acquire_fence = -1;
    m_slots[index].release_fence = -1;

    m_dequeue_condition.notify_all();
    return NO_ERROR;
}

status_t BufferQueue::setSynchronousMode(bool enabled)
{
    std::unique_lock<std::mutex> l(m_mutex);

    if (m_is_synchronous != enabled)
    {
        // drain the queue when changing to asynchronous mode
        if (!enabled) drainQueueLocked(l);

        m_is_synchronous = enabled;
        m_dequeue_condition.notify_all();
    }

    return NO_ERROR;
}

status_t BufferQueue::acquireBuffer(Buffer* buffer, bool async)
{
    AutoLog();

    std::lock_guard<std::mutex> l(m_mutex);

    // check if queue is empty
    // In asynchronous mode the list is guaranteed to be one buffer deep.
    // In synchronous mode we use the oldest buffer.
    if (!m_queue.empty())
    {
        auto front(m_queue.begin());
        int idx = *front;

        // buffer is now in ACQUIRED state
        m_slots[idx].state = BufferSlot::ACQUIRED;

        buffer->handle        = m_slots[idx].handle;
        buffer->ion_fd        = m_slots[idx].ion_fd;
        buffer->sec_handle    = m_slots[idx].sec_handle;
        buffer->data_size     = m_slots[idx].data_size;
        buffer->data_pitch    = m_slots[idx].data_pitch;
        buffer->data_format   = m_slots[idx].data_format;
        buffer->frame_num     = m_slots[idx].frame_num;
        buffer->secure        = m_slots[idx].secure;
        buffer->sequence      = m_slots[idx].sequence;
        buffer->acquire_fence = m_slots[idx].acquire_fence;
        buffer->va            = m_slots[idx].va;
        buffer->index         = idx;

        m_slots[idx].acquire_fence = -1;

        SEC_LOGD("acquireBuffer (idx=%d, fence=%d, state=%d)",
                idx, buffer->acquire_fence, m_slots[idx].state);

        // remember last acquire buffer's index
        m_last_acquire_idx = idx;

        m_queue.erase(front);
        m_dequeue_condition.notify_all();
    }
    else
    {
        SEC_LOGW("acquireBuffer failed");

        buffer->index = INVALID_BUFFER_SLOT;
        return NO_BUFFER_AVAILABLE;
    }

    // wait acquire fence
    if (!async)
    {
        sp<IFence> acquire_fence(IFence::create(buffer->acquire_fence));
        acquire_fence->wait(1000);
        buffer->acquire_fence = -1;
    }

    return NO_ERROR;
}

status_t BufferQueue::releaseBuffer(int index, int fence)
{
    AutoLog();

    std::shared_ptr<ProducerListener> listener;

    {
        std::lock_guard<std::mutex> l(m_mutex);

        if (index == INVALID_BUFFER_SLOT)
        {
            SEC_LOGE("releaseBuffer invalid_buffer_slot");
            return -EINVAL;
        }

        if (index < 0 || index >= m_buffer_count)
        {
            SEC_LOGE("releaseBuffer: slot index out of range [0, %d]: %d",
                    m_buffer_count, index);
            return -EINVAL;
        }

        if (m_slots[index].state != BufferSlot::ACQUIRED)
        {
            SEC_LOGE("attempted to releasebuffer(%d) with state(%d)",
                    index, m_slots[index].state);
            return -EINVAL;
        }

        m_slots[index].state = BufferSlot::FREE;
        m_slots[index].release_fence = fence;

        listener = m_producer_listener;

        SEC_LOGD("releaseBuffer (idx=%d, fence=%d)", index, fence);

        m_dequeue_condition.notify_all();
    }

    if (listener != nullptr) listener->onBufferReleased();

    return NO_ERROR;
}

void BufferQueue::setConsumerListener(
    const std::shared_ptr<ConsumerListener> & listener)
{
    AutoLog();
    std::lock_guard<std::mutex> l(m_mutex);
    m_consumer_listener = listener;
}

void BufferQueue::setProducerListener(
    const std::shared_ptr<ProducerListener> & listener)
{
    AutoLog();
    std::lock_guard<std::mutex> l(m_mutex);
    m_producer_listener = listener;
}
