/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define DEBUG_LOG_TAG "StreamInfoHelper"

#include <mtkcam3/main/security/utils/Debug.h>

#include <mtkcam3/main/security/utils/StreamInfoHelper.h>
#include <mtkcam3/pipeline/stream/IStreamBuffer.h>
#include <mtkcam3/pipeline/utils/streaminfo/ImageStreamInfo.h>
#include <mtkcam/utils/hw/HwInfoHelper.h>

#include <cmath>

using namespace android;
using namespace NSCamHW;

using NSCam::v3::Utils::ImageStreamInfo;
using NSCam::v3::IImageStreamInfo;
using namespace NSCam::security::utils;

using NSCam::MSize;

// ---------------------------------------------------------------------------

sp<IImageStreamInfo>
NSCam::security::utils::createImageStreamInfo(
    char const*         streamName,
    StreamId_T          streamId,
    MUINT32             streamType,
    size_t              maxBufNum,
    size_t              minInitBufNum,
    MUINT               usageForAllocator,
    MINT                imgFormat,
    MSize const&        imgSize,
    MUINT32             transform,
    MSize const&        align,
    SecureInfo const&   secureInfo
)
{
    IImageStreamInfo::BufPlanes_t bufPlanes;
    MSize alignSize = imgSize;
    if( imgSize.w > 0 && imgSize.h > 0 )
    {
        alignSize.w = std::ceil((double)imgSize.w/align.w)*align.w;
        alignSize.h = std::ceil((double)imgSize.h/align.h)*align.h;
    }
    SEC_LOGI("in(%d,%d), align(%d,%d), out(%d,%d)", imgSize.w, imgSize.h, align.w, align.h, alignSize.w, alignSize.h);

#define addBufPlane(planes, height, stride)                                                 \
        do{                                                                                 \
            size_t _planeIndex = planes.size();                                             \
            size_t _height = (size_t)(height);                                              \
            size_t _stride = (size_t)(stride);                                              \
            size_t _sizeInBytes = (size_t)( _height * _stride );                            \
                                                                                            \
            size_t ori_height = alignSize.h;                                                \
            size_t ori_width  = alignSize.w;                                                \
            size_t _longLength = (ori_height > ori_width) ? ori_height : ori_width;         \
            size_t _shortLength = (ori_height > ori_width) ? ori_width : ori_height;        \
                                                                                            \
            IImageStreamInfo::BufPlane bufPlane= { _sizeInBytes, _stride };                 \
            planes.push_back(bufPlane);                                                     \
            SEC_LOGD("bufPlanes[%zu](sizeInBytes:%zu, stride:%zu)",                         \
                    _planeIndex, bufPlane.sizeInBytes, bufPlane.rowStrideInBytes);          \
        }while(0)

    switch( imgFormat ) {
        case NSCam::EImageFormat::eImgFmt_YV12:
            addBufPlane(bufPlanes , alignSize.h      , alignSize.w);
            addBufPlane(bufPlanes , alignSize.h >> 1 , alignSize.w >> 1);
            addBufPlane(bufPlanes , alignSize.h >> 1 , alignSize.w >> 1);
            break;
        case NSCam::EImageFormat::eImgFmt_I422:
            addBufPlane(bufPlanes , alignSize.h      , alignSize.w);
            addBufPlane(bufPlanes , alignSize.h      , alignSize.w >> 1);
            addBufPlane(bufPlanes , alignSize.h      , alignSize.w >> 1);
            break;
        case NSCam::EImageFormat::eImgFmt_I420:
            addBufPlane(bufPlanes , alignSize.h      , alignSize.w);
            addBufPlane(bufPlanes , alignSize.h >> 1 , alignSize.w >> 1);
            addBufPlane(bufPlanes , alignSize.h >> 1 , alignSize.w >> 1);
            break;
        case NSCam::EImageFormat::eImgFmt_NV21:
        case NSCam::EImageFormat::eImgFmt_NV12:
            addBufPlane(bufPlanes , alignSize.h      , alignSize.w);
            addBufPlane(bufPlanes , alignSize.h >> 1 , alignSize.w);
            break;
        case NSCam::EImageFormat::eImgFmt_YUY2:
            addBufPlane(bufPlanes , alignSize.h      , alignSize.w << 1);
            break;
        case NSCam::EImageFormat::eImgFmt_BLOB:
            /*
            add 328448 for image size
            standard exif: 1280 bytes
            4 APPn for debug exif: 0xFF80*4 = 65408*4 bytes
            max thumbnail size: 64K bytes
            */
            addBufPlane(bufPlanes , 1 , (imgSize.w * imgSize.h * 12 / 10) + 328448); //328448 = 64K+1280+65408*4
            break;
        case NSCam::EImageFormat::eImgFmt_RGBA8888:
            addBufPlane(bufPlanes , alignSize.h, alignSize.w << 2);
            break;
        case NSCam::EImageFormat::eImgFmt_Y8:
            addBufPlane(bufPlanes , imgSize.h      , imgSize.w);
            break;
        case NSCam::EImageFormat::eImgFmt_STA_BYTE:
            addBufPlane(bufPlanes , imgSize.h      , imgSize.w);
            break;
        default:
            SEC_LOGE("format not support yet %d", imgFormat);
            break;
    }
#undef  addBufPlane

    sp<ImageStreamInfo>
        pStreamInfo = new ImageStreamInfo(
                streamName,
                streamId,
                streamType,
                maxBufNum, minInitBufNum,
                usageForAllocator, imgFormat, imgSize, bufPlanes,
                transform, 0/*dataSpace*/, secureInfo);

    if( pStreamInfo == NULL ) {
        SEC_LOGE("create ImageStream failed, %s, %#" PRIx64,
                streamName, streamId);
    }

    return pStreamInfo;
}

sp<IImageStreamInfo>
NSCam::security::utils::createRawImageStreamInfo(
    char const*         streamName,
    StreamId_T          streamId,
    MUINT32             streamType,
    size_t              maxBufNum,
    size_t              minInitBufNum,
    MUINT               usageForAllocator,
    MINT                imgFormat,
    MSize const&        imgSize,
    size_t const        stride,
    SecureInfo const&   secureInfo
)
{
    IImageStreamInfo::BufPlanes_t bufPlanes;
    //
#define addBufPlane(planes, height, stride)                                      \
        do{                                                                      \
            size_t _height = (size_t)(height);                                   \
            size_t _stride = (size_t)(stride);                                   \
            IImageStreamInfo::BufPlane bufPlane= { _height * _stride, _stride }; \
            planes.push_back(bufPlane);                                          \
        }while(0)
    switch( imgFormat ) {
        case NSCam::EImageFormat::eImgFmt_BAYER10:
        case NSCam::EImageFormat::eImgFmt_BAYER10_UNPAK:
        case NSCam::EImageFormat::eImgFmt_FG_BAYER10:
        case NSCam::EImageFormat::eImgFmt_BAYER8: // LCSO
        case NSCam::EImageFormat::eImgFmt_BAYER12:
        case NSCam::EImageFormat::eImgFmt_STA_BYTE:
        case NSCam::EImageFormat::eImgFmt_STA_2BYTE: // LCSO with LCE3.0
        case NSCam::EImageFormat::eImgFmt_FG_BAYER12:
        case NSCam::EImageFormat::eImgFmt_YUY2:
        case NSCam::EImageFormat::eImgFmt_UYVY:
        case NSCam::EImageFormat::eImgFmt_VYUY:
        case NSCam::EImageFormat::eImgFmt_YVYU:
            addBufPlane(bufPlanes , imgSize.h, stride);
            break;
        default:
            SEC_LOGE("format not support yet %d", imgFormat);
            break;
    }
#undef  addBufPlane

    sp<ImageStreamInfo>
        pStreamInfo = new ImageStreamInfo(
                streamName,
                streamId,
                streamType,
                maxBufNum, minInitBufNum,
                usageForAllocator, imgFormat, imgSize, bufPlanes,
                0/*transform*/, 0/*dataSpace*/, secureInfo);

    if( pStreamInfo == NULL ) {
        SEC_LOGE("create ImageStream failed, %s, %#" PRIx64,
                streamName, streamId);
    }

    return pStreamInfo;
}
