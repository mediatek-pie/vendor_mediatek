#ifndef VENDOR_MEDIATEK_HARDWARE_CAMERA_FRHANDLER_V1_0_FRCLIENTCALLBACK_H
#define VENDOR_MEDIATEK_HARDWARE_CAMERA_FRHANDLER_V1_0_FRCLIENTCALLBACK_H

#include <vendor/mediatek/hardware/camera/frhandler/1.0/IFRClientCallback.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>

namespace vendor {
namespace mediatek {
namespace hardware {
namespace camera {
namespace frhandler {
namespace V1_0 {
namespace implementation {

using ::android::hardware::hidl_array;
using ::android::hardware::hidl_memory;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_vec;
using ::android::hardware::Return;
using ::android::hardware::Void;
using ::android::sp;

struct FRClientCallback : public IFRClientCallback {
    // Methods from IFRClientCallback follow.
    Return<void> onFRClientCallback(int32_t fr_cmd_id, int32_t errCode, const FRCbParam& param) override;

    // Methods from ::android::hidl::base::V1_0::IBase follow.

};

// FIXME: most likely delete, this is only for passthrough implementations
// extern "C" IFRClientCallback* HIDL_FETCH_IFRClientCallback(const char* name);

}  // namespace implementation
}  // namespace V1_0
}  // namespace frhandler
}  // namespace camera
}  // namespace hardware
}  // namespace mediatek
}  // namespace vendor

#endif  // VENDOR_MEDIATEK_HARDWARE_CAMERA_FRHANDLER_V1_0_FRCLIENTCALLBACK_H
