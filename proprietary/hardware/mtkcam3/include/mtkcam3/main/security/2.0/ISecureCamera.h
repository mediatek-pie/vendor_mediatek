/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_HARDWARE_MTKCAM_INCLUDE_MTKCAM3_MAIN_SECURITY_V2_0_ISECURECAMERA_H_
#define _MTK_HARDWARE_MTKCAM_INCLUDE_MTKCAM3_MAIN_SECURITY_V2_0_ISECURECAMERA_H_

#include <mtkcam3/main/security/2.0/Types.h>

#include <type_traits>

namespace NSCam {
namespace security {
namespace V2_0 {

using namespace NSCam::security::V2_0::types;

// ---------------------------------------------------------------------------

enum class Command : uint32_t {
    SET_SRC_DEV,
    STREAMING_ON,
    STREAMING_OFF,
    REGISTER_CB_FUNC
};

/// Generic function pointer
typedef void (*callback_function_pointer_t)();

/// Callback function descriptor
/// NOTE: These descriptors are covered in the same mask,
///       and only one can be registered.
typedef enum {
    CALLBACK_NONE    = 0x0,
    CALLBACK_ADDR    = 0x1,
    CALLBACK_MASK    = 0xFUL,
    // add new callback from here
    CALLBACK_ALL     = 0x7FFFFFFFUL
} callback_descriptor_t;

/// Callback
class SecureCameraCallback {
public:
    SecureCameraCallback() = delete;
    virtual ~SecureCameraCallback() = default;

    template <typename PFN, typename HOOK>
    static inline SecureCameraCallback createCallback(
            const callback_descriptor_t desc,
            const HOOK hook)
    {
        return SecureCameraCallback(desc, asFunction<PFN>(hook));
    }

    callback_descriptor_t inline getDescriptor() const { return mDesc; }
    callback_function_pointer_t inline getHook() const { return mHook; }

private:
    SecureCameraCallback(
            const callback_descriptor_t desc,
            const callback_function_pointer_t hook)
        : mDesc(desc),
        mHook(hook)
    {};

    // function loaders
    template<typename PFN, typename T>
    static inline callback_function_pointer_t asFunction(T function)
    {
        static_assert(std::is_same<PFN, T>::value,
                "Mismatched type of callback function pointer");
        return reinterpret_cast<callback_function_pointer_t>(function);
    }

    // indicate a valid callback descriptor
    callback_descriptor_t       mDesc;
    // callback function pointer corresponding to the callback descriptor
    callback_function_pointer_t mHook;
};

// ---------------------------------------------------------------------------

/// Function pointer prototypes
///
/// NOTE: Add new callback prototype below the existing ones.
///       The name must be the combination of NewType and your function name.
///       (NewType should be defined in callback_descriptor_t)
///
/// EXAMPLE:
/// // Function prototype
/// typedef Result (*PFN_CALLBACK_ADDR)(Buffer buffer);
///
/// // Function implementation
/// Result __addrCallback(Buffer buffer) {
///     return SUCCESS;
/// }
///
/// // Create callback object
/// Callback cbAddr = Callback::createCallback<PFN_CALLBACK_ADDR>(
///         CALLBACK_ADDR, __addrCallback);
typedef Result (*PFN_CALLBACK_ADDR)(const Buffer& buffer);

// ---------------------------------------------------------------------------

class ISecureCamera
{
public:
    virtual ~ISecureCamera() = default;

    static ISecureCamera* loadModule();

    /// Open secure camera device.
    virtual Result open(Path path) = 0;

    /// Close secure camera device.
    virtual Result close() = 0;

    virtual Result init() = 0;

    virtual Result unInit() = 0;

    /// secure camera send command.
    /// the commands are defined as NSCam::security::Command
    virtual Result sendCommand
        (Command cmd, intptr_t arg1 = 0, intptr_t arg2 = 0) = 0;
};

} // namespace V2_0
} // namespace security
} // namespace NSCam

#endif // _MTK_HARDWARE_MTKCAM_INCLUDE_MTKCAM3_MAIN_SECURITY_V2_0_ISECURECAMERA_H_
