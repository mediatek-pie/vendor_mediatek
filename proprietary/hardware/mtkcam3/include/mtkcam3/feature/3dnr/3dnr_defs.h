/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/

/**
* @file 3dnr_defs.h
*
* 3DNR Defs Header File
*/


#ifndef _3DNR_DEFS_H_
#define _3DNR_DEFS_H_

#include <mtkcam/def/BuiltinTypes.h>

namespace NSCam{
namespace NR3D{

/*********************************************************************
* Define Value
*********************************************************************/

static const char *LOG_LEVEL_PROPERTY = "vendor.camera.3dnr.log.level";
static const char *DEBUG_LEVEL_PROPERTY = "debug.camera.3dnr.level";
static const char *DEBUG_RESET_GMV_PROPERTY = "debug.camera.3dnr.reset_gmv";
#define NR3D_LMV_MAX_GMV_DEFAULT 32


/*********************************************************************
* ENUM
*********************************************************************/

/**
*@brief Return enum of 3DNR Modr for P2 flow.
*/
enum E3DNR_MODE_MASK
{
    E3DNR_MODE_MASK_HAL_FORCE_SUPPORT     = 1 << 0,  // hal force support 3DNR
    E3DNR_MODE_MASK_UI_SUPPORT            = 1 << 1,  // feature option on/off
    E3DNR_MODE_MASK_RSC_EN                = 1 << 5,  // enable RSC support for 3DNR
    E3DNR_MODE_MASK_SL2E_EN               = 1 << 6,  // enable SL2E
};

#define E3DNR_MODE_MASK_ENABLED(x, mask)   ((x) & (mask))

/*********************************************************************
* Struct
*********************************************************************/

struct GyroData
{
    MBOOL isValid;
    MFLOAT x, y, z;

    GyroData() : isValid(MFALSE), x(0.0f), y(0.0f), z(0.0f)
    {
    }
};


/******************************************************************************
 *
 * @struct NR3DMVInfo
 * @brief parameter for set nr3d
 * @details
 *
 ******************************************************************************/

struct NR3DMVInfo
{
    enum Status {
        INVALID = 0,
        VALID
    };

    // 3dnr vipi: needs x_int/y_int/gmvX/gmvY
    // ISP smoothNR3D: needs gmvX/gmvY/confX/confY/maxGMV
    MINT32 status;
    MUINT32 x_int;
    MUINT32 y_int;
    MINT32 gmvX;
    MINT32 gmvY;
    MINT32 confX;
    MINT32 confY;
    MINT32 maxGMV;

    NR3DMVInfo()
        : status(INVALID)
        , x_int(0)
        , y_int(0)
        , gmvX(0)
        , gmvY(0)
        , confX(0)
        , confY(0)
        , maxGMV(NR3D_LMV_MAX_GMV_DEFAULT)
        {};
};

/******************************************************************************
 *
 * @struct NR3DParam
 * @brief parameter for NR3D HW setting
 * @details
 *
 ******************************************************************************/
struct NR3DParam
{
public:
    MUINT32 ctrl_onEn;
    MUINT32 onOff_onOfStX;
    MUINT32 onOff_onOfStY;
    MUINT32 onSiz_onWd;
    MUINT32 onSiz_onHt;
    MUINT32 vipi_offst;     //in byte
    MUINT32 vipi_readW;     //in pixel
    MUINT32 vipi_readH;     //in pixel

   NR3DParam()
       : ctrl_onEn(0x0)
       , onOff_onOfStX(0x0)
       , onOff_onOfStY(0x0)
       , onSiz_onWd(0x0)
       , onSiz_onHt(0x0)
       , vipi_offst(0x0)
       , vipi_readW(0x0)
       , vipi_readH(0x0)
   {
   }
};

struct NR3DTuningInfo
{
    MBOOL canEnable3dnrOnFrame;
    MINT32 isoThreshold;
    NR3DMVInfo mvInfo;
    MSize inputSize; // of NR3D hardware
    MRect inputCrop;
    GyroData gyroData;
    NR3DParam *pNr3dParam = NULL;
};


}; // namespace NR3D
}; // namespace NSCam
#endif // _3DNR_DEFS_H_

