/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_HARDWARE_MTKCAM_INCLUDE_MTKCAM_PIPELINE_POLICY_CUSTOMSUPERNIGHTEVDECISION_H_
#define _MTK_HARDWARE_MTKCAM_INCLUDE_MTKCAM_PIPELINE_POLICY_CUSTOMSUPERNIGHTEVDECISION_H_
//
//// MTKCAM
#include <mtkcam/drv/IHalSensor.h>
#include <mtkcam/aaa/IHal3A.h>
//// MTKCAM3
#include <mtkcam3/3rdparty/core/EvDecision.h>
//
#include <map>
#include <memory>
#include <vector>
#include <unordered_map>


using namespace NSCam::v3::pipeline::policy::evdecision;
/******************************************************************************
*
******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace policy {
namespace supernightevdecision {

struct Current3AInfo
{
    // iso
    uint32_t iso       = 0;

    // shutter exposure time
    uint32_t exposureTime  = 0;

    // lux value (0.0001 ~ 100000)*10000
    uint32_t luxValue = 0;

    // preview average brightness (0 ~ 255)
    uint32_t avgY = 0;
};

/******************************************************************************
 *
 ******************************************************************************/
class CustomSuperNightEvDecision : public EvDecision
{
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Data Members.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
private:

private:
    CreationParams mDecisionParams;

    // current status
    uint32_t mFrameIndex = 0;
    uint32_t mTotalFramesCount = 0;
    bool mIsLastFrame = false;
    bool mIsKeepTuning = false;

    // sensor name
    std::string mSensorName;

    // property for debug
    int32_t mbDebug = 0;
    int32_t mForcedSuperNightState = 0;

    // Hal3a
    std::mutex mHal3aLocker;
    std::shared_ptr<NS3Av3::IHal3A> mHal3a = nullptr;
    // preview 3A info before super night trigger
    Current3AInfo mPreview3AInfo;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Operations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
protected:

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Interfaces.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:
    // EvDecision Interfaces.
    virtual auto    reprocessTriggered(
                    ) -> bool override;

    virtual auto    evaluateEvDecision(
                        DecisionOutputParams* out,
                        DecisionInputParams const* in
                    ) -> bool override;
private:
    // use to get supported frames count by super night state
    virtual auto    getSupportedFramesCountByState(
                        uint32_t  superNightState,
                        uint32_t& superNightFramesCount
                    ) -> bool;

   // use to get supported frames count by super night state
    // Lamda function: use to get dedicated ev setting by super night state and frame index
    virtual auto    getEvSettingByHint(
                        uint32_t  superNightState,
                        uint32_t  frameIndex,
                        int32_t& evSetting
                    ) -> bool;

    // use to get current 3A info for ev decision
    virtual auto    getCurrent3AInfo(
                        Current3AInfo& current3AInfo
                    ) -> bool;

public:
    // CustomSuperNightEvDecision Interfaces.
    CustomSuperNightEvDecision(CreationParams const& params);

};

/******************************************************************************
 *
 ******************************************************************************/
};  //namespace
};  //namespace policy
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam
#endif  //_MTK_HARDWARE_MTKCAM_INCLUDE_MTKCAM_PIPELINE_POLICY_CUSTOMSUPERNIGHTEVDECISION_H_

