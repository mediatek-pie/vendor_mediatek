/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
 /*
** $Log: fdvt_hal.cpp $
 *
*/
#define LOG_TAG "mHalFDVT"

#define FDVT_DDP_SUPPORT

#include <utils/Errors.h>
#include <utils/Mutex.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <cutils/atomic.h>
#include <cutils/properties.h>
#include <faces.h>

#include "fdvt_hal.h"
#include <mtkcam/utils/std/Log.h>

#include "camera_custom_fd.h"

#include <mtkcam/def/PriorityDefs.h>
#include <sys/prctl.h>

using namespace android;


//****************************//
//-------------------------------------------//
//  Global face detection related parameter  //
//-------------------------------------------//

#define USE_SW_FD_TO_DEBUG (0)
#if (MTKCAM_FDFT_USE_HW == '1') && (USE_SW_FD_TO_DEBUG == 0)
#define USE_HW_FD (1)

#define USE_HW_FD (0)
#endif

#if (MTKCAM_FDFT_SUB_VERSION == '1')
#define HW_FD_SUBVERSION (1)
#elif (MTKCAM_FDFT_SUB_VERSION == '2')
#define HW_FD_SUBVERSION (2)
#else
#define HW_FD_SUBVERSION (0)
#endif

#define MHAL_NO_ERROR 0
#define MHAL_INPUT_SIZE_ERROR 1
#define MHAL_UNINIT_ERROR 2
#define MHAL_REINIT_ERROR 3

#undef MAX_FACE_NUM
#define MAX_FACE_NUM 15

// FD algo parameter predefine
#define MHAL_FDVT_FDBUF_W (400)
#define FD_DRIVER_MAX_WIDTH (640)
#define FD_DRIVER_MAX_HEIGHT (640)


#define DLFD_SMOOTH_PARAM (5)
#define DLFD_SMOOTH_PARAM_BASE (2816)


static Mutex       gLock;
static Mutex       gInitLock;
//****************************//

/******************************************************************************
*
*******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)

/*******************************************************************************
* static function
********************************************************************************/
template <class T1, class T2>
void handleAttribResult(T1 &AlgoResult, T2 &DrvResult)
{

}

// Create RGB565 QVGA for Tracking, use another thread
void* halFDVT::onAttThreadLoop(void* arg)
{
    halFDVT *_this = reinterpret_cast<halFDVT*>(arg);
    while(1) {
        ::sem_wait(&(_this->mSemAttExec));
        if(_this->mAttStop) {
            break;
        }

    }
    return NULL;
}


/*******************************************************************************
* callback
********************************************************************************/
void FDVTCallback(EGNParams<FDVTConfig>& rParams)
{
    halFDVT *pFDHal = rParams.mpCookie;
    pFDHal->handleFDCallback(rParams);
    return;
}

/*******************************************************************************
* Public API
********************************************************************************/
halFDBase*
halFDVT::
getInstance()
{
    Mutex::Autolock _l(gLock);
    halFDBase* pHalFD;

    pHalFD = new halFDVT();

    return  pHalFD;

}

void
halFDVT::
destroyInstance()
{
    Mutex::Autolock _l(gLock);
    delete this;

}

halFDVT::halFDVT()
{
    mpMTKFDVTObj = NULL;

    mFDW = 0;
    mFDH = 0;
    mInited = 0;
    mDoDumpImage = 0;
    mFDRefresh = 3;
    mpAttrWorkBuf = NULL;
    mAllocator = NULL;
    mAttStop = 0;

    mpMTKFDVTObj = MTKDetection::createInstance(DRV_FD_OBJ_HW);
}

halFDVT::~halFDVT()
{
    mFDW = 0;
    mFDH = 0;

    {
        mAttStop = 1;
        ::sem_post(&mSemAttExec);
        pthread_join(mAttThread, NULL);
        sem_destroy(&mSemAttExec);
    }

    if (mpMTKFDVTObj) {
        mpMTKFDVTObj->destroyInstance();
    }
    MY_LOGD("[Destroy] mpMTKFDVTObj->destroyInstance");

    mpMTKFDVTObj = NULL;
}

MINT32
halFDVT::halFDInit(
    MUINT32 fdW,
    MUINT32 fdH,
    MUINT8 *WorkingBuffer,
    MUINT32 WorkingBufferSize,
    MBOOL   SWResizerEnable,
    MUINT8  Current_mode,  //0:FD, 1:SD, 2:vFB 3:CFB 4:VSDOF
    MINT32 FldNum
)
{
    Mutex::Autolock _l(gInitLock);
    MUINT32 i;
    MINT32 err = MHAL_NO_ERROR;
    MTKFDFTInitInfo FDFTInitInfo;
    FD_Customize_PARA FDCustomData;

    if(mInited) {
        MY_LOGW("Warning!!! FDVT HAL OBJ is already inited!!!!");
        MY_LOGW("Old Width/Height : %d/%d, Parameter Width/Height : %d/%d", mFDW, mFDH, fdW, fdH);
        return MHAL_REINIT_ERROR;
    }
    {
        char cLogLevel[PROPERTY_VALUE_MAX];
        ::property_get("vendor.debug.camera.fd.dumpimage", cLogLevel, "0");
        mDoDumpImage = atoi(cLogLevel);
    }
    // Start initial FD
    mCurrentMode = Current_mode;

    MY_LOGD("[mHalFDInit] Current_mode:%d, SrcW:%d, SrcH:%d, ",Current_mode, fdW, fdH);


    get_fd_CustomizeData(&FDCustomData);

    // Set FD/FT buffer resolution
    mFDW = fdW;
    mFDH = fdH;
    mFTWidth  = fdW;
    mFTHeight = fdH;

    mFDhw_W = FDFTInitInfo.FDBufWidth = MHAL_FDVT_FDBUF_W;
    mFDhw_H = FDFTInitInfo.FDBufHeight = MHAL_FDVT_FDBUF_W * mFDH / mFDW;
    FDFTInitInfo.FDTBufWidth = mFTWidth;
    FDFTInitInfo.FDTBufHeight =  mFTHeight;
    FDFTInitInfo.FDSrcWidth = mFDW;
    FDFTInitInfo.FDSrcHeight = mFDH;

    // Set FD/FT initial parameters
    mFDFilterRatio = FDCustomData.FDSizeRatio;
    FDFTInitInfo.WorkingBufAddr = WorkingBuffer;
    FDFTInitInfo.WorkingBufSize = WorkingBufferSize;
    FDFTInitInfo.FDThreadNum = FDCustomData.FDThreadNum;
    #if (USE_SW_FD_TO_DEBUG)
    FDFTInitInfo.FDThreshold = 256;
    #else
    FDFTInitInfo.FDThreshold = FDCustomData.FDThreshold;
    #endif
    FDFTInitInfo.MajorFaceDecision = FDCustomData.MajorFaceDecision;
    FDFTInitInfo.OTRatio = FDCustomData.OTRatio;
    FDFTInitInfo.SmoothLevel = FDCustomData.SmoothLevel;
    FDFTInitInfo.Momentum = DLFD_SMOOTH_PARAM_BASE + DLFD_SMOOTH_PARAM;// ALGO tuning
    FDFTInitInfo.MaxTrackCount = FDCustomData.MaxTrackCount;
    if(mCurrentMode == HAL_FD_MODE_VFB)
        FDFTInitInfo.FDSkipStep = 1;   //FB mode
    else
        FDFTInitInfo.FDSkipStep = FDCustomData.FDSkipStep;

    FDFTInitInfo.FDRectify = FDCustomData.FDRectify;

    FDFTInitInfo.OTFlow = FDCustomData.OTFlow;
    if(mCurrentMode == HAL_FD_MODE_VFB) {
        FDFTInitInfo.OTFlow = 1;
        if(FDFTInitInfo.OTFlow==0)
            FDFTInitInfo.FDRefresh = 90;   //FB mode
        else
            FDFTInitInfo.FDRefresh = FDCustomData.FDRefresh;   //FB mode
    }
    else{
        FDFTInitInfo.FDRefresh = FDCustomData.FDRefresh;
    }
    mFDRefresh = FDFTInitInfo.FDRefresh;
    //FDFTInitInfo.FDImageArrayNum = 14;
    //FDFTInitInfo.FDImageWidthArray = mimage_width_array;
    //FDFTInitInfo.FDImageHeightArray = mimage_height_array;
    FDFTInitInfo.FDCurrent_mode = mCurrentMode;
    FDFTInitInfo.FDModel = FDCustomData.FDModel;
    FDFTInitInfo.FDMinFaceLevel = 0;
    FDFTInitInfo.FDMaxFaceLevel = 13;
    FDFTInitInfo.FDImgFmtCH1 = FACEDETECT_IMG_Y_SINGLE;
    FDFTInitInfo.FDImgFmtCH2 = FACEDETECT_IMG_RGB565;
    FDFTInitInfo.SDImgFmtCH1 = FACEDETECT_IMG_Y_SCALES;
    FDFTInitInfo.SDImgFmtCH2 = FACEDETECT_IMG_Y_SINGLE;
    FDFTInitInfo.SDThreshold = FDCustomData.SDThreshold;
    FDFTInitInfo.SDMainFaceMust = FDCustomData.SDMainFaceMust;
    FDFTInitInfo.GSensor = FDCustomData.GSensor;
    FDFTInitInfo.GenScaleImageBySw = 1;
    FDFTInitInfo.ParallelRGB565Conversion = false;
    FDFTInitInfo.LockOtBufferFunc = nullptr;
    FDFTInitInfo.UnlockOtBufferFunc = nullptr;
    FDFTInitInfo.lockAgent = nullptr;
    FDFTInitInfo.DisLimit = 0;
    FDFTInitInfo.DecreaseStep = 0;
    FDFTInitInfo.OTBndOverlap = 8;
    FDFTInitInfo.OTds = 2;
    FDFTInitInfo.OTtype = 1;
    {
        FDFTInitInfo.DelayThreshold = 293; // 127 is default value for FD3.5
        FDFTInitInfo.DelayCount = 2; // 2 is default value
        FDFTInitInfo.DisLimit = 1; // 2 is default value
        FDFTInitInfo.DecreaseStep = 48; // 2 is default value
    }
    FDFTInitInfo.LandmarkEnableCnt = FldNum;
    FDFTInitInfo.SilentModeFDSkipNum = 2;
    FDFTInitInfo.FLDAttribConfig = 0; // enable gender detection
    FDFTInitInfo.GenderEnableCnt = 0;
    FDFTInitInfo.PoseEnableCnt = 0;
    //FDFTInitInfo.gender_status_mutexAddr = &mattribureMutex;
    FDFTInitInfo.FDVersion = MTKCAM_HWFD_MAIN_VERSION + HW_FD_SUBVERSION;
    #if (USE_HW_FD)&&(HW_FD_SUBVERSION == 2)
    FDFTInitInfo.FDMINSZ = 0;
    #endif
    mSkipPartialFD = ::property_get_int32("vendor.debug.camera.fd.skipdlfd", 0);
    mSkipAllFD     = ::property_get_int32("vendor.debug.camera.fd.skipallfd", 0);
    if (mSkipPartialFD == 0 || mSkipAllFD == 0)
    {
        mSkipPartialFD = FDCustomData.SkipPartialFD + 1;
        mSkipAllFD = FDCustomData.SkipAllFD + 1;
    }
    // allocate attribute buffer
    if (FDFTInitInfo.GenderEnableCnt || FDFTInitInfo.PoseEnableCnt)
    {
        mworkbufList = (unsigned char**)malloc(sizeof(unsigned char*) * MAX_CROP_NUM);
        mAllocator = IImageBufferAllocator::getInstance();
        IImageBufferAllocator::ImgParam imgParam(MAX_CROP_NUM * sizeof(unsigned char) * MAX_CROP_W * MAX_CROP_W * 2, 16);
        mpAttrWorkBuf = mAllocator->alloc("FDAttributeBuf", imgParam);
        if ( mpAttrWorkBuf.get() == 0 )
        {
            MY_LOGE("NULL Buffer\n");
            return MFALSE;
        }
        if ( !mpAttrWorkBuf->lockBuf( "FDAttributeBuf", (eBUFFER_USAGE_HW_CAMERA_READ | eBUFFER_USAGE_SW_MASK)) )
        {
            MY_LOGE("lock Buffer failed\n");
            return MFALSE;
        }
        MY_LOGD("allocator buffer : %" PRIXPTR "", mpAttrWorkBuf->getBufVA(0));
        for(int i = 0; i < MAX_CROP_NUM; i++)
        {
            mworkbufList[i] = (unsigned char*)(mpAttrWorkBuf->getBufVA(0) + (i * sizeof(unsigned char) * MAX_CROP_W * MAX_CROP_W * 2));
            mworkbufListPA[i] = (mpAttrWorkBuf->getBufPA(0) + (i * sizeof(unsigned char) * MAX_CROP_W * MAX_CROP_W * 2));
            MY_LOGD("mworkbufList[%d] : %p", i, mworkbufList[i]);
        }
        mAttrBusy = false;
    }
    sem_init(&mSemAttExec, 0, 0);
    pthread_create(&mAttThread, NULL, onAttThreadLoop, this);
    // init driver
    mpFDStream = NSCam::NSIoPipe::NSEgn::IEgnStream<FDVTConfig>::createInstance(LOG_TAG);
    mpFDStream->initFD(FD_DRIVER_MAX_WIDTH, FD_DRIVER_MAX_HEIGHT);
    // dump initial info
    dumpFDParam(FDFTInitInfo);
    // Set initial info to FD algo
    mpMTKFDVTObj->FDVTInit(&FDFTInitInfo);

    MY_LOGD("[%s] End", __FUNCTION__);
    mFrameCount = 0;
    mDetectedFaceNum = 0;

    mInited = 1;
    return err;
}

MINT32
halFDVT::halFDGetVersion(
)
{
    return HAL_FD_VER_HW51;
}

MINT32
halFDVT::halFDDo(
struct FD_Frame_Parameters &Param
)
{
    Mutex::Autolock _l(gInitLock);
    FdOptions FDOps;
    MUINT8* y_vga;
    MBOOL SDEnable;
    MINT32 StartPos = 0;
    MINT32 ScaleNum = mUserScaleNum;
    FDVT_OPERATION_MODE_ENUM Mode = FDVT_IDLE_MODE;
    #if USE_PORTRAIT
    MINT32 PortraitRunning = 0;
    #endif
    fd_cal_struct *pfd_cal_data;


    SDEnable = Param.SDEnable && (mCurrentMode == HAL_FD_MODE_SD);
    MY_LOGD("SD Status: SDEnable:%d, img_w:%d, img_h:%d, Src_Phy_Addr:%p,", mCurrentMode, mFTWidth, mFTHeight, Param.pImageBufferPhyP0);

    if(!mInited) {
        return MHAL_UNINIT_ERROR;
    }

    #if USE_PORTRAIT
    if (((mFrameCount % mSkipAllFD) != 0) && (mNeedRunPort == 0))
    #else
    if (((mFrameCount % mSkipAllFD) != 0))
    #endif
    {
        MY_LOGD("skip this frame : mFrameCount:%d, mSkipAllFD:%d", mFrameCount, mSkipAllFD);
        mFrameCount++;
        return MHAL_NO_ERROR;
    }

    FACEDETECT_GSENSOR_DIRECTION direction;
    if( Param.Rotation_Info == 0 )
        direction = FACEDETECT_GSENSOR_DIRECTION_0;
    else if( Param.Rotation_Info == 90 )
        direction = FACEDETECT_GSENSOR_DIRECTION_270;
    else if( Param.Rotation_Info == 270 )
        direction = FACEDETECT_GSENSOR_DIRECTION_90;
    else if( Param.Rotation_Info == 180 )
        direction = FACEDETECT_GSENSOR_DIRECTION_180;
    else
        direction = FACEDETECT_GSENSOR_DIRECTION_NO_SENSOR;

    // Set FD operation
    FDOps.fd_state = FDVT_GFD_MODE;
    FDOps.direction = direction;
    FDOps.fd_scale_count = ScaleNum;
    FDOps.fd_scale_start_position = StartPos;
    FDOps.gfd_fast_mode = 0;
    FDOps.ae_stable = Param.AEStable;
    FDOps.ForceFDMode = FDVT_GFD_MODE;
    if (Param.pImageBufferPhyP2 != NULL) {
        FDOps.inputPlaneCount = 3;
    } else if (Param.pImageBufferPhyP1 != NULL) {
        FDOps.inputPlaneCount = 2;
    } else {
        FDOps.inputPlaneCount = 1;
    }
    FDOps.ImageBufferPhyPlane1 = Param.pImageBufferPhyP0;
    FDOps.ImageBufferPhyPlane2 = Param.pImageBufferPhyP1;
    FDOps.ImageBufferPhyPlane3 = Param.pImageBufferPhyP2;
    FDOps.ImageScaleBuffer = mpImageScaleBuffer;
    FDOps.ImageBufferRGB565 = Param.pRGB565Image;
    FDOps.ImageBufferSrcVirtual = (MUINT8*)Param.pImageBufferVirtual;
    FDOps.startW = 0;
    FDOps.startH = 0;
    FDOps.model_version = 0;

    {
        FDVT_OPERATION_MODE_ENUM mode;
        mpMTKFDVTObj->FDVTGetMode(&mode);

        MY_LOGD("FDVTMain IN : %p", Param.pImageBufferPhyP0);
        mpMTKFDVTObj->FDVTMain(&FDOps);
        if (FDOps.doPhase2)
        {
            pfd_cal_data = mpMTKFDVTObj->FDGetCalData();//Get From Algo

            for(int i = 0; i < MAX_FACE_SEL_NUM; i++) {
                pfd_cal_data->display_flag[i] = (kal_bool)0;
            }
            if ((mFrameCount % mSkipPartialFD) == 0)
            {
                MY_LOGD("FDVTMain out1");
                doHWFaceDetection(pfd_cal_data);
            }
            else
            {
                MY_LOGD("skip FD core, reset FD cal data");
            }

            MY_LOGD("FDVTMain out2");

            mpMTKFDVTObj->FDVTMainFastPhase(mGammaCtrl);
            if ( FDOps.doGender || FDOps.doPose ) // run attribute
            {
                Mutex::Autolock _l(mattribureMutex);
                if ( !mAttrBusy )
                {
                    mpMTKFDVTObj->FDVTMainCropPhase(mtodoList, mbufStatus, mworkbufList, mpatchSize, mAIEAttrTask);
                }
                mpMTKFDVTObj->FDVTMainJoinPhase(mbufStatus[AIE_ATTR_TYPE_GENDER], mgender_FM, AIE_ATTR_TYPE_GENDER);
                mpMTKFDVTObj->FDVTMainJoinPhase(mbufStatus[AIE_ATTR_TYPE_POSE], mpose_FM, AIE_ATTR_TYPE_POSE);
            }

            mpMTKFDVTObj->FDVTMainPostPhase();

        }
        MY_LOGD("FDVTMain out3");

    }

    mFrameCount++;

    return MHAL_NO_ERROR;
}

// internal function
bool
halFDVT::handleFDCallback(
    EGNParams<FDVTConfig>& rParams
)
{
    Mutex::Autolock _l(mDriverLock);
    if (rParams.FD_MODE == FDMODE)
    {
        mFDCond.signal();
    }
    else
    {
        mAttrCond.signal();
    }

    return true;
}

bool
halFDVT::doHWAttribute(
)
{
    EGNParams<FDVTConfig> FdvtParams;
    FDVTConfig FdvtConfig;

    for (int attr = 0; attr < MAX_AIE_ATTR_TYPE; attr++)
    {
        for (int task = 0; task < mAIEAttrTask[attr]; task++)
        {
            int psz = mpatchSize[mtodoList[attr][task]];
            if (psz == 0)
            {
                break;
            }
            {
                Mutex::Autolock _l(mDriverLock);
                FdvtParams.mpEngineID = eFDVT;
                FdvtParams.mpfnCallback = handleFDCallback;
                FdvtParams.mpCookie = this;

                FdvtConfig.FD_MODE = (attr == AIE_ATTR_TYPE_GENDER) ? ATTRIBUTEMODE : POSEMODE;
                FdvtConfig.SRC_IMG_WIDTH = psz;
                FdvtConfig.SRC_IMG_HEIGHT = psz;
                FdvtConfig.SRC_IMG_FMT = FMT_YUYV;
                FdvtConfig.source_img_address = (MUINT64*)mworkbufListPA[mtodoList[attr][task]];
                FdvtConfig.source_img_address_UV = nullptr;

                FdvtParams.mEGNConfigVec.push_back(FdvtConfig);
                mpFDStream->EGNenque(FdvtParams);

                err = mAttrCond.waitRelative(mDriverLock, 2000000000); // wait 2 sec if time out

                if  ( OK != err ) {
                    // error handling
                    MY_LOGE("FD HW driver run attribute timeout...please check...");
                    return false;
                }
            }
            {
                Mutex::Autolock _l(mattribureMutex);
                if (attr == AIE_ATTR_TYPE_GENDER)
                {
                    memcpy(mgender_FM[mtodoList[attr][task]], &(FdvtConfig.ATTRIBUTEOUTPUT), sizeof(struct ATTRIBUTE_RESULT));
                }
                else
                {
                    memcpy(mpose_FM[mtodoList[attr][task]], &(FdvtConfig.POSEOUTPUT), sizeof(struct POSE_RESULT));
                }
                mbufStatus[attr][mtodoList[attr][task]] = 2;
            }
        }
    }

    return true;
}

bool
halFDVT::doHWFaceDetection(
    void *pCal_Data
)
{
    Mutex::Autolock _l(mDriverLock);
    fd_cal_struct *pfd_cal_data = (fd_cal_struct *)pCal_Data;
    EGNParams<FDVTConfig> FdvtParams;
    FDVTConfig FdvtConfig;
    int err = OK;

    FdvtParams.mpEngineID = eFDVT;
    FdvtParams.mpfnCallback = FDVTCallback;
    FdvtParams.mpCookie = this;

    FdvtConfig.FD_MODE = FDMODE;
    FdvtConfig.SRC_IMG_WIDTH = mFDW;
    FdvtConfig.SRC_IMG_HEIGHT = mFDH;
    FdvtConfig.SRC_IMG_FMT = FMT_YUYV;
    FdvtConfig.source_img_address = (MUINT64*)pfd_cal_data->srcbuffer_phyical_addr_plane1;
    FdvtConfig.source_img_address_UV = nullptr;

    FdvtParams.mEGNConfigVec.push_back(FdvtConfig);

    mpFDStream->EGNenque(FdvtParams);

    err = mFDCond.waitRelative(mDriverLock, 2000000000); // wait 2 sec if time out
    if  ( OK != err ) {
        // error handling
        MY_LOGE("FD HW driver timeout...please check...");
        return false;
    }
    float widthRatio = mFDhw_W / mFDW;
    float heightRatio = mFDhw_H / mFDH;

    auto handleFDResult = [&] (FDRESULT &DrvResult, int Idx) -> int
    {
        for(int i = Idx; i < DrvResult.fd_partial_result; i++)
        {

            pfd_cal_data->face_candi_pos_x0[i] = DrvResult.anchor_x0[i] * widthRatio;
            pfd_cal_data->face_candi_pos_y0[i] = DrvResult.anchor_y0[i] * heightRatio;
            pfd_cal_data->face_candi_pos_x1[i] = DrvResult.anchor_x1[i] * widthRatio;
            pfd_cal_data->face_candi_pos_y1[i] = DrvResult.anchor_y1[i] * heightRatio;
            // landmark
            pfd_cal_data->fld_leye_x0[i]  = DrvResult.landmark_x0[i] * widthRatio;
            pfd_cal_data->fld_leye_y0[i]  = DrvResult.landmark_y0[i] * heightRatio;
            pfd_cal_data->fld_leye_x1[i]  = DrvResult.landmark_x1[i] * widthRatio;
            pfd_cal_data->fld_leye_y1[i]  = DrvResult.landmark_y1[i] * heightRatio;

            pfd_cal_data->fld_reye_x0[i]  = DrvResult.landmark_x2[i] * widthRatio;
            pfd_cal_data->fld_reye_y0[i]  = DrvResult.landmark_y2[i] * heightRatio;
            pfd_cal_data->fld_reye_x1[i]  = DrvResult.landmark_x3[i] * widthRatio;
            pfd_cal_data->fld_reye_y1[i]  = DrvResult.landmark_y3[i] * heightRatio;

            pfd_cal_data->fld_nose_x[i]   = DrvResult.landmark_x4[i] * widthRatio;
            pfd_cal_data->fld_nose_y[i]   = DrvResult.landmark_y4[i] * heightRatio;

            pfd_cal_data->fld_mouth_x0[i] = DrvResult.landmark_x5[i] * widthRatio;
            pfd_cal_data->fld_mouth_y0[i] = DrvResult.landmark_y5[i] * heightRatio;
            pfd_cal_data->fld_mouth_x1[i] = DrvResult.landmark_x6[i] * widthRatio;
            pfd_cal_data->fld_mouth_y1[i] = DrvResult.landmark_y6[i] * heightRatio;
            //
            pfd_cal_data->face_reliabiliy_value[i] = DrvResult.anchor_score[i];
            pfd_cal_data->display_flag[i] = (kal_bool)1;
            pfd_cal_data->face_feature_set_index[i] = pfd_cal_data->current_feature_index;
            pfd_cal_data->rip_dir[i] = pfd_cal_data->current_feature_index;
            pfd_cal_data->rop_dir[i] = 0;
            pfd_cal_data->result_type[i] = GFD_RST_TYPE;

            MY_LOGD("caldata: bbox(%d,%d,%d,%d)",
                    pfd_cal_data->face_candi_pos_x0[i],
                    pfd_cal_data->face_candi_pos_y0[i],
                    pfd_cal_data->face_candi_pos_x1[i],
                    pfd_cal_data->face_candi_pos_y1[i] );
        }
        return i;
    }
    int FaceNum = 0;
    FaceNum = handleFDResult(FdvtConfig.FDOUTPUT.PYRAMID0_RESULT, FaceNum);
    FaceNum = handleFDResult(FdvtConfig.FDOUTPUT.PYRAMID1_RESULT, FaceNum);
    FaceNum = handleFDResult(FdvtConfig.FDOUTPUT.PYRAMID2_RESULT, FaceNum);

    if (CC_UNLIKELY(FaceNum != FdvtConfig.FDOUTPUT.FD_TOTAL_NUM))
    {
        MY_LOGW("please check face pyramid total num(%d) != FD_TOTAL_NUM(%d)", FaceNum, FdvtConfig.FDOUTPUT.FD_TOTAL_NUM);
    }


    return true;
}
//

MINT32
halFDVT::halFDUninit(
)
{
    //MHAL_LOG("[halFDUninit] IN \n");
    Mutex::Autolock _l(gInitLock);

    if(!mInited) {
        MY_LOGW("FD HAL Object is already uninited...");
        return MHAL_NO_ERROR;
    }

    mAttStop = 1;
    ::sem_post(&mSemAttExec);
    pthread_join(mAttThread, NULL);
    sem_destroy(&mSemAttExec);

    mpMTKFDVTObj->FDVTReset();

    mpFDStream->uninit();
    mpFDStream->destroyInstance(LOG_TAG);

    if(mpAttrWorkBuf != NULL && mAllocator != NULL) {
        mpAttrWorkBuf->unlockBuf("FDAttributeBuf");
        mAllocator->free(mDupImage.pImg.get());
        mpAttrWorkBuf = NULL;
    }

    mInited = 0;

    return MHAL_NO_ERROR;
}

MINT32
halFDVT::halFDGetFaceInfo(
    MtkCameraFaceMetadata *fd_info_result
)
{
    MUINT8 i;
    MY_LOGD("[GetFaceInfo] NUM_Face:%d,", mFdResult_Num);

    if( (mFdResult_Num < 0) || (mFdResult_Num > 15) )
         mFdResult_Num = 0;

    fd_info_result->number_of_faces =  mFdResult_Num;

    for(i=0;i<mFdResult_Num;i++)
    {
        fd_info_result->faces[i].rect[0] = mFdResult[i].rect[0];
        fd_info_result->faces[i].rect[1] = mFdResult[i].rect[1];
        fd_info_result->faces[i].rect[2] = mFdResult[i].rect[2];
        fd_info_result->faces[i].rect[3] = mFdResult[i].rect[3];
        fd_info_result->faces[i].score = mFdResult[i].score;
        fd_info_result->posInfo[i].rop_dir = mFdResult[i].rop_dir;
        fd_info_result->posInfo[i].rip_dir  = mFdResult[i].rip_dir;
    }

    return MHAL_NO_ERROR;
}

MINT32
halFDVT::halFDGetFaceResult(
    MtkCameraFaceMetadata *fd_result,
    MINT32 /*ResultMode*/
)
{

    MINT32 faceCnt = 0;
    result pbuf[MAX_FACE_NUM];
    MUINT8 i;
    MINT8 DrawMode = 0;
    MINT32 PrevFaceNum = mDetectedFaceNum;

    faceCnt = mpMTKFDVTObj->FDVTGetResult((MUINT8 *) pbuf, FACEDETECT_TRACKING_DISPLAY);

    MY_LOGD("[%s]first scale W(%d) H(%d)", __FUNCTION__, mimage_width_array[0], mimage_height_array[0]);
    mpMTKFDVTObj->FDVTGetICSResult((MUINT8 *) fd_result, (MUINT8 *) pbuf, mimage_width_array[0], mimage_height_array[0], 0, 0, 0, DrawMode);

    mDetectedFaceNum = mFdResult_Num = fd_result->number_of_faces;

    fd_result->CNNFaces.PortEnable = 0;
    fd_result->CNNFaces.IsTrueFace = 0;

    #if USE_PORTRAIT
    if (mNeedRunPort && mvCNNout.size() >= 2)
    {
        MY_LOGD("mvCNNout : %f, %f", mvCNNout[0], mvCNNout[1]);
        fd_result->CNNFaces.PortEnable = 1;
        fd_result->CNNFaces.CnnResult0 = mvCNNout[0];
        fd_result->CNNFaces.CnnResult1 = mvCNNout[1];
        fd_result->CNNFaces.IsTrueFace = (mvCNNout[1] > mvCNNout[0]);
    }
    else
    {
        fd_result->CNNFaces.PortEnable = 0;
        fd_result->CNNFaces.IsTrueFace = 0;
        fd_result->CNNFaces.CnnResult0 = 0.0;
        fd_result->CNNFaces.CnnResult1 = 0.0;
    }
    if((PrevFaceNum != 0 && mDetectedFaceNum == 0) || ((mNeedRunPort > 0 && mNeedRunPort < 50 && mDetectedFaceNum == 0) && fd_result->CNNFaces.IsTrueFace))
    {
        if (gPortInited)
        {
            mNeedRunPort++;
        }
        else
        {
            mNeedRunPort = 0;
        }
    }
    else
    {
        mNeedRunPort = 0;
    }
    #endif

    //Facial Size Filter
    for(i=0;i < mFdResult_Num;i++)
    {
        int diff = fd_result->faces[i].rect[3] - fd_result->faces[i].rect[1];
        float ratio = (float)diff / 2000.0;
        if(ratio < mFDFilterRatio) {
            int j;
            for(j = i; j < (mFdResult_Num - 1); j++) {
                fd_result->faces[j].rect[0] = fd_result->faces[j+1].rect[0];
                fd_result->faces[j].rect[1] = fd_result->faces[j+1].rect[1];
                fd_result->faces[j].rect[2] = fd_result->faces[j+1].rect[2];
                fd_result->faces[j].rect[3] = fd_result->faces[j+1].rect[3];
                fd_result->faces[j].score = fd_result->faces[j+1].score;
                fd_result->faces[j].id = fd_result->faces[j+1].id;
                fd_result->posInfo[j].rop_dir = fd_result->posInfo[j+1].rop_dir;
                fd_result->posInfo[j].rip_dir = fd_result->posInfo[j+1].rip_dir;
                fd_result->faces_type[j] = fd_result->faces_type[j+1];
            }
            fd_result->faces[j].rect[0] = 0;
            fd_result->faces[j].rect[1] = 0;
            fd_result->faces[j].rect[2] = 0;
            fd_result->faces[j].rect[3] = 0;
            fd_result->faces[j].score = 0;
            fd_result->posInfo[j].rop_dir = 0;
            fd_result->posInfo[j].rip_dir = 0;
            fd_result->number_of_faces--;
            mFdResult_Num--;
            faceCnt--;
            i--;
        }
    }

    for(i=0;i<mFdResult_Num;i++)
    {
          mFdResult[i].rect[0] = fd_result->faces[i].rect[0];
          mFdResult[i].rect[1] = fd_result->faces[i].rect[1];
          mFdResult[i].rect[2] = fd_result->faces[i].rect[2];
          mFdResult[i].rect[3] = fd_result->faces[i].rect[3];
          mFdResult[i].score = fd_result->faces[i].score;
          mFdResult[i].rop_dir = fd_result->posInfo[i].rop_dir;
          mFdResult[i].rip_dir  = fd_result->posInfo[i].rip_dir;
    }
    for(i=mFdResult_Num;i<MAX_FACE_NUM;i++)
    {
          mFdResult[i].rect[0] = 0;
          mFdResult[i].rect[1] = 0;
          mFdResult[i].rect[2] = 0;
          mFdResult[i].rect[3] = 0;
          mFdResult[i].score = 0;
          mFdResult[i].rop_dir = 0;
          mFdResult[i].rip_dir  = 0;
    }

    return faceCnt;
}

MINT32
halFDVT::halFDYUYV2ExtractY(
MUINT8 *dstAddr,
MUINT8 *srcAddr,
MUINT32 src_width,
MUINT32 src_height
)
{
    MY_LOGD("DO Extract Y In");
    int i,j;

    int length = src_width*src_height*2;

    for(i=length;i != 0;i-=2)
    {
      *dstAddr++ = *srcAddr;
      srcAddr+=2;
    }

    MY_LOGD("DO Extract Y Out");

    return MHAL_NO_ERROR;
}

void halFDVT::dumpFDImages(MUINT8* pFDImgBuffer, MUINT8* pFTImgBuffer)
{
    char szFileName[100]={'\0'};
    char szFileName1[100]={'\0'};
    FILE * pRawFp;
    FILE * pRawFp1;
    int i4WriteCnt;
    static int count = 0;

    if (mDoDumpImage > 1) {
        sprintf(szFileName1, "/sdcard/FDTest/src_%04dx%04d_%04d_YUV.raw", mFDW, mFDH, count);
        sprintf(szFileName, "/sdcard/FDTest/dst_%04dx%04d_%04d_RGB.raw", mFTWidth, mFTHeight, count);
        count++;
    } else {
        sprintf(szFileName1, "/sdcard/src_%04d_%04d_YUV.raw", mFDW, mFDH);
        sprintf(szFileName, "/sdcard/dst_%04d_%04d_RGB.raw", mFTWidth, mFTHeight);
    }
    pRawFp1 = fopen(szFileName1, "wb");
    if (NULL == pRawFp1 )
    {
        MY_LOGD("Can't open file to save RAW Image\n");
        while(1);
    }
    i4WriteCnt = fwrite((void *)pFDImgBuffer,1, (mFDW * mFDH * 2),pRawFp1);
    fflush(pRawFp1);
    fclose(pRawFp1);

    pRawFp = fopen(szFileName, "wb");
    if (NULL == pRawFp )
    {
        MY_LOGD("Can't open file to save RAW Image\n");
        while(1);
    }
    i4WriteCnt = fwrite((void *)pFTImgBuffer,1, (mFTWidth * mFTHeight * 2),pRawFp);
    fflush(pRawFp);
    fclose(pRawFp);
}

// Dump FD information
void halFDVT::dumpFDParam(MTKFDFTInitInfo &FDFTInitInfo)
{
    MY_LOGD("FDThreshold = %d", FDFTInitInfo.FDThreshold);
    MY_LOGD("MajorFaceDecision = %d", FDFTInitInfo.MajorFaceDecision);
    MY_LOGD("FDTBufW/H = %d/%d", FDFTInitInfo.FDTBufWidth, FDFTInitInfo.FDTBufHeight);

    MY_LOGD("GSensor = %d", FDFTInitInfo.GSensor);
    MY_LOGD("FDManualMode = %d", FDFTInitInfo.FDManualMode);
    #if (USE_HW_FD)&&(HW_FD_SUBVERSION == 2)
    MY_LOGD("FDMINSZ = %d", FDFTInitInfo.FDMINSZ);
    #endif
    #if MTK_ALGO_PSD_MODE
    MY_LOGD("FDVersion = %d, Version = %d", FDFTInitInfo.FDVersion, halFDGetVersion());
    #endif
    MY_LOGD("DL skip : %d, ALL Skip : %d", mSkipPartialFD, mSkipAllFD);
}
