/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_CAMERA_STREAMING_FEATURE_PIPE_TPI_TPI_NODE_H_
#define _MTK_CAMERA_STREAMING_FEATURE_PIPE_TPI_TPI_NODE_H_

#include "tpi_type.h"

typedef struct TPI_InitData_t
{
  unsigned          mSessionID = 0;
  unsigned          mLogicalSensorID = 0;
  TPI_SCENARIO_TYPE mScenario = TPI_SCENARIO_UNKNOWN;
  void*             mVendorSessionCookie = NULL;

  unsigned          mNodeID = 0;
  char              mNodeName[TPI_NODE_NAME_SIZE] = "unknown";
  unsigned          mCustomOption = 0;
  TPI_BufferInfo    mBufferInfoList[TPI_BUFFER_INFO_LIST_SIZE];
  size_t            mBufferInfoListCount = 0;

} TPI_InitData;

typedef struct TPI_EnqueData_t
{
  unsigned          mFrameID = 0;
  void*             mVendorFrameCookie = NULL;
  unsigned          mCustomOption = 0;

  TPI_Meta          mMetaList[TPI_META_LIST_SIZE];
  size_t            mMetaListCount = 0;

  TPI_Buffer        mBufferList[TPI_BUFFER_LIST_SIZE];
  size_t            mBufferListCount = 0;
} TPI_EnqueData;

// try keep TPI_NODE as POD
class TPI_Node
{
public:
  TPI_Node() {}
  virtual ~TPI_Node() {}

public:
  virtual bool onInit(TPI_InitData &initData) = 0;
  virtual bool onUninit() = 0;
  virtual bool onEnque(TPI_EnqueData &enque) = 0;
  // bool onEnqueAsync();
};

#endif // _MTK_CAMERA_STREAMING_FEATURE_PIPE_TPI_TPI_NODE_H_
