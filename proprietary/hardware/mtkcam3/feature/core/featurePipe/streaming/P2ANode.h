/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_CAMERA_STREAMING_FEATURE_PIPE_P2A_NODE_H_
#define _MTK_CAMERA_STREAMING_FEATURE_PIPE_P2A_NODE_H_

#include "StreamingFeatureNode.h"
#include "NormalStreamBase.h"
#include "P2CamContext.h"
#include "TuningHelper.h"

#include <mtkcam/drv/iopipe/PostProc/IHalPostProcPipe.h>
#include <mtkcam3/feature/3dnr/util_3dnr.h>
#include <common/3dnr/3dnr_hal_base.h>
#include <featurePipe/core/include/CamThreadNode.h>
#include "FMHal.h"
#include <mtkcam3/feature/DualCam/FOVHal.h>
#include <mtkcam/aaa/IHal3A.h>
#include <mtkcam/aaa/IHalISP.h>
#include <mtkcam3/feature/utils/p2/P2IO.h>
#include <mtkcam3/feature/utils/p2/P2Util.h>
#include <mtkcam3/feature/utils/SecureBufferControlUtils.h>

using namespace NS3Av3;

class EisHal;

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {

class P2ASrzRecord : public virtual android::RefBase
{
public :
    _SRZ_SIZE_INFO_ srz4;
};

class P2AEnqueData
{
public:
    RequestPtr  mRequest;
    BasicImg    mFullImg;
    ImgBuffer   mPrevFullImg; // VIPI use
    BasicImg    mNextFullImg;
    BasicImg    mSlaveFullImg;
    BasicImg    mSlaveNextFullImg;
    FMResult    mFMResult;
    ImgBuffer   mFE1Img;
    ImgBuffer   mFE2Img;
    ImgBuffer   mFE3Img;
    ImgBuffer   mPureImg;
    ImgBuffer   mSlavePureImg;

    FovP2AResult mFovP2AResult;
    std::vector<SFPOutput>  mRemainingOutputs; // Master may Need additional MDP to generate output
    /* If feature pipe run dynamic tuning, P2ANode need prepare tuning data by itself */
    std::vector<SmartTuningBuffer> tuningBufs;
    std::vector<android::sp<P2ASrzRecord>> tuningSrzs;
};

class P2ATuningExtra
{
public:
    IMetadata *mExtraAppOut = NULL;
    TuningHelper::Scene mScene = TuningHelper::Tuning_Normal;

    P2ATuningExtra(){}
    P2ATuningExtra(IMetadata *extraAppOut)
    :  mExtraAppOut(extraAppOut)
    {}
    P2ATuningExtra(IMetadata *extraAppOut, TuningHelper::Scene scen)
    :  mExtraAppOut(extraAppOut)
    ,  mScene(scen)
    {}
};

class P2ATuningIndex
{
public:
    MINT32 mGenMaster = -1;
    MINT32 mGenSlave = -1;
    MINT32 mPhyMaster = -1;
    MINT32 mPhySlave = -1;
    MINT32 mLargeMaster = -1;
    MINT32 mLargeSlave = -1;
    MINT32 mPureMaster = -1;
    MINT32 mPureSlave = -1;

    MBOOL isGenMasterValid() const {return mGenMaster >= 0;}
    MBOOL isGenSlaveValid() const {return mGenSlave >= 0;}
    MBOOL isPhyMasterValid() const {return mPhyMaster >= 0;}
    MBOOL isPhySlaveValid() const {return mPhySlave >= 0;}
    MBOOL isLargeMasterValid() const {return mLargeMaster >= 0;}
    MBOOL isLargeSlaveValid() const {return mLargeSlave >= 0;}
    MBOOL isPureMasterValid() const {return mPureMaster >= 0;}
    MBOOL isPureSlaveValid() const {return mPureSlave >= 0;}

    MBOOL isMasterMainValid() const
    {
        return isGenMasterValid() || isPureMasterValid() || isPhyMasterValid();
    }
    MUINT32 getMasterMainIndex() const
    {
        return isGenMasterValid() ? mGenMaster : isPureMasterValid() ? mPureMaster : mPhyMaster;
    }
    MBOOL isSlaveMainValid() const
    {
        return isGenSlaveValid() || isPureSlaveValid() || isPhySlaveValid();
    }
    MUINT32 getSlaveMainIndex() const
    {
        return isGenSlaveValid() ? mGenSlave : isPureSlaveValid() ? mPureSlave : mPhySlave;
    }
};

class P2ANode : public StreamingFeatureNode, public NormalStreamBase<P2AEnqueData>
{
public:
    P2ANode(const char *name);
    virtual ~P2ANode();

    MVOID setNormalStream(NSCam::NSIoPipe::NSPostProc::INormalStream *stream, MUINT32 version);

    MVOID setFullImgPool(const android::sp<IBufferPool> &pool, MUINT32 allocate = 0);
    MVOID setPureImgPool(const PoolMap &poolMap);

public:
    virtual MBOOL onData(DataID id, const RequestPtr &data);
    virtual MBOOL onData(DataID id, const RSCData &data);
    virtual IOPolicyType getIOPolicy(StreamType stream, const StreamingReqInfo &reqInfo) const;

protected:
    virtual MBOOL onInit();
    virtual MBOOL onUninit();
    virtual MBOOL onThreadStart();
    virtual MBOOL onThreadStop();
    virtual MBOOL onThreadLoop();

protected:
    virtual MVOID onNormalStreamBaseCB(const QParams &params, const P2AEnqueData &request);

private:
    MVOID handleResultData(const RequestPtr &request, const P2AEnqueData &data);
    MBOOL initP2();
    MVOID uninitP2();
    MBOOL processP2A(const RequestPtr &request, const RSCData &rscData);
    MBOOL prepareQParams(QParams &params, const RequestPtr &request, P2ATuningIndex &tuningIndex);
    MBOOL prepareStreamTag(QParams &params, const RequestPtr &request);
    MBOOL prepareFullImgFromInput(const RequestPtr &request, P2AEnqueData &data);
    MBOOL prepareNonMdpIO(QParams &params, const RequestPtr &request, P2AEnqueData &data, const P2ATuningIndex &tuningIndex);
    MBOOL prepareMasterMdpOuts(QParams &params, const RequestPtr &request, P2AEnqueData &data, const P2ATuningIndex &tuningIndex);
    MBOOL prepareSlaveOuts(QParams &params, const RequestPtr &request, P2AEnqueData &data, const P2ATuningIndex &tuningIndex);
    MBOOL prepareLargeMdpOuts(QParams &params, const RequestPtr &request, MINT32 frameIndex, MUINT32 sensorID);
    MVOID prepareVIPI(FrameParams &frame, const RequestPtr &request, P2AEnqueData &data);
    MVOID prepareFEFM(QParams &params, const RequestPtr &request, P2AEnqueData &data);
    MVOID prepareFDImg(FrameParams &frame, const RequestPtr &request, P2AEnqueData &data);
    MVOID prepareFDCrop(FrameParams &frame, const RequestPtr &request, P2AEnqueData &data);
    MVOID prepareFullImg(FrameParams &frame, const RequestPtr &request, BasicImg &outImg, const FrameInInfo &inInfo, MUINT32 sensorID);
    MVOID preparePureImg(FrameParams &frame, const RequestPtr &request, ImgBuffer &outImg, MUINT32 sensorID);
    MVOID prepareNextFullSFPOut(SFPOutput &output, const RequestPtr &request, BasicImg &outImg, const FrameInInfo &inInfo, MUINT32 sensorID);
    MBOOL prepareExtraMdpCrop(const BasicImg &fullImg, std::vector<SFPOutput> &leftOutList);
    MVOID enqueFeatureStream(QParams &params, P2AEnqueData &data);
    MBOOL needFullForExtraOut(std::vector<SFPOutput> &outList);

    MBOOL needPureYuv(MUINT32 sensorID, const RequestPtr &request);
    MBOOL needNormalYuv(MUINT32 sensorID, const RequestPtr &request);

private:
    MBOOL init3A();
    MVOID uninit3A();
    MBOOL prepare3A(QParams &params, const RequestPtr &request);
    // Tuning
    MBOOL prepareRawTuning(QParams &params, const RequestPtr &request, P2AEnqueData &data, P2ATuningIndex &tuningIndex);
    MINT32 prepareOneRawTuning(        MUINT32           sensorID,
                                const  SFPIOMap          &ioMap,
                                       QParams           &params,
                                const  RequestPtr        &request,
                                       P2AEnqueData      &data,
                                       P2ATuningExtra    &extra);

private:
    struct eis_region
    {
        MUINT32 x_int;
        MUINT32 x_float;
        MUINT32 y_int;
        MUINT32 y_float;
        MSize s;
        MINT32 gmvX;
        MINT32 gmvY;
        MINT32 confX;
        MINT32 confY;
    };

    MBOOL prepare3DNR(QParams &params, const RequestPtr &request, const RSCData &rscData, const P2ATuningIndex &tuningIndex);
    MBOOL prepare3DNR35(QParams &params, const RequestPtr &request, MUINT32 sensorID, NR3DParam &Nr3dParam);
    MBOOL update3DNRMvInfo(const RequestPtr &request, const RSCData &rscData);
    MVOID dump_Qparam(QParams& rParams, const char *pSep);
    MVOID dump_vOutImageBuffer(const QParams & params);
    MVOID dump_imgiImageBuffer(const QParams & params);

    MBOOL do3dnrFlow(
        NSCam::NSIoPipe::QParams &enqueParams,
        const RequestPtr &request,
        const MRect &dst_resizer_rect,
        const MSize &resize_size,
        eis_region &eisInfo,
        MINT32 iso,
        MINT32 isoThreshold,
        MUINT32 requestNo,
        const RSCData &rscData,
        const P2ATuningIndex &tuningIndex
        );

    MINT32 m3dnrLogLevel;

    IHal3A *mp3A = NULL;
    IHalISP *mpISP = NULL;

private:
    // VHDR functions, implemented in P2A_VHDR.cpp
    MBOOL initVHDR();
    MVOID uninitVHDR();
    MBOOL prepareVHDR(QParams &params, const RequestPtr &request);
    // VHDR members
    //VHdrHal *mVHDR;

    // EIS: FE/FM functions, implemented in P2A_FMFE.cpp
    MBOOL initFEFM();
    MVOID uninitFEFM();
    MBOOL prepareCropInfo_FE(QParams &params, const RequestPtr &request, P2AEnqueData &data);
    MBOOL prepareFE(QParams &params, const RequestPtr &request, P2AEnqueData &data);
    MBOOL prepareFM(QParams &params, const RequestPtr &request, P2AEnqueData &data);

private:
    Feature::SecureBufferControl mSecBufCtrl;

private:
    MBOOL prepareFOVFEFM(QParams &params, const RequestPtr &request, P2AEnqueData &data, const P2ATuningIndex &tuningIndex);
    MBOOL prepareCropInfo_FOV(QParams &params, const RequestPtr &request, P2AEnqueData &data, const P2ATuningIndex &tuningIndex);

private:
    WaitQueue<RequestPtr> mRequests;
    WaitQueue<RSCData> mRSCDatas;

    NSCam::NSIoPipe::NSPostProc::INormalStream *mNormalStream;
    MUINT32 mDipVersion = 0;
    android::Mutex mEnqueMutex;
    FMHal::Config mFM_FE_cfg[3];

    MUINT32 mFullImgPoolAllocateNeed;

    std::unordered_map<MUINT32, android::sp<IBufferPool>> mPureImgPoolMap;
    android::sp<IBufferPool> mFullImgPool;
    android::sp<ImageBufferPool> mFE1ImgPool;
    android::sp<ImageBufferPool> mFE2ImgPool;
    android::sp<ImageBufferPool> mFE3ImgPool;
    android::sp<FatImageBufferPool> mFEOutputPool;
    android::sp<FatImageBufferPool> mFEOutputPool_m;
    android::sp<FatImageBufferPool> mFMOutputPool;
    android::sp<FatImageBufferPool> mFMOutputPool_m;
    android::sp<FatImageBufferPool> mFMRegisterPool;
    android::sp<TuningBufferPool> mDynamicTuningPool;

    FEFMGroup mPrevFE;
    NSCam::NSIoPipe::FEInfo mFEInfo[3];
    NSCam::NSIoPipe::FMInfo mFMInfo[6];

    char* mFEFMTuning[9];
    _SRZ_SIZE_INFO_ mpsrz1Param;
    _SRZ_SIZE_INFO_ mpsrz1_2Param;
    _SRZ_SIZE_INFO_ mpsrz1_3Param;


    MUINT32   mCropMode;
    MUINT32   mEisMode;
    MBOOL     mLastDualParamValid;


    typedef enum
    {
        CROP_MODE_NONE = 0,
        CROP_MODE_USE_CRZ
    } CROP_MODE_ENUM;

    android::sp<ImageBufferPool> mFovFEOImgPool;
    android::sp<ImageBufferPool> mFovFMOImgPool;
    android::sp<TuningBufferPool> mFovTuningBufferPool;
    NSCam::NSIoPipe::FEInfo mFovFEInfo;
    NSCam::NSIoPipe::FMInfo mFovFMInfo;
    _SRZ_SIZE_INFO_ mFovSrzInfo[2];
};

} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam

#endif // _MTK_CAMERA_STREAMING_FEATURE_PIPE_P2A_NODE_H_
