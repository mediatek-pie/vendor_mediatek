/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_CAMERA_STREAMING_FEATURE_PIPE_TPI_NODE_H_
#define _MTK_CAMERA_STREAMING_FEATURE_PIPE_TPI_NODE_H_

#include "StreamingFeatureNode.h"

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {

class TPINode : public StreamingFeatureNode
{
public:
    TPINode(const char* name, MUINT32 index, const TPI_IO &nodeIO, TPIMgr *mgr);
    virtual ~TPINode();
    MVOID setInputBufferPool(const android::sp<IBufferPool> &pool, MUINT32 allocate = 0);
    MVOID setOutputBufferPool(const android::sp<IBufferPool> &pool, MUINT32 allocate = 0);

public:
    virtual MBOOL onData(DataID id, const BasicImgData&);
    virtual MBOOL onData(DataID id, const DualBasicImgData&);
    virtual MBOOL onData(DataID id, const DepthImgData&);
    virtual MBOOL onData(DataID id, const TPIData&);
    virtual IOPolicyType getIOPolicy(StreamType stream, const StreamingReqInfo &reqInfo) const;
    virtual MBOOL getInputBufferPool(const StreamingReqInfo &reqInfo, android::sp<IBufferPool> &pool, MSize &resize);

protected:
    virtual MBOOL onInit();
    virtual MBOOL onUninit();
    virtual MBOOL onThreadStart();
    virtual MBOOL onThreadStop();
    virtual MBOOL onThreadLoop();

private:
    MBOOL isLastTPI() const;
    MBOOL checkFirstTPI(const RequestPtr &request, TPIRes &in);
    unsigned findOutID();
    MBOOL process(const RequestPtr &request, const TPIRes &in, TPIRes &out);
    MVOID handleBypass(const RequestPtr &request, const TPIRes &in, TPIRes &out);
    MVOID dumpLog(const RequestPtr &request, const TPIRes &in, const TPIRes &out, TPI_Meta meta[], unsigned metaCount, TPI_Buffer img[], unsigned imgCount);
    MVOID dumpBuffer(const RequestPtr &request, const TPIRes &in, const TPIRes &out);
    MVOID handleResultData(const RequestPtr &request, const TPIRes &out);

private:
    MUINT32 mTPINodeIndex = 0;
    MBOOL mIsFirstTPI = MFALSE;
    MBOOL mIsLastTPI = MFALSE;
    MBOOL mIsInplace = MFALSE;
    MBOOL mWaitDepthData = MFALSE;
    TPI_IO mTPINodeIO;
    WaitQueue<TPIData> mData;
    WaitQueue<DepthImgData> mDepthDatas;
    MUINT32 mInputBufferPoolAllocateNeed = 0;
    MUINT32 mOutputBufferPoolAllocateNeed = 0;
    android::sp<IBufferPool> mInputBufferPool;
    android::sp<IBufferPool> mOutputBufferPool;
    std::map<unsigned, android::sp<IBufferPool>> mBufferPools;

    MBOOL mNeedDump = MFALSE;
    MBOOL mNeedDebug = MFALSE;

    TPIMgr *mTPIMgr = NULL;
};

} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam

#endif // _MTK_CAMERA_STREAMING_FEATURE_PIPE_TPI_NODE_H_
