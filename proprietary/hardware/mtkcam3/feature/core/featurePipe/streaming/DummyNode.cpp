/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "DummyNode.h"

#define PIPE_CLASS_TAG "DummyNode"
#define PIPE_TRACE TRACE_DUMMY_NODE
#include <featurePipe/core/include/PipeLog.h>
#include <mtkcam3/feature/eis/eis_ext.h>

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {

DummyNode::DummyNode(const char *name)
    : StreamingFeatureNode(name)
{
    TRACE_FUNC_ENTER();
    this->addWaitQueue(&mData);
    TRACE_FUNC_EXIT();
}

DummyNode::~DummyNode()
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
}

MVOID DummyNode::setInputBufferPool(const android::sp<IBufferPool> &pool, MUINT32 allocate)
{
    TRACE_FUNC_ENTER();
    mInputBufferPool = pool;
    mInputBufferPoolAllocateNeed = allocate;
    TRACE_FUNC_EXIT();
}

MVOID DummyNode::setOutputBufferPool(const android::sp<IBufferPool> &pool, MUINT32 allocate)
{
    TRACE_FUNC_ENTER();
    mOutputBufferPool = pool;
    mOutputBufferPoolAllocateNeed = allocate;
    TRACE_FUNC_EXIT();
}

MBOOL DummyNode::onData(DataID id, const BasicImgData &data)
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC("Frame %d: %s arrived", data.mRequest->mRequestNo, ID2Name(id));
    if( id == ID_PREV_TO_DUMMY_FULLIMG )
    {
        this->mData.enque(data);
    }
    TRACE_FUNC_EXIT();
    return MTRUE;
}

IOPolicyType DummyNode::getIOPolicy(StreamType /*stream*/, const StreamingReqInfo &reqInfo) const
{
    IOPolicyType policy = IOPOLICY_BYPASS;

    if( HAS_DUMMY(reqInfo.mFeatureMask) && reqInfo.isMaster())
    {
        policy = IOPOLICY_INOUT;
    }

    return policy;
}

MBOOL DummyNode::getInputBufferPool(const StreamingReqInfo &/*reqInfo*/, android::sp<IBufferPool>& pool, MSize &resize)
{
    MBOOL ret = MFALSE;
    resize = MSize(0,0);

    if( mInputBufferPool != NULL )
    {
        pool = mInputBufferPool;
        ret = MTRUE;
    }

    return ret;
}

MBOOL DummyNode::onInit()
{
    TRACE_FUNC_ENTER();
    StreamingFeatureNode::onInit();
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL DummyNode::onUninit()
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL DummyNode::onThreadStart()
{
    TRACE_FUNC_ENTER();
    if( mInputBufferPoolAllocateNeed && mInputBufferPool != NULL )
    {
        Timer timer;
        timer.start();
        mInputBufferPool->allocate(mInputBufferPoolAllocateNeed);
        timer.stop();
        MY_LOGD("fpipe.dummy.in %s %d buf in %d ms", STR_ALLOCATE, mInputBufferPoolAllocateNeed, timer.getElapsed());
    }
    if( mOutputBufferPoolAllocateNeed && mOutputBufferPool != NULL )
    {
        Timer timer;
        timer.start();
        mOutputBufferPool->allocate(mOutputBufferPoolAllocateNeed);
        timer.stop();
        MY_LOGD("fpipe.dummy.out %s %d buf in %d ms", STR_ALLOCATE, mOutputBufferPoolAllocateNeed, timer.getElapsed());
    }
    return MTRUE;
}

MBOOL DummyNode::onThreadStop()
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL DummyNode::onThreadLoop()
{
    TRACE_FUNC("Waitloop");
    RequestPtr request;
    BasicImgData data;

    if( !waitAllQueue() )
    {
        return MFALSE;
    }
    if( !mData.deque(data) )
    {
        MY_LOGE("Data deque out of sync");
        return MFALSE;
    }

    if( data.mRequest == NULL)
    {
        MY_LOGE("Request out of sync");
        return MFALSE;
    }

    TRACE_FUNC_ENTER();
    request = data.mRequest;
    //request->mTimer.startDummy();
    TRACE_FUNC("Frame %d in dummy", request->mRequestNo);
    process(request, data.mData);
    //request->mTimer.stopDummy();
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL DummyNode::process(const RequestPtr &request, const BasicImg &fullImg)
{
    TRACE_FUNC_ENTER();

    BasicImg out;

    MDPWrapper::SFP_OUTPUT_ARRAY outputs;
    if( prepareMDPOut(request, fullImg, outputs, out) )
    {
        if( fullImg.mBuffer != NULL )
        {
            MBOOL result = mMDP.process(fullImg.mBuffer->getImageBufferPtr(), outputs, request->needPrintIO());
            request->updateResult(result);

            if( request->needDump() )
            {
                for( unsigned i = 0; i < outputs.size(); ++i )
                {
                    dumpData(request, outputs[i].mBuffer, "dummy_%d", i);
                }
            }
        }
        else
        {
            MY_LOGW("sensor(%d) Frame %d fullImg is NULL", mSensorIndex, request->mRequestNo);
        }
    }

    handleResultData(request, out);

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL DummyNode::prepareMDPOut(const RequestPtr &request, const BasicImg &fullImg, MDPWrapper::SFP_OUTPUT_ARRAY& outputs, BasicImg &out)
{
    TRACE_FUNC_ENTER();

    SFPOutput sfpOut;
    // display
    if( request->needDisplayOutput(this) &&
        request->getDisplayOutput(sfpOut) &&
        refineCrop(request, fullImg, sfpOut) )
    {
        outputs.push_back(sfpOut);
    }

    // record
    if( request->needRecordOutput(this) &&
        request->getRecordOutput(sfpOut) &&
        refineCrop(request, fullImg, sfpOut) )
    {
        outputs.push_back(sfpOut);
    }

    // Extra data
    std::vector<SFPOutput> sfpOutList;
    if( request->needExtraOutput(this) &&
        request->getExtraOutputs(sfpOutList))
    {
        for(auto&& so : sfpOutList)
        {
            if(refineCrop(request, fullImg, so))
            {
                outputs.push_back(so);
            }
        }
    }

    // next FullImg
    const MUINT32 sID = request->mMasterID;
    if( request->needFullImg(this, sID) || request->needNextFullImg(this, sID) )
    {
        MSize outSize = fullImg.mBuffer->getImageBuffer()->getImgSize();
        if( request->needNextFullImg(this, sID) )
        {
            MSize resize;
            out.mBuffer = request->requestNextFullImg(this, sID, resize);
            if( resize.w && resize.h )
            {
                outSize = resize;
            }
        }
        else
        {
            out.mBuffer = mOutputBufferPool->requestIIBuffer();
        }
        out.mBuffer->getImageBuffer()->setExtParam(outSize);
        out.setDomainInfo(fullImg);

        SFPOutput sfout;
        sfout.mBuffer = out.mBuffer->getImageBufferPtr();
        sfout.mCropDstSize = outSize;
        sfout.mCropRect.s = sfout.mCropDstSize;
        outputs.push_back(sfout);
    }

    TRACE_FUNC_EXIT();
    return !outputs.empty();
}

MBOOL DummyNode::refineCrop(const RequestPtr &/*request*/, const BasicImg &fullImg, SFPOutput &sfpOut)
{
    sfpOut.mCropRect.p.x = max(sfpOut.mCropRect.p.x - fullImg.mDomainOffset.x, 0.0f);
    sfpOut.mCropRect.p.y = max(sfpOut.mCropRect.p.y - fullImg.mDomainOffset.y, 0.0f);
    return MTRUE;
}

MVOID DummyNode::handleResultData(const RequestPtr &request, const BasicImg &fullImg)
{
    if( mPipeUsage.supportVendor() || mPipeUsage.supportWarpNode())
    {
        handleData(ID_DUMMY_TO_NEXT_FULLIMG, BasicImgData(fullImg, request));
    }
    else
    {
        handleData(ID_DUMMY_TO_NEXT_FULLIMG, HelperData(FeaturePipeParam::MSG_FRAME_DONE, request));
    }
}
} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam
