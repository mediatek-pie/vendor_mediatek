/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_CAMERA_FEATURE_PIPE_CORE_IO_UTIL_T_H_
#define _MTK_CAMERA_FEATURE_PIPE_CORE_IO_UTIL_T_H_

#include <set>
#include <map>
#include <vector>
#include <unordered_map>
#include <unordered_set>

#include "MtkHeader.h"
#include "BufferPool.h"

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {

enum IOPolicyType {
  IOPOLICY_BYPASS,
  IOPOLICY_INOUT,
  IOPOLICY_LOOPBACK,
  IOPOLICY_INOUT_EXCLUSIVE,
  IOPOLICY_INOUT_QUEUE,
  IOPOLICY_INPLACE,
  IOPOLICY_DINDOUT,
  IOPOLICY_DINSOUT,
  IOPOLICY_COUNT,
};

enum IOProcessorType {
  IOPROCESSOR_BYPASS,
  IOPROCESSOR_INPLACE,
  IOPROCESSOR_OUTPLACE,
};

enum OutputType {
  OUTPUT_INVALID,
  OUTPUT_FULL,
  OUTPUT_NEXT_FULL,
  OUTPUT_NEXT_EXCLUSIVE_FULL,
  OUTPUT_DUAL_FULL,
  OUTPUT_STREAM_PREVIEW,
  OUTPUT_STREAM_PREVIEW_CALLBACK,
  OUTPUT_STREAM_RECORD,
  OUTPUT_STREAM_PHYSICAL,
  OUTPUT_STREAM_FD, // Must be the last one
};

enum StreamType {
  STREAMTYPE_PREVIEW,
  STREAMTYPE_PREVIEW_CALLBACK,
  STREAMTYPE_RECORD,
  STREAMTYPE_PHYSICAL,
  STREAMTYPE_FD, // Must be the last one
};

template <typename Node_T>
class IONode
{
public:
    typedef typename std::set<Node_T*> NODE_SET;
    typedef typename std::set<Node_T*>::iterator NODE_SET_ITERATOR;
    typedef std::map<OutputType, NODE_SET> OUTPUT_MAP;

    IONode();
    ~IONode();

    Node_T* mNode;
    OUTPUT_MAP mOutputMap;
};

template <typename Node_T>
class IOGraph
{
public:
    IOGraph();
    ~IOGraph();

    Node_T* mRoot;
};

template <typename Node_T, typename ReqInfo_T>
class IOControl
{
public:
  struct BufferInfo
  {
  public:
    BufferInfo();
    BufferInfo(const android::sp<IIBuffer> &buffer, const MSize &resize);
    MVOID pop(android::sp<IIBuffer> &buffer, MSize &resize);
  public:
    android::sp<IIBuffer> mBuffer = NULL;
    MSize mResize = MSize(0,0);
  };

public:
  typedef typename std::set<Node_T*> NODE_SET;
  typedef typename std::set<Node_T*>::iterator NODE_SET_ITERATOR;
  typedef typename std::list<Node_T*> NODE_LIST;
  typedef typename std::list<Node_T*>::iterator NODE_LIST_ITERATOR;
  typedef std::map<StreamType, NODE_LIST> STREAM_MAP;
  typedef typename STREAM_MAP::iterator   STREAM_MAP_ITERATOR;
  typedef std::list<IOPolicyType> NODE_POLICY_LIST;
  typedef std::list<IOPolicyType>::iterator NODE_POLICY_LIST_ITERATOR;
  typedef std::map<OutputType, NODE_SET> OUTPUT_MAP;
  typedef typename OUTPUT_MAP::iterator OUTPUT_MAP_ITERATOR;
  typedef std::map<Node_T*, OUTPUT_MAP> NODE_OUTPUT_MAP;
  typedef typename NODE_OUTPUT_MAP::iterator NODE_OUTPUT_MAP_ITERATOR;
  typedef std::map<Node_T*, BufferInfo> NODE_BUFFER_MAP;
  typedef typename std::set<StreamType> STREAM_SET;
  typedef typename std::set<StreamType>::iterator STREAM_SET_ITERATOR;

  IOControl();
  ~IOControl();

  MBOOL init();
  MBOOL uninit();

  MVOID setRoot(Node_T *root);
  MVOID addStream(StreamType stream, NODE_LIST list);
  MBOOL prepareMap(STREAM_SET streams, const ReqInfo_T &reqInfo, NODE_OUTPUT_MAP &outMap, NODE_BUFFER_MAP &bufMap);

  MVOID printNode(Node_T *node, NODE_OUTPUT_MAP &outMap, std::string depth, std::string edge, bool isLast, std::set<Node_T*> &visited);
  MVOID printMap(NODE_OUTPUT_MAP &outMap);
  MVOID dumpInfo(NODE_OUTPUT_MAP &outMap);
  MVOID dumpInfo(NODE_BUFFER_MAP &bufMap);
  MVOID dumpInfo(const char* name, OUTPUT_MAP &oMap);

private:
  MBOOL prepareStreamMap(StreamType stream, const ReqInfo_T &reqInfo, NODE_LIST &list, NODE_OUTPUT_MAP &outMap);
  MVOID allocNextBuf(const ReqInfo_T &reqInfo, NODE_OUTPUT_MAP &outMap, NODE_BUFFER_MAP &bufMap);
  NODE_POLICY_LIST getStreamPolicy(const ReqInfo_T &reqInfo, StreamType stream);
  MBOOL forwardCheck(NODE_POLICY_LIST policys);
  MVOID backwardCalc(const ReqInfo_T &reqInfo, StreamType stream, NODE_LIST list, NODE_OUTPUT_MAP &outMap);

  class Finder
  {
  public:
    Finder(const ReqInfo_T &reqInfo, StreamType stream);
    bool operator()(const Node_T* node) const;
  private:
    const ReqInfo_T& mReqInfo;
    StreamType mStream;
  };

  Node_T                     *mRoot;
  NODE_SET                    mNodes;
  STREAM_MAP                  mStreams;
};

template <typename Node_T, typename ReqInfo_T>
class IORequest
{
public:
  typedef typename IOControl<Node_T, ReqInfo_T>::NODE_OUTPUT_MAP NODE_OUTPUT_MAP;
  typedef typename IOControl<Node_T, ReqInfo_T>::NODE_BUFFER_MAP NODE_BUFFER_MAP;

  MBOOL needPreview(Node_T *node);
  MBOOL needPreviewCallback(Node_T *node);
  MBOOL needRecord(Node_T *node);
  MBOOL needFD(Node_T *node);
  MBOOL needPhysicalOut(Node_T *node);
  MBOOL needFull(Node_T *node);
  MBOOL needNextFull(Node_T *node);
  MBOOL needDualFull(Node_T *node);
  android::sp<IIBuffer> getNextFullImg(Node_T *node, MSize &resize);

  MBOOL needOutputType(Node_T *node, OutputType type);

  NODE_OUTPUT_MAP mOutMap;
  NODE_BUFFER_MAP mBufMap;
};

} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam

#endif // _MTK_CAMERA_FEATURE_PIPE_CORE_IO_UTIL_T_H_
