/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "P2ANode.h"

#define PIPE_CLASS_TAG "P2ANode"
#define PIPE_TRACE TRACE_P2A_NODE
#include <featurePipe/core/include/PipeLog.h>

#include <isp_tuning/isp_tuning.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>

#include <mtkcam/drv/iopipe/SImager/IImageTransform.h>
#include <mtkcam/drv/iopipe/PostProc/INormalStream.h>
#include <mtkcam/drv/IHalSensor.h>
#include <mtkcam/drv/def/Dip_Notify_datatype.h>
#include <mtkcam/utils/TuningUtils/FileReadRule.h>
#include <mtkcam/utils/exif/DebugExifUtils.h>
//
#include <mtkcam3/feature/lcenr/lcenr.h>
//
#include <cmath>
#include <camera_custom_nvram.h>

#include "../thread/CaptureTaskQueue.h"
#include <MTKDngOp.h>

using namespace NSCam::NSCamFeature::NSFeaturePipe;
using namespace NSCam::NSIoPipe;
using namespace NSCam::TuningUtils;
using namespace NSCam::NSIoPipe::NSSImager;

#define ISP30_RULE01_CROP_OFFSET         (196)
#define ISP30_RULE02_CROP_OFFSET         (128)
#define ISP30_RULE02_RESIZE_RATIO        (8)

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {
namespace NSCapture {

// flag for workround that resize yuv force use the time sharing mode
static MBOOL USEING_TIME_SHARING = MTRUE;

enum DeubgMode {
    AUTO    = -1,
    OFF     = 0,
    ON      = 1,
};

enum P2Task {
    TASK_M1_RRZO,
    TASK_M1_IMGO_PRE,
    TASK_M1_IMGO,
    TASK_M2_RRZO,
    TASK_M2_IMGO,
};

template<typename T>
static inline MBOOL HasFeatureVSDoF(T& t)
{
    const MBOOL hasDepth = t.hasFeature(FID_DEPTH) || t.hasFeature(FID_DEPTH_3RD_PARTY);
    const MBOOL hasBokeh = t.hasFeature(FID_BOKEH) || t.hasFeature(FID_BOKEH_3RD_PARTY);
    return (hasDepth && hasBokeh) || t.hasFeature(FID_PUREBOKEH_3RD_PARTY);
}

MVOID P2ANode::unpack(IImageBuffer* pPkInBuf, IImageBuffer* pUpkOutBuf )
{
    void *pUpkOutVa = (void *) (pUpkOutBuf->getBufVA(0));
    void *pPkInVa    = (void *) (pPkInBuf->getBufVA(0));
    MTKDngOp *MyDngop;
    DngOpResultInfo MyDngopResultInfo;
    DngOpImageInfo MyDngopImgInfo;
    int nImgWidth  = pPkInBuf->getImgSize().w;
    int nImgHeight = pPkInBuf->getImgSize().h;
    int nBufSize   = pPkInBuf->getBufSizeInBytes(0);
    int nImgStride = pPkInBuf->getBufStridesInBytes(0);
    // unpack algorithm
    MY_LOGD("Unpack +");
    MyDngop = MTKDngOp::createInstance(DRV_DNGOP_UNPACK_OBJ_SW);
    MyDngopImgInfo.Width = nImgWidth;
    MyDngopImgInfo.Height = nImgHeight;
    MyDngopImgInfo.Stride_src = nImgStride;
    MyDngopImgInfo.Stride_dst = pUpkOutBuf->getBufStridesInBytes(0);
    MyDngopImgInfo.BIT_NUM = 10;
    MyDngopImgInfo.BIT_NUM_DST = 10;
    MUINT32 buf_size = DNGOP_BUFFER_SIZE(nImgWidth * 2, nImgHeight);
    MyDngopImgInfo.Buff_size = buf_size;
    MyDngopImgInfo.srcAddr = pPkInVa;
    MyDngopResultInfo.ResultAddr = pUpkOutVa;
    MyDngop->DngOpMain((void*)&MyDngopImgInfo, (void*)&MyDngopResultInfo);
    MyDngop->destroyInstance(MyDngop);
    MY_LOGD("Unpack -");
    MY_LOGD("unpack processing. va[in]:%p, va[out]:%p", MyDngopImgInfo.srcAddr, MyDngopResultInfo.ResultAddr);
    MY_LOGD("img size(%dx%d) src stride(%d) bufSize(%d) -> dst stride(%d) bufSize(%zu)", nImgWidth, nImgHeight,
             MyDngopImgInfo.Stride_src,nBufSize, MyDngopImgInfo.Stride_dst , pUpkOutBuf->getBufSizeInBytes(0));
}

P2ANode::P2ANode(NodeID_T nid, const char* name, MINT32 policy, MINT32 priority)
    : CaptureFeatureNode(nid, name, 0, policy, priority)
    , mpIspHal(NULL)
    , mpIspHal2(NULL)
    , mpP2Opt(NULL)
    , mpP2Opt2(NULL)
    , mSensorFmt(-1)
    , mSensorFmt2(-1)
    , mDipVer(0)
    , mSupportDRE(MFALSE)
    , mSupportCZ(MFALSE)
    , mSupportHFG(MFALSE)
    , mSupportDCE(MFALSE)
    , mISP3_0(MFALSE)
    , mISP4_0(MFALSE)
    , mISP5_0(MFALSE)
    , mISP6_0(MFALSE)
    , mDebugDCE(MFALSE)
    , mDebugHFG(MFALSE)
    , mDCES_Size(0, 0)
    , mDCES_Format(0)

{
    TRACE_FUNC_ENTER();
    this->addWaitQueue(&mRequestPacks);

    mDebugPerFrame          = property_get_int32("vendor.debug.camera.p2c.perframe", 0);
    mDebugDS                = property_get_int32("vendor.debug.camera.ds.enable", -1);
    mDebugDSRatio_dividend  = property_get_int32("vendor.debug.camera.ds.ratio_dividend", 8);
    mDebugDSRatio_divisor   = property_get_int32("vendor.debug.camera.ds.ratio_divisor", 16);
    mDebugDRE               = property_get_int32("vendor.debug.camera.dre.enable", -1);
    mDebugCZ                = property_get_int32("vendor.debug.camera.cz.enable", -1);

#ifdef SUPPORT_HFG
    mDebugHFG               = property_get_int32("vendor.debug.camera.hfg.enable", -1);
#endif

#ifdef SUPPORT_DCE
    // TODO: Default off for now
    mDebugDCE               = property_get_int32("vendor.debug.camera.dce.enable", -1);
#endif
    mDebugLoadIn            = (property_get_int32("vendor.debug.camera.dumpin.en", 0) == 2);

    mDebugDump              = property_get_int32("vendor.debug.camera.p2.dump", 0) > 0;
    mDebugUpkRaw            = property_get_int32("vendor.debug.camera.upkraw.dump", 0) > 0;

    mDebugImg2o             = property_get_int32("vendor.debug.camera.img2o.dump", 0) > 0;
    mDebugImg3o             = property_get_int32("vendor.debug.camera.img3o.dump", 0) > 0;
    mDebugImg3oOnly         = (!mDebugDump && mDebugImg3o) ? MTRUE : MFALSE ;
    if (mDebugImg3oOnly) {
        mDebugDump = MTRUE;
    }

#if MTKCAM_TARGET_BUILD_VARIANT != 'u'
    mDebugDumpMDP           = MTRUE;
#else
    mDebugDumpMDP           = property_get_int32("vendor.debug.camera.mdp.dump", 0) > 0;
#endif

    TRACE_FUNC_EXIT();
}

P2ANode::~P2ANode()
{
    TRACE_FUNC_ENTER();

    TRACE_FUNC_EXIT();
}

MVOID P2ANode::setSensorIndex(MINT32 sensorIndex, MINT32 sensorIndex2)
{
    CaptureFeatureNode::setSensorIndex(sensorIndex, sensorIndex2);
    mSensorFmt = (sensorIndex >= 0) ? getSensorRawFmt(mSensorIndex) : SENSOR_RAW_FMT_NONE;
    mSensorFmt2 = (sensorIndex2 >= 0) ? getSensorRawFmt(mSensorIndex2) : SENSOR_RAW_FMT_NONE;
    MY_LOGD("sensorIndex:%d -> sensorFmt:%#09x, sensorIndex2:%d -> sensorFmt2:%#09x",
        mSensorIndex, mSensorFmt, mSensorIndex2, mSensorFmt2);
}

MVOID P2ANode::setBufferPool(const android::sp<CaptureBufferPool> &pool)
{
    TRACE_FUNC_ENTER();
    mpBufferPool = pool;
    TRACE_FUNC_EXIT();
}

MBOOL P2ANode::onInit()
{
    TRACE_FUNC_ENTER();
    CaptureFeatureNode::onInit();

    mpIspHal = MAKE_HalISP(mSensorIndex, "CFP");
    mpP2Opt = new P2Operator(LOG_TAG, mSensorIndex);

    if (mSensorIndex2 >= 0) {
        mpIspHal2 = MAKE_HalISP(mSensorIndex2, "CFP2");
        mpP2Opt2 = new P2Operator(LOG_TAG, mSensorIndex2);
    }

    if (!NSIoPipe::NSPostProc::INormalStream::queryDIPInfo(mDipInfo)) {
        MY_LOGE("queryDIPInfo Error! Please check the error msg above!");
    }

    std::map<DP_ISP_FEATURE_ENUM, bool> mdpFeatures;
    DpIspStream::queryISPFeatureSupport(mdpFeatures);
    mSupportCZ = mdpFeatures[ISP_FEATURE_CLEARZOOM];
    mSupportDRE = mdpFeatures[ISP_FEATURE_DRE];
#ifdef SUPPORT_HFG
    mSupportHFG = mdpFeatures[ISP_FEATURE_HFG];
#endif
    mDipVer = mDipInfo[EDIPINFO_DIPVERSION];
    mISP3_0 = (mDipVer == EDIPHWVersion_30);
    mISP4_0 = (mDipVer == EDIPHWVersion_40);
    mISP5_0 = (mDipVer == EDIPHWVersion_50);
    mISP6_0 = (mDipVer == EDIPHWVersion_60);
#ifdef SUPPORT_DCE
    if (mISP6_0) {
        NS3Av3::Buffer_Info info;
        if (mpIspHal->queryISPBufferInfo(info) && info.DCESO_Param.bSupport)
        {
            mSupportDCE  = MTRUE;
            // mDCES_Size   = info.DCESO_Param.size;
            // TODO: The queried size is not correct. HW accepts 128x1 only
            mDCES_Size   = MSize(128,1);
            mDCES_Format = info.DCESO_Param.format;
        }
    }

    MY_LOGD("Support CZ(%d) DRE(%d) HFG(%d) DCE(%d)",
            mSupportCZ, mSupportDRE, mSupportHFG, mSupportDCE);
#endif
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL P2ANode::onUninit()
{
    TRACE_FUNC_ENTER();

    if (mpIspHal) {
        mpIspHal->destroyInstance("CFP");
        mpIspHal = NULL;
    }

    if (mpIspHal2) {
        mpIspHal2->destroyInstance("CFP2");
        mpIspHal2 = NULL;
    }

    TRACE_FUNC_EXIT();
    return MTRUE;
}

/*******************************************************************************
 *
 ********************************************************************************/
MBOOL
P2ANode::
enqueISP(RequestPtr& pRequest, shared_ptr<P2EnqueData>& pEnqueData)
{
    TRACE_FUNC_ENTER();

    MERROR ret = OK;
    P2EnqueData& enqueData = *pEnqueData.get();

    // Trigger Dump
    enqueData.mDebugDump = mDebugDump;
    enqueData.mDebugDumpMDP = mDebugDumpMDP;
    enqueData.mDebugUpkRaw = mDebugUpkRaw;
    enqueData.mDebugImg3oOnly = mDebugImg3oOnly;
    enqueData.mDebugLoadIn = mDebugLoadIn;
    MINT32& frameNo = enqueData.mFrameNo;
    MINT32& requestNo = enqueData.mRequestNo;

    MBOOL master = enqueData.mSensorId == mSensorIndex;

    sp<CaptureFeatureNodeRequest> pNodeReq = pRequest->getNodeRequest(pEnqueData->mNodeId);

    auto GetBuffer = [&](IImageBuffer*& rpBuf, BufferID_T bufId) -> IImageBuffer* {
        if (rpBuf)
            return rpBuf;
        if (bufId != NULL_BUFFER) {
            rpBuf = pNodeReq->acquireBuffer(bufId);
        }
        return rpBuf;
    };

    EnquePackage* pPackage = NULL;

#define ASSERT(predict, mesg)     \
    do {                          \
        if (!(predict)) {         \
            if (pPackage != NULL) \
                delete pPackage;  \
            MY_LOGE(mesg);        \
            return MFALSE;        \
        }                         \
    } while(0)

    // 1.input & output data
    IImageBuffer* pIMG2O = GetBuffer(enqueData.mIMG2O.mpBuf, enqueData.mIMG2O.mBufId);
    IImageBuffer* pIMG3O = GetBuffer(enqueData.mIMG3O.mpBuf, enqueData.mIMG3O.mBufId);
    IImageBuffer* pDCESO = GetBuffer(enqueData.mDCESO.mpBuf, enqueData.mDCESO.mBufId);
    IImageBuffer* pWROTO = GetBuffer(enqueData.mWROTO.mpBuf, enqueData.mWROTO.mBufId);
    IImageBuffer* pWDMAO = GetBuffer(enqueData.mWDMAO.mpBuf, enqueData.mWDMAO.mBufId);
    IImageBuffer* pCopy1 = GetBuffer(enqueData.mCopy1.mpBuf, enqueData.mCopy1.mBufId);
    IImageBuffer* pCopy2 = GetBuffer(enqueData.mCopy2.mpBuf, enqueData.mCopy2.mBufId);

    ASSERT(!!pIMG2O || !!pIMG3O || !!pWROTO || !!pWDMAO, "do not acquire a output buffer");

    IMetadata* pIMetaDynamic    = enqueData.mpIMetaDynamic;
    IMetadata* pIMetaApp        = enqueData.mpIMetaApp;
    IMetadata* pIMetaHal        = enqueData.mpIMetaHal;
    IMetadata* pOMetaApp        = enqueData.mpOMetaApp;
    IMetadata* pOMetaHal        = enqueData.mpOMetaHal;

    IImageBuffer* pIMGI = GetBuffer(enqueData.mIMGI.mpBuf, enqueData.mIMGI.mBufId);
    IImageBuffer* pLCEI = GetBuffer(enqueData.mLCEI.mpBuf, enqueData.mLCEI.mBufId);
    IImageBuffer* pDCESI = GetBuffer(enqueData.mDCESI.mpBuf, enqueData.mDCESI.mBufId);

    ASSERT(!!pIMGI, "do not acquire a input buffer");

    // 2. Prepare enque package
    SmartTuningBuffer pBufTuning;
    IMetadata metaApp;
    IMetadata metaHal;
    MBOOL bMainFrame = MFALSE;
    MBOOL bSubFrame  = MFALSE;
    if (enqueData.mEnableMFB && enqueData.mTaskId == TASK_M1_IMGO) {
        MUINT8 uTuningMode = 0;
        tryGetMetadata<MUINT8>(pIMetaHal, MTK_ISP_P2_TUNING_UPDATE_MODE, uTuningMode);

        switch (uTuningMode) {
            case 0:
                pBufTuning = mpBufferPool->getTuningBuffer();
                mpKeepTuningData = pBufTuning;

                bMainFrame       = MTRUE;
                break;
            case 2:
                if (mpKeepTuningData == NULL)
                    MY_LOGE("Should have a previous tuning data");
                else {
                    pBufTuning = mpBufferPool->getTuningBuffer();
                    // copy tuning buffer
                    memcpy((void*)pBufTuning->mpVA, (void*)mpKeepTuningData->mpVA, mpBufferPool->getTuningBufferSize());
                    // copy metadata
                    metaApp = mKeepMetaApp;
                    metaHal = mKeepMetaHal;

                    bSubFrame = MTRUE;
                }
                break;
            default:
                mpKeepTuningData = NULL;
                break;
        }
    }

    if (pBufTuning == NULL)
        pBufTuning = mpBufferPool->getTuningBuffer();

    PQParam* pPQParam = new PQParam();
    pPackage = new EnquePackage();
    pPackage->mpEnqueData = pEnqueData;
    pPackage->mpTuningData = pBufTuning;
    pPackage->mpPQParam = pPQParam;
    pPackage->mpNode = this;
    if (mDebugUpkRaw) {
        SmartImageBuffer pUpkWorkingBuf = mpBufferPool->getImageBuffer(enqueData.mIMGI.mpBuf->getImgSize(),
                                                                       eImgFmt_BAYER10_UNPAK);
        pPackage->mUpkWorkingBuf = pUpkWorkingBuf;
    }
    // 3. Crop Calculation & add log
    const MSize& rImgiSize = pIMGI->getImgSize();
    String8 strEnqueLog;
    strEnqueLog += String8::format("Sensor(%d) Resized(%d) R/F Num: %d/%d, EnQ: IMGI Fmt(0x%x) Size(%dx%d) VA/PA(%#" PRIxPTR "/%#" PRIxPTR ")",
               enqueData.mSensorId,
               enqueData.mResized,
               pRequest->getRequestNo(),
               pRequest->getFrameNo(),
               pIMGI->getImgFormat(),
               rImgiSize.w, rImgiSize.h,
               pIMGI->getBufVA(0),
               pIMGI->getBufPA(0));

    sp<CropCalculator::Factor> pFactor;
    if (enqueData.mWDMAO.mHasCrop ||
        enqueData.mWROTO.mHasCrop ||
        enqueData.mIMG2O.mHasCrop ||
        enqueData.mCopy1.mHasCrop ||
        enqueData.mCopy2.mHasCrop)
    {
        pFactor = mpCropCalculator->getFactor(pIMetaApp, pIMetaHal);
        ASSERT(pFactor != NULL, "can not get crop factor!");

        if (pOMetaApp != NULL) {
            MRect cropRegion = pFactor->mActiveCrop;
            if (pFactor->mEnableEis) {
                cropRegion.p.x += pFactor->mActiveEisMv.p.x;
                cropRegion.p.y += pFactor->mActiveEisMv.p.y;
            }
            // Update crop region to output app metadata
            trySetMetadata<MRect>(pOMetaApp, MTK_SCALER_CROP_REGION, cropRegion);
        }
    }

    auto GetCropRegion = [&](const char* sPort, P2Output& rOut, IImageBuffer* pImg) mutable {
        if (pImg == NULL)
            return;

        if (rOut.mHasCrop) {
            // Pass if already have
            if (!rOut.mCropRegion.s) {
                MSize cropSize = pImg->getImgSize();
                if (rOut.mTrans & eTransform_ROT_90)
                    swap(cropSize.w, cropSize.h);

                mpCropCalculator->evaluate(pFactor, cropSize, rOut.mCropRegion, enqueData.mResized);
                if (enqueData.mScaleUp) {
                    simpleTransform tranTG2DS(MPoint(0,0), enqueData.mScaleUpSize, rImgiSize);
                    rOut.mCropRegion = transform(tranTG2DS, rOut.mCropRegion);
                }
            }
        } else
            rOut.mCropRegion = MRect(rImgiSize.w, rImgiSize.h);

        strEnqueLog += String8::format(", %s Rot(%d) Crop(%d,%d)(%dx%d) Size(%dx%d) VA/PA(%#" PRIxPTR "/%#" PRIxPTR ")",
            sPort, rOut.mTrans,
            rOut.mCropRegion.p.x, rOut.mCropRegion.p.y,
            rOut.mCropRegion.s.w, rOut.mCropRegion.s.h,
            pImg->getImgSize().w, pImg->getImgSize().h,
            pImg->getBufVA(0), pImg->getBufPA(0));
    };

    GetCropRegion("WDMAO", enqueData.mWDMAO, pWDMAO);
    GetCropRegion("WROTO", enqueData.mWROTO, pWROTO);
    GetCropRegion("IMG2O", enqueData.mIMG2O, pIMG2O);
    GetCropRegion("IMG3O", enqueData.mIMG3O, pIMG3O);
    GetCropRegion("DCESO", enqueData.mDCESO, pDCESO);
    GetCropRegion("COPY1", enqueData.mCopy1, pCopy1);
    GetCropRegion("COPY2", enqueData.mCopy2, pCopy2);

    MY_LOGD("%s", strEnqueLog.string());

    // 3.1 ISP tuning
    TuningParam tuningParam = {NULL, NULL};
    {
        // For NDD
        trySetMetadata<MINT32>(pIMetaHal, MTK_PIPELINE_FRAME_NUMBER, frameNo);
        trySetMetadata<MINT32>(pIMetaHal, MTK_PIPELINE_REQUEST_NUMBER, requestNo);

        {
            const MBOOL isImgo = !enqueData.mResized;
            const MBOOL isMasterImgo = (master && isImgo);
            const MBOOL isBayerBayer = (mSensorFmt == SENSOR_RAW_Bayer) && (mSensorFmt2 == SENSOR_RAW_Bayer);
            const MBOOL isBayerMono = (mSensorFmt == SENSOR_RAW_Bayer) && (mSensorFmt2 == SENSOR_RAW_MONO);
            if (!isMasterImgo && (enqueData.mYuvRep || enqueData.mScaleUp || enqueData.mEnableMFB)) {
                MY_LOGW("no combined of non-MasterImgo with yuvRep(%d) or scaleUp(%d) or enableMFB(%d)",
                    enqueData.mYuvRep, enqueData.mScaleUp, enqueData.mEnableMFB);
            }

            IspProfileInfo profileInfo = MAKE_ISP_PROFILE_INFO(EIspProfile_Capture);

            // The value should be used
            MINT32 iP2InputFormat = 0;
            // The value stored in HAL metadata. Should update HAL metadata if the two values are no equal
            MINT32 iP2CurrentFormat = 0;

            if (enqueData.mIspProfile >= 0) {
                profileInfo.mValue = enqueData.mIspProfile;
                profileInfo.mName = " ";
            } else if (enqueData.mYuvRep) {
                profileInfo = MAKE_ISP_PROFILE_INFO(EIspProfile_YUV_Reprocess);
                iP2InputFormat = 1;
            } else if (enqueData.mScaleUp) {
#ifdef SUPPORT_DSDN_20
                profileInfo = MAKE_ISP_PROFILE_INFO(EIspProfile_Capture_DSDN);
#else
                profileInfo = MAKE_ISP_PROFILE_INFO(EIspProfile_Capture_MultiPass_HWNR);
#endif
                MINT32 resolution = rImgiSize.w | rImgiSize.h << 16;
                trySetMetadata<MINT32>(pIMetaHal, MTK_ISP_P2_IN_IMG_RES_REVISED, resolution);
                iP2InputFormat = 1;
                MY_LOGD("apply profile(MultiPass_HWNR or DSDN) revised resolution: 0x%x", resolution);
            } else
#ifdef SUPPORT_DCE
            if (enqueData.mEnableDCE) { // DCE ONLY, not DSDN
                profileInfo = MAKE_ISP_PROFILE_INFO(EIspProfile_Capture_DCE);
                iP2InputFormat = 1;
            } else
#endif
            if (enqueData.mEnableMFB) {
                profileInfo = MAKE_ISP_PROFILE_INFO(EIspProfile_MFNR_Before_Blend);
            // dualcam vsdof part
            } else if (enqueData.mEnableVSDoF) {
                if( !isBayerBayer && !isBayerMono ) {
                    MY_LOGW("unknown seneorFmt, sensorIndex:%d -> sensorFmt:%u, sensorIndex2:%d -> sensorFmt2:%u",
                            mSensorIndex, mSensorIndex2, mSensorFmt, mSensorFmt2);
                } else {
                    IspProfileHint hint;
                    hint.mSensorAliasName = master ? eSAN_Master : eSAN_Sub_01;
                    hint.mSensorConfigType = isBayerBayer ? eSCT_BayerBayer : eSCT_BayerMono;
                    hint.mRawImageType = isImgo ? eRIT_Imgo : eRIT_Rrzo;
                    profileInfo = IspProfileManager::get(hint);

                    // MT6739 only support one pass1 so main2 RAW is handled by CamSV.
                    // MW set MTK_ISP_P2_IN_IMG_FMT as 5 in this case
                    if(mISP3_0 && isBayerMono)
                    {
                        tryGetMetadata<MINT32>(pIMetaHal, MTK_ISP_P2_IN_IMG_FMT, iP2InputFormat);
                        MY_LOGD("Bokeh Captuer ISP3.0 P2Format:%d", iP2InputFormat);
                    }
                }
            }

            // 0/Empty: RAW->YUV, 1: YUV->YUV, 5: mono RAW->YUV
            tryGetMetadata<MINT32>(pIMetaHal, MTK_ISP_P2_IN_IMG_FMT, iP2CurrentFormat);
            if (iP2CurrentFormat != iP2InputFormat)
                trySetMetadata<MINT32>(pIMetaHal, MTK_ISP_P2_IN_IMG_FMT, iP2InputFormat);

            trySetMetadata<MUINT8>(pIMetaHal, MTK_3A_ISP_PROFILE, profileInfo.mValue);
            MY_LOGD("set ispProfile, R/F Num: %d/%d, taskId:%d, profile:%d(%s), master:%d, imgo:%d, yuvRep:%d, scaleUp:%d, mfb:%d, vsdof:%d, bb:%d, bm:%d",
                requestNo, frameNo, enqueData.mTaskId,
                profileInfo.mValue, profileInfo.mName,
                master, isImgo,
                enqueData.mYuvRep, enqueData.mScaleUp, enqueData.mEnableMFB, enqueData.mEnableVSDoF,
                isBayerBayer, isBayerMono);
        }
        // Consturct tuning parameter
        {
            tuningParam.pRegBuf = reinterpret_cast<void*>(pBufTuning->mpVA);

            // LCEI
            if (pLCEI != NULL) {
                ASSERT(!enqueData.mResized, "use LCSO for RRZO buffer, should not happended!");
                tuningParam.pLcsBuf = reinterpret_cast<void*>(pLCEI);
            }

            // DCES
            if (pDCESI != NULL) {
                ASSERT(!enqueData.mResized, "use DCESI for RRZO buffer, should not happended!");
                tuningParam.pDcsBuf = reinterpret_cast<void*>(pDCESI);
            }

            // USE resize raw-->set PGN 0
            if (enqueData.mResized) {
                trySetMetadata<MUINT8>(pIMetaHal, MTK_3A_PGN_ENABLE, 0);
            } else {
                trySetMetadata<MUINT8>(pIMetaHal, MTK_3A_PGN_ENABLE, 1);
            }
            MetaSet_T inMetaSet;
            if (bSubFrame) {
                // For MFNR,
                // sub frame uses same metadata and tuning data as main frame,
                // but must restore some metadata of sub frame
                trySetMetadata<MINT32>(&metaHal, MTK_PIPELINE_FRAME_NUMBER, frameNo);
                trySetMetadata<MUINT8>(&metaHal, MTK_ISP_P2_TUNING_UPDATE_MODE, 2);
                trySetMetadata<MINT32>(&metaHal, MTK_HAL_REQUEST_INDEX_BSS, pRequest->getParameter(PID_FRAME_INDEX_FORCE_BSS));
                inMetaSet.appMeta = metaApp;
                inMetaSet.halMeta = metaHal;
            } else {
                inMetaSet.appMeta = *pIMetaApp;
                inMetaSet.halMeta = *pIMetaHal;
            }

            MetaSet_T outMetaSet;
            MBOOL needOutMeta = enqueData.mRound == 1 && (pOMetaApp != NULL || pOMetaHal != NULL);

            {
                MBOOL bLockDCESI = (enqueData.mpLockDCES != NULL && pDCESI != NULL);
                if (bLockDCESI)
                    enqueData.mpLockDCES->lock();

                if (master)
                    ret = mpIspHal->setP2Isp(0, inMetaSet, &tuningParam, needOutMeta ? &outMetaSet : NULL);
                else
                    ret = mpIspHal2->setP2Isp(0, inMetaSet, &tuningParam, needOutMeta ? &outMetaSet : NULL);

                if (bMainFrame) {
                    mKeepMetaApp = *pIMetaApp;
                    mKeepMetaHal = *pIMetaHal;
                }

                if (bLockDCESI)
                    enqueData.mpLockDCES->unlock();
            }
            ASSERT(ret == OK, "fail to set ISP");

            if (pOMetaHal != NULL) {
                (*pOMetaHal) = inMetaSet.halMeta + outMetaSet.halMeta;
#if MTK_ISP_SUPPORT_COLOR_SPACE
                // If flag on, the NVRAM will always prepare tuning data for P3 color space
                trySetMetadata<MINT32>(pOMetaHal, MTK_ISP_COLOR_SPACE, MTK_ISP_COLOR_SPACE_DISPLAY_P3);
#endif
            }
            if (pOMetaApp != NULL)
                (*pOMetaApp) += outMetaSet.appMeta;
        }
    }

    // 3.2 Fill PQ Param
    {
        MINT32 iIsoValue = 0;
        if (!tryGetMetadata<MINT32>(pIMetaDynamic, MTK_SENSOR_SENSITIVITY, iIsoValue))
            MY_LOGW("can not get iso value");

        MINT32 iMagicNum = 0;
        if (!tryGetMetadata<MINT32>(pIMetaHal, MTK_P1NODE_PROCESSOR_MAGICNUM, iMagicNum))
            MY_LOGW("can not get magic number");

        MINT32 iRealLv = 0;
        if (!tryGetMetadata<MINT32>(pIMetaHal, MTK_REAL_LV, iRealLv))
            MY_LOGW("can not get read lv");

        auto fillPQ = [&](DpPqParam& rPqParam, PortID port, MBOOL enableCZ, MBOOL enableHFG) {
            String8 strEnqueLog;
            strEnqueLog += String8::format("Port(%d) Timestamp:%d",(int) port ,enqueData.mUniqueKey);

            rPqParam.enable           = false;
            rPqParam.scenario         = MEDIA_ISP_CAPTURE;
            rPqParam.u.isp.iso        = iIsoValue;
            rPqParam.u.isp.lensId     = enqueData.mSensorId;
            rPqParam.u.isp.LV         = iRealLv;
            rPqParam.u.isp.timestamp  = enqueData.mUniqueKey;
            rPqParam.u.isp.frameNo    = enqueData.mFrameNo;
            rPqParam.u.isp.requestNo  = enqueData.mRequestNo;

            if (mSupportCZ && enableCZ) {
                rPqParam.enable = (PQ_ULTRARES_EN);
                ClearZoomParam& rCzParam = rPqParam.u.isp.clearZoomParam;

                rCzParam.captureShot = CAPTURE_SINGLE;

                // Pass this part if user load
                // Be here only if don't apply DRE.
                if (mDebugDumpMDP) {
                    rPqParam.u.isp.p_mdpSetting = new MDPSetting();
                    rPqParam.u.isp.p_mdpSetting->size = MDPSETTING_MAX_SIZE;
                    rPqParam.u.isp.p_mdpSetting->buffer = ::malloc(MDPSETTING_MAX_SIZE);
                    rPqParam.u.isp.p_mdpSetting->offset = 0;
                    memset(rPqParam.u.isp.p_mdpSetting->buffer, 0xFF, MDPSETTING_MAX_SIZE);
                }

                MUINT32 idx = 0;
                rCzParam.p_customSetting = (void*)getTuningFromNvram(enqueData.mSensorId, idx, iMagicNum, NVRAM_TYPE_CZ, false);
                strEnqueLog += String8::format(" CZ Capture:%d idx:0x%x",
                    rCzParam.captureShot,
                    idx);
            }

            if (mSupportDRE && enqueData.mEnableDRE) {
                rPqParam.enable |= (PQ_DRE_EN);
                rPqParam.scenario = MEDIA_ISP_CAPTURE;

                DpDREParam& rDreParam = rPqParam.u.isp.dpDREParam;
                rDreParam.cmd         = DpDREParam::Cmd::Initialize | DpDREParam::Cmd::Generate;
                rDreParam.userId      = rPqParam.u.isp.frameNo;
                rDreParam.buffer      = nullptr;
                MUINT32 idx = 0;
                rDreParam.p_customSetting = (void*)getTuningFromNvram(enqueData.mSensorId, idx, iMagicNum, NVRAM_TYPE_DRE, false);
                rDreParam.customIndex     = idx;
                strEnqueLog += String8::format(" DRE cmd:0x%x buffer:%p p_customSetting:%p idx:%d",
                                rDreParam.cmd, rDreParam.buffer, rDreParam.p_customSetting, idx);

            }
#ifdef SUPPORT_HFG
            if (mSupportHFG & enableHFG) {
                rPqParam.enable |= (PQ_HFG_EN);
                DpHFGParam& rHfgParam = rPqParam.u.isp.dpHFGParam;

                MUINT32 idx = 0;
                rHfgParam.p_lowerSetting = (void*)getTuningFromNvram(enqueData.mSensorId, idx, iMagicNum, NVRAM_TYPE_HFG, false);
                rHfgParam.p_upperSetting = rHfgParam.p_lowerSetting ;
                rHfgParam.lowerISO = iIsoValue;
                rHfgParam.upperISO = iIsoValue;
                rHfgParam.p_slkParam = tuningParam.pRegBuf;

                strEnqueLog += String8::format(" HFG idx:%d", idx);
            }
#endif
            strEnqueLog += String8::format(" PQ Enable:0x%x scenario:%d iso:%d",
                    rPqParam.enable,
                    rPqParam.scenario,
                    iIsoValue);

            MY_LOGD("%s", strEnqueLog.string());
        };

        // WROT
        if (pWROTO != NULL) {
            DpPqParam* pIspParam_WROT = new DpPqParam();
            fillPQ(*pIspParam_WROT, PORT_WROTO, enqueData.mWROTO.mEnableCZ, enqueData.mWROTO.mEnableHFG);
            pPQParam->WROTPQParam = static_cast<void*>(pIspParam_WROT);
        }

        // WDMA
        if (pWDMAO != NULL) {
            DpPqParam* pIspParam_WDMA = new DpPqParam();
            fillPQ(*pIspParam_WDMA, PORT_WDMAO, enqueData.mWDMAO.mEnableCZ, enqueData.mWDMAO.mEnableHFG);
            pPQParam->WDMAPQParam = static_cast<void*>(pIspParam_WDMA);
        }

    }

    // 3.3 Srz tuning for Imgo (LCE not applied to rrzo)
    if (!enqueData.mScaleUp && !enqueData.mResized) {
        auto fillSRZ4 = [&]() -> ModuleInfo* {
            if (mDipVer < EDIPHWVersion_50) {
                MY_LOGD_IF(0, "isp version(0x%x) < 5.0, dont need SrzInfo", mDipVer);
                return NULL;
            }
            // srz4 config
            // ModuleInfo srz4_module;
            ModuleInfo* p = new ModuleInfo();
            p->moduleTag = EDipModule_SRZ4;
            p->frameGroup=0;

            _SRZ_SIZE_INFO_* pSrzParam = new _SRZ_SIZE_INFO_;
            if (pLCEI) {
                pSrzParam->in_w = pLCEI->getImgSize().w;
                pSrzParam->in_h = pLCEI->getImgSize().h;
                pSrzParam->crop_w = pLCEI->getImgSize().w;
                pSrzParam->crop_h = pLCEI->getImgSize().h;
            }
            if (pIMGI) {
                pSrzParam->out_w = pIMGI->getImgSize().w;
                pSrzParam->out_h = pIMGI->getImgSize().h;
            }

            p->moduleStruct = reinterpret_cast<MVOID*> (pSrzParam);

            return p;
        };
        pPackage->mpModuleSRZ4 = fillSRZ4();
    }
#if 0
    // 3.4 Srz3 tuning
    if (tuningParam.pFaceAlphaBuf) {
        auto fillSRZ3 = [&]() -> ModuleInfo* {
            // srz3 config
            // ModuleInfo srz3_module;
            ModuleInfo* p = new ModuleInfo();
            p->moduleTag = EDipModule_SRZ3;
            p->frameGroup=0;

            _SRZ_SIZE_INFO_* pSrzParam = new _SRZ_SIZE_INFO_;

            FACENR_IN_PARAMS in;
            FACENR_OUT_PARAMS out;
            IImageBuffer *facei = (IImageBuffer*)tuningParam.pFaceAlphaBuf;
            in.p2_in    = rImgiSize;
            in.face_map = facei->getImgSize();
            calculateFACENRConfig(in, out);
            *pSrzParam = out.srz3Param;
            p->moduleStruct = reinterpret_cast<MVOID*>(pSrzParam);
            return p;
        };
        pPackage->mpModuleSRZ3 = fillSRZ3();
    }
#endif
    // 4.create enque param
    NSIoPipe::QParams qParam;

    // 4.1 QParam template
    MINT32 iFrameNum = 0;
    QParamTemplateGenerator qPTempGen = QParamTemplateGenerator(
            iFrameNum, enqueData.mSensorId,
            enqueData.mTimeSharing ? ENormalStreamTag_Vss : ENormalStreamTag_Cap);

    MBOOL bYuvTunning = enqueData.mRound == 2 || enqueData.mYuvRep;

    qPTempGen.addInput(PORT_IMGI);

    if (pPackage->mpModuleSRZ3 != NULL) {
        qPTempGen.addModuleInfo(EDipModule_SRZ3,  pPackage->mpModuleSRZ3->moduleStruct);
        qPTempGen.addInput(PORT_YNR_FACEI);
    }

    if (!bYuvTunning && !enqueData.mResized && tuningParam.pLsc2Buf != NULL) {
        if (mISP6_0) {
            qPTempGen.addInput(PORT_LSCI);
        } else if (mDipVer == EDIPHWVersion_50) {
            qPTempGen.addInput(PORT_IMGCI);
        } else if (mDipVer == EDIPHWVersion_40) {
            qPTempGen.addInput(PORT_DEPI);
        } else {
            MY_LOGE("should not have LSC buffer in ISP 3.0");
        }
    }

    if (!bYuvTunning && !enqueData.mResized && pLCEI != NULL) {
        qPTempGen.addInput(PORT_LCEI);
        if (pPackage->mpModuleSRZ4 != NULL) {
            qPTempGen.addModuleInfo(EDipModule_SRZ4,  pPackage->mpModuleSRZ4->moduleStruct);
            if (mISP6_0)
                qPTempGen.addInput(PORT_YNR_LCEI);
            else
                qPTempGen.addInput(PORT_DEPI);
        }
    }

    if (!bYuvTunning && tuningParam.pBpc2Buf != NULL) {
         if (mISP6_0)
            qPTempGen.addInput(PORT_BPCI);
        else if (mDipVer == EDIPHWVersion_50)
            qPTempGen.addInput(PORT_IMGBI);
        else if (mDipVer == EDIPHWVersion_40)
            qPTempGen.addInput(PORT_DMGI);
    }

    if (pIMG2O != NULL) {
        qPTempGen.addOutput(PORT_IMG2O, 0);
        qPTempGen.addCrop(eCROP_CRZ, enqueData.mIMG2O.mCropRegion.p, enqueData.mIMG2O.mCropRegion.s, pIMG2O->getImgSize());
    }

    if (pIMG3O != NULL) {
        qPTempGen.addOutput(PORT_IMG3O, 0);
    }

    if (pDCESO != NULL) {
        qPTempGen.addOutput(PORT_DCESO, 0);
    }

    if (pWROTO != NULL) {
        qPTempGen.addOutput(PORT_WROTO, enqueData.mWROTO.mTrans);
        qPTempGen.addCrop(eCROP_WROT, enqueData.mWROTO.mCropRegion.p, enqueData.mWROTO.mCropRegion.s, pWROTO->getImgSize());
    }

    if (pWDMAO != NULL) {
        qPTempGen.addOutput(PORT_WDMAO, 0);
        qPTempGen.addCrop(eCROP_WDMA, enqueData.mWDMAO.mCropRegion.p, enqueData.mWDMAO.mCropRegion.s, pWDMAO->getImgSize());
    }

    qPTempGen.addExtraParam(EPIPE_MDP_PQPARAM_CMD, (MVOID*)pPQParam);

    ret = qPTempGen.generate(qParam) ? OK : BAD_VALUE;
    ASSERT(ret == OK, "fail to create QParams");

    // 4.2 QParam filler
    QParamTemplateFiller qParamFiller(qParam);
    qParamFiller.insertInputBuf(iFrameNum, PORT_IMGI, pIMGI);
    qParamFiller.insertTuningBuf(iFrameNum, pBufTuning->mpVA);

    if (pPackage->mpModuleSRZ3 != NULL) {
        qParamFiller.insertInputBuf(iFrameNum, PORT_YNR_FACEI, static_cast<IImageBuffer*>(tuningParam.pFaceAlphaBuf));
    }
    if (!bYuvTunning && !enqueData.mResized && tuningParam.pLsc2Buf != NULL) {
        if (mISP6_0)
            qParamFiller.insertInputBuf(iFrameNum,  PORT_LSCI,  static_cast<IImageBuffer*>(tuningParam.pLsc2Buf));
        else if (mDipVer == EDIPHWVersion_50)
            qParamFiller.insertInputBuf(iFrameNum,  PORT_IMGCI, static_cast<IImageBuffer*>(tuningParam.pLsc2Buf));
        else if (mDipVer == EDIPHWVersion_40)
            qParamFiller.insertInputBuf(iFrameNum,  PORT_DEPI,  static_cast<IImageBuffer*>(tuningParam.pLsc2Buf));
    }

    if (!bYuvTunning && !enqueData.mResized && pLCEI != NULL) {
        qParamFiller.insertInputBuf(iFrameNum, PORT_LCEI, pLCEI);
        if (pPackage->mpModuleSRZ4 != NULL) {
            if (mISP6_0)
                qParamFiller.insertInputBuf(iFrameNum, PORT_YNR_LCEI, pLCEI);
            else
                qParamFiller.insertInputBuf(iFrameNum, PORT_DEPI, pLCEI);
        }
    }

    if (!bYuvTunning && tuningParam.pBpc2Buf != NULL) {
        if (mISP6_0)
            qParamFiller.insertInputBuf(iFrameNum,  PORT_BPCI,  static_cast<IImageBuffer*>(tuningParam.pBpc2Buf));
        else if (mDipVer == EDIPHWVersion_50)
            qParamFiller.insertInputBuf(iFrameNum,  PORT_IMGBI, static_cast<IImageBuffer*>(tuningParam.pBpc2Buf));
        else if (mDipVer == EDIPHWVersion_40)
            qParamFiller.insertInputBuf(iFrameNum,  PORT_DMGI,  static_cast<IImageBuffer*>(tuningParam.pBpc2Buf));
    }

    if (pIMG2O != NULL) {
        qParamFiller.insertOutputBuf(iFrameNum, PORT_IMG2O, pIMG2O);
    }

    if (pIMG3O != NULL) {
        qParamFiller.insertOutputBuf(iFrameNum, PORT_IMG3O, pIMG3O);
    }

    if (pDCESO != NULL) {
        qParamFiller.insertOutputBuf(iFrameNum, PORT_DCESO, pDCESO);
    }

    if (pWROTO != NULL) {
        qParamFiller.insertOutputBuf(iFrameNum, PORT_WROTO, pWROTO);
    }

    if (pWDMAO != NULL) {
        qParamFiller.insertOutputBuf(iFrameNum, PORT_WDMAO, pWDMAO);
    }

    qParamFiller.setInfo(iFrameNum, requestNo, requestNo, enqueData.mTaskId);

    ret = qParamFiller.validate() ? OK : BAD_VALUE;
    ASSERT(ret == OK, "fail to create QParamFiller");

    // 5. prepare rest buffers using mdp copy
    auto IsCovered = [](P2Output& rSrc, MDPOutput& rDst) {
        if (rSrc.mpBuf == NULL || rDst.mpBuf == NULL)
            return MFALSE;

        // Ignore gary image
        MINT srcFmt = rSrc.mpBuf->getImgFormat();
        if (srcFmt == eImgFmt_Y8)
            return MFALSE;

        MSize& srcCrop = rSrc.mCropRegion.s;
        MSize& dstCrop = rDst.mCropRegion.s;
        // Make sure that the source FOV covers the destination FOV
#define FOV_THRES (1)
        if (srcCrop.w * (100 + FOV_THRES) < dstCrop.w * 100 ||
            srcCrop.h * (100 + FOV_THRES) < dstCrop.h * 100)
            return MFALSE;
#undef FOV_THRES

        MSize srcSize = rSrc.mpBuf->getImgSize();
        MSize dstSize = rDst.mpBuf->getImgSize();
        if (rSrc.mTrans & eTransform_ROT_90)
            swap(srcSize.w, srcSize.h);

        if (rDst.mTrans & eTransform_ROT_90)
            swap(dstSize.w, dstSize.h);

        // Make sure that the source image is bigger than destination image
#define SIZE_THRES (5)
        if (srcSize.w * (100 + SIZE_THRES) < dstSize.w * 100 ||
            srcSize.h * (100 + SIZE_THRES) < dstSize.h * 100)
            return MFALSE;
#undef SIZE_THRES

        simpleTransform tranCrop2SrcBuf(rSrc.mCropRegion.p, rSrc.mCropRegion.s, srcSize);
        MRect& rCropRegion = rDst.mSourceCrop;
        rCropRegion = transform(tranCrop2SrcBuf, rDst.mCropRegion);

        // Boundary Check for FOV tolerance
        if (rCropRegion.p.x < 0)
            rCropRegion.p.x = 0;
        if (rCropRegion.p.y < 0)
            rCropRegion.p.y = 0;

        if ((rCropRegion.p.x + rCropRegion.s.w) > srcSize.w)
            rCropRegion.s.w = srcSize.w - rCropRegion.p.x;
        if ((rCropRegion.p.y + rCropRegion.s.h) > srcSize.h)
            rCropRegion.s.h = srcSize.h - rCropRegion.p.y;

        // Make 2 bytes alignment
        rCropRegion.s.w &= ~(0x1);
        rCropRegion.s.h &= ~(0x1);

        if (rSrc.mTrans & eTransform_ROT_90) {
            swap(rCropRegion.p.x, rCropRegion.p.y);
            swap(rCropRegion.s.w, rCropRegion.s.h);
        }

        rDst.mpSource = rSrc.mpBuf;

        MUINT32& rSrcTrans = rSrc.mTrans;
        MUINT32& rDstTrans = rDst.mTrans;
        // Use XOR to calucate the transform, where the source image has a rotation or flip
        rDst.mSourceTrans = rSrcTrans ^ rDstTrans;
        // MDP does image rotation after flip.
        // If the source has a rotation and the destinatio doesn't, do the exceptional rule
        if (rSrcTrans & eTransform_ROT_90 && ~rDstTrans & eTransform_ROT_90) {
            if ((rSrcTrans & eTransform_ROT_180) == (rDstTrans & eTransform_ROT_180))
                rDst.mSourceTrans = eTransform_ROT_90 | eTransform_FLIP_V | eTransform_FLIP_H;
            else if ((rSrcTrans & eTransform_ROT_180) == (~rDstTrans & eTransform_ROT_180))
                rDst.mSourceTrans = eTransform_ROT_90;
        }

        return MTRUE;
    };

    // select a buffer source for MDP copy
    P2Output* pFirstHit = NULL;
    if (pCopy1 != NULL) {
        if (IsCovered(enqueData.mIMG2O, enqueData.mCopy1))
            pFirstHit = &enqueData.mIMG2O;
        else if (IsCovered(enqueData.mWROTO, enqueData.mCopy1))
            pFirstHit = &enqueData.mWROTO;
        else if (IsCovered(enqueData.mWDMAO, enqueData.mCopy1))
            pFirstHit = &enqueData.mWDMAO;
        else
            MY_LOGE("Copy1's FOV is not covered by P2 first round output");
    }

    if (pCopy2 != NULL) {
        if (pFirstHit != NULL && IsCovered(*pFirstHit, enqueData.mCopy2)) {
            MY_LOGD("Use the same output to serve two MDP outputs");
        } else if (pFirstHit != &enqueData.mIMG2O && IsCovered(enqueData.mIMG2O, enqueData.mCopy2)) {
            if (!IsCovered(enqueData.mIMG2O, enqueData.mCopy1))
                MY_LOGD("Use different output to serve two MDP outputs");
        } else if (pFirstHit != &enqueData.mWROTO && IsCovered(enqueData.mWROTO, enqueData.mCopy2)) {
            if (IsCovered(enqueData.mWROTO, enqueData.mCopy1))
                MY_LOGD("Use different output to serve two MDP outputs");
        } else if (pFirstHit != &enqueData.mWDMAO && IsCovered(enqueData.mWDMAO, enqueData.mCopy2)) {
            if (!IsCovered(enqueData.mWDMAO, enqueData.mCopy1))
                MY_LOGD("Use different output to serve two MDP outputs");
        } else {
            MY_LOGE("Copy2's FOV is not covered by P2 first round output");
        }
    }



    // 6.enque
    pPackage->start();

    // callbacks
    qParam.mpfnCallback = onP2SuccessCallback;
    qParam.mpfnEnQFailCallback = onP2FailedCallback;
    qParam.mpCookie = (MVOID*) pPackage;

    CAM_TRACE_ASYNC_BEGIN("P2_Enque", (enqueData.mFrameNo << 3) + enqueData.mTaskId);

    // !!!!
    MBOOL bLockDCESO = (enqueData.mpLockDCES != NULL && pDCESO != NULL);
    if (bLockDCESO)
        enqueData.mpLockDCES->lock();

    // p2 enque
    if (master)
        ret = mpP2Opt->enque(qParam, LOG_TAG);
    else
        ret = mpP2Opt2->enque(qParam, LOG_TAG);

    ASSERT(ret == OK, "fail to enque P2");

    TRACE_FUNC_EXIT();
    return MTRUE;

#undef ASSERT

}

MBOOL P2ANode::onThreadStart()
{
    TRACE_FUNC_ENTER();

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL P2ANode::onThreadStop()
{
    TRACE_FUNC_ENTER();

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL P2ANode::onData(DataID id, const RequestPtr& pRequest)
{
    TRACE_FUNC_ENTER();
    MY_LOGD_IF(mLogLevel, "Frame %d: %s arrived", pRequest->getRequestNo(), PathID2Name(id));
    MBOOL ret = MTRUE;

    const NodeID_T* nodeId = GetPath(id);
    NodeID_T sink = nodeId[1];

    if (sink != NID_P2A && sink != NID_P2B) {
        MY_LOGE("Unexpected sink node: %s", NodeID2Name(sink));
        return MFALSE;
    }

    if (pRequest->isReadyFor(sink)) {
        RequestPack pack = {
            .mNodeId = sink,
            .mpRequest = pRequest
        };
        mRequestPacks.enque(pack);
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL P2ANode::onThreadLoop()
{
    TRACE_FUNC("Waitloop");
    RequestPack pack;

    CAM_TRACE_CALL();

    if (!waitAllQueue()) {
        return MFALSE;
    }

    if (!mRequestPacks.deque(pack)) {
        MY_LOGE("Request deque out of sync");
        return MFALSE;
    } else if (pack.mpRequest == NULL) {
        MY_LOGE("Request out of sync");
        return MFALSE;
    }

    RequestPtr pRequest = pack.mpRequest;

    TRACE_FUNC_ENTER();

    MBOOL ret = onRequestProcess(pack.mNodeId, pRequest);
    if (ret == MFALSE) {
        pRequest->addParameter(PID_FAILURE, 1);
    }

    // Default timing of next capture is P2 start
    if (pRequest->getParameter(PID_ENABLE_NEXT_CAPTURE) > 0)
    {
        if (!pRequest->hasParameter(PID_THUMBNAIL_TIMING) && !pRequest->hasParameter(PID_CSHOT_REQUEST))
        {
            MINT32 frameCount = pRequest->getFrameCount();
            MINT32 frameIndex = pRequest->getFrameIndex();

            if ((pRequest->isSingleFrame() && frameIndex == 0) || frameCount == frameIndex + 1)
            {
                if (pRequest->mpCallback != NULL) {
                    MY_LOGI("Nofity next capture at P2 beginning(%d|%d)", frameIndex, frameCount);
                    pRequest->mpCallback->onContinue(pRequest);
                } else {
                    MY_LOGW("have no request callback instance!");
                }
            }
        }
    }

    TRACE_FUNC_EXIT();
    return MTRUE;
}

/*******************************************************************************
 *
 ********************************************************************************/
MVOID
P2ANode::
onP2SuccessCallback(QParams& rParams)
{
    EnquePackage* pPackage = (EnquePackage*) (rParams.mpCookie);
    P2EnqueData& rData = *pPackage->mpEnqueData.get();
    P2ANode* pNode = pPackage->mpNode;

    CAM_TRACE_ASYNC_END("P2_Enque", (rData.mFrameNo << 3) + rData.mTaskId);
    pPackage->stop();

    MY_LOGI("(%d) R/F Num: %d/%d, task:%d, timeconsuming: %dms",
            rData.mNodeId,
            rData.mRequestNo,
            rData.mFrameNo,
            rData.mTaskId,
            pPackage->getElapsed());
    // !!!!
    MBOOL bLockDCESO = (rData.mpLockDCES != NULL && rData.mDCESO.mpBuf != NULL);
    if (bLockDCESO)
        rData.mpLockDCES->unlock();

    if (rData.mDebugDump) {
        char filename[256] = {0};
        FILE_DUMP_NAMING_HINT hint;
        hint.UniqueKey          = rData.mUniqueKey;
        hint.RequestNo          = rData.mRequestNo;
        hint.FrameNo            = rData.mFrameNo;

        extract_by_SensorOpenId(&hint, rData.mSensorId);

        auto DumpYuvBuffer = [&](IImageBuffer* pImgBuf, YUV_PORT port, const char* pStr, MBOOL bCfg) -> MVOID {
            if (pImgBuf == NULL)
                return;
            {
                extract(&hint, pImgBuf);
                // reset FrameNo, UniqueKey and RequestNo
                if (bCfg) {
#if 0
                    FileReadRule rule;
                    MINT32 index = 0;
                    tryGetMetadata<MINT32>(rData.mpIMetaHal, MTK_HAL_REQUEST_INDEX_BSS, index);
                    rule.on_device_dump_file_rename(index, rData.mEnableMFB ? "MFNR" : "single_capture", &hint, NULL);

                    trySetMetadata<MINT32>(rData.mpIMetaHal, MTK_PIPELINE_DUMP_UNIQUE_KEY,     hint.UniqueKey);
                    trySetMetadata<MINT32>(rData.mpIMetaHal, MTK_PIPELINE_DUMP_REQUEST_NUMBER, hint.RequestNo);
                    trySetMetadata<MINT32>(rData.mpIMetaHal, MTK_PIPELINE_DUMP_FRAME_NUMBER,   hint.FrameNo);
#endif
                }
                if (pStr == NULL) {
                    genFileName_YUV(filename, sizeof(filename), &hint, port);
                } else {
                    genFileName_YUV(filename, sizeof(filename), &hint, port, pStr);
                }
#if 0
                // restore FrameNo, UniqueKey and RequestNo
                if (bCfg) {
                    hint.UniqueKey = rData.mUniqueKey;
                    hint.RequestNo = rData.mRequestNo;
                    hint.FrameNo   = rData.mFrameNo;
                }
#endif
                pImgBuf->saveToFile(filename);
                MY_LOGD("Dump YUV: %s", filename);
            }
        };

        auto DumpLcsBuffer = [&](IImageBuffer* pImgBuf, const char* pStr) -> MVOID {
            if (pImgBuf == NULL)
                return;

            extract(&hint, pImgBuf);
            genFileName_LCSO(filename, sizeof(filename), &hint, pStr);
            pImgBuf->saveToFile(filename);
            MY_LOGD("Dump LCEI: %s", filename);
        };

        auto DumpRawBuffer = [&](IImageBuffer* pImgBuf, RAW_PORT port, const char* pStr) -> MVOID {
            if (pImgBuf == NULL)
                return;

            extract(&hint, pImgBuf);
            genFileName_RAW(filename, sizeof(filename), &hint, port, pStr);
            pImgBuf->saveToFile(filename);
            MY_LOGD("Dump RAW: %s", filename);
        };

        if (rData.mEnableMFB) {
            String8 str;
            if (rData.mIMG3O.mTypeId != TID_MAN_FULL_YUV) {// node's working for dump
                DumpYuvBuffer(rData.mIMG3O.mpBuf, YUV_PORT_IMG3O, NULL, rData.mDebugLoadIn);
            }

            // do NOT show sensor name for MFNR naming
            hint.SensorDev = -1;

            MINT32 iso = 0;
            MINT64 exp = 0;
            tryGetMetadata<MINT32>(rData.mpIMetaDynamic, MTK_SENSOR_SENSITIVITY, iso);
            tryGetMetadata<MINT64>(rData.mpIMetaDynamic, MTK_SENSOR_EXPOSURE_TIME, exp);

            // convert ns into us
            exp /= 1000;

            auto DumpMFNRBuffer = [&](P2Output& o) {

                switch (o.mTypeId) {
                    case TID_MAN_FULL_YUV:
                        str = String8::format("mfll-iso-%d-exp-%" PRId64 "-bfbld-yuv", iso, exp);
                        break;
                    case TID_MAN_SPEC_YUV:
                        str = String8::format("mfll-iso-%d-exp-%" PRId64 "-bfbld-qyuv", iso, exp);
                        break;
                    default:
                        return;
                }
                DumpYuvBuffer(o.mpBuf, YUV_PORT_NULL, str.string(), rData.mDebugLoadIn);
            };

            DumpMFNRBuffer(rData.mIMG3O);
            // use the same property for MFNR img3o only dump
            if (!rData.mDebugImg3oOnly || rData.mWDMAO.mTypeId == TID_MAN_FULL_YUV) {
                DumpMFNRBuffer(rData.mWDMAO);
            }

            // ONLY dump img3o
            if (!rData.mDebugImg3oOnly) {

                DumpYuvBuffer(rData.mIMG2O.mpBuf, YUV_PORT_IMG2O, NULL, rData.mDebugLoadIn);

                DumpMFNRBuffer(rData.mWROTO);

                IImageBuffer* pLCEI = rData.mLCEI.mpBuf;
                if (pLCEI != NULL) {
                    str = String8::format("mfll-iso-%d-exp-%" PRId64 "-bfbld-lcso__%dx%d",
                            iso, exp,
                            pLCEI->getImgSize().w,
                            pLCEI->getImgSize().h);
                    DumpLcsBuffer(pLCEI, str.string());
                }

                str = String8::format("mfll-iso-%d-exp-%" PRId64 "-bfbld-raw", iso, exp);
                if(rData.mDebugUpkRaw){
                   SmartImageBuffer pUpkWorkingBuf = pPackage->mUpkWorkingBuf;
                   unpack(rData.mIMGI.mpBuf,pUpkWorkingBuf->mImageBuffer.get());
                   DumpRawBuffer(pUpkWorkingBuf->mImageBuffer.get(), RAW_PORT_NULL, str.string());
                }else{
                   DumpRawBuffer(rData.mIMGI.mpBuf, RAW_PORT_NULL, str.string());
                }
            }
        } else {

            const char* pStr = NULL;
            if (rData.mRound > 1) {
                pStr = "round2";
            }

            DumpYuvBuffer(rData.mIMG3O.mpBuf, YUV_PORT_IMG3O, pStr, rData.mDebugLoadIn);
            // ONLY dump img3o
            if (!rData.mDebugImg3oOnly) {

                DumpYuvBuffer(rData.mIMG2O.mpBuf, YUV_PORT_IMG2O, pStr, rData.mDebugLoadIn);
                DumpYuvBuffer(rData.mWDMAO.mpBuf, YUV_PORT_WDMAO, pStr, rData.mDebugLoadIn);
                DumpYuvBuffer(rData.mWROTO.mpBuf, YUV_PORT_WROTO, pStr, rData.mDebugLoadIn);

                if (rData.mRound < 2) {
                    DumpLcsBuffer(rData.mLCEI.mpBuf, NULL);
                    if (!rData.mYuvRep) {
                        if(rData.mDebugUpkRaw){
                           SmartImageBuffer pUpkWorkingBuf = pPackage->mUpkWorkingBuf;
                           unpack(rData.mIMGI.mpBuf,pUpkWorkingBuf->mImageBuffer.get());
                           DumpRawBuffer(
                                   pUpkWorkingBuf->mImageBuffer.get(),
                                   rData.mResized ? RAW_PORT_RRZO : RAW_PORT_IMGO,
                                   NULL);
                        }else{
                           DumpRawBuffer(
                                   rData.mIMGI.mpBuf,
                                   rData.mResized ? RAW_PORT_RRZO : RAW_PORT_IMGO,
                                   NULL);
                       }
                    }
                }
            }
        }
    }

    // Update EXIF Metadata
    if (rData.mDebugDumpMDP && rData.mpOMetaHal) {

        auto GetMdpSetting = [] (void* p) -> MDPSetting*
        {
            if (p != nullptr) {
                DpPqParam* pParam = static_cast<DpPqParam*>(p);
                MDPSetting* pSetting = pParam->u.isp.p_mdpSetting;
                if (pSetting != nullptr)
                    return pSetting;
            }
            return NULL;
        };

        auto pSetting = GetMdpSetting(pPackage->mpPQParam->WDMAPQParam);

        if (pSetting == NULL)
            pSetting = GetMdpSetting(pPackage->mpPQParam->WROTPQParam);

        if (pSetting) {
            unsigned char* pBuffer = static_cast<unsigned char*>(pSetting->buffer);
            MUINT32 size = pSetting->size;
            IMetadata exifMeta;
            tryGetMetadata<IMetadata>(rData.mpOMetaHal, MTK_3A_EXIF_METADATA, exifMeta);
            if (DebugExifUtils::setDebugExif(
                    DebugExifUtils::DebugExifType::DEBUG_EXIF_RESERVE3,
                    static_cast<MUINT32>(MTK_RESVC_EXIF_DBGINFO_KEY),
                    static_cast<MUINT32>(MTK_RESVC_EXIF_DBGINFO_DATA),
                    size, pBuffer, &exifMeta) == nullptr)
            {
                MY_LOGW("fail to set debug exif to metadata");
            }
            else
            {
                MY_LOGD("Update Mdp debug info: addr %p, size %u %d %d", pBuffer, size, *pBuffer, *(pBuffer+1));
                trySetMetadata<IMetadata>(rData.mpOMetaHal, MTK_3A_EXIF_METADATA, exifMeta);
            }
        }
    }

    MBOOL hasCopyTask = MFALSE;
    if (rData.mCopy1.mpBuf != NULL || rData.mCopy2.mpBuf != NULL) {
        sp<CaptureTaskQueue> pTaskQueue = pNode->mpTaskQueue;
        if (pTaskQueue != NULL) {

            std::function<void()> func =
                [pPackage]()
                {
                    copyBuffers(pPackage);
                    delete pPackage;
                };

            pTaskQueue->addTask(func);
            hasCopyTask = MTRUE;
        }
    }

    if (!hasCopyTask) {
        // Could early callback only if there is no copy task
        if (rData.mWROTO.mEarlyRelease || rData.mWDMAO.mEarlyRelease) {

            RequestPtr pRequest = rData.mpHolder->mpRequest;
            sp<CaptureFeatureNodeRequest> pNodeReq = pRequest->getNodeRequest(rData.mNodeId);

            if (rData.mWROTO.mBufId != NULL_BUFFER)
                pNodeReq->releaseBuffer(rData.mWROTO.mBufId);
            if (rData.mWDMAO.mBufId != NULL_BUFFER)
                pNodeReq->releaseBuffer(rData.mWDMAO.mBufId);
        }

        delete pPackage;
    }
}

/*******************************************************************************
 *
 ********************************************************************************/
MVOID
P2ANode::
onP2FailedCallback(QParams& rParams)
{
    EnquePackage* pPackage = (EnquePackage*) (rParams.mpCookie);
    P2EnqueData& rData = *pPackage->mpEnqueData.get();
    rData.mpHolder->mStatus = UNKNOWN_ERROR;

    CAM_TRACE_ASYNC_END("P2_Enque", (rData.mFrameNo << 3) + rData.mTaskId);
    pPackage->stop();

    MY_LOGE("R/F Num: %d/%d, task:%d, timeconsuming: %dms",
            rData.mRequestNo,
            rData.mFrameNo,
            rData.mTaskId,
            pPackage->getElapsed());

    delete pPackage;
}

/*******************************************************************************
 *
 ********************************************************************************/
MBOOL
P2ANode::
onRequestProcess(NodeID_T nodeId, RequestPtr& pRequest)
{
    MINT32 requestNo = pRequest->getRequestNo();
    MINT32 frameNo = pRequest->getFrameNo();

    if (mDebugPerFrame) {
        mDebugDS                = property_get_int32("vendor.debug.camera.ds.enable", -1);
        mDebugDSRatio_dividend  = property_get_int32("vendor.debug.camera.ds.ratio_dividend", 8);
        mDebugDSRatio_divisor   = property_get_int32("vendor.debug.camera.ds.ratio_divisor", 16);
        mDebugDRE               = property_get_int32("vendor.debug.camera.dre.enable", -1);
        mDebugCZ                = property_get_int32("vendor.debug.camera.cz.enable", -1);

#ifdef SUPPORT_HFG
        mDebugHFG               = property_get_int32("vendor.debug.camera.hfg.enable", -1);
#endif

#ifdef SUPPORT_DCE
        // TODO: Default off for now
        mDebugDCE               = property_get_int32("vendor.debug.camera.dce.enable", -1);
#endif
    }

#ifdef GTEST
    MY_LOGD("run GTEST, return directly, request:%d", requestNo);
    dispatch(pRequest);
    return MTRUE;
#endif

    CAM_TRACE_FMT_BEGIN("p2a:process|r%df%d", requestNo, frameNo);
    MY_LOGI("(%d) +, R/F Num: %d/%d", nodeId, requestNo, frameNo);

    sp<CaptureFeatureNodeRequest> pNodeReq = pRequest->getNodeRequest(nodeId);
    MBOOL ret;

    // 0. Create request holder
    incExtThreadDependency();
    shared_ptr<RequestHolder> pHolder(new RequestHolder(pRequest),
            [=](RequestHolder* p) mutable
            {
                if (p->mStatus != OK)
                    p->mpRequest->addParameter(PID_FAILURE, 1);

                onRequestFinish(nodeId, p->mpRequest);
                decExtThreadDependency();
                delete p;
            }
    );

    // Make sure that requests are finished in order
    {
        shared_ptr<RequestHolder> pLastHolder = mpLastHolder.lock();
        if (pLastHolder != NULL)
            pLastHolder->mpPrecedeOver = pHolder;

        mpLastHolder = pHolder;
    }

    // check drop frame
    if (pRequest->isCancelled())
    {
        MY_LOGD("Cancel, R/F Num: %d/%d", requestNo, frameNo);
        return MFALSE;
    }

    // 0-1. Acquire Metadata
    IMetadata* pIMetaDynamic    = pNodeReq->acquireMetadata(MID_MAN_IN_P1_DYNAMIC);
    IMetadata* pIMetaApp        = pNodeReq->acquireMetadata(MID_MAN_IN_APP);
    IMetadata* pIMetaHal        = pNodeReq->acquireMetadata(MID_MAN_IN_HAL);
    IMetadata* pOMetaApp        = pNodeReq->acquireMetadata(MID_MAN_OUT_APP);
    IMetadata* pOMetaHal        = pNodeReq->acquireMetadata(MID_MAN_OUT_HAL);

    IMetadata* pIMetaHal2       = NULL;
    IMetadata* pIMetaDynamic2   = NULL;
    if (hasSubSensor()) {
        pIMetaHal2 = pNodeReq->acquireMetadata(MID_SUB_IN_HAL);
        pIMetaDynamic2 = pNodeReq->acquireMetadata(MID_SUB_IN_P1_DYNAMIC);
    }

    // 0-2. Get Data
    MINT32 uniqueKey = 0;
    tryGetMetadata<MINT32>(pIMetaHal, MTK_PIPELINE_UNIQUE_KEY, uniqueKey);

    MINT32 iIsoValue = 0;
    tryGetMetadata<MINT32>(pIMetaDynamic, MTK_SENSOR_SENSITIVITY, iIsoValue);

    // 0-3. Set Index
    if (pRequest->hasParameter(PID_FRAME_INDEX_FORCE_BSS)) {
        trySetMetadata<MINT32>(pIMetaHal, MTK_HAL_REQUEST_INDEX_BSS, pRequest->getParameter(PID_FRAME_INDEX_FORCE_BSS));
    }

    const MBOOL isVsdof = mSensorIndex2 >= 0 && HasFeatureVSDoF(*pRequest);
    if (isVsdof && mpFOVCalculator->getIsEnable()) {
        pRequest->addParameter(PID_IGNORE_CROP, 1);
        MY_LOGD("R/F Num: %d/%d set ignore crop", requestNo, frameNo);
    }

    // 1. Resized RAW of main sensor
    {
        BufferID_T uOResizedYuv = pNodeReq->mapBufferID(TID_MAN_RSZ_YUV, OUTPUT);
        BufferID_T uOPostview = mISP3_0 ? pNodeReq->mapBufferID(TID_POSTVIEW, OUTPUT) : NULL_BUFFER;
        BufferID_T uOThumbnail = mISP3_0 ? pNodeReq->mapBufferID(TID_THUMBNAIL, OUTPUT) : NULL_BUFFER;
        if ((uOResizedYuv & uOPostview & uOThumbnail) != NULL_BUFFER) {
            shared_ptr<P2EnqueData> pEnqueData = make_shared<P2EnqueData>();
            P2EnqueData& rEnqueData = *pEnqueData.get();

            rEnqueData.mpHolder             = pHolder;
            rEnqueData.mIMGI.mBufId         = pNodeReq->mapBufferID(TID_MAN_RSZ_RAW, INPUT);
            rEnqueData.mWDMAO.mBufId        = uOResizedYuv;
            rEnqueData.mWROTO.mBufId        = uOPostview;
            rEnqueData.mWROTO.mHasCrop      = (uOPostview != NULL_BUFFER);
            rEnqueData.mWROTO.mEarlyRelease = (uOPostview != NULL_BUFFER);
            rEnqueData.mIMG2O.mBufId        = uOThumbnail;
            rEnqueData.mIMG2O.mHasCrop      = (uOThumbnail != NULL_BUFFER);
            rEnqueData.mpIMetaApp = pIMetaApp;
            rEnqueData.mpIMetaHal = pIMetaHal;
            rEnqueData.mSensorId  = mSensorIndex;
            rEnqueData.mResized   = MTRUE;
            rEnqueData.mEnableVSDoF = isVsdof;
            rEnqueData.mUniqueKey = uniqueKey;
            rEnqueData.mRequestNo = requestNo;
            rEnqueData.mFrameNo   = frameNo;
            rEnqueData.mTaskId    = TASK_M1_RRZO;
            rEnqueData.mNodeId    = nodeId;
            rEnqueData.mTimeSharing = USEING_TIME_SHARING;
            ret = enqueISP(
                pRequest,
                pEnqueData);

            if (!ret) {
                MY_LOGE("main sensor: resized yuv failed!");
                return MFALSE;
            }
        }
    }

    // 2. Full RAW of main sensor
    // YUV Reprocessing
    MBOOL isYuvRep = pNodeReq->mapBufferID(TID_MAN_FULL_YUV, INPUT) != NULL_BUFFER;
    // Down Scale: Only for IMGO
    MINT32 iDSRatio_dividend = 1;
    MINT32 iDSRatio_divisor  = 1;
    IImageBuffer* pRound1_Buf = NULL;

    Mutex lockDCES;
    IImageBuffer* pDCES_Buf = NULL;
    MBOOL hasDCES = MFALSE;
    if (mISP6_0) {
        // [AINR] DCES from P2B
        BufferID_T uIBufDCE = pNodeReq->mapBufferID(TID_MAN_DCES, INPUT);
        if (uIBufDCE != NULL_BUFFER) {
            hasDCES = MTRUE;
            pDCES_Buf = pNodeReq->acquireBuffer(uIBufDCE);
        }
    }

    MSize fullSize;
    MSize downSize;

    MBOOL isRunDSDN = MFALSE;
    MBOOL isRunDCE = MFALSE;

    if (mISP6_0 && pRequest->hasFeature(FID_AINR)) {
        isRunDCE = MTRUE;
    } else if (!mSupportDCE || !pRequest->isSingleFrame() || isYuvRep || mDebugDCE == OFF) {
        // do NOT execute DCE if multi-frame blending or YUV reprocessing or force off
    } else if (pRequest->hasFeature(FID_DCE) || mDebugDCE == ON) {
        isRunDCE = MTRUE;
    }

    if (mISP6_0 && pRequest->hasFeature(FID_AINR)) {
        isRunDSDN = MFALSE;
    } else if (mISP3_0 || !pRequest->isSingleFrame()|| isYuvRep || isVsdof || mDebugDS == OFF) {
        // do NOT execute down-scale if multi-frame blending or YUV reprocessing or force off
    } else if (mDebugDS == ON) {
        iDSRatio_dividend = mDebugDSRatio_dividend;
        iDSRatio_divisor  = mDebugDSRatio_divisor;
        MY_LOGD("force downscale ratio: (%d/%d)",iDSRatio_dividend, iDSRatio_divisor);
        isRunDSDN  = MTRUE;
    } else {
#if MTK_CAM_NEW_NVRAM_SUPPORT
        MINT32 iMagicNo = 0;
        MUINT32 idx = 1;
        tryGetMetadata<MINT32>(pIMetaHal, MTK_P1NODE_PROCESSOR_MAGICNUM, iMagicNo);
#ifdef SUPPORT_DSDN_20
        FEATURE_NVRAM_DSDN_T* t = (FEATURE_NVRAM_DSDN_T*)
            getTuningFromNvram(mSensorIndex, idx, iMagicNo, NVRAM_TYPE_DSDN, mLogLevel > 0);
        if (t != NULL) {
            iDSRatio_dividend = t->dsdn_dividend;
            iDSRatio_divisor  = t->dsdn_divisor;
            MY_LOGD("downscale ratio: (%d/%d)",iDSRatio_dividend, iDSRatio_divisor);
            if (iDSRatio_divisor < 1) {
                MY_LOGE("Has wrong downscale ratio: (%d,%d)", iDSRatio_dividend, iDSRatio_divisor);
                iDSRatio_divisor  = 1;
                iDSRatio_dividend = 1;
            }
        } else {
            MY_LOGE("fail to query nvram!");
        }

        isRunDSDN = iDSRatio_dividend != iDSRatio_divisor;
#else
        NVRAM_CAMERA_FEATURE_SWNR_THRES_STRUCT* t = (NVRAM_CAMERA_FEATURE_SWNR_THRES_STRUCT*)
            getTuningFromNvram(mSensorIndex, idx, iMagicNo, NVRAM_TYPE_SWNR_THRES, mLogLevel > 0);
        if (t != NULL) {
            iDSRatio_divisor  = t->downscale_ratio;
            isRunDSDN = t->downscale_thres <= iIsoValue;

            MY_LOGD("Decide downscale iso:%d thres:%d ratio: %d",iIsoValue, t->downscale_thres, iDSRatio_divisor);
            if (iDSRatio_divisor < 1) {
                MY_LOGE("Has wrong downscale ratio: %d", iDSRatio_divisor);
                iDSRatio_divisor  = 1;
                iDSRatio_dividend = 1;
            }
        } else {
            MY_LOGE("fail to query nvram!");
        }
#endif
#endif
    }
    // 2-1. Downscale De-noise or DCE
    if (isRunDSDN || (isRunDCE && !hasDCES)) {
        shared_ptr<P2EnqueData> pEnqueData = make_shared<P2EnqueData>();
        P2EnqueData& rEnqueData = *pEnqueData.get();
        rEnqueData.mpHolder  = pHolder;
        rEnqueData.mIMGI.mBufId = pNodeReq->mapBufferID(TID_MAN_FULL_RAW, INPUT);
        rEnqueData.mIMGI.mpBuf  = pNodeReq->acquireBuffer(rEnqueData.mIMGI.mBufId);
        rEnqueData.mLCEI.mBufId = pNodeReq->mapBufferID(TID_MAN_LCS, INPUT);
        rEnqueData.mpIMetaDynamic = pIMetaDynamic;
        rEnqueData.mpIMetaApp = pIMetaApp;
        rEnqueData.mpIMetaHal = pIMetaHal;
        rEnqueData.mpOMetaApp = pOMetaApp;
        rEnqueData.mpOMetaHal = pOMetaHal;

        fullSize = rEnqueData.mIMGI.mpBuf->getImgSize();

        if (isRunDCE) {
#ifdef SUPPORT_AINR
            if (nodeId == NID_P2B && pRequest->hasFeature(FID_AINR)) {
                rEnqueData.mIspProfile = EIspProfile_AINR_Single;
            }
#endif

            SmartImageBuffer pWorkingBuffer = mpBufferPool->getImageBuffer(mDCES_Size, (EImageFormat) mDCES_Format);
            pHolder->mpBuffers.push_back(pWorkingBuffer);
            pDCES_Buf = pWorkingBuffer->mImageBuffer.get();
            rEnqueData.mDCESO.mpBuf = pDCES_Buf;
            rEnqueData.mpLockDCES = &lockDCES;

            if (isRunDSDN) {
                downSize = MSize(((fullSize.w * iDSRatio_dividend)/ iDSRatio_divisor) & ~0x3, ((fullSize.h * iDSRatio_dividend)/ iDSRatio_divisor) & ~0x1);
                MY_LOGD("apply down-scale denoise: (%dx%d) -> (%dx%d)",
                        fullSize.w, fullSize.h, downSize.w, downSize.h);

                // Get working buffer
                SmartImageBuffer pWorkingBuffer = mpBufferPool->getImageBuffer(downSize, eImgFmt_MTK_YUV_P010, MSize(16,0));
                // Push to resource holder
                pHolder->mpBuffers.push_back(pWorkingBuffer);
                pRound1_Buf = pWorkingBuffer->mImageBuffer.get();
                rEnqueData.mWDMAO.mpBuf = pRound1_Buf;
            } else {
                SmartImageBuffer pWorkingBuffer = mpBufferPool->getImageBuffer(fullSize, eImgFmt_MTK_YUV_P010, MSize(16,0));
                // Push to resource holder
                pHolder->mpBuffers.push_back(pWorkingBuffer);
                pRound1_Buf = pWorkingBuffer->mImageBuffer.get();
                rEnqueData.mIMG3O.mpBuf = pRound1_Buf;
            }

        } else {
            // should use MDP to do down-scaling
            if (isRunDSDN) {
#ifdef SUPPORT_DSDN_20
                downSize = MSize(((fullSize.w * iDSRatio_dividend)/ iDSRatio_divisor) & ~0x3, ((fullSize.h * iDSRatio_dividend)/ iDSRatio_divisor) & ~0x1);
                MUINT32 format = eImgFmt_MTK_YUV_P010;
                MUINT32 alignW = 16;
#else
                downSize = MSize(((fullSize.w * iDSRatio_dividend)/ iDSRatio_divisor) & ~0x1, ((fullSize.h * iDSRatio_dividend)/ iDSRatio_divisor) & ~0x1);
                MUINT32 format = eImgFmt_NV12;
                MUINT32 alignW = 4;
#endif
                MY_LOGD("apply down-scale denoise: (%dx%d) -> (%dx%d)",
                        fullSize.w, fullSize.h, downSize.w, downSize.h);
                // Get working buffer
                SmartImageBuffer pWorkingBuffer = mpBufferPool->getImageBuffer(downSize, format, MSize(alignW,0));
                // Push to resource holder
                pHolder->mpBuffers.push_back(pWorkingBuffer);
                pRound1_Buf = pWorkingBuffer->mImageBuffer.get();
                rEnqueData.mWDMAO.mpBuf = pRound1_Buf;
            }
        }

        if (!mISP3_0 && mDebugImg3o && rEnqueData.mIMG3O.mpBuf == NULL) {
            MUINT32 img3oFormat = mISP6_0 ? eImgFmt_MTK_YUYV_Y210 : eImgFmt_YUY2;
            MUINT32 img3oAlign = mISP6_0 ? 16 : 4;
            SmartImageBuffer pDebugBuffer = mpBufferPool->getImageBuffer(fullSize, img3oFormat, MSize(img3oAlign, 0));
            pHolder->mpBuffers.push_back(pDebugBuffer);
            rEnqueData.mIMG3O.mpBuf = pDebugBuffer->mImageBuffer.get();
        }

        if (mDebugLoadIn) {
            FileReadRule rule;
            String8 profileName;
            MINT32  index;
            if (pRequest->hasFeature(FID_MFNR)) { // MFNR
                profileName = String8::format("MFNR");
                index       = pRequest->getParameter(PID_FRAME_INDEX_FORCE_BSS);
            } else {
                profileName = String8::format("single_capture");
                index       = 0;
            }

            rule.getFile_RAW(index, profileName.string(), rEnqueData.mIMGI.mpBuf, "P2Node", mSensorIndex);
            rEnqueData.mLCEI.mpBuf = pNodeReq->acquireBuffer(rEnqueData.mLCEI.mBufId);
            rule.getFile_LCSO(index, profileName.string(), rEnqueData.mLCEI.mpBuf, "P2Node", mSensorIndex);
        }

        rEnqueData.mSensorId    = mSensorIndex;
        rEnqueData.mUniqueKey   = uniqueKey;
        rEnqueData.mRequestNo   = requestNo;
        rEnqueData.mFrameNo     = frameNo;
        rEnqueData.mTaskId      = TASK_M1_IMGO_PRE;
        rEnqueData.mNodeId      = nodeId;
        rEnqueData.mTimeSharing = MTRUE;
        ret = enqueISP(
            pRequest,
            pEnqueData);

        if (!ret) {
            MY_LOGE("main sensor: downsize failed!");
            return MFALSE;
        }
    }

    // 2-2. Upscale or Full-size enque
    {
        shared_ptr<P2EnqueData> pEnqueData = make_shared<P2EnqueData>();
        P2EnqueData& rEnqueData = *pEnqueData.get();
        rEnqueData.mpHolder  = pHolder;
        MBOOL isPureRaw = MFALSE;
        MBOOL isOutputMeta = MFALSE;
        MSize srcSize;
        if (isRunDSDN || isRunDCE) {

            if (pRound1_Buf != NULL) {
                rEnqueData.mIMGI.mpBuf = pRound1_Buf;
                rEnqueData.mRound = 2;
            } else {
                // Enque RAW with LCS
                rEnqueData.mIMGI.mBufId = pNodeReq->mapBufferID(TID_MAN_FULL_RAW, INPUT);
                rEnqueData.mIMGI.mpBuf  = pNodeReq->acquireBuffer(rEnqueData.mIMGI.mBufId);
                rEnqueData.mLCEI.mBufId = pNodeReq->mapBufferID(TID_MAN_LCS, INPUT);
                rEnqueData.mLCEI.mpBuf = pNodeReq->acquireBuffer(rEnqueData.mLCEI.mBufId);
            }

            if (isRunDCE) {
#ifdef SUPPORT_AINR
                if (nodeId == NID_P2A && pRequest->hasFeature(FID_AINR)) {
                    rEnqueData.mIspProfile = EIspProfile_AINR_Main;
                }
#endif

                rEnqueData.mpLockDCES   = &lockDCES;
                rEnqueData.mDCESI.mpBuf = pDCES_Buf;
                rEnqueData.mEnableDCE   = MTRUE;
                srcSize = fullSize;
            }

            if (isRunDSDN) {
                rEnqueData.mScaleUp     = isRunDSDN;
                rEnqueData.mScaleUpSize = fullSize;
                srcSize = downSize;
            }

        } else {
            if (isYuvRep) {
                rEnqueData.mIMGI.mBufId = pNodeReq->mapBufferID(TID_MAN_FULL_YUV, INPUT);
                rEnqueData.mYuvRep = MTRUE;
            } else {
                rEnqueData.mIMGI.mBufId = pNodeReq->mapBufferID(TID_MAN_FULL_RAW, INPUT);
            }
            // Check Raw type
            auto IsPureRaw = [](IImageBuffer *pBuf) -> MBOOL {
                MINT64 rawType;
                if (pBuf->getImgDesc(eIMAGE_DESC_ID_RAW_TYPE, rawType))
                    return rawType == eIMAGE_DESC_RAW_TYPE_PURE;
                return MFALSE;
            };
            rEnqueData.mIMGI.mpBuf     = pNodeReq->acquireBuffer(rEnqueData.mIMGI.mBufId);
            isPureRaw = IsPureRaw(rEnqueData.mIMGI.mpBuf);

            rEnqueData.mIMGI.mPureRaw  =  isPureRaw;
            rEnqueData.mLCEI.mBufId    = pNodeReq->mapBufferID(TID_MAN_LCS, INPUT);

            if (mDebugLoadIn) {
                FileReadRule rule;
                String8 profileName;
                MINT32  index;
                if (pRequest->hasFeature(FID_MFNR)) { // MFNR
                    profileName = String8::format("MFNR");
                    index       = pRequest->getParameter(PID_FRAME_INDEX_FORCE_BSS);
                } else {
                    profileName = String8::format("single_capture");
                    index       = 0;
                }

                rule.getFile_RAW(index, profileName.string(), rEnqueData.mIMGI.mpBuf, "P2Node", mSensorIndex);
                rEnqueData.mLCEI.mpBuf = pNodeReq->acquireBuffer(rEnqueData.mLCEI.mBufId);
                rule.getFile_LCSO(index, profileName.string(), rEnqueData.mLCEI.mpBuf, "P2Node", mSensorIndex);
            }

            srcSize = rEnqueData.mIMGI.mpBuf->getImgSize();
            isOutputMeta = MTRUE;
        }

        // the larger size has higher priority, the smaller size could using larger image to copy via MDP
        const TypeID_T typeIds[] = {
                TID_MAN_FULL_YUV,
                TID_MAN_FULL_PURE_YUV,
                TID_JPEG,
                TID_MAN_CROP1_YUV,
                TID_MAN_CROP2_YUV,
                TID_MAN_SPEC_YUV,
                TID_MAN_FD_YUV,
                TID_POSTVIEW,
                TID_THUMBNAIL,
                TID_MAN_CLEAN
        };

        MBOOL hasP2Resizer = !mISP3_0 || !isPureRaw;
        MBOOL hasCopyBuffer = MFALSE;
        auto UsedOutput = [&](P2Output& o) -> MBOOL {
            return o.mBufId != NULL_BUFFER || o.mpBuf != NULL;
        };

        // ISP 4.0/5.0: MFNR should use IMG3O for bit-true
        MBOOL bFullByImg3o = MFALSE;
        if (mISP4_0 || mISP5_0 || mISP6_0) {
            bFullByImg3o = pRequest->hasFeature(FID_MFNR) && (pRequest->getFrameCount() > 1);
        }

        // ISP 3.0: Calculate cropping & scaling for limitation of different view angle
        sp<CropCalculator::Factor> pFactor = mpCropCalculator->getFactor(pIMetaApp, pIMetaHal);
        for (TypeID_T typeId : typeIds) {
            if (mISP3_0) {
                if (typeId == TID_POSTVIEW || typeId == TID_THUMBNAIL)
                    continue;
            }

            BufferID_T bufId = pNodeReq->mapBufferID(typeId, OUTPUT);
            if (bufId == NULL_BUFFER)
                continue;

            IImageBuffer* pBuf = pNodeReq->acquireBuffer(bufId);
            if (pBuf == NULL) {
                MY_LOGE("should not have null buffer. type:%d, buf:%d",typeId, bufId);
                continue;
            }

            MUINT32 trans   = pNodeReq->getImageTransform(bufId);
            MBOOL needCrop  = typeId == TID_JPEG ||
                              typeId == TID_MAN_CROP1_YUV ||
                              typeId == TID_MAN_CROP2_YUV ||
                              typeId == TID_POSTVIEW ||
                              typeId == TID_THUMBNAIL;

            auto GetScaleCrop = [&](MRect& rCrop, MUINT32& rScaleRatio) mutable {
                if (needCrop) {
                    MSize imgSize = pBuf->getImgSize();
                    if (trans & eTransform_ROT_90)
                        swap(imgSize.w, imgSize.h);

                    mpCropCalculator->evaluate(pFactor, imgSize, rCrop, MFALSE);
                    if (rEnqueData.mScaleUp) {
                        simpleTransform tranTG2DS(MPoint(0,0), rEnqueData.mScaleUpSize, srcSize);
                        rCrop = transform(tranTG2DS, rCrop);
                    }
                    rScaleRatio = imgSize.w * 100 / rCrop.s.w;
                } else {
                    rCrop = MRect(srcSize.w, srcSize.h);
                    rScaleRatio = 1;
                }
            };


            MRect cropRegion;
            MUINT32 scaleRatio = 1;
            // Apply customized crop region to full-size domain
            if (isVsdof && (typeId == TID_MAN_FULL_YUV || typeId == TID_POSTVIEW))
            {
                FovCalculator::FovInfo fovInfo;

                if (rEnqueData.mScaleUp) {
                    MY_LOGE("Can not combine VSDOF with DSDN");
                }
                else if (mpFOVCalculator->getIsEnable() && mpFOVCalculator->getFovInfo(mSensorIndex, fovInfo)) {
                    needCrop = MTRUE;
                    cropRegion.p = fovInfo.mFOVCropRegion.p;
                    cropRegion.s = fovInfo.mFOVCropRegion.s;
                    MY_LOGD("VSDOF Sensor(%d) FOV Crop(%d,%d)(%dx%d)",
                                mSensorIndex,
                                fovInfo.mFOVCropRegion.p.x,
                                fovInfo.mFOVCropRegion.p.y,
                                fovInfo.mFOVCropRegion.s.w,
                                fovInfo.mFOVCropRegion.s.h);
                }

            } else {
                GetScaleCrop(cropRegion, scaleRatio);
            }

            auto SetOutput = [&](P2Output& o) {
                o.mpBuf = pBuf;
                o.mBufId = bufId;
                o.mTypeId = typeId;
                o.mHasCrop = needCrop;
                if (bufId == BID_MAN_OUT_JPEG) {
                    o.mEnableCZ = pRequest->hasFeature(FID_CZ);
                    o.mEnableHFG = pRequest->hasFeature(FID_HFG);
                }
                o.mTrans = trans;
                o.mCropRegion = cropRegion;
                o.mScaleRatio = scaleRatio;
            };

            auto BeyondCapability = [&]() -> MBOOL {
                if (!mISP3_0)
                    return MFALSE;

                MUINT32 maxRatio= scaleRatio;
                MUINT32 minRatio = maxRatio;
                MINT32 maxOffset = cropRegion.p.x;
                MINT32 minOffset = maxOffset;

                MY_LOGD("ratio:%d offset:%d", scaleRatio, cropRegion.p.x);
                auto UpdateStatistics = [&](P2Output &o) {
                    if (o.mpBuf == NULL)
                        return;

                    if (o.mCropRegion.p.x > maxOffset)
                        maxOffset = o.mCropRegion.p.x;
                    if (o.mCropRegion.p.x < minOffset)
                        minOffset = o.mCropRegion.p.x;

                    MUINT32 oRatio = o.mScaleRatio;
                    if (oRatio > maxRatio)
                        maxRatio = oRatio;
                    if (oRatio < minRatio)
                        minRatio = oRatio;
                };

                UpdateStatistics(rEnqueData.mWDMAO);
                UpdateStatistics(rEnqueData.mWROTO);
                UpdateStatistics(rEnqueData.mIMG2O);

                // It's not acceptible if the offset difference is over 196
                if (maxOffset - minOffset > ISP30_RULE01_CROP_OFFSET)
                    return MTRUE;

                // To use HW resizer has the following limitation:
                // - Don't allow that downscale ratio is over 8 and offset is over 128.
                MBOOL hitRatio = maxRatio > minRatio * ISP30_RULE02_RESIZE_RATIO;
                MBOOL hitOffset = maxOffset - minOffset > ISP30_RULE02_CROP_OFFSET;
                if (hitRatio && hitOffset)
                    return MTRUE;

                // - The offset difference multiplied by the ratio differnce must be smaller than 1024
                if (hitRatio || hitOffset) {
                    if ((maxOffset - minOffset) * maxRatio >
                        minRatio * ISP30_RULE02_CROP_OFFSET * ISP30_RULE02_RESIZE_RATIO) {
                        return MTRUE;
                    }
                }

                return MFALSE;
            };

            auto AcceptedByIMG3O = [&]() -> MBOOL {
                const MSize& rDstSize = pBuf->getImgSize();
                const MSize& rSrcSize = rEnqueData.mIMGI.mpBuf->getImgSize();
                return rSrcSize == rDstSize;
            };

            MBOOL limited  = BeyondCapability();
            // Use P2 resizer to serve FD or thumbnail buffer,
            // but do NOT use IMG2O to crop or resize in ISP 3.0 while enque pure raw

            if (!limited && !UsedOutput(rEnqueData.mIMG2O) && (typeId == TID_MAN_FD_YUV || typeId == TID_THUMBNAIL) && hasP2Resizer)
            {
                SetOutput(rEnqueData.mIMG2O);
            } else if (typeId == TID_MAN_FULL_YUV && bFullByImg3o && AcceptedByIMG3O()) {
                SetOutput(rEnqueData.mIMG3O);
            } else if (!limited && !UsedOutput(rEnqueData.mWDMAO) && trans == 0) {
                SetOutput(rEnqueData.mWDMAO);
            } else if (!limited && !UsedOutput(rEnqueData.mWROTO)) {
                SetOutput(rEnqueData.mWROTO);
            } else if (!UsedOutput(rEnqueData.mCopy1)) {
                SetOutput(rEnqueData.mCopy1);
                hasCopyBuffer = MTRUE;
            } else if (!UsedOutput(rEnqueData.mCopy2)) {
                SetOutput(rEnqueData.mCopy2);
                hasCopyBuffer = MTRUE;
            } else {
                MY_LOGE("the buffer is not served, type:%s", TypeID2Name(typeId));
            }
        }

        // Create a smaller intermediate buffer for memory copying
        if (mISP3_0 && hasCopyBuffer) {
            if (!UsedOutput(rEnqueData.mWDMAO) || !UsedOutput(rEnqueData.mWROTO)) {
                P2Output& rUnusedOutput = UsedOutput(rEnqueData.mWDMAO) ? rEnqueData.mWROTO : rEnqueData.mWDMAO;

                MSize tempSize = MSize(srcSize.w / ISP30_RULE02_RESIZE_RATIO,
                                       srcSize.h / ISP30_RULE02_RESIZE_RATIO);
                tempSize.w &= ~(0x1);
                tempSize.h &= ~(0x1);

                MY_LOGW("Create an intermediate buffer(%d,%d), resized ratio:%d",
                    tempSize.w, tempSize.h, ISP30_RULE02_RESIZE_RATIO);

                // Should hit the hw limitation here. Create a temp buffer to avoid full size processing
                SmartImageBuffer pTempBuffer = mpBufferPool->getImageBuffer(
                                    tempSize, eImgFmt_YUY2);

                pHolder->mpBuffers.push_back(pTempBuffer);
                rUnusedOutput.mpBuf = pTempBuffer->mImageBuffer.get();
                rUnusedOutput.mBufId = 200; // Magic No for Temp Buffer
                rUnusedOutput.mHasCrop = MTRUE;
            }
        }

        if ((rEnqueData.mIMG2O.mBufId & rEnqueData.mWROTO.mBufId & rEnqueData.mWDMAO.mBufId) != NULL_BUFFER) {
            rEnqueData.mEnableMFB = pRequest->hasFeature(FID_MFNR) && (pRequest->getFrameCount() > 1);
            rEnqueData.mEnableDRE = !rEnqueData.mEnableMFB && pRequest->hasFeature(FID_DRE) && (pRequest->getFrameIndex() == 0);
            rEnqueData.mpIMetaDynamic = pIMetaDynamic;
            rEnqueData.mpIMetaApp = pIMetaApp;
            rEnqueData.mpIMetaHal = pIMetaHal;
            rEnqueData.mpOMetaApp = (isOutputMeta) ? pOMetaApp : NULL;
            rEnqueData.mpOMetaHal = (isOutputMeta) ? pOMetaHal : NULL;
            rEnqueData.mSensorId  = mSensorIndex;
            rEnqueData.mEnableVSDoF = isVsdof;
            rEnqueData.mUniqueKey = uniqueKey;
            rEnqueData.mRequestNo = requestNo;
            rEnqueData.mFrameNo   = frameNo;
            rEnqueData.mTaskId    = TASK_M1_IMGO;
            rEnqueData.mNodeId    = nodeId;
            rEnqueData.mTimeSharing = MTRUE;

            // Debug: Dump IMG3O only for ISP 4.0+
            if (!mISP3_0 && mDebugImg3o && rEnqueData.mIMG3O.mpBuf == NULL) {
                MUINT32 img3oFormat = mISP6_0 ? eImgFmt_MTK_YUYV_Y210 : eImgFmt_YUY2;
                MUINT32 img3oAlign = mISP6_0 ? 16 : 4;
                SmartImageBuffer pDebugBuffer = mpBufferPool->getImageBuffer(srcSize, img3oFormat, MSize(img3oAlign, 0));
                pHolder->mpBuffers.push_back(pDebugBuffer);
                rEnqueData.mIMG3O.mpBuf = pDebugBuffer->mImageBuffer.get();
            }

            // Debug: Dump IMG2O only for ISP 3.0
            if (mISP3_0 && mDebugImg2o) {
                SmartImageBuffer pDebugBuffer = mpBufferPool->getImageBuffer(srcSize, eImgFmt_YUY2);
                pHolder->mpBuffers.push_back(pDebugBuffer);
                rEnqueData.mIMG2O.mpBuf = pDebugBuffer->mImageBuffer.get();
            }

            ret = enqueISP(
                pRequest,
                pEnqueData);

            if (!ret) {
                MY_LOGE("enqueISP failed!");
                return MFALSE;
            }
        }
    }

    // 3. Full RAW of sub sensor
    if (hasSubSensor()) {
        BufferID_T uOBufId = pNodeReq->mapBufferID(TID_SUB_FULL_YUV, OUTPUT);

        if (uOBufId != NULL_BUFFER) {
            shared_ptr<P2EnqueData> pEnqueData = make_shared<P2EnqueData>();
            P2EnqueData& rEnqueData = *pEnqueData.get();

            rEnqueData.mpHolder  = pHolder;
            rEnqueData.mIMGI.mBufId  = pNodeReq->mapBufferID(TID_SUB_FULL_RAW, INPUT);
            rEnqueData.mLCEI.mBufId  = pNodeReq->mapBufferID(TID_SUB_LCS, INPUT);
            rEnqueData.mWDMAO.mBufId = uOBufId;
            rEnqueData.mpIMetaApp = pIMetaApp;
            rEnqueData.mpIMetaHal = pIMetaHal2;
            rEnqueData.mSensorId  = mSensorIndex2;
            rEnqueData.mEnableVSDoF = isVsdof;
            rEnqueData.mUniqueKey = uniqueKey;
            rEnqueData.mRequestNo = requestNo;
            rEnqueData.mFrameNo   = frameNo;
            rEnqueData.mTaskId    = TASK_M2_IMGO;
            rEnqueData.mNodeId    = nodeId;
            rEnqueData.mTimeSharing = MTRUE;
            if (isVsdof) {
                FovCalculator::FovInfo fovInfo;
                if (mpFOVCalculator->getIsEnable() && mpFOVCalculator->getFovInfo(mSensorIndex2, fovInfo)) {
                    rEnqueData.mWDMAO.mHasCrop = isVsdof;
                    rEnqueData.mWDMAO.mCropRegion.p = fovInfo.mFOVCropRegion.p;
                    rEnqueData.mWDMAO.mCropRegion.s = fovInfo.mFOVCropRegion.s;
                    MY_LOGD("VSDOF Sensor(%d) FOV Crop(%d,%d)(%dx%d)",
                                mSensorIndex2,
                                fovInfo.mFOVCropRegion.p.x,
                                fovInfo.mFOVCropRegion.p.y,
                                fovInfo.mFOVCropRegion.s.w,
                                fovInfo.mFOVCropRegion.s.h);
                }
            }

            ret = enqueISP(
                pRequest,
                pEnqueData);

            if (!ret) {
                MY_LOGE("sub sensor: full yuv failed!");
                return MFALSE;
            }
        }
    }

    // 4. Resized RAW of sub sensor
    if (hasSubSensor()) {
        BufferID_T uOBufId = pNodeReq->mapBufferID(TID_SUB_RSZ_YUV, OUTPUT);

        if (uOBufId != NULL_BUFFER) {
            shared_ptr<P2EnqueData> pEnqueData = make_shared<P2EnqueData>();
            P2EnqueData& rEnqueData = *pEnqueData.get();

            rEnqueData.mpHolder  = pHolder;
            rEnqueData.mIMGI.mBufId  = pNodeReq->mapBufferID(TID_SUB_RSZ_RAW, INPUT);
            rEnqueData.mWDMAO.mBufId = uOBufId;
            rEnqueData.mpIMetaApp = pIMetaApp;
            rEnqueData.mpIMetaHal = pIMetaHal2;
            rEnqueData.mSensorId  = mSensorIndex2;
            rEnqueData.mResized   = MTRUE;
            rEnqueData.mEnableVSDoF = isVsdof;
            rEnqueData.mUniqueKey = uniqueKey;
            rEnqueData.mRequestNo = requestNo;
            rEnqueData.mFrameNo   = frameNo;
            rEnqueData.mTaskId    = TASK_M2_RRZO;
            rEnqueData.mNodeId    = nodeId;
            rEnqueData.mTimeSharing = USEING_TIME_SHARING;
            ret = enqueISP(
                pRequest,
                pEnqueData);

            if (!ret) {
                MY_LOGE("sub sensor: resized yuv failed!");
                return MFALSE;
            }
        }
    }

    MY_LOGI("(%d) -, R/F Num: %d/%d", nodeId, requestNo, frameNo);
    CAM_TRACE_FMT_END();
    return MTRUE;
}


/*******************************************************************************
 *
 ********************************************************************************/
MBOOL P2ANode::copyBuffers(EnquePackage* pPackage)
{
    P2EnqueData& rData = *pPackage->mpEnqueData.get();
    MINT32 requestNo = rData.mRequestNo;
    MINT32 frameNo = rData.mFrameNo;
    NodeID_T nodeId = rData.mNodeId;
    CAM_TRACE_FMT_BEGIN("p2a:copy|r%df%d", requestNo, frameNo);
    MY_LOGD("(%d) +, R/F Num: %d/%d", nodeId, requestNo, frameNo);


    IImageBuffer* pSource1  = rData.mCopy1.mpSource;
    IImageBuffer* pSource2  = rData.mCopy2.mpSource;
    IImageBuffer* pCopy1    = rData.mCopy1.mpBuf;
    IImageBuffer* pCopy2    = rData.mCopy2.mpBuf;
    MUINT32 uTrans1         = rData.mCopy1.mSourceTrans;
    MUINT32 uTrans2         = rData.mCopy2.mSourceTrans;
    MRect& rCrop1           = rData.mCopy1.mSourceCrop;
    MRect& rCrop2           = rData.mCopy2.mSourceCrop;
    MBOOL hasCopy2          = pCopy2 != NULL;
    MBOOL hasSameSrc        = hasCopy2 ? pSource1 == pSource2 : MFALSE;

    if (pSource1 == NULL || pCopy1 == NULL) {
        MY_LOGE("should have source1 & copy1 buffer here. src:%p, dst:%p", pSource1, pCopy1);
        return MFALSE;
    }

    if (hasCopy2 && pSource2 == NULL) {
        MY_LOGE("should have source2 buffer here. src:%p", pSource1);
        return MFALSE;
    }

    String8 strCopyLog;

    auto InputLog = [&](const char* sPort, IImageBuffer* pBuf) mutable {
        strCopyLog += String8::format("Sensor(%d) Resized(%d) R/F Num: %d/%d, Copy: %s Fmt(0x%x) Size(%dx%d) VA/PA(%#" PRIxPTR "/%#" PRIxPTR ")",
            rData.mSensorId,
            rData.mResized,
            requestNo,
            frameNo,
            sPort,
            pBuf->getImgFormat(),
            pBuf->getImgSize().w, pBuf->getImgSize().h,
            pBuf->getBufVA(0),
            pBuf->getBufPA(0));
    };

    auto OutputLog = [&](const char* sPort, MDPOutput& rOut) mutable {
        strCopyLog += String8::format(", %s Rot(%d) Crop(%d,%d)(%dx%d) Size(%dx%d) VA/PA(%#" PRIxPTR "/%#" PRIxPTR ")",
            sPort, rOut.mSourceTrans,
            rOut.mSourceCrop.p.x, rOut.mSourceCrop.p.y,
            rOut.mSourceCrop.s.w, rOut.mSourceCrop.s.h,
            rOut.mpBuf->getImgSize().w, rOut.mpBuf->getImgSize().h,
            rOut.mpBuf->getBufVA(0), rOut.mpBuf->getBufPA(0));
    };

    InputLog("SRC1", pSource1);
    OutputLog("COPY1", rData.mCopy1);

    if (hasCopy2) {
        if (!hasSameSrc) {
            MY_LOGD("%s", strCopyLog.string());
            strCopyLog.clear();
            InputLog("SRC2", pSource2);
        }
        OutputLog("COPY2", rData.mCopy2);
    }
    MY_LOGD("%s", strCopyLog.string());

    // use IImageTransform to handle image cropping
    std::unique_ptr<IImageTransform, std::function<void(IImageTransform*)>> transform(
            IImageTransform::createInstance(),
            [](IImageTransform *p)
            {
                if (p)
                    p->destroyInstance();
            }
        );

    if (transform.get() == NULL) {
        MY_LOGE("IImageTransform is NULL, cannot generate output");
        return MFALSE;
    }

    MBOOL ret = MTRUE;
    ret = transform->execute(
            pSource1,
            pCopy1,
            (hasSameSrc) ? pCopy2 : 0,
            rCrop1,
            (hasSameSrc) ? rCrop2 : 0,
            uTrans1,
            (hasSameSrc) ? uTrans2 : 0,
            3000
        );

    if (!ret) {
        MY_LOGE("fail to do image transform: first round");
        return MFALSE;
    }

    if (hasCopy2 && !hasSameSrc) {
        ret = transform->execute(
                pSource2,
                pCopy2,
                0,
                rCrop2,
                0,
                uTrans2,
                0,
                3000
            );

        if (!ret) {
            MY_LOGE("fail to do image transform: second round");
            return MFALSE;
        }
    }

    CAM_TRACE_FMT_END();
    MY_LOGD("(%d) -, R/F Num: %d/%d", nodeId, requestNo, frameNo);
    return MTRUE;
}

MVOID P2ANode::onRequestFinish(NodeID_T nodeId, RequestPtr& pRequest)
{
    MINT32 requestNo = pRequest->getRequestNo();
    MINT32 frameNo = pRequest->getFrameNo();
    CAM_TRACE_FMT_BEGIN("p2a::finish|r%df%d)", requestNo, frameNo);
    MY_LOGI("(%d), R/F Num: %d/%d", nodeId, requestNo, frameNo);

    const MINT32 frameCount = pRequest->getFrameCount();
    const MINT32 frameIndex = pRequest->getFrameIndex();
    const MBOOL isFirstFrame = (frameIndex == 0);

    if (pRequest->getParameter(PID_ENABLE_NEXT_CAPTURE) > 0)
    {
        if ((pRequest->hasParameter(PID_THUMBNAIL_TIMING) &&
             pRequest->getParameter(PID_THUMBNAIL_TIMING) == NSPipelinePlugin::eTiming_P2)
          || pRequest->hasParameter(PID_CSHOT_REQUEST))
        {
            if (nodeId == NID_P2B && pRequest->hasFeature(FID_AINR))
            {
                if (isFirstFrame)
                {
                    if (pRequest->mpCallback != NULL) {
                        MY_LOGI("[AINR] Nofity next capture at P2 ending(%d|%d)", frameIndex, frameCount);
                        pRequest->mpCallback->onContinue(pRequest);
                    } else {
                        MY_LOGW("[AINR] have no request callback instance!");
                    }
                }
            }
            else if ((pRequest->isSingleFrame() && isFirstFrame) || frameCount == frameIndex + 1)
            {
                if (pRequest->mpCallback != NULL) {
                    MY_LOGI("Nofity next capture at P2 ending(%d|%d)", frameIndex, frameCount);
                    pRequest->mpCallback->onContinue(pRequest);
                } else {
                    MY_LOGW("have no request callback instance!");
                }
            }
        }
    }

    // Keep the delay request while user assigns a delay postview
    if (pRequest->hasParameter(PID_THUMBNAIL_DELAY)) {
        MINT32 delay = pRequest->getParameter(PID_THUMBNAIL_DELAY);
        if (delay != 0) {
            // Assign to delay request, and increate postview refernce count to keep the handle
            if (isFirstFrame) {
                sp<CaptureFeatureNodeRequest> pNodeReq = pRequest->getNodeRequest(nodeId);
                BufferID_T uPostView = pNodeReq->mapBufferID(TID_POSTVIEW, OUTPUT);

                if (uPostView != NULL_BUFFER) {
                    mwpDelayRequest = pRequest;
                    pRequest->incBufferRef(uPostView);
                    MY_LOGD("Delay Release: Postview(%d|%d)", frameIndex, frameCount);
                }
            }

            // Decrease the reference count of delayed buffer
            if (frameIndex == delay || frameCount == frameIndex + 1) {
                RequestPtr r = mwpDelayRequest.promote();
                if (r != NULL) {
                    mwpDelayRequest = NULL;

                    sp<CaptureFeatureNodeRequest> pNodeReq = r->getNodeRequest(nodeId);
                    BufferID_T uPostView = pNodeReq->mapBufferID(TID_POSTVIEW, OUTPUT);

                    if (uPostView != NULL_BUFFER) {
                        r->decBufferRef(uPostView);
                        MY_LOGD("Release: Postview(%d|%d)", frameIndex, frameCount);
                    }
                } else {
                    MY_LOGW("Release: Postview(%d|%d). But request released earlier", frameIndex, frameCount);
                }

            }
        }
    }

    if (isFirstFrame && mpFOVCalculator->getIsEnable()) {
        updateFovCropRegion(pRequest, mSensorIndex, MID_MAN_IN_HAL);
        updateFovCropRegion(pRequest, mSensorIndex2, MID_SUB_IN_HAL);
    }

    pRequest->decNodeReference(nodeId);

    dispatch(pRequest, nodeId);

    CAM_TRACE_FMT_END();
}

MVOID P2ANode::updateFovCropRegion(RequestPtr& pRequest, MINT32 seneorIndex, MetadataID_T metaId)
{
    const MINT32 requestNo = pRequest->getRequestNo();
    const MINT32 frameNo = pRequest->getFrameNo();
    sp<MetadataHandle> metaPtr = pRequest->getMetadata(metaId);
    IMetadata* pMetaPtr = (metaPtr != nullptr) ? metaPtr->native() : nullptr;
    if (pMetaPtr == nullptr)
    {
        MY_LOGW("failed to get meta, R/F Num: %d/%d, metaId:%d", requestNo, frameNo, metaId);
        return;
    }

    FovCalculator::FovInfo fovInfo;
    if (!mpFOVCalculator->getFovInfo(seneorIndex, fovInfo))
    {
        MY_LOGW("failed to get fovInfo, R/F Num: %d/%d, sensorIndex:%d", requestNo, frameNo, seneorIndex);
        return;
    }

    IMetadata::IEntry fovCropRegionEntry(MTK_STEREO_FEATURE_FOV_CROP_REGION);
    fovCropRegionEntry.push_back(fovInfo.mFOVCropRegion.p.x, Type2Type<MINT32>());
    fovCropRegionEntry.push_back(fovInfo.mFOVCropRegion.p.y, Type2Type<MINT32>());
    fovCropRegionEntry.push_back(fovInfo.mFOVCropRegion.s.w, Type2Type<MINT32>());
    fovCropRegionEntry.push_back(fovInfo.mFOVCropRegion.s.h, Type2Type<MINT32>());
    pMetaPtr->update(fovCropRegionEntry.tag(), fovCropRegionEntry);
    MY_LOGD("update fov crop region, R/F Num: %d/%d, sensorIndex:%d, metaId:%d, region:(%d, %d)x(%d, %d)",
        requestNo, frameNo,
        seneorIndex, metaId,
        fovInfo.mFOVCropRegion.p.x, fovInfo.mFOVCropRegion.p.y,
        fovInfo.mFOVCropRegion.s.w, fovInfo.mFOVCropRegion.s.h);
}

P2ANode::EnquePackage::~EnquePackage()
{
    if (mpPQParam != nullptr)
    {
        auto DeletePqParam = [] (void* p)
        {
            if (p != nullptr) {
                DpPqParam* pParam = static_cast<DpPqParam*>(p);
                MDPSetting* pSetting = pParam->u.isp.p_mdpSetting;
                if (pSetting != nullptr) {
                    if (pSetting->buffer != nullptr)
                        free(pSetting->buffer);
                    delete pSetting;
                }
                delete pParam;
            }
        };

        DeletePqParam(mpPQParam->WDMAPQParam);
        DeletePqParam(mpPQParam->WROTPQParam);
        delete mpPQParam;
    }

    auto DeleteModuleInfo = [] (ModuleInfo* pModuleInfo) {
        if (pModuleInfo != nullptr) {
            if (pModuleInfo->moduleStruct != nullptr)
                delete static_cast<_SRZ_SIZE_INFO_*>(pModuleInfo->moduleStruct);
            delete pModuleInfo;
        }
    };

    DeleteModuleInfo(mpModuleSRZ4);
    DeleteModuleInfo(mpModuleSRZ3);
}

MERROR P2ANode::evaluate(NodeID_T nodeId, CaptureFeatureInferenceData& rInfer)
{
    auto& rSrcData = rInfer.getSharedSrcData();
    auto& rDstData = rInfer.getSharedDstData();
    auto& rFeatures = rInfer.getSharedFeatures();
    auto& rMetadatas = rInfer.getSharedMetadatas();

    // Debug
    if (mDebugDRE == OFF || !mSupportDRE)
        rInfer.clearFeature(FID_DRE);
    else if (mDebugDRE == ON)
        rInfer.markFeature(FID_DRE);

    if (mDebugCZ == OFF || !mSupportCZ)
        rInfer.clearFeature(FID_CZ);
    else if (mDebugCZ == ON)
        rInfer.markFeature(FID_CZ);

    if (mDebugHFG == OFF || !mSupportHFG)
        rInfer.clearFeature(FID_HFG);
    else if (mDebugHFG == ON)
        rInfer.markFeature(FID_HFG);

    if (mDebugDCE == OFF || !mSupportDCE)
        rInfer.clearFeature(FID_DCE);
    else if (mDebugDCE == ON)
        rInfer.markFeature(FID_DCE);

    MBOOL isP2A = nodeId == NID_P2A;
    MBOOL hasMain = MFALSE;
    MBOOL hasSub = MFALSE;

    // Reprocessing
    if (isP2A && rInfer.hasType(TID_MAN_FULL_YUV))
    {
        auto& src_0 = rSrcData.editItemAt(rSrcData.add());
        src_0.mTypeId = TID_MAN_FULL_YUV;
        src_0.mSizeId = SID_FULL;

        auto& dst_0 = rDstData.editItemAt(rDstData.add());
        dst_0.mTypeId = TID_MAN_FULL_YUV;
        dst_0.mSizeId = SID_FULL;
        dst_0.mSize = rInfer.getSize(TID_MAN_FULL_YUV);
        dst_0.mFormat = eImgFmt_YV12;

        auto& dst_1 = rDstData.editItemAt(rDstData.add());
        dst_1.mTypeId = TID_MAN_CROP1_YUV;

        auto& dst_2 = rDstData.editItemAt(rDstData.add());
        dst_2.mTypeId = TID_MAN_CROP2_YUV;

        auto& dst_3 = rDstData.editItemAt(rDstData.add());
        dst_3.mTypeId = TID_JPEG;

        auto& dst_4 = rDstData.editItemAt(rDstData.add());
        dst_4.mTypeId = TID_THUMBNAIL;

        auto& dst_5 = rDstData.editItemAt(rDstData.add());
        dst_5.mTypeId = TID_MAN_FD_YUV;

        auto& dst_6 = rDstData.editItemAt(rDstData.add());
        dst_6.mTypeId = TID_POSTVIEW;

        hasMain = MTRUE;
    }
    else if (isP2A && rInfer.hasType(TID_MAN_FULL_RAW))
    {
        auto& src_0 = rSrcData.editItemAt(rSrcData.add());
        src_0.mTypeId = TID_MAN_FULL_RAW;
        src_0.mSizeId = SID_FULL;

        auto& src_1 = rSrcData.editItemAt(rSrcData.add());
        src_1.mTypeId = TID_MAN_LCS;
        src_1.mSizeId = SID_ARBITRARY;

        auto& dst_0 = rDstData.editItemAt(rDstData.add());
        dst_0.mTypeId = TID_MAN_FULL_YUV;
        dst_0.mSizeId = SID_FULL;

        // Use 10bit as default foramt for ISP 6.0
#if 0
        if (mISP6_0 && rInfer.hasFeature(FID_DRE))
            dst_0.mFormat = eImgFmt_MTK_YUV_P010;
        else
#endif
            dst_0.mFormat = eImgFmt_YV12;

        if (HasFeatureVSDoF(rInfer) && mpFOVCalculator->getIsEnable())
        {
            FovCalculator::FovInfo fovInfo;
            if (mpFOVCalculator->getFovInfo(mSensorIndex, fovInfo)) {
                dst_0.mSize = fovInfo.mDestinationSize;
            } else {
                dst_0.mSize = rInfer.getSize(TID_MAN_FULL_RAW);
            }
        } else {
            dst_0.mSize = rInfer.getSize(TID_MAN_FULL_RAW);
        }

        // should only output working buffer if run DRE
        if (!(mSupportDRE && rInfer.hasFeature(FID_DRE))) {
            auto& dst_1 = rDstData.editItemAt(rDstData.add());
            dst_1.mTypeId = TID_MAN_CROP1_YUV;

            auto& dst_2 = rDstData.editItemAt(rDstData.add());
            dst_2.mTypeId = TID_MAN_CROP2_YUV;
        }

        auto& dst_3 = rDstData.editItemAt(rDstData.add());
        dst_3.mTypeId = TID_MAN_SPEC_YUV;

        auto& dst_4 = rDstData.editItemAt(rDstData.add());
        dst_4.mTypeId = TID_JPEG;

        auto& dst_5 = rDstData.editItemAt(rDstData.add());
        dst_5.mTypeId = TID_THUMBNAIL;

        auto& dst_6 = rDstData.editItemAt(rDstData.add());
        dst_6.mTypeId = TID_MAN_FD_YUV;

        if (rInfer.hasFeature(FID_AINR)) {
            if (rInfer.hasFeature(FID_DCE)) {
                auto& src_2 = rSrcData.editItemAt(rSrcData.add());
                src_2.mTypeId = TID_MAN_DCES;
                src_2.mSizeId = SID_SPECIFIED;
            }
        } else {
            auto& dst_7 = rDstData.editItemAt(rDstData.add());
            dst_7.mTypeId = TID_POSTVIEW;
        }

        if (mSensorIndex2 >= 0) {
            auto& dst_8 = rDstData.editItemAt(rDstData.add());
            dst_8.mTypeId = TID_MAN_CLEAN;
        }

        hasMain = MTRUE;
    }
    else if (!isP2A && rInfer.hasType(TID_MAN_FULL_RAW))
    {
        auto& src_0 = rSrcData.editItemAt(rSrcData.add());
        src_0.mTypeId = TID_MAN_FULL_RAW;
        src_0.mSizeId = SID_FULL;

        auto& src_1 = rSrcData.editItemAt(rSrcData.add());
        src_1.mTypeId = TID_MAN_LCS;
        src_1.mSizeId = SID_ARBITRARY;

        auto& dst_0 = rDstData.editItemAt(rDstData.add());
        dst_0.mTypeId = TID_MAN_FULL_PURE_YUV;
        dst_0.mSizeId = SID_FULL;
        dst_0.mSize = rInfer.getSize(TID_MAN_FULL_RAW);
        dst_0.mFormat = eImgFmt_YV12;

        if (rInfer.hasFeature(FID_AINR)) {
            auto& dst_7 = rDstData.editItemAt(rDstData.add());
            dst_7.mTypeId = TID_POSTVIEW;

            if (rInfer.hasFeature(FID_DCE)) {
                auto& dst_8 = rDstData.editItemAt(rDstData.add());
                dst_8.mTypeId = TID_MAN_DCES;
                dst_8.mSizeId = SID_SPECIFIED;
                dst_8.mSize = mDCES_Size;
                dst_8.mFormat = mDCES_Format;
            }
        }

        hasMain = MTRUE;
    }

    if (isP2A && rInfer.hasType(TID_SUB_FULL_RAW))
    {
        auto& src_0 = rSrcData.editItemAt(rSrcData.add());
        src_0.mTypeId = TID_SUB_FULL_RAW;
        src_0.mSizeId = SID_FULL;

        auto& src_1 = rSrcData.editItemAt(rSrcData.add());
        src_1.mTypeId = TID_SUB_LCS;
        src_1.mSizeId = SID_ARBITRARY;

        auto& dst_0 = rDstData.editItemAt(rDstData.add());
        dst_0.mTypeId = TID_SUB_FULL_YUV;
        dst_0.mSizeId = SID_FULL;
        dst_0.mFormat = eImgFmt_YV12;

        if (HasFeatureVSDoF(rInfer) && mpFOVCalculator->getIsEnable())
        {
            FovCalculator::FovInfo fovInfo;
            if(mpFOVCalculator->getFovInfo(mSensorIndex2, fovInfo)) {
                dst_0.mSize = fovInfo.mDestinationSize;
            } else {
                dst_0.mSize = rInfer.getSize(TID_SUB_FULL_RAW);
            }
        } else {
            dst_0.mSize = rInfer.getSize(TID_SUB_FULL_RAW);
        }

        hasSub = MTRUE;
    }

    if (isP2A && rInfer.hasType(TID_MAN_RSZ_RAW))
    {
        auto& src_0 = rSrcData.editItemAt(rSrcData.add());
        src_0.mTypeId = TID_MAN_RSZ_RAW;
        src_0.mSizeId = SID_RESIZED;

        auto& dst_0 = rDstData.editItemAt(rDstData.add());
        dst_0.mTypeId = TID_MAN_RSZ_YUV;
        dst_0.mSizeId = SID_RESIZED;
        dst_0.mSize = rInfer.getSize(TID_MAN_RSZ_RAW);
        dst_0.mFormat = eImgFmt_YV12;

        hasMain = MTRUE;
    }

    if (isP2A && rInfer.hasType(TID_SUB_RSZ_RAW))
    {
        auto& src_0 = rSrcData.editItemAt(rSrcData.add());
        src_0.mTypeId = TID_SUB_RSZ_RAW;
        src_0.mSizeId = SID_RESIZED;

        auto& dst_0 = rDstData.editItemAt(rDstData.add());
        dst_0.mTypeId = TID_SUB_RSZ_YUV;
        dst_0.mSizeId = SID_RESIZED;
        dst_0.mSize = rInfer.getSize(TID_SUB_RSZ_RAW);
        dst_0.mFormat = eImgFmt_YV12;

        hasSub = MTRUE;
    }

    if (hasMain) {
        rMetadatas.push_back(MID_MAN_IN_P1_DYNAMIC);
        rMetadatas.push_back(MID_MAN_IN_APP);
        rMetadatas.push_back(MID_MAN_IN_HAL);
        rMetadatas.push_back(MID_MAN_OUT_APP);
        rMetadatas.push_back(MID_MAN_OUT_HAL);

        if (isP2A  && !rInfer.hasFeature(FID_DRE)&& !rInfer.hasFeature(FID_DRE)) {
            if (rInfer.hasFeature(FID_CZ))
                rFeatures.push_back(FID_CZ);
            if (rInfer.hasFeature(FID_HFG))
                rFeatures.push_back(FID_HFG);
        }

    }

    if (isP2A && hasSub) {
        rMetadatas.push_back(MID_SUB_IN_P1_DYNAMIC);
        rMetadatas.push_back(MID_SUB_IN_HAL);
    }

    rInfer.addNodeIO(nodeId, rSrcData, rDstData, rMetadatas, rFeatures);

    return OK;
}

} // NSCapture
} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam
