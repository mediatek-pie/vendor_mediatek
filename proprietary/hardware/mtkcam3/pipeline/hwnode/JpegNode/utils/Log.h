
#include <mtkcam/utils/std/Log.h>
#include <cutils/compiler.h>
#include <type_traits>
#include <string>

/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, ...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##__VA_ARGS__)
#define MY_LOGD(fmt, ...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##__VA_ARGS__)
#define MY_LOGI(fmt, ...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##__VA_ARGS__)
#define MY_LOGW(fmt, ...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##__VA_ARGS__)
#define MY_LOGE(fmt, ...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##__VA_ARGS__)
#define MY_LOGA(fmt, ...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##__VA_ARGS__)
#define MY_LOGF(fmt, ...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##__VA_ARGS__)

#define JPG_LOGV(fmt, ...)        CAM_LOGV("[%s] " fmt, LOG_TAG, ##__VA_ARGS__)
#define JPG_LOGD(fmt, ...)        CAM_LOGD("[%s] " fmt, LOG_TAG, ##__VA_ARGS__)
#define JPG_LOGI(fmt, ...)        CAM_LOGI("[%s] " fmt, LOG_TAG, ##__VA_ARGS__)
#define JPG_LOGW(fmt, ...)        CAM_LOGW("[%s] " fmt, LOG_TAG, ##__VA_ARGS__)
#define JPG_LOGE(fmt, ...)        CAM_LOGE("[%s] " fmt, LOG_TAG, ##__VA_ARGS__)
#define JPG_LOGA(fmt, ...)        CAM_LOGA("[%s] " fmt, LOG_TAG, ##__VA_ARGS__)
#define JPG_LOGF(fmt, ...)        CAM_LOGF("[%s] " fmt, LOG_TAG, ##__VA_ARGS__)

//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)

namespace NSCam {

class JpgLog final
{
public:
    JpgLog()
    {
        JpgLog("");
    }
    JpgLog(const char* msg) : message(msg)
    {
        if(CC_UNLIKELY(message == nullptr))
            return;
        JPG_LOGD("%s +", message);

    }

    ~JpgLog()
    {
        JPG_LOGD("%s -", message);
    }
private:
    const char *message;
};

#define JpgLog() JpgLog __JpgLog__(__func__)

};
