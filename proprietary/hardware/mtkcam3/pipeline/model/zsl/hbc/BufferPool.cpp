/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/BufferPool"
//
#include "MyUtils.h"
#include "BufferPool.h"

#include <mtkcam/utils/imgbuf/IIonImageBufferHeap.h>
#include <mtkcam/utils/imgbuf/ImageBufferHeap.h>
#include <mtkcam/utils/std/Format.h>
#include <mtkcam3/pipeline/utils/streaminfo/ImageStreamInfo.h>
#include <mtkcam3/pipeline/stream/IStreamInfo.h>

#include <sys/prctl.h>
#include <sys/resource.h>

#include <utils/RWLock.h>
#include <utils/Thread.h>
//

using namespace android;
using namespace NSCam;
using namespace NSCam::v3;
using namespace NSCam::Utils;
using namespace NSCam::v3::Utils;
/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGD1(...)               MY_LOGD_IF((mLogLevel>=1),__VA_ARGS__)
#define MY_LOGD2(...)               MY_LOGD_IF((mLogLevel>=2),__VA_ARGS__)
#define MY_LOGD3(...)               MY_LOGD_IF((mLogLevel>=3),__VA_ARGS__)
//
#if 0
#define FUNC_START     MY_LOGD("%p:+", this)
#define FUNC_END       MY_LOGD("%p:-", this)
#else
#define FUNC_START
#define FUNC_END
#endif

/******************************************************************************
 *
 ******************************************************************************/
#define BUFFERPOOL_NAME       ("Cam@v3BufferPool")
#define BUFFERPOOL_POLICY     (SCHED_OTHER)
#define BUFFERPOOL_PRIORITY   (0)


/******************************************************************************
 *
 ******************************************************************************/
BufferPool::
BufferPool(
    sp<IImageStreamInfo> pStreamInfo
)
    : mLogLevel()
    , mpStreamInfo(pStreamInfo)
    , mPoolOpsLock()
    , mAvailLock()
    , mInUseLock()
    , mAvailableBuf()
    , mInUseBuf()
    //
    , mbInited(false)
    , mMaxBuffer(0)
    , mMinBuffer(0)
    , mvFutures()
    , mAvailableCond()
    , mExit(false)
{
    mLogLevel = ::property_get_int32("vendor.debug.camera.log", 0);
    if ( mLogLevel == 0 ) {
        mLogLevel = ::property_get_int32("vendor.debug.camera.log.bufferpool", 0);
    }
}


/******************************************************************************
 *
 ******************************************************************************/
auto
BufferPool::
allocateBuffer(
    char const* szCallerName,
    size_t maxNumberOfBuffers,
    size_t minNumberOfInitialCommittedBuffers
) -> status_t
{
    FUNC_START;
    //
    if ( mpStreamInfo == 0 ) {
        MY_LOGE("No ImageStreamInfo.");
        return UNKNOWN_ERROR;
    }

    if ( minNumberOfInitialCommittedBuffers > maxNumberOfBuffers) {
        MY_LOGE("mMinBuffer:%zu > mMaxBuffer:%zu", minNumberOfInitialCommittedBuffers, maxNumberOfBuffers);
        return UNKNOWN_ERROR;
    }

    MY_LOGD1("inited(%s)", (mbInited==true)? "TRUE":"FALSE" );
    if ( ! mbInited ) {
        mMaxBuffer = minNumberOfInitialCommittedBuffers;
        mMinBuffer = minNumberOfInitialCommittedBuffers;

        for ( MINT32 i = 0; i < mMinBuffer; ++i ) {
            sp<IImageBufferHeap> pHeap;
            if( do_construct(pHeap) == NO_MEMORY ) {
                MY_LOGE("do_construct allocate buffer failed");
                continue;
            }
            {
                Mutex::Autolock _l(mAvailLock);
                mAvailableBuf.push_back(pHeap);
            }
        }
        mbInited = true;
    }

    return updateBufferCount(szCallerName, maxNumberOfBuffers);
}

/******************************************************************************
 *
 ******************************************************************************/
auto
BufferPool::
onLastStrongRef(const void* /*id*/) -> void
{
    mExit = true;
    mAvailableCond.signal();
    //
    status_t result = OK;
    for( auto &fut : mvFutures ) {
        result = fut.get();
    }
    mvFutures.clear();
    //
    {
        Mutex::Autolock _l(mAvailLock);
        mAvailableBuf.clear();
    }

    {
        Mutex::Autolock _ll(mInUseLock);
        if ( mInUseBuf.size() > 0 ) {
            typename List< sp<IImageBufferHeap> >::iterator iter = mInUseBuf.begin();
            while( iter != mInUseBuf.end() ) {
                MY_LOGW("[%s] buffer %p not return to pool.", poolName(), (*iter).get());
                iter++;
            }
        }

        mInUseBuf.clear();
    }
}


/******************************************************************************
 *
 ******************************************************************************/
auto
BufferPool::
acquireFromPool(
    char const*             szCallerName,
    uint32_t const          iRequestNo,
    sp<IImageBufferHeap>&   rpBuffer
) -> status_t
{
    MY_LOGD1("[%d] %s", iRequestNo, szCallerName );

    Mutex::Autolock _l(mPoolOpsLock);
    return getBufferFromPool_l(
                      szCallerName,
                      rpBuffer
                   );
}

/******************************************************************************
 *
 ******************************************************************************/
auto
BufferPool::
getBufferFromPool_l(
    char const*           szCallerName,
    sp<IImageBufferHeap>& rpBuffer
) -> status_t
{
    FUNC_START;

    Mutex::Autolock _l(mAvailLock);
    Mutex::Autolock _ll(mInUseLock);

    MY_LOGD1( "[%s] mAvailableBuf:%zu mInUseBuf:%zu",
              szCallerName, mAvailableBuf.size(), mInUseBuf.size() );
    if( !mAvailableBuf.empty() )
    {
        typename List< sp<IImageBufferHeap> >::iterator iter = mAvailableBuf.begin();
        mInUseBuf.push_back(*iter);
        rpBuffer = *iter;
        mAvailableBuf.erase(iter);
        //
        FUNC_END;
        return OK;
    }

    FUNC_END;
    return TIMED_OUT;
}

/******************************************************************************
 *
 ******************************************************************************/
auto
BufferPool::
releaseToPool(
    char const*             szCallerName,
    uint32_t const          /*iRequestNo*/,
    sp<IImageBufferHeap>    rpBuffer
) -> status_t
{
    MY_LOGD1( "%s", szCallerName );

    Mutex::Autolock _l(mPoolOpsLock);
    return returnBufferToPool_l(
                      szCallerName,
                      rpBuffer
                   );

    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
auto
BufferPool::
returnBufferToPool_l(
    char const* szCallerName,
    sp<IImageBufferHeap> rpBuffer
) -> status_t
{
    FUNC_START;

    Mutex::Autolock _l(mAvailLock);
    Mutex::Autolock _ll(mInUseLock);

    if(mInUseBuf.size()==0)
    {
        MY_LOGE("mInUseBuf is empty! [%s] mAvailableBuf:%zu", szCallerName, mAvailableBuf.size());
        FUNC_END;
        return UNKNOWN_ERROR;
    }
    typename List< sp<IImageBufferHeap> >::iterator iter = mInUseBuf.begin();
    while( iter != mInUseBuf.end() ) {
        if ( rpBuffer == (*iter) ) {
            mAvailableBuf.push_back(*iter);
            mInUseBuf.erase(iter);

            mAvailableCond.signal();

            MY_LOGD1( "[%s] mAvailableBuf:%zu mInUseBuf:%zu return buf:%p",
                      szCallerName, mAvailableBuf.size(), mInUseBuf.size(), rpBuffer.get() );
            FUNC_END;
            return OK;
        }
        iter++;
    }

    MY_LOGE("[%s] Cannot find buffer %p.", szCallerName, rpBuffer.get());

    FUNC_END;

    return UNKNOWN_ERROR;
}

/******************************************************************************
 *
 ******************************************************************************/
auto
BufferPool::
poolName() const -> char const*
{
    return (mpStreamInfo == 0) ? mpStreamInfo->getStreamName() : "Pool";
}

/******************************************************************************
 *
 ******************************************************************************/
auto
BufferPool::
dumpPool() const -> MVOID
{
    MY_LOGI( "[%#" PRIx64 " %s] stream:%dx%d format:%#x transform:%d",
             mpStreamInfo->getStreamId(), mpStreamInfo->getStreamName(),
             mpStreamInfo->getImgSize().w, mpStreamInfo->getImgSize().h,
             mpStreamInfo->getImgFormat(), mpStreamInfo->getTransform()
    );

    Mutex::Autolock _l(mAvailLock);
    Mutex::Autolock _ll(mInUseLock);

    MY_LOGI( "logLevel:%d Max:%d Min:%d future:%zu inUse:%zu available:%zu",
             mLogLevel, mMaxBuffer, mMinBuffer, mvFutures.size(),
             mInUseBuf.size(), mAvailableBuf.size() );
    //
    {
        String8 str = String8::format("Available Buffer: ");
        typename List< sp<IImageBufferHeap> >::const_iterator iter = mAvailableBuf.begin();
        while( iter != mAvailableBuf.end() ) {
            str += String8::format("%p; ", (*iter).get());
            iter++;
        }
        MY_LOGI("%s", str.string());
    }
    //
    {
        String8 str = String8::format("Inuse Buffer: ");
        typename List< sp<IImageBufferHeap> >::const_iterator iter = mInUseBuf.begin();
        while( iter != mInUseBuf.end() ) {
            str += String8::format("%p; ", (*iter).get());
            iter++;
        }
        MY_LOGI("%s", str.string());
    }
}

/******************************************************************************
 *
 ******************************************************************************/
auto
BufferPool::
updateBufferCount(
    char const*                   /*szCallerName*/,
    MINT32                        rMaxBufferNumber
) -> status_t
{
    status_t result = OK;
    for( auto &fut : mvFutures ) {
        result = fut.get();
    }
    //
    mvFutures.clear();
    //
    mvFutures.push_back(
        std::async(std::launch::async,
            [ this ](MINT32 rMaxBufferNumber) {
                MY_LOGD("updateBufferCount: thread for allocate buffer of pool +");
                ::prctl(PR_SET_NAME, (unsigned long)"updateBufferCount", 0, 0, 0);
                //
                status_t err = OK;
                while( mMaxBuffer != rMaxBufferNumber && !mExit ) {
                    MY_LOGD("updateBufferCount: current max(%d) target(%d)", mMaxBuffer, rMaxBufferNumber);
                    // increase buffer
                    if ( mMaxBuffer < rMaxBufferNumber ) {
                        sp<IImageBufferHeap> pHeap;
                        if( do_construct(pHeap) == NO_MEMORY ) {
                            MY_LOGE("do_construct allocate buffer failed");
                            continue;
                        }
                        {
                            Mutex::Autolock _l(mAvailLock);
                            mAvailableBuf.push_back(pHeap);
                            mMaxBuffer++;
                        }
                    }
                    // reduce buffer
                    else if ( mMaxBuffer > rMaxBufferNumber ) {
                        Mutex::Autolock _l(mAvailLock);
                        if ( mAvailableBuf.empty() ) {
                            mAvailableCond.wait(mAvailLock);
                        }
                        //
                        if ( !mAvailableBuf.empty() ) {
                            mAvailableBuf.erase(mAvailableBuf.begin());
                            mMaxBuffer--;
                        }
                    }
                }
                MY_LOGD("updateBufferCount: thread for allocate buffer of pool -");
                //
                return err;
            }, rMaxBufferNumber
        )
    );
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
BufferPool::
do_construct(
    sp<IImageBufferHeap>& pImageBufferHeap
) -> status_t
{
    IImageStreamInfo::BufPlanes_t const& bufPlanes = mpStreamInfo->getAllocBufPlanes();
    size_t bufStridesInBytes[3] = {0};
    size_t bufBoundaryInBytes[3]= {0};
    for (size_t i = 0; i < bufPlanes.size(); i++) {
        bufStridesInBytes[i] = bufPlanes[i].rowStrideInBytes;
    }

    if ( eImgFmt_JPEG == mpStreamInfo->getAllocImgFormat() ||
         eImgFmt_BLOB == mpStreamInfo->getAllocImgFormat() )
    {
        IImageBufferAllocator::ImgParam imgParam(
                mpStreamInfo->getImgSize(),
                (*bufStridesInBytes),
                (*bufBoundaryInBytes) );
        imgParam.imgFormat = eImgFmt_BLOB;
        MY_LOGD("eImgFmt_JPEG -> eImgFmt_BLOB");
        pImageBufferHeap = IIonImageBufferHeap::create(
                                mpStreamInfo->getStreamName(),
                                imgParam,
                                IIonImageBufferHeap::AllocExtraParam(),
                                MFALSE
                            );
    }
    else
    {
        IImageBufferAllocator::ImgParam imgParam(
            mpStreamInfo->getAllocImgFormat(),
            mpStreamInfo->getImgSize(),
            bufStridesInBytes, bufBoundaryInBytes,
            bufPlanes.size()
            );
        MY_LOGD( "streamId:%#" PRIx64 " name:%s format:%x, size:(%d,%d), stride:%zu, boundary:%zu, planes:%zu",
                 mpStreamInfo->getStreamId(), mpStreamInfo->getStreamName(), mpStreamInfo->getAllocImgFormat(),
                 mpStreamInfo->getImgSize().w, mpStreamInfo->getImgSize().h,
                 bufStridesInBytes[0], bufBoundaryInBytes[0], bufPlanes.size() );
        pImageBufferHeap = IIonImageBufferHeap::create(
                                mpStreamInfo->getStreamName(),
                                imgParam,
                                IIonImageBufferHeap::AllocExtraParam(),
                                MFALSE
                            );
    }

    return OK;
}
