/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_SESSION_PIPELINEMODELSESSIONMULTICAM_H_
#define _MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_SESSION_PIPELINEMODELSESSIONMULTICAM_H_
//
#include "PipelineModelSessionDefault.h"
#include <mtkcam/drv/IHalSensor.h>
#include <mtkcam/utils/LogicalCam/Type.h>


/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace model {
/******************************************************************************
 * multicam utility
 ******************************************************************************/
class MultiCamMetadataUtility
{
public:
    MultiCamMetadataUtility() = default;
    ~MultiCamMetadataUtility();
public:
    static bool split(
                PipelineModelSessionBase::Result& original,
                PipelineModelSessionBase::Result& residue,
                std::unordered_map<int32_t, PipelineModelSessionBase::Result>& result,
                std::unordered_map<int64_t, int32_t>& groupSet);
    static bool filterResultByResultKey(int32_t sensorId, const IMetadata& src, IMetadata& dst);
};
/******************************************************************************
 *
 ******************************************************************************/
class PipelineModelSessionMultiCam
    : public PipelineModelSessionDefault
{
public:
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Interfaces (called by Session Factory).
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////    Instantiation.
    static  auto    makeInstance(
                        std::string const& name,
                        CtorParams const& rCtorParams
                        ) -> android::sp<IPipelineModelSession>;
    PipelineModelSessionMultiCam(
            std::string const& name,
            CtorParams const& rCtorParams);
    virtual ~PipelineModelSessionMultiCam();
public:     ////    Configuration.
    auto    configure() -> int override final;
    auto    updateBeforeBuildPipelineContext() -> int override final;
    auto    updateFrame(
                        MUINT32 const requestNo,
                        MINTPTR const userId,
                        Result const& result
                    ) -> MVOID override final;
public:
    std::function<bool(void)> mDualFeatureConfiguration = nullptr;
    std::function<
                bool(
                    MUINT32 const requestNo,
                    MINTPTR const userId,
                    Result const& result)>
                        mDualFeatureFrameUpdate = nullptr;
public:
    auto configure_vsdof() -> bool;
    auto configure_multicam() -> bool;
    auto updateFrame_vsdof(
                        MUINT32 const requestNo,
                        MINTPTR const userId,
                        Result const& result) -> bool;
    auto updateFrame_multicam(
                        MUINT32 const requestNo,
                        MINTPTR const userId,
                        Result const& result) -> bool;
private:
    auto    prepareSensorObject() -> bool;
    auto    setSensorSyncToSensorDriver(bool enable) -> bool;
private:
    MINT32 mMultiCamFeatureMode = 0;
    SensorSyncType mSensorSyncType = SensorSyncType::NOT_SUPPORT;
    std::vector<MUINT32> mvSensorDevIdList;
    std::vector<NSCam::IHalSensor*> mvSensorHalInterface;
    // blacklist for vsdof, need to skip some stream id for app callback
    std::vector<int64_t> mvStreamId_BlackList;
    // thermal policy name
    std::string thermal_policy_name;
    bool mbShowLog = false;
};


/******************************************************************************
 *
 ******************************************************************************/
};  //namespace model
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam
#endif  //_MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_SESSION_PIPELINEMODELSESSIONMULTICAM_H_

