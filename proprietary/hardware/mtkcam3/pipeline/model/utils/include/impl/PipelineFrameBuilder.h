/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_UTILS_INCLUDE_IMPL_PIPELINEFRAMEBUILDER_H_
#define _MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_UTILS_INCLUDE_IMPL_PIPELINEFRAMEBUILDER_H_

#include <mtkcam3/pipeline/pipeline/PipelineContext.h>

#include <impl/types.h>

#include <unordered_map>
#include <vector>

/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace model {


/**
 * Unsed on the call buildPipelineFrame().
 */
struct BuildPipelineFrameInputParams
{
    using HalImageStreamBuffer = NSCam::v3::Utils::HalImageStreamBuffer;
    using HalMetaStreamBuffer = NSCam::v3::Utils::HalMetaStreamBuffer;
    using IOMapSet = NSCam::v3::NSPipelineContext::IOMapSet;

    /**
     * Request no.
     */
    uint32_t const                              requestNo = 0;

    /**
     * Reprocess frame or not.
     */
    MBOOL const                                 bReprocessFrame = MFALSE;

    /**
     * App image stream buffers.
     */
    ParsedAppImageStreamBuffers const*          pAppImageStreamBuffers = nullptr;

    /**
     * App meta stream buffers.
     */
    std::vector<android::sp<IMetaStreamBuffer>> const*
                                                pAppMetaStreamBuffers = nullptr;

    /**
     * Hal image stream buffers.
     */
    std::vector<android::sp<HalImageStreamBuffer>> const*
                                                pHalImageStreamBuffers = nullptr;

    /**
     * Hal meta stream buffers.
     */
    std::vector<android::sp<HalMetaStreamBuffer>> const*
                                                pHalMetaStreamBuffers = nullptr;

    /**
     * Updated image stream info.
     */
    std::unordered_map<StreamId_T, android::sp<IImageStreamInfo>> const*
                                                pvUpdatedImageStreamInfo = nullptr;

    /**
     * IOMapSet for all pipeline nodes.
     */
    std::vector<NodeId_T> const*                pnodeSet = nullptr;
    std::unordered_map<NodeId_T, IOMapSet>const*pnodeIOMapImage = nullptr;
    std::unordered_map<NodeId_T, IOMapSet>const*pnodeIOMapMeta = nullptr;

    /**
     * The root nodes of a pipeline.
     */
    NSPipelineContext::NodeSet const*           pRootNodes = nullptr;

    /**
     * The edges to connect pipeline nodes.
     */
    NSPipelineContext::NodeEdgeSet const*       pEdges = nullptr;

    /**
     * RequestBuilder's callback.
     */
    android::wp<NSPipelineContext::RequestBuilder::AppCallbackT> const
                                                pCallback = nullptr;

    /**
     * Pipeline context
     */
    android::sp<NSPipelineContext::PipelineContext> const
                                                pPipelineContext = nullptr;

    /**
     * Pipeline context
     */
    std::vector<int32_t> const*                 vPhysicalCameraSetting;

};


/**
 * Generate a new pipeline frame.
 *
 * @param[out] out: a new-created pipeline frame.
 *
 * @param[in] in: input parameters.
 *
 * @return
 *      0 indicates success; otherwise failure.
 */
auto buildPipelineFrame(
    android::sp<IPipelineFrame>& out,
    BuildPipelineFrameInputParams const& in
) -> int;


/******************************************************************************
 *
 ******************************************************************************/
};  //namespace model
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam
#endif  //_MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_UTILS_INCLUDE_IMPL_PIPELINEFRAMEBUILDER_H_

