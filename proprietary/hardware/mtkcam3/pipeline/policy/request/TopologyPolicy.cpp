/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mtkcam-TopologyPolicy"

#include <mtkcam3/pipeline/policy/ITopologyPolicy.h>

#include "MyUtils.h"

#include <mtkcam3/pipeline/hwnode/NodeId.h>


/******************************************************************************
 *
 ******************************************************************************/
using namespace android;


/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)


/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace policy {


/**
 * Make a function target as a policy - default version
 */
FunctionType_TopologyPolicy makePolicy_Topology_Default()
{
    using namespace topology;

    return [](
        RequestOutputParams& out __unused,
        RequestInputParams const& in __unused
    ) -> int
    {
        auto const pCfgNodesNeed    = in.pConfiguration_PipelineNodesNeed;
        auto const pCfgStream_NonP1 = in.pConfiguration_StreamInfo_NonP1;
        if ( CC_UNLIKELY( !pCfgNodesNeed || !pCfgStream_NonP1 ) ) {
            MY_LOGE("null configuration params");
            return -EINVAL;
        }
        if ( in.pConfiguration_PipelineNodesNeed->needP1Node.size()>2) {
            MY_LOGE("current flow not support more than 2 p1node.");
            return -EINVAL;
        }

        PipelineNodesNeed*                pNodesNeed = out.pNodesNeed;
        std::vector<NodeId_T>*            pNodeSet   = out.pNodeSet;
        RequestOutputParams::NodeSet*     pRootNodes = out.pRootNodes;
        RequestOutputParams::NodeEdgeSet* pEdges     = out.pEdges;

        pRootNodes->add(eNODEID_P1Node);
        pNodesNeed->needP1Node.push_back(true);
        pNodeSet->push_back(eNODEID_P1Node);
        MBOOL needConnectMain2ToStreaming = MFALSE;
        MBOOL needConnectMain2ToCapture = MFALSE;
        if ( in.pConfiguration_PipelineNodesNeed->needP1Node.size()>1 ) {
            pRootNodes->add(eNODEID_P1Node_main2);
            pNodesNeed->needP1Node.push_back(true);
            pNodeSet->push_back(eNODEID_P1Node_main2);
            needConnectMain2ToStreaming = MTRUE;
            needConnectMain2ToCapture = MTRUE;
        }

        if ( in.isDummyFrame ) {
            return OK;
        }

        auto const pReqImageSet = in.pRequest_AppImageStreamInfo;
        // jpeg
        if ( pReqImageSet!=nullptr && pReqImageSet->pAppImage_Jpeg.get() ) {
            bool found = false;
            const auto& streamId = pCfgStream_NonP1->pHalImage_Jpeg_YUV->getStreamId();
            for ( const auto& s : *(in.pvImageStreamId_from_CaptureNode) ) {
                if ( s == streamId ) {
                    pEdges->addEdge(eNODEID_P2CaptureNode, eNODEID_JpegNode);
                    found = true;
                    break;
                }
            }

            if ( !found ) {
                for ( const auto& s : *(in.pvImageStreamId_from_StreamNode) ) {
                    if ( s == streamId ) {
                        pEdges->addEdge(eNODEID_P2StreamNode, eNODEID_JpegNode);
                        found = true;
                        break;
                    }
                }
            }

            if ( !found ) {
                MY_LOGE("no p2 streaming&capture node w/ jpeg output");
                return -EINVAL;
            }
            pNodesNeed->needJpegNode = true;
            pNodeSet->push_back(eNODEID_JpegNode);
        }

        // fd
        if ( in.isFdEnabled && in.needP2StreamNode ) {
            pNodesNeed->needFDNode = true;
            pNodeSet->push_back(eNODEID_FDNode);
            if ( in.pPipelineStaticInfo->isP1DirectFDYUV )
            {
                pEdges->addEdge(eNODEID_P1Node, eNODEID_FDNode);
            }
            else
            {
                pEdges->addEdge(eNODEID_P2StreamNode, eNODEID_FDNode);
            }
        }

        // raw16
        if  ( pCfgNodesNeed->needRaw16Node
           && (pReqImageSet!=nullptr && pReqImageSet->pAppImage_Output_RAW16.get())
            )
        {
            pNodesNeed->needRaw16Node = true;
            pNodeSet->push_back(eNODEID_RAW16);
            pEdges->addEdge(eNODEID_P1Node, eNODEID_RAW16);
#warning "dual cam main2?"
        }

        // p2 streaming
        if ( in.needP2StreamNode ) {
            pNodesNeed->needP2StreamNode = true;
            pNodeSet->push_back(eNODEID_P2StreamNode);
            pEdges->addEdge(eNODEID_P1Node, eNODEID_P2StreamNode);
            if(needConnectMain2ToStreaming)
            {
                pEdges->addEdge(eNODEID_P1Node_main2, eNODEID_P2StreamNode);
            }
        }

        // p2 capture
        if ( in.needP2CaptureNode ) {
            pNodesNeed->needP2CaptureNode = true;
            pNodeSet->push_back(eNODEID_P2CaptureNode);
            pEdges->addEdge(eNODEID_P1Node, eNODEID_P2CaptureNode);
            if(needConnectMain2ToCapture)
            {
                pEdges->addEdge(eNODEID_P1Node_main2, eNODEID_P2CaptureNode);
            }
        }

        // if ( in.pPipelineStaticInfo->isType3PDSensorWithoutPDE ) {
        //     pNodesNeed->needPDENode = true;
        //     MY_LOGE("not defined PDE node yet");
        //     return -EINVAL;
        // }

        return OK;
    };
}


};  //namespace policy
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam

