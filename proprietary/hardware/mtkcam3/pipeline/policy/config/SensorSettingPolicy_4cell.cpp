/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mtkcam-SensorSettingPolicy4cell"

#include <mtkcam3/pipeline/policy/IConfigSensorSettingPolicy.h>
//
#include "MyUtils.h"

#include <mtkcam/drv/IHalSensor.h>
#include <mtkcam/utils/hw/HwTransform.h>
#include <mtkcam/utils/hw/HwInfoHelper.h>

/******************************************************************************
 *
 ******************************************************************************/
using namespace android;
using namespace NSCam::v3::pipeline::policy;
using namespace NSCamHW;

/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)

#define CHECK_ERROR(_err_)                                \
    do {                                                  \
        MERROR const err = (_err_);                       \
        if( CC_UNLIKELY( err != OK ) ) {                                 \
            MY_LOGE("err:%d(%s)", err, ::strerror(-err)); \
            return err;                                   \
        }                                                 \
    } while(0)

#define parseSensorSetting(res, _scenarioId_, infoHelper)                        \
    do {                                                                       \
        int32_t fps;                                                           \
        MSize   size;                                                          \
        if ( CC_UNLIKELY( ! infoHelper.getSensorFps( _scenarioId_, fps) ) ) {    \
            MY_LOGW("getSensorFps fail"); break;                               \
        }                                                                      \
        if ( CC_UNLIKELY( ! infoHelper.getSensorSize(_scenarioId_, size) ) ) {   \
            MY_LOGW("getSensorSize fail"); break;                              \
        }                                                                      \
        res.sensorFps  = static_cast<uint32_t>(fps);                           \
        res.sensorSize = size;                                                 \
        res.sensorMode = _scenarioId_;                                           \
    } while(0)
/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace policy {

static auto
checkVhdrSensor(
    SensorSetting& res,
    const uint32_t vhdrMode,
    const HwInfoHelper& rHelper
) -> int
{
    uint32_t supportHDRMode = 0;
    char forceSensorMode[PROPERTY_VALUE_MAX];
    property_get("vendor.debug.force.vhdr.sensormode", forceSensorMode, "0");
    switch( forceSensorMode[0] )
    {
        case '0':
            break;
        case 'P':
        case 'p':
            parseSensorSetting(res,SENSOR_SCENARIO_ID_NORMAL_PREVIEW,rHelper);
            MY_LOGD("set sensor mode to NORMAL_PREVIEW(%d)",SENSOR_SCENARIO_ID_NORMAL_PREVIEW);
            return OK;
        case 'V':
        case 'v':
            parseSensorSetting(res,SENSOR_SCENARIO_ID_NORMAL_VIDEO,rHelper);
            MY_LOGD("set sensor mode to NORMAL_VIDEO(%d)",SENSOR_SCENARIO_ID_NORMAL_VIDEO);
            return OK;
        case 'C':
        case 'c':
            parseSensorSetting(res,SENSOR_SCENARIO_ID_NORMAL_CAPTURE,rHelper);
            MY_LOGD("set sensor mode to NORMAL_CAPTURE(%d)",SENSOR_SCENARIO_ID_NORMAL_CAPTURE);
            return OK;
        default:
            MY_LOGW("unknown force sensor mode(%s), not used", forceSensorMode);
            MY_LOGW("usage : setprop debug.force.vhdr.sensormode P/V/C");
            break;
        }

    // 1. Current sensor mode is VHDR support, use it.
    if ( CC_UNLIKELY( ! rHelper.querySupportVHDRMode( res.sensorMode,
                                                    supportHDRMode) ) )
    {
        MY_LOGE("[vhdrhal] HwInfoHelper querySupportVHDRMode fail");
        return -EINVAL;
    }
    if(vhdrMode == supportHDRMode)
    {
        MY_LOGD("[vhdrhal] senosr setting : vhdrMode_supportHDRMode_sensormode(%d,%d, %d)", vhdrMode, supportHDRMode, res.sensorMode);
        return OK;
    }

    // 2. Check sensor mode in order: preview -> video -> capture
    // Find acceptable sensor mode for this vhdrMode
    /*
           SENSOR_SCENARIO_ID_NORMAL_PREVIEW
           SENSOR_SCENARIO_ID_NORMAL_CAPTURE
           SENSOR_SCENARIO_ID_NORMAL_VIDEO
           SENSOR_SCENARIO_ID_SLIM_VIDEO1
           SENSOR_SCENARIO_ID_SLIM_VIDEO2
      */

#define CHECK_SENSOR_MODE_VHDR_SUPPORT(senMode, scenariomode)                \
do {                                                                         \
       if ( ! rHelper.querySupportVHDRMode(senMode, supportHDRMode) )        \
       {                                                                     \
           return -EINVAL;                                                   \
       }                                                                     \
       if (vhdrMode == supportHDRMode)                                       \
       {                                                                     \
           parseSensorSetting(res ,senMode ,rHelper);                        \
           MY_LOGD("[vhdrhal] re-try senosr setting :                        \
           vhdrMode_supportHDRMode_sensormode(%d,%d, %d)",                   \
           vhdrMode, supportHDRMode, res.sensorMode);                        \
           return OK;                                                        \
       }                                                                     \
} while (0)
       CHECK_SENSOR_MODE_VHDR_SUPPORT(SENSOR_SCENARIO_ID_NORMAL_PREVIEW,eNORMAL_PREVIEW);
       CHECK_SENSOR_MODE_VHDR_SUPPORT(SENSOR_SCENARIO_ID_NORMAL_VIDEO,eNORMAL_VIDEO);
       CHECK_SENSOR_MODE_VHDR_SUPPORT(SENSOR_SCENARIO_ID_NORMAL_CAPTURE, eNORMAL_CAPTURE);
#undef CHECK_SENSOR_MODE_VHDR_SUPPORT

    //3.  PREVIEW & VIDEO & CAPTURE mode are all not acceptable
    MY_LOGE("[vhdrhal] VHDR not support preview & video & capture mode.");
    return -EINVAL;
}
/**
 * Make a function target - 4cell version
 */
FunctionType_Configuration_SensorSettingPolicy makePolicy_Configuration_SensorSetting_4Cell()
{
    return [](Configuration_SensorSetting_Params const& params) -> int {
        auto pvOut = params.pvOut;
        auto pStreamingFeatureSetting = params.pStreamingFeatureSetting;
        auto pPipelineStaticInfo = params.pPipelineStaticInfo;
        auto pPipelineUserConfiguration = params.pPipelineUserConfiguration;

        if ( CC_UNLIKELY( ! pvOut || ! pStreamingFeatureSetting ||
                          ! pPipelineStaticInfo || ! pPipelineUserConfiguration ) ) {
            MY_LOGE("error input params");
            return -EINVAL;
        }

        auto const& pParsedAppImageInfo = pPipelineUserConfiguration->pParsedAppImageStreamInfo;

        for ( auto const id : pPipelineStaticInfo->sensorId ) {
            SensorSetting res;  // output
            HwInfoHelper  infoHelper = HwInfoHelper(id);
            infoHelper.updateInfos();

            // sensor mode decision policy:
            // 1. has video consumer
            if ( pParsedAppImageInfo->hasVideoConsumer ) {
                parseSensorSetting(res,SENSOR_SCENARIO_ID_NORMAL_VIDEO,infoHelper);
                goto lbDone;
            }
            // 2. default preview mode
            parseSensorSetting(res,SENSOR_SCENARIO_ID_NORMAL_PREVIEW,infoHelper);

lbDone:
            if (pStreamingFeatureSetting->vhdrMode != SENSOR_VHDR_MODE_NONE){
                CHECK_ERROR( checkVhdrSensor(res, pStreamingFeatureSetting->vhdrMode, infoHelper) );
            }
            CAM_LOGI("select size(%4dx%-4d)@%-3d sensorMode:%d vhdrMode:%d",
                res.sensorSize.w, res.sensorSize.h, res.sensorFps,
                res.sensorMode,
                pStreamingFeatureSetting->vhdrMode);
            pvOut->push_back(res);
        }
        return OK;
    };
}


};  //namespace policy
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam

