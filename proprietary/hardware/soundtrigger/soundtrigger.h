#ifndef __SOUNGTRIGGER_H__
#define __SOUNGTRIGGER_H__

//#include <linux/compat.h>
//#include <linux/fs.h>
//#include <linux/uaccess.h>
#include <linux/vow.h>

/***********************************************************************************
** Phase2.5 definition
************************************************************************************/
//#define VOW_DATA_READ_PCMFILE
//#define VOW_RECOG_PCMFILE

enum {
    RECOG_CONTINUOUS = 1,
    RECOG_PASS,
    RECOG_FAIL,
    RECOG_BAD
};

#endif //__SOUNGTRIGGER_H__
