/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/Util/LogicalDevice"

#include "MyUtils.h"
#include <cutils/properties.h>
#include <mtkcam/utils/LogicalCam/IHalLogicalDeviceList.h>
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
#include <camera_custom_logicaldevice.h>
#include <vector>

#include "LogicalDeviceMetadataHelper.h"

#if (MTKCAM_HAVE_AEE_FEATURE == 1)
#include <aee.h>
#endif

#include <kd_imgsensor_define.h>

#define MAX_SENSOR_NAME_SIZE (128)
#define MAX_DEVICE_NAME_SIZE (192)

/******************************************************************************
*
*******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)

using namespace android;
using namespace NSCam;
using namespace NSLogicalDeviceMetadataHelper;
/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {


/******************************************************************************
 *  Hal Sensor List Interface.
 ******************************************************************************/
class HalLogicalDeviceList
      : public IHalLogicalDeviceList
{
public:
                                    HalLogicalDeviceList();

protected:  ////                    Destructor.
    /**
     * Disallowed to directly delete a raw pointer.
     */
    virtual                         ~HalLogicalDeviceList() {}

public:     ////                    Attributes.

    /**
     * Query the number of logical devices.
     * This call is legal only after searchDevices().
     */
    MUINT                   queryNumberOfDevices() const override;

    /**
     * Query the number of image sensors.
     * This call is legal only after searchDevices().
     */
    MUINT                   queryNumberOfSensors() const override;

    /**
     * Query static information for a specific sensor index.
     * This call is legal only after searchSensors().
     */
    IMetadata const&        queryStaticInfo(MUINT const index) const override;

    /**
     * Query the driver name for a specific sensor index.
     * This call is legal only after searchSensors().
     */
    char const*             queryDriverName(MUINT const index) const override;

    /**
     * Query the sensor type of NSSensorType::Type for a specific sensor index.
     * This call is legal only after searchSensors().
     */
    MUINT                   queryType(MUINT const index) const override;

    /**
     * Query the sensor facing direction for a specific sensor index.
     * This call is legal only after searchSensors().
     */
    MUINT                   queryFacingDirection(
                                        MUINT const index
                                    ) const override;

    /**
     * Query SensorDev Index by sensor list index.
     * This call is legal only after searchSensors().
     * Return SENSOR_DEV_MAIN, SENSOR_DEV_SUB,...
     */
	MUINT                   querySensorDevIdx(MUINT const index) const override;

    /**
     * Query static SensorStaticInfo for a specific sensor index.
     * This call is legal only after searchSensors().
     */
    SensorStaticInfo const* querySensorStaticInfo(
                                        MUINT const indexDual
                                    ) const override;

    /**
     * Query Sensor Information.
     * This call is legal only after searchSensors().
     */
    MVOID                   querySensorStaticInfo(
                                        MUINT indexDual,
                                        SensorStaticInfo *pSensorStaticInfo
                                    ) const override;

    /**
     * Search sensors and return the number of image sensors.
     */
    MUINT                   searchDevices() override;

    std::vector<MINT32>     getSensorId(MINT32 deviceId) override;

    MINT32                  getDeviceId(MINT32 sensorId) override;

    MINT32                  getSupportedFeature(MINT32 deviceId) const override;

    SensorSyncType          getSyncType(MINT32 deviceId) const override;

    MINT32                  getSensorSyncMasterDevId(MINT32 deviceId) const override;

    bool                    updateMultiCamSensorMetadata(MINT32 sensorId, IMetadata &metadata);

private:
    struct TempSensorInfo
    {
        MINT32 SensorId;
        MINT32 RawType;
        MINT32 Facing;
        MINT32 CaptureModeWidth;
        char   Name[MAX_SENSOR_NAME_SIZE];
    };
    using SensorInfo_t = ::android::KeyedVector<std::string, struct TempSensorInfo>;

    class SyncTypeInfo : public virtual RefBase
    {
    public:
        SensorSyncType syncType = SensorSyncType::NOT_SUPPORT;
        MUINT32 masterDevId = 0xFF;
        std::vector<MUINT32> syncMode;
        std::vector<MUINT32> slaveDevId; //only one master, but can have more than 1 slave.
    };

    //
    // local function
    MINT32                          createDeviceMap();
    MINT32                          addLogicalDevice(SensorInfo_t vInfo, struct LogicalSensorStruct* pLogicalSen, MINT32 DevNum);
    void                            dumpDebugInfo();
    // query sync mode
    sp<SyncTypeInfo>                querySyncMode(std::vector<MINT32>& Sensors);

    class CamDeviceInfo : public virtual RefBase
    {
    public:
        std::vector<MINT32> Sensors;
        MUINT DualFeature;
        MINT32 RawType;
        char Name[MAX_DEVICE_NAME_SIZE];
        sp<SyncTypeInfo> syncTypeInfo = nullptr;
        IMetadata sensorStaticMetadata;
    };
    
    KeyedVector< MINT32, sp<CamDeviceInfo> > mDeviceSensorMap;
    mutable Mutex mLock;
    //
    sp<LogicalDeviceMetadataHelper> mpLogicalDeviceMetadataHelper = nullptr;
};


};  //namespace NSCam

static Mutex gHalLogicalDevCreateLock;


/******************************************************************************
 *
 ******************************************************************************/
IHalLogicalDeviceList* IHalLogicalDeviceList::get()
{
    Mutex::Autolock _l(gHalLogicalDevCreateLock);
    static HalLogicalDeviceList* gInst = new HalLogicalDeviceList();

    //MY_LOGD("gInst : %p", gInst);
        
    return gInst;
}

HalLogicalDeviceList::
HalLogicalDeviceList()
{
    // create logical device metadata helper.
    mpLogicalDeviceMetadataHelper = new LogicalDeviceMetadataHelper();
}

MINT32
HalLogicalDeviceList::
addLogicalDevice(SensorInfo_t vInfo, struct LogicalSensorStruct* pLogicalSen, MINT32 DevNum)
{
    sp<CamDeviceInfo> Info = new CamDeviceInfo();
    const char* Main1Name;
    // check sensor count is match to NumofCombinSensor.
    if((pLogicalSen->Sensorlist.size())%(pLogicalSen->NumofCombinSensor) != 0)
    {
        MY_LOGE("Sensor list count(%zu) does not match to combin sensor count(%d)",
                    pLogicalSen->Sensorlist.size(),
                    pLogicalSen->NumofCombinSensor);
#if (MTKCAM_HAVE_AEE_FEATURE == 1)
        aee_system_exception(
            LOG_TAG,
            NULL,
            DB_OPT_DEFAULT,
            "\nCRDISPATCH_KEY:logic device create fail.");
#endif
        return -1;
    }
    for(int i = 0; i < pLogicalSen->NumofCombinSensor; i++)
    {
        MINT32 idx = vInfo.indexOfKey(pLogicalSen->Sensorlist[DevNum*pLogicalSen->NumofCombinSensor + i]);
        if(idx < 0)
        {
            return -1;
        }
        Info->Sensors.push_back(vInfo.valueAt(idx).SensorId);
        if (i == 0)
        {
            Main1Name = vInfo.valueAt(idx).Name;
        }
    }
    Info->DualFeature = pLogicalSen->Feature;
    Info->RawType = SENSOR_RAW_Bayer;
    Info->syncTypeInfo = querySyncMode(Info->Sensors);
    strncpy(Info->Name, Main1Name, MAX_DEVICE_NAME_SIZE - 1);
    strncat(Info->Name, "_", 1);
    strncat(Info->Name, pLogicalSen->Name, sizeof(pLogicalSen->Name));
    // assign sensor static metadata
    if(Info->Sensors.size() > 1)
    {
        MY_LOGD("query static metadata for multi-cam (%s)", Info->Name);
        auto sensorDevId = MAKE_HalSensorList()->querySensorDevIdx(Info->Sensors[0]);
        SensorStaticInfo sensorStaticInfo;
        MAKE_HalSensorList()->querySensorStaticInfo(sensorDevId, &sensorStaticInfo);
        mpLogicalDeviceMetadataHelper->constructStaticMetadata(
                                            Info->Sensors[0],
                                            std::string(Info->Name),
                                            (sensorStaticInfo.facingDirection==0)?true:false,
                                            Info->sensorStaticMetadata);
        // update sensor query metadata
        updateMultiCamSensorMetadata(Info->Sensors[0], Info->sensorStaticMetadata);
    }
    else
    {
        Info->sensorStaticMetadata = MAKE_HalSensorList()->queryStaticInfo(Info->Sensors[0]);
    }
    MY_LOGI("add new logic device: %s", Info->Name);
    mDeviceSensorMap.add(mDeviceSensorMap.size(), Info);
    return 0;
}

void
HalLogicalDeviceList::
dumpDebugInfo()
{
    MY_LOGI_IF(1, "map size : %zu", mDeviceSensorMap.size());
    for(unsigned int i = 0; i < mDeviceSensorMap.size(); i++)
    {
        MY_LOGI_IF(1, "index(%u) name : %s", i, queryDriverName(i));
        MY_LOGI_IF(1, "index(%u) facing : %d", i, queryFacingDirection(i));
    }
}

sp<HalLogicalDeviceList::SyncTypeInfo>
HalLogicalDeviceList::
querySyncMode(
    std::vector<MINT32>& Sensors)
{
    IHalSensorList* const pHalSensorList = MAKE_HalSensorList();
    // for sync, main sensor must support master mode, and other
    // sensors need support slave mode.
    String8 checkString("");
    sp<SyncTypeInfo> info = new SyncTypeInfo();
    std::vector<bool> masterCheckList;
    std::vector<bool> slaveCheckList;
    std::vector<MUINT32> devIdList;
    for(auto& sensorId : Sensors)
    {
        NSCam::IHalSensor* pHalSensor = pHalSensorList->createSensor(
                                            LOG_TAG,
                                            sensorId);
        MUINT32 syncMode;
        MUINT32 sensorDevId = pHalSensorList->querySensorDevIdx(sensorId);
        MINT ret = pHalSensor->sendCommand(
                                sensorDevId,
                                SENSOR_CMD_GET_SENSOR_SYNC_MODE_CAPACITY,
                                (MUINTPTR)&syncMode,
                                0 /* unused */,
                                0 /* unused */);
        if(ret != 0)
        {
#if (MTKCAM_HAVE_AEE_FEATURE == 1)
            aee_system_exception(
                LOG_TAG,
                NULL,
                DB_OPT_DEFAULT,
                "\nCRDISPATCH_KEY:query sync mode fail.");
#endif
        }
        info->syncMode.push_back(syncMode);
        if((syncMode & SENSOR_MASTER_SYNC_MODE) && (syncMode & SENSOR_SLAVE_SYNC_MODE))
        {
            masterCheckList.push_back(true);
            slaveCheckList.push_back(true);
        }
        else if(syncMode & SENSOR_MASTER_SYNC_MODE)
        {
            masterCheckList.push_back(true);
            slaveCheckList.push_back(false);
        }
        else if(syncMode & SENSOR_SLAVE_SYNC_MODE)
        {
            masterCheckList.push_back(false);
            slaveCheckList.push_back(true);
        }
        else
        {
            masterCheckList.push_back(false);
            slaveCheckList.push_back(false);
        }
        devIdList.push_back(sensorDevId);
        checkString.appendFormat("S[%d:D%d:M%d:S%d] ",
                                    sensorId,
                                    sensorDevId,
                                    !!(syncMode & SENSOR_MASTER_SYNC_MODE),
                                    !!(syncMode & SENSOR_SLAVE_SYNC_MODE));
    }
    MINT masterIndex = -1;
    MINT slaveIndex = -1;
    for(unsigned int i = 0;i<masterCheckList.size();++i)
    {
        if(masterCheckList[i])
        {
            masterIndex = (MINT)i;
            for(unsigned int j=0;j<slaveCheckList.size();++j)
            {
                if(i != j)
                {
                    if(slaveCheckList[j])
                    {
                        slaveIndex = (MINT)j;
                        break;
                    }
                }
            }
            if(slaveIndex != -1) break;
        }
    }
    // set master id
    if(masterIndex != -1)
    {
        info->masterDevId = devIdList[masterIndex];
        checkString.appendFormat("M[D:%d] ", devIdList[masterIndex]);
    }
    for(unsigned int i=0;i<slaveCheckList.size();++i)
    {
        if(((MINT)i != masterIndex) && slaveCheckList[i] )
        {
            info->slaveDevId.push_back(devIdList[i]);
            checkString.appendFormat("S[D:%d] ", devIdList[i]);
        }
    }
    checkString.appendFormat("Master[D:%d] SlaveList[%zu]", info->masterDevId, info->slaveDevId.size());
    if(info->masterDevId != 0xFF && info->slaveDevId.size() == (Sensors.size() -1))
    {
        info->syncType = SensorSyncType::CALIBRATED;
        checkString.appendFormat("R[Calibrated]");
    }
    else
    {
        info->syncType = SensorSyncType::APPROXIMATE;
        checkString.appendFormat("R[Approximate]");
    }
    MY_LOGI("%s", checkString.string());
    return info;
}


MINT32
HalLogicalDeviceList::
createDeviceMap()
{
    IHalSensorList* const pHalSensorList = MAKE_HalSensorList();
    size_t const sensorNum = pHalSensorList->searchSensors();
    SensorInfo_t vTempInfo;
    TempSensorInfo TempInfo;
    SensorStaticInfo sensorStaticInfo;
    unsigned int i = 0;
    std::vector<struct LogicalSensorStruct> CustomDevList;

    MY_LOGD("sensorNum : %zu", sensorNum);

    CustomDevList = get_LogicalDevice_CustomizeData();
    MY_LOGD("manual device count = %zu", CustomDevList.size());
    for(i = 0; i < sensorNum; i++)
    {
        memset(&sensorStaticInfo, 0, sizeof(SensorStaticInfo));
        int sendorDevIndex = pHalSensorList->querySensorDevIdx(i);
        pHalSensorList->querySensorStaticInfo(sendorDevIndex, &sensorStaticInfo);

        TempInfo.SensorId = i;
        TempInfo.RawType = sensorStaticInfo.rawFmtType;
        TempInfo.Facing = sensorStaticInfo.facingDirection;
        TempInfo.CaptureModeWidth = sensorStaticInfo.captureWidth;
        strncpy(TempInfo.Name, pHalSensorList->queryDriverName(i), MAX_SENSOR_NAME_SIZE - 1);
        vTempInfo.add(TempInfo.Name, TempInfo);
        MY_LOGD("i : %d, facing : %d", i, sensorStaticInfo.facingDirection);
        MY_LOGD("i : %d, Name : %s", i, TempInfo.Name);
        MY_LOGD("i : %d, vTempInfo Name : %s", i, vTempInfo.valueFor(TempInfo.Name).Name);

        sp<CamDeviceInfo> Info = new CamDeviceInfo();
        Info->Sensors.push_back(i);
        Info->DualFeature = 0;
        Info->RawType = TempInfo.RawType;
        strncpy(Info->Name, TempInfo.Name, sizeof(Info->Name));
        // add physical sensor static metadata
        Info->sensorStaticMetadata = MAKE_HalSensorList()->queryStaticInfo(Info->Sensors[0]);
        MY_LOGD("i : %d, Info Name : %s, %p", i, Info->Name, Info->Name);

        mDeviceSensorMap.add(i, Info);
    }

    // manually add logical device from custom
    for(i = 0; i < CustomDevList.size(); i++)
    {
        for(int j = 0; j < CustomDevList[i].NumofDefinition; j++)
        {
            addLogicalDevice(vTempInfo, &CustomDevList[i], j);
        }
    }

    dumpDebugInfo();
    return 0;
}

MUINT
HalLogicalDeviceList::
queryNumberOfDevices() const
{
    Mutex::Autolock _l(mLock);
    return mDeviceSensorMap.size();
}

MUINT
HalLogicalDeviceList::
queryNumberOfSensors() const
{
    Mutex::Autolock _l(mLock);
    IHalSensorList* const pHalSensorList = MAKE_HalSensorList();
    return pHalSensorList->queryNumberOfSensors();
}


IMetadata const&
HalLogicalDeviceList::
queryStaticInfo(MUINT const index) const
{
    Mutex::Autolock _l(mLock);
    if (mDeviceSensorMap.size() == 0)
    {
        MY_LOGE("[queryStaticInfo] mDeviceSensorMap.size() == 0");
    }
    sp<CamDeviceInfo> info = mDeviceSensorMap.valueFor(index);
    return info->sensorStaticMetadata;
}

char const*
HalLogicalDeviceList::
queryDriverName(MUINT const index) const
{
    Mutex::Autolock _l(mLock);
    if (mDeviceSensorMap.size() == 0)
    {
        MY_LOGE("[queryDriverName] mDeviceSensorMap.size() == 0");
    }
    MY_LOGD("queryDriverName index : %d", index);
    sp<CamDeviceInfo> info = mDeviceSensorMap.valueFor(index);
    MY_LOGD("queryDriverName : %s, %p", info->Name, info->Name);

    return info->Name;
}

MUINT
HalLogicalDeviceList::
queryType(MUINT const index) const
{
    Mutex::Autolock _l(mLock);
    if (mDeviceSensorMap.size() == 0)
    {
        MY_LOGE("[queryType] mDeviceSensorMap.size() == 0");
    }
    IHalSensorList* const pHalSensorList = MAKE_HalSensorList();
    sp<CamDeviceInfo> info = mDeviceSensorMap.valueFor(index);
    
    return pHalSensorList->queryType(info->Sensors[0]);
}

MUINT
HalLogicalDeviceList::
queryFacingDirection(MUINT const index) const
{
    Mutex::Autolock _l(mLock);
    if (mDeviceSensorMap.size() == 0)
    {
        MY_LOGE("[queryFacingDirection] mDeviceSensorMap.size() == 0");
    }
    IHalSensorList* const pHalSensorList = MAKE_HalSensorList();
    sp<CamDeviceInfo> info = mDeviceSensorMap.valueFor(index);

    return pHalSensorList->queryFacingDirection(info->Sensors[0]);
}

MUINT
HalLogicalDeviceList::
querySensorDevIdx(MUINT const index) const
{
    Mutex::Autolock _l(mLock);
    if (mDeviceSensorMap.size() == 0)
    {
        MY_LOGE("[querySensorDevIdx] mDeviceSensorMap.size() == 0");
    }
    IHalSensorList* const pHalSensorList = MAKE_HalSensorList();
    sp<CamDeviceInfo> info = mDeviceSensorMap.valueFor(index);

    return pHalSensorList->querySensorDevIdx(info->Sensors[0]);
}

SensorStaticInfo const*
HalLogicalDeviceList::
querySensorStaticInfo(MUINT const indexDual) const
{
    Mutex::Autolock _l(mLock);
    if (mDeviceSensorMap.size() == 0)
    {
        MY_LOGE("[querySensorStaticInfo] mDeviceSensorMap.size() == 0");
    }
    MY_LOGD("queryDriverName1 index : %d", indexDual);
    IHalSensorList* const pHalSensorList = MAKE_HalSensorList();
    //CamDeviceInfo info = mDeviceSensorMap.valueFor(indexDual);

    return pHalSensorList->querySensorStaticInfo(indexDual);
}

MVOID
HalLogicalDeviceList::
querySensorStaticInfo(MUINT indexDual,
                               SensorStaticInfo *pSensorStaticInfo) const
{
    Mutex::Autolock _l(mLock);
    if (mDeviceSensorMap.size() == 0)
    {
        MY_LOGE("[querySensorStaticInfo] mDeviceSensorMap.size() == 0");
    }
    MY_LOGD("queryDriverName2 index : %d", indexDual);
    IHalSensorList* const pHalSensorList = MAKE_HalSensorList();
    //CamDeviceInfo info = mDeviceSensorMap.valueFor(indexDual);

    pHalSensorList->querySensorStaticInfo(indexDual, pSensorStaticInfo);
}

MUINT
HalLogicalDeviceList::
searchDevices()
{
    //Mutex::Autolock _l(mLock);
    
    if( mDeviceSensorMap.size() == 0 )
    {
        MY_LOGD("Create logical device map");
        createDeviceMap();
    }

    return mDeviceSensorMap.size();
}

std::vector<MINT32>
HalLogicalDeviceList::
getSensorId(MINT32 deviceId)
{
    Mutex::Autolock _l(mLock);
    if (mDeviceSensorMap.size() == 0)
    {
        MY_LOGE("[getSensorId] mDeviceSensorMap.size() == 0");
    }
    sp<CamDeviceInfo> info = mDeviceSensorMap.valueFor(deviceId);

    return info->Sensors;
}

MINT32
HalLogicalDeviceList::
getDeviceId(MINT32 sensorId)
{
    Mutex::Autolock _l(mLock);
    if (mDeviceSensorMap.size() == 0)
    {
        MY_LOGE("[getDeviceId] mDeviceSensorMap.size() == 0");
    }
    for (unsigned int i = 0; i < mDeviceSensorMap.size(); i++)
    {
        sp<CamDeviceInfo> info = mDeviceSensorMap.valueAt(i);
        if (info->Sensors.size() == 1 && info->Sensors[0] == sensorId)
        {
            return i;
        }
    }

    return -1;
}

MINT32
HalLogicalDeviceList::
getSupportedFeature(
    MINT32 deviceId
) const
{
    Mutex::Autolock _l(mLock);
    if (mDeviceSensorMap.size() == 0)
    {
        MY_LOGE("[getDeviceId] mDeviceSensorMap.size() == 0");
    }
    sp<CamDeviceInfo> info = mDeviceSensorMap.valueFor(deviceId);
    return info->DualFeature;
}

SensorSyncType
HalLogicalDeviceList::
getSyncType(
    MINT32 deviceId
) const
{
    Mutex::Autolock _l(mLock);
    sp<CamDeviceInfo> info = mDeviceSensorMap.valueFor(deviceId);
    return info->syncTypeInfo->syncType;
}

MINT32
HalLogicalDeviceList::
getSensorSyncMasterDevId(
    MINT32 deviceId
) const
{
    Mutex::Autolock _l(mLock);
    sp<CamDeviceInfo> info = mDeviceSensorMap.valueFor(deviceId);
    return info->syncTypeInfo->masterDevId;
}

bool
HalLogicalDeviceList::
updateMultiCamSensorMetadata(
    MINT32 sensorId,
    IMetadata &metadata
)
{
    SensorStaticInfo pSensorStaticInfo;
    // get sensor metadata
    auto sensor_metadata = MAKE_HalSensorList()->queryStaticInfo(sensorId);
    auto entry = sensor_metadata.entryFor(MTK_MULTI_CAM_FEATURE_SENSOR_MANUAL_UPDATED);
    if(entry.isEmpty())
    {
        MY_LOGE("cannot get MTK_MULTI_CAM_FEATURE_SENSOR_MANUAL_UPDATED");
        return false;
    }
    auto tagToString = [](MINT64 tag)
    {
        switch((MUINT32)tag)
        {
            case MTK_SENSOR_INFO_ACTIVE_ARRAY_REGION: return "MTK_SENSOR_INFO_ACTIVE_ARRAY_REGION";
            case MTK_SENSOR_INFO_PRE_CORRECTION_ACTIVE_ARRAY_SIZE: return "MTK_SENSOR_INFO_PRE_CORRECTION_ACTIVE_ARRAY_SIZE";
            case MTK_SENSOR_INFO_PIXEL_ARRAY_SIZE: return "MTK_SENSOR_INFO_PIXEL_ARRAY_SIZE";
            case MTK_SENSOR_INFO_COLOR_FILTER_ARRANGEMENT: return "MTK_SENSOR_INFO_COLOR_FILTER_ARRANGEMENT";
            case MTK_SENSOR_INFO_ORIENTATION: return "MTK_SENSOR_INFO_ORIENTATION";
            case MTK_SENSOR_INFO_WANTED_ORIENTATION: return "MTK_SENSOR_INFO_WANTED_ORIENTATION";
            default: return "not defined";
        }
    };
    MINT64 tagVal = 0;
    for(MUINT i=0;i<entry.count();++i)
    {
        tagVal = entry.itemAt(i, Type2Type<MINT64>());
        MY_LOGD("update sensor manual tag (%s)", tagToString(tagVal));
        // get value from physical sensor metadata
        auto sensorMetadataEntry = sensor_metadata.entryFor((MUINT32)tagVal);
        if(sensorMetadataEntry.isEmpty())
        {
            MY_LOGE("%s is not exist in sensor metadata", tagToString(tagVal));
        }
        else
        {
            metadata.update(tagVal, sensorMetadataEntry);
        }
    }
    return true;
}