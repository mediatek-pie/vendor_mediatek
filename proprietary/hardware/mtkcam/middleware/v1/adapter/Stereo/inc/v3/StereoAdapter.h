/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_HAL_CAMADAPTER_MTKSTEREO_INC_MTKSTEREOCAMADAPTER_H_
#define _MTK_HAL_CAMADAPTER_MTKSTEREO_INC_MTKSTEREOCAMADAPTER_H_
//
/*******************************************************************************
*
*******************************************************************************/

#include <inc/IState.h>
using namespace NSCamState;
//
#include "inc/v3/CaptureCmdQueThread.h"
#include "inc/v3/ZipImageCallbackThread.h"
#include <mtkcam/middleware/v1/camutils/FrameworkCBThread.h>
//
#include <mtkcam/middleware/v1/IShot.h>
using namespace NSShot;
//
#include <vector>
using namespace std;
//
#include "EngParam.h"
//
#include <mtkcam/middleware/v1/Scenario/IFlowControl.h>
#include <mtkcam/middleware/v1/LegacyPipeline/processor/StreamingProcessor.h>
//
#include <mtkcam/aaa/IHal3A.h>
using namespace NS3Av3;
//
#include <mtkcam/utils/metastore/IMetadataProvider.h>
//
namespace android {
namespace NSStereoAdapter {
//
class CamAdapter : public BaseCamAdapter
                 , public IStateHandler
                 , public ICaptureCmdQueThreadHandler
                 , public IShotCallback
                 , public NSCam::v1::INotifyCallback
{
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  ICamAdapter Interfaces.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:
    // preview
    virtual bool                    init();

    virtual bool                    uninit();

    virtual status_t                startPreview();

    virtual void                    stopPreview();

    virtual bool                    previewEnabled() const;

    // record
    virtual status_t                startRecording();

    virtual void                    stopRecording();

    virtual bool                    recordingEnabled() const;

    // AF
    virtual status_t                autoFocus();

    virtual status_t                cancelAutoFocus();

    // take picture
    virtual bool                    isTakingPicture() const;

    virtual status_t                takePicture();

    virtual status_t                cancelPicture();

    virtual status_t                setCShotSpeed(int32_t i4CShotSpeeed);

    // send command
    virtual status_t                setParameters();

    virtual status_t                sendCommand(int32_t cmd, int32_t arg1, int32_t arg2);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  IStateHandler Interfaces.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////
    virtual status_t                onHandleStartPreview();
    virtual status_t                onHandleStopPreview();
    //
    virtual status_t                onHandlePreCapture();
    virtual status_t                onHandleNormalCapture();
    virtual status_t                onHandleCaptureDone();
    virtual status_t                onHandleCancelCapture();
    //
    virtual status_t                onHandleZSLCapture();

    virtual status_t                onHandleCancelPreviewCapture();
    //
    virtual status_t                onHandleStartRecording();
    virtual status_t                onHandleStopRecording();
    virtual status_t                onHandleVideoSnapshot();

//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    virtual status_t                onHandleCancelVideoSnapshot();
    virtual status_t                onHandleCancelNormalCapture();
    virtual status_t                onHandleCancelZSLCapture();
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    virtual status_t                onHandleNormalCaptureDone();
    virtual status_t                onHandleVideoSnapshotDone();
    virtual status_t                onHandleZSLCaptureDone();

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  ICaptureCmdQueThreadHandler Interfaces.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////
    virtual bool                    onCaptureThreadLoop();

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  IShotCallback Interfaces.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////
    //  Directly include this inline file to reduce file dependencies since the
    //  interfaces in this file may often vary.
    #include "inc/v3/ImpShotCallback.inl"

private:
    //  [Focus Callback]
    status_t                        init3A();
    void                            uninit3A();
    void                            enableAFMove(bool flag);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  INotifyCallback Interfaces.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:
    virtual void                    doNotifyCallback(
                                        int32_t _msgType,
                                        int32_t _ext1,
                                        int32_t _ext2
                                    );

    virtual void                    doDataCallback(
                                        int32_t  _msgType,
                                        void*    _data,
                                        uint32_t _size
                                    );
    virtual void                    doExtCallback(
                                        int32_t _msgType,
                                        int32_t _ext1,
                                        int32_t _ext2
                                    );

    virtual bool                    msgTypeEnabled( int32_t msgType );

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////                    Instantiation.

    virtual                         ~CamAdapter();
                                    CamAdapter(
                                        String8 const&      rAppMode,
                                        int32_t const       i4OpenId,
                                        sp<IParamsManagerV3>  pParamsMgrV3
                                    );

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Implementation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
protected:  ////

    bool                            updateShotInstance();
    status_t                        prepareToCapture();
    status_t                        forceStopAndCleanPreview();
    status_t                        enableFlashCalibration(int enable);
    int                             getFlashQuickCalibrationResult();
    bool                            ProcessCBMetaData(MUINTPTR const pMeta, ZipImageCallbackThread::callback_type const cbType);

protected:  ////

    MVOID                           dumpTuningParams();

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Helper function
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
protected:
    int                             getSensorScenario();
    bool                           isSupportVideoSnapshot();


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Implementation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
protected:  ////                    Data Members.
    //
    IStateManager*                  mpStateManager          = nullptr;
    //
    sp<ICaptureCmdQueThread>        mpCaptureCmdQueThread   = nullptr;
    wp<ZipImageCallbackThread>      mpZipCallbackThread     = nullptr;
    sp<IFrameworkCBThread>          mpFrameworkCBThread     = nullptr;
    //
    String8                         msAppMode;

    //
    bool                            mbTakePicPrvNotStop     = false;
    bool                            mbFixFps                = false;
    uint32_t                        mPreviewMaxFps          = 0;
    uint32_t                        mShotMode               = 0;
    //
    int32_t                         mLastVdoWidth           = 0;
    int32_t                         mLastVdoHeight          = 0;
    //
    EngParam                        mEngParam;
    //
    MUINT32                         mLogLevel               = 0;
    //
    sp<IFlowControl>                mpFlowControl           = nullptr;
    // af region, for capture use(temp work run solution)
    String8                         msFocusArea;
    MINT32                          miMaxFocusArea          = 0;
    //
    MBOOL                           mIsRaw16CBEnable        = MFALSE;

    MUINT32                         mFlowControlType = 0;
    MBOOL                           mbFlashOn = MFALSE;
    MBOOL                           mOldFlashSetting = MFALSE;
    MBOOL                           mbCancelAF = MFALSE;
};


};  // namespace NSMtkStereoCamAdapter
};  // namespace android
#endif  //_MTK_HAL_CAMADAPTER_MTKSTEREO_INC_MTKSTEREOCAMADAPTER_H_

