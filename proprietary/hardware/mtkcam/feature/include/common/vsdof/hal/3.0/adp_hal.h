/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
 *     TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#ifndef _ADP_HAL_H_
#define _ADP_HAL_H_

#include <mtkcam/feature/stereo/hal/stereo_common.h>
#include <vector>
#include <mtkcam/utils/TuningUtils/FileDumpNamingRule.h>

namespace StereoHAL {

struct ADP_HAL_INIT_PARAM
{
    bool isSecurity = false;
};

struct ADP_HAL_IN_DATA
{
    NSCam::IImageBuffer *dispMapL = NULL;
    NSCam::IImageBuffer *dispMapR = NULL;
    NSCam::IImageBuffer *confidenceMap = NULL;

    float focalLensFactor = 0.0f;   //from N3D

    NSCam::TuningUtils::FILE_DUMP_NAMING_HINT *dumpHint = NULL;
};

struct ADP_HAL_OUT_DATA
{
    NSCam::IImageBuffer *depthmap;
};

class ADP_HAL
{
public:
    /**
     * \brief Create a new ADP instance(not singleton)
     * \details Callers should delete the instance by themselves:
     *          ADP_HAL_INIT_PARAM initParam;
     *          ADP_HAL *ADPHal = ADP_HAL::createInstance(initParam);
     *          ...
     *          if(ADPHal) delete ADPHal;
     *
     * \return Pointer to ADP HAL instance(not singleton)
     */
    static ADP_HAL *createInstance(ADP_HAL_INIT_PARAM &initParam);

    /**
     * \brief Default destructor
     * \details Callers should delete the instance by themselves
     */
    virtual ~ADP_HAL() {}

    /**
     * \brief Run ADP
     * \details Run ADP algorithm and get result
     *
     * \param inData Input data
     * \param outData Output data
     *
     * \return True if success
     */
    virtual bool ADPHALRun(ADP_HAL_IN_DATA &inData, ADP_HAL_OUT_DATA &outData) = 0;
    //
protected:
    ADP_HAL() {}
};

};  //namespace StereoHAL
#endif