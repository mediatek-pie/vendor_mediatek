/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

 // Standard C header file

// Android system/core header file

// mtkcam custom header file
// mtkcam global header file

// Module header file
#include <stereo_tuning_provider.h>
// Local header file
#include "ADPNode.h"
#include "WPENode.h"
#include "../DepthMapPipe_Common.h"
#include "./bufferPoolMgr/BaseBufferHandler.h"

// Logging
#undef PIPE_CLASS_TAG
#define PIPE_CLASS_TAG "WPENode"
#include <featurePipe/core/include/PipeLog.h>

/*******************************************************************************
* Namespace start.
********************************************************************************/
namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe_DepthMap {

using namespace NSCam::NSIoPipe::NSPostProc;
using namespace NSCam::NSIoPipe;

#define WPENODE_USERNAME "VSDOF_WPE"

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Instantiation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
WPENode::
WPENode(
    const char *name,
    DepthMapPipeNodeID nodeID,
    PipeNodeConfigs config
)
: DepthMapPipeNode(name, nodeID, config)
, miSensorIdx_Main1(config.mpSetting->miSensorIdx_Main1)
{
    this->addWaitQueue(&mJobQueue);
}

WPENode::
~WPENode()
{
    MY_LOGD("[Destructor]");
}

MVOID
WPENode::
cleanUp()
{
    MY_LOGD("+");
    if(mpINormalStream != NULL)
    {
        mpINormalStream->uninit(WPENODE_USERNAME);
        mpINormalStream->destroyInstance();
        mpINormalStream = NULL;
    }

    MY_LOGD("-");
}

MBOOL
WPENode::
onInit()
{
    VSDOF_INIT_LOG("+");
    VSDOF_INIT_LOG("-");
    return MTRUE;
}

MBOOL
WPENode::
onUninit()
{
    VSDOF_INIT_LOG("+");
    VSDOF_INIT_LOG("-");
    return MTRUE;
}

MBOOL
WPENode::
onThreadStart()
{
    CAM_TRACE_NAME("WPENode::onThreadStart");
    VSDOF_INIT_LOG("+");
    // Create NormalStream, 0xFFF for WPE
    VSDOF_LOGD("NormalStream create instance");
    CAM_TRACE_BEGIN("WPENode::NormalStream::createInstance+init");
    mpINormalStream = NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(miSensorIdx_Main1);

    if (mpINormalStream == NULL ||
        !mpINormalStream->init(WPENODE_USERNAME, NSCam::NSIoPipe::EStreamPipeID_WarpEG))
    {
        MY_LOGE("mpINormalStream create instance for WPE Node failed!");
        cleanUp();
        return MFALSE;
    }
    CAM_TRACE_END();
    //
    mpMTKADP = MTKADP::createInstance();
    //
    VSDOF_INIT_LOG("-");
    return MTRUE;
}

MBOOL
WPENode::
onThreadStop()
{
    CAM_TRACE_NAME("WPENode::onThreadStop");
    VSDOF_INIT_LOG("+");
    cleanUp();
    VSDOF_INIT_LOG("-");
    return MTRUE;
}

MBOOL
WPENode::
onData(DataID data, DepthMapRequestPtr &request)
{
  MBOOL ret = MTRUE;
  VSDOF_LOGD("+ : reqID=%d", request->getRequestNo());
  CAM_TRACE_NAME("WPENode::onData");

  switch(data)
  {
  case N3D_TO_WPE_IMG_MASK:
    mJobQueue.enque(request);
    break;
  default:
    MY_LOGW("Un-recognized data ID, id=%d reqID=%d", data, request->getRequestNo());
    ret = MFALSE;
    break;
  }

  VSDOF_LOGD("-");
  return ret;
}

MBOOL
WPENode::
onThreadLoop()
{
    DepthMapRequestPtr pRequest;

    if( !waitAllQueue() )
    {
        return MFALSE;
    }

    if( !mJobQueue.deque(pRequest) )
    {
        MY_LOGE("mJobQueue.deque() failed");
        return MFALSE;
    }

    CAM_TRACE_NAME("WPENode::onThreadLoop");
    // mark on-going-pRequest start
    this->incExtThreadDependency();
    const EffectRequestAttrs& attr = pRequest->getRequestAttr();
    VSDOF_PRFLOG("threadLoop start, reqID=%d eState=%d needFEFM=%d",
                    pRequest->getRequestNo(), attr.opState, attr.needFEFM);
    //
    MBOOL bRet = MFALSE;
    EnqueCookieContainer *pCookieIns = new EnqueCookieContainer(pRequest, this);
    QParams enqueParams;
    if(!prepareWPEEnqueParams(pRequest, enqueParams, pCookieIns))
        goto lbExit;
    // callback
    enqueParams.mpfnCallback = onWPECallback;
    enqueParams.mpfnEnQFailCallback = onWPEFailedCallback;
    enqueParams.mpCookie = (MVOID*) pCookieIns;
    // enque
    pRequest->mTimer.startWPE();
    pRequest->mTimer.startWPEEnque();
    CAM_TRACE_BEGIN("WPENode::NormalStream::enque");
    VSDOF_LOGD("WPE enque start! reqID=%d", pRequest->getRequestNo());
    bRet = mpINormalStream->enque(enqueParams);
    CAM_TRACE_END();
    // stop P2A Enque timer
    pRequest->mTimer.stopWPEEnque();
    VSDOF_PRFTIME_LOG("WPE enque end! reqID=%d, exec-time(enque)=%d msec",
                    pRequest->getRequestNo(), pRequest->mTimer.getElapsedWPEEnque());
    if(!bRet)
    {
        MY_LOGE("WPE enque failed! reqID=%d", pRequest->getRequestNo());
        goto lbExit;
    }
    return MTRUE;

lbExit:
    delete pCookieIns;
    // mark on-going-request end
    this->decExtThreadDependency();
    return MFALSE;

}

MBOOL
WPENode::
setBorderMask(
    MBOOL bIsInput,
    IImageBuffer* pImgBuf
)
{
    MUINT32 top;
    MUINT32 left;
    MUINT32 bottom;
    MUINT32 right;

    if(bIsInput && !StereoTuningProvider::getWPEInputMargin(top, left, bottom, right))
        return MFALSE;
    else if(!bIsInput && !StereoTuningProvider::getDPEInputMargin(top, left, bottom, right))
        return MFALSE;

    VSDOF_LOGD("top=%d left=%d bottom=%d right=%d", top, left, bottom, right);

    ADP_UTIL_MEM_RESET_T config;
    config.pcBuffer = (MINT8*)pImgBuf->getBufVA(0);
    config.u4Width = pImgBuf->getImgSize().w;
    config.u4Height = pImgBuf->getImgSize().h;
    config.u2BorderU = top;
    config.u2BorderD = bottom;
    config.u2BorderL = left;
    config.u2BorderR = right;
    config.ucBorderValue = 0; //black dark
    config.bBorderOnly = !bIsInput; // for input, need content clear
    if(pImgBuf->getImgFormat() == eImgFmt_NV12)
        config.u4InputImgFormat = ADP_INPUT_IMAGE_GRAY;
    else
        config.u4InputImgFormat = ADP_INPUT_IMAGE_YUY2;
    if(bIsInput)
        config.ucResetValue = 255; // white color

    auto res = mpMTKADP->FeatureCtrl(ADP_FEATURE_RESET_MEM, &config, NULL);
    if(res!=S_ADP_OK)
    {
        MY_LOGE("Failed to set Border.");
        return MFALSE;
    }
    else
        return MTRUE;
}

MBOOL
WPENode::
prepareWPEEnqueParams(
    DepthMapRequestPtr& pRequest,
    QParams& rQParam,
    EnqueCookieContainer* pEnqueContainer
)
{
    VSDOF_LOGD("prepareWPEEnqueParams reqID=%d", pRequest->getRequestNo());
    auto clearYUY2ToWhite = [](IImageBuffer* pImgBuf)
    {
        if(pImgBuf->getImgFormat() != eImgFmt_YUY2)
            return MFALSE;

        MUINT8 *pData = (MUINT8*)pImgBuf->getBufVA(0);
        MUINT32 iSize = pImgBuf->getBufSizeInBytes(0);
        MUINT32 iCount = 0;
        while(iCount<iSize)
        {
            // Y set 255
            if(iCount%2==0)
            {
                (*pData) = 255;
            }
            //UV set 128
            else
            {
                (*pData) = 128;
            }
            pData++;
            iCount++;
        }
        return MTRUE;
    };
    //
    sp<BaseBufferHandler> pBufferHandler = pRequest->getBufferHandler();

    IImageBuffer* pImgBuf_RectIn1 = nullptr;
    IImageBuffer* pImgBuf_RectIn2 = nullptr;
    IImageBuffer* pImgBuf_WarpMtx_Main1 = nullptr;
    IImageBuffer* pImgBuf_WarpMtx_Main2 = nullptr;

    if(!pBufferHandler->getEnqueBuffer(getNodeId(), BID_P2A_OUT_RECT_IN1, pImgBuf_RectIn1) ||
       !pBufferHandler->getEnqueBuffer(getNodeId(), BID_P2A_OUT_RECT_IN2, pImgBuf_RectIn2) ||
       !pBufferHandler->getEnqueBuffer(getNodeId(), BID_N3D_OUT_WARPING_MATRIX_MAIN1, pImgBuf_WarpMtx_Main1) ||
       !pBufferHandler->getEnqueBuffer(getNodeId(), BID_N3D_OUT_WARPING_MATRIX_MAIN2, pImgBuf_WarpMtx_Main2))
    {
        return MFALSE;
    }
    // warp input mask
    IImageBuffer* pImgBuf_DefaultMask1 = pBufferHandler->requestWorkingBuffer(BID_WPE_IN_MASK);
    IImageBuffer* pImgBuf_DefaultMask2 = pBufferHandler->requestWorkingBuffer(BID_WPE_IN_MASK);
    //
    if(!this->setBorderMask(MTRUE, pImgBuf_DefaultMask1) ||
        !this->setBorderMask(MTRUE, pImgBuf_DefaultMask2)
    )
    {
        MY_LOGE("Failed to config border.");
        return MFALSE;
    }
    //warp output
    IImageBuffer* pImgBuf_MV_Y = pBufferHandler->requestBuffer(getNodeId(), BID_WPE_OUT_MV_Y);
    IImageBuffer* pImgBuf_MASK_M = pBufferHandler->requestBuffer(getNodeId(), BID_WPE_OUT_MASK_M);
    IImageBuffer* pImgBuf_SV_Y = pBufferHandler->requestBuffer(getNodeId(), BID_WPE_OUT_SV_Y);
    IImageBuffer* pImgBuf_MASK_S = pBufferHandler->requestBuffer(getNodeId(), BID_WPE_OUT_MASK_S);
    //
    MBOOL bRet = MTRUE;
    // Frame 1: MV_Y
    bRet &= prepareImgWarpParams(pImgBuf_RectIn1, pImgBuf_WarpMtx_Main1, pImgBuf_MV_Y, rQParam, pEnqueContainer);
    // Frame 2:  SV_Y
    bRet &= prepareImgWarpParams(pImgBuf_RectIn2, pImgBuf_WarpMtx_Main2, pImgBuf_SV_Y, rQParam, pEnqueContainer);
    // Frame 3: BID_WPE_OUT_MASK_M
    bRet &= prepareImgWarpParams(pImgBuf_DefaultMask1, pImgBuf_WarpMtx_Main1, pImgBuf_MASK_M, rQParam, pEnqueContainer);
    // Frame 4: BID_WPE_OUT_MASK_S
    bRet &= prepareImgWarpParams(pImgBuf_DefaultMask2, pImgBuf_WarpMtx_Main2, pImgBuf_MASK_S, rQParam, pEnqueContainer);

    VSDOF_LOGD("pImgBuf_RectIn1=%dx%d pImgBuf_RectIn2=%dx%d",
                pImgBuf_RectIn1->getImgSize().w, pImgBuf_RectIn1->getImgSize().h,
                pImgBuf_RectIn2->getImgSize().w, pImgBuf_RectIn2->getImgSize().h);
    VSDOF_LOGD("pImgBuf_DefaultMask1=%dx%d pImgBuf_DefaultMask2=%dx%d",
                pImgBuf_DefaultMask1->getImgSize().w, pImgBuf_DefaultMask1->getImgSize().h,
                pImgBuf_DefaultMask2->getImgSize().w, pImgBuf_DefaultMask2->getImgSize().h);
    VSDOF_LOGD("pImgBuf_MV_Y=%dx%d pImgBuf_MASK_M=%dx%d",
                pImgBuf_MV_Y->getImgSize().w, pImgBuf_MV_Y->getImgSize().h,
                pImgBuf_MASK_M->getImgSize().w, pImgBuf_MASK_M->getImgSize().h);

    VSDOF_LOGD("pImgBuf_SV_Y=%dx%d pImgBuf_MASK_S=%dx%d",
                pImgBuf_SV_Y->getImgSize().w, pImgBuf_SV_Y->getImgSize().h,
                pImgBuf_MASK_S->getImgSize().w, pImgBuf_MASK_S->getImgSize().h);

    VSDOF_LOGD("pImgBuf_MASK_M Y plane VA=%x PA=%x, UV plane VA=%x PA=%x, plane count=%d",
                pImgBuf_MASK_M->getBufVA(0), pImgBuf_MASK_M->getBufPA(0),
                pImgBuf_MASK_M->getBufVA(1), pImgBuf_MASK_M->getBufPA(1), pImgBuf_MASK_M->getPlaneCount());
    VSDOF_LOGD("pImgBuf_MASK_S Y plane VA=%x PA=%x, UV plane VA=%x PA=%x, plane count=%d",
                pImgBuf_MASK_S->getBufVA(0), pImgBuf_MASK_S->getBufPA(0),
                pImgBuf_MASK_S->getBufVA(1), pImgBuf_MASK_S->getBufPA(1), pImgBuf_MASK_S->getPlaneCount());
    if(!bRet)
    {
        MY_LOGE("Failed to prepare the WPE param data!");
    }
    return bRet;
}

MBOOL
WPENode::
prepareImgWarpParams(
    IImageBuffer* pImgBuffer,
    IImageBuffer* pWarpMtx,
    IImageBuffer* pOutputBuffer,
    QParams& rQParam,
    EnqueCookieContainer* pEnqueContainer
)
{
    if(pImgBuffer == nullptr ||
        pWarpMtx == nullptr ||
        pOutputBuffer == nullptr)
    {
        MY_LOGE("Buffer is null!!!pImgBuffer=%x, pWarpMtx=%x, pOutputBuffer=%d",
                pImgBuffer, pWarpMtx, pOutputBuffer);
        return MFALSE;
    }
    FrameParams frameParam;
    // prepare input
    Input input;
    input.mPortID = PORT_WPEI;
    input.mBuffer = pImgBuffer;
    frameParam.mvIn.push_back(input);
    // prepare output
    Output output;
    output.mPortID = PORT_WPEO;
    output.mBuffer = pOutputBuffer;
    frameParam.mvOut.push_back(output);
    // prepare WPEParam
    WPEQParams* pWpeParam = new WPEQParams();
    prepareWPEQParam(pImgBuffer, pWarpMtx, *pWpeParam);
    pEnqueContainer->mvData.push_back((void*)pWpeParam);
    // extra param
    ExtraParam exParams;
    exParams.CmdIdx = EPIPE_WPE_INFO_CMD;
    exParams.moduleStruct = (void*) pWpeParam;
    frameParam.mvExtraParam.push_back(exParams);
    // insert frame to QParam
    rQParam.mvFrameParams.push_back(frameParam);

    return MTRUE;
}

MVOID
WPENode::
prepareWPEQParam(
    IImageBuffer* pInputImg,
    IImageBuffer* pWarpMatrix,
    WPEQParams& rWPEQParam
)
{
    // standalone mode
    rWPEQParam.wpe_mode = WPE_MODE_WPEO;
    // no z plane
    rWPEQParam.vgen_hmg_mode = 0;
    // plane 0, x
    rWPEQParam.warp_veci_info.width = pWarpMatrix->getImgSize().w;
    rWPEQParam.warp_veci_info.height = pWarpMatrix->getImgSize().h;
    rWPEQParam.warp_veci_info.stride = pWarpMatrix->getBufStridesInBytes(0);
    rWPEQParam.warp_veci_info.virtAddr = pWarpMatrix->getBufVA(0);
    rWPEQParam.warp_veci_info.phyAddr = pWarpMatrix->getBufPA(0);
    rWPEQParam.warp_veci_info.bus_size = WPE_BUS_SIZE_32_BITS;
    rWPEQParam.warp_veci_info.addr_offset = 0;
    rWPEQParam.warp_veci_info.veci_v_flip_en = 0;
    // plane 1, y
    rWPEQParam.warp_vec2i_info.width = pWarpMatrix->getImgSize().w;
    rWPEQParam.warp_vec2i_info.height = pWarpMatrix->getImgSize().h;
    rWPEQParam.warp_vec2i_info.stride = pWarpMatrix->getBufStridesInBytes(1);
    rWPEQParam.warp_vec2i_info.virtAddr = pWarpMatrix->getBufVA(1);
    rWPEQParam.warp_vec2i_info.phyAddr = pWarpMatrix->getBufPA(1);
    rWPEQParam.warp_vec2i_info.bus_size = WPE_BUS_SIZE_32_BITS;
    rWPEQParam.warp_vec2i_info.addr_offset = 0;
    rWPEQParam.warp_vec2i_info.veci_v_flip_en = 0;
    //
    rWPEQParam.wpecropinfo.x_start_point = 0;
    rWPEQParam.wpecropinfo.y_start_point = 0;
    rWPEQParam.wpecropinfo.x_end_point = pInputImg->getImgSize().w-1;
    rWPEQParam.wpecropinfo.y_end_point = pInputImg->getImgSize().h-1;
    // vgen crop
    MCrpRsInfo vgenCropInfo;
    vgenCropInfo.mCropRect = MCropRect(MPoint(0,0), pWarpMatrix->getImgSize());
    rWPEQParam.mwVgenCropInfo.push_back(vgenCropInfo);
    // tuning parameters
    rWPEQParam.tbl_sel_v = 24;
    rWPEQParam.tbl_sel_h = 24;
    //
    auto logWPEParam = [](WarpMatrixInfo& veciInfo)
    {
        VSDOF_LOGD("warp_veci_info: width=%d height=%d stride=%d virtAddr=%x phyAddr=%x bus_size=%d offset=%d flip=%d",
                    veciInfo.width, veciInfo.height, veciInfo.stride, veciInfo.virtAddr, veciInfo.phyAddr,
                    veciInfo.bus_size, veciInfo.addr_offset, veciInfo.veci_v_flip_en);
    };

    logWPEParam(rWPEQParam.warp_veci_info);
    logWPEParam(rWPEQParam.warp_vec2i_info);
}

MVOID
WPENode::
onWPECallback(QParams& rParams)
{
    EnqueCookieContainer* pEnqueData = (EnqueCookieContainer*) (rParams.mpCookie);
    WPENode* pWPENode = (WPENode*) (pEnqueData->mpNode);
    pWPENode->handleWPEDone(rParams, pEnqueData);
}

MVOID
WPENode::
onWPEFailedCallback(QParams& rParams)
{
    MY_LOGD("+");
    EnqueCookieContainer* pEnqueData = (EnqueCookieContainer*) (rParams.mpCookie);
    WPENode* pWPENode = (WPENode*) (pEnqueData->mpNode);
    MUINT32 iReqNo = pEnqueData->mRequest->getRequestNo();
    //
    MY_LOGE("reqID=%d WPE operations failed!!Check the following log:", iReqNo);
    debugQParams(rParams);
    pWPENode->handleData(ERROR_OCCUR_NOTIFY, pEnqueData->mRequest);
    for(void* data : pEnqueData->mvData )
    {
        WPEQParams* pData = reinterpret_cast<WPEQParams*>(data);
        delete pData;
    }
    delete pEnqueData;
    // launch onProcessDone
    pEnqueData->mRequest->getBufferHandler()->onProcessDone(pWPENode->getNodeId());
    // mark on-going-request end
    pWPENode->decExtThreadDependency();
    MY_LOGD("-");
}

MVOID
WPENode::
handleWPEDone(QParams& rParams, EnqueCookieContainer* pEnqueCookie)
{
    CAM_TRACE_NAME("WPENode::handleP2Done");
    DepthMapRequestPtr pRequest = pEnqueCookie->mRequest;
    DumpConfig config;
    // check flush status
    if(mpNodeSignal->getStatus(NodeSignal::STATUS_IN_FLUSH))
        goto lbExit;
    // stop timer
    pRequest->mTimer.stopWPE();
    VSDOF_PRFTIME_LOG("+ :reqID=%d , WPE exec-time=%d ms", pRequest->getRequestNo(), pRequest->mTimer.getElapsedWPE());
    //
    this->configureToNext(pRequest);
lbExit:
    // launch onProcessDone
    pRequest->getBufferHandler()->onProcessDone(getNodeId());
    for(void* data : pEnqueCookie->mvData )
    {
        WPEQParams* pData = reinterpret_cast<WPEQParams*>(data);
        delete pData;
    }
    delete pEnqueCookie;
    VSDOF_LOGD("- :reqID=%d", pRequest->getRequestNo());
    // mark on-going-request end
    this->decExtThreadDependency();
}

MBOOL
WPENode::
configureToNext(DepthMapRequestPtr pRequest)
{
    VSDOF_LOGD("+, reqID=%d", pRequest->getRequestNo());
    sp<BaseBufferHandler> pBufferHandler = pRequest->getBufferHandler();
    //
    IImageBuffer *pImgBuf_MV_Y = nullptr, *pImgBuf_SV_Y = nullptr;
    IImageBuffer *pImgBuf_MASK_M = nullptr , *pImgBuf_MASK_S = nullptr;
    //
    MBOOL bRet = pBufferHandler->getEnqueBuffer(getNodeId(), BID_WPE_OUT_MV_Y, pImgBuf_MV_Y);
    bRet &= pBufferHandler->getEnqueBuffer(getNodeId(), BID_WPE_OUT_SV_Y, pImgBuf_SV_Y);
    bRet &= pBufferHandler->getEnqueBuffer(getNodeId(), BID_WPE_OUT_MASK_M, pImgBuf_MASK_M);
    bRet &= pBufferHandler->getEnqueBuffer(getNodeId(), BID_WPE_OUT_MASK_S, pImgBuf_MASK_S);
    //
    if(!this->setBorderMask(MFALSE, pImgBuf_MASK_M) ||
        !this->setBorderMask(MFALSE, pImgBuf_MASK_S)
    )
    {
        MY_LOGE("Failed to config border.");
        return MFALSE;
    }
    pImgBuf_SV_Y->syncCache(eCACHECTRL_INVALID);
    pImgBuf_MV_Y->syncCache(eCACHECTRL_INVALID);
    pImgBuf_MASK_M->syncCache(eCACHECTRL_INVALID);
    pImgBuf_MASK_S->syncCache(eCACHECTRL_INVALID);
    // set output buffer
    pBufferHandler->configOutBuffer(getNodeId(), BID_WPE_OUT_MV_Y, eDPETHMAP_PIPE_NODEID_DPE);
    pBufferHandler->configOutBuffer(getNodeId(), BID_WPE_OUT_MASK_M, eDPETHMAP_PIPE_NODEID_DPE);
    pBufferHandler->configOutBuffer(getNodeId(), BID_WPE_OUT_SV_Y, eDPETHMAP_PIPE_NODEID_DPE);
    pBufferHandler->configOutBuffer(getNodeId(), BID_WPE_OUT_MASK_S, eDPETHMAP_PIPE_NODEID_DPE);
    //
    this->handleDataAndDump(WPE_TO_DPE_WARP_IMG, pRequest);
    VSDOF_LOGD("-, reqID=%d", pRequest->getRequestNo());
    return MTRUE;
}

/******************************************************************************
 *
 ******************************************************************************/
MVOID
WPENode::
onFlush()
{
    MY_LOGD("+ extDep=%d", this->getExtThreadDependency());
    DepthMapRequestPtr pRequest;
    while( mJobQueue.deque(pRequest) )
    {
        sp<BaseBufferHandler> pBufferHandler = pRequest->getBufferHandler();
        pBufferHandler->onProcessDone(getNodeId());
    }
    DepthMapPipeNode::onFlush();
    MY_LOGD("-");
}

}; //NSFeaturePipe_DepthMap
}; //NSCamFeature
}; //NSCam

