/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

 /**
 * @file DepthMapFlowOption_ActiveStereo.cpp
 * @brief DepthMapFlowOption for bayer+mono VSDOF
 */

// Standard C header file

// Android system/core header file

// mtkcam custom header file
#include <camera_custom_stereo.h>
// mtkcam global header file
#include <mtkcam/def/common.h>
#include <mtkcam/drv/iopipe/PostProc/INormalStream.h>
#include <mtkcam/drv/iopipe/PostProc/IHalPostProcPipe.h>
#include <mtkcam/feature/stereo/hal/stereo_setting_provider.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
#include <isp_tuning.h>
// Module header file
#include <stereo_tuning_provider.h>
#include <fefm_setting_provider.h>
// Local header file
#include "DepthMapFlowOption_ActiveStereo.h"
#include "../DepthMapPipe.h"
#include "../DepthMapPipeNode.h"
#include "./bufferPoolMgr/BaseBufferHandler.h"

// logging
#undef PIPE_CLASS_TAG
#define PIPE_MODULE_TAG "DepthMapPipe"
#define PIPE_CLASS_TAG "DepthMapFlowOption_ActiveStereo"
#include <featurePipe/core/include/PipeLog.h>


/*******************************************************************************
* Namespace start.
********************************************************************************/
namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe_DepthMap {


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Instantiation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
DepthMapFlowOption_ActiveStereo::
DepthMapFlowOption_ActiveStereo(
    sp<DepthMapPipeSetting> pSetting,
    sp<DepthMapPipeOption> pOption,
    sp<DepthInfoStorage> pStorage
)
: ActiveDepthQTemplateProvider(pSetting, pOption, this)
, mpPipeOption(pOption)
{
    mpDepthStorage = pStorage;
    //
    mpSizeMgr = new NodeBufferSizeMgr(mpPipeOption);
    // flow option config
    const P2ABufferSize& P2ASize = mpSizeMgr->getP2A(eSTEREO_SCENARIO_RECORD);
    mConfig.mbCaptureFDEnable = MTRUE;
    mConfig.mFDSize = P2ASize.mFD_IMG_SIZE;
}

DepthMapFlowOption_ActiveStereo::
~DepthMapFlowOption_ActiveStereo()
{
    MY_LOGD("[Destructor] +");
    if(mpSizeMgr != nullptr)
        delete mpSizeMgr;
    MY_LOGD("[Destructor] -");
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  P2AFlowOption Public Operations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


MBOOL
DepthMapFlowOption_ActiveStereo::
buildQParam(
    DepthMapRequestPtr pRequest,
    const Stereo3ATuningRes& tuningResult,
    QParams& rOutParam
)
{
    MBOOL bRet = MFALSE;
    DepthMapPipeOpState opState = pRequest->getRequestAttr().opState;
    if(opState == eSTATE_NORMAL)
    {
        if(pRequest->getRequestAttr().needFEFM)
            bRet = TemplateProvider::buildQParams_NORMAL(pRequest, tuningResult, rOutParam);
        else
            bRet = TemplateProvider::buildQParams_NORMAL_NOFEFM(pRequest, tuningResult, rOutParam);
    }

    return bRet;
}

MBOOL
DepthMapFlowOption_ActiveStereo::
onP2ProcessDone(
    P2ANode* pNode,
    sp<DepthMapEffectRequest> pRequest
)
{
    VSDOF_LOGD("+, reqID=%d", pRequest->getRequestNo());
    // notify template provider p2 done
    TemplateProvider::onHandleP2Done(eDPETHMAP_PIPE_NODEID_P2A, pRequest);
    VSDOF_LOGD("-, reqID=%d", pRequest->getRequestNo());
    return MTRUE;
}

INPUT_RAW_TYPE
DepthMapFlowOption_ActiveStereo::
getInputRawType(
    sp<DepthMapEffectRequest> pReq,
    StereoP2Path path
)
{
    INPUT_RAW_TYPE rawType = eRESIZE_RAW;
    return rawType;
}


MBOOL
DepthMapFlowOption_ActiveStereo::
buildQParam_Bayer(
    DepthMapRequestPtr pRequest,
    const Stereo3ATuningRes& tuningResult,
    QParams& rOutParam
)
{
    MBOOL bRet = MFALSE;
    DepthMapPipeOpState opState = pRequest->getRequestAttr().opState;
    if(opState == eSTATE_STANDALONE)
    {
        bRet = TemplateProvider::buildQParams_BAYER_STANDALONE(pRequest, tuningResult, rOutParam);
    }
    return bRet;
}

MBOOL
DepthMapFlowOption_ActiveStereo::
onP2ProcessDone_Bayer(
    P2ABayerNode* pNode,
    sp<DepthMapEffectRequest> pRequest
)
{
    VSDOF_LOGD("+ reqID=%d", pRequest->getRequestNo());
    // notify template provider p2 done
    TemplateProvider::onHandleP2Done(eDPETHMAP_PIPE_NODEID_P2ABAYER, pRequest);
    VSDOF_LOGD("- reqID=%d", pRequest->getRequestNo());
    return MTRUE;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  DepthMapFlowOption Public Operations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL
DepthMapFlowOption_ActiveStereo::
init()
{
    // prepare template
    MBOOL bRet = TemplateProvider::init(mpSizeMgr);
    return bRet;
}

MBOOL
DepthMapFlowOption_ActiveStereo::
queryReqAttrs(
    sp<DepthMapEffectRequest> pRequest,
    EffectRequestAttrs& rReqAttrs
)
{
    // deccide EIS on/off
    IMetadata* pInAppMeta = nullptr;
    pRequest->getRequestMetadata({.bufferID=BID_META_IN_APP, .ioType=eBUFFER_IOTYPE_INPUT}
                                   , pInAppMeta);
    rReqAttrs.isEISOn = isEISOn(pInAppMeta);

    // check flash mode
    IMetadata* pInHalMeta_Main1 = nullptr;
    pRequest->getRequestMetadata({.bufferID=PBID_IN_HAL_META_MAIN1,
                                        .ioType=eBUFFER_IOTYPE_INPUT}, pInHalMeta_Main1);

    MINT32 mode;
    MBOOL bIsProjectorReq = (tryGetMetadata<MINT32>(pInHalMeta_Main1, MTK_IR_FLASH_MODE, mode) &&
                          mode == MTK_IR_FLASH_MODE_PROJECTOR);
    MBOOL bIsStandAlone = !pRequest->isRequestBuffer(BID_P2A_IN_RSRAW2) || !bIsProjectorReq;
    // flash type
    rReqAttrs.flashType = (mode == MTK_IR_FLASH_MODE_PROJECTOR) ? eFLASH_TYPE_PROJECTOR :
                            (mode == MTK_IR_FLASH_MODE_FLOOD) ? eFLASH_TYPE_FLOOD :
                                eFLASH_TYPE_NONE;
    MINT32 magicNum=-1;
    if(!tryGetMetadata<MINT32>(pInHalMeta_Main1, MTK_P1NODE_PROCESSOR_MAGICNUM, magicNum))
    {
        MY_LOGE("reqID=%d, Failed to get magicNum!", pRequest->getRequestNo());
    }
    // check non-queue depth request
    if(!pRequest->isQueuedDepthRequest(mpPipeOption) && bIsStandAlone)
    {
        MY_LOGE("Non supported request: not-queued depth request + standalone request! reqID=%d main2 raw=%d isProjector=%d",
                pRequest->getRequestNo(), pRequest->isRequestBuffer(BID_P2A_IN_RSRAW2), bIsProjectorReq);
        return MFALSE;
    }
    // main2 not exist or not projector frame --> standalone request
    if(bIsStandAlone)
    {
        rReqAttrs.opState = eSTATE_STANDALONE;
    }
    else
    {
        rReqAttrs.opState = eSTATE_NORMAL;
        if(rReqAttrs.isEISOn)
            rReqAttrs.bufferScenario = eBUFFER_POOL_SCENARIO_RECORD;
        else
            rReqAttrs.bufferScenario = eBUFFER_POOL_SCENARIO_PREVIEW;
    }


    if(rReqAttrs.opState != eSTATE_STANDALONE)
    {
        //
        MINT32 magicNum1 = 0, magicNum2 = 0;
        IMetadata* pInHalMeta_Main1 = nullptr, *pInHalMeta_Main2 = nullptr;
        MBOOL bRet = pRequest->getRequestMetadata({.bufferID=BID_META_IN_HAL_MAIN1,
                                        .ioType=eBUFFER_IOTYPE_INPUT}, pInHalMeta_Main1);
        bRet &= pRequest->getRequestMetadata({.bufferID=BID_META_IN_HAL_MAIN2,
                                        .ioType=eBUFFER_IOTYPE_INPUT}, pInHalMeta_Main2);
        if(!bRet)
        {
            MY_LOGE("Cannot get the input metadata!");
            return MFALSE;
        }
        if(!tryGetMetadata<MINT32>(pInHalMeta_Main1, MTK_P1NODE_PROCESSOR_MAGICNUM, magicNum1)) {
            MY_LOGE("Cannot find MTK_P1NODE_PROCESSOR_MAGICNUM meta of Main1!");
        }
        if(!tryGetMetadata<MINT32>(pInHalMeta_Main2, MTK_P1NODE_PROCESSOR_MAGICNUM, magicNum2)) {
            MY_LOGE("Cannot find MTK_P1NODE_PROCESSOR_MAGICNUM meta of Main2!");
        }
    #ifndef GTEST
        rReqAttrs.needFEFM = FEFMSettingProvider::getInstance()->needToRunFEFM(
                                    magicNum1, magicNum2, (rReqAttrs.opState==eSTATE_CAPTURE));
    #endif
    }

    MY_LOGD("reqID=%d is_rsraw2_exist=%d is_projector_req=%d magicNum=%d isStandAlone=%d needFEFM=%d flashType=%d",
                pRequest->getRequestNo(), pRequest->isRequestBuffer(BID_P2A_IN_RSRAW2), bIsProjectorReq,
                magicNum, bIsStandAlone, rReqAttrs.needFEFM, rReqAttrs.flashType);

    return MTRUE;
}

MBOOL
DepthMapFlowOption_ActiveStereo::
queryPipeNodeBitSet(PipeNodeBitSet& nodeBitSet)
{
    nodeBitSet.reset();
    nodeBitSet.set(eDPETHMAP_PIPE_NODEID_P2A, 1);
    nodeBitSet.set(eDPETHMAP_PIPE_NODEID_N3D, 1);
    nodeBitSet.set(eDPETHMAP_PIPE_NODEID_WPE, 1);
    nodeBitSet.set(eDPETHMAP_PIPE_NODEID_DPE, 1);
    nodeBitSet.set(eDPETHMAP_PIPE_NODEID_ADP, 1);
    nodeBitSet.set(eDPETHMAP_PIPE_NODEID_P2ABAYER, 1);
    return MTRUE;
}

MBOOL
DepthMapFlowOption_ActiveStereo::
buildPipeGraph(DepthMapPipe* pPipe, const DepthMapPipeNodeMap& nodeMap)
{
    DepthMapPipeNode* pP2ANode = nodeMap.valueFor(eDPETHMAP_PIPE_NODEID_P2A);
    DepthMapPipeNode* pN3DNode = nodeMap.valueFor(eDPETHMAP_PIPE_NODEID_N3D);
    DepthMapPipeNode* pDPENode = nodeMap.valueFor(eDPETHMAP_PIPE_NODEID_DPE);
    DepthMapPipeNode* pP2ABayerNode = nodeMap.valueFor(eDPETHMAP_PIPE_NODEID_P2ABAYER);
    DepthMapPipeNode* pWPENode = nodeMap.valueFor(eDPETHMAP_PIPE_NODEID_WPE);
    DepthMapPipeNode* pADPNode = nodeMap.valueFor(eDPETHMAP_PIPE_NODEID_ADP);

    #define CONNECT_DATA(DataID, src, dst)\
        pPipe->connectData(DataID, DataID, src, dst);\
        mvAllowDataIDMap.add(DataID, MTRUE);
    // P2A
    CONNECT_DATA(P2A_TO_N3D_FEFM_CCin, *pP2ANode, *pN3DNode);
    CONNECT_DATA(P2A_TO_N3D_NOFEFM_RECTS, *pP2ANode, *pN3DNode);
    CONNECT_DATA(TO_DUMP_BUFFERS, *pP2ANode, pPipe);
    CONNECT_DATA(P2A_OUT_MV_F, *pP2ANode, pPipe);
    CONNECT_DATA(P2A_OUT_FD, *pP2ANode, pPipe);
    CONNECT_DATA(P2A_OUT_MV_F, *pP2ABayerNode, pPipe);
    CONNECT_DATA(P2A_OUT_FD, *pP2ABayerNode, pPipe);
    CONNECT_DATA(P2A_OUT_DEPTHMAP, *pP2ABayerNode, pPipe);
    // N3D
    CONNECT_DATA(N3D_TO_WPE_IMG_MASK, *pN3DNode, *pWPENode);
    // WPE
    CONNECT_DATA(WPE_TO_DPE_WARP_IMG, *pWPENode, *pDPENode);
    // DPE
    CONNECT_DATA(DPE_TO_ADP_DISPARITY, *pDPENode, *pADPNode);
    // ADP
    CONNECT_DATA(ADP_OUT_INTERNAL_DEPTHMAP, *pADPNode, pPipe);
    CONNECT_DATA(ADP_OUT_DEPTH, *pADPNode, pPipe);
    // P2ABayer
    CONNECT_DATA(BAYER_ENQUE, *pP2ANode, *pP2ABayerNode);
    CONNECT_DATA(REQUEST_DEPTH_NOT_READY, *pP2ABayerNode, pPipe);
    // P2A buffer dump
    CONNECT_DATA(TO_DUMP_IMG3O, *pP2ABayerNode, pPipe);
    CONNECT_DATA(TO_DUMP_RAWS, *pP2ANode, pPipe);
    CONNECT_DATA(TO_DUMP_RAWS, *pP2ABayerNode, pPipe);
    CONNECT_DATA(TO_DUMP_MAPPINGS, *pP2ANode, pPipe);
    // Hal Meta frame output
    CONNECT_DATA(DEPTHMAP_META_OUT, *pN3DNode, pPipe);

    // QUEUED DEPTH specific
    if(mpPipeOption->mFlowType == eDEPTH_FLOW_TYPE_QUEUED_DEPTH)
    {
        // queue flow done
        for(size_t index=0;index<nodeMap.size();++index)
        {
            CONNECT_DATA(QUEUED_FLOW_DONE, *nodeMap.valueAt(index), pPipe);
        }
    }
    // default node graph - Error handling
    for(size_t index=0;index<nodeMap.size();++index)
    {
        CONNECT_DATA(ERROR_OCCUR_NOTIFY, *nodeMap.valueAt(index), pPipe);
    }
    pPipe->setRootNode(pP2ANode);

    return MTRUE;
}

MBOOL
DepthMapFlowOption_ActiveStereo::
checkConnected(
    DepthMapDataID dataID
)
{
    // check data id is allowed to handledata or not.
    if(mvAllowDataIDMap.indexOfKey(dataID)>=0)
    {
        return MTRUE;
    }
    return MFALSE;
}

MBOOL
DepthMapFlowOption_ActiveStereo::
config3ATuningMeta(
    sp<DepthMapEffectRequest> pRequest,
    StereoP2Path path,
    MetaSet_T& rMetaSet
)
{
    // IR+IR only use N3D_Preview
    trySetMetadata<MUINT8>(&rMetaSet.halMeta, MTK_3A_ISP_PROFILE, NSIspTuning::EIspProfile_N3D_Preview);
    return MTRUE;
}

MBOOL
DepthMapFlowOption_ActiveStereo::
config3ATuningMeta_Bayer(
    sp<DepthMapEffectRequest> pRequest,
    MetaSet_T& rMetaSet
)
{
   trySetMetadata<MUINT8>(&rMetaSet.halMeta, MTK_3A_ISP_PROFILE, NSIspTuning::EIspProfile_N3D_Preview);
   return MFALSE;
}

DepthMapBufferID
DepthMapFlowOption_ActiveStereo::
reMapBufferID(
    const EffectRequestAttrs& reqAttr,
    DepthMapBufferID bufferID
)
{
    return bufferID;
}

IImageBuffer*
DepthMapFlowOption_ActiveStereo::
get3DNRVIPIBuffer()
{
    return ActiveDepthQTemplateProvider::get3DNRVIPIBuffer();
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  DepthMapFlowOption_ActiveStereo Private Operations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

}; // NSFeaturePipe_DepthMap
}; // NSCamFeature
}; // NSCam
