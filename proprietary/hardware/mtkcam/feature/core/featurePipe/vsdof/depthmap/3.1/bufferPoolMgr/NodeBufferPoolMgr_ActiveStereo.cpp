/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**
 * @file NodeBufferPoolMgr_ActiveStereo.cpp
 * @brief BufferPoolMgr for VSDOF
*/

// Standard C header file
#include <future>
#include <thread>
// Android system/core header file
#include <ui/gralloc_extra.h>
// mtkcam custom header file

// mtkcam global header file
#include <mtkcam/drv/def/dpecommon.h>
#include <mtkcam/drv/iopipe/PostProc/DpeUtility.h>
#include <mtkcam/feature/stereo/pipe/IDepthMapPipe.h>
#include <mtkcam/drv/iopipe/PostProc/INormalStream.h>
// Module header file
#include <mtkcam/feature/stereo/hal/stereo_size_provider.h>
// Local header file
#include "NodeBufferPoolMgr_ActiveStereo.h"
#include "NodeBufferHandler.h"
#include "../DepthMapPipe_Common.h"
#include "../DepthMapPipeNode.h"
#include "../DepthMapPipeUtils.h"
#include "./bufferSize/NodeBufferSizeMgr.h"
// Logging header file
#undef PIPE_CLASS_TAG
#define PIPE_CLASS_TAG "NodeBufferPoolMgr_ActiveStereo"
#include <featurePipe/core/include/PipeLog.h>

/*******************************************************************************
* Namespace start.
********************************************************************************/
namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe_DepthMap {

using namespace NSCam::NSIoPipe;
using NSCam::NSIoPipe::NSPostProc::INormalStream;

/*******************************************************************************
* Global Define
********************************************************************************/

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  NodeBufferPoolMgr_ActiveStereo class - Instantiation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

NodeBufferPoolMgr_ActiveStereo::
NodeBufferPoolMgr_ActiveStereo(
    PipeNodeBitSet& nodeBitSet,
    sp<DepthMapPipeSetting> pPipeSetting,
    sp<DepthMapPipeOption> pPipeOption
)
: mNodeBitSet(nodeBitSet)
, mpPipeOption(pPipeOption)
, mpPipeSetting(pPipeSetting)
{
    // decide size mgr
    mpBufferSizeMgr = new NodeBufferSizeMgr(pPipeOption);
    MBOOL bRet = this->initializeBufferPool();

    if(!bRet)
    {
        MY_LOGE("Failed to initialize buffer pool set! Cannot continue..");
        uninit();
        return;
    }

    this->buildImageBufferPoolMap();
    this->buildBufScenarioToTypeMap();

}

NodeBufferPoolMgr_ActiveStereo::
~NodeBufferPoolMgr_ActiveStereo()
{
    MY_LOGD("[Destructor] +");
    uninit();
    MY_LOGD("[Destructor] -");
}


MBOOL
NodeBufferPoolMgr_ActiveStereo::
uninit()
{
    // destroy buffer pools
    FatImageBufferPool::destroy(mpRectInBufPool_Main1_PV);
    FatImageBufferPool::destroy(mpRectInBufPool_Main2_PV);
    FatImageBufferPool::destroy(mpCCInBufPool_Main1);
    FatImageBufferPool::destroy(mpCCInBufPool_Main2);
    FatImageBufferPool::destroy(mpFEOB_BufPool);
    FatImageBufferPool::destroy(mpFEOC_BufPool);
    FatImageBufferPool::destroy(mpFMOB_BufPool);
    FatImageBufferPool::destroy(mpFMOC_BufPool);
    FatImageBufferPool::destroy(mpFEBInBufPool_Main1);
    FatImageBufferPool::destroy(mpFEBInBufPool_Main2);
    FatImageBufferPool::destroy(mpFECInBufPool_Main1);
    FatImageBufferPool::destroy(mpFECInBufPool_Main2);
    TuningBufferPool::destroy(mpTuningBufferPool);
    TuningBufferPool::destroy(mpPQTuningBufferPool);
    TuningBufferPool::destroy(mpDpPQParamTuningBufferPool);
    FatImageBufferPool::destroy(mIMG3OmgBufPool);
    //----------------------N3D section--------------------------------//
    FatImageBufferPool::destroy(mN3DWarpingMatrix_Main1);
    FatImageBufferPool::destroy(mN3DWarpingMatrix_Main2);
    //----------------------WPE section--------------------------------//
    FatImageBufferPool::destroy(mDefaultMaskBufPool);
    FatImageBufferPool::destroy(mWarpImgBufPool_Main1);
    FatImageBufferPool::destroy(mWarpMaskBufPool_Main1);
    FatImageBufferPool::destroy(mWarpImgBufPool_Main2);
    FatImageBufferPool::destroy(mWarpMaskBufPool_Main2);
    //----------------------DPE section--------------------------------//
    FatImageBufferPool::destroy(mDMPBuffPool);
    FatImageBufferPool::destroy(mCFMBuffPool);
    FatImageBufferPool::destroy(mRespBuffPool);
    //----------------------ADP section--------------------------------//
    FatImageBufferPool::destroy(mpInternalDepthImgBufPool);

    if(mpBufferSizeMgr != nullptr)
        delete mpBufferSizeMgr;

    return MTRUE;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  BaseBufferPoolMgr Public Operations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
SmartFatImageBuffer
NodeBufferPoolMgr_ActiveStereo::
request(DepthMapBufferID id, BufferPoolScenario scen)
{
    ssize_t index;
    if((index=mBIDtoImgBufPoolMap_Default.indexOfKey(id)) >= 0)
    {
        TIME_THREHOLD(10, "request buffer id=%d %s", id, DepthMapPipeNode::onDumpBIDToName(id));
        sp<FatImageBufferPool> pBufferPool = mBIDtoImgBufPoolMap_Default.valueAt(index);
        return pBufferPool->request();
    }
    else if((index=mBIDtoImgBufPoolMap_Scenario.indexOfKey(id)) >= 0)
    {
        ScenarioToImgBufPoolMap ScenarioBufMap = mBIDtoImgBufPoolMap_Scenario.valueAt(index);
        if((index=ScenarioBufMap.indexOfKey(scen))>=0)
        {
            TIME_THREHOLD(10, "request buffer id=%d %s", id, DepthMapPipeNode::onDumpBIDToName(id));
            sp<FatImageBufferPool> pBufferPool = ScenarioBufMap.valueAt(index);
            return pBufferPool->request();
        }
    }
    return NULL;
}

SmartGraphicBuffer
NodeBufferPoolMgr_ActiveStereo::
requestGB(DepthMapBufferID id, BufferPoolScenario scen)
{

    ssize_t index;
    if((index=mBIDtoGraBufPoolMap_Scenario.indexOfKey(id)) >= 0)
    {
        TIME_THREHOLD(10, "request GB buffer id=%d %s", id, DepthMapPipeNode::onDumpBIDToName(id));
        ScenarioToGraBufPoolMap ScenarioBufMap = mBIDtoGraBufPoolMap_Scenario.valueAt(index);
        if((index=ScenarioBufMap.indexOfKey(scen))>=0)
        {
            sp<GraphicBufferPool> pBufferPool = ScenarioBufMap.valueAt(index);
            VSDOF_LOGD("requestGB  bufferID=%d", id);
            SmartGraphicBuffer smGraBuf = pBufferPool->request();
            // config
            android::sp<NativeBufferWrapper> pNativBuffer = smGraBuf->mGraphicBuffer;
            // config graphic buffer to BT601_FULL
            gralloc_extra_ion_sf_info_t info;
            gralloc_extra_query(pNativBuffer->getHandle(), GRALLOC_EXTRA_GET_IOCTL_ION_SF_INFO, &info);
            gralloc_extra_sf_set_status(&info, GRALLOC_EXTRA_MASK_YUV_COLORSPACE, GRALLOC_EXTRA_BIT_YUV_BT601_FULL);
            gralloc_extra_perform(pNativBuffer->getHandle(), GRALLOC_EXTRA_SET_IOCTL_ION_SF_INFO, &info);
            return smGraBuf;
        }
    }
    return NULL;
}

SmartTuningBuffer
NodeBufferPoolMgr_ActiveStereo::
requestTB(DepthMapBufferID id, BufferPoolScenario scen)
{
    TIME_THREHOLD(10, "request tuning buffer id=%d %s", id, DepthMapPipeNode::onDumpBIDToName(id));
    SmartTuningBuffer smTuningBuf = nullptr;
    if(id == BID_P2A_TUNING)
    {
        smTuningBuf = mpTuningBufferPool->request();
        memset(smTuningBuf->mpVA, 0, mpTuningBufferPool->getBufSize());
    }
    else if(id == BID_PQ_PARAM)
    {
        smTuningBuf = mpPQTuningBufferPool->request();
        memset(smTuningBuf->mpVA, 0, mpPQTuningBufferPool->getBufSize());
    }
    else if(id == BID_DP_PQ_PARAM)
    {
        smTuningBuf = mpDpPQParamTuningBufferPool->request();
        memset(smTuningBuf->mpVA, 0, mpDpPQParamTuningBufferPool->getBufSize());
    }

    if(smTuningBuf.get() == nullptr)
        MY_LOGE("Cannot find the TuningBufferPool with scenario:%d of buffer id:%d!!", scen, id);
    return smTuningBuf;
}

BufferPoolHandlerPtr
NodeBufferPoolMgr_ActiveStereo::
createBufferPoolHandler()
{
    BaseBufferHandler* pPtr = new NodeBufferHandler(this);
    return pPtr;
}

MBOOL
NodeBufferPoolMgr_ActiveStereo::
queryBufferType(
    DepthMapBufferID bid,
    BufferPoolScenario scen,
    DepthBufferType& rOutBufType
)
{
    ssize_t index;

    if((index=mBIDToScenarioTypeMap.indexOfKey(bid))>=0)
    {
        BufScenarioToTypeMap scenMap = mBIDToScenarioTypeMap.valueAt(index);
        if((index=scenMap.indexOfKey(scen))>=0)
        {
            rOutBufType = scenMap.valueAt(index);
            return MTRUE;
        }
    }
    return MFALSE;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  NodeBufferPoolMgr_ActiveStereo class - Private Functinos
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL
NodeBufferPoolMgr_ActiveStereo::
initializeBufferPool()
{
    CAM_TRACE_BEGIN("NodeBufferPoolMgr_ActiveStereo::initializeBufferPool");
    VSDOF_INIT_LOG("+");

    MBOOL bRet = MTRUE;
    std::future<MBOOL> vFutures[eDPETHMAP_PIPE_NODE_SIZE+1];
    int index = 0;

    // P2A buffer pool
    if(mNodeBitSet.test(eDPETHMAP_PIPE_NODEID_P2A))
    {
        vFutures[index++] = std::async(
                                std::launch::async,
                                &NodeBufferPoolMgr_ActiveStereo::initP2ABufferPool, this);
        vFutures[index++] = std::async(
                                std::launch::async,
                                &NodeBufferPoolMgr_ActiveStereo::initFEFMBufferPool, this);
    }
    // N3D buffer pool
    if(mNodeBitSet.test(eDPETHMAP_PIPE_NODEID_N3D))
        vFutures[index++] = std::async(
                                std::launch::async,
                                &NodeBufferPoolMgr_ActiveStereo::initN3DWarpBufferPool, this);
    // DPE buffer pool
    if(mNodeBitSet.test(eDPETHMAP_PIPE_NODEID_DPE))
        vFutures[index++] = std::async(
                                std::launch::async,
                                &NodeBufferPoolMgr_ActiveStereo::initDPEADPBufferPool, this);
    // wait all futures
    for(int idx = index-1; idx >=0; --idx)
    {
        bRet &= vFutures[idx].get();
    }

    VSDOF_INIT_LOG("-");
    return bRet;
}



MBOOL
NodeBufferPoolMgr_ActiveStereo::
initP2ABufferPool()
{
    VSDOF_INIT_LOG("+");
    const P2ABufferSize& rP2A_VR=mpBufferSizeMgr->getP2A(eSTEREO_SCENARIO_RECORD);
    const P2ABufferSize& rP2A_PV=mpBufferSizeMgr->getP2A(eSTEREO_SCENARIO_PREVIEW);
    const P2ABufferSize& rP2A_CAP=mpBufferSizeMgr->getP2A(eSTEREO_SCENARIO_CAPTURE);

    /**********************************************************************
     * Rectify_in/CC_in/FD/Tuning/FE/FM buffer pools
     **********************************************************************/
    // PV, Rect_in1
    CREATE_IMGBUF_POOL_WITH_OFFSET(mpRectInBufPool_Main1_PV, "RectInBufPool_Main1_PV",
                        rP2A_PV.mRECT_IN_SIZE_MAIN1,
                        eImgFmt_YUY2, FatImageBufferPool::USAGE_HW, 64);
    // PV, Rect_in2
    CREATE_IMGBUF_POOL_WITH_OFFSET(mpRectInBufPool_Main2_PV, "RectInPV_BufPool_Main2",
                        rP2A_PV.mRECT_IN_SIZE_MAIN2,
                        eImgFmt_YUY2, FatImageBufferPool::USAGE_HW, 64);
    // CC_in
    CREATE_IMGBUF_POOL(mpCCInBufPool_Main1, "CCin_BufPool_Main1", rP2A_VR.mCCIN_SIZE_MAIN1,
                        eImgFmt_Y8, FatImageBufferPool::USAGE_HW, MTRUE);
    CREATE_IMGBUF_POOL(mpCCInBufPool_Main2, "CCin_BufPool_Main2", rP2A_VR.mCCIN_SIZE_MAIN2,
                        eImgFmt_Y8, FatImageBufferPool::USAGE_HW, MTRUE);
    // TuningBufferPool creation
    mpTuningBufferPool = TuningBufferPool::create("VSDOF_TUNING_P2A", INormalStream::getRegTableSize());
    mpPQTuningBufferPool = TuningBufferPool::create("PQTuningPool", sizeof(PQParam));
    mpDpPQParamTuningBufferPool = TuningBufferPool::create("PQTuningPool", sizeof(DpPqParam));

    // IMG3O bufers
    CREATE_IMGBUF_POOL(mIMG3OmgBufPool, "IMG3OmgBufPool", mpPipeSetting->mszRRZO_Main1,
                        eImgFmt_YV12, FatImageBufferPool::USAGE_HW, MTRUE);
    // allcate buffer
    ALLOCATE_BUFFER_POOL(mpRectInBufPool_Main1_PV, VSDOF_WORKING_EXTRA_BUF_SET);
    ALLOCATE_BUFFER_POOL(mpRectInBufPool_Main2_PV, VSDOF_WORKING_EXTRA_BUF_SET);
    //
    ALLOCATE_BUFFER_POOL(mpCCInBufPool_Main1, VSDOF_WORKING_EXTRA_BUF_SET);
    ALLOCATE_BUFFER_POOL(mpCCInBufPool_Main2, VSDOF_WORKING_EXTRA_BUF_SET);
    ALLOCATE_BUFFER_POOL(mpTuningBufferPool, VSDOF_DEPTH_P2FRAME_SIZE + 2);
    ALLOCATE_BUFFER_POOL(mIMG3OmgBufPool, VSDOF_WORKING_EXTRA_BUF_SET);
    ALLOCATE_BUFFER_POOL(mpPQTuningBufferPool, VSDOF_DEPTH_P2FRAME_SIZE);
    ALLOCATE_BUFFER_POOL(mpDpPQParamTuningBufferPool, VSDOF_DEPTH_P2FRAME_SIZE);

    VSDOF_INIT_LOG("-");
    return MTRUE;
}


MBOOL
NodeBufferPoolMgr_ActiveStereo::
initFEFMBufferPool()
{
    VSDOF_INIT_LOG("+");
    const P2ABufferSize& rP2aSize = mpBufferSizeMgr->getP2A(eSTEREO_SCENARIO_CAPTURE);
    /**********************************************************************
     * FE/FM has 3 stage A,B,C, currently only apply 2 stage FEFM: stage=B(1),C(2)
     **********************************************************************/
    // FE/FM buffer pool - stage B
    MUINT32 iBlockSize = StereoSettingProvider::fefmBlockSize(1);
    // query the FEO buffer size from FE input buffer size
    MSize szFEBufSize = rP2aSize.mFEB_INPUT_SIZE_MAIN1;
    MSize szFEOBufferSize, szFMOBufferSize;
    queryFEOBufferSize(szFEBufSize, iBlockSize, szFEOBufferSize);
    // create buffer pool
    CREATE_IMGBUF_POOL(mpFEOB_BufPool, "FEB_BufPoll", szFEOBufferSize,
                        eImgFmt_STA_BYTE, FatImageBufferPool::USAGE_HW, MTRUE);
    // query the FMO buffer size from FEO size
    queryFMOBufferSize(szFEOBufferSize, szFMOBufferSize);
    // create buffer pool
    CREATE_IMGBUF_POOL(mpFMOB_BufPool, "FMB_BufPool", szFMOBufferSize,
                        eImgFmt_STA_BYTE, FatImageBufferPool::USAGE_HW, MTRUE);

    // FE/FM buffer pool - stage C
    iBlockSize = StereoSettingProvider::fefmBlockSize(2);
    // query FEO/FMO size and create pool
    szFEBufSize = rP2aSize.mFEC_INPUT_SIZE_MAIN1;
    queryFEOBufferSize(szFEBufSize, iBlockSize, szFEOBufferSize);
    CREATE_IMGBUF_POOL(mpFEOC_BufPool, "FEC_BufPool", szFEOBufferSize,
                        eImgFmt_STA_BYTE, FatImageBufferPool::USAGE_HW, MTRUE);
    queryFMOBufferSize(szFEOBufferSize, szFMOBufferSize);
    CREATE_IMGBUF_POOL(mpFMOC_BufPool, "FMC_BufPool", szFMOBufferSize,
                        eImgFmt_STA_BYTE, FatImageBufferPool::USAGE_HW, MTRUE);

    // create the FE input buffer pool - stage B (the seocond FE input buffer)
    //FEB Main1 input
    CREATE_IMGBUF_POOL(mpFEBInBufPool_Main1, "FE1BInputBufPool", rP2aSize.mFEB_INPUT_SIZE_MAIN1,
                    eImgFmt_YV12, FatImageBufferPool::USAGE_HW, MTRUE);

    //FEB Main2 input
    CREATE_IMGBUF_POOL(mpFEBInBufPool_Main2, "FE2BInputBufPool", rP2aSize.mFEB_INPUT_SIZE_MAIN2,
                    eImgFmt_YV12, FatImageBufferPool::USAGE_HW, MTRUE);

    //FEC Main1 input
    CREATE_IMGBUF_POOL(mpFECInBufPool_Main1, "FE1CInputBufPool", rP2aSize.mFEC_INPUT_SIZE_MAIN1,
                    eImgFmt_YUY2, FatImageBufferPool::USAGE_HW, MTRUE);

    //FEC Main2 input
    CREATE_IMGBUF_POOL(mpFECInBufPool_Main2, "FE2CInputBufPool", rP2aSize.mFEC_INPUT_SIZE_MAIN2,
                    eImgFmt_YUY2, FatImageBufferPool::USAGE_HW, MTRUE);

    // FEO/FMO buffer pool- ALLOCATE buffers : Main1+Main2 -> two working set
    ALLOCATE_BUFFER_POOL(mpFEOB_BufPool, 2*VSDOF_WORKING_BUF_SET)
    ALLOCATE_BUFFER_POOL(mpFEOC_BufPool, 2*VSDOF_WORKING_BUF_SET)
    ALLOCATE_BUFFER_POOL(mpFMOB_BufPool, 2*VSDOF_WORKING_BUF_SET)
    ALLOCATE_BUFFER_POOL(mpFMOC_BufPool, 2*VSDOF_WORKING_BUF_SET)

    // FEB/FEC_Input buffer pool- ALLOCATE buffers : 2 (internal working buffer in Burst trigger)
    ALLOCATE_BUFFER_POOL(mpFEBInBufPool_Main1, 2)
    ALLOCATE_BUFFER_POOL(mpFEBInBufPool_Main2, 2)
    ALLOCATE_BUFFER_POOL(mpFECInBufPool_Main1, 2)
    ALLOCATE_BUFFER_POOL(mpFECInBufPool_Main2, 2)

    VSDOF_INIT_LOG("-");
    return MTRUE;
}


MBOOL
NodeBufferPoolMgr_ActiveStereo::
initN3DWarpBufferPool()
{
    VSDOF_INIT_LOG("+");
    // n3d size : no difference between scenarios
    const N3DBufferSize& rN3DSize = mpBufferSizeMgr->getN3D(eSTEREO_SCENARIO_PREVIEW);
    const N3DBufferSize& rN3DSize_CAP = mpBufferSizeMgr->getN3D(eSTEREO_SCENARIO_CAPTURE);
    // SW read/write, hw read
    MUINT32 usage = eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_SW_WRITE_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ;

    // BID_WPE_OUT_MV_Y
    CREATE_IMGBUF_POOL(mWarpImgBufPool_Main1, "WarpImgBufPool_Main", rN3DSize.mWARP_IMG_SIZE,
                        eImgFmt_YUY2, usage, MTRUE);
    // BID_WPE_OUT_SV_Y
    CREATE_IMGBUF_POOL(mWarpImgBufPool_Main2, "WarpImgBufPool_Main2",
                        rN3DSize.mWARP_IMG_SIZE, eImgFmt_YUY2, usage, MTRUE);
    // BID_WPE_OUT_MASK_M
    CREATE_IMGBUF_POOL(mWarpMaskBufPool_Main1, "WarpMaskBufPool_Main1", rN3DSize.mWARP_MASK_SIZE,
                        eImgFmt_NV12, usage, MTRUE);
    // BID_WPE_OUT_MASK_S
    CREATE_IMGBUF_POOL(mWarpMaskBufPool_Main2, "WarpMaskBufPool_Main2", rN3DSize.mWARP_MASK_SIZE,
                        eImgFmt_NV12, usage, MTRUE);
    // Warping matrix
    CREATE_IMGBUF_POOL(mN3DWarpingMatrix_Main1, "N3DWarpingMatrix_Main1", rN3DSize.mWARP_MAP_SIZE_MAIN1,
                        eImgFmt_WARP_3PLANE, usage, MTRUE);
    CREATE_IMGBUF_POOL(mN3DWarpingMatrix_Main2, "N3DWarpingMatrix_Main2", rN3DSize.mWARP_MAP_SIZE_MAIN2,
                        eImgFmt_WARP_3PLANE, usage, MTRUE);
    //
    CREATE_IMGBUF_POOL(mDefaultMaskBufPool, "DefaultMaskBufPool", rN3DSize.mWARP_MASK_SIZE,
                        eImgFmt_NV12, usage, MTRUE);
    // allocate
    ALLOCATE_BUFFER_POOL(mWarpImgBufPool_Main1, VSDOF_WORKING_EXTRA_BUF_SET);
    ALLOCATE_BUFFER_POOL(mWarpImgBufPool_Main2, VSDOF_WORKING_EXTRA_BUF_SET);
    //
    ALLOCATE_BUFFER_POOL(mWarpMaskBufPool_Main1, VSDOF_WORKING_EXTRA_BUF_SET);
    ALLOCATE_BUFFER_POOL(mWarpMaskBufPool_Main2, VSDOF_WORKING_EXTRA_BUF_SET);
    //
    ALLOCATE_BUFFER_POOL(mN3DWarpingMatrix_Main1, VSDOF_WORKING_EXTRA_BUF_SET);
    ALLOCATE_BUFFER_POOL(mN3DWarpingMatrix_Main2, VSDOF_WORKING_EXTRA_BUF_SET);
    //
    ALLOCATE_BUFFER_POOL(mDefaultMaskBufPool, 2);
    VSDOF_INIT_LOG("-");
    return MTRUE;
}

MBOOL
NodeBufferPoolMgr_ActiveStereo::
initDPEADPBufferPool()
{
    VSDOF_INIT_LOG("+");
    #define BUFFRPOOL_EXTRA_SIZE_FOR_LAST_DMP 2
    // DPE size : no difference between scenarios
    const DPEBufferSize& rDPESize = mpBufferSizeMgr->getDPE(eSTEREO_SCENARIO_PREVIEW);
    const DPEBufferSize& rDPESize_CAP = mpBufferSizeMgr->getDPE(eSTEREO_SCENARIO_CAPTURE);
    // allocate with the stride size as the width
    CREATE_IMGBUF_POOL(mDMPBuffPool, "DMPBufPool", rDPESize.mDMP_SIZE,
                                        eImgFmt_Y16, FatImageBufferPool::USAGE_HW, MTRUE);
    CREATE_IMGBUF_POOL(mCFMBuffPool, "CFMBufPool", rDPESize.mCFM_SIZE,
                                        eImgFmt_Y8, FatImageBufferPool::USAGE_HW, MTRUE);
    CREATE_IMGBUF_POOL(mRespBuffPool, "RESPOBufPool", rDPESize.mRESPO_SIZE,
                                        eImgFmt_Y8, FatImageBufferPool::USAGE_HW, MTRUE);
    //DMP, CFM, RESPO have all Left/Right side ->  double size
    ALLOCATE_BUFFER_POOL(mDMPBuffPool, VSDOF_WORKING_BUF_SET*2 + BUFFRPOOL_EXTRA_SIZE_FOR_LAST_DMP);
    ALLOCATE_BUFFER_POOL(mCFMBuffPool, VSDOF_WORKING_BUF_SET*2);
    ALLOCATE_BUFFER_POOL(mRespBuffPool, VSDOF_WORKING_BUF_SET*2);

    // ADP
    const ADPBufferSize& rADPSize = mpBufferSizeMgr->getADP(eSTEREO_SCENARIO_PREVIEW);
     // allocate with the stride size as the width
    CREATE_IMGBUF_POOL(mpInternalDepthImgBufPool, "InternalDepthImgBufPool", rADPSize.mDEPTHMAP_SIZE,
                                        eImgFmt_Y16, FatImageBufferPool::USAGE_SW, MTRUE);
    ALLOCATE_BUFFER_POOL(mpInternalDepthImgBufPool, VSDOF_WORKING_BUF_SET);
    VSDOF_INIT_LOG("-");
    return MTRUE;
}

MBOOL
NodeBufferPoolMgr_ActiveStereo::
buildImageBufferPoolMap()
{
    MY_LOGD("+");

    //---- build the buffer pool without specific scenario ----
    // P2A Part
    if(mNodeBitSet.test(eDPETHMAP_PIPE_NODEID_P2A))
    {
        mBIDtoImgBufPoolMap_Default.add(BID_P2A_OUT_CC_IN1, mpCCInBufPool_Main1);
        mBIDtoImgBufPoolMap_Default.add(BID_P2A_OUT_CC_IN2, mpCCInBufPool_Main2);
        mBIDtoImgBufPoolMap_Default.add(BID_P2A_OUT_FE1BO, mpFEOB_BufPool);
        mBIDtoImgBufPoolMap_Default.add(BID_P2A_OUT_FE2BO, mpFEOB_BufPool);
        mBIDtoImgBufPoolMap_Default.add(BID_P2A_OUT_FE1CO, mpFEOC_BufPool);
        mBIDtoImgBufPoolMap_Default.add(BID_P2A_OUT_FE2CO, mpFEOC_BufPool);
        mBIDtoImgBufPoolMap_Default.add(BID_P2A_OUT_FMBO_RL, mpFMOB_BufPool);
        mBIDtoImgBufPoolMap_Default.add(BID_P2A_OUT_FMBO_LR, mpFMOB_BufPool);
        mBIDtoImgBufPoolMap_Default.add(BID_P2A_OUT_FMCO_LR, mpFMOC_BufPool);
        mBIDtoImgBufPoolMap_Default.add(BID_P2A_OUT_FMCO_RL, mpFMOC_BufPool);
        mBIDtoImgBufPoolMap_Default.add(BID_P2A_FE1B_INPUT, mpFEBInBufPool_Main1);
        mBIDtoImgBufPoolMap_Default.add(BID_P2A_FE2B_INPUT, mpFEBInBufPool_Main2);
        mBIDtoImgBufPoolMap_Default.add(BID_P2A_FE1C_INPUT, mpFECInBufPool_Main1);
        mBIDtoImgBufPoolMap_Default.add(BID_P2A_FE2C_INPUT, mpFECInBufPool_Main2);
    }

    //---- build the buffer pool WITH specific scenario ----
    // P2A
    if(mNodeBitSet.test(eDPETHMAP_PIPE_NODEID_P2A))
    {
        // ============ image buffer pool mapping =============
        // Rectify Main1 : BID_P2A_OUT_RECT_IN1
        ScenarioToImgBufPoolMap RectInImgBufMap_Main1;
        RectInImgBufMap_Main1.add(eBUFFER_POOL_SCENARIO_PREVIEW, mpRectInBufPool_Main1_PV);
        mBIDtoImgBufPoolMap_Scenario.add(BID_P2A_OUT_RECT_IN1, RectInImgBufMap_Main1);
        // Rectify Main2:
        ScenarioToImgBufPoolMap RectInImgBufMap_Main2;
        RectInImgBufMap_Main2.add(eBUFFER_POOL_SCENARIO_PREVIEW, mpRectInBufPool_Main2_PV);
        mBIDtoImgBufPoolMap_Scenario.add(BID_P2A_OUT_RECT_IN2, RectInImgBufMap_Main2);
        // IMG3O
        ScenarioToImgBufPoolMap IMG3OImgBufMap;
        IMG3OImgBufMap.add(eBUFFER_POOL_SCENARIO_PREVIEW, mIMG3OmgBufPool);
        IMG3OImgBufMap.add(eBUFFER_POOL_SCENARIO_RECORD, mIMG3OmgBufPool);
        mBIDtoImgBufPoolMap_Scenario.add(BID_P2A_INTERNAL_IMG3O, IMG3OImgBufMap);
    }
    // N3D & WPE
    if(mNodeBitSet.test(eDPETHMAP_PIPE_NODEID_N3D) &&
        mNodeBitSet.test(eDPETHMAP_PIPE_NODEID_WPE))
    {
        // MV_Y
        ScenarioToImgBufPoolMap MVY_ImgBufMap;
        MVY_ImgBufMap.add(eBUFFER_POOL_SCENARIO_PREVIEW, mWarpImgBufPool_Main1);
        MVY_ImgBufMap.add(eBUFFER_POOL_SCENARIO_RECORD, mWarpImgBufPool_Main1);
        mBIDtoImgBufPoolMap_Scenario.add(BID_WPE_OUT_MV_Y, MVY_ImgBufMap);
        // SV_Y
        ScenarioToImgBufPoolMap SVY_ImgBufMap;
        SVY_ImgBufMap.add(eBUFFER_POOL_SCENARIO_PREVIEW, mWarpImgBufPool_Main2);
        SVY_ImgBufMap.add(eBUFFER_POOL_SCENARIO_RECORD, mWarpImgBufPool_Main2);
        mBIDtoImgBufPoolMap_Scenario.add(BID_WPE_OUT_SV_Y, SVY_ImgBufMap);
        // MASK_M
        ScenarioToImgBufPoolMap MASKImgBufMap;
        MASKImgBufMap.add(eBUFFER_POOL_SCENARIO_PREVIEW, mWarpMaskBufPool_Main1);
        MASKImgBufMap.add(eBUFFER_POOL_SCENARIO_RECORD, mWarpMaskBufPool_Main1);
        mBIDtoImgBufPoolMap_Scenario.add(BID_WPE_OUT_MASK_M, MASKImgBufMap);

        // MASK_S
        ScenarioToImgBufPoolMap MASKImgBufMap_SUB;
        MASKImgBufMap_SUB.add(eBUFFER_POOL_SCENARIO_PREVIEW, mWarpMaskBufPool_Main2);
        MASKImgBufMap_SUB.add(eBUFFER_POOL_SCENARIO_RECORD, mWarpMaskBufPool_Main2);
        mBIDtoImgBufPoolMap_Scenario.add(BID_WPE_OUT_MASK_S, MASKImgBufMap_SUB);
        // Warping Matrix
        ScenarioToImgBufPoolMap ImgBufMap_WarpMtx;
        ImgBufMap_WarpMtx.add(eBUFFER_POOL_SCENARIO_PREVIEW, mN3DWarpingMatrix_Main1);
        ImgBufMap_WarpMtx.add(eBUFFER_POOL_SCENARIO_RECORD, mN3DWarpingMatrix_Main1);
        mBIDtoImgBufPoolMap_Scenario.add(BID_N3D_OUT_WARPING_MATRIX_MAIN1, ImgBufMap_WarpMtx);

        ScenarioToImgBufPoolMap ImgBufMap_WarpMtx2;
        ImgBufMap_WarpMtx2.add(eBUFFER_POOL_SCENARIO_PREVIEW, mN3DWarpingMatrix_Main2);
        ImgBufMap_WarpMtx2.add(eBUFFER_POOL_SCENARIO_RECORD, mN3DWarpingMatrix_Main2);
        mBIDtoImgBufPoolMap_Scenario.add(BID_N3D_OUT_WARPING_MATRIX_MAIN2, ImgBufMap_WarpMtx2);

        ScenarioToImgBufPoolMap ImgBufMap_DefaultMask;
        ImgBufMap_DefaultMask.add(eBUFFER_POOL_SCENARIO_PREVIEW, mDefaultMaskBufPool);
        ImgBufMap_DefaultMask.add(eBUFFER_POOL_SCENARIO_RECORD, mDefaultMaskBufPool);
        mBIDtoImgBufPoolMap_Scenario.add(BID_WPE_IN_MASK, ImgBufMap_DefaultMask);
    }
    //DPE Part
    if(mNodeBitSet.test(eDPETHMAP_PIPE_NODEID_DPE))
    {
        ScenarioToImgBufPoolMap DMPImgBufMap;
        DMPImgBufMap.add(eBUFFER_POOL_SCENARIO_PREVIEW, mDMPBuffPool);
        DMPImgBufMap.add(eBUFFER_POOL_SCENARIO_RECORD, mDMPBuffPool);

        mBIDtoImgBufPoolMap_Scenario.add(BID_DPE_OUT_DMP_L, DMPImgBufMap);
        mBIDtoImgBufPoolMap_Scenario.add(BID_DPE_OUT_DMP_R, DMPImgBufMap);
        mBIDtoImgBufPoolMap_Scenario.add(BID_DPE_INTERNAL_LAST_DMP, DMPImgBufMap);
        //
        ScenarioToImgBufPoolMap CFMImgBufMap;
        CFMImgBufMap.add(eBUFFER_POOL_SCENARIO_PREVIEW, mCFMBuffPool);
        CFMImgBufMap.add(eBUFFER_POOL_SCENARIO_RECORD, mCFMBuffPool);
        mBIDtoImgBufPoolMap_Scenario.add(BID_DPE_OUT_CFM_R, CFMImgBufMap);
        mBIDtoImgBufPoolMap_Scenario.add(BID_DPE_OUT_CFM_L, CFMImgBufMap);

        ScenarioToImgBufPoolMap RESPImgBufMap;
        RESPImgBufMap.add(eBUFFER_POOL_SCENARIO_PREVIEW, mRespBuffPool);
        RESPImgBufMap.add(eBUFFER_POOL_SCENARIO_RECORD, mRespBuffPool);
        mBIDtoImgBufPoolMap_Scenario.add(BID_DPE_OUT_RESPO_L, RESPImgBufMap);
        mBIDtoImgBufPoolMap_Scenario.add(BID_DPE_OUT_RESPO_R, RESPImgBufMap);
    }
    // ADP Part
    if(mNodeBitSet.test(eDPETHMAP_PIPE_NODEID_ADP))
    {
        ScenarioToImgBufPoolMap DepthImgBufMap;
        DepthImgBufMap.add(eBUFFER_POOL_SCENARIO_PREVIEW, mpInternalDepthImgBufPool);
        DepthImgBufMap.add(eBUFFER_POOL_SCENARIO_RECORD, mpInternalDepthImgBufPool);
        mBIDtoImgBufPoolMap_Scenario.add(BID_ADP_INTERNAL_DEPTHMAP, DepthImgBufMap);
    }

    MY_LOGD("-");
    return MTRUE;
}


sp<FatImageBufferPool>
NodeBufferPoolMgr_ActiveStereo::
getImageBufferPool(
    DepthMapBufferID bufferID,
    BufferPoolScenario scenario
)
{
    ssize_t index=mBIDtoImgBufPoolMap_Default.indexOfKey(bufferID);
    if(index >= 0)
    {
        return mBIDtoImgBufPoolMap_Default.valueAt(index);
    }

    index = mBIDtoImgBufPoolMap_Scenario.indexOfKey(bufferID);
    if(index >= 0)
    {
        ScenarioToImgBufPoolMap bufMap;
        bufMap = mBIDtoImgBufPoolMap_Scenario.valueAt(index);
        if((index = bufMap.indexOfKey(scenario)) >= 0)
            return bufMap.valueAt(index);
    }

    MY_LOGW("Failed to get ImageBufferPool! bufferID=%d, scenario=%d", bufferID, scenario);
    return NULL;
}

sp<GraphicBufferPool>
NodeBufferPoolMgr_ActiveStereo::
getGraphicImageBufferPool(
    DepthMapBufferID bufferID,
    BufferPoolScenario scenario
)
{
    ssize_t index = mBIDtoGraBufPoolMap_Scenario.indexOfKey(bufferID);
    if(index >= 0)
    {
        ScenarioToGraBufPoolMap bufMap;
        bufMap = mBIDtoGraBufPoolMap_Scenario.valueAt(index);
        if((index = bufMap.indexOfKey(scenario)) >= 0)
            return bufMap.valueAt(index);
    }

    return NULL;
}

MBOOL
NodeBufferPoolMgr_ActiveStereo::
buildBufScenarioToTypeMap()
{
    // buffer id in default imgBuf pool map -> all scenario are image buffer type
    for(ssize_t idx=0;idx<mBIDtoImgBufPoolMap_Default.size();++idx)
    {
        DepthMapBufferID bid = mBIDtoImgBufPoolMap_Default.keyAt(idx);
        BufScenarioToTypeMap typeMap;
        typeMap.add(eBUFFER_POOL_SCENARIO_PREVIEW, eBUFFER_IMAGE);
        typeMap.add(eBUFFER_POOL_SCENARIO_RECORD, eBUFFER_IMAGE);
        typeMap.add(eBUFFER_POOL_SCENARIO_CAPTURE, eBUFFER_IMAGE);
        mBIDToScenarioTypeMap.add(bid, typeMap);
    }

    // buffer id in scenario imgBuf pool map -> the scenario is image buffer type
    for(ssize_t idx=0;idx<mBIDtoImgBufPoolMap_Scenario.size();++idx)
    {
        DepthMapBufferID bid = mBIDtoImgBufPoolMap_Scenario.keyAt(idx);
        ScenarioToImgBufPoolMap scenToBufMap = mBIDtoImgBufPoolMap_Scenario.valueAt(idx);

        BufScenarioToTypeMap typeMap;
        for(ssize_t idx2=0;idx2<scenToBufMap.size();++idx2)
        {
            BufferPoolScenario sce = scenToBufMap.keyAt(idx2);
            typeMap.add(sce, eBUFFER_IMAGE);
        }
        mBIDToScenarioTypeMap.add(bid, typeMap);
    }
    // graphic buffer section
    for(ssize_t idx=0;idx<mBIDtoGraBufPoolMap_Scenario.size();++idx)
    {
        DepthMapBufferID bid = mBIDtoGraBufPoolMap_Scenario.keyAt(idx);
        ScenarioToGraBufPoolMap scenToBufMap = mBIDtoGraBufPoolMap_Scenario.valueAt(idx);
        ssize_t idx2;
        if((idx2=mBIDToScenarioTypeMap.indexOfKey(bid))>=0)
        {
            BufScenarioToTypeMap& bufScenMap = mBIDToScenarioTypeMap.editValueAt(idx2);
            for(ssize_t idx2=0;idx2<scenToBufMap.size();++idx2)
            {
                BufferPoolScenario sce = scenToBufMap.keyAt(idx2);
                bufScenMap.add(sce, eBUFFER_GRAPHIC);
            }
        }
        else
        {
            BufScenarioToTypeMap typeMap;
            for(ssize_t idx2=0;idx2<scenToBufMap.size();++idx2)
            {
                BufferPoolScenario sce = scenToBufMap.keyAt(idx2);
                typeMap.add(sce, eBUFFER_GRAPHIC);
            }
            mBIDToScenarioTypeMap.add(bid, typeMap);
        }
    }
    // tuning buffer section
    BufScenarioToTypeMap typeMap;
    typeMap.add(eBUFFER_POOL_SCENARIO_PREVIEW, eBUFFER_TUNING);
    typeMap.add(eBUFFER_POOL_SCENARIO_RECORD, eBUFFER_TUNING);
    typeMap.add(eBUFFER_POOL_SCENARIO_CAPTURE, eBUFFER_TUNING);
    mBIDToScenarioTypeMap.add(BID_P2A_TUNING, typeMap);
    return MTRUE;
}

MBOOL
NodeBufferPoolMgr_ActiveStereo::
getAllPoolImageBuffer(
    DepthMapBufferID id,
    BufferPoolScenario scen,
    std::vector<IImageBuffer*>& rImgVec
)
{
    MBOOL bRet = MFALSE;
    if(mBIDtoImgBufPoolMap_Default.indexOfKey(id) >= 0)
    {
        bRet = MTRUE;
        sp<FatImageBufferPool> pool = mBIDtoImgBufPoolMap_Default.valueFor(id);
        FatImageBufferPool::CONTAINER_TYPE poolContents = pool->getPoolContents();
        for(sp<FatImageBufferHandle>& handle : poolContents)
        {
            rImgVec.push_back(handle->mImageBuffer.get());
        }
    }

    if(mBIDtoImgBufPoolMap_Scenario.indexOfKey(id) >= 0)
    {
        bRet = MTRUE;
        ScenarioToImgBufPoolMap bufMap = mBIDtoImgBufPoolMap_Scenario.valueFor(id);
        if(bufMap.indexOfKey(scen)>=0)
        {
            sp<FatImageBufferPool> pool = bufMap.valueFor(scen);
            FatImageBufferPool::CONTAINER_TYPE poolContents = pool->getPoolContents();
            for(sp<FatImageBufferHandle>& handle : poolContents)
            {
                // if not exist
                if(std::find(rImgVec.begin(), rImgVec.end(), handle->mImageBuffer.get())==rImgVec.end())
                    rImgVec.push_back(handle->mImageBuffer.get());
            }
        }
    }

    if(mBIDtoGraBufPoolMap_Scenario.indexOfKey(id) >= 0)
    {
        bRet = MTRUE;
        ScenarioToGraBufPoolMap bufMap = mBIDtoGraBufPoolMap_Scenario.valueFor(id);
        if(bufMap.indexOfKey(scen)>=0)
        {
            sp<GraphicBufferPool> pool = bufMap.valueFor(scen);
            GraphicBufferPool::CONTAINER_TYPE poolContents = pool->getPoolContents();
            for(sp<GraphicBufferHandle>& handle : poolContents)
            {
                // if not exist
                if(std::find(rImgVec.begin(), rImgVec.end(), handle->mImageBuffer.get())==rImgVec.end())
                    rImgVec.push_back(handle->mImageBuffer.get());
            }
        }
    }

    return bRet;
}


}; // NSFeaturePipe_DepthMap
}; // NSCamFeature
}; // NSCam

