/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

// Standard C header file
// Android system/core header file

// mtkcam custom header file

// mtkcam global header file
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
// Module header file

// Local header file
#include "N3DNode.h"
#include "../DepthMapPipe_Common.h"
#include "../DepthMapPipeUtils.h"
#include "./bufferPoolMgr/BaseBufferHandler.h"
// logging
#undef PIPE_CLASS_TAG
#define PIPE_CLASS_TAG "N3DNode"
#include <featurePipe/core/include/PipeLog.h>

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe_DepthMap {

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Instantiation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

N3DNode::
N3DNode(
    const char *name,
    DepthMapPipeNodeID nodeID,
    PipeNodeConfigs config
)
: DepthMapPipeNode(name, nodeID, config)
{
    this->addWaitQueue(&mJobQueue);
}

N3DNode::
~N3DNode()
{
    MY_LOGD("[Destructor]");
}

MVOID
N3DNode::
cleanUp()
{
    VSDOF_LOGD("+");

    if(mpN3DHAL_VRPV)
    {
        delete mpN3DHAL_VRPV;
        mpN3DHAL_VRPV = NULL;
    }
    mJobQueue.clear();
    VSDOF_LOGD("-");
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  DepthMapPipeNode Public Operations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

MBOOL
N3DNode::
onInit()
{
    VSDOF_INIT_LOG("+");
    VSDOF_INIT_LOG("-");
    return MTRUE;
}

MBOOL
N3DNode::
onUninit()
{
    VSDOF_INIT_LOG("+");
    VSDOF_INIT_LOG("-");
    return MTRUE;
}

MBOOL
N3DNode::
onThreadStart()
{
    VSDOF_INIT_LOG("+");
    CAM_TRACE_NAME("N3DNode::onInitonThreadStart");
    // create N3DHAL instance - Preview/Record
    N3D_HAL_INIT_PARAM initParam_VRPV;
    initParam_VRPV.eScenario = eSTEREO_SCENARIO_RECORD;
    initParam_VRPV.fefmRound = VSDOF_CONST_FE_EXEC_TIMES;
    mpN3DHAL_VRPV  = N3D_HAL::createInstance(initParam_VRPV);
    VSDOF_INIT_LOG("-");
    return MTRUE;
}

MBOOL
N3DNode::
onThreadStop()
{
    VSDOF_INIT_LOG("+");
    cleanUp();
    VSDOF_INIT_LOG("-");
    return MTRUE;
}


MBOOL
N3DNode::
onData(DataID data, DepthMapRequestPtr& pRequest)
{
    MBOOL ret = MTRUE;
    VSDOF_LOGD("+ : dataID=%s reqId=%d", ID2Name(data), pRequest->getRequestNo());

    switch(data)
    {
        case P2A_TO_N3D_FEFM_CCin:
        case P2A_TO_N3D_NOFEFM_RECTS:
            mJobQueue.enque(pRequest);
            break;
        default:
            MY_LOGW("Unrecongnized DataID=%d", data);
            ret = MFALSE;
            break;
    }

    VSDOF_LOGD("-");
    return ret;
}

MBOOL
N3DNode::
onThreadLoop()
{
    DepthMapRequestPtr pRequest;

    if( !waitAnyQueue() )
    {
        return MFALSE;
    }
    // deque prioritized request
    if( !mJobQueue.deque(pRequest) )
    {
        return MFALSE;
    }
    // mark on-going-request start
    this->incExtThreadDependency();

    VSDOF_PRFLOG("threadLoop start, reqID=%d", pRequest->getRequestNo());
    CAM_TRACE_NAME("N3DNode::onThreadLoop");

    MBOOL ret;
    if(pRequest->getRequestAttr().opState == eSTATE_NORMAL)
    {
        // process done are launched inside it
        ret = performN3DALGO_VRPV(pRequest);
    }
    else
    {
        ret = MFALSE;
        MY_LOGE("reqID=%d, not support this state:%d",
                pRequest->getRequestNo(), pRequest->getRequestAttr().opState);
    }
    // error handling
    if(!ret)
    {
        MY_LOGE("N3D operation failed: reqID=%d", pRequest->getRequestNo());
        // if error occur in the queued-flow, skip this operation and call queue-done
        if(pRequest->isQueuedDepthRequest(mpPipeOption))
            handleData(QUEUED_FLOW_DONE, pRequest);
        else
            handleData(ERROR_OCCUR_NOTIFY, pRequest);
    }
    //
    pRequest->getBufferHandler()->onProcessDone(getNodeId());
    // mark on-going-request end
    this->decExtThreadDependency();
    return ret;
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  N3DNode Private Operations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

MBOOL
N3DNode::
performN3DALGO_VRPV(DepthMapRequestPtr& pRequest)
{
    CAM_TRACE_NAME("N3DNode::performN3DALGO_VRPV");
    N3D_HAL_PARAM n3dParams;
    N3D_HAL_OUTPUT n3dOutput;
    sp<BaseBufferHandler> pBufferHandler = pRequest->getBufferHandler();

    // prepare params
    if(!prepareN3DParam_NORMAL(pRequest, n3dParams, n3dOutput))
        return MFALSE;
    // debug param
    debugN3DParams(n3dParams, n3dOutput);
    // timer
    pRequest->mTimer.startN3D();
    VSDOF_PRFLOG("start N3D(PV/VR) ALGO, reqID=%d", pRequest->getRequestNo());
    CAM_TRACE_BEGIN("N3DNode::N3DHALRun");
    //
    MBOOL bRet = mpN3DHAL_VRPV->N3DHALRun(n3dParams, n3dOutput);
    CAM_TRACE_END();
    pRequest->mTimer.stopN3D();
    // write meta
    writeN3DResultToMeta(n3dOutput, pRequest);
    //
    VSDOF_PRFTIME_LOG("finsished N3D(PV/VR) ALGO, reqID=%d,exec-time=%d msec",
            pRequest->getRequestNo(), pRequest->mTimer.getElapsedN3D());
    //
    if(bRet)
    {
        pBufferHandler->configOutBuffer(getNodeId(), BID_P2A_OUT_RECT_IN1, eDPETHMAP_PIPE_NODEID_WPE);
        pBufferHandler->configOutBuffer(getNodeId(), BID_P2A_OUT_RECT_IN2, eDPETHMAP_PIPE_NODEID_WPE);
        pBufferHandler->configOutBuffer(getNodeId(), BID_N3D_OUT_WARPING_MATRIX_MAIN1, eDPETHMAP_PIPE_NODEID_WPE);
        pBufferHandler->configOutBuffer(getNodeId(), BID_N3D_OUT_WARPING_MATRIX_MAIN2, eDPETHMAP_PIPE_NODEID_WPE);
        handleDataAndDump(N3D_TO_WPE_IMG_MASK, pRequest);
    }

    return bRet;
}

MBOOL
N3DNode::
prepareN3DParam_NORMAL(
    DepthMapRequestPtr& pRequest,
    N3D_HAL_PARAM& rN3dInParam,
    N3D_HAL_OUTPUT& rN3dOutParam
)
{
    sp<BaseBufferHandler> pBufferHandler = pRequest->getBufferHandler();
    MBOOL bRet = MTRUE;

    // if needFEFM, input the fefm
    if(pRequest->getRequestAttr().needFEFM)
    {
        bRet &= prepareFEFMData(pBufferHandler, rN3dInParam.hwfefmData);
    }
    //
    bRet &= prepareN3DInputMeta(pRequest, rN3dInParam);
    bRet &= prepareN3DOutputParam(pBufferHandler, rN3dOutParam);
    //
    if(!bRet)
        MY_LOGE("Failed to prepare the N3DParams, reqID=%d", pRequest->getRequestNo());

    return bRet;
}

MBOOL
N3DNode::
prepareN3DInputMeta(
    DepthMapRequestPtr& pRequest,
    N3D_HAL_PARAM& rN3dParam
)
{
    VSDOF_LOGD("prepareN3DInputParam");
    CAM_TRACE_NAME("prepareN3DInputParam");
    sp<BaseBufferHandler> pBufferHandler = pRequest->getBufferHandler();
    //
    MINT32 depthAFON = 0;
    MINT32 disMeasureON = 0;
    MUINT8 isAFTrigger = 0;
    MINT32 magicNum1 = 0;
    MINT32 magicNum2 = 0;
    //
    DepthMapBufferID inAppBID = mapQueuedBufferID(pRequest, mpPipeOption, BID_META_IN_APP);
    DepthMapBufferID inHalBID_Main1 = mapQueuedBufferID(pRequest, mpPipeOption, BID_META_IN_HAL_MAIN1);
    DepthMapBufferID inHalBID_Main2 = mapQueuedBufferID(pRequest, mpPipeOption, BID_META_IN_HAL_MAIN2);
    //
    IMetadata* pInAppMeta = pBufferHandler->requestMetadata(getNodeId(), inAppBID);
    IMetadata* pInHalMeta_Main1 = pBufferHandler->requestMetadata(getNodeId(), inHalBID_Main1);
    IMetadata* pInHalMeta_Main2 = pBufferHandler->requestMetadata(getNodeId(), inHalBID_Main2);
    //
    if(!tryGetMetadata<MINT32>(pInAppMeta, MTK_STEREO_FEATURE_DEPTH_AF_ON, depthAFON))
        VSDOF_LOGD("reqID=%d, Cannot find MTK_STEREO_FEATURE_DEPTH_AF_ON meta!", pRequest->getRequestNo());
    if(!tryGetMetadata<MINT32>(pInAppMeta, MTK_STEREO_FEATURE_DISTANCE_MEASURE_ON, disMeasureON))
        VSDOF_LOGD("reqID=%d, Cannot find MTK_STEREO_FEATURE_DISTANCE_MEASURE_ON meta!", pRequest->getRequestNo());
    if(!tryGetMetadata<MUINT8>(pInAppMeta, MTK_CONTROL_AF_TRIGGER, isAFTrigger))
        MY_LOGE("reqID=%d, Cannot find MTK_CONTROL_AF_TRIGGER meta!", pRequest->getRequestNo());

    if(!tryGetMetadata<MINT32>(pInHalMeta_Main1, MTK_P1NODE_PROCESSOR_MAGICNUM, magicNum1)) {
        MY_LOGE("Cannot find MTK_P1NODE_PROCESSOR_MAGICNUM meta of Main1!");
        return MFALSE;
    }
    if(!tryGetMetadata<MINT32>(pInHalMeta_Main2, MTK_P1NODE_PROCESSOR_MAGICNUM, magicNum2)) {
        MY_LOGE("Cannot find MTK_P1NODE_PROCESSOR_MAGICNUM meta of Main2!");
        return MFALSE;
    }
    // prepare EIS data
    prepareEISData(pInAppMeta, pInHalMeta_Main1, rN3dParam.eisData);
    // prepare params
    rN3dParam.isAFTrigger = isAFTrigger;
    rN3dParam.isDepthAFON = depthAFON;
    rN3dParam.isDistanceMeasurementON = disMeasureON;
    rN3dParam.magicNumber[0] = magicNum1;
    rN3dParam.magicNumber[1] = magicNum2;
    rN3dParam.requestNumber = pRequest->getRequestNo();
    rN3dParam.timestamp = pRequest->mTimeStamp;

    if(!checkDumpIndex(pRequest->getRequestNo())) {
        rN3dParam.dumpHint = nullptr;
    } else {
        extract(&mDumpHint_Main1, pInHalMeta_Main1);
        rN3dParam.dumpHint = &mDumpHint_Main1;
    }
    return MTRUE;
}

MBOOL
N3DNode::
writeN3DResultToMeta(
    const N3D_HAL_OUTPUT& n3dOutput,
    DepthMapRequestPtr& pRequest
)
{
    DepthMapBufferID outAppBID = mapQueuedBufferID(pRequest, mpPipeOption, BID_META_OUT_APP);
    IMetadata* pOutAppMeta = pRequest->getBufferHandler()->requestMetadata(getNodeId(), outAppBID);
    VSDOF_LOGD("output distance:%d", n3dOutput.distance);
    trySetMetadata<MFLOAT>(pOutAppMeta, MTK_STEREO_FEATURE_RESULT_DISTANCE, n3dOutput.distance);
    // set outAppMeta ready
    pRequest->setOutputBufferReady(outAppBID);

    DepthMapBufferID outHalBID = mapQueuedBufferID(pRequest, mpPipeOption, BID_META_OUT_HAL);
    IMetadata* pOutHalMeta = pRequest->getBufferHandler()->requestMetadata(getNodeId(), outHalBID);
    VSDOF_LOGD("output convOffset:%f", n3dOutput.convOffset);
    trySetMetadata<MFLOAT>(pOutHalMeta, MTK_CONVERGENCE_DEPTH_OFFSET, n3dOutput.convOffset);
    // set outHalMeta ready
    pRequest->setOutputBufferReady(outHalBID);
    // pass data finish
    if(!pRequest->isQueuedDepthRequest(mpPipeOption))
        this->handleData(DEPTHMAP_META_OUT, pRequest);
    return MTRUE;
}

MVOID
N3DNode::
debugN3DParams(N3D_HAL_PARAM& inParam, N3D_HAL_OUTPUT& output)
{
    if(!DepthPipeLoggingSetup::mbDebugLog)
        return;

    MY_LOGD("+");
    for(int i=0;i<3;i++)
    {
        MY_LOGD("inParam.hwfefmData.geoDataMain1[%d]=%x",i, inParam.hwfefmData.geoDataMain1[i]);
    }

    for(int i=0;i<3;i++)
    {
        MY_LOGD("inParam.hwfefmData.geoDataMain2[%d]=%x",i, inParam.hwfefmData.geoDataMain2[i]);
    }

    for(int i=0;i<3;i++)
    {
        MY_LOGD("inParam.hwfefmData.geoDataLeftToRight[%d]=%x",i, inParam.hwfefmData.geoDataLeftToRight[i]);
    }

    for(int i=0;i<3;i++)
    {
        MY_LOGD("inParam.hwfefmData.geoDataRightToLeft[%d]=%x",i, inParam.hwfefmData.geoDataRightToLeft[i]);
    }

    MY_LOGD("inParam.rectifyImgMain1=%x",inParam.rectifyImgMain1);
    MY_LOGD("inParam.rectifyImgMain2=%x",inParam.rectifyImgMain2);
    MY_LOGD("inParam.ccImage[0]=%x",inParam.ccImage[0]);
    MY_LOGD("inParam.ccImage[1]=%x", inParam.ccImage[1]);
    MY_LOGD("inParam.magicNumber=%d, %d", inParam.magicNumber[0], inParam.magicNumber[1]);
    MY_LOGD("inParam.requestNumber=%d", inParam.requestNumber);
    MY_LOGD("inParam.isAFTrigger=%d", inParam.isAFTrigger);
    MY_LOGD("inParam.isDepthAFON=%d", inParam.isDepthAFON);
    MY_LOGD("inParam.isDistanceMeasurementON=%d", inParam.isDistanceMeasurementON);

    MY_LOGD("inParam.eisData.isON=%d", inParam.eisData.isON);
    MY_LOGD("inParam.eisData.eisOffset=%d", inParam.eisData.eisOffset);
    MY_LOGD("inParam.eisData.eisImgSize=%dx%d", inParam.eisData.eisImgSize.w, inParam.eisData.eisImgSize.h);

    MY_LOGD("output.rectifyImgMain1=%x",output.rectifyImgMain1);
    MY_LOGD("output.maskMain1=%x",output.maskMain1);
    MY_LOGD("output.rectifyImgMain2=%x",output.rectifyImgMain2);
    MY_LOGD("output.maskMain2=%x",output.maskMain2);
    MY_LOGD("-");
}

MBOOL
N3DNode::
prepareN3DOutputParam(
    sp<BaseBufferHandler>& pBufferHandler,
    N3D_HAL_OUTPUT& rN3dParam
)
{
    rN3dParam.warpMapMain1 = pBufferHandler->requestBuffer(getNodeId(), BID_N3D_OUT_WARPING_MATRIX_MAIN1);
    rN3dParam.warpMapMain2 = pBufferHandler->requestBuffer(getNodeId(), BID_N3D_OUT_WARPING_MATRIX_MAIN2);

    return MTRUE;
}

MBOOL
N3DNode::
prepareEISData(
    IMetadata*& pInAppMeta,
    IMetadata*& pInHalMeta_Main1,
    EIS_DATA& rEISData
)
{
    if(isEISOn(pInAppMeta))
    {
        eis_region region;
        if(queryEisRegion(pInHalMeta_Main1, region))
        {
            rEISData.isON = true;
            rEISData.eisOffset.x = region.x_int;
            rEISData.eisOffset.y = region.y_int;
            rEISData.eisImgSize = region.s;
        }
        else
            return MFALSE;
    }
    else
    {
        rEISData.isON = false;
    }
    return MTRUE;
}

MBOOL
N3DNode::
prepareFEFMData(sp<BaseBufferHandler>& pBufferHandler, HWFEFM_DATA& rFefmData)
{
    CAM_TRACE_NAME("prepareFEFMData");
    VSDOF_LOGD("prepareFEFMData");
    // N3D input FEO/FMO data
    IImageBuffer *pFe1boBuf = nullptr, *pFe2boBuf = nullptr;
    IImageBuffer *pFe1coBuf = nullptr, *pFe2coBuf = nullptr;
    IImageBuffer *pFmboLRBuf = nullptr, *pFmboRLBuf = nullptr;
    IImageBuffer *pFmcoLRBuf = nullptr, *pFmcoRLBuf = nullptr;

    // Get FEO
    pBufferHandler->getEnqueBuffer(getNodeId(), BID_P2A_OUT_FE1BO, pFe1boBuf);
    pBufferHandler->getEnqueBuffer(getNodeId(), BID_P2A_OUT_FE2BO, pFe2boBuf);
    pBufferHandler->getEnqueBuffer(getNodeId(), BID_P2A_OUT_FE1CO, pFe1coBuf);
    pBufferHandler->getEnqueBuffer(getNodeId(), BID_P2A_OUT_FE2CO, pFe2coBuf);
    // syncCache
    pFe1boBuf->syncCache(eCACHECTRL_INVALID);
    pFe2boBuf->syncCache(eCACHECTRL_INVALID);
    pFe1coBuf->syncCache(eCACHECTRL_INVALID);
    pFe2coBuf->syncCache(eCACHECTRL_INVALID);

    // insert params
    rFefmData.geoDataMain1[0] = pFe1boBuf;
    rFefmData.geoDataMain1[1] = pFe1coBuf;
    rFefmData.geoDataMain1[2] = NULL;
    rFefmData.geoDataMain2[0] = pFe2boBuf;
    rFefmData.geoDataMain2[1] = pFe2coBuf;
    rFefmData.geoDataMain2[2] = NULL;

    // Get FMO
    pBufferHandler->getEnqueBuffer(getNodeId(), BID_P2A_OUT_FMBO_LR, pFmboLRBuf);
    pBufferHandler->getEnqueBuffer(getNodeId(), BID_P2A_OUT_FMBO_RL, pFmboRLBuf);
    pBufferHandler->getEnqueBuffer(getNodeId(), BID_P2A_OUT_FMCO_LR, pFmcoLRBuf);
    pBufferHandler->getEnqueBuffer(getNodeId(), BID_P2A_OUT_FMCO_RL, pFmcoRLBuf);
    // sync cache
    pFmboLRBuf->syncCache(eCACHECTRL_INVALID);
    pFmboRLBuf->syncCache(eCACHECTRL_INVALID);
    pFmcoLRBuf->syncCache(eCACHECTRL_INVALID);
    pFmcoRLBuf->syncCache(eCACHECTRL_INVALID);

    // insert params
    rFefmData.geoDataLeftToRight[0] = pFmboLRBuf;
    rFefmData.geoDataLeftToRight[1] = pFmcoLRBuf;
    rFefmData.geoDataLeftToRight[2] = NULL;
    rFefmData.geoDataRightToLeft[0] = pFmboRLBuf;
    rFefmData.geoDataRightToLeft[1] = pFmcoRLBuf;
    rFefmData.geoDataRightToLeft[2] = NULL;

    return MTRUE;
}

MVOID
N3DNode::
onFlush()
{
    MY_LOGD("+ extDep=%d", this->getExtThreadDependency());
    DepthMapRequestPtr pRequest;
    while( mJobQueue.deque(pRequest) )
    {
        sp<BaseBufferHandler> pBufferHandler = pRequest->getBufferHandler();
        pBufferHandler->onProcessDone(getNodeId());
    }
    DepthMapPipeNode::onFlush();
    MY_LOGD("-");
}

}; //NSFeaturePipe_DepthMap
}; //NSCamFeature
}; //NSCam



