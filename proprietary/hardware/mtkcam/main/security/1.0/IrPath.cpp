#define DEBUG_LOG_TAG "IrPath"
//
#include <stdlib.h>
#include <utils/Errors.h>
#include <utils/List.h>

#include "IrPath.h"
#include <mtkcam/utils/std/TypeTraits.h>
#include <mtkcam/utils/std/Format.h>
#include <mtkcam/utils/std/Misc.h>
#include <mtkcam/utils/imgbuf/ISecureImageBufferHeap.h>
//
#include <mtkcam/aaa/ISync3A.h>
#include <mtkcam/feature/stereo/hal/stereo_setting_provider.h>


#define DUMP_PATH "/sdcard/raw/"

using namespace NSCam::security;

// NOTE: When MTK TEE (GenieZone) is enabled,
//       streaming data from image sensor is written into
//       the secure memory region allocated from MTK TEE.
#ifdef USING_MTK_TEE
static const bool isSecurity = false; //true
static const SecType secType = SecType::mem_secure; //SecType::mem_protected;
#else
static const bool isSecurity = false;
static const SecType secType = SecType::mem_secure;
#endif

int IrPath::sSensorNum = 0;

static constexpr int kBufferUsage =
    (eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);

template <typename T>
static inline MBOOL
tryGetMetadata(
        IMetadata* pMetadata,
        MUINT32 const tag,
        T & rVal
        )
{
    if( pMetadata == NULL ) {
        IRIS_LOGE("pMetadata == NULL");
        return MFALSE;
    }

    IMetadata::IEntry entry = pMetadata->entryFor(tag);
    if( !entry.isEmpty() ) {
        rVal = entry.itemAt(0, Type2Type<T>());
        return MTRUE;
    }
    return MFALSE;
}

template <typename T>
static inline MBOOL
trySetMetadata(
        IMetadata* pMetadata,
        MUINT32 const tag,
        T const& val
        )
{
    if( pMetadata == NULL ) {
        IRIS_LOGE("pMetadata == NULL");
        return MFALSE;
    }

    IMetadata::IEntry entry(tag);
    entry.push_back(val, Type2Type<T>());
    pMetadata->update(tag, entry);
    return MTRUE;
}


static
sp<ImageStreamInfo> createRawImageStreamInfo(
        char const* streamName,
        StreamId_T streamId,
        MUINT32 streamType,
        size_t maxBufNum,
        size_t minInitBufNum,
        MUINT usageForAllocator,
        MINT imgFormat,
        MSize const& imgSize,
        size_t const stride
        );

static
sp<ImageStreamInfo> createImageStreamInfo(
        char const* streamName,
        StreamId_T streamId,
        MUINT32 streamType,
        size_t maxBufNum,
        size_t minInitBufNum,
        MUINT usageForAllocator,
        MINT imgFormat,
        MSize const& imgSize,
        MUINT32 transform = 0
        );

static
MVOID add_stream_to_set( StreamSet& set, sp<IStreamInfo> pInfo ) {
    if( pInfo.get() ) set.add(pInfo->getStreamId());
}

template<typename PFN>
static inline PFN loadFunctionPointer(iris_callback_function_pointer_t func)
{
    return reinterpret_cast<PFN>(func);
}

// TODO: add this helper into HwInfoHelper
static constexpr unsigned int getRAWBitDepth(int rawSensorBit)
{
    switch (rawSensorBit)
    {
        case RAW_SENSOR_8BIT:
            return 8;
        case RAW_SENSOR_10BIT:
            return 10;
        case RAW_SENSOR_12BIT:
            return 12;
        case RAW_SENSOR_14BIT:
            return 14;
        default:
            IRIS_LOGE("unknown raw sensor bit(0x%x), return 10 bit", rawSensorBit);
            return 10;
    }
}

// ---------------------------------------------------------------------------

static void NSCam::security::StreamingLoop(IrPath& camera)
{
    AutoLog();

    bool stop = false;

    while(!stop) {
        IRIS_LOGD("[%s] wait event", __FUNCTION__);
        sem_wait(&camera.mSemStreamLoop);

        State eState = camera.getState();
        IRIS_LOGD("[%s] receive event: state(0x%04x)", __FUNCTION__, eState);
        switch (eState)
        {
            case State::PREVIEW:
                camera.processRequest();
                stop = true;
                sem_post(&camera.mSemStreamLoopDone);
                break;
            case State::PREVIEW_STOP:
                sem_post(&camera.mSemStreamLoopDone);
                break;
            case State::UNINIT:
                IRIS_LOGD("uninit thread");
                break;
            case State::EXIT:
                IRIS_LOGD("exit thread");
                stop = true;
                break;
            default:
                IRIS_LOGD("Unknown state(0x%04x)", eState);
                break;
        }
        if (stop == true)
        {
            IRIS_LOGD("exit thread %s", __FUNCTION__);
            break;
        }
    }

}

// ---------------------------------------------------------------------------

IrPath::IrPath(int maxBuffers)
: mpSensorHalObj(nullptr),
    mSensorId(SENSOR_DEV_NONE),
    mRequestTemplate(CAMERA3_TEMPLATE_PREVIEW),
    mRrzoFormat(eImgFmt_FG_BAYER10),
    mImgoFormat(eImgFmt_BAYER10_UNPAK),
    mContext(nullptr),
    mControlMeta_App(nullptr),
    mControlMeta_Hal(nullptr),
    mResultMeta_P1_App(nullptr),
    mResultMeta_P1_Hal(nullptr),
    mImage_RrzoRaw(nullptr),
    mImage_ImgoRaw(nullptr),
    mFullRawPool(nullptr),
    mResizedRawPool(nullptr),
    mImgoProducer(nullptr),
    mRrzoProducer(nullptr),
    mCallbackHandler(nullptr),
    mCallback(nullptr),
    mRequestBuilderP1(nullptr),
    mResultProcessor(nullptr),
    mTimestampProcessor(nullptr),
    mEnableDump(0),
    mbInitDone(false)
{
    AutoLog();
    mState.first = State::NONE;
    // init semaohore
    sem_init(&mSemStreamLoop, 0, 0);
    sem_init(&mSemStreamLoopDone, 0, 0);
    mEnableDump = ::property_get_int32("debug.securecamera.dump.en", 0);

    if (mEnableDump)
    {
        if (!NSCam::Utils::makePath(DUMP_PATH, 0660))
        {
            IRIS_LOGE("create folder <%s> fail", DUMP_PATH);
        }
    }
}

IrPath::~IrPath()
{
    AutoLog();

    // destroy semaphore
    sem_destroy(&mSemStreamLoop);
    sem_destroy(&mSemStreamLoopDone);
}

void IrPath::destroyInstance()
{
    AutoLog();

    {
        setState(State::EXIT);

        if (mStreamWorker.joinable())
        {
            sem_post(&mSemStreamLoop);
            IRIS_LOGD("join streaming worker +");
            mStreamWorker.join();
            IRIS_LOGD("join streaming worker -");
        }
    }
}

void IrPath::onBufferReleased()
{
    AutoLog();
}

void IrPath::registerCallback(Callback* cb, void* priv)
{
    AutoLog();

    if (cb == nullptr)
    {
        IRIS_LOGE("invalid callback");
        return;
    }

    const auto key(cb->getDescriptor());
    std::unique_lock<std::mutex> _l(mRegisteredCallbacksLock);
    auto search = mRegisteredCallbacks.find(key);
    if (CC_UNLIKELY(search != mRegisteredCallbacks.end()))
    {
        IRIS_LOGW("callback key(0x%x) already exist, overwrite it", key);
        mRegisteredCallbacks.erase(key);
    }

    if (mRegisteredCallbacks.emplace(key, std::make_pair(cb->getHook(), priv)).second == false)
        IRIS_LOGE("cannot emplace callback: key(0x%x)", key);
}

Result IrPath::init(const std::unordered_map<StreamID, IrisStream>& streamMap)
{
    AutoLog();

    setState(State::INIT);

    mStreamMap = streamMap;

    // a worker thread of sumbiting request
    mStreamWorker = std::thread(StreamingLoop, std::ref(*this));

    prepareConfiguration();

    setupMetaStreamInfo();

    setupImageStreamInfo();

    setupRawBufferPool();

    setupPipelineContext();

    setupRequestBuilder();

    setState(State::IDLE);

    return OK;
}

Result IrPath::unInit()
{
    AutoLog();

    Result err = OK;

    State eState = getState();

    IRIS_LOGD("eState(0x%04x)", eState);
    if (eState != State::NONE)
    {
        if ((eState != State::IDLE) && (eState != State::ERROR))
        {
            IRIS_LOGD("Camera is not in the idle state");
            if (toLiteral(eState) & toLiteral(State::PREVIEW_MASK))
            {
                err = StreamingOff();
                if (err != OK)
                {
                    IRIS_LOGE("streamingPreviewStop fail(0x%x)", err);
                }
            }

            while (eState != State::IDLE)
            {
                usleep(10000);
                eState = getState();
            }
            IRIS_LOGD("Now camera is in the idle state");
        }

        setState(State::UNINIT);
        IRIS_LOGD("trigger streaming loop");
        sem_post(&mSemStreamLoop);
    }
    else
    {
        setState(State::UNINIT);
    }


    IRIS_LOGD("flush...");
    mContext->flush();
    IRIS_LOGD("waitUntilDrained...");
    mContext->waitUntilDrained();
    IRIS_LOGD("set context to NULL...");
    mContext = nullptr;

    {
        std::unique_lock<std::mutex> _l(mRegisteredCallbacksLock);
        mRegisteredCallbacks.clear();
    }

    if (mResultProcessor.get())
    {
        mResultProcessor->flush();
        mResultProcessor = nullptr;
    }

    mTimestampProcessor = nullptr;
    mRequestBuilderP1 = nullptr;
    mCallbackHandler = nullptr;

    MUINT32 sensorArray[1] = {mSensorId};
    mpSensorHalObj->powerOff(DEBUG_LOG_TAG, 1, &sensorArray[0]);
    mpSensorHalObj->destroyInstance(DEBUG_LOG_TAG);
    mpSensorHalObj = nullptr;

    {
        std::unique_lock<std::mutex> _l(mStreamMapLock);
        mStreamMap.clear();
    }

    return err;
}

Result IrPath::setShutterTime(uint32_t time)
{

    return OK;
}

Result IrPath::setGainValue(uint32_t value)
{
    return OK;
}

Result IrPath::StreamingOn()
{
    AutoLog();

    Result err = OK;

    if (getState() != State::IDLE)
    {
        IRIS_LOGE("Camera state is not IDLE");
        return INVALID_OPERATION;
    }

    IRIS_LOGD("Preview state(0x%x)", State::PREVIEW);
    setState(State::PREVIEW);
    IRIS_LOGD("trigger streaming loop");
    sem_post(&mSemStreamLoop);

    return err;
}

Result IrPath::StreamingOff()
{
    AutoLog();
    State state = getState();
    IRIS_LOGD("state(0x%04x)", state);

    if (state == State::IDLE)
    {
        IRIS_LOGE("is in IDLE state");
        return INVALID_OPERATION;
    }

    if (state != State::PREVIEW_STOP)
    {
        setState(State::PREVIEW_STOP);
    }

    IRIS_LOGD("wait streaming loop...");
    sem_wait(&mSemStreamLoopDone);
    IRIS_LOGD("wait streaming loop done");
    finishPipelineContext();

    setState(State::IDLE);
    return OK;
}

sp<IMetaStreamBuffer>
IrPath::get_default_request()
{
    AutoLog();
    sp<IMetaStreamBuffer> pSBuffer;

    ITemplateRequest* obj = NSTemplateRequestManager::valueFor(mSensorId);
    if(obj == NULL) {
        obj = ITemplateRequest::getInstance(mSensorId);
        NSTemplateRequestManager::add(mSensorId, obj);
    }
    IMetadata meta = obj->getMtkData(mRequestTemplate);
    //
    pSBuffer = createMetaStreamBuffer(mControlMeta_App, meta, false);
    //
    return pSBuffer;
}

IMetaStreamBuffer*
IrPath::createMetaStreamBuffer(
        android::sp<IMetaStreamInfo> pStreamInfo,
        IMetadata const& rSettings,
        MBOOL const repeating
        )
{
    AutoLog();
    HalMetaStreamBuffer*
        pStreamBuffer =
        HalMetaStreamBuffer::Allocator(pStreamInfo.get())(rSettings);
    //
    pStreamBuffer->setRepeating(repeating);
    //
    return pStreamBuffer;
}

void IrPath::setState(const State newState)
{
    std::lock_guard<std::mutex> _l(mState.second);

    IRIS_LOGD("Now(0x%04x), Next(0x%04x)", mState.first, newState);
    if (newState == State::ERROR)
        goto EXIT;

    switch (mState.first)
    {
        case State::NONE:
            switch (newState)
            {
                case State::INIT:
                case State::UNINIT:
                    break;
                default:
                    IRIS_LOGA("State error NONE");
            }
            break;
        case State::INIT:
            switch (newState)
            {
                case State::IDLE:
                    break;
                default:
                    IRIS_LOGA("State error INIT");
            }
            break;
        case State::IDLE:
            switch (newState)
            {
                case State::IDLE:
                case State::PREVIEW:
                case State::UNINIT:
                    break;
                default:
                    IRIS_LOGA("State error IDLE");
            }
            break;
        case State::PREVIEW:
            switch (newState)
            {
                case State::IDLE:
                case State::PREVIEW:
                case State::PREVIEW_STOP:
                    break;
                default:
                    IRIS_LOGA("State error PREVIEW");
                    break;
            }
            break;
        case State::PREVIEW_STOP:
            switch (newState)
            {
                case State::IDLE:
                    break;
                default:
                    IRIS_LOGA("State error PREVIEW_STOP");
            }
            break;
        case State::UNINIT:
            switch (newState)
            {
                case State::EXIT:
                    break;
                default:
                    IRIS_LOGA("State error UNINIT");
            }
            break;
        case State::EXIT:
            IRIS_LOGD("Exit state");
            break;
        case State::ERROR:
            switch (newState)
            {
                case State::IDLE:
                case State::UNINIT:
                    break;
                default:
                    IRIS_LOGA("State error IRIS_MHAL_ERROR");
                    break;
            }
            break;
        default:
            IRIS_LOGE("Unknown state Now(0x%04x), Next(0x%04x)",
                    mState.first, newState);
    }

EXIT:
    mState.first = newState;
    IRIS_LOGD("Now(0x%04x)", mState.first);

}

State IrPath::getState()
{
    std::lock_guard<std::mutex> _l(mState.second);
    return mState.first;
}

void IrPath::processRequest()
{
    AutoLog();
    int current_cnt = 0;
    while ((toLiteral(getState()) & toLiteral(State::PREVIEW_MASK)) || (current_cnt == 0))
    {
        //
        IRIS_LOGD("request %d +\n", current_cnt);
        //
        sp<IPipelineFrame> pFrame;
        //
        sp<IMetaStreamBuffer> pAppMetaControlSB = get_default_request();
        sp<HalMetaStreamBuffer> pHalMetaControlSB =
            HalMetaStreamBuffer::Allocator(mControlMeta_Hal.get())();
        {
            // modify hal control metadata
            IMetadata* pAppMetadata = pAppMetaControlSB->tryWriteLock(DEBUG_LOG_TAG);
            IMetadata* pHalMetadata = pHalMetaControlSB->tryWriteLock(DEBUG_LOG_TAG);
            trySetMetadata<MSize>(pHalMetadata, MTK_HAL_REQUEST_SENSOR_SIZE, mSensorParam.size);

            if(!mbInitDone) {
                IRIS_LOGD("Init dual IR metadata");
                setupInitialMeta(pAppMetadata, pHalMetadata);
                mbInitDone = true;
            }

            pAppMetaControlSB->unlock(DEBUG_LOG_TAG, pAppMetadata);
            pHalMetaControlSB->unlock(DEBUG_LOG_TAG, pHalMetadata);
        }
        {
            sp<RequestBuilder> pRequestBuilder;
            pRequestBuilder = mRequestBuilderP1;

            if (pRequestBuilder.get())
            {
                pRequestBuilder->setMetaStreamBuffer(
                        mControlMeta_App->getStreamId(),
                        pAppMetaControlSB
                        );
                pRequestBuilder->setMetaStreamBuffer(
                        mControlMeta_Hal->getStreamId(),
                        pHalMetaControlSB
                        );
                pFrame = pRequestBuilder->build(current_cnt, mContext);
            }
            if( ! pFrame.get() )
            {
                IRIS_LOGE("build request failed\n");
            }
        }
        if (0)  // Dump All MetaData
        {
            IMetadataTagSet const &mtagInfo = IDefaultMetadataTagSet::singleton()->getTagSet();
            sp<IMetadataConverter> mMetaDataConverter = IMetadataConverter::createInstance(mtagInfo);

            IRIS_LOGD("\n\nDump AppMeta:\n");
            IRIS_LOGD("==========================================================\n");
            IMetadata* pMetadata = pAppMetaControlSB->tryReadLock(DEBUG_LOG_TAG);
            pMetadata->dump();
            IRIS_LOGD("==========================================================\n");
            mMetaDataConverter->dumpAll(*pMetadata);
            pAppMetaControlSB->unlock(DEBUG_LOG_TAG, pMetadata);
            IRIS_LOGD("==========================================================\n");
            IRIS_LOGD("\n\nDump HalMeta:\n");
            IRIS_LOGD("==========================================================\n");
            pMetadata = pHalMetaControlSB->tryReadLock(DEBUG_LOG_TAG);
            pMetadata->dump();
            //IRIS_LOGD("==========================================================\n");
            //mMetaDataConverter->dumpAll(*pMetadata);  // Not Support
            pHalMetaControlSB->unlock(DEBUG_LOG_TAG, pMetadata);
            IRIS_LOGD("==========================================================\n");
        }
        //
        if( pFrame.get() )
        {
            if( OK != mContext->queue(pFrame) )
            {
                IRIS_LOGE("queue pFrame failed\n");
            }
        }

        IRIS_LOGD("request %d -\n", current_cnt);
        current_cnt++;
    }
}

Result IrPath::getSensorSetting(NSCam::security::SensorStaticInfo& sensorSetting)
{
    AutoLog();
    return OK;
}

std::vector<StreamInfo> IrPath::getStreamInfo(
        const NSCam::security::SensorStaticInfo& sensorSetting,
        const std::vector<StreamID>& identifiers)
{
    AutoLog();
    std::vector<StreamInfo> streamInfos;

    for (auto&& id : identifiers)
    {
        switch (id)
        {
            case StreamID::FULL_RAW:
                {
                    StreamInfo info(StreamID::FULL_RAW);
                    info.size = mImgoSize;
                    info.stride = mImgoStride;
                    info.format = static_cast<EImageFormat>(mImgoFormat);
                    info.sizeInBytes = info.stride * info.size.h;

                    IRIS_LOGD("full imgSize(%u) = stride(%zu) * height(%d)",
                            info.sizeInBytes, info.stride , info.size.h);

                    streamInfos.push_back(std::move(info));
                }
                break;
            case StreamID::RESIZED_RAW:
                {
                    StreamInfo info(StreamID::RESIZED_RAW);

                    info.size = mRrzoSize;
                    info.stride = mRrzoStride;
                    info.format = static_cast<EImageFormat>(mRrzoFormat);
                    info.sizeInBytes = info.stride * info.size.h;

                    IRIS_LOGD("resized imgSize(%u) = stride(%zu) * height(%d)",
                            info.sizeInBytes, info.stride , info.size.h);

                    streamInfos.push_back(std::move(info));
                }
                break;
            default:
                IRIS_LOGE("unknown stream ID(0x%x)", toLiteral(id));
        }
    }

    return streamInfos;
}

void IrPath::getStreamInfo(sp<IImageStreamInfo> &imageStreamInfo) {
    imageStreamInfo = mImage_RrzoRaw;
}

Result IrPath::setSrcDev(unsigned int devID)
{
    AutoLog();

    IHalSensorList* const pHalSensorList = MAKE_HalSensorList();
    if (isSecurity)
    {
        IRIS_LOGD("enable secure sensor");
        pHalSensorList->enableSecure("IrPath");
    }

    // TODO: Need to separate this behavior from setSrcDev
    // We should not searchSensors more than one time after
    // searchSensors, otherwise it would cause config P1 error.
    if(!sSensorNum) {
        sSensorNum = pHalSensorList ? pHalSensorList->searchSensors() : 0;
        IRIS_LOGD("searchSensors (%d)\n", sSensorNum);
        if (sSensorNum == 0)
        {
            IRIS_LOGE("search Sensor failed");
            return NAME_NOT_FOUND;
        }
    }

    //
    auto devIndex = pHalSensorList->querySensorDevIdx(devID);
    mDevID.store(devIndex, memory_order_relaxed);
    IRIS_LOGD("sensor ID : %d", mSensorId);
    //

    mSensorId = UINT_MAX;
    const unsigned int sensors = pHalSensorList->queryNumberOfSensors();
    for (unsigned int i = 0; i < sensors; i++)
    {
        if (pHalSensorList->querySensorDevIdx(i) != devIndex)
        {
            IRIS_LOGD("sensor dev ID(%u) idx(%u)",
                    devIndex, pHalSensorList->querySensorDevIdx(i));
            continue;
        }

        mSensorId = i;
        break;
    }
    IRIS_LOGD("sensor dev index(%u), openID(%d)", mSensorId, devIndex);

    if (mSensorId == UINT_MAX)
    {
        IRIS_LOGE("Sensor Not found");
        return NAME_NOT_FOUND;
    }

    mpSensorHalObj = pHalSensorList->createSensor(DEBUG_LOG_TAG, 1, &mSensorId);
    if (!mpSensorHalObj)
    {
        IRIS_LOGE("create sensor failed");
        exit(1);
        return BAD_VALUE;
    }

    // update sensor information
    NSCam::SensorStaticInfo sensorStaticInfo;
    {
        pHalSensorList->querySensorStaticInfo(devIndex, &sensorStaticInfo);

        IRIS_LOGD("sensor dev(%d) index(%u) bitDepth(%u)",
                devIndex, mSensorId,
                getRAWBitDepth(sensorStaticInfo.rawSensorBit));
        IRIS_LOGD("Sensor raw type(%d)", sensorStaticInfo.rawFmtType);
    }

    mSensorParam.mode = SENSOR_SCENARIO_ID_NORMAL_PREVIEW;
    mSensorParam.size = MSize(sensorStaticInfo.previewWidth, sensorStaticInfo.previewHeight);//(sensorStaticInfo.previewWidth, sensorStaticInfo.previewHeight);
#ifdef USING_MTK_LDVT /*[EP_TEMP]*/ //[FIXME] TempTestOnly - USING_FAKE_SENSOR
    mSensorParam.fps = 1;
#else
    mSensorParam.fps = sensorStaticInfo.previewFrameRate/10;//previewFrameRate/10;
#endif
    mSensorParam.pixelMode = 0;

    mpSensorHalObj->sendCommand(
            pHalSensorList->querySensorDevIdx(mSensorId),
            SENSOR_CMD_GET_SENSOR_PIXELMODE,
            (MUINTPTR)(&mSensorParam.mode),
            (MUINTPTR)(&mSensorParam.fps),
            (MUINTPTR)(&mSensorParam.pixelMode));

    IRIS_LOGD("sensor params mode %d, size %dx%d, fps %d, pixelmode %d\n",
            mSensorParam.mode,
            mSensorParam.size.w, mSensorParam.size.h,
            mSensorParam.fps,
            mSensorParam.pixelMode);

    mpSensorHalObj->powerOn(DEBUG_LOG_TAG, 1, &mSensorId);

    // TODO: We need to debug why sometime it would fail!
    // We should powerOn before GET_SENSOR_SYNC_MODE_CAPACITY
    MUINT32 syncMode;
    mpSensorHalObj->sendCommand(pHalSensorList->querySensorDevIdx(mSensorId)
                                , SENSOR_CMD_GET_SENSOR_SYNC_MODE_CAPACITY
                                , (MUINTPTR)&syncMode, 0 /* unused */, 0 /* unused */);
    IRIS_LOGD("sensorId(%d) sensorDevIdx(%d)", mSensorId, pHalSensorList->querySensorDevIdx(mSensorId));

    if (syncMode & SENSOR_MASTER_SYNC_MODE) {
        // Sensor support master mode
        IRIS_LOGD("Sensor(%d) support master sync mode", mSensorId);
    } else {
        IRIS_LOGD("Not sensor(%d) support master sync mode", mSensorId);
    }

    if (syncMode & SENSOR_SLAVE_SYNC_MODE) {
        // Sensor support slave mode
        IRIS_LOGD("Sensor(%d) support slave sync mode", mSensorId);
    } else {
        IRIS_LOGD("Not sensor(%d) support slave sync mode", mSensorId);
    }

    int32_t mainSensorId = 0, subSensorId = 0;
    StereoSettingProvider::getStereoSensorIndex(mainSensorId, subSensorId);

    if(mSensorId == mainSensorId) {
        syncMode = SENSOR_MASTER_SYNC_MODE;
    } else {
        syncMode = SENSOR_SLAVE_SYNC_MODE;
    }

    mpSensorHalObj->sendCommand(pHalSensorList->querySensorDevIdx(mSensorId)
                                , SENSOR_CMD_SET_SENSOR_SYNC_MODE
                                , (MUINTPTR)&syncMode, 0 /* unused */, 0 /* unused */);
    return OK;
}

Result IrPath::sensorPower(bool turnOn)
{
    return OK;
}

void IrPath::prepareConfiguration()
{
    AutoLog();
#define ALIGN_2(x)     (((x) + 1) & (~1))
    //
    {
        IRIS_LOGD("pMetadataProvider ...\n");
        sp<IMetadataProvider> pMetadataProvider = IMetadataProvider::create(mSensorId);
        if (pMetadataProvider.get() != NULL) {
            IRIS_LOGD("pMetadataProvider (%p) +++\n", pMetadataProvider.get());
        }
        NSMetadataProviderManager::add(mSensorId, pMetadataProvider.get());
        if (pMetadataProvider.get() != NULL) {
            IRIS_LOGD("pMetadataProvider (%p) ---\n", pMetadataProvider.get());
        }
    }
    {
        ITemplateRequest* obj = NSTemplateRequestManager::valueFor(mSensorId);
        if(obj == NULL) {
            obj = ITemplateRequest::getInstance(mSensorId);
            NSTemplateRequestManager::add(mSensorId, obj);
        }
    }
    //
    MSize rrzoSize = MSize( ALIGN_2(mSensorParam.size.w / 2), ALIGN_2(mSensorParam.size.h / 2) );
    //
#if 0
    NSImageio::NSIspio::ISP_QUERY_RST queryRst;
    //
    NSImageio::NSIspio::ISP_QuerySize(
            NSImageio::NSIspio::EPortIndex_RRZO,
            NSImageio::NSIspio::ISP_QUERY_X_PIX|
            NSImageio::NSIspio::ISP_QUERY_STRIDE_PIX|
            NSImageio::NSIspio::ISP_QUERY_STRIDE_BYTE,
            (EImageFormat)gRrzoFormat,
            rrzoSize.w,
            queryRst,
            mSensorParam.pixelMode  == 0 ?
            NSImageio::NSIspio::ISP_QUERY_1_PIX_MODE :
            NSImageio::NSIspio::ISP_QUERY_2_PIX_MODE
            );
    rrzoSize.w = queryRst.x_pix;
    gRrzoSize = MSize(rrzoSize.w, rrzoSize.h);
    gRrzoStride = queryRst.stride_byte;
#else
    NSCam::NSIoPipe::NSCamIOPipe::NormalPipe_QueryInfo qry_rrzo;
    NSCam::NSIoPipe::NSCamIOPipe::NormalPipe_QueryIn input_rrz;
    input_rrz.width = rrzoSize.w;

    NSCam::NSIoPipe::NSCamIOPipe::INormalPipeModule::get()->query(
            NSCam::NSIoPipe::PORT_RRZO.index,
            NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_X_PIX|NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_STRIDE_BYTE,
            (EImageFormat)mRrzoFormat, input_rrz, qry_rrzo);
    rrzoSize.w = qry_rrzo.x_pix;
    mRrzoSize = MSize(rrzoSize.w, rrzoSize.h);
    mRrzoStride = qry_rrzo.stride_byte;
#endif
    IRIS_LOGD("rrzo size %dx%d, stride %d\n", mRrzoSize.w, mRrzoSize.h, (int)mRrzoStride);
    //
    //
    MSize imgoSize = MSize( mSensorParam.size.w, mSensorParam.size.h );
    //
#if 0
    NSImageio::NSIspio::ISP_QUERY_RST queryRstF;
    //
    NSImageio::NSIspio::ISP_QuerySize(
            NSImageio::NSIspio::EPortIndex_IMGO,
            NSImageio::NSIspio::ISP_QUERY_X_PIX|
            NSImageio::NSIspio::ISP_QUERY_STRIDE_PIX|
            NSImageio::NSIspio::ISP_QUERY_STRIDE_BYTE,
            (EImageFormat)gImgoFormat,
            imgoSize.w,
            queryRstF,
            mSensorParam.pixelMode  == 0 ?
            NSImageio::NSIspio::ISP_QUERY_1_PIX_MODE :
            NSImageio::NSIspio::ISP_QUERY_2_PIX_MODE
            );
    imgoSize.w = queryRstF.x_pix;
    gImgoSize = MSize(imgoSize.w, imgoSize.h);
    gImgoStride = queryRstF.stride_byte;
#else
    NSCam::NSIoPipe::NSCamIOPipe::NormalPipe_QueryInfo qry_imgo;
    NSCam::NSIoPipe::NSCamIOPipe::NormalPipe_QueryIn input_imgo;
    input_imgo.width = imgoSize.w;

    NSCam::NSIoPipe::NSCamIOPipe::INormalPipeModule::get()->query(
            NSCam::NSIoPipe::PORT_IMGO.index,
            NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_X_PIX|NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_STRIDE_BYTE,
            (EImageFormat)mImgoFormat, input_imgo, qry_imgo);
    imgoSize.w = qry_imgo.x_pix;
    mImgoSize = MSize(imgoSize.w, imgoSize.h);
    mImgoStride = qry_imgo.stride_byte;
#endif
    IRIS_LOGD("imgo size %dx%d, stride %d\n", mImgoSize.w, mImgoSize.h, (int)mImgoStride);
    //
}

void IrPath::setupMetaStreamInfo()
{
    AutoLog();
    mControlMeta_App =
        new MetaStreamInfo(
                "App:Meta:Control",
                STREAM_ID_METADATA_CONTROL_APP,
                eSTREAMTYPE_META_IN,
                0
                );
    mControlMeta_Hal =
        new MetaStreamInfo(
                "Hal:Meta:Control",
                STREAM_ID_METADATA_CONTROL_HAL,
                eSTREAMTYPE_META_IN,
                0
                );
    mResultMeta_P1_App =
        new MetaStreamInfo(
                "App:Meta:ResultP1",
                STREAM_ID_METADATA_RESULT_P1_APP,
                eSTREAMTYPE_META_OUT,
                0
                );
    mResultMeta_P1_Hal =
        new MetaStreamInfo(
                "Hal:Meta:ResultP1",
                STREAM_ID_METADATA_RESULT_P1_HAL,
                eSTREAMTYPE_META_INOUT,
                0
                );

}

void IrPath::setupImageStreamInfo()
{
    AutoLog();

    {// Resized Raw
        MSize const& size = mRrzoSize;
        MINT const format = mRrzoFormat;
        size_t const stride = mRrzoStride;
        MUINT const usage = 0;//eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READWRITE ;
        mImage_RrzoRaw = createRawImageStreamInfo(
                "Hal:Image:Resiedraw",
                STREAM_ID_RAW1,
                eSTREAMTYPE_IMAGE_INOUT,
                6, 4,
                usage, format, size, stride
                );
    }
    {// Full Raw
        MSize const& size = mImgoSize;
        MINT const format = mImgoFormat;
        size_t const stride = mImgoStride;
        MUINT const usage = 0;//eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READWRITE ;
        mImage_ImgoRaw = createRawImageStreamInfo(
                "Hal:Image:Fullraw",
                STREAM_ID_RAW2,
                eSTREAMTYPE_IMAGE_INOUT,
                6, 4,
                usage, format, size, stride
                );
    }

}

void IrPath::setupRawBufferPool()
{
    AutoLog();

    mCallback = new ImageCallback(this, 0);
    mMetaListener = new MetadataListener(this);
    mCallbackHandler = new BufferCallbackHandler(mSensorId);
    mCallbackHandler->setImageCallback(mCallback);

    // rrzo
    sp<StreamBufferProviderFactory> pRrzoFactory =
        StreamBufferProviderFactory::createInstance();
#if 0
    mResizedRawPool = new CallbackBufferPool(mImage_RrzoRaw, isSecurity, secType);
    mResizedRawPool->allocateBuffer(
            mImage_RrzoRaw->getStreamName(),
            mImage_RrzoRaw->getMaxBufNum(),
            mImage_RrzoRaw->getMinInitBufNum()
            );
    mCallbackHandler->setBufferPool(mResizedRawPool);

    pRrzoFactory->setImageStreamInfo(mImage_RrzoRaw);
    pRrzoFactory->setUsersPool(
            mCallbackHandler->queryBufferPool(mImage_RrzoRaw->getStreamId())
            );
#else

    mResizedRawPool = new SecureBufferPool(mSensorId, mImage_RrzoRaw);

    mResizedRawPool->setBufferQueue(mStreamMap[StreamID::RESIZED_RAW]);

    pRrzoFactory->setImageStreamInfo(mImage_RrzoRaw);
    pRrzoFactory->setUsersPool(mResizedRawPool);
#endif
    mRrzoProducer = pRrzoFactory->create(true);

    // imgo
    sp<StreamBufferProviderFactory> pImgoFactory =
        StreamBufferProviderFactory::createInstance();
#if 1
    mFullRawPool = new CallbackBufferPool(mImage_ImgoRaw, isSecurity, secType);
    mFullRawPool->allocateBuffer(
            mImage_ImgoRaw->getStreamName(),
            mImage_ImgoRaw->getMaxBufNum(),
            mImage_ImgoRaw->getMinInitBufNum()
            );
    mCallbackHandler->setBufferPool(mFullRawPool);

    pImgoFactory->setImageStreamInfo(mImage_ImgoRaw);
    pImgoFactory->setUsersPool(
            mCallbackHandler->queryBufferPool(mImage_ImgoRaw->getStreamId())
            );
#else
    mFullRawPool = new SecureBufferPool(mSensorId, mImage_ImgoRaw);

    mFullRawPool->setBufferQueue(mStreamMap[StreamID::FULL_RAW]);

    pImgoFactory->setImageStreamInfo(mImage_ImgoRaw);
    pImgoFactory->setUsersPool(mFullRawPool);
#endif
    mImgoProducer = pImgoFactory->create(true);

}

void IrPath::setupPipelineContext()
{
    AutoLog();
    mContext = PipelineContext::create(DEBUG_LOG_TAG);
    if( !mContext.get() ) {
        IRIS_LOGE("cannot create context\n");
        return;
    }
    //
    mContext->beginConfigure();
    //
    // 1. Streams ***************
    //
    // 1.a. check if stream exist
    // 1.b. setup streams
    {
        // Meta
        StreamBuilder(eStreamType_META_APP, mControlMeta_App)
            .build(mContext);
        StreamBuilder(eStreamType_META_HAL, mControlMeta_Hal)
            .build(mContext);
        StreamBuilder(eStreamType_META_APP, mResultMeta_P1_App)
            .build(mContext);
        StreamBuilder(eStreamType_META_HAL, mResultMeta_P1_Hal)
            .build(mContext);

        // Image
        StreamBuilder(eStreamType_IMG_HAL_PROVIDER, mImage_RrzoRaw)
            .setProvider(mRrzoProducer)
            .build(mContext);
        StreamBuilder(eStreamType_IMG_HAL_PROVIDER, mImage_ImgoRaw)
            .setProvider(mImgoProducer)
            .build(mContext);
    }
    //
    // 2. Nodes   ***************
    //
    // 2.a. check if node exist
    // 2.b. setup nodes
    //
    {
        typedef P1Node                  NodeT;
        typedef NodeActor< NodeT >      MyNodeActorT;
        //
        IRIS_LOGD("Nodebuilder p1 +\n");
        IRIS_LOGD("P1 config sensor dev index(%u)", mSensorId);
        NodeT::InitParams initParam;
        {
            initParam.openId   = mSensorId;
            initParam.nodeId   = eNODEID_P1Node;
            initParam.nodeName = "P1Node";
        }
        NodeT::ConfigParams cfgParam;
        {
            cfgParam.pInAppMeta        = mControlMeta_App;
            cfgParam.pInHalMeta        = mControlMeta_Hal;
            cfgParam.pOutAppMeta       = mResultMeta_P1_App;
            cfgParam.pOutHalMeta       = mResultMeta_P1_Hal;
            cfgParam.pOutImage_resizer = mImage_RrzoRaw;
            cfgParam.pvOutImage_full.push_back(mImage_ImgoRaw); //N/A
            cfgParam.sensorParams = mSensorParam;
#if 0 // test the NULL pool parameter
            cfgParam.pStreamPool_resizer = NULL;
            cfgParam.pStreamPool_full    = NULL;
#else
            cfgParam.pStreamPool_resizer = mContext->queryImageStreamPool(mImage_RrzoRaw->getStreamId());
            cfgParam.pStreamPool_full    = mContext->queryImageStreamPool(mImage_ImgoRaw->getStreamId());
#endif

            cfgParam.enableSecurity = isSecurity;
            cfgParam.secType = secType;
            // for dualCamera
            cfgParam.tgNum = 2;
            cfgParam.enableEIS = MTRUE;
        }
        //
        sp<MyNodeActorT> pNode = new MyNodeActorT( NodeT::createInstance() );
        pNode->setInitParam(initParam);
        pNode->setConfigParam(cfgParam);
        //
        StreamSet vIn;
        add_stream_to_set(vIn, mControlMeta_App);
        add_stream_to_set(vIn, mControlMeta_Hal);
        //
        StreamSet vOut;
        add_stream_to_set(vOut, mImage_RrzoRaw);
        add_stream_to_set(vOut, mImage_ImgoRaw);
        add_stream_to_set(vOut, mResultMeta_P1_App);
        add_stream_to_set(vOut, mResultMeta_P1_Hal);
        //
        NodeBuilder aNodeBuilder(eNODEID_P1Node, pNode);

        aNodeBuilder.addStream(NodeBuilder::eDirection_IN , vIn);
        aNodeBuilder.addStream(NodeBuilder::eDirection_OUT, vOut);

        if (mImage_RrzoRaw != 0)
            aNodeBuilder.setImageStreamUsage(mImage_RrzoRaw->getStreamId(), eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
        if (mImage_ImgoRaw != 0)
            aNodeBuilder.setImageStreamUsage(mImage_ImgoRaw->getStreamId(), eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);

        MERROR ret = aNodeBuilder.build(mContext);

        IRIS_LOGD("Nodebuilder p1 -\n");

        if( ret != OK ) {
            IRIS_LOGE("build p1 node error\n");
            return;
        }
    }

    //
    // 3. Pipeline **************
    //
    {
        MERROR ret = PipelineBuilder()
            .setRootNode(
                    NodeSet().add(eNODEID_P1Node)
                    )
            .build(mContext);
        if( ret != OK ) {
            IRIS_LOGE("build pipeline error\n");
            return;
        }
    }
    //
    mResultProcessor = ResultProcessor::createInstance();
    mContext->setDataCallback(mResultProcessor);
    mContext->endConfigure();
}

void IrPath::setupRequestBuilder()
{
    AutoLog();
    {// ONLY P1
        mRequestBuilderP1 = new RequestBuilder();

        mRequestBuilderP1->setIOMap(
                eNODEID_P1Node,
                IOMapSet().add(
                    IOMap()
                    .addOut(mImage_RrzoRaw->getStreamId())
                    .addOut(mImage_ImgoRaw->getStreamId())
                    ),
                IOMapSet().add(
                    IOMap()
                    .addIn(mControlMeta_App->getStreamId())
                    .addIn(mControlMeta_Hal->getStreamId())
                    .addOut(mResultMeta_P1_App->getStreamId())
                    .addOut(mResultMeta_P1_Hal->getStreamId())
                    )
                );
        mRequestBuilderP1->setRootNode(
                NodeSet().add(eNODEID_P1Node)
                );
    }

    mRequestBuilderP1->updateFrameCallback(mResultProcessor);

    // register listener
    mTimestampProcessor = TimestampProcessor::createInstance(mSensorId);

    mResultProcessor->registerListener(
            STREAM_ID_METADATA_RESULT_P1_APP,
            mTimestampProcessor);
    mResultProcessor->registerListener(
            STREAM_ID_METADATA_RESULT_P1_HAL,
            mTimestampProcessor);

    IRIS_LOGD("Raw buffer provider");
    mTimestampProcessor->registerCB(mImgoProducer);
    mTimestampProcessor->registerCB(mRrzoProducer);

    mResultProcessor->registerListener(
            STREAM_ID_METADATA_RESULT_P1_APP,
            mMetaListener);

    mResultProcessor->registerListener(
            STREAM_ID_METADATA_RESULT_P1_HAL,
            mMetaListener);
}

void IrPath::finishPipelineContext()
{
    AutoLog();
    IRIS_LOGD("waitUntilDrained ...\n");
    mContext->waitUntilDrained();
    IRIS_LOGD("waitUntilDrained END\n");
}

void IrPath::onMetaReceived(
        MUINT32         const requestNo,
        StreamId_T      const streamId,
        MBOOL           const errorResult,
        IMetadata       const result
        )
{
    AutoLog();
    IRIS_LOGD("listener onMetaReceived! StreamID %#" PRIx64, streamId);

    auto search = mBufferCallback.find(requestNo);

    if(CC_LIKELY(search != mBufferCallback.end())) {

        if(streamId == STREAM_ID_METADATA_RESULT_P1_APP) {
            search->second.appMeta = result;
            search->second.reqNum = requestNo;

        }
        else if(streamId == STREAM_ID_METADATA_RESULT_P1_HAL) {
            MINT64 timestamp;
            search->second.halMeta = result;

            tryGetMetadata<MINT64>(&(search->second.halMeta), MTK_P1NODE_FRAME_START_TIMESTAMP, timestamp);
            IRIS_LOGD("Metadata openID(%u), requestNum(%d), timeStamp:%" PRId64, mSensorId, requestNo, timestamp);
            // Callback to DepthCallback
            mInputListener->onMetaAvailable(mSensorId, search->second);
            mBufferCallback.erase(requestNo);
        }
   }

}

void IrPath::dumpBuffer(android::sp<IImageBuffer>& pBuffer)
{
    AutoLog();
    char tmp[512];
    static int frameNo = 0;

    int stride = pBuffer->getImgSize().w * Utils::Format::queryPlaneBitsPerPixel(pBuffer->getImgFormat(), 0) / 8;

    IRIS_LOGD("stride : %d", stride);

    ::snprintf(tmp, sizeof(tmp), "%sbuf(%d)__%d_%d_%d_%d.raw", DUMP_PATH, mSensorId, frameNo++,
                                                    pBuffer->getImgSize().w,
                                                    pBuffer->getImgSize().h,
                                                    stride);

    int fd = ::open(tmp, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU);
    if(fd < 0)
    {
        IRIS_LOGE("fail to open %s", tmp);
        return;
    }
    for (size_t i = 0; i < pBuffer->getPlaneCount(); i++)
    {
        MUINT8* pBuf = (MUINT8*)pBuffer->getBufVA(0);
        for (size_t j = 0; j < pBuffer->getImgSize().h; j++)
        {
            ::write(fd, pBuf, stride);
            pBuf += pBuffer->getBufStridesInBytes(0);
        }
    }

    if (fd >= 0) ::close(fd);
}


void IrPath::onDataReceived(
        MUINT32 const requestNo,
        StreamId_T const streamId,
        android::sp<IImageBuffer>& pBuffer
        )
{
    AutoLog();
    IRIS_LOGD("listener onDataReceived! StreamID %#" PRIx64, streamId);
    IRIS_LOGD("Data openID(%u), requestNum(%d), format(%p)", mSensorId, requestNo, pBuffer->getImgFormat());

    // TODO: translate ImageBuffer to callback buffer
    {
        constexpr iris_callback_descriptor_t des = IRIS_CALLBACK_ADDR;

        std::unique_lock<std::mutex> _l(mRegisteredCallbacksLock);
        auto search = mRegisteredCallbacks.find(des);
        void *priv = nullptr;
        // Need to remove registeredCallbacks
        if (search != mRegisteredCallbacks.end())
            priv = search->second.second;
        //if (search != mRegisteredCallbacks.end())
        {
            // TODO: check if these information is correct
            // NOTE: Bayer RAW is single-plane
            Buffer result
            {
                .addr.secureHandle.fd =
                    [&pBuffer, this]
                    {
                        if (!(pBuffer->lockBuf(DEBUG_LOG_TAG, kBufferUsage)))
                        {
                            IRIS_LOGE("Image Buffer lock failed");
                            return -1;
                        }

                        if (mEnableDump) {
                            this->dumpBuffer(pBuffer);
                        }

                        auto fd = pBuffer->getFD();
                        pBuffer->unlockBuf(DEBUG_LOG_TAG);

                        return fd;
                    }(),
                .attribute.identifier = std::make_pair(streamId, requestNo),
                .attribute.length = pBuffer->getBufSizeInBytes(0),
                .attribute.size = pBuffer->getImgSize(),
                .attribute.stride = pBuffer->getBufStridesInBytes(0),
                .attribute.format = pBuffer->getImgFormat(),
                .path = NSCam::security::Path::BAYER,
                .priv = priv
            };
            DepthCallbackInfo depthCbInfo;

            depthCbInfo.fullRaw = result;

            IRIS_LOGD("result FD(%d)", result.addr.secureHandle.fd);
            // We record Buffer information
            // TODO: Remove hard code here, we need to confirm with Ivan which raw is needed by depth
            if(result.attribute.format == eImgFmt_BAYER10_UNPAK) {
                if (mBufferCallback.emplace(requestNo, std::move(depthCbInfo)).second == false)
                    IRIS_LOGE("cannot emplace buffercallback: key(0x%x)", requestNo);
            }

        //    if (search != mRegisteredCallbacks.end())
        //        loadFunctionPointer<IRIS_PFN_CALLBACK_ADDR>(search->second.first)(result);
        }
    #if 0
        pBuffer->lockBuf(DEBUG_LOG_TAG, kBufferUsage);
        dumpBuffer(pBuffer);
        pBuffer->unlockBuf(DEBUG_LOG_TAG);
    #endif
    }
}

void IrPath::setupInitialMeta(IMetadata *appMeta, IMetadata *halMeta)
{
    if(CC_UNLIKELY(appMeta == nullptr || halMeta == nullptr)) {
        IRIS_LOGE("setupInitialMeta fail because of meta pointers are null");
        return;
    }
    //Set AE mode on
    {
        IMetadata::IEntry tag_aemode(MTK_CONTROL_AE_MODE);
        tag_aemode.push_back(MTK_CONTROL_AE_MODE_ON, Type2Type<MUINT8>());
        appMeta->update(MTK_CONTROL_AE_MODE, tag_aemode);
    }

    // Set Flash mode torch
    if(0) {
        IMetadata::IEntry tag_flash_mode(MTK_FLASH_MODE);
        tag_flash_mode.push_back(MTK_FLASH_MODE_TORCH, Type2Type<MUINT8>());
        appMeta->update(MTK_FLASH_MODE, tag_flash_mode);
    }

    // Set hw sync
    {
        IMetadata::IEntry tag_sync_mode(MTK_STEREO_HW_FRM_SYNC_MODE);
        tag_sync_mode.push_back(NS3Av3::ISync3AMgr::E_HW_FRM_SYNC_MODE_ON, Type2Type<MINT32>());
        halMeta->update(MTK_STEREO_HW_FRM_SYNC_MODE, tag_sync_mode);
    }
    // Enable 2A sync
    {
        IMetadata::IEntry tag_2A_mode(MTK_STEREO_SYNC2A_MODE);
        tag_2A_mode.push_back(NS3Av3::ISync3AMgr::E_SYNC2A_MODE_VSDOF, Type2Type<MINT32>());
        halMeta->update(MTK_STEREO_SYNC2A_MODE, tag_2A_mode);
    }
    // Enable AF sync
    {
        IMetadata::IEntry tag_syncaf_mode(MTK_STEREO_SYNCAF_MODE);
        tag_syncaf_mode.push_back(NS3Av3::ISync3AMgr::E_SYNCAF_MODE_ON, Type2Type<MINT32>());
        halMeta->update(MTK_STEREO_SYNCAF_MODE, tag_syncaf_mode);
    }
    //IMetadata::setEntry<MINT32>(halMeta, MTK_STEREO_SYNCAF_MODE, NS3Av3::ISync3AMgr::E_SYNCAF_MODE_ON);

    int32_t mainSensorId = 0, subSensorId = 0;
    StereoSettingProvider::getStereoSensorIndex(mainSensorId, subSensorId);

    IRIS_LOGD("Query main & sub (%d, %d)", mainSensorId, subSensorId);
    // Set up 2A master/slave
    IMetadata::IEntry tag(MTK_STEREO_SYNC2A_MASTER_SLAVE);
    tag.push_back(mainSensorId, Type2Type<MINT32>()); // Master
    tag.push_back(subSensorId, Type2Type<MINT32>()); // Slave
    halMeta->update(MTK_STEREO_SYNC2A_MASTER_SLAVE, tag);

    return;
}

    sp<ImageStreamInfo>
createImageStreamInfo(
        char const*         streamName,
        StreamId_T          streamId,
        MUINT32             streamType,
        size_t              maxBufNum,
        size_t              minInitBufNum,
        MUINT               usageForAllocator,
        MINT                imgFormat,
        MSize const&        imgSize,
        MUINT32             transform
        )
{
    AutoLog();
    IImageStreamInfo::BufPlanes_t bufPlanes;
#define addBufPlane(planes, height, stride)                                      \
    do{                                                                      \
        size_t _height = (size_t)(height);                                   \
        size_t _stride = (size_t)(stride);                                   \
        IImageStreamInfo::BufPlane bufPlane= { _height * _stride, _stride }; \
        planes.push_back(bufPlane);                                          \
    }while(0)
    switch( imgFormat ) {
        case eImgFmt_YV12:
            addBufPlane(bufPlanes , imgSize.h      , imgSize.w);
            addBufPlane(bufPlanes , imgSize.h >> 1 , imgSize.w >> 1);
            addBufPlane(bufPlanes , imgSize.h >> 1 , imgSize.w >> 1);
            break;
        case eImgFmt_NV21:
            addBufPlane(bufPlanes , imgSize.h      , imgSize.w);
            addBufPlane(bufPlanes , imgSize.h >> 1 , imgSize.w);
            break;
        case eImgFmt_YUY2:
            addBufPlane(bufPlanes , imgSize.h      , imgSize.w << 1);
            break;
        case eImgFmt_BLOB:
            /*
               add 328448 for image size
               standard exif: 1280 bytes
               4 APPn for debug exif: 0xFF80*4 = 65408*4 bytes
               max thumbnail size: 64K bytes
             */
            addBufPlane(bufPlanes , 1              , (imgSize.w * imgSize.h * 12 / 10) + 328448); //328448 = 64K+1280+65408*4
            break;
        default:
            IRIS_LOGE("format not support yet 0x%x \n", imgFormat);
            break;
    }
#undef  addBufPlane

    sp<ImageStreamInfo>
        pStreamInfo = new ImageStreamInfo(
                streamName,
                streamId,
                streamType,
                maxBufNum, minInitBufNum,
                usageForAllocator, imgFormat, imgSize, bufPlanes, transform
                );

    if( pStreamInfo == NULL ) {
        IRIS_LOGE("create ImageStream failed, %s, %ld \n",
                streamName, streamId);
    }

    return pStreamInfo;
}

    sp<ImageStreamInfo>
createRawImageStreamInfo(
        char const*         streamName,
        StreamId_T          streamId,
        MUINT32             streamType,
        size_t              maxBufNum,
        size_t              minInitBufNum,
        MUINT               usageForAllocator,
        MINT                imgFormat,
        MSize const&        imgSize,
        size_t const        stride
        )
{
    AutoLog();
    IImageStreamInfo::BufPlanes_t bufPlanes;
    //
#define addBufPlane(planes, height, stride)                                      \
    do{                                                                      \
        size_t _height = (size_t)(height);                                   \
        size_t _stride = (size_t)(stride);                                   \
        IImageStreamInfo::BufPlane bufPlane= { _height * _stride, _stride }; \
        planes.push_back(bufPlane);                                          \
    }while(0)
    switch( imgFormat ) {
        case eImgFmt_BAYER10:
        case eImgFmt_FG_BAYER10:
        case eImgFmt_BAYER10_UNPAK:
            addBufPlane(bufPlanes , imgSize.h, stride);
            break;
        default:
            IRIS_LOGE("format not support yet 0x%x \n", imgFormat);
            break;
    }
#undef  addBufPlane

    sp<ImageStreamInfo>
        pStreamInfo = new ImageStreamInfo(
                streamName,
                streamId,
                streamType,
                maxBufNum, minInitBufNum,
                usageForAllocator, imgFormat, imgSize, bufPlanes, 0, 0
                );

    if( pStreamInfo == NULL ) {
        IRIS_LOGE("create ImageStream failed, %s, %ld \n",
                streamName, streamId);
    }

    return pStreamInfo;
}
