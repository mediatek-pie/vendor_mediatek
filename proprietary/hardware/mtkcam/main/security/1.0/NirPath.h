/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#ifndef _NIR_PATH_H_
#define _NIR_PATH_H_

#include "IPath.h"

#ifndef LEGACY_PATH // non-legacy path
#include <mtkcam/utils/imgbuf/ImageBufferHeap.h>
#else // legacy path
#include <mtkcam/utils/ImageBufferHeap.h>
#endif // legacy path

#include <utils/StrongPointer.h>

#include <semaphore.h>

#include <unordered_map>
#include <tuple>

#include <atomic>
#include <mutex>
#include <condition_variable>

#include <thread>
#include <future>

namespace NSCam {

class IHalSensor;
struct SensorStaticInfo;

namespace NSIoPipe {
namespace NSCamIOPipe {
#ifndef LEGACY_PATH // non-legacy path
    class INormalPipe;
#else
    class INormalPipe_FrmB;
#endif
} // namespace NSCamIOPipe
} // namespace NSIoPipe

namespace Utils {
namespace Sync {
    class ITimeline;
} // namespace Sync
} // namespace Utils

namespace v3 {
    class HwEventIrq;
} // namespace v3

} // namespace NSCam

#ifdef LEGACY_PATH // legacy path
namespace NS3Av3 {
    class IEventIrq;
}
#endif

namespace NSCam {
namespace security {

// ---------------------------------------------------------------------------

class NirPath;
static void enqueLoop(NirPath& camera);
static void mainLoop(NirPath& camera);
static void onFrameDrop(uint32_t frameNumber, void* cookie);

// ---------------------------------------------------------------------------

// TODO: assure all APIs are thread-safe
class NirPath final : public IPath
{
public:
    NirPath(int maxBuffers);

    ~NirPath();

    // interface of IPath
    void destroyInstance() override;

    Result init(const std::unordered_map<StreamID, IrisStream>& streamMap) override;

    Result unInit() override;

    Result setSrcDev(unsigned int devID) override;

    Result setShutterTime(uint32_t time);

    Result setGainValue(uint32_t value);

    Result sensorPower(bool turnON);

    Result StreamingOn() override;

    Result StreamingOff() override;

    Result getSensorSetting(NSCam::security::SensorStaticInfo& sensorSetting) override;

    std::vector<StreamInfo> getStreamInfo(
            const NSCam::security::SensorStaticInfo& sensorSetting,
            const std::vector<StreamID>& identifiers) override;

    // interface of IrisBufferQueue::ProducerListener
    void onBufferReleased() override;

    Result StreamingProc();

    Result StreamingEnque();

private:
    friend void enqueLoop(NirPath& camera);
    friend void mainLoop(NirPath& camera);
    friend void onFrameDrop(uint32_t frameNumber, void* cookie);

    sem_t mSemMainLoop;
    sem_t mSemMainLoopDone;
    sem_t mSemEnqueLoop;
    sem_t mSemEnqueLoopDone;

    std::pair<State, std::mutex> mState;

    // stream map is decided at init() and invalidated at unInit()
    // NOTE: for simplicity and efficiency, map can be regarded as READ ONLY
    //       after init() and before unInit() so as to no need to adopt lock
    mutable std::mutex mStreamMapLock;
    std::unordered_map<StreamID, IrisStream> mStreamMap;

    // NOTE:
    // 1. key_type is ION file descriptor and is the same as IrisBuffer.ion_fd
    // 2. this map records
    //    a. frame number used for ISP's deque() and enque()
    //    b. stream ID representing the stream type
    //    c. inflight buffers
    //
    //    and removes them either
    //    - after buffer is filled and returned
    //    or
    //    - buffer is dropped by ISP
    // 3. this map may exist different streams
    using InflightFrameMap =
        std::unordered_map<int,
        std::tuple<uint32_t, StreamID, IrisBufferQueue::IrisBuffer>>;
    mutable std::mutex mMapLock;
    std::condition_variable mMapCondition;
    InflightFrameMap mMap;

    mutable std::mutex mTimelineLock;
    android::sp<NSCam::Utils::Sync::ITimeline> mTimeline;
    int mTimelineCounter;

#ifndef LEGACY_PATH // non-legacy path
    NSCam::NSIoPipe::NSCamIOPipe::INormalPipe *mNormalPipe;
#else
    NSCam::NSIoPipe::NSCamIOPipe::INormalPipe_FrmB *mNormalPipe;
    NS3Av3::IEventIrq *mEventIrq;
#endif

    // per-frame ISP enqueued counter (start from 1)
    // NOTE: different streams use the same count value per frame
    uint32_t mEnqCount;

    // NOTE:
    // BufCtl records the newly allocated buffers before streaming and
    // works as a FIFO cache list to reuse the allocated buffers efficiently
    // during streaming
    struct BufCtl
    {
        std::vector<android::sp<NSCam::IImageBuffer>> mvBuffer;

        virtual void clear();

        virtual ~BufCtl() { clear(); }
    };

    BufCtl       mFullSizedBufCtrl;
    BufCtl       mResizedBufCtrl;

#ifdef LEGACY_PATH // legacy path
    class DummyBufCtl final : public BufCtl
    {
    public:
        DummyBufCtl() : mIsSecure(false) {};
        ~DummyBufCtl();

        Result create(const int dataSize, const int bufferCount,
                bool isSecure = false);
        void clear() override;

        // read-only access to an item at a given index
        inline NSCam::IImageBuffer* itemAt(size_t index) const;

        // read-only access to an item at a given index
        inline const Iris::IrisBufferQueue::IrisBuffer* secureItemAt(
                size_t index) const;

    private:
        union Buffer
        {
            // blob format and access by offseting the base address
            android::sp<NSCam::IImageBuffer> buffer;
            // store one or more separate buffers allocated via BufferQueue
            std::vector<Iris::IrisBufferQueue::IrisBuffer> buffers;

            Buffer() : buffer(nullptr) {}
            ~Buffer() { buffer = nullptr; buffers.clear(); }
        } mBufferList;

        mutable std::mutex mDummyStreamLock;
        IrisStream mDummyStream;

        std::atomic<bool> mIsSecure;
    };

    DummyBufCtl mFullSizedDummyBufCtrl;
    DummyBufCtl mResizedDummyBufCtrl;
#endif

    int mBufNum;

    // sensor scenario ID in IHalSensor.h
    uint32_t mSensorScenario;

    mutable std::mutex mSensorStaticInfoLock;
    std::unique_ptr<NSCam::SensorStaticInfo> mSensorStaticInfo;

    // sensro HAL and sensor dev are protected via mutex
    // TODO: check if need to call IHalSensor::destroyInstance()
    //       when destruction
    std::pair<NSCam::IHalSensor*, std::mutex> mSensorHAL;

    std::pair<NSCam::v3::HwEventIrq*, std::mutex> mHwEventIrq;

    // NOTE: the future's result is the dropped frame number
    mutable std::mutex mFrameDropHandlerLock;
    std::vector<std::future<uint32_t>> mFrameDropHandler;

    std::atomic<int> mDevID;

    std::pair<unsigned int, std::mutex> mSensorArray;

    std::thread mControlWorker;
    std::thread mEnqueWorker;

    std::pair<bool, std::mutex> mRequestExit;

    void setState(const State newState);
    State getState();

    Result handleFrameDrop(const uint32_t frameNumber);

    // NormalPipe shim
    enum class PipeCommand
    {
        GET_CURRENT_START_OF_FRAME_INDEX,
    };

    template <typename T>
    Result configPipe(T& normalPipe, NSCam::security::SensorStaticInfo& sensorSetting);

    template <typename T, typename INFO>
    Result dequeue(T& normalPipe, INFO& outBufferInfo);

    template <typename T>
    Result sendCommand(T& normalPipe, PipeCommand command,
            intptr_t arg1, intptr_t arg2, intptr_t arg3);

    // start-of-frame event is used for buffer flow control
    template <typename T>
    Result waitStartOfFrame(T& normalPipe);
}; // class NirPath

} // namespace security
} // namespace NSCam

#endif // _NIR_PATH_H_
