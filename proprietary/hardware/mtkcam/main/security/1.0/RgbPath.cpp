#define DEBUG_LOG_TAG "RgbPath"
//
#include <stdlib.h>
#include <utils/Errors.h>
#include <utils/List.h>
#include <mtkcam/utils/hw/HwInfoHelper.h>
#include "RgbPath.h"
#include <mtkcam/utils/std/TypeTraits.h>
#include <mtkcam/utils/std/Format.h>
#include <mtkcam/utils/std/Misc.h>
#include <mtkcam/utils/imgbuf/ISecureImageBufferHeap.h>
#include <mtkcam/aaa/IIspMgr.h>

//
#include <ion/ion.h>
#include <ion.h>
#include <linux/mtk_ion.h>
#include <linux/ion_drv.h>
//

#include <sstream>

// NOTE: This feature dumps buffers during the execution of camera streaming.
// WARNING: Please be aware that this feature disables secure mode
//          and the camera streaming executes in the REE (Rich Execution Environment).
//          SHOULD ENABLED this feature only for debugging purpose.
//#define DEBUG_DUMP

#ifdef DEBUG_DUMP
#define DUMP_PATH "/sdcard/raw/"
#endif // DEBUG_DUMP

using namespace NSCam::security;
using namespace NSCam::v1::NSLegacyPipeline;

// ---------------------------------------------------------------------------

// NOTE: When MTK TEE (GenieZone) is enabled,
//       streaming data from image sensor is written into
//       the secure memory region allocated from MTK TEE.
//
// WARNING: If DEBUG_DUMP is enabled, the streaming data is exposed to REE.
#if defined(USING_MTK_TEE) && !defined(DEBUG_DUMP)
static const bool isSecurity = true;
static const SecType secType = SecType::mem_protected;
#elif defined(DEBUG_DUMP)
#warning "DEBUG_DUMP is enabled, the streaming data is exposed to REE."
static const bool isSecurity = false;
static const SecType secType = SecType::mem_normal;
#else
#error "Unsupported secure configuration"
#endif

static constexpr int kBufferUsage =
    (eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_SW_WRITE_OFTEN);
static const MSize kResizedRAWSize(640, 480);
static constexpr EImageFormat kYUVFormat = eImgFmt_NV12;

static int g_ion_fd;
static ion_user_handle_t g_ionHandle;
// ---------------------------------------------------------------------------

static
MVOID add_stream_to_set( StreamSet& set, sp<IStreamInfo> pInfo ) {
    if( pInfo.get() ) set.add(pInfo->getStreamId());
}

template<typename PFN>
static inline PFN loadFunctionPointer(iris_callback_function_pointer_t func)
{
    return reinterpret_cast<PFN>(func);
}

// TODO: add this helper into HwInfoHelper
static constexpr unsigned int getRAWBitDepth(int rawSensorBit)
{
    switch (rawSensorBit)
    {
        case RAW_SENSOR_8BIT:
            return 8;
        case RAW_SENSOR_10BIT:
            return 10;
        case RAW_SENSOR_12BIT:
            return 12;
        case RAW_SENSOR_14BIT:
            return 14;
        default:
            IRIS_LOGE("unknown raw sensor bit(0x%x), return 10 bit", rawSensorBit);
            return 10;
    }
}

// NOTE: for performance's sake, we reset the stream's state
//       rather than invoking str(""); the latter one incurs reallocation.
static void resetostringstream(std::ostringstream& stream)
{
    // reset the stream's internal error state flags
    stream.clear();
    // set position to the beginning in output sequence
    stream.seekp(0);
}

// ---------------------------------------------------------------------------

static void NSCam::security::StreamingLoop(RgbPath& camera)
{
    AutoLog();

    bool stop = false;

    while(!stop) {
        IRIS_LOGD("[%s] wait event", __FUNCTION__);
        sem_wait(&camera.mSemStreamLoop);

        State eState = camera.getState();
        IRIS_LOGD("[%s] receive event: state(0x%04x)", __FUNCTION__, eState);
        switch (eState)
        {
            case State::PREVIEW:
                camera.processRequest();
                stop = true;
                sem_post(&camera.mSemStreamLoopDone);
                break;
            case State::PREVIEW_STOP:
                sem_post(&camera.mSemStreamLoopDone);
                break;
            case State::UNINIT:
                IRIS_LOGD("uninit thread");
                break;
            case State::EXIT:
                stop = true;
                IRIS_LOGD("exit thread(enable stop)");
                break;
            default:
                IRIS_LOGD("Unknown state(0x%04x)", eState);
                break;
        }
        if (stop == true)
        {
            IRIS_LOGD("exit thread %s", __FUNCTION__);
            break;
        }
    }

}

static MUINT32 generateSecureStatusHandle() {
    AutoLog();

    g_ion_fd = mt_ion_open(DEBUG_LOG_TAG);
    size_t pageSize = sysconf(_SC_PAGESIZE);

    // struct {
    //  MUINT32 status;
    //  E_SEC_SCEN_ID secid;
    // } SecMgr_ChkInfo;

    size_t StatusBufSize = 16;

    int sec_type_flags = (secType == SecType::mem_secure) ? (int)(ION_HEAP_MULTIMEDIA_TYPE_2D_FR_MASK) : (int)(ION_HEAP_MULTIMEDIA_TYPE_PROT_MASK);

    if (ion_alloc(g_ion_fd, StatusBufSize, pageSize,
        sec_type_flags, 0, &g_ionHandle)) {
        IRIS_LOGE("ion_alloc failed!");
        return -1;
    }

    struct ion_sys_data sys_data;
    sys_data.sys_cmd = ION_SYS_GET_PHYS;
    sys_data.get_phys_param.handle = g_ionHandle;
    if (ion_custom_ioctl(g_ion_fd, ION_CMD_SYSTEM, &sys_data))
    {
        IRIS_LOGE("Secure Status : ion_custom_ioctl failed to get physical address");
        return -1;
    }
    else
    {
        IRIS_LOGI("Secure Status buffer allocated OK, handle(0x%x), size(%lu)",
                sys_data.get_phys_param.phy_addr, sys_data.get_phys_param.len);
        return sys_data.get_phys_param.phy_addr;
    }
}

static void clearSecureStatusHandle() {
    AutoLog();

    ion_free(g_ion_fd, g_ionHandle);
    ion_close(g_ion_fd);
}

// ---------------------------------------------------------------------------

RgbPath::RgbPath(int maxBuffers)
: mpSensorHalObj(nullptr),
    mSensorId(SENSOR_DEV_NONE),
    mRequestTemplate(CAMERA3_TEMPLATE_PREVIEW),
    mRrzoFormat(eImgFmt_FG_BAYER10),
    mImgoFormat(eImgFmt_BAYER10),
    mYuvFormat(kYUVFormat),
    mContext(nullptr),
    mControlMeta_App(nullptr),
    mControlMeta_Hal(nullptr),
    mResultMeta_P1_App(nullptr),
    mResultMeta_P1_Hal(nullptr),
    mResultMeta_P2_App(nullptr),
    mResultMeta_P2_Hal(nullptr),
    mImage_RrzoRaw(nullptr),
    mImage_ImgoRaw(nullptr),
    mImage_LcsoRaw(nullptr),
    mImage_Yuv(nullptr),
    mFullRawPool(nullptr),
    mResizedRawPool(nullptr),
    mLcsoRawPool(nullptr),
    mYuvPool(nullptr),
    mImgoProducer(nullptr),
    mRrzoProducer(nullptr),
    mLcsoProducer(nullptr),
    mYuvProducer(nullptr),
    mCallbackHandler(nullptr),
    mCallback(nullptr),
    mRequestBuilderP1(nullptr),
    mRequestBuilderPrv(nullptr),
    mResultProcessor(nullptr),
    mTimestampProcessor(nullptr),
    mSensorScenario(SENSOR_SCENARIO_ID_NORMAL_PREVIEW),
    mExitRequest(false)
{
    IRIS_TRACE_CALL();
    AutoLog();

    mState.first = State::NONE;
    // init semaohore
    sem_init(&mSemStreamLoop, 0, 0);
    sem_init(&mSemStreamLoopDone, 0, 0);

#ifdef DEBUG_DUMP
    mEnableDump = ::property_get_int32("vendor.securecamera.dump", 0);
    if (mEnableDump)
    {
        if (!NSCam::Utils::makePath(DUMP_PATH, 0660))
        {
            IRIS_LOGE("create folder <%s> fail", DUMP_PATH);
        }
    }
#endif

    mSensorStaticInfo.reset(new NSCam::SensorStaticInfo());

}

RgbPath::~RgbPath()
{
    IRIS_TRACE_CALL();
    AutoLog();

    // destroy semaphore
    sem_destroy(&mSemStreamLoop);
    sem_destroy(&mSemStreamLoopDone);
}

void RgbPath::destroyInstance()
{
    IRIS_TRACE_CALL();
    AutoLog();

    {
        setState(State::EXIT);

        if (mStreamWorker.joinable())
        {
            sem_post(&mSemStreamLoop);
            IRIS_LOGD("join streaming worker +");
            mStreamWorker.join();
            IRIS_LOGD("join streaming worker -");
        }
    }
}

void RgbPath::onBufferReleased()
{
    IRIS_TRACE_CALL();
    AutoLog();
}

void RgbPath::registerCallback(Callback* cb, void* priv)
{
    IRIS_TRACE_CALL();
    AutoLog();

    if (cb == nullptr)
    {
        IRIS_LOGE("invalid callback");
        return;
    }

    const auto key(cb->getDescriptor());
    std::unique_lock<std::mutex> _l(mRegisteredCallbacksLock);
    auto search = mRegisteredCallbacks.find(key);
    if (CC_UNLIKELY(search != mRegisteredCallbacks.end()))
    {
        IRIS_LOGW("callback key(0x%x) already exist, overwrite it", key);
        mRegisteredCallbacks.erase(key);
    }

    if (mRegisteredCallbacks.emplace(key, std::make_pair(cb->getHook(), priv)).second == false)
        IRIS_LOGE("cannot emplace callback: key(0x%x)", key);
}

Result RgbPath::init(const std::unordered_map<StreamID, IrisStream>& streamMap)
{
    IRIS_TRACE_CALL();
    AutoLog();

    setState(State::INIT);

    // a worker thread of sumbiting request
    mStreamWorker = std::thread(StreamingLoop, std::ref(*this));

    prepareConfiguration();

    setupMetaStreamInfo();

    setupImageStreamInfo();

    setupRawBufferPool();

    setupPipelineContext();

    setupRequestBuilder();

    setState(State::IDLE);

    return OK;
}

Result RgbPath::unInit()
{
    IRIS_TRACE_CALL();
    AutoLog();

    Result err = OK;

    State eState = getState();

    IRIS_LOGD("eState(0x%04x)", eState);
    if (eState != State::NONE)
    {
        if ((eState != State::IDLE) && (eState != State::ERROR))
        {
            IRIS_LOGD("Camera is not in the idle state");
            if (toLiteral(eState) & toLiteral(State::PREVIEW_MASK))
            {
                err = StreamingOff();
                if (err != OK)
                {
                    IRIS_LOGE("streamingPreviewStop fail(0x%x)", err);
                }
            }

            while (eState != State::IDLE)
            {
                usleep(10000);
                eState = getState();
            }
            IRIS_LOGD("Now camera is in the idle state");
        }
        //in case only init and uninit flow
        if (mContext.get())
        {
            // stop pipeline asynchronously to avoid queuing redundant capture requests
            auto future = std::async(std::launch::async, [this] {
                        this->finishPipelineContext();
                    });
        }

        setState(State::UNINIT);
        IRIS_LOGD("trigger streaming loop");
        sem_post(&mSemStreamLoop);
    }
    else
    {
        setState(State::UNINIT);
    }

    {
        std::unique_lock<std::mutex> _l(mRegisteredCallbacksLock);
        mRegisteredCallbacks.clear();
    }

    if (mResultProcessor.get())
    {
        mResultProcessor->flush();
        mResultProcessor = nullptr;
    }

    mTimestampProcessor = nullptr;
    mRequestBuilderP1 = nullptr;
    mRequestBuilderPrv = nullptr;
    mCallbackHandler = nullptr;

    IHalSensorList* const pHalSensorList = MAKE_HalSensorList();

    MUINT32 sensorArray[1] = {mSensorId};
    mpSensorHalObj->powerOff(DEBUG_LOG_TAG, 1, &sensorArray[0]);
    if (isSecurity) {
        pHalSensorList->disableSecure(DEBUG_LOG_TAG);
        clearSecureStatusHandle();
    }
    mpSensorHalObj->destroyInstance(DEBUG_LOG_TAG);
    mpSensorHalObj = nullptr;


    return err;
}

Result RgbPath::setShutterTime(uint32_t time)
{
    IRIS_TRACE_CALL();

    return OK;
}

Result RgbPath::setGainValue(uint32_t value)
{
    IRIS_TRACE_CALL();

    return OK;
}

Result RgbPath::StreamingOn()
{
    IRIS_TRACE_CALL();
    AutoLog();

    Result err = OK;

    if (getState() != State::IDLE)
    {
        IRIS_LOGE("Camera state is not IDLE");
        return INVALID_OPERATION;
    }

    IRIS_LOGD("Preview state(0x%x)", State::PREVIEW);
    setState(State::PREVIEW);
    IRIS_LOGD("trigger streaming loop");
    sem_post(&mSemStreamLoop);

    return err;
}

Result RgbPath::StreamingOff()
{
    IRIS_TRACE_CALL();
    AutoLog();

    State state = getState();
    IRIS_LOGD("state(0x%04x)", state);

    if (state == State::IDLE)
    {
        IRIS_LOGE("is in IDLE state");
        return INVALID_OPERATION;
    }

    // request streaming worker thread to exit
    mExitRequest.store(true, memory_order_relaxed);

    if (state != State::PREVIEW_STOP)
    {
        setState(State::PREVIEW_STOP);
    }

    // pipeline context is released after capture request thread is done
    {
        IRIS_TRACE_NAME("waitStreamingLoopDone");
        IRIS_LOGD("wait streaming loop...");
        sem_wait(&mSemStreamLoopDone);
        IRIS_LOGD("wait streaming loop done");
    }

    // stop pipeline asynchronously to avoid queuing redundant capture requests
    auto future = std::async(std::launch::async, [this] {
                this->finishPipelineContext();
            });

    setState(State::IDLE);

    return OK;
}

sp<IMetaStreamBuffer>
RgbPath::get_default_request()
{
    IRIS_TRACE_CALL();
    AutoLog();

    sp<IMetaStreamBuffer> pSBuffer;

    ITemplateRequest* obj = NSTemplateRequestManager::valueFor(mSensorId);
    if(obj == NULL) {
        obj = ITemplateRequest::getInstance(mSensorId);
        NSTemplateRequestManager::add(mSensorId, obj);
    }
    IMetadata meta = obj->getMtkData(mRequestTemplate);
    //
    pSBuffer = createMetaStreamBuffer(mControlMeta_App, meta, false);
    //
    return pSBuffer;
}

IMetaStreamBuffer*
RgbPath::createMetaStreamBuffer(
        android::sp<IMetaStreamInfo> pStreamInfo,
        IMetadata const& rSettings,
        MBOOL const repeating
        )
{
    IRIS_TRACE_CALL();
    AutoLog();

    HalMetaStreamBuffer*
        pStreamBuffer =
        HalMetaStreamBuffer::Allocator(pStreamInfo.get())(rSettings);
    //
    pStreamBuffer->setRepeating(repeating);
    //
    return pStreamBuffer;
}

void RgbPath::setState(const State newState)
{
    std::lock_guard<std::mutex> _l(mState.second);

    IRIS_LOGD("Now(0x%04x), Next(0x%04x)", mState.first, newState);
    if (newState == State::ERROR)
        goto EXIT;

    switch (mState.first)
    {
        case State::NONE:
            switch (newState)
            {
                case State::INIT:
                case State::UNINIT:
                    break;
                default:
                    IRIS_LOGA("State error NONE");
            }
            break;
        case State::INIT:
            switch (newState)
            {
                case State::IDLE:
                    break;
                default:
                    IRIS_LOGA("State error INIT");
            }
            break;
        case State::IDLE:
            switch (newState)
            {
                case State::IDLE:
                case State::PREVIEW:
                case State::UNINIT:
                    break;
                default:
                    IRIS_LOGA("State error IDLE");
            }
            break;
        case State::PREVIEW:
            switch (newState)
            {
                case State::IDLE:
                case State::PREVIEW:
                case State::PREVIEW_STOP:
                    break;
                default:
                    IRIS_LOGA("State error PREVIEW");
                    break;
            }
            break;
        case State::PREVIEW_STOP:
            switch (newState)
            {
                case State::IDLE:
                    break;
                default:
                    IRIS_LOGA("State error PREVIEW_STOP");
            }
            break;
        case State::UNINIT:
            switch (newState)
            {
                case State::EXIT:
                    break;
                default:
                    IRIS_LOGA("State error UNINIT");
            }
            break;
        case State::EXIT:
            IRIS_LOGD("Exit state");
            break;
        case State::ERROR:
            switch (newState)
            {
                case State::IDLE:
                case State::UNINIT:
                    break;
                default:
                    IRIS_LOGA("State error IRIS_MHAL_ERROR");
                    break;
            }
            break;
        default:
            IRIS_LOGE("Unknown state Now(0x%04x), Next(0x%04x)",
                    mState.first, newState);
    }

EXIT:
    mState.first = newState;
    IRIS_LOGD("Now(0x%04x)", mState.first);

}

State RgbPath::getState()
{
    std::lock_guard<std::mutex> _l(mState.second);
    return mState.first;
}

void RgbPath::processRequest()
{
    AutoLog();

    int current_cnt = 0;
    std::ostringstream stringStream;
    while ((toLiteral(getState()) & toLiteral(State::PREVIEW_MASK)) || (current_cnt == 0))
    {
        resetostringstream(stringStream);
        stringStream << "request(" << current_cnt << ")";
        IRIS_TRACE_NAME(stringStream.str().c_str());
        //
        IRIS_LOGD("request %d +", current_cnt);
        //
        sp<IPipelineFrame> pFrame;
        //
        sp<IMetaStreamBuffer> pAppMetaControlSB = get_default_request();
        sp<HalMetaStreamBuffer> pHalMetaControlSB =
            HalMetaStreamBuffer::Allocator(mControlMeta_Hal.get())();
        {
            // modify hal control metadata
            IMetadata* pMetadata = pHalMetaControlSB->tryWriteLock(DEBUG_LOG_TAG);
            IMetadata::setEntry<MSize>(pMetadata, MTK_HAL_REQUEST_SENSOR_SIZE, mSensorParam.size);

            pHalMetaControlSB->unlock(DEBUG_LOG_TAG, pMetadata);
        }
        {
            sp<RequestBuilder> pRequestBuilder =
                mYuvProducer.get() ? mRequestBuilderPrv : mRequestBuilderP1;

            if (pRequestBuilder.get())
            {
                pRequestBuilder->setMetaStreamBuffer(
                        mControlMeta_App->getStreamId(),
                        pAppMetaControlSB
                        );
                pRequestBuilder->setMetaStreamBuffer(
                        mControlMeta_Hal->getStreamId(),
                        pHalMetaControlSB
                        );
                pFrame = pRequestBuilder->build(current_cnt, mContext);
            }
            if( ! pFrame.get() )
            {
                IRIS_LOGE("build request failed");
            }
        }
        if (0)  // Dump All MetaData
        {
            IMetadataTagSet const &mtagInfo = IDefaultMetadataTagSet::singleton()->getTagSet();
            sp<IMetadataConverter> mMetaDataConverter = IMetadataConverter::createInstance(mtagInfo);

            IRIS_LOGD("\n\nDump AppMeta:\n");
            IRIS_LOGD("==========================================================\n");
            IMetadata* pMetadata = pAppMetaControlSB->tryReadLock(DEBUG_LOG_TAG);
            pMetadata->dump();
            IRIS_LOGD("==========================================================\n");
            mMetaDataConverter->dumpAll(*pMetadata);
            pAppMetaControlSB->unlock(DEBUG_LOG_TAG, pMetadata);
            IRIS_LOGD("==========================================================\n");
            IRIS_LOGD("\n\nDump HalMeta:\n");
            IRIS_LOGD("==========================================================\n");
            pMetadata = pHalMetaControlSB->tryReadLock(DEBUG_LOG_TAG);
            pMetadata->dump();
            //IRIS_LOGD("==========================================================\n");
            //mMetaDataConverter->dumpAll(*pMetadata);  // Not Support
            pHalMetaControlSB->unlock(DEBUG_LOG_TAG, pMetadata);
            IRIS_LOGD("==========================================================\n");
        }
        //
        if( pFrame.get() )
        {
            if (mExitRequest.load(memory_order_relaxed))
            {
                IRIS_LOGD("queue request %d to exit, skip queuing...", current_cnt);
                mExitRequest.store(false, std::memory_order_relaxed);
            }
            else if ( OK != mContext->queue(pFrame) )
            {
                IRIS_LOGE("queue pFrame failed\n");
            }
        }

        IRIS_LOGD("request %d -", current_cnt);
        current_cnt++;
    }
}

Result RgbPath::getSensorSetting(NSCam::security::SensorStaticInfo& sensorSetting)
{
    AutoLog();
    return OK;
}

std::vector<StreamInfo> RgbPath::getStreamInfo(
        const NSCam::security::SensorStaticInfo& sensorSetting,
        const std::vector<StreamID>& identifiers)
{
    AutoLog();
    std::vector<StreamInfo> streamInfos;
    return streamInfos;
}

Result RgbPath::setSrcDev(unsigned int devID)
{
    IRIS_TRACE_CALL();
    AutoLog();

    mDevID.store(devID, memory_order_relaxed);
    IRIS_LOGD("sensor ID : %d", mSensorId);

    IHalSensorList* const pHalSensorList = MAKE_HalSensorList();
    if (isSecurity)
    {
        IRIS_LOGD("enable secure sensor");
        pHalSensorList->enableSecure(DEBUG_LOG_TAG);
    }

    mSensorId = UINT_MAX;
    const unsigned int sensors = pHalSensorList->queryNumberOfSensors();
    for (unsigned int i = 0; i < sensors; i++)
    {
        if (pHalSensorList->querySensorDevIdx(i) != devID)
        {
            IRIS_LOGD("sensor dev ID(%u) idx(%u)",
                    devID, pHalSensorList->querySensorDevIdx(i));
            continue;
        }

        mSensorId = i;
        break;
    }
    IRIS_LOGD("sensor dev index(%u)", mSensorId);

    if (mSensorId == UINT_MAX)
    {
        IRIS_LOGE("Sensor Not found");
        return NAME_NOT_FOUND;
    }

    mpSensorHalObj = pHalSensorList->createSensor(DEBUG_LOG_TAG, 1, &mSensorId);
    if (!mpSensorHalObj)
    {
        IRIS_LOGE("create sensor failed");
        exit(1);
        return BAD_VALUE;
    }

    // update sensor imformation
    {
        std::lock_guard<std::mutex> _l(mSensorStaticInfoLock);
        pHalSensorList->querySensorStaticInfo(devID, mSensorStaticInfo.get());

        IRIS_LOGD("sensor dev(%d) index(%u) bitDepth(%u)",
                devID, mSensorId,
                getRAWBitDepth(mSensorStaticInfo->rawSensorBit));
    }

    // get sensor setting

    NSCamHW::HwInfoHelper helper(mSensorId);

    if (!helper.updateInfos())
    {
        IRIS_LOGE("update sensor static information failed");
        return BAD_VALUE;
    }

    if (!helper.getSensorSize(mSensorScenario, mSensorParam.size) ||
            !helper.getSensorFps(mSensorScenario, (MINT32&)mSensorParam.fps) ||
            !helper.queryPixelMode(
                mSensorScenario, mSensorParam.fps, mSensorParam.pixelMode))
    {
        IRIS_LOGE("wrong parameter for IMGO");
        return BAD_VALUE;
    }

    IRIS_LOGD("sensor params mode %d, size %dx%d, fps %d, pixelmode %d\n",
            mSensorParam.mode,
            mSensorParam.size.w, mSensorParam.size.h,
            mSensorParam.fps,
            mSensorParam.pixelMode);

    mpSensorHalObj->powerOn(DEBUG_LOG_TAG, 1, &mSensorId);

    return OK;
}

Result RgbPath::sensorPower(bool turnOn)
{
    return OK;
}

void RgbPath::prepareConfiguration()
{
    IRIS_TRACE_CALL();
    AutoLog();
    //
    {
        IRIS_LOGD("pMetadataProvider ...\n");
        sp<IMetadataProvider> pMetadataProvider = IMetadataProvider::create(mSensorId);
        if (pMetadataProvider.get() != NULL) {
            IRIS_LOGD("pMetadataProvider (%p) +++\n", pMetadataProvider.get());
        }
        NSMetadataProviderManager::add(mSensorId, pMetadataProvider.get());
        if (pMetadataProvider.get() != NULL) {
            IRIS_LOGD("pMetadataProvider (%p) ---\n", pMetadataProvider.get());
        }

        IRIS_LOGD("sensor index(%u) sensorOrientation(%d)",
                mSensorId, pMetadataProvider->getDeviceSetupOrientation());
    }
    {
        ITemplateRequest* obj = NSTemplateRequestManager::valueFor(mSensorId);
        if(obj == NULL) {
            obj = ITemplateRequest::getInstance(mSensorId);
            NSTemplateRequestManager::add(mSensorId, obj);
        }
    }

    NSCamHW::HwInfoHelper helper(mSensorId);
    if (!helper.updateInfos())
    {
        IRIS_LOGE("update sensor static information failed");
        return;
    }

// RRZO
    if (!helper.getRrzoFmt(
                getRAWBitDepth(mSensorStaticInfo->rawSensorBit), mRrzoFormat) ||
            !helper.alignRrzoHwLimitation(
                kResizedRAWSize, mSensorParam.size, mRrzoSize) ||
            !helper.alignPass1HwLimitation(
                mSensorParam.pixelMode, mRrzoFormat, false, mRrzoSize, mRrzoStride))
    {
        IRIS_LOGE("wrong parameter for RRZO");
        return;
    }

    uint32_t rrzo_sizeInBytes = mRrzoStride * mRrzoSize.h;

    IRIS_LOGD("resized pixelSize(%dx%d), imgSizeInBytes(%u) = stride(%zu) * height(%d)",
            mRrzoSize.w, mRrzoSize.h, rrzo_sizeInBytes, mRrzoStride , mRrzoSize.h);

// IMGO
    mImgoSize = mSensorParam.size;
    if (!helper.getImgoFmt(
                getRAWBitDepth(mSensorStaticInfo->rawSensorBit), mImgoFormat) ||
            !helper.alignPass1HwLimitation(
                mSensorParam.pixelMode, mImgoFormat, true, mImgoSize, mImgoStride))
    {
        IRIS_LOGE("wrong parameter for IMGO");
        return;
    }

    uint32_t imgo_sizeInBytes = mImgoStride * mImgoSize.h;

    IRIS_LOGD("full pixelSize(%dx%d), imgSizeInBytes(%u) = stride(%zu) * height(%d)",
            mImgoSize.w, mImgoSize.h, imgo_sizeInBytes, mImgoStride , mImgoSize.h);
    //
}

void RgbPath::setupMetaStreamInfo()
{
    IRIS_TRACE_CALL();
    AutoLog();

    mControlMeta_App =
        new MetaStreamInfo(
                "App:Meta:Control",
                eSTREAMID_META_APP_CONTROL,
                eSTREAMTYPE_META_IN,
                0
                );
    mControlMeta_Hal =
        new MetaStreamInfo(
                "Hal:Meta:Control",
                eSTREAMID_META_HAL_CONTROL,
                eSTREAMTYPE_META_IN,
                0
                );
    mResultMeta_P1_App =
        new MetaStreamInfo(
                "App:Meta:ResultP1",
                eSTREAMID_META_APP_DYNAMIC_P1,
                eSTREAMTYPE_META_OUT,
                0
                );
    mResultMeta_P1_Hal =
        new MetaStreamInfo(
                "Hal:Meta:ResultP1",
                eSTREAMID_META_HAL_DYNAMIC_P1,
                eSTREAMTYPE_META_INOUT,
                0
                );
    mResultMeta_P2_App =
        new MetaStreamInfo(
                "App:Meta:ResultP2",
                eSTREAMID_META_APP_DYNAMIC_P2,
                eSTREAMTYPE_META_OUT,
                0
                );
    mResultMeta_P2_Hal =
        new MetaStreamInfo(
                "Hal:Meta:ResultP2",
                eSTREAMID_META_HAL_DYNAMIC_P2,
                eSTREAMTYPE_META_INOUT,
                0
                );
}

void RgbPath::setupImageStreamInfo()
{
    IRIS_TRACE_CALL();
    AutoLog();

    {// Resized Raw
        MSize const& size = mRrzoSize;
        MINT const format = mRrzoFormat;
        size_t const stride = mRrzoStride;
        MUINT const usage = 0;//eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READWRITE ;
        mImage_RrzoRaw = createRawImageStreamInfo(
                "Hal:Image:Resiedraw",
                eSTREAMID_IMAGE_PIPE_RAW_RESIZER,
                eSTREAMTYPE_IMAGE_INOUT,
                6, 4,
                usage, format, size, stride, isSecurity
                );
    }
    {// Full Raw
        MSize const& size = mImgoSize;
        MINT const format = mImgoFormat;
        size_t const stride = mImgoStride;
        MUINT const usage = 0;//eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READWRITE ;
        mImage_ImgoRaw = createRawImageStreamInfo(
                "Hal:Image:Fullraw",
                eSTREAMID_IMAGE_PIPE_RAW_OPAQUE,
                eSTREAMTYPE_IMAGE_INOUT,
                6, 4,
                usage, format, size, stride, isSecurity
                );
    }
    {// Lcso Raw
        NS3Av3::LCSO_Param lcsoParam;
        MUINT const usage = 0;//eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READWRITE ;
        if (auto pIspMgr = MAKE_IspMgr()) {
            pIspMgr->queryLCSOParams(lcsoParam);
        } else {
            IRIS_LOGE("create IspMgr failed!");
        }
        IRIS_LOGD("lcso num:%d-%d format:%d actual size:%dx%d, stride:%d",
            2, 8, lcsoParam.format,
            lcsoParam.size.w, lcsoParam.size.h,
            lcsoParam.stride);
        mImage_LcsoRaw = createRawImageStreamInfo(
            "Hal:Image:LCSraw",
            eSTREAMID_IMAGE_PIPE_RAW_LCSO,
            eSTREAMTYPE_IMAGE_INOUT,
            8, 2,
            usage, lcsoParam.format, lcsoParam.size, lcsoParam.stride, MFALSE);

    }
    {// Display
        MSize const& size = kResizedRAWSize;
        MINT const format = mYuvFormat;
        MUINT const usage = 0;//eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READWRITE ;//0;
        mImage_Yuv = createImageStreamInfo(
                "Hal:Image:yuv",
                eSTREAMID_IMAGE_PIPE_YUV_00,
                eSTREAMTYPE_IMAGE_INOUT,
                5, 1,
                usage, format, size, 0, MSize(1,1), MFALSE, 0, isSecurity
                );
    }

}

void RgbPath::setupRawBufferPool()
{
    IRIS_TRACE_CALL();
    AutoLog();

    mCallback = new ImageCallback(this, 0);
    mMetaListener = new MetadataListener(this);
    mCallbackHandler = new BufferCallbackHandler(mSensorId);
    mCallbackHandler->setImageCallback(mCallback);

    // rrzo
    sp<StreamBufferProviderFactory> pRrzoFactory =
        StreamBufferProviderFactory::createInstance();
    mResizedRawPool = new CallbackBufferPool(mImage_RrzoRaw, isSecurity, secType);
    mResizedRawPool->allocateBuffer(
            mImage_RrzoRaw->getStreamName(),
            mImage_RrzoRaw->getMaxBufNum(),
            mImage_RrzoRaw->getMinInitBufNum()
            );
    mCallbackHandler->setBufferPool(mResizedRawPool);

    pRrzoFactory->setImageStreamInfo(mImage_RrzoRaw);
    pRrzoFactory->setUsersPool(
            mCallbackHandler->queryBufferPool(mImage_RrzoRaw->getStreamId())
            );
    mRrzoProducer = pRrzoFactory->create(true);

    // imgo
    sp<StreamBufferProviderFactory> pImgoFactory =
        StreamBufferProviderFactory::createInstance();
    mFullRawPool = new CallbackBufferPool(mImage_ImgoRaw, isSecurity, secType);
    mFullRawPool->allocateBuffer(
            mImage_ImgoRaw->getStreamName(),
            mImage_ImgoRaw->getMaxBufNum(),
            mImage_ImgoRaw->getMinInitBufNum()
            );
    mCallbackHandler->setBufferPool(mFullRawPool);

    pImgoFactory->setImageStreamInfo(mImage_ImgoRaw);
    pImgoFactory->setUsersPool(
            mCallbackHandler->queryBufferPool(mImage_ImgoRaw->getStreamId())
            );
    mImgoProducer = pImgoFactory->create(true);

    // lcso
    sp<StreamBufferProviderFactory> pLcsoFactory =
        StreamBufferProviderFactory::createInstance();
    // TODO Hard code create lcso normal buffer
    mLcsoRawPool = new CallbackBufferPool(mImage_LcsoRaw, MFALSE, SecType::mem_normal);
    mLcsoRawPool->allocateBuffer(
            mImage_LcsoRaw->getStreamName(),
            mImage_LcsoRaw->getMaxBufNum(),
            mImage_LcsoRaw->getMinInitBufNum()
            );
    mCallbackHandler->setBufferPool(mLcsoRawPool);

    pLcsoFactory->setImageStreamInfo(mImage_LcsoRaw);
    pLcsoFactory->setUsersPool(
            mCallbackHandler->queryBufferPool(mImage_LcsoRaw->getStreamId())
            );
    mLcsoProducer = pLcsoFactory->create(true);

    // yuv
    sp<StreamBufferProviderFactory> pYuvFactory =
        StreamBufferProviderFactory::createInstance();
    mYuvPool = new CallbackBufferPool(mImage_Yuv, isSecurity, secType);
    mYuvPool->allocateBuffer(
            mImage_Yuv->getStreamName(),
            mImage_Yuv->getMaxBufNum(),
            mImage_Yuv->getMinInitBufNum()
            );
    mCallbackHandler->setBufferPool(mYuvPool);

    pYuvFactory->setImageStreamInfo(mImage_Yuv);
    pYuvFactory->setUsersPool(
            mCallbackHandler->queryBufferPool(mImage_Yuv->getStreamId())
            );
    mYuvProducer = pYuvFactory->create(true);
}

void RgbPath::setupPipelineContext()
{
    IRIS_TRACE_CALL();
    AutoLog();

    mContext = PipelineContext::create(DEBUG_LOG_TAG);
    if( !mContext.get() ) {
        IRIS_LOGE("cannot create context");
        return;
    }
    //
    mContext->beginConfigure();
    //
    // 1. Streams ***************
    //
    // 1.a. check if stream exist
    // 1.b. setup streams
    {
        // Meta
        StreamBuilder(eStreamType_META_APP, mControlMeta_App)
            .build(mContext);
        StreamBuilder(eStreamType_META_HAL, mControlMeta_Hal)
            .build(mContext);
        StreamBuilder(eStreamType_META_APP, mResultMeta_P1_App)
            .build(mContext);
        StreamBuilder(eStreamType_META_HAL, mResultMeta_P1_Hal)
            .build(mContext);
        StreamBuilder(eStreamType_META_APP, mResultMeta_P2_App)
            .build(mContext);
        StreamBuilder(eStreamType_META_HAL, mResultMeta_P2_Hal)
            .build(mContext);

        // Image
        StreamBuilder(eStreamType_IMG_HAL_PROVIDER, mImage_RrzoRaw)
            .setProvider(mRrzoProducer)
            .build(mContext);
        StreamBuilder(eStreamType_IMG_HAL_PROVIDER, mImage_ImgoRaw)
            .setProvider(mImgoProducer)
            .build(mContext);
        StreamBuilder(eStreamType_IMG_HAL_PROVIDER, mImage_LcsoRaw)
            .setProvider(mLcsoProducer)
            .build(mContext);
        StreamBuilder(eStreamType_IMG_HAL_PROVIDER, mImage_Yuv)
            .setProvider(mYuvProducer)
            .build(mContext);
    }
    //
    // 2. Nodes   ***************
    //
    // 2.a. check if node exist
    // 2.b. setup nodes
    //
    {
        typedef P1Node                  NodeT;
        typedef NodeActor< NodeT >      MyNodeActorT;
        //
        IRIS_LOGD("Nodebuilder p1 +");
        NodeT::InitParams initParam;
        {
            initParam.openId   = mSensorId;
            initParam.nodeId   = eNODEID_P1Node;
            initParam.nodeName = "P1Node";
        }
        NodeT::ConfigParams cfgParam;
        {
            cfgParam.pInAppMeta        = mControlMeta_App;
            cfgParam.pInHalMeta        = mControlMeta_Hal;
            cfgParam.pOutAppMeta       = mResultMeta_P1_App;
            cfgParam.pOutHalMeta       = mResultMeta_P1_Hal;
            cfgParam.pOutImage_resizer = mImage_RrzoRaw;
            cfgParam.pvOutImage_full.push_back(mImage_ImgoRaw); //N/A
            cfgParam.pOutImage_lcso    = mImage_LcsoRaw;
            cfgParam.sensorParams = mSensorParam;
            cfgParam.enableLCS         = (MTKCAM_LTM_SUPPORT) != 0 ? MTRUE : MFALSE;
#if 0 // test the NULL pool parameter
            cfgParam.pStreamPool_resizer = NULL;
            cfgParam.pStreamPool_full    = NULL;
#else
            cfgParam.pStreamPool_resizer = mContext->queryImageStreamPool(mImage_RrzoRaw->getStreamId());
            cfgParam.pStreamPool_full    = mContext->queryImageStreamPool(mImage_ImgoRaw->getStreamId());
            cfgParam.pStreamPool_lcso    = mContext->queryImageStreamPool(mImage_LcsoRaw->getStreamId());
#endif

            cfgParam.enableSecurity = isSecurity;
            cfgParam.secType = secType;
            cfgParam.statusSecHandle = (isSecurity) ? generateSecureStatusHandle() : 0;
        }
        //
        sp<MyNodeActorT> pNode = new MyNodeActorT( NodeT::createInstance() );
        pNode->setInitParam(initParam);
        pNode->setConfigParam(cfgParam);
        //
        StreamSet vIn;
        add_stream_to_set(vIn, mControlMeta_App);
        add_stream_to_set(vIn, mControlMeta_Hal);
        //
        StreamSet vOut;
        add_stream_to_set(vOut, mImage_RrzoRaw);
        add_stream_to_set(vOut, mImage_ImgoRaw);
        add_stream_to_set(vOut, mImage_LcsoRaw);
        add_stream_to_set(vOut, mResultMeta_P1_App);
        add_stream_to_set(vOut, mResultMeta_P1_Hal);
        //
        NodeBuilder aNodeBuilder(eNODEID_P1Node, pNode);

        aNodeBuilder.addStream(NodeBuilder::eDirection_IN , vIn);
        aNodeBuilder.addStream(NodeBuilder::eDirection_OUT, vOut);

        if (mImage_RrzoRaw != 0)
            aNodeBuilder.setImageStreamUsage(mImage_RrzoRaw->getStreamId(),
            eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
        if (mImage_ImgoRaw != 0)
            aNodeBuilder.setImageStreamUsage(mImage_ImgoRaw->getStreamId(),
            eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
        if (mImage_LcsoRaw != 0)
            aNodeBuilder.setImageStreamUsage(mImage_LcsoRaw->getStreamId(),
            eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);

        MERROR ret = aNodeBuilder.build(mContext);

        IRIS_LOGD("Nodebuilder p1 -");

        if( ret != OK ) {
            IRIS_LOGE("build p1 node error");
            return;
        }
    }

    {
        typedef P2FeatureNode NodeT;
        typedef NodeActor< NodeT > MyNodeActorT;
        NodeT::UsageHint usageHint;
        //
        IRIS_LOGD("Nodebuilder p2 +");
        NodeT::InitParams initParam;
        {
            initParam.openId   = mSensorId;
            initParam.nodeId   = eNODEID_P2Node;
            initParam.nodeName = "P2FeatureNode";
            usageHint.mSecType = secType;
        }
        NodeT::ConfigParams cfgParam;
        {
            cfgParam.pInAppMeta    = mControlMeta_App;
            cfgParam.pInAppRetMeta = mResultMeta_P1_App;
            cfgParam.pInHalMeta    = mResultMeta_P1_Hal;
            cfgParam.pOutAppMeta   = mResultMeta_P2_App;
            cfgParam.pOutHalMeta   = mResultMeta_P2_Hal;
            if (mImage_ImgoRaw != 0)
                cfgParam.pvInFullRaw.push_back(mImage_ImgoRaw);
            cfgParam.pInResizedRaw = mImage_RrzoRaw;
            cfgParam.pInLcsoRaw = mImage_LcsoRaw;
            if (mImage_Yuv != 0)
                cfgParam.vOutImage.push_back(mImage_Yuv);
            if (!mResultMeta_P1_App.get()) {
                IRIS_LOGE("NO ResultMeta_P1");
            }
        }
        //
        sp<MyNodeActorT> pNode = new MyNodeActorT( NodeT::createInstance(P2FeatureNode::PASS2_STREAM, usageHint) );
        pNode->setInitParam(initParam);
        pNode->setConfigParam(cfgParam);
        //
        NodeBuilder aNodeBuilder(eNODEID_P2Node, pNode);
        //
        StreamSet vIn;
        add_stream_to_set(vIn, mImage_ImgoRaw);
        add_stream_to_set(vIn, mImage_RrzoRaw);
        add_stream_to_set(vIn, mImage_LcsoRaw);
        add_stream_to_set(vIn, mControlMeta_App);
        add_stream_to_set(vIn, mResultMeta_P1_App);
        add_stream_to_set(vIn, mResultMeta_P1_Hal);
        //
        StreamSet vOut;
        add_stream_to_set(vOut, mImage_Yuv);
        add_stream_to_set(vOut, mResultMeta_P2_App);
        add_stream_to_set(vOut, mResultMeta_P2_Hal);

        aNodeBuilder.addStream(NodeBuilder::eDirection_IN , vIn);
        aNodeBuilder.addStream(NodeBuilder::eDirection_OUT, vOut);

        if (mImage_RrzoRaw != 0)
            aNodeBuilder.setImageStreamUsage(mImage_RrzoRaw->getStreamId(),
                    eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
        if (mImage_ImgoRaw != 0)
            aNodeBuilder.setImageStreamUsage(mImage_ImgoRaw->getStreamId(),
                    eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
        if ( mImage_LcsoRaw != 0 )
            aNodeBuilder.setImageStreamUsage(mImage_LcsoRaw->getStreamId(),
                    eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
        if (mImage_Yuv != 0)
            aNodeBuilder.setImageStreamUsage(mImage_Yuv->getStreamId(),
                    eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);

        MERROR ret = aNodeBuilder.build(mContext);

        IRIS_LOGD("Nodebuilder p2 -");

        if( ret != OK ) {
            IRIS_LOGE("build p2 node error");
            return;
        }
    }

    //
    // 3. Pipeline **************
    //
    {
        NodeEdgeSet edges;
        edges.addEdge(eNODEID_P1Node, eNODEID_P2Node);
        MERROR ret = PipelineBuilder()
            .setRootNode(
                    NodeSet().add(eNODEID_P1Node)
                    )
            .setNodeEdges(
                    edges
                    )
            .build(mContext);
        if( ret != OK ) {
            IRIS_LOGE("build pipeline error");
            return;
        }
    }
    //
    mResultProcessor = ResultProcessor::createInstance();
    mContext->setDataCallback(mResultProcessor);
    mContext->endConfigure();
}

void RgbPath::setupRequestBuilder()
{
    IRIS_TRACE_CALL();
    AutoLog();

    {// ONLY P1
        mRequestBuilderP1 = new RequestBuilder();

        mRequestBuilderP1->setIOMap(
                eNODEID_P1Node,
                IOMapSet().add(
                    IOMap()
                    .addOut(mImage_RrzoRaw->getStreamId())
                    .addOut(mImage_ImgoRaw->getStreamId())
                    .addOut(mImage_LcsoRaw->getStreamId())
                    ),
                IOMapSet().add(
                    IOMap()
                    .addIn(mControlMeta_App->getStreamId())
                    .addIn(mControlMeta_Hal->getStreamId())
                    .addOut(mResultMeta_P1_App->getStreamId())
                    .addOut(mResultMeta_P1_Hal->getStreamId())
                    )
                );
        mRequestBuilderP1->setRootNode(
                NodeSet().add(eNODEID_P1Node)
                );
    }

    mRequestBuilderP1->updateFrameCallback(mResultProcessor);

    {
        mRequestBuilderPrv = new RequestBuilder();
        mRequestBuilderPrv->setIOMap(
                                eNODEID_P1Node,
                                IOMapSet().add(
                                    IOMap()
                                    .addOut(mImage_RrzoRaw->getStreamId())
                                    .addOut(mImage_ImgoRaw->getStreamId())
                                    .addOut(mImage_LcsoRaw->getStreamId())
                                    ),
                                IOMapSet().add(
                                    IOMap()
                                    .addIn(mControlMeta_App->getStreamId())
                                    .addIn(mControlMeta_Hal->getStreamId())
                                    .addOut(mResultMeta_P1_App->getStreamId())
                                    .addOut(mResultMeta_P1_Hal->getStreamId())
                                    )
                                );
        IOMapSet imgIOMapSet, metaIOMapSet;
        imgIOMapSet.add(
                    IOMap()
                    .addIn(mImage_ImgoRaw->getStreamId())
                    .addIn(mImage_LcsoRaw->getStreamId())
                    .addOut(mImage_Yuv->getStreamId())
                    );
        metaIOMapSet.add(
                    IOMap()
                    .addIn(mControlMeta_App->getStreamId())
                    .addIn(mResultMeta_P1_App->getStreamId())
                    .addIn(mResultMeta_P1_Hal->getStreamId())
                    .addOut(mResultMeta_P2_App->getStreamId())
                    .addOut(mResultMeta_P2_Hal->getStreamId())
                );

        mRequestBuilderPrv->setIOMap(
                                eNODEID_P2Node,
                                imgIOMapSet,
                                metaIOMapSet
                            );
        mRequestBuilderPrv->setRootNode(
                                NodeSet().add(eNODEID_P1Node)
                            );
        mRequestBuilderPrv->setNodeEdges(
                                NodeEdgeSet().addEdge(eNODEID_P1Node, eNODEID_P2Node)
                            );

    }

    mRequestBuilderPrv->updateFrameCallback(mResultProcessor);

    // register listener
    mTimestampProcessor = TimestampProcessor::createInstance(mSensorId);

    mResultProcessor->registerListener(
            eSTREAMID_META_APP_DYNAMIC_P1,
            mTimestampProcessor);
    mResultProcessor->registerListener(
            eSTREAMID_META_HAL_DYNAMIC_P1,
            mTimestampProcessor);
    mResultProcessor->registerListener(
            eSTREAMID_META_APP_DYNAMIC_P2,
            mTimestampProcessor);
    mResultProcessor->registerListener(
            eSTREAMID_META_HAL_DYNAMIC_P2,
            mTimestampProcessor);

    IRIS_LOGD("Raw/Yuv buffer provider");
    mTimestampProcessor->registerCB(mImgoProducer);
    mTimestampProcessor->registerCB(mRrzoProducer);
    mTimestampProcessor->registerCB(mLcsoProducer);
    mTimestampProcessor->registerCB(mYuvProducer);

    mResultProcessor->registerListener(
            eSTREAMID_META_APP_DYNAMIC_P1,
            mMetaListener);
    mResultProcessor->registerListener(
            eSTREAMID_META_HAL_DYNAMIC_P1,
            mMetaListener);
    mResultProcessor->registerListener(
            eSTREAMID_META_APP_DYNAMIC_P2,
            mMetaListener);
    mResultProcessor->registerListener(
            eSTREAMID_META_HAL_DYNAMIC_P2,
            mMetaListener);
}

void RgbPath::finishPipelineContext()
{
    IRIS_TRACE_CALL();
    AutoLog();
    if (mContext.get())
    {
        IRIS_LOGD("flush...");
        mContext->flush();
        IRIS_LOGD("waitUntilDrained...");
        mContext->waitUntilDrained();
        IRIS_LOGD("clear pipeline context...");
        mContext = nullptr;
    }
}

void RgbPath::onMetaReceived(
        MUINT32         const requestNo,
        StreamId_T      const streamId,
        MBOOL           const errorResult,
        IMetadata       const result
        )
{
    AutoLog();
    IRIS_LOGD("listener onMetaReceived!");

}

void RgbPath::onDataReceived(
        MUINT32 const requestNo,
        StreamId_T const streamId,
        android::sp<IImageBuffer>& pBuffer
        )
{
    AutoLog();
    IRIS_LOGD("listener onDataReceived!");
    IRIS_LOGD("image format : %p", pBuffer->getImgFormat());

    // TODO: translate ImageBuffer to callback buffer
    {
        constexpr iris_callback_descriptor_t des = IRIS_CALLBACK_ADDR;

        std::unique_lock<std::mutex> _l(mRegisteredCallbacksLock);
        auto search = mRegisteredCallbacks.find(des);
        if (search != mRegisteredCallbacks.end())
        {
            // TODO: check if these information is correct
            // NOTE: Bayer RAW is single-plane
            Buffer result
            {
                .addr.secureHandle.fd =
                    [&pBuffer, this]
                    {
                        if (!(pBuffer->lockBuf(DEBUG_LOG_TAG, kBufferUsage)))
                        {
                            IRIS_LOGE("Image Buffer lock failed");
                            return -1;
                        }
#ifdef DEBUG_DUMP
                        if (this->mEnableDump) {
                            char path[256] = {0};
                            static int frameNo = 0;
                            const char* extension =
                                (pBuffer->getImgFormat() != eImgFmt_NV12) ? "raw" : "nv12";
                            snprintf(path, sizeof(path),
                                    DUMP_PATH"%d_%d_%d_%d.%s",
                                    frameNo++,
                                    pBuffer->getImgSize().w, pBuffer->getImgSize().h,
                                    pBuffer->getBufStridesInBytes(0), extension);
                            pBuffer->saveToFile(path);
                        }
#endif
                        auto fd = pBuffer->getFD();
                        pBuffer->unlockBuf(DEBUG_LOG_TAG);

                        return fd;
                    }(),
                .attribute.identifier = std::make_pair(streamId, requestNo),
                .attribute.length = [&pBuffer]()
                {
                    size_t bufferSizeInBytes = 0;
                    const auto planeCount = pBuffer->getPlaneCount();
                    for (size_t i = 0; i < planeCount; i++)
                        bufferSizeInBytes += pBuffer->getBufSizeInBytes(i);
                    return bufferSizeInBytes;
                }(),
                .attribute.size = pBuffer->getImgSize(),
                .attribute.stride = pBuffer->getBufStridesInBytes(0),
                .attribute.format = pBuffer->getImgFormat(),
                .path = NSCam::security::Path::BAYER,
                .priv = search->second.second
            };

            IRIS_LOGD("result FD(%d)", result.addr.secureHandle.fd);

            loadFunctionPointer<IRIS_PFN_CALLBACK_ADDR>(search->second.first)(result);
        }
    }
}
