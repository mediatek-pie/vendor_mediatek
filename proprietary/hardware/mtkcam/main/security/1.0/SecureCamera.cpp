/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define DEBUG_LOG_TAG "SecureCamera"

#include "SecureCamera.h"
#include "IrisCallback.h"

// TODO: add rules for normal or secure path compilation
#include "NirPath.h"
#include "RgbPath.h"

#include <linux/ioctl.h>
#include <fcntl.h>

#include <mtkcam/main/security/utils/BufferQueue.h>
#include <mtkcam/main/security/utils/Debug.h>
#include <mtkcam/utils/std/TypeTraits.h>

#ifdef LEGACY_PATH // legacy path
#define RESIZED_RAW_OUTPUT
#endif

using namespace NSCam::security;

// ---------------------------------------------------------------------------

// the amount of ISP working buffers
static constexpr uint32_t kISPWorkingBufferCount = 3;

// ---------------------------------------------------------------------------

//Removed to secure
ISecureCamera* createSecureCamera()
{
    return new SecureCamera();
}

// ---------------------------------------------------------------------------

Path IPath::sPath(Path::INVALID);

template <typename IMPL>
shared_ptr<IMPL> IPath::createInstance(int maxBuffers, Path path, IPath** interface)
{
    IRIS_LOGD("create instance: maxBuffers(%d), path(%x)", maxBuffers, path);

    sPath = path;

    std::shared_ptr<IMPL> __impl(new IMPL(maxBuffers));
    *interface = __impl.get();

    return __impl;
}

Path IPath::getPath()
{
    return sPath;
}

// ---------------------------------------------------------------------------

SecureCamera::SecureCamera()
{
    AutoLog();
}

SecureCamera::~SecureCamera()
{
    AutoLog();
}

Result SecureCamera::open(Path path)
{
    AutoLog();
    mSecureCameraProxy.reset(new SecureCameraProxy(path));

    return OK;
}

Result SecureCamera::close()
{
    AutoLog();
    mSecureCameraProxy = nullptr;

    return OK;
}

Result SecureCamera::init()
{
    AutoLog();

    if (mSecureCameraProxy->init() != OK)
    {
        IRIS_LOGE("camera init failed");
        unInit();
        return NO_INIT;
    }

    return OK;
}

Result SecureCamera::unInit()
{
    AutoLog();
    if (mSecureCameraProxy != nullptr)
    {
      return mSecureCameraProxy->unInit();
    }
    return OK;
}

Result SecureCamera::sendCommand(Command cmd, intptr_t arg1, intptr_t arg2)
{
    AutoLog();
    IRIS_LOGD("sendCommand cmd(%d)", cmd);

    switch (cmd)
    {
        case Command::STREAMING_ON:
        {
            IRIS_LOGD("STREAMING_ON");
            return mSecureCameraProxy->streamingOn();
        }
        case Command::STREAMING_OFF:
        {
            IRIS_LOGD("STREAMING_OFF");
            return mSecureCameraProxy->streamingOff();
        }
        case Command::REGISTER_CB_FUNC:
        {
            IRIS_LOGD("REGISTER_CB_FUNC");
            return mSecureCameraProxy->registerCallback(reinterpret_cast<Callback*>(arg1), reinterpret_cast<void*>(arg2));
        }
        case Command::RETURN_CB_DATA:
        {
            IRIS_LOGD("RETURN_CB_DATA");
            return mSecureCameraProxy->returnCallbackData(reinterpret_cast<Buffer*>(arg2));
        }
        case Command::SET_SRC_DEV:
        {
            IRIS_LOGD("SET_SRC_DEV");
            return mSecureCameraProxy->setSrcDev(arg1);
        }
        case Command::SET_SHUTTER_TIME:
        {
            IRIS_LOGD("SET_SHUTTER_TIME");
            return mSecureCameraProxy->setShutterTime(arg1);
        }
        case Command::SET_GAIN_VALUE:
        {
            IRIS_LOGD("SET_GAIN_VALUE");
            return mSecureCameraProxy->setGainValue(arg1);
        }
        case Command::SET_SENSOR_CONFIG:
        {
            IRIS_LOGD("SET_SENSOR_CONFIG");
            return mSecureCameraProxy->setSensorConfig(
                    reinterpret_cast<const Configuration*>(arg1));
        }
        case Command::GET_SENSOR_CONFIG:
        {
            IRIS_LOGD("GET_SENSOR_CONFIG");
            const Configuration config = mSecureCameraProxy->getSensorConfig();
            Configuration& _config(*reinterpret_cast<Configuration*>(arg1));
            _config = config;
            break;
        }
        default:
        {
            IRIS_LOGE("unknown command(%u)", NSCam::toLiteral<Command>(cmd));
            return INVALID_OPERATION;
        }
    }
    return OK;
}

// ---------------------------------------------------------------------------

SecureCameraProxy::SecureCameraProxy(Path path)
    : mPath(nullptr)
{
    AutoLog();

    mCallback.reset(new IrisCallback());
    if (!mCallback)
        IRIS_LOGE("create callback failed");

    if (path == Path::IR)
    {
        mNirPath = IPath::createInstance<NirPath>(kISPWorkingBufferCount, path, &mPath);
    }
    else if (path == Path::BAYER)
    {
       mRGBPath = IPath::createInstance<RgbPath>(kISPWorkingBufferCount, path, &mPath);
    }

    if (!mPath)
        IRIS_LOGE("create IPath failed");

    mFullSizedStream.reset(new IrisBufferQueue(kISPWorkingBufferCount));

#ifdef RESIZED_RAW_OUTPUT
    mResizedStream.reset(new IrisBufferQueue(kISPWorkingBufferCount));
#endif
}

SecureCameraProxy::~SecureCameraProxy()
{
    AutoLog();
    // TODO: reset IPath::sPath here
}

Result SecureCameraProxy::init()
{
    AutoLog();

    std::unordered_map<StreamID, IrisStream> streamMap;

    // NOTE: Full-sized stream must exist
    {
        std::lock_guard<std::mutex> _l(mFullSizedStreamLock);
        if (!mFullSizedStream)
        {
            IRIS_LOGE("Invalid buffer queue");
            return NO_INIT;
        }
        streamMap.emplace(StreamID::FULL_RAW, mFullSizedStream);
    }

    {
        std::lock_guard<std::mutex> _l(mResizedStreamLock);
        if (mResizedStream)
            streamMap.emplace(StreamID::RESIZED_RAW, mResizedStream);
    }

    {
        std::lock_guard<std::mutex> _l(mCallbackLock);
        if (!mCallback)
        {
            IRIS_LOGE("invalid callback thread");
            return NO_INIT;
        }
        mCallback->init(streamMap);
    }

    {
        std::lock_guard<std::mutex> _l(mPathLock);
        if (!mPath)
        {
            IRIS_LOGE("Invalid path");
            return NO_INIT;
        }
        mPath->init(streamMap);
    }

    return OK;
}

Result SecureCameraProxy::unInit()
{
    AutoLog();

    Result err = OK;

    {
        std::lock_guard<std::mutex> _l(mPathLock);
        if (mPath)
        {
            // turn sensor off
            err = mPath->sensorPower(false);
            if (err != OK)
            {
                IRIS_LOGE("power sensor off failed");
                return err;
            }

            err = mPath->unInit();
            if (err != OK)
            {
                IRIS_LOGE("uninit failed");
                return err;
            }

            mPath->destroyInstance();
        }
    }

    {
        std::lock_guard<std::mutex> _l(mCallbackLock);
        if (!mCallback)
        {
            IRIS_LOGE("invalid callback thread");
            return NO_INIT;
        }
        mCallback->unInit();
    }

    {
        std::lock_guard<std::mutex> _l(mFullSizedStreamLock);
        // NOTE: If use_count returns 1, there are no other owners.
        if (mFullSizedStream && (mFullSizedStream.use_count() > 1))
            IRIS_LOGW("IrisBufferQueueFullSized is managed and not release elsewhere");

        mFullSizedStream = nullptr;
    }

    return err;
}

Result SecureCameraProxy::streamingOn()
{
    AutoLog();

    Result err = OK;

    // configure buffer queue parameters
    if (IPath::getPath() == Path::IR)
    {
        // calculate pass1 frame buffer size and stride
        SensorStaticInfo sensorSetting;
        std::lock_guard<std::mutex> _l(mPathLock);
        err = mPath->getSensorSetting(sensorSetting);
        if (err != OK)
            return UNKNOWN_ERROR;

        std::vector<StreamID> identifiers;
        // query stream information
        auto streamInfo = [&]
        {
            if (mFullSizedStream)
                identifiers.push_back(StreamID::FULL_RAW);

            if (mResizedStream)
                identifiers.push_back(StreamID::RESIZED_RAW);

            return mPath->getStreamInfo(sensorSetting, identifiers);
        }();

        // NOTE: the order of identifiers and streamInfo are the same with each other
        for (size_t i = 0; i < identifiers.size(); i++)
        {
            IrisBufferQueue::BufferParam param(
                    streamInfo[i].size.w, streamInfo[i].size.h,
                    streamInfo[i].stride, streamInfo[i].format,
                    streamInfo[i].sizeInBytes,
                    IrisBufferQueue::CacheType::CACHEABLE);

            auto stream = [this](const StreamID& streamID) -> IrisStream
            {
                switch (streamID)
                {
                    case StreamID::FULL_RAW:
                        return mFullSizedStream;
                    case StreamID::RESIZED_RAW:
                        return mResizedStream;
                    default:
                        IRIS_LOGE("unknown stream(%d)", toLiteral(streamID));
                        return nullptr;
                }
            }(identifiers[i]);

            if (stream)
            {
                stream->setBufferParam(std::move(param));
                stream->setConsumerListener(mCallback);
            }
        }
    }

    {
        std::lock_guard<std::mutex> _l(mPathLock);
        if (mPath->StreamingOn() != OK)
        {
            IRIS_LOGE("preview start fail(err=0x%x)",err);
            return UNKNOWN_ERROR;
        }
    }

    return OK;
}

Result SecureCameraProxy::streamingOff()
{
    AutoLog();

    Result err = OK;

    {
        std::lock_guard<std::mutex> _l(mPathLock);
        if (!mPath)
        {
            IRIS_LOGE("Invalid path");
            return NO_INIT;
        }
        err = mPath->StreamingOff();
        IRIS_LOGE_IF(err != OK, "streaming off failed");
    }

    if (IPath::getPath() == Path::IR)
    {
        std::lock_guard<std::mutex> _l(mCallbackLock);
        if (!mCallback)
        {
            IRIS_LOGE("invalid callback thread");
            return NO_INIT;
        }
        mCallback->unInit();
    }

    return err;
}

Result SecureCameraProxy::returnCallbackData(const Buffer* data)
{
    AutoLog();

    if (CC_UNLIKELY(!data))
    {
        IRIS_LOGE("invalid callback data");
        return BAD_VALUE;
    }

    // pass Iris Hal RETURN_CB_DATA message to IrisCallback
    std::lock_guard<std::mutex> _l(mCallbackLock);
    mCallback->releaseBuffer(data->attribute.identifier);

    return OK;
}

Result SecureCameraProxy::registerCallback(Callback* cb, void* priv)
{
    AutoLog();

    std::lock_guard<std::mutex> _l(mCallbackLock);
    if (IPath::getPath() == Path::IR)
    {
        if (!mCallback)
        {
            IRIS_LOGE("invalid callback thread");
            return NO_INIT;
        }

        mCallback->registerCallback(cb);
    }
    else if (IPath::getPath() == Path::BAYER)
    {
        if (!mRGBPath)
        {
            IRIS_LOGE("invalid RGB path");
            return NO_INIT;
        }
        // callback to RGB path is realized by the derived class
        mRGBPath->registerCallback(cb, priv);
    }

    return OK;
}

Result SecureCameraProxy::setSensorConfig(const Configuration *config)
{
    AutoLog();

    if (!config)
    {
        IRIS_LOGE("ivalid configuration");
        return BAD_VALUE;
    }

    {
        std::lock_guard<std::mutex> _l(mConfigurationLock);
        mConfig = *config;
    }

    return OK;
}

Configuration SecureCameraProxy::getSensorConfig() const
{
    std::lock_guard<std::mutex> _l(mConfigurationLock);
    return mConfig;
}

Result SecureCameraProxy::setSrcDev(unsigned int openID)
{
    AutoLog();

    std::lock_guard<std::mutex> _l(mPathLock);
    if (!mPath)
    {
        IRIS_LOGE("Invalid path");
        return NO_INIT;
    }

    Result err = OK;

    err = mPath->setSrcDev(openID);
    if (err != OK)
    {
        IRIS_LOGE("set source camera device failed");
        return err;
    }

    // turn sensor on
    err = mPath->sensorPower(true);
    if (err != OK)
    {
        IRIS_LOGE("power sensor on failed");
        return err;
    }

    return err;
}

Result SecureCameraProxy::setShutterTime(uint32_t time)
{
    AutoLog();

    std::lock_guard<std::mutex> _l(mPathLock);
    if (!mPath)
    {
        IRIS_LOGE("Invalid path");
        return NO_INIT;
    }

    return mPath->setShutterTime(time);
}

Result SecureCameraProxy::setGainValue(uint32_t gain)
{
    AutoLog();

    std::lock_guard<std::mutex> _l(mPathLock);
    if (!mPath)
    {
        IRIS_LOGE("Invalid path");
        return NO_INIT;
    }

    return mPath->setGainValue(gain);
}
