/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_HARDWARE_MTKCAM_MAIN_SECURITY_1_0_IPATH_H_
#define _MTK_HARDWARE_MTKCAM_MAIN_SECURITY_1_0_IPATH_H_

#include <stdint.h>

#include <mtkcam/def/UITypes.h>
#include <mtkcam/def/ImageFormat.h>
#include <mtkcam/main/security/utils/BufferQueue.h>

#include <mtkcam/main/security/Types.h>
#include "IrisTypes.h"

#include <vector>
#include <unordered_map>

#include <memory>

namespace NSCam {
namespace security {

// ---------------------------------------------------------------------------

using Result = android::status_t;

struct IrisCallBackInfo
{
    // NOTE: the same stream id may bind to different identifiers
    StreamID streamID;
    int identifier;
    std::pair<void*, size_t> result;
    int orientation;

    IrisCallBackInfo(
            StreamID _streamID = StreamID::UNKNOWN,
            int _identifier = 0,
            std::pair<void*, size_t>&& _result =
                std::pair<void*, size_t>(),
            int _orientation = 0,
            int _format = 0,
            NSCam::MSize _size = NSCam::MSize(),
            size_t _stride = 0)
        : streamID(_streamID),
          identifier(_identifier),
          result(_result),
          orientation(_orientation)
    {}
};

enum class State
{
    NONE         = 0x0000,
    INIT         = 0x0001,
    IDLE         = 0x0002,
    PREVIEW_MASK = 0x0100,
    PREVIEW      = 0x0100,
    PREVIEW_STOP = 0x0400,
    UNINIT       = 0x0800,
    EXIT         = 0x1000,
    ERROR        = 0x8000,
};

struct SensorStaticInfo
{
    // image dimension
    NSCam::MSize size;
    // frame rate per second
    int32_t fps;
    // pixelMode
    uint32_t pixelMode;
};

struct StreamInfo
{
    // id
    StreamID id;
    // stream format
    enum NSCam::EImageFormat format;
    // stream dimension
    NSCam::MSize size;
    // stream stride
    size_t stride;
    // stream size in bytes
    uint32_t sizeInBytes;

    StreamInfo(StreamID _id = StreamID::UNKNOWN)
        : id(_id),
          format(NSCam::eImgFmt_UNKNOWN),
          size(),
          stride(0),
          sizeInBytes(0)
    {}
};

// ---------------------------------------------------------------------------

class IPath
    : public IrisBufferQueue::ProducerListener
{
public:
    virtual ~IPath() = default;

    template <typename IMPL>
    // return an IPath instance; the caller must delete the allocated instance by themselves
    static std::shared_ptr<IMPL> createInstance(int maxBuffers, Path path, IPath** interface);

    // path is valid after a successful createInstance()
    static Path getPath();

    virtual void destroyInstance() = 0;

    virtual Result init(
            const std::unordered_map<StreamID, IrisStream>& streamMap) = 0;

    virtual Result unInit() = 0;

    virtual Result setSrcDev(unsigned int devID) = 0;

    virtual Result setShutterTime(uint32_t time) = 0;

    virtual Result setGainValue(uint32_t gain) = 0;

    virtual Result sensorPower(bool turnON) = 0;

    virtual Result StreamingOn() = 0;

    virtual Result StreamingOff() = 0;

    virtual Result getSensorSetting(NSCam::security::SensorStaticInfo& sensorSetting) = 0;

    virtual std::vector<StreamInfo> getStreamInfo(
            const NSCam::security::SensorStaticInfo& sensorSetting,
            const std::vector<StreamID>& identifiers) = 0;

private:
    static Path sPath;
};

} // namespace security
} // namespace NSCam

#endif // _MTK_HARDWARE_MTKCAM_MAIN_SECURITY_1_0_IPATH_H_
