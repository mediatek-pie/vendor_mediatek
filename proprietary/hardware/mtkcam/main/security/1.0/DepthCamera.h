/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_HARDWARE_MTKCAM_MAIN_SECURITY_1_0_SECURE_CAMERA_H_
#define _MTK_HARDWARE_MTKCAM_MAIN_SECURITY_1_0_SECURE_CAMERA_H_

#include <mtkcam/main/security/ISecureCamera.h>
#include "IrisTypes.h"

#include <memory>

#include <utility>
#include <mutex>


#ifdef MTK_SECURE_RGBCAM_PATH

#include <mtkcam/main/security/utils/Debug.h>

#include <chrono>
#include <regex>
#include <unordered_map>
#include <condition_variable>
#include <string>

#include <inttypes.h>

#include <utils/RefBase.h>

#include <android/hardware/camera/common/1.0/types.h>
#include <android/hardware/camera/provider/2.4/ICameraProvider.h>

#include <gui/BufferItemConsumer.h>
#include <gui/BufferQueue.h>
#include <gui/Surface.h>
#include <hardware/gralloc.h>
#include <hardware/gralloc1.h>
#include <system/camera.h>
#include <system/camera_metadata.h>
#include <ui/GraphicBuffer.h>

#include <Camera1Client.h>

using android::sp;
using android::wp;
using android::IGraphicBufferProducer;
using android::IGraphicBufferConsumer;
using android::BufferItemConsumer;

using ::android::Camera1Client;

#endif

namespace NSCam {
namespace security {

// ------------------------------------------------------------------------

class DepthCameraProxy;
class IrPath;
class IrisCallback;
class DepthCallback;
class DepthConsumerListener;
class IPath;

// ------------------------------------------------------------------------

class DepthCamera final : public ISecureCamera
{
public:
    DepthCamera();
    ~DepthCamera();

    Result open(Path path) override;
    Result close() override;
    Result init() override;
    Result unInit() override;
    Result sendCommand(
            Command cmd, intptr_t arg1 = 0, intptr_t arg2 = 0) override;

private:
    std::unique_ptr<DepthCameraProxy> mSecureCameraProxy;
}; // class SecureCamera

// ------------------------------------------------------------------------

class DepthCameraProxy final
{
public:
    DepthCameraProxy(Path path);

    ~DepthCameraProxy();

    Result init();

    Result unInit();

    Result streamingOn();

    Result streamingOff();

    Result returnCallbackData(const Buffer* data);

    Result registerCallback(Callback* cb, void* priv);

    Result setSensorConfig(const Configuration *config);

    Configuration getSensorConfig() const;

    Result setSrcDev(unsigned int openID);

    Result setShutterTime(uint32_t time);

    int setGainValue(uint32_t value);

private:
    mutable std::mutex mConfigurationLock;
    Configuration mConfig;

    mutable std::mutex mPathLock;

    //MainIR Camera
    IPath* mMainPath;
    std::shared_ptr<IrPath> mMainRgbPath;

    //SubIR Camera
    IPath* mSubPath;
    std::shared_ptr<IrPath> mSubRgbPath;

    // NOTE: for simplicity and efficiency, IrisStream is designed to be
    // thread-safe and can be regarded as valid after init() and before unInit()
    // so as to no need to adopt lock
    mutable std::mutex mFullSizedStreamLock;
    IrisStream mFullSizedStream;
    IrisStream mSubFullSizedStream;

    mutable std::mutex mResizedStreamLock;
    IrisStream mResizedStream;

    mutable std::mutex mCallbackLock;
    std::shared_ptr<IrisCallback> mCallback;
    //
    mutable std::mutex mDethCallbackLock;
    std::shared_ptr<DepthCallback> mDethCallback;
    std::shared_ptr<DepthConsumerListener> mMainConsumer;
    std::shared_ptr<DepthConsumerListener> mSubConsumer;
}; // class SecureCameraProxy

} // namespace security
} // namespace NSCam

#endif // _MTK_HARDWARE_MTKCAM_MAIN_SECURITY_1_0_SECURE_CAMERA_H_
