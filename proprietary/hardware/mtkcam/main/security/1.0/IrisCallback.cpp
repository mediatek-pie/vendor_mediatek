#define DEBUG_LOG_TAG "IrisCallback"

#include "IrisCallback.h"
#include "IPath.h"

#include <sys/prctl.h>

#include <chrono>

#include <mtkcam/main/security/utils/Debug.h>
#include <mtkcam/utils/std/TypeTraits.h>

using namespace NSCam;
using namespace NSCam::security;
using namespace NSCam::Utils::Sync;

// ---------------------------------------------------------------------------

static constexpr std::chrono::duration<int64_t> kWaitResultTimeout =
    std::chrono::seconds(1);

// ---------------------------------------------------------------------------

template<typename PFN>
static inline PFN loadFunctionPointer(iris_callback_function_pointer_t func)
{
    return reinterpret_cast<PFN>(func);
}

// ---------------------------------------------------------------------------

static void NSCam::security::handler(IrisCallback& callback)
{
    AutoLog();

    // set thread name
    prctl(PR_SET_NAME, "Iris@CbThread", 0, 0, 0);

    while (true)
    {
        if (!callback.threadLoop())
        {
            IRIS_LOGD("exit thread %s", __FUNCTION__);
            break;
        }
    };
}

// ---------------------------------------------------------------------------

IrisCallback::IrisCallback()
    : mResultQueue(),
      mTimeline(ITimeline::create(DEBUG_LOG_TAG)),
      mTimelineCounter(0)
{
    IRIS_LOGD_IF(mTimeline == nullptr,
            "no sync mechanism, switch to synchronous mode");
}

void IrisCallback::init(const std::unordered_map<StreamID, IrisStream>& streamMap)
{
    AutoLog();

    // copy data
    mStreamMap = streamMap;
    for (const auto& stream : mStreamMap)
        IRIS_LOGD("add stream index(%d)", toLiteral(stream.first));

    {
        std::unique_lock<std::mutex> _l(mResultQueueLock);
        mRequestExit = false;
        mCallbackWorker = std::thread(handler, std::ref(*this));
    }

    IRIS_LOGD("callback thread created");
}

void IrisCallback::unInit()
{
    AutoLog();

    requestExit();
    if (mCallbackWorker.joinable())
    {
        IRIS_LOGD("join callback worker +");
        mCallbackWorker.join();
        IRIS_LOGD("join callback worker -");
    }

    {
        std::unique_lock<std::mutex> _l(mRegisteredCallbacksLock);
        mRegisteredCallbacks.clear();
    }

    {
        std::unique_lock<std::mutex> _l(mStreamMapLock);
        mStreamMap.clear();
    }
}

void IrisCallback::registerCallback(Callback* cb)
{
    AutoLog();

    if (cb == nullptr)
    {
        IRIS_LOGE("invalid callback");
        return;
    }

    const auto key(cb->getDescriptor());
    std::unique_lock<std::mutex> _l(mRegisteredCallbacksLock);
    auto search = mRegisteredCallbacks.find(key);
    if (CC_UNLIKELY(search != mRegisteredCallbacks.end()))
    {
        IRIS_LOGW("callback key(0x%x) already exist, overwrite it", key);
        mRegisteredCallbacks.erase(key);
    }

    if (mRegisteredCallbacks.emplace(key, cb->getHook()).second == false)
        IRIS_LOGE("cannot emplace callback: key(0x%x)", key);
}

void IrisCallback::onBufferQueued(const void* opaqueMessage)
{
    AutoLog();

    // NOTE: opaque message is used to transfer callback information
    if (opaqueMessage == nullptr)
        IRIS_LOGA("invalid opaque message");

    if (cameraCallback(opaqueMessage))
        mResultQueueCondition.notify_one();
}

bool IrisCallback::cameraCallback(const void* param)
{
    AutoLog();

    bool needNotify = false;

    if (param == nullptr)
    {
        IRIS_LOGE("invalid callback result");
        return needNotify;
    }

    // cast parameter nack to its underlying data structure
    const IrisCallBackInfo& callbackInfo(
            *reinterpret_cast<const IrisCallBackInfo*>(param));

    // NOTE: only send notification for the full-sized stream buffer
    //       and release other stream buffers directly

    auto identifier(std::make_pair(toLiteral(callbackInfo.streamID),
                                                 callbackInfo.identifier));

    const auto streamID(callbackInfo.streamID);
    switch (streamID)
    {
        case StreamID::FULL_RAW:
            {
                Buffer result
                {
                    .addr.virtualAddress = callbackInfo.result.first,
                    .attribute.identifier = identifier,
                    .attribute.length = callbackInfo.result.second,
                    .attribute.orientation = callbackInfo.orientation,
                    .path = Path::IR,
                    .priv = nullptr
                };
                std::lock_guard<std::mutex> _l(mResultQueueLock);
                mResultQueue.push_back(std::move(result));
                needNotify = true;
            }
            break;
        case StreamID::RESIZED_RAW:
            {
                IrisBufferQueue::IrisBuffer buffer;
                mStreamMap[streamID]->acquireBuffer(&buffer);
                IRIS_LOGD("acquire stream(%d) buffer FD(%d)",
                        toLiteral(streamID), buffer.ion_fd);
                mStreamMap[streamID]->releaseBuffer(buffer.index);
                IRIS_LOGD("release buffer FD(%d)", buffer.ion_fd);

                needNotify = false;
            }
            break;
        default:
            IRIS_LOGD("no notification for stream(%d)",
                    toLiteral(callbackInfo.streamID));
    }

    return needNotify;
}

void IrisCallback::releaseBuffer(const std::pair<int, int>& index)
{
    AutoLog();

    IrisBufferQueue::IrisBuffer buffer =
        [this, &index]() -> IrisBufferQueue::IrisBuffer
        {
            std::lock_guard<std::mutex> _l(mMapLock);
            auto search = mMap.find(index.second);
            if (search == mMap.end())
            {
                IRIS_LOGE("index(%d) is not found!", index.second);
                return IrisBufferQueue::IrisBuffer();
            }
            IrisBufferQueue::IrisBuffer _buffer = search->second;
            // FIXME: check if we can erase the found item without copy operation
            mMap.erase(search);
            return _buffer;
        }();

    {
        std::lock_guard<std::mutex> _l(mTimelineLock);
        if (mTimeline.get())
        {
            buffer.release_fence =
                mTimeline->createFence(DEBUG_LOG_TAG, ++mTimelineCounter);
            IRIS_LOGD("release buffer FD(%d)", buffer.ion_fd);
            mTimeline->inc(1);
        }
    }

    mStreamMap[static_cast<StreamID>(index.first)]->releaseBuffer(
            buffer.index, buffer.release_fence);

}

void IrisCallback::requestExit()
{
    AutoLog();

    IRIS_LOGD("request callback thread to exit");

    std::lock_guard<std::mutex> _l(mResultQueueLock);
    mRequestExit = true;
    mResultQueueCondition.notify_one();
}

bool IrisCallback::threadLoop()
{
    AutoLog();

    std::unique_lock<std::mutex> _l(mResultQueueLock);

    if (CC_UNLIKELY(mRequestExit))
        return false;

    Buffer result;

    bool needCallback = false;

    // exit loop if receiving available result or exit request
    while (!mRequestExit && mResultQueue.empty())
        mResultQueueCondition.wait(_l);

    if (!mRequestExit)
    {
        result = *mResultQueue.begin();
        needCallback = true;
        if (mResultQueue.begin() != mResultQueue.end())
            mResultQueue.erase(mResultQueue.begin());

        IRIS_LOGD("queue size(%zu)", mResultQueue.size());

        // FIXME: do not establish unused stream (e.g. ResizedRAW stream)
        for (auto&& stream : mStreamMap)
        {
            // TODO: move this hardcoding part after the aforementioned FIXME done
            if (stream.first == StreamID::RESIZED_RAW)
                continue;

            const auto streamID(toLiteral(stream.first));

            IrisBufferQueue::IrisBuffer buffer;
            stream.second->acquireBuffer(&buffer);
            // NOTE: Buffer producer: ISP DMA; Buffer consumer: CPU
            stream.second->cacheSync(
                    buffer, IrisBufferQueue::SyncType::INVALIDATE);
            IRIS_LOGD("acquire stream(%d) buffer FD(%d)",
                    streamID, buffer.ion_fd);

            // establish buffer mapping rules
            {
                const auto key = buffer.ion_fd;

                std::lock_guard<std::mutex> _l(mMapLock);
                auto search = mMap.find(key);
                if (search != mMap.end())
                {
                    IRIS_LOGD("stream(%d) buffer key(0x%x) already exist, overwrite it",
                            streamID, key);
                    mMap.erase(search);
                }
                if (mMap.emplace(key, std::move(buffer)).second == false)
                    IRIS_LOGA("cannot emplace bufer: key(0x%x)", key);
            }
        }
    }

    // TODO: should decide the callback descriptor at runtime
    constexpr iris_callback_descriptor_t des = IRIS_CALLBACK_ADDR;
    if (needCallback)
        notify(des, result);

    return true;
}

void IrisCallback::notify(
        const iris_callback_descriptor_t des, const Buffer& result)
{
    AutoLog();

    std::unique_lock<std::mutex> _l(mRegisteredCallbacksLock);
    IRIS_LOGD("result addr(0x%" PRIXPTR ")",
            reinterpret_cast<uintptr_t>(result.addr.virtualAddress));
    auto search = mRegisteredCallbacks.find(des);
    if (search != mRegisteredCallbacks.end())
        loadFunctionPointer<IRIS_PFN_CALLBACK_ADDR>(search->second)(result);
}
