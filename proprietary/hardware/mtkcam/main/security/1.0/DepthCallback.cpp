#define DEBUG_LOG_TAG "DepthCallback"

#include "DepthCallback.h"
#include "IPath.h"

#include <sys/prctl.h>

#include <chrono>

#include <mtkcam/main/security/utils/Debug.h>
#include <mtkcam/utils/std/TypeTraits.h>
//
#include <cmath>
//
#include <mtkcam/feature/stereo/hal/stereo_size_provider.h>
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
//
#include <mtkcam/feature/stereo/hal/stereo_setting_provider.h>

using namespace NSCam;
using namespace NSCam::security;
using namespace NSCam::Utils::Sync;
using namespace NSCam::v3::Utils;
using namespace NSCam::v1::Stereo;

#define DUMP_PATH "/sdcard/raw/"

std::mutex              DepthCallback::mDepthComepeteLock;
std::condition_variable DepthCallback::mDepthComepeteCondition;

// ---------------------------------------------------------------------------

static constexpr std::chrono::duration<int64_t> kWaitResultTimeout =
    std::chrono::seconds(1);

// ---------------------------------------------------------------------------

template<typename PFN>
static inline PFN loadFunctionPointer(iris_callback_function_pointer_t func)
{
    return reinterpret_cast<PFN>(func);
}

// ---------------------------------------------------------------------------
DepthConsumerListener::DepthConsumerListener()
{
}

void DepthConsumerListener::init(unsigned int openId
                                , const std::unordered_map<StreamID, IrisStream>& streamMap)
{
    mOpenId = openId;

    {
        std::unique_lock<std::mutex> _l(mStreamMapLock);
        mStreamMap = streamMap;
    }
}

void DepthConsumerListener::releaseBuffer(const std::pair<int, int>& index)
{
    AutoLog();

    IrisBufferQueue::IrisBuffer buffer =
        [this, &index]() -> IrisBufferQueue::IrisBuffer
        {
            std::lock_guard<std::mutex> _l(mMapLock);
            auto search = mMap.find(index.second);
            if (search == mMap.end())
            {
                IRIS_LOGE("index(%d) is not found!", index.second);
                return IrisBufferQueue::IrisBuffer();
            }
            IrisBufferQueue::IrisBuffer _buffer = search->second;
            // FIXME: check if we can erase the found item without copy operation
            mMap.erase(search);
            return _buffer;
        }();


    buffer.release_fence = -1;

    mStreamMap[static_cast<StreamID>(index.first)]->releaseBuffer(
            buffer.index, buffer.release_fence);

}

void DepthConsumerListener::onBufferQueued(const void* opaqueMessage)
{
    // Acquire Iris::Buffer which be used only in Hal
    // Record it in the map, if we release buffer we will reference to the map
    // to release buffer
    auto stream = mStreamMap.find(StreamID::RESIZED_RAW);
    sp<IImageBuffer> result = nullptr;

    if(CC_LIKELY(stream != mStreamMap.end()))
    {

        const auto streamID(toLiteral(stream->first));

        IrisBufferQueue::IrisBuffer buffer;
        stream->second->acquireBuffer(&buffer);
        // NOTE: Buffer producer: ISP DMA; Buffer consumer: CPU
        stream->second->cacheSync(
                buffer, IrisBufferQueue::SyncType::INVALIDATE);
        IRIS_LOGD("acquire stream(%d) buffer FD(%d)",
                streamID, buffer.ion_fd);

        // Create ImageBuffer
        sp<IImageBufferHeap> bufferHeap = buffer.buffer_heap;
        result = bufferHeap->createImageBuffer();

        // establish buffer mapping rules
        {
            const auto key = buffer.ion_fd;

            std::lock_guard<std::mutex> _l(mMapLock);
            auto search = mMap.find(key);
            if (search != mMap.end())
            {
                IRIS_LOGD("stream(%d) buffer key(0x%x) already exist, overwrite it",
                        streamID, key);
                mMap.erase(search);
            }
            if (mMap.emplace(key, std::move(buffer)).second == false)
                IRIS_LOGA("cannot emplace bufer: key(0x%x)", key);
        }

        //auto identifier(std::make_pair(toLiteral(stream->first),
        //                                         buffer.ion_fd));
        // TODO: Remove release buffer here, this is test only
        //releaseBuffer(identifier);
    }
#if 0
    AutoLockImg locker(result);

    // Buffer is a structure transparent to user
    // therefore we use it to callback to upper layer.
    // Iris::Buffer is paired with Buffer, it has more data type which
    // exposed too many BufferQueue detail, we only use it in Hal level

    // Callback Buffer to DepthCallback.
    IRIS_LOGD("BufferQueue equeue buffer: openId(%d), fullRaw(%zu) res(%d, %d) bufferAddr(%p)", mOpenId, result->getFD(0)
                                                                          , result->getImgSize().w, result->getImgSize().h
                                                                          , result.get());
#endif
    IRIS_LOGD("BufferQueue equeue buffer: openId(%d), res(%d, %d) bufferAddr(%p)", mOpenId, result->getImgSize().w
                                                                              , result->getImgSize().h
                                                                              , result.get());

    mInputListener->onBufferAvailable(mOpenId, result);

    return;
}

// ---------------------------------------------------------------------------

static void NSCam::security::depthThreadHandler(DepthCallback& callback)
{
    AutoLog();

    // set thread name
    prctl(PR_SET_NAME, "Depth@CbThread", 0, 0, 0);

    while (true)
    {
        if (!callback.threadLoop())
        {
            IRIS_LOGD("exit thread %s", __FUNCTION__);
            break;
        }
    };
}

// ---------------------------------------------------------------------------

DepthCallback::DepthCallback() : mReqCount(0)
                                    , mDepthPipe(nullptr)
                                    , mbDepthInit(false)
                                    , mbNeedCbIr(false)
{
}

void DepthCallback::init(std::vector<StreamInfo> vStreamInfo)
{
    AutoLog();

    // Create callback thread to work
    {
        mRequestExit.store(false, std::memory_order_relaxed);
        mbIsProcExecute.store(false, std::memory_order_relaxed);
        mCallbackWorker = std::thread(depthThreadHandler, std::ref(*this));

        mDepthWorker = std::thread([](DepthCallback& depthProc) {
            AutoLog();

            // set thread name
            prctl(PR_SET_NAME, "Depth@ProcessThread", 0, 0, 0);

            while (true)
            {
                if (!depthProc.postprocThreadLoop())
                {
                    IRIS_LOGD("exit thread %s", __FUNCTION__);
                    break;
                }
            };
        }, std::ref(*this));
    }

    IRIS_LOGD("callback threads created");

    mDebugDump = property_get_int32("debug.securecamera.dump.en", 0);

    // make debug dump path
    if (mDebugDump && !Utils::makePath(DUMP_PATH, 0660))
    {
        IRIS_LOGW("make debug dump path %s failed", DUMP_PATH);
    }
    // Set up stereo environment
    StereoSettingProvider::setStereoProfile(STEREO_SENSOR_PROFILE_FRONT_FRONT);
    StereoSettingProvider::setStereoFeatureMode(v1::Stereo::E_STEREO_FEATURE_ACTIVE_STEREO);
    StereoSettingProvider::setStereoModuleType(v1::Stereo::IR_AND_IR);
    StereoSettingProvider::setImageRatio(eRatio_16_9);
    //
    StereoSizeProvider * pSizeProvder = StereoSizeProvider::getInstance();
    MSize depthSize = pSizeProvder->getBufferSize(E_DEPTH_MAP, eSTEREO_SCENARIO_PREVIEW);
    MUINT32 boundary[3] = {0, 0, 0};
    // Y16 -> stride = w * 2
    MUINT32 depthMapStride[3] = {(MUINT32)depthSize.w*2, 0, 0};
    IImageBufferAllocator::ImgParam depthImgParam(eImgFmt_Y16, depthSize, depthMapStride, boundary, 3);

    IImageBufferAllocator *allocator = IImageBufferAllocator::getInstance();
    const char* name = "DepthImage";
    mDepthImage = allocator->alloc(name, depthImgParam);
    mDepthImage->lockBuf(name, eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_SW_WRITE_OFTEN);
}

void DepthCallback::unInit()
{
    AutoLog();

    requestExit();
    if (mCallbackWorker.joinable())
    {
        IRIS_LOGD("join callback worker +");
        mCallbackWorker.join();
        IRIS_LOGD("join callback worker -");
    }

    if (mDepthWorker.joinable())
    {
        IRIS_LOGD("join depth worker +");
        mDepthWorker.join();
        IRIS_LOGD("join depth worker -");
        if(mbDepthInit && mDepthPipe) {
            mDepthPipe->destroyInstance();
            mDepthPipe = nullptr;
        }
    }
}

void DepthCallback::registerCallback(Callback* cb, void* priv)
{
    AutoLog();

    if (cb == nullptr)
    {
        IRIS_LOGE("invalid callback");
        return;
    }

    const auto key(cb->getDescriptor());
    std::unique_lock<std::mutex> _l(mRegisteredCallbacksLock);
    auto search = mRegisteredCallbacks.find(key);
    if (CC_UNLIKELY(search != mRegisteredCallbacks.end()))
    {
        IRIS_LOGW("callback key(0x%x) already exist, overwrite it", key);
        mRegisteredCallbacks.erase(key);
    }

    if (mRegisteredCallbacks.emplace(key, std::make_pair(cb->getHook(), priv)).second == false)
        IRIS_LOGE("cannot emplace callback: key(0x%x)", key);
}

bool DepthCallback::cameraCallback(const void* param)
{
    AutoLog();

    bool needNotify = false;

    if (param == nullptr)
    {
        IRIS_LOGE("invalid callback result");
        return needNotify;
    }

    return needNotify;
}

void DepthCallback::requestExit()
{
    AutoLog();

    IRIS_LOGD("request callback thread to exit");

    //std::lock_guard<std::mutex> _l(mResultQueueLock);
    {
        std::unique_lock<std::mutex> _l(mMainResultQueueLock);
        mMainResultQueueCondition.notify_one();
    }

    {
        std::unique_lock<std::mutex> _l(mSubResultQueueLock);
        mSubResultQueueCondition.notify_one();
    }

    mRequestExit.store(true, std::memory_order_relaxed);

    {
        std::unique_lock<std::mutex> _l(mDepthProcLock);
        mDepthProcCondition.notify_one();
    }

    {
        std::unique_lock<std::mutex> _l(mDepthComepeteLock);
        mDepthComepeteCondition.notify_one();
    }
}

bool DepthCallback::threadLoop()
{
    //AutoLog();

    // TODO: Need to protect mRequestExit
    if (CC_UNLIKELY(mRequestExit.load(std::memory_order_relaxed)))
        return false;

    DepthResult mainResult;
    DepthResult subResult;

    int32_t main1Idx, main2Idx;
    StereoSettingProvider::getStereoSensorIndex(main1Idx, main2Idx);

    dequeuebuffer(main1Idx, mainResult);
    dequeuebuffer(main2Idx, subResult);

    sp<IImageBuffer> &imgMain = mainResult.imgBuffer;
    sp<IImageBuffer> &imgSub = subResult.imgBuffer;

    if(CC_UNLIKELY(imgMain.get() == nullptr || imgSub.get() == nullptr)) {
        IRIS_LOGD("Dequeue fail RRZO openId(%d) addr(%p)", main1Idx, imgMain.get());
        IRIS_LOGD("Dequeue fail RRZO openId(%d) addr(%p)", main2Idx, imgSub.get());
        return android::NO_MEMORY;
    }

    // TODO: Judge frame sync
    IMetadata *mainAppMeta = &mainResult.callbackInfo.appMeta;
    IMetadata *mainHalMeta = &mainResult.callbackInfo.halMeta;

    IMetadata *subAppMeta = &subResult.callbackInfo.appMeta;
    IMetadata *subHalMeta = &subResult.callbackInfo.halMeta;

    MINT64 mainTs, subTs;
    int32_t dropCamId;

    IMetadata::getEntry<MINT64>(mainHalMeta, MTK_P1NODE_FRAME_START_TIMESTAMP, mainTs);
    IMetadata::getEntry<MINT64>(subHalMeta, MTK_P1NODE_FRAME_START_TIMESTAMP, subTs);

    while(!isTimeSync(mainTs, subTs, dropCamId)) {
        IRIS_LOGD("We need to drop frame openId(%d)", dropCamId);
        if(dropCamId == main1Idx) {
            // Release previous buffer back to bufferQueue
            int fd;
            {
                AutoLockImg imgMainlock(mainResult.imgBuffer);
                fd = mainResult.imgBuffer->getFD(0);
                IRIS_LOGD("Ready to release fd(%d)", fd);
            }
            releaseBufferToBufferQueue(mMainListener, fd);
            dequeuebuffer(main1Idx, mainResult);
            IMetadata::getEntry<MINT64>(&mainResult.callbackInfo.halMeta, MTK_P1NODE_FRAME_START_TIMESTAMP, mainTs);

        } else {
            int fd;
            {
                AutoLockImg imgSublock(subResult.imgBuffer);
                fd = subResult.imgBuffer->getFD(0);
                IRIS_LOGD("Ready to release fd(%d)", fd);
            }
            releaseBufferToBufferQueue(mSubListener, fd);
            dequeuebuffer(main2Idx, subResult);
            IMetadata::getEntry<MINT64>(&subResult.callbackInfo.halMeta, MTK_P1NODE_FRAME_START_TIMESTAMP, subTs);
        }
    }
    //
    Buffer &mainFullRaw = mainResult.callbackInfo.fullRaw;
    Buffer &subFullRaw = subResult.callbackInfo.fullRaw;

    MBOOL bMainIsProjectorReq = false;
    MBOOL bSubIsProjectorReq  = false;

    {
        IMetadata *mainHalMeta = &mainResult.callbackInfo.halMeta;
        IMetadata *subHalMeta = &subResult.callbackInfo.halMeta;

        MINT32 mode1 = 0, mode2 = 0;
        bMainIsProjectorReq = (IMetadata::getEntry<MINT32>(mainHalMeta, MTK_IR_FLASH_MODE, mode1) &&
                                mode1 == MTK_IR_FLASH_MODE_PROJECTOR);
        bSubIsProjectorReq = (IMetadata::getEntry<MINT32>(subHalMeta, MTK_IR_FLASH_MODE, mode2) &&
                                mode2 == MTK_IR_FLASH_MODE_PROJECTOR);
    }

    if((bMainIsProjectorReq || bSubIsProjectorReq) && !mbIsProcExecute.load(std::memory_order_relaxed)) {

        std::unique_lock<std::mutex> _l(mDepthProcLock);
        mDepthProcQueue.push_back(std::make_pair(mainResult, subResult));
        // Start to do postprocessing
        mbIsProcExecute.store(true, std::memory_order_relaxed);
        IRIS_LOGD("Depth postprocessing notify");
        mDepthProcCondition.notify_one();
        mbNeedCbIr = true;
        return true;
    }

#if 1
    if(CC_LIKELY(!mRequestExit.load(std::memory_order_relaxed)) && mbNeedCbIr) {
        constexpr iris_callback_descriptor_t des = IRIS_CALLBACK_ADDR;
        std::unique_lock<std::mutex> _l(mRegisteredCallbacksLock);
        auto search = mRegisteredCallbacks.find(des);

        mainFullRaw.priv = search->second.second;

        IRIS_LOGD("Callback first IR frame");
        if (search != mRegisteredCallbacks.end()) {
            loadFunctionPointer<IRIS_PFN_CALLBACK_ADDR>(search->second.first)(mainFullRaw);
        }
        // Callback done
        mbNeedCbIr = false;
    }
#endif
    int mainFd, subFd;
    {
        sp<IImageBuffer> imgMain = mainResult.imgBuffer;
        sp<IImageBuffer> imgSub  = subResult.imgBuffer;
        AutoLockImg imgMainlock(imgMain);
        AutoLockImg imgSublock(imgSub);
        mainFd = imgMain->getFD(0);
        subFd = imgSub->getFD(0);
        IRIS_LOGD("Ready to release fd(%d, %d)", mainFd, subFd);
    }
    // Finish depth postprocessing
    releaseBufferToBufferQueue(mMainListener, mainFd);
    releaseBufferToBufferQueue(mSubListener, subFd);

    // TODO: After callback depth to upper layer we need to clear up queue
    return true;
}

bool DepthCallback::postprocThreadLoop()
{
    // TODO: Need to protect mRequestExit
    if (CC_UNLIKELY(mRequestExit.load(std::memory_order_relaxed)))
        return false;

    if(mbIsProcExecute.load(std::memory_order_relaxed)) {
        std::unique_lock<std::mutex> _l(mDepthProcLock);
        // exit loop if receiving available result or exit request
        while (!mRequestExit.load(std::memory_order_relaxed) && mDepthProcQueue.empty())
            mDepthProcCondition.wait(_l);
        IRIS_LOGD("Start to handle postproc");
        if(CC_LIKELY(!mRequestExit.load(std::memory_order_relaxed) && !mDepthProcQueue.empty())) {
            std::pair<DepthResult, DepthResult> depthInput = *mDepthProcQueue.begin();
            if (mDepthProcQueue.begin() != mDepthProcQueue.end())
                mDepthProcQueue.erase(mDepthProcQueue.begin());

            depthPostProcessing(depthInput.first, depthInput.second);
            // Finish postProcessing. We release buffer
            // Be aware of that we should unlock buffer before release
            // buffer to Queue
            int mainFd, subFd;
            {
                sp<IImageBuffer> imgMain = depthInput.first.imgBuffer;
                sp<IImageBuffer> imgSub  = depthInput.second.imgBuffer;
                AutoLockImg imgMainlock(imgMain);
                AutoLockImg imgSublock(imgSub);
                mainFd = imgMain->getFD(0);
                subFd = imgSub->getFD(0);

                //Call back depth to service
                Buffer &mainBuff = depthInput.first.callbackInfo.fullRaw;

                Buffer depthResult
                {
                    .addr.secureHandle.fd = mDepthImage->getFD(0),
                    .attribute.identifier = mainBuff.attribute.identifier,
                    .attribute.length = mDepthImage->getBufSizeInBytes(0),
                    .attribute.size = mDepthImage->getImgSize(),
                    .attribute.stride = mDepthImage->getBufStridesInBytes(0),
                    .attribute.format = eImgFmt_Y16,//mDepthImage->getImgFormat(),
                    .path = NSCam::security::Path::BAYER,
                    .priv = mainBuff.priv
                };
                constexpr iris_callback_descriptor_t des = IRIS_CALLBACK_ADDR;
                std::unique_lock<std::mutex> _l(mRegisteredCallbacksLock);
                auto search = mRegisteredCallbacks.find(des);

                depthResult.priv = search->second.second;

                IRIS_LOGD("Callback depth fmt(%p)", mDepthImage->getImgFormat());
                if (search != mRegisteredCallbacks.end()) {
                    loadFunctionPointer<IRIS_PFN_CALLBACK_ADDR>(search->second.first)(depthResult);
                }
            }

            IRIS_LOGD("Finish to return fd(%d, %d)", mainFd, subFd);
            releaseBufferToBufferQueue(mMainListener, mainFd);
            releaseBufferToBufferQueue(mSubListener, subFd);
            mbIsProcExecute.store(false, std::memory_order_relaxed);
        }

    }

    return true;
}

void DepthCallback::releaseBufferToBufferQueue(weak_ptr<DepthConsumerListener> listener, int ionFd)
{
    // Finish depth postprocessing
    // TODO: Need to unlock Image Buffer before release
    auto releaseListener = listener.lock();

    IRIS_LOGD("release buffer FD(%d)", ionFd);

    if(CC_UNLIKELY(releaseListener == nullptr)) {
        IRIS_LOGE("listener is null addr(%p)", releaseListener.get());
        return;
    }

    auto identifier(std::make_pair(toLiteral(StreamID::RESIZED_RAW), ionFd));

    releaseListener->releaseBuffer(identifier);
    return;
}

Result DepthCallback::depthPostProcessing(DepthResult &mainInfo, DepthResult &subInfo)
{
    AutoLockImg imgMainlock(mainInfo.imgBuffer);
    AutoLockImg imgSublock(subInfo.imgBuffer);
    sp<IImageBuffer> imgMain = mainInfo.imgBuffer;
    sp<IImageBuffer> imgSub  = subInfo.imgBuffer;

    if(!mbDepthInit) {
        int32_t main1Idx, main2Idx;
        StereoSettingProvider::getStereoSensorIndex(main1Idx, main2Idx);
        sp<DepthMapPipeOption> pPipeOption = new DepthMapPipeOption(IR_AND_IR, eDEPTHNODE_MODE_ACTIVE_STEREO, eDEPTH_FLOW_TYPE_STANDARD);
        sp<DepthMapPipeSetting> pPipeSetting = new DepthMapPipeSetting();

        pPipeSetting->miSensorIdx_Main1 = main1Idx;
        pPipeSetting->miSensorIdx_Main2 = main2Idx;
        pPipeSetting->mszRRZO_Main1 = imgMain->getImgSize();
        // Enqueue request to depthPipe
        mDepthPipe = IDepthMapPipe::createInstance(pPipeSetting, pPipeOption);
        mDepthPipe->init();
        mbDepthInit = true;
    }

    if(mDepthPipe == nullptr) {
        IRIS_LOGE("mDepthPipe is null please check");
        return INVALID_OPERATION;
    }

    IMetadata* mainAppMeta = &mainInfo.callbackInfo.appMeta;
    IMetadata* mainHalMeta = &mainInfo.callbackInfo.halMeta;
    IMetadata* subHalMeta = &subInfo.callbackInfo.halMeta;
    IMetadata oHalMeta;
    IMetadata oAppMeta;
    int32_t reqCount = mReqCount;

    sp<IDepthMapEffectRequest> pEffReq = prepareEnqueRequest(mReqCount++, eSTATE_NORMAL, imgMain, imgSub);

    // Set up unique key
    MINT64 mainTs, subTs;

    IMetadata::getEntry<MINT64>(mainHalMeta, MTK_P1NODE_FRAME_START_TIMESTAMP, mainTs);
    IMetadata::getEntry<MINT64>(subHalMeta, MTK_P1NODE_FRAME_START_TIMESTAMP, subTs);
    IMetadata::setEntry<MINT32>(mainHalMeta, MTK_PIPELINE_UNIQUE_KEY, static_cast<MINT32>(mainTs));
    IMetadata::setEntry<MINT32>(subHalMeta, MTK_PIPELINE_UNIQUE_KEY, static_cast<MINT32>(subTs));
    //

    //Set up metadata
    // PBID_IN_APP_META,
    // PBID_IN_HAL_META_MAIN1,
    // PBID_IN_HAL_META_MAIN2,
    // PBID_IN_P1_RETURN_META,
    // output meta
    // PBID_OUT_APP_META,
    // PBID_OUT_HAL_META,
    pEffReq->pushRequestMetadata({PBID_IN_APP_META, eBUFFER_IOTYPE_INPUT}, mainAppMeta);
    pEffReq->pushRequestMetadata({PBID_IN_HAL_META_MAIN1, eBUFFER_IOTYPE_INPUT}, mainHalMeta);
    pEffReq->pushRequestMetadata({PBID_IN_HAL_META_MAIN2, eBUFFER_IOTYPE_INPUT}, subHalMeta);
    // Depth "PBID_IN_P1_RETURN_META" means that it is P1 node output APP metadata
    // Depth pipe want to parse MTK_SENSOR_SENSITIVITY
    pEffReq->pushRequestMetadata({PBID_IN_P1_RETURN_META, eBUFFER_IOTYPE_INPUT}, mainAppMeta);
    // end
    pEffReq->pushRequestMetadata({PBID_OUT_APP_META, eBUFFER_IOTYPE_OUTPUT}, &oAppMeta);
    pEffReq->pushRequestMetadata({PBID_OUT_HAL_META, eBUFFER_IOTYPE_OUTPUT}, &oHalMeta);

    mDepthPipe->enque(pEffReq);

    {
        std::unique_lock<std::mutex> _l(mDepthComepeteLock);
        IRIS_LOGD("Depth processing+");
        mDepthComepeteCondition.wait(_l);
        IRIS_LOGD("Depth processing-");
    }
#if 0
    // Finish depth postprocessing
    releaseBufferToBufferQueue(mMainListener, mainInfo.imgBuffer);
    releaseBufferToBufferQueue(mSubListener, subInfo.imgBuffer);

    mbIsProcExecute.store(false, std::memory_order_relaxed);
#endif
    return OK;
}

void DepthCallback::onBufferAvailable(unsigned int openId, sp<IImageBuffer> inputBuffer)
{
    int32_t main1Idx, main2Idx;
    StereoSettingProvider::getStereoSensorIndex(main1Idx, main2Idx);

    if (openId == main1Idx) {
        //std::unique_lock<std::mutex> _l(mMainQueueLock);
        mMainQueue.push_back(inputBuffer);
        //mMainQueueCondition.notify_one();
    } else {
        //std::unique_lock<std::mutex> _l(mSubQueueLock);
        mSubQueue.push_back(inputBuffer);
        //mSubQueueCondition.notify_one();
    }

    return;
}

void DepthCallback::onMetaAvailable(unsigned int openId, DepthCallbackInfo callbackInfo)
{
    //IRIS_LOGD("onMetaAvailable: openId(%d), reqNum(%d)", openId, callbackInfo.reqNum);
    int32_t main1Idx, main2Idx;
    StereoSettingProvider::getStereoSensorIndex(main1Idx, main2Idx);

    if (openId == main1Idx) {
        DepthResult result;

        {   //Query buffer from Queue. Buffer would be queued before metadata
            //std::unique_lock<std::mutex> _l(mMainQueueLock);
            result.imgBuffer = *mMainQueue.begin();
            if (mMainQueue.begin() != mMainQueue.end())
                mMainQueue.erase(mMainQueue.begin());
        }

        result.callbackInfo = callbackInfo;

        {
            std::unique_lock<std::mutex> _l(mMainResultQueueLock);
            mMainResultQueue.push_back(result);
            mMainResultQueueCondition.notify_one();
        }

    } else {
        DepthResult result;

        {   //Query buffer from Queue. Buffer would be queued before metadata
            //std::unique_lock<std::mutex> _l(mSubQueueLock);
            result.imgBuffer = *mSubQueue.begin();
            if (mSubQueue.begin() != mSubQueue.end())
                mSubQueue.erase(mSubQueue.begin());
        }

        result.callbackInfo = callbackInfo;

        {
            std::unique_lock<std::mutex> _l(mSubResultQueueLock);
            mSubResultQueue.push_back(result);
            mSubResultQueueCondition.notify_one();
        }
    }

    return;
}

Result DepthCallback::dequeuebuffer(unsigned int openId, DepthResult &inputBuffer)
{
    //AutoLog();
    int32_t main1Idx, main2Idx;
    StereoSettingProvider::getStereoSensorIndex(main1Idx, main2Idx);

    if (openId == main1Idx) {
        std::unique_lock<std::mutex> _l(mMainResultQueueLock);

        // exit loop if receiving available result or exit request
        while (!mRequestExit.load(std::memory_order_relaxed) && mMainResultQueue.empty())
            mMainResultQueueCondition.wait(_l);
        if(CC_LIKELY(!mRequestExit.load(std::memory_order_relaxed))) {
            inputBuffer = *mMainResultQueue.begin();
            if (mMainResultQueue.begin() != mMainResultQueue.end())
                mMainResultQueue.erase(mMainResultQueue.begin());

            //IRIS_LOGD("Main queue size(%zu)", mMainResultQueue.size());
        }
    } else {
        std::unique_lock<std::mutex> _l(mSubResultQueueLock);

        // exit loop if receiving available result or exit request
        while (!mRequestExit.load(std::memory_order_relaxed) && mSubResultQueue.empty())
            mSubResultQueueCondition.wait(_l);
        if(CC_LIKELY(!mRequestExit.load(std::memory_order_relaxed))) {
            inputBuffer = *mSubResultQueue.begin();
            if (mSubResultQueue.begin() != mSubResultQueue.end())
                mSubResultQueue.erase(mSubResultQueue.begin());

            //IRIS_LOGD("Sub queue size(%zu)", mSubResultQueue.size());
        }
    }

    return OK;
}

bool DepthCallback::isTimeSync(uint64_t t1, uint64_t t2, int32_t &dropCamId)
{
    uint64_t timeDiffMs = std::abs(MINT64(t1 - t2))/ 1000000;
    IRIS_LOGD("Time diff in (%d) ms", timeDiffMs);

    int32_t main1Idx, main2Idx;
    StereoSettingProvider::getStereoSensorIndex(main1Idx, main2Idx);

    if (timeDiffMs > 1) {
        if (t1 > t2) {
            dropCamId = main2Idx;
        } else {
            dropCamId = main1Idx;
        }
        return false;
    }

    return true;
}

sp<IDepthMapEffectRequest> DepthCallback::prepareEnqueRequest(int request_id, DepthMapPipeOpState eState
                                                                        , sp<IImageBuffer> rrzMain, sp<IImageBuffer> rrzSub)
{
    IRIS_LOGD("+: reqID = %d, request state=%d", request_id, eState);
    // new request
    sp<IDepthMapEffectRequest> pRequest =  IDepthMapEffectRequest::createInstance(request_id, DepthCallback::depthCompleteCallback);

    // prepare input frame info: RSRAW1
    pRequest->pushRequestImageBuffer({PBID_IN_RSRAW1, eBUFFER_IOTYPE_INPUT}, rrzMain);
    // prepare input frame info: RSRAW2
    pRequest->pushRequestImageBuffer({PBID_IN_RSRAW2, eBUFFER_IOTYPE_INPUT}, rrzSub);
    // prepare output
    //prepareReqOutputBuffer(eState, pRequest);
    pRequest->pushRequestImageBuffer({PBID_OUT_DEPTHMAP, eBUFFER_IOTYPE_OUTPUT}, mDepthImage);

    IRIS_LOGD("-");
    return pRequest;
}

MVOID DepthCallback::depthCompleteCallback(MVOID* tag, ResultState state, sp<IDualFeatureRequest>& request)
{
    std::unique_lock<std::mutex> _l(mDepthComepeteLock);

    IRIS_LOGD("DepthCallback is back!!!");
    mDepthComepeteCondition.notify_one();
    return;
}

void DepthCallback::setListener(weak_ptr<DepthConsumerListener> mainListener, weak_ptr<DepthConsumerListener> subListener) {
    mMainListener = mainListener;
    mSubListener  = subListener;
}
