#include <cstdlib>

#include <dlfcn.h>
#include <unistd.h>

#include <ion/ion.h>
#include <ion.h>
#include <linux/mtk_ion.h>
#include <linux/ion_drv.h>

#ifndef LEGACY_PATH // non-legacy path
#include <mtkcam/utils/imgbuf/ImageBufferHeap.h>
#else // legacy path
#include <mtkcam/utils/ImageBufferHeap.h>
#endif // legacy path

#define DEBUG_LOG_TAG "tee_test"

#include <mtkcam/main/security/utils/Debug.h>
#include <mtkcam/utils/imgbuf/ISecureImageBufferHeap.h>

#ifdef __LP64__
#define MTK_IRIS_SEC_ISP_LIB "/vendor/lib64/libtestca.so"
#else
#define MTK_IRIS_SEC_ISP_LIB "/vendor/lib/libtestca.so"
#endif

// TODO: symbol names should be placed in CA's public header
#define MTK_IRIS_SEC_ISP_CREATE  "test_create"
#define MTK_IRIS_SEC_ISP_DESTROY "test_destroy"
#define MTK_IRIS_SEC_ISP_OPEN "test_open"
#define MTK_IRIS_SEC_ISP_INIT "test_init"
#define MTK_IRIS_SEC_ISP_UNINIT "test_uninit"
#define MTK_IRIS_SEC_ISP_ENQUE "test_enque"
#define MTK_IRIS_SEC_ISP_DEQUE "test_deque"
#define MTK_IRIS_SEC_ISP_CLOSE "test_close"

// ---------------------------------------------------------------------------

typedef void *test_create_ptr();
typedef uint32_t *test_destroy_ptr(void*);
typedef int test_open_ptr(void*);
typedef int test_init_ptr(void*);
typedef int test_uninit_ptr(void*);
typedef int test_enque_ptr(void*, uint64_t, uint32_t);
typedef int test_deque_ptr(void*, uint32_t*);
typedef int test_close_ptr(void*);

// ---------------------------------------------------------------------------

test_create_ptr *g_isp_ca_HandleCreate = nullptr;
test_destroy_ptr *g_isp_ca_HandleDestroy = nullptr;
test_open_ptr *g_isp_ca_open = nullptr;
test_close_ptr *g_isp_ca_close = nullptr;
test_init_ptr *g_isp_ca_init = nullptr;
test_uninit_ptr *g_isp_ca_uninit = nullptr;
test_enque_ptr *g_isp_ca_enque = nullptr;
test_deque_ptr *g_isp_ca_deque = nullptr;

using namespace NSCam;

// ---------------------------------------------------------------------------

static constexpr uint32_t kBufferSize = (1920*1080);
static constexpr int32_t format = eImgFmt_BAYER10;
static constexpr int kBufferUsage =
    (eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);

// ---------------------------------------------------------------------------

static inline void closeIonDevice(int ionFD)
{
    if (ionFD >= 0)
        ion_close(ionFD);
}

static void* getSymbolAddress(void* handle, const char* symbol)
{
    void* generic_ptr = dlsym(handle, symbol);
    const char *dlsym_error = dlerror();
    if (dlsym_error)
        IRIS_LOGE("load %s failed: %s", symbol, dlsym_error);

    return generic_ptr;
}

// ---------------------------------------------------------------------------

int main ()
{
    int ret = EXIT_SUCCESS;

    // open ION device
    const int g_ion_fd = mt_ion_open(DEBUG_LOG_TAG);
    if (g_ion_fd < 0)
    {
        IRIS_LOGE("ion_open failed!");
        ret = EXIT_FAILURE;
    }

    // get memory page size
    size_t pageSize = sysconf(_SC_PAGESIZE);

    ion_user_handle_t ionHandle;
    if (!ret && ion_alloc(g_ion_fd, kBufferSize, pageSize,
                ION_HEAP_MULTIMEDIA_SEC_MASK, 0, &ionHandle))
    {
        IRIS_LOGE("ion_alloc failed!");
        ret = EXIT_FAILURE;
    }

    void *ispCA = nullptr;
    if (!ret)
    {
        ispCA = dlopen(MTK_IRIS_SEC_ISP_LIB, RTLD_NOW);
        if (ispCA == nullptr)
        {
            IRIS_LOGE("ispCA open failed: %s", dlerror());
            ret = EXIT_FAILURE;
        }

        IRIS_LOGD("ispCA open success!");

        // reset error
        dlerror();

        // load APIs
        char *symbol = MTK_IRIS_SEC_ISP_CREATE;
        ret = [&]()
        {
            g_isp_ca_HandleCreate =
                (test_create_ptr*) getSymbolAddress(ispCA, symbol);
            if (!g_isp_ca_HandleCreate) ret |= EXIT_FAILURE;

            symbol = MTK_IRIS_SEC_ISP_DESTROY;
            g_isp_ca_HandleDestroy =
                (test_destroy_ptr*) getSymbolAddress(ispCA, symbol);
            if (!g_isp_ca_HandleDestroy) ret |= EXIT_FAILURE;

            symbol = MTK_IRIS_SEC_ISP_OPEN;
            g_isp_ca_open =
                (test_open_ptr*) getSymbolAddress(ispCA, symbol);
            if (!g_isp_ca_open) ret |= EXIT_FAILURE;

            symbol = MTK_IRIS_SEC_ISP_INIT;
            g_isp_ca_init =
                (test_init_ptr*) getSymbolAddress(ispCA, symbol);
            if (!g_isp_ca_init) ret |= EXIT_FAILURE;

            symbol = MTK_IRIS_SEC_ISP_UNINIT;
            g_isp_ca_uninit =
                (test_uninit_ptr*) getSymbolAddress(ispCA, symbol);
            if (!g_isp_ca_uninit) ret |= EXIT_FAILURE;

            symbol = MTK_IRIS_SEC_ISP_ENQUE;
            g_isp_ca_enque =
                (test_enque_ptr*) getSymbolAddress(ispCA, symbol);
            if (!g_isp_ca_enque) ret |= EXIT_FAILURE;

            symbol = MTK_IRIS_SEC_ISP_DEQUE;
            g_isp_ca_deque =
                (test_deque_ptr*) getSymbolAddress(ispCA, symbol);
            if (!g_isp_ca_deque) ret |= EXIT_FAILURE;

            symbol = MTK_IRIS_SEC_ISP_CLOSE;
            g_isp_ca_close =
                (test_close_ptr*) getSymbolAddress(ispCA, symbol);
            if (!g_isp_ca_close) ret |= EXIT_FAILURE;

            return ret;
        }();
    }

    if (!ret)
    {
        void *ispHandle = g_isp_ca_HandleCreate();
        IRIS_LOGI("isp handle is created");

        uint32_t err = 0;

        if ((err = g_isp_ca_open(ispHandle)) != 0)
        {
            IRIS_LOGE("isp ca open failed");
            return -1;
        }

        if (g_isp_ca_init(ispHandle) != 0)
        {
            IRIS_LOGE("isp ca init failed");
            return -1;
        }
/*
        // query physical address via handle
        struct ion_sys_data sys_data;
        sys_data.sys_cmd = ION_SYS_GET_PHYS;
        sys_data.get_phys_param.handle = ionHandle;
        if (ion_custom_ioctl(g_ion_fd, ION_CMD_SYSTEM, &sys_data))
        {
            IRIS_LOGE("ion_custom_ioctl failed to get physical address");
            return -1;
        }
        else
        {
            IRIS_LOGI("Secure memory allocated OK, handle(0x%x), size(%lu)",
                    sys_data.get_phys_param.phy_addr, sys_data.get_phys_param.len);
        }

        uint32_t sec_handle = sys_data.get_phys_param.phy_addr;
        uint32_t sec_size = sys_data.get_phys_param.len;

        IRIS_LOGD("sec_handle : %p", sec_handle);
        IRIS_LOGD("FD : %d", g_ion_fd);

        // encapsulate the secure imagebuffer
        size_t bufStridesInBytes[3] { 1920*10/8, 0, 0 };
        size_t bufBoundaryInBytes[3] {};
        IImageBufferAllocator::ImgParam imgParam(
                format,
                MSize(1920, 1080),
                bufStridesInBytes, bufBoundaryInBytes, 1);

        PortBufInfo_v1 portBufInfo( g_ion_fd, (uintptr_t)(sec_handle),0, 1, 0);

        ImageBufferHeap* const bufferHeap(
                ImageBufferHeap::create(DEBUG_LOG_TAG, imgParam ,portBufInfo));
        if (bufferHeap == nullptr)
        {
            IRIS_LOGE("create Image Buffer heap failed");
            return -1;
        }

        IImageBuffer* imageBuffer = bufferHeap->createImageBuffer();
        if (imageBuffer == nullptr)
        {
            IRIS_LOGE("create Image Buffer failed");
            return -1;
        }

        if(!(imageBuffer->lockBuf(DEBUG_LOG_TAG, kBufferUsage)))
        {
            IRIS_LOGE("Image Buffer lock failed");
            return -1;
        }

        uint32_t buf_sec_handle = imageBuffer->getBufVA(0);
        IRIS_LOGD("VA buf_sec_handle : %p", buf_sec_handle);

        uint32_t buf_sec_handle_pa = imageBuffer->getBufPA(0);
        IRIS_LOGD("PA buf_sec_handle : %p", buf_sec_handle_pa);

        uint32_t buf_fd = imageBuffer->getFD(0);
        IRIS_LOGD("FD : %d", buf_fd);
*/

        uint32_t sec_size = kBufferSize;
        ISecureImageBufferHeap* const ionBufferHeap(ISecureImageBufferHeap::create(DEBUG_LOG_TAG,
                    IImageBufferAllocator::ImgParam(kBufferSize, 0), ISecureImageBufferHeap::AllocExtraParam(0, 1, 0)));
        if (ionBufferHeap == nullptr)
        {
            IRIS_LOGE("create Image Buffer heap failed");
            return -1;
        }

        IImageBuffer* _imageBuffer = ionBufferHeap->createImageBuffer();
        if (_imageBuffer == nullptr)
        {
            IRIS_LOGE("createIMageBUffer failed");
            return -1;
        }

        if (!_imageBuffer->lockBuf(DEBUG_LOG_TAG, kBufferUsage))
        {
            IRIS_LOGE("Image Buffer lock failed");
            return -1;
        }

        uint32_t buf_sec_handle = _imageBuffer->getBufVA(0);
        IRIS_LOGD("VA buf_sec_handle : %p", buf_sec_handle);

        uint32_t buf_sec_handle_pa = _imageBuffer->getBufPA(0);
        IRIS_LOGD("PA buf_sec_handle : %p", buf_sec_handle_pa);

        uint32_t buf_fd = _imageBuffer->getFD(0);
        IRIS_LOGD("FD : %d", buf_fd);

        if (g_isp_ca_enque(ispHandle, buf_sec_handle, sec_size) != 1)
        {
            IRIS_LOGE("isp ca enque failed!, LINE : %d", __LINE__);
            return -1;
        }
        IRIS_LOGD("enqueue done");

        uint32_t res_handle;

        if (g_isp_ca_deque(ispHandle, &res_handle) != 1)
        {
            IRIS_LOGE("isp ca deque failed!, LINE : %d", __LINE__);
            return -1;
        }
        IRIS_LOGD("sec_handle : %p", res_handle);
        IRIS_LOGD("deque done!");

        if (!_imageBuffer->unlockBuf(DEBUG_LOG_TAG))
        {
            IRIS_LOGE("Image Buffer unlock failed");
            return -1;
        }

        IRIS_LOGD("close session!");

        if(g_isp_ca_close(ispHandle) != 1)
        {
            IRIS_LOGE("isp ca close session failed");
            return -1;
        }
        IRIS_LOGD("close session done");

        if (g_isp_ca_HandleDestroy(ispHandle) != 0)
        {
            IRIS_LOGE("destroy isp handle failed");
        }
        else
        {
            ispHandle = nullptr;
            IRIS_LOGI("isp handle is destroyed");
        }
    }

    // free ION buffer handle
    if (ion_free(g_ion_fd, ionHandle))
        IRIS_LOGE("ion_free failed!");
    else
        IRIS_LOGI("Secure memory free OK, ionHandle(%d)", ionHandle);

    // close ION device
    closeIonDevice(g_ion_fd);
    if (ispCA)
        dlclose(ispCA);

    return EXIT_SUCCESS;
}
