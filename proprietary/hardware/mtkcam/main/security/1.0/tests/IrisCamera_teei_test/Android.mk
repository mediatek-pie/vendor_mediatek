LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

ifeq ($(strip $(MTK_IRIS_SUPPORT)), yes)

-include $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/mtkcam.mk

LOCAL_MODULE := iriscam_teei_test
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

LOCAL_ADDITIONAL_DEPENDENCIES := $(LOCAL_PATH)/Android.mk

MTK_PATH_CAM := $(MTK_PATH_SOURCE)/hardware/mtkcam

LOCAL_SRC_FILES := \
    teei_test.cpp

LOCAL_C_INCLUDES := \
    $(MTK_PATH_CAM)/include

ifeq ($(MTK_ION_SUPPORT), yes)
LOCAL_CFLAGS += -DUSING_MTK_ION
LOCAL_C_INCLUDES += $(TOP)/system/core/libion/include \
    $(TOP)/$(MTK_PATH_SOURCE)/external/libion_mtk/include

endif
LOCAL_SHARED_LIBRARIES := \
	liblog \
	libutils \
	libcutils \
    libmtkcam_stdutils \
    libdl \
    libimageio \
    libmtkcam_imgbuf \
    libmtkcam_hwutils \
    libcam.iopipe

ifeq ($(MTK_ION_SUPPORT),yes)
LOCAL_SHARED_LIBRARIES += libion libion_mtk
endif

LOCAL_MODULE_TAGS := eng userdebug

LOCAL_CFLAGS += $(MTKCAM_CFLAGS)
LOCAL_CFLAGS := -DLOG_TAG=\"SecureCamera/TestApplication\"

include $(MTK_EXECUTABLE)

endif # MTK_IRIS_SUPPORT
