/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define DEBUG_LOG_TAG "IonHelper"

#include <mtkcam/main/security/utils/Debug.h>

#include <mtkcam/main/security/utils/IonHelper.h>
#include <mtkcam/utils/std/Format.h>
#include <mtkcam/def/ImageFormat.h>

#include <ion/ion.h>
#include <ion.h>
#include <linux/mtk_ion.h>
#include <linux/ion_drv.h>

#include <unistd.h>
#include <stdlib.h>

#ifdef USING_MTK_TEE
#include <uree/system.h>
#include <uree/mem.h>
#endif // USING_MTK_TEE

typedef uint32_t SECHAND;

using namespace NSCam::security;

// ---------------------------------------------------------------------------

const char *IonHelper::kDumpPath = "/sdcard/raw";
const char *IonHelper::kMemSrvName = "com.mediatek.geniezone.srv.mem";
int64_t IonHelper::sDumpFileCounter = 0;

// ---------------------------------------------------------------------------

static std::pair<SECHAND, unsigned long> getSecureInfo(
        const int ionDevFD, const ion_user_handle_t& handle)
{
    struct ion_sys_data sys_data;
    sys_data.sys_cmd = ION_SYS_GET_PHYS;
    sys_data.get_phys_param.handle = handle;
    if (ion_custom_ioctl(ionDevFD, ION_CMD_SYSTEM, &sys_data))
    {
        IRIS_LOGE("ion_custom_ioctl failed to get secure handle");
        return std::pair<SECHAND, unsigned long>();
    }

    IRIS_LOGI("Secure memory: handle(0x%x) size(%lu)",
            sys_data.get_phys_param.phy_addr, sys_data.get_phys_param.len);

    return std::pair<SECHAND, unsigned long>(sys_data.get_phys_param.phy_addr, sys_data.get_phys_param.len);
}

constexpr static bool isYUVFormat(NSCam::EImageFormat format)
{
    switch (format)
    {
        // NOTE: please extend verified format here
        case NSCam::EImageFormat::eImgFmt_NV12:
            return true;
        default:
            return false;
    }
}

// ---------------------------------------------------------------------------

IonHelper::IonHelper()
{
    // open ION device
    mIonDevFd.first = mt_ion_open(DEBUG_LOG_TAG);
    if (mIonDevFd.first < 0)
    {
        IRIS_LOGE("open Ion device failed!");
        mIonDevFd.first = -1;
    }
}

IonHelper::~IonHelper()
{
    // close ION device
    {
        std::lock_guard<std::mutex> _l(mIonDevFd.second);
        if (mIonDevFd.first != -1)
        {
            ion_close(mIonDevFd.first);
            mIonDevFd.first = -1;
        }
    }
}

void* IonHelper::mmap(void *addr, size_t length, int prot, int flags,
        int share_fd, off_t offset)
{
    void *mappingAddress = ::mmap(addr, length, prot, flags, share_fd, offset);
    if (mappingAddress == MAP_FAILED)
    {
        IRIS_LOGE("mmap failed(%s): length(%zu) prot(%d) flags(%d)" \
                " share_fd(%d) offset(%ld) addr(0x%" PRIxPTR ")",
                strerror(errno), length, prot, flags, share_fd, offset,
                reinterpret_cast<uintptr_t>(addr));
    }

    return mappingAddress;
}

int IonHelper::munmap(void *addr, size_t length)
{
    int ret = ::munmap(addr, length);

    if (ret < 0)
    {
        IRIS_LOGE("munmap failed(%d:%s): addr(0x%" PRIxPTR ") length(%zu)",
                ret, strerror(errno), reinterpret_cast<uintptr_t>(addr), length);
    }

    return ret;
}

void IonHelper::dumpBuffer(int fd, size_t size, int width, int height,
        size_t stride, int format, bool isSecure)
{
        AutoLog();

        char fileName[512];

        ::snprintf(fileName, sizeof(fileName), "%sbuf_%" PRId64 "_%d_%d_%zu.raw",
                kDumpPath, ++sDumpFileCounter, width, height, stride);

        int dumpFileFD = ::open(fileName, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU);
        if (dumpFileFD < 0)
        {
            IRIS_LOGE("%s: fail to open %s", __FUNCTION__, fileName);
            return;
        }

        int widthInBytes = (width * Utils::Format::queryPlaneBitsPerPixel(format, 0)) >> 3;

        IRIS_LOGD("dump %s buffer +", isSecure ? "secure" : "normal");

        if (!isSecure)
        {
            char *addr = static_cast<char *>(
                    mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd));

            char *baseAddr = addr;

            IRIS_LOGD("%s: bufferAddr(0x%" PRIxPTR ") dataSize(%zu) widthInBytes(%d) sharedFD(%d)",
                    __FUNCTION__, reinterpret_cast<uintptr_t>(addr), size, widthInBytes, fd);

            for (int i = 0; i < height; i++)
            {
                ::write(dumpFileFD, baseAddr, widthInBytes);
                baseAddr += stride;
            }

            munmap(addr, size);
        }
#ifdef USING_MTK_TEE
        else
        {
            ion_user_handle_t ionHandle;
            SECHAND secHandle;

            // get handle from ION file descriptor and
            // increase ION handle reference counting
            {
                std::lock_guard<std::mutex> _l(mIonDevFd.second);
                ion_import(mIonDevFd.first, fd, &ionHandle);
                IRIS_LOGD("ION handle(%d)", ionHandle);
            }
            auto secureInfo = getSecureInfo(mIonDevFd.first, ionHandle);

            UREE_SESSION_HANDLE memSrvSession;

            uint32_t secureMemorySize = static_cast<uint32_t>(secureInfo.second);
            if (secureInfo.second != secureMemorySize)
                IRIS_LOGE("mispatch size after narrowed down");

            // create session for shared memory
            TZ_RESULT ret = UREE_CreateSession(kMemSrvName, &memSrvSession);
            if (ret != TZ_RESULT_SUCCESS)
                IRIS_LOGE("create mem sesion failed(%d)", ret);

            // shared memory registration
            {
                // allocate shared memory, which size is the same as the secure one
                std::unique_ptr<char[], std::function<void(char[])>>
                    sharedMemory(reinterpret_cast<char *>(memalign(sysconf(_SC_PAGESIZE), secureMemorySize)), [](char memory[])
                            {
                                IRIS_LOGD("release shared memory");
                                free(memory);
                            });
                IRIS_LOGD("allocate shared memory: size(%u)", secureMemorySize);

                UREE_SHAREDMEM_HANDLE sharedMemoryHandle;
                UREE_SHAREDMEM_PARAM sharedMemoryParam {
                    .buffer = sharedMemory.get(), .size = secureMemorySize };
                ret = UREE_RegisterSharedmem(
                        memSrvSession, &sharedMemoryHandle, &sharedMemoryParam);
                if (ret != TZ_RESULT_SUCCESS)
                    IRIS_LOGE("register shared memory to MTK TEE failed(%d)", ret);

                // copy secure memory to shared memory
                ret = UREE_ION_CP_Chm2Shm(
                        memSrvSession, sharedMemoryHandle,
                        secureInfo.first, secureMemorySize);
                if (ret != TZ_RESULT_SUCCESS)
                {
                    if (ret == TZ_RESULT_ERROR_NOT_SUPPORTED)
                    {
                        IRIS_LOGW("UREE_ION_CP_Chm2Shm is not supported(%d)", ret);
                    }
                    else
                    {
                        IRIS_LOGE("copy from secure(%u) to shared memory(%u) failed(%d)",
                                secureInfo.first, sharedMemoryHandle, ret);
                    }
                }

                ret = UREE_UnregisterSharedmem(memSrvSession, sharedMemoryHandle);
                if (ret != TZ_RESULT_SUCCESS)
                    IRIS_LOGE("unregister shared memory from MTK TEE failed(%d)", ret);

                // dump buffer to file
                char *baseAddr = sharedMemory.get();

                IRIS_LOGD("%s: bufferAddr(0x%" PRIxPTR ") dataSize(%zu) widthInBytes(%d) sharedFD(%d)",
                        __FUNCTION__, reinterpret_cast<uintptr_t>(baseAddr), size, widthInBytes, fd);

                if (!isYUVFormat(static_cast<NSCam::EImageFormat>(format)))
                {
                    // strip padding for secure Bayer-pattern buffer for off-line image viewer
                    for (int i = 0; i < height; i++)
                    {
                        ::write(dumpFileFD, baseAddr, widthInBytes);
                        baseAddr += stride;
                    }
                }
                else
                {
                    // secure YUV buffer is physically continuously addressing by nature
                    ::write(dumpFileFD, baseAddr, secureMemorySize);
                }
            }

            // close session for shared memory
            ret = UREE_CloseSession(memSrvSession);
            if (ret != TZ_RESULT_SUCCESS)
                IRIS_LOGE("close mem sesion failed(%d)", ret);

            // decrease ION handle reference countin
            {
                IRIS_LOGD("ion_free +");
                std::lock_guard<std::mutex> _l(mIonDevFd.second);
                ion_free(mIonDevFd.first, ionHandle);
                IRIS_LOGD("ion_free -");
            }
        }
#endif // USING_MTK_TEE

        // synchronize a file's in-core state with storage device
        fsync(dumpFileFD);

        IRIS_LOGD("dump %s buffer -", isSecure ? "secure" : "normal");

        ::close(dumpFileFD);
}
