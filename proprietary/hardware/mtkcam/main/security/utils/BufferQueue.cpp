#define DEBUG_LOG_TAG "DBQ"

#include <ion/ion.h>
#include <ion.h>
#include <linux/mtk_ion.h>
#include <linux/ion_drv.h>

#include <unistd.h>

#include <sys/mman.h>
#include <linux/mman-proprietary.h>

#include <mtkcam/utils/std/Sync.h>
#include <mtkcam/main/security/utils/BufferQueue.h>
#include <mtkcam/main/security/utils/Debug.h>

#include <chrono>

using namespace NSCam::Utils::Sync;
using namespace NSCam::security;

// ---------------------------------------------------------------------------

// timeout if below 10 FPS
static constexpr std::chrono::duration<int64_t, std::milli>
    kDequeueBufferTimeout = std::chrono::milliseconds(500);

static const int kIONNonCachable = 0;
static const int kIONCachable = (ION_FLAG_CACHED | ION_FLAG_CACHED_NEEDS_SYNC);

// ---------------------------------------------------------------------------

static SECHAND getSecureHandle(const int ionDevFD, const ion_user_handle_t& handle)
{
    struct ion_sys_data sys_data;
    sys_data.sys_cmd = ION_SYS_GET_PHYS;
    sys_data.get_phys_param.handle = handle;
    if (ion_custom_ioctl(ionDevFD, ION_CMD_SYSTEM, &sys_data))
    {
        IRIS_LOGE("ion_custom_ioctl failed to get secure handle");
        return 0;
    }

    IRIS_LOGI("Secure memory allocated: handle(0x%x) size(%lu)",
            sys_data.get_phys_param.phy_addr, sys_data.get_phys_param.len);

    return sys_data.get_phys_param.phy_addr;
}

// TODO: encapsulates ION operations into class level
static status_t createIonBuffer(
        const int ionDevFD, std::mutex& lock,
        const bool isSecure,
        const size_t data_size, const size_t align,
        ion_user_handle_t& handle, SECHAND& sec_handle,
        IrisBufferQueue::CacheType cache_mechanism,
        int& ion_fd, void*& va)
{
    AutoLog();

    std::lock_guard<std::mutex> _l(lock);

    status_t err = NO_ERROR;

    // allocate buffer from multimedia heap
    // TODO: need cache sync
    const auto isCachable =
        (cache_mechanism == IrisBufferQueue::CacheType::CACHEABLE);
    if (ion_alloc(ionDevFD, data_size, align,
                isSecure ? ION_HEAP_MULTIMEDIA_SEC_MASK : ION_HEAP_MULTIMEDIA_MASK,
                isCachable ? kIONCachable : kIONNonCachable,
                &handle))
    {
        IRIS_LOGE("ion_alloc failed: ionDevFD(%d), BufSize(%zu) secure(%d)",
                ionDevFD, data_size, isSecure);
        return NO_INIT;
    }
    IRIS_LOGD("ion_alloc done: %s %s buffer",
            isCachable ? "cachable" : "non-cachable",
            isSecure ? "secure" : "normal");

    // obtain a file descriptor that can pass to other clients
    if (ion_share(ionDevFD, handle, &ion_fd))
    {
        IRIS_LOGE("ion_share failed: ionDevFD(%d), BufSize(%zu)," \
                " ionHandle(%d)",
                ionDevFD, data_size, handle);
        return INVALID_OPERATION;
    }

    if (isSecure == false)
    {
        // map virtual address of ION buffer
        va = ion_mmap(
                ionDevFD, nullptr, data_size,
                (PROT_READ | PROT_WRITE | PROT_NOCACHE),
                MAP_SHARED, ion_fd, 0);

        if (va != 0)
            IRIS_LOGD("ion_mmap: devFD(%d) FD(%d) va(0x%" PRIxPTR ") size(%zu)",
                    ionDevFD, ion_fd, reinterpret_cast<uintptr_t>(va), data_size);
        else
        {
            IRIS_LOGE("ion_mmap failed: invalid virutal address");
            return INVALID_OPERATION;
        }
    }
    else
    {
        // query secure handle via ION handle
        sec_handle = getSecureHandle(ionDevFD, handle);
        if (CC_UNLIKELY(sec_handle == 0))
        {
            IRIS_LOGE("get secure handle failed");
            return BAD_VALUE;
        }
    }

    return err;
}

static status_t destroyIonBuffer(
        const int ionDevFD, std::mutex& lock,
        const bool secure,
        const size_t data_size,
        ion_user_handle_t& handle, int& ion_fd, void*& va)
{
    AutoLog();

    std::lock_guard<std::mutex> _l(lock);

    status_t err = NO_ERROR;

    // clear content from allocated ION buffer and
    // unmap virtual address of ION buffer
    if (secure == false)
    {
        memset(va, 0, data_size);
        ion_munmap(ionDevFD, va, data_size);
    }

    // close share fd of ION buffer
    if (ion_fd >= 0)
    {
        if (ion_share_close(ionDevFD, ion_fd))
            IRIS_LOGE("ion_share_close failed");
        ion_fd = -1;
    }

    // free ION buffer handle
    if (ion_free(ionDevFD, handle))
        IRIS_LOGE("ion_free failed");
    handle = 0;

    return err;
}

static inline status_t handleSecureBuffer(
        bool __unused is_secure,
        ion_user_handle_t __unused hand, SECHAND* __unused sec_handle)
{
    AutoLog();
/*
    *sec_handle = 0;
    if (is_secure)
    {
        SECHAND tmp_sec_handle;
        int err = getSecureHwcBuf(hand, &tmp_sec_handle);
        if (GRALLOC_EXTRA_OK != err)
        {
            SVPLOGE("handleSecureBuffer", "Failed to allocate secure memory");
            return -EINVAL;
        }
        else
        {
            *sec_handle = tmp_sec_handle;
        }
    }
    else
    {
        freeSecureHwcBuf(hand);
    }
    */
    return NO_ERROR;
}

static void protectedCloseImpl(
        const int32_t& __unused fd, const char* __unused str,
        const int32_t& __unused line)
{
    AutoLog();
#ifdef FENCE_DEBUG
    android::CallStack stack;
    stack.update();
    IRIS_LOGW("close fd %d backtrace: %s", fd, stack.toString().string());

    IRIS_LOGW("close fd %d(%s) line(%d)", fd, str, line);
#endif
    ::close(fd);
}
#define protectedClose(fd) protectedCloseImpl(fd, __func__, __LINE__)

// ---------------------------------------------------------------------------

IrisBufferQueue::IrisBufferQueue(int maxBuffers, bool bSecurity, SecType secureType, int allocType)
    : m_is_synchronous(true)
    , m_dequeue_block(true)
    , m_frame_counter(0)
    , m_last_acquire_idx(INVALID_BUFFER_SLOT)
    , m_page_size(sysconf(_SC_PAGESIZE))
    , m_security(bSecurity)
    , m_secType(secureType)
    , m_alloc_type(allocType)
{
    AutoLog();

    m_buffer_count = maxBuffers;

    // assure no "array-index out of bounds" exception
    IRIS_LOGA_IF(m_buffer_count > NUM_BUFFER_SLOTS,
        "maximum buffer count(%d) exceeds NUM_BUFFER_SLOTS(%d)",
        m_buffer_count, NUM_BUFFER_SLOTS);

    // open ION device
    mIonDevFd.first = mt_ion_open(DEBUG_LOG_TAG);
    if (mIonDevFd.first < 0)
    {
        IRIS_LOGE("open Ion device failed!");
        mIonDevFd.first = -1;
    }
}

IrisBufferQueue::~IrisBufferQueue()
{
    AutoLog();

    for (int i = 0; i < m_buffer_count; i++)
    {
        BufferSlot* slot = &m_slots[i];

        if (0 == slot->handle) continue;

        if (slot->release_fence != -1) protectedClose(slot->release_fence);
        if (slot->acquire_fence != -1) protectedClose(slot->acquire_fence);

        if(m_alloc_type == BUFFER_HEAP_ALLOC) {
            slot->imageBufferHeap = nullptr;
        } else {
            if (NO_ERROR != destroyIonBuffer(
                        mIonDevFd.first, mIonDevFd.second, slot->secure,
                        slot->data_size, slot->handle, slot->ion_fd,
                        reinterpret_cast<void*&>(slot->va)))
            {
                IRIS_LOGE("Failed to free memory size(%zu)", slot->data_size);
            }
        }
    }

    m_producer_listener = nullptr;
    m_consumer_listener = nullptr;

    // close ION device
    {
        std::lock_guard<std::mutex> _l(mIonDevFd.second);
        if (mIonDevFd.first != -1)
        {
            ion_close(mIonDevFd.first);
            mIonDevFd.first = -1;
        }
    }
}

status_t IrisBufferQueue::cacheSync(
        const struct IrisBuffer& buffer, const SyncType type)
{
    AutoLog();

    if(m_alloc_type == BUFFER_HEAP_ALLOC) {
        sp<IImageBufferHeap> heap = buffer.buffer_heap;
        sp<IImageBuffer> imageBuffer = heap->createImageBuffer();
        // Define in eCacheCtrl
        eCacheCtrl sync_type = eCACHECTRL_FLUSH;

        if(type == SyncType::INVALIDATE) {
            sync_type = eCACHECTRL_INVALID;
        }

        imageBuffer->syncCache(sync_type);
    } else {
        // skip cache sync if non-cacheable
        {
            std::lock_guard<std::mutex> l(m_mutex);
            if (CC_UNLIKELY(m_buffer_param.cache_mechanism == CacheType::NON_CACHEABLE))
            {
                IRIS_LOGW("Non-cacheable and no need to do cache sync");
                return NO_ERROR;
            }
        }

        std::lock_guard<std::mutex> _l(mIonDevFd.second);
        if (CC_UNLIKELY(mIonDevFd.first == -1))
        {
            IRIS_LOGE("Invalid Ion device");
            return NO_INIT;
        }

        if (CC_UNLIKELY(buffer.handle == 0))
        {
            IRIS_LOGE("Invalid Ion user handle");
            return BAD_VALUE;
        }

        // cache sync by flush/invalidate all
        struct ion_sys_data sys_data;
        sys_data.sys_cmd                    = ION_SYS_CACHE_SYNC;
        sys_data.cache_sync_param.handle    = buffer.handle;
        sys_data.cache_sync_param.sync_type =
            [&type]()
            {
                if (type == SyncType::FLUSH)
                    return ION_CACHE_FLUSH_ALL;
                else
                    return ION_CACHE_INVALID_ALL;
            }();
        if (ion_custom_ioctl(mIonDevFd.first, ION_CMD_SYSTEM, &sys_data))
        {
            IRIS_LOGE("Failed to flush cache through ION: %s", strerror(errno));
            return -errno;
        }
    }

    return NO_ERROR;
}

status_t IrisBufferQueue::setBufferParam(const BufferParam&& param)
{
    AutoLog();

    std::lock_guard<std::mutex> l(m_mutex);

    m_buffer_param = std::move(param);

    return NO_ERROR;
}

status_t IrisBufferQueue::setStreamInfo(sp<IImageStreamInfo> streamInfo)
{
    AutoLog();

    std::lock_guard<std::mutex> l(m_mutex);

    m_streamInfo = streamInfo;

    return NO_ERROR;
}

status_t IrisBufferQueue::reallocateLocked(int idx, bool is_secure)
{
    BufferSlot* slot = &m_slots[idx];

    slot->data_pitch = m_buffer_param.pitch;
    slot->data_format = m_buffer_param.format;

    // allocate new buffer
    if (m_alloc_type == BUFF_ION_ALLOC && slot->handle == 0)
    {
        slot->data_size = m_buffer_param.size;
        slot->secure = is_secure;
        if (NO_ERROR != createIonBuffer(
                    mIonDevFd.first, mIonDevFd.second, slot->secure,
                    slot->data_size, m_page_size, slot->handle, slot->sec_handle,
                    m_buffer_param.cache_mechanism,
                    slot->ion_fd, reinterpret_cast<void*&>(slot->va)))
        {
            IRIS_LOGE("Failed to allocate memory size(%zu)", slot->data_size);
            slot->handle = 0;
            slot->data_size = 0;
            return -EINVAL;
        }

        IRIS_LOGD("Alloc Slot(%d): ion handle(%d) secure(%d) sec_handle(0x%x) FD(%d)",
                idx, slot->handle, slot->secure, slot->sec_handle, slot->ion_fd);
    }
    else if (m_alloc_type == BUFFER_HEAP_ALLOC && !slot->bAllocated)
    {
        slot->data_size = m_buffer_param.size;
        slot->secure = is_secure;

        IRIS_LOGD("Use ImageBufferHeap as allocator");
        if (NO_ERROR != allocateBufferHeap(idx)) {
            IRIS_LOGE("Fail to allocate memory");
            return -EINVAL;
        }
        slot->bAllocated = true;
    }
    return NO_ERROR;
}

status_t IrisBufferQueue::drainQueueLocked(std::unique_lock<std::mutex>& lock)
{
    while (m_is_synchronous && !m_queue.empty())
    {
        m_dequeue_condition.wait(lock);
    }

    return NO_ERROR;
}

status_t IrisBufferQueue::dequeueBuffer(
        IrisBuffer* buffer, bool async, bool is_secure)
{
    AutoLog();
    std::unique_lock<std::mutex> l(m_mutex);

    int found = INVALID_BUFFER_SLOT;
    bool tryAgain = true;
    while (tryAgain)
    {
        found = INVALID_BUFFER_SLOT;
        for (int i = 0; i < m_buffer_count; i++)
        {
            const int state = m_slots[i].state;
            if (state == BufferSlot::FREE)
            {
                // return the oldest of the free buffers to avoid
                // stalling the producer if possible.
                if (found < 0)
                {
                    found = i;
                }
                else if (m_slots[i].frame_num < m_slots[found].frame_num)
                {
                    found = i;
                }
            }

            IRIS_LOGD("slot %d state(%d) FD(%d) num(%llu)",
                    i, state, m_slots[i].ion_fd, m_slots[i].frame_num);
        }

        // if no buffer is found, wait for a buffer to be released
        tryAgain = (found == INVALID_BUFFER_SLOT);
        if (tryAgain)
        {
            if (CC_LIKELY(m_buffer_param.dequeue_block))
            {
                IRIS_LOGV("dequeueBuffer: wait for available buffer...");
                const std::cv_status res =
                    m_dequeue_condition.wait_for(l, kDequeueBufferTimeout);

                if (CC_LIKELY(res != std::cv_status::timeout))
                    IRIS_LOGV("dequeueBuffer: wake up to find available buffer");
                else
                    IRIS_LOGW("dequeueBuffer: cannot find available buffer");
            }
            else
            {
                IRIS_LOGE("dequeueBuffer: cannot find available buffer, exit...");
                return -EBUSY;
            }
        }
    }

    const int idx = found;

    reallocateLocked(idx, is_secure);

    // TODO: add handleSecureBuffer here

    // buffer is now in DEQUEUED state
    m_slots[idx].state = BufferSlot::DEQUEUED;

    buffer->handle           = m_slots[idx].handle;
    buffer->ion_fd           = m_slots[idx].ion_fd;
    buffer->sec_handle       = m_slots[idx].sec_handle;
    buffer->data_size        = m_slots[idx].data_size;
    buffer->data_pitch       = m_slots[idx].data_pitch;
    buffer->data_format      = m_slots[idx].data_format;
    buffer->frame_num        = m_slots[idx].frame_num;
    buffer->secure           = m_slots[idx].secure;
    buffer->release_fence    = m_slots[idx].release_fence;
    buffer->va               = m_slots[idx].va;
    buffer->buffer_heap      = m_slots[idx].imageBufferHeap;
    buffer->index            = idx;

    m_slots[idx].release_fence = -1;

    IRIS_LOGD("dequeueBuffer (idx=%d, fence=%d) (handle=%d, ion=%d)",
            idx, buffer->release_fence, buffer->handle, buffer->ion_fd);

    // wait release fence
    if (!async)
    {
        sp<IFence> release_fence(IFence::create(buffer->release_fence));
        release_fence->wait(1000);
        buffer->release_fence = -1;
    }

    return NO_ERROR;
}

status_t IrisBufferQueue::queueBuffer(
        IrisBuffer* buffer, const void* opaqueMessage)
{
    AutoLog();

    std::shared_ptr<ConsumerListener> listener;

    {
        std::lock_guard<std::mutex> l(m_mutex);

        const int idx = buffer->index;

        if (idx < 0 || idx >= m_buffer_count)
        {
            IRIS_LOGE("queueBuffer: slot index out of range [0, %d): %d",
                     m_buffer_count, idx);
            return -EINVAL;
        }
        else if (m_slots[idx].state != BufferSlot::DEQUEUED)
        {
            IRIS_LOGE("queueBuffer: slot %d is not owned by the client "
                     "(state=%d)", idx, m_slots[idx].state);
            return -EINVAL;
        }

        // if queue not empty, means consumer is slower than producer
        // * in sync mode, may cause lag (but size 1 should be OK for triple buffer)
        // * in async mode, frame drop
        //bool dump_fifo = false;
        bool dump_fifo = false;
        if (true == m_is_synchronous)
        {
            // fifo depth 1 is ok for multiple buffer, but 2 would cause lag
            if (1 < m_queue.size())
            {
                IRIS_LOGW("queued:%zu (lag)", m_queue.size());
                dump_fifo = true;
            }
        }
        else
        {
            // frame drop is fifo is not empty
            if (0 < m_queue.size())
            {
                IRIS_LOGD("queued:%zu (drop frame)", m_queue.size());
                dump_fifo = true;
            }
        }

        // dump current fifo data, and the new coming one
        if (true == dump_fifo)
        {
            // print from the oldest to the latest queued buffers
            const BufferSlot *slot = nullptr;

            for (auto i : m_queue)
            {
                slot = &(m_slots[i]);
                IRIS_LOGD(" [idx:%d] handle:%d", i, slot->handle);
            }

            IRIS_LOGD("NEW [idx:%d] handle:%d", idx, m_slots[idx].handle);
        }

        if (m_is_synchronous)
        {
            // In synchronous mode we queue all buffers in a FIFO
            m_queue.push_back(idx);

            listener = m_consumer_listener;
        }
        else
        {
            // In asynchronous mode we only keep the most recent buffer.
            if (m_queue.empty())
            {
                m_queue.push_back(idx);
            }
            else
            {
                auto front(m_queue.begin());
                // buffer currently queued is freed
                m_slots[*front].state = BufferSlot::FREE;
                // and we record the new buffer index in the queued list
                *front = idx;
            }

            listener = m_consumer_listener;
        }

        m_slots[idx].handle               = buffer->handle;
        m_slots[idx].state                = BufferSlot::QUEUED;
        m_slots[idx].frame_num            = (++m_frame_counter);
        m_slots[idx].secure               = buffer->secure;
        m_slots[idx].sequence             = buffer->sequence;
        m_slots[idx].acquire_fence        = buffer->acquire_fence;

        IRIS_LOGD("queueBuffer (idx=%d, fence=%d)",
                idx, m_slots[idx].acquire_fence);

        m_dequeue_condition.notify_all();
    }

    if (listener != nullptr) listener->onBufferQueued(opaqueMessage);

    return NO_ERROR;
}

status_t IrisBufferQueue::cancelBuffer(int index, bool isDropped)
{
    AutoLog();

    std::lock_guard<std::mutex> l(m_mutex);

    if (index == INVALID_BUFFER_SLOT) return -EINVAL;

    if (index < 0 || index >= m_buffer_count)
    {
        IRIS_LOGE("cancelBuffer: slot index out of range [0, %d]: %d",
                m_buffer_count, index);
        return -EINVAL;
    }
    else if ((m_slots[index].state != BufferSlot::DEQUEUED))
    {
        IRIS_LOGE("cancelBuffer: slot %d is not owned by the client (state=%d)",
                index, m_slots[index].state);
        return -EINVAL;
    }

    IRIS_LOGD("cancelBuffer (%d) isDropped(%d)", index, isDropped);

    m_slots[index].state = BufferSlot::FREE;
    m_slots[index].frame_num = 0;
    m_slots[index].acquire_fence = -1;
    m_slots[index].release_fence = -1;

    m_dequeue_condition.notify_all();
    return NO_ERROR;
}

status_t IrisBufferQueue::setSynchronousMode(bool enabled)
{
    std::unique_lock<std::mutex> l(m_mutex);

    if (m_is_synchronous != enabled)
    {
        // drain the queue when changing to asynchronous mode
        if (!enabled) drainQueueLocked(l);

        m_is_synchronous = enabled;
        m_dequeue_condition.notify_all();
    }

    return NO_ERROR;
}

status_t IrisBufferQueue::acquireBuffer(IrisBuffer* buffer, bool async)
{
    AutoLog();

    std::lock_guard<std::mutex> l(m_mutex);

    // check if queue is empty
    // In asynchronous mode the list is guaranteed to be one buffer deep.
    // In synchronous mode we use the oldest buffer.
    if (!m_queue.empty())
    {
        auto front(m_queue.begin());
        int idx = *front;

        // buffer is now in ACQUIRED state
        m_slots[idx].state = BufferSlot::ACQUIRED;

        buffer->handle        = m_slots[idx].handle;
        buffer->ion_fd        = m_slots[idx].ion_fd;
        buffer->sec_handle    = m_slots[idx].sec_handle;
        buffer->data_size     = m_slots[idx].data_size;
        buffer->data_pitch    = m_slots[idx].data_pitch;
        buffer->data_format   = m_slots[idx].data_format;
        buffer->frame_num     = m_slots[idx].frame_num;
        buffer->secure        = m_slots[idx].secure;
        buffer->sequence      = m_slots[idx].sequence;
        buffer->acquire_fence = m_slots[idx].acquire_fence;
        buffer->va            = m_slots[idx].va;
        buffer->buffer_heap   = m_slots[idx].imageBufferHeap;
        buffer->index         = idx;

        m_slots[idx].acquire_fence = -1;

        IRIS_LOGD("acquireBuffer (idx=%d, fence=%d, state=%d)",
                idx, buffer->acquire_fence, m_slots[idx].state);

        // remember last acquire buffer's index
        m_last_acquire_idx = idx;

        m_queue.erase(front);
        m_dequeue_condition.notify_all();
    }
    else
    {
        IRIS_LOGW("acquireBuffer failed");

        buffer->index = INVALID_BUFFER_SLOT;
        return NO_BUFFER_AVAILABLE;
    }

    // wait acquire fence
    if (!async)
    {
        sp<IFence> acquire_fence(IFence::create(buffer->acquire_fence));
        acquire_fence->wait(1000);
        buffer->acquire_fence = -1;
    }

    return NO_ERROR;
}

status_t IrisBufferQueue::releaseBuffer(int index, int fence)
{
    AutoLog();

    std::shared_ptr<ProducerListener> listener;

    {
        std::lock_guard<std::mutex> l(m_mutex);

        if (index == INVALID_BUFFER_SLOT)
        {
            IRIS_LOGE("releaseBuffer invalid_buffer_slot");
            return -EINVAL;
        }

        if (index < 0 || index >= m_buffer_count)
        {
            IRIS_LOGE("releaseBuffer: slot index out of range [0, %d]: %d",
                    m_buffer_count, index);
            return -EINVAL;
        }

        if (m_slots[index].state != BufferSlot::ACQUIRED)
        {
            IRIS_LOGE("attempted to releasebuffer(%d) with state(%d)",
                    index, m_slots[index].state);
            return -EINVAL;
        }

        m_slots[index].state = BufferSlot::FREE;
        m_slots[index].release_fence = fence;

        listener = m_producer_listener;

        IRIS_LOGD("releaseBuffer (idx=%d, fence=%d)", index, fence);

        m_dequeue_condition.notify_all();
    }

    if (listener != nullptr) listener->onBufferReleased();

    return NO_ERROR;
}

void IrisBufferQueue::setConsumerListener(
    const std::shared_ptr<ConsumerListener> & listener)
{
    AutoLog();
    std::lock_guard<std::mutex> l(m_mutex);
    m_consumer_listener = listener;
}

void IrisBufferQueue::setProducerListener(
    const std::shared_ptr<ProducerListener> & listener)
{
    AutoLog();
    std::lock_guard<std::mutex> l(m_mutex);
    m_producer_listener = listener;
}

/******************************************************************************
 *
 ******************************************************************************/
status_t
IrisBufferQueue::allocateBufferHeap(int idx)
{
    AutoLog();

    if ( m_streamInfo == 0 ) {
        IRIS_LOGE("No ImageStreamInfo.");
        return UNKNOWN_ERROR;
    }

    BufferSlot* slot = &m_slots[idx];

    if( do_construct(slot->imageBufferHeap) == NO_MEMORY ) {
            IRIS_LOGE("do_construct allocate buffer failed");
    }

    sp<IImageBuffer> imageBuffer = slot->imageBufferHeap->createImageBuffer();
    imageBuffer->lockBuf(DEBUG_LOG_TAG, eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    slot->ion_fd = imageBuffer->getFD(0);
    slot->handle = imageBuffer->getFD(0);
    slot->va     = imageBuffer->getBufVA(0);
    imageBuffer->unlockBuf(DEBUG_LOG_TAG);

    IRIS_LOGD("Allocate io_fd[%d]=(%d)", idx, slot->ion_fd);
    return NO_ERROR;
}

/******************************************************************************
 *
 ******************************************************************************/
status_t
IrisBufferQueue::do_construct(
    sp<IImageBufferHeap>& pImageBufferHeap
)
{
    IImageStreamInfo::BufPlanes_t const& bufPlanes = m_streamInfo->getBufPlanes();
    size_t bufStridesInBytes[3] = {0};
    size_t bufBoundaryInBytes[3]= {0};
    size_t bufCustomSizeInBytes[3] = {0};
    size_t bufReusableSizeInBytes[3] = {0};
    for (size_t i = 0; i < bufPlanes.size(); i++) {
        bufStridesInBytes[i] = bufPlanes[i].rowStrideInBytes;
        bufCustomSizeInBytes[i] = bufPlanes[i].sizeInBytes;
        bufReusableSizeInBytes[i] = bufPlanes[i].reusableSizeInBytes;
    }

    if ( eImgFmt_JPEG == m_streamInfo->getImgFormat() ||
         eImgFmt_BLOB == m_streamInfo->getImgFormat() )
    {
        IImageBufferAllocator::ImgParam imgParam(
                m_streamInfo->getImgSize(),
                (*bufStridesInBytes),
                (*bufBoundaryInBytes));
        imgParam.imgFormat = eImgFmt_BLOB;
        IRIS_LOGD("eImgFmt_JPEG -> eImgFmt_BLOB");
        if (!m_security) {
            pImageBufferHeap = IIonImageBufferHeap::create(
                    m_streamInfo->getStreamName(),
                    imgParam,
                    IIonImageBufferHeap::AllocExtraParam(),
                    MFALSE
                    );
        } else {
            pImageBufferHeap = ISecureImageBufferHeap::create(
                    m_streamInfo->getStreamName(),
                    imgParam,
                    ISecureImageBufferHeap::AllocExtraParam(0, 1, 0, MFALSE, m_secType),
                    MFALSE
                    );

        }
    }
    else
    {
        IImageBufferAllocator::ImgParam imgParam(
            m_streamInfo->getImgFormat(),
            m_streamInfo->getImgSize(),
            bufStridesInBytes, bufBoundaryInBytes,
            bufCustomSizeInBytes, bufReusableSizeInBytes,
            bufPlanes.size()
            );
        IRIS_LOGD("format:%x, size:(%d,%d), stride:%zu, boundary:%zu, customized size: %zu, planes:%zu",
            m_streamInfo->getImgFormat(),
            m_streamInfo->getImgSize().w, m_streamInfo->getImgSize().h,
            bufStridesInBytes[0], bufBoundaryInBytes[0],
            bufCustomSizeInBytes[0], bufReusableSizeInBytes[0],
            bufPlanes.size());
        if (!m_security) {
            pImageBufferHeap = IIonImageBufferHeap::create(
                    m_streamInfo->getStreamName(),
                    imgParam,
                    IIonImageBufferHeap::AllocExtraParam(),
                    MFALSE
                    );
        } else {
            pImageBufferHeap = ISecureImageBufferHeap::create(
                    m_streamInfo->getStreamName(),
                    imgParam,
                    ISecureImageBufferHeap::AllocExtraParam(0, 1, 0, MFALSE, m_secType),
                    MFALSE
                    );
        }
    }

    return NO_ERROR;
}
