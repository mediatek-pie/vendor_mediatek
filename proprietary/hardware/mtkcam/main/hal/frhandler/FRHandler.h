#ifndef VENDOR_MEDIATEK_HARDWARE_CAMERA_FRHANDLER_V1_0_FRHANDLER_H
#define VENDOR_MEDIATEK_HARDWARE_CAMERA_FRHANDLER_V1_0_FRHANDLER_H

#include <vendor/mediatek/hardware/camera/frhandler/1.0/IFRHandler.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>

#include <FRAlgoCA.h>

namespace vendor {
namespace mediatek {
namespace hardware {
namespace camera {
namespace frhandler {
namespace V1_0 {
namespace implementation {

using ::vendor::mediatek::hardware::camera::frhandler::FRAlgoCA;

using ::android::hardware::hidl_array;
using ::android::hardware::hidl_memory;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_vec;
using ::android::hardware::Return;
using ::android::hardware::Void;
using ::android::Mutex;
using ::android::hardware::camera::common::V1_0::Status;

using ::android::sp;


struct FRHandler : public IFRHandler {
    // Methods from IFRHandler follow.
    Return<void> getFRImageFormat(getFRImageFormat_cb _hidl_cb) override;
    Return<int32_t> init(int32_t frConfig, const sp<IFRClientCallback>& clientCallback) override;
    Return<int32_t> uninit() override;
    Return<int32_t> registerCallback(const sp<IFRClientCallback>& clientCallback) override;
    Return<int32_t> setPowerMode(int32_t powerMode) override;
    Return<int32_t> startSaveFeature(const hidl_string& userName) override;
    Return<int32_t> stopSaveFeature() override;
    Return<int32_t> deleteFeature(const hidl_string& userName) override;
    Return<int32_t> startCompareFeature(const hidl_string& userName) override;
    Return<int32_t> stopCompareFeature() override;

    /* debug usage */
    Return<int32_t> simCbRequest(int32_t cmdID, int32_t errCode, const FRCbParam &cbParam) override;

    // Methods from ::android::hidl::base::V1_0::IBase follow.

private:
       struct FRAlgoCAClientCallback :
                virtual public IFRAlgoCAClientCallback
        {
                FRAlgoCAClientCallback(const wp<FRHandler>& frHandler) : mwpFRHandler(frHandler) {}

                // Implementation of vendor::mediatek::hardware::camera::security::V1_0::ISecureCameraClientCallback
                virtual void onFRAlgoCAClientCallback(const int32_t fr_cmd_id, const int32_t errCode) override;

                wp<FRHandler> mwpFRHandler;
        };


    int mIsInited;
    int mLogLevel;

    std::vector<FRImageFormat> mVecFRImgFormat;
    sp<IFRClientCallback> mFRCliCallback;
    sp<FRAlgoCAClientCallback> mFRAlgoCACliCallback;

    FRCA_CMD_ENUM mCurrCmd;

    sp<FRAlgoCA> mpFRAlgoCA;
    hidl_string mUserName;

    mutable Mutex    mLock;

// === simluation related ===
public:
    void sim_getFRImageFormat(getFRImageFormat_cb _hidl_cb);
    Return<int32_t> sim_registerCallback(const sp<IFRClientCallback>& clientCallback);

};

struct DummyFRHandler : public IFRHandler {
    // Methods from IFRHandler follow.
    Return<void> getFRImageFormat(getFRImageFormat_cb _hidl_cb) override;
    Return<int32_t> init(int32_t frConfig, const sp<IFRClientCallback>& clientCallback) override;
    Return<int32_t> uninit() override;
    Return<int32_t> registerCallback(const sp<IFRClientCallback>& clientCallback) override;
    Return<int32_t> setPowerMode(int32_t powerMode) override;
    Return<int32_t> startSaveFeature(const hidl_string& userName) override;
    Return<int32_t> stopSaveFeature() override;
    Return<int32_t> deleteFeature(const hidl_string& userName) override;
    Return<int32_t> startCompareFeature(const hidl_string& userName) override;
    Return<int32_t> stopCompareFeature() override;

    /* debug usage */
    Return<int32_t> simCbRequest(int32_t cmdID, int32_t errCode, const FRCbParam &cbParam) override;
    // Methods from ::android::hidl::base::V1_0::IBase follow.
};

// FIXME: most likely delete, this is only for passthrough implementations
extern "C" IFRHandler* HIDL_FETCH_IFRHandler(const char* name);

}  // namespace implementation
}  // namespace V1_0
}  // namespace frhandler
}  // namespace camera
}  // namespace hardware
}  // namespace mediatek
}  // namespace vendor

#endif  // VENDOR_MEDIATEK_HARDWARE_CAMERA_FRHANDLER_V1_0_FRHANDLER_H

