#include "FRClientCallback.h"

#define LOG_TAG "FRClientCallback"

#include <cutils/log.h>
#include <mtkcam/utils/std/Log.h>

namespace vendor {
namespace mediatek {
namespace hardware {
namespace camera {
namespace frhandler {
namespace V1_0 {
namespace implementation {

// Methods from IFRClientCallback follow.
Return<void> FRClientCallback::onFRClientCallback(int32_t fr_cmd_id, int32_t errCode, const FRCbParam& param) {
    // TODO implement
    CAM_LOGI("mkdbg: %s is called", __FUNCTION__);

    return Void();
}


// Methods from ::android::hidl::base::V1_0::IBase follow.

//IFRClientCallback* HIDL_FETCH_IFRClientCallback(const char* /* name */) {
//    return new FRClientCallback();
//}

}  // namespace implementation
}  // namespace V1_0
}  // namespace frhandler
}  // namespace camera
}  // namespace hardware
}  // namespace mediatek
}  // namespace vendor
