#ifndef VENDOR_MEDIATEK_HARDWARE_CAMERA_FRHANDLER_FRALGOCA_2D_H
#define VENDOR_MEDIATEK_HARDWARE_CAMERA_FRHANDLER_FRALGOCA_2D_H

#include <utils/RefBase.h> // android::RefBase

#include "FRAlgoCA.h"

namespace vendor {
namespace mediatek {
namespace hardware {
namespace camera {
namespace frhandler {

// FR-custom
typedef struct FRAlgo2DInputCollector {
public:
    typedef enum InputCollectState_ENUM {
        FRAIC_WAIT_FOR_RAW = 0,
        FRAIC_WAIT_FOR_YUV,
        FRAIC_WAIT_FOR_ALGO_HANDLING,
        FRAIC_STOP
    } InputCollectState_ENUM;

    FRAlgo2DInputCollector();
    ~FRAlgo2DInputCollector();
    void reset();

    InputCollectState_ENUM getCurrState();
    void setCurrState(InputCollectState_ENUM state);
    int setData(InputCollectState_ENUM eState, uint64_t bufferId, const hidl_handle& hidl_buffer, const Stream& stream);
    void dumpBuffer();

private:
    std::pair<int /* phyAddr */, unsigned int/*size*/> acquireSecureInfo(InputCollectState_ENUM eState, const int shareFD);
    void releaseSecureInfo(const ion_user_handle_t& handle);
            
public:
    InputCollectState_ENUM mState;

    FRCaTaBuf mBuf;
    buffer_handle_t mImportedBuffer;
    HandleImporter mHandleImporter;

    int mLogLevel;
    
    // ion related
    int mIonDevFd;
    ion_user_handle_t mIonHandle;
} FRAlgo2DInputCollector;

class FRAlgoCA_2D: public FRAlgoCA {

private:
    // FR-custom
    struct SecureCamClientCallback : virtual public ISecureCameraClientCallback
    {
        friend FRAlgoCA_2D;
        SecureCamClientCallback(const wp<FRAlgoCA_2D>& frAlgoCA_2D, uint64_t cookie=0) : mwpFRAlgoCA_2D(frAlgoCA_2D), mKCookie(cookie) {}
        // Implementation of vendor::mediatek::hardware::camera::security::V1_0::ISecureCameraClientCallback
        virtual Return<void> onBufferAvailable(
                Status status, uint64_t bufferId,
                const hidl_handle& buffer, const Stream& stream) override;

        wp<FRAlgoCA_2D> mwpFRAlgoCA_2D;
        // 
        const uint64_t mKCookie;
    };

public:
        // FR-custom: start
        FRAlgoCA_2D(const char *pCaller, const FRCFG_ENUM frConfig);
        virtual ~FRAlgoCA_2D();
        virtual int init();
        virtual int uninit();
        virtual void reset();
        virtual ISecureCameraClientCallback* getSecureCameraClientCallback();
        virtual int fralgo_prepareFRInput(uint64_t bufferId, const hidl_handle& buffer, const Stream& stream);
        // FR-custom: end
    
private:
        
protected:
    // custom
    FRAlgo2DInputCollector mFRAlgoIC;

};


}  // namespace implementation
}  // namespace V1_0
}  // namespace frhandler
}  // namespace camera
}  // namespace hardware

#endif // VVENDOR_MEDIATEK_HARDWARE_CAMERA_FRHANDLER_FRALGOCA_2D_H





