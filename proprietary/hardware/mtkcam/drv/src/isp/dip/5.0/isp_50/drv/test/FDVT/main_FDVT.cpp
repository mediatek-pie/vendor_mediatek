#include <vector>
#include <list>
#include <stdio.h>
#include <stdlib.h>
//
#include <errno.h>
#include <fcntl.h>


//#include "isp_drv_cam.h"
#include "isp_drv_dip.h"
#include "isp_drv_dip_phy.h"
#include "isp_drv_dip_platform.h"
#include "Dip/project.h"
#include "FDVT/FDVT_LPDVT/fdvt_reg_frame01.h"
#include <imem_drv.h>

#if 0
MUINT32 IspDrvDipPhy::readReg(MUINT32 Addr,MINT32 caller)
{
    return this->m_pIspDrvImp->readReg(Addr, caller);
}
#endif

//++ for alska ldvt non-swo #include "isp_function_cam.h"
#undef LOG_TAG
#define LOG_TAG "Test_IspDrv_DIP"
#define FDVT_WHILE_ONE_TEST MTRUE

#define LOG_VRB(fmt, arg...)        printf("[%s]" fmt "\n", __func__, ##arg)
#define LOG_DBG(fmt, arg...)        printf("[%s]" fmt "\n", __func__, ##arg)
#define LOG_INF(fmt, arg...)        printf("[%s]" fmt "\n", __func__, ##arg)
#define LOG_WRN(fmt, arg...)        printf("[%s]" fmt "\n", __func__, ##arg)
#define LOG_ERR(fmt, arg...)        printf("error:[%s]" fmt "\n", __func__, ##arg)

enum {
    _DMAI_TBL_SCALE0 = 0,
    _DMAI_TBL_SCALE1,
    _DMAI_TBL_SCALE2,
    _DMAI_TBL_SCALE3,
    _DMAI_TBL_SCALE4,
    _DMAI_TBL_SCALE5,
    _DMAI_TBL_SCALE6,
    _DMAI_TBL_SCALE7,
    _DMAI_TBL_SCALE8,
    _DMAI_TBL_SCALE9,
    _DMAI_TBL_SCALE10,
    _DMAI_TBL_SCALE11,
    _DMAI_TBL_INPUTFRAME,
    _DMAI_TBL_FDCONFIG,
    _DMAI_TBL_RSCONFIG,
    _DMAI_TBL_LEARNDATA0,
    _DMAI_TBL_LEARNDATA1,
    _DMAI_TBL_LEARNDATA2,
    _DMAI_TBL_LEARNDATA3,
    _DMAI_TBL_LEARNDATA4,
    _DMAI_TBL_LEARNDATA5,
    _DMAI_TBL_LEARNDATA6,
    _DMAI_TBL_LEARNDATA7,
    _DMAI_TBL_LEARNDATA8,
    _DMAI_TBL_LEARNDATA9,
    _DMAI_TBL_LEARNDATA10,
    _DMAI_TBL_LEARNDATA11,
    _DMAI_TBL_LEARNDATA12,
    _DMAI_TBL_LEARNDATA13,
    _DMAI_TBL_LEARNDATA14,
    _DMAI_TBL_LEARNDATA15,
    _DMAI_TBL_LEARNDATA16,
    _DMAI_TBL_LEARNDATA17,
    _DMAI_TBL_FDOUT,
    _DMAI_TBL_NUM,
} ;//_DMAI_TBL_INDEX;

struct TestInputInfo {
    struct DmaiTableInfo {
        const unsigned int      *pTblAddr;
        MUINT32                 tblLength;
    };

    IMEM_BUF_INFO           ****pImemBufs;
    DmaiTableInfo           DmaiTbls[1][_DMAI_TBL_NUM];

    TestInputInfo()
        {
            int i = 0, cam = 0;

            pImemBufs = NULL;
            for (cam = 0; cam < 1; cam++) {
                for (i = 0; i < _DMAI_TBL_NUM; i++) {
                    DmaiTbls[cam][i].pTblAddr = NULL;
                    DmaiTbls[cam][i].tblLength = 0;
                }
            }
        }
};

MINT32 Pattern_Start_FDVT_1(MUINT32* _ptr, MUINT32* _ptrphy, MUINTPTR inputInfo)
{
    (void)_ptrphy;
    UINT32 i;
    IMEM_BUF_INFO**** pBuf;
    DipIMemDrv* pImemDrv = NULL;
    TestInputInfo *pInputInfo = (TestInputInfo *)inputInfo;
    PhyDipDrv* pDrvDip = (PhyDipDrv*)_ptr;

    pBuf = (IMEM_BUF_INFO****)pInputInfo->pImemBufs;

    pImemDrv = DipIMemDrv::createInstance();
    if(pImemDrv->init() < 0){
        LOG_ERR(" imem init fail\n");
        return 1;
    }

    //DMA_EN = DIP_READ_PHY_REG(DIP_HW_A, pDrvDipPhy, DIP_X_CTL_DMA_EN);
    //LOG_INF("module_DIP:  enabled DMA:0x%x\n", DMA_EN);

    LOG_INF("Start to assign DMA to HW reg\n");

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_FDCONFIG].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_FDCONFIG].tblLength) {
		pBuf[0][_DMAI_TBL_FDCONFIG][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_FDCONFIG].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_FDCONFIG][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_FDCONFIG");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_FDCONFIG][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_FDCONFIG");
			return 1;
		}

        pDrvDip->writeReg((FDVT_FD_CON_BASE_ADR - 0x15022000), pBuf[0][_DMAI_TBL_FDCONFIG][0]->phyAddr);

		LOG_INF("_DMAI_TBL_FDCONFIG srcTable=%p size=%d pa=%p , va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_FDCONFIG].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_FDCONFIG].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_FDCONFIG][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_FDCONFIG][0]->virtAddr);

	    memcpy((MUINT32*)pBuf[0][_DMAI_TBL_FDCONFIG][0]->virtAddr, (MUINT32*)pInputInfo->DmaiTbls[0][_DMAI_TBL_FDCONFIG].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_FDCONFIG].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_RSCONFIG].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_RSCONFIG].tblLength) {
		pBuf[0][_DMAI_TBL_RSCONFIG][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_RSCONFIG].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_RSCONFIG][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_RSCONFIG");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_RSCONFIG][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_RSCONFIG");
			return 1;
		}

        pDrvDip->writeReg((FDVT_RSCON_BASE_ADR - 0x15022000), pBuf[0][_DMAI_TBL_RSCONFIG][0]->phyAddr);

		LOG_INF("_DMAI_TBL_RSCONFIG srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_RSCONFIG].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_RSCONFIG].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_RSCONFIG][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_RSCONFIG].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_RSCONFIG].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA0].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA0].tblLength) {
		pBuf[0][_DMAI_TBL_LEARNDATA0][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA0].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_LEARNDATA0][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_LEARNDATA0");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_LEARNDATA0][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_LEARNDATA0");
			return 1;
		}

        pDrvDip->writeReg((FDVT_FF_BASE_ADR_0 - 0x15022000), pBuf[0][_DMAI_TBL_LEARNDATA0][0]->phyAddr);

		LOG_INF("_DMAI_TBL_LEARNDATA0 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA0].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA0].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA0][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA0][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_LEARNDATA0][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA0].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA0].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA1].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA1].tblLength) {
		pBuf[0][_DMAI_TBL_LEARNDATA1][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA1].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_LEARNDATA1][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_LEARNDATA1");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_LEARNDATA1][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_LEARNDATA1");
			return 1;
		}

        pDrvDip->writeReg((FDVT_FF_BASE_ADR_1 - 0x15022000), pBuf[0][_DMAI_TBL_LEARNDATA1][0]->phyAddr);

		LOG_INF("_DMAI_TBL_LEARNDATA1 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA1].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA1].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA1][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA1][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_LEARNDATA1][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA1].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA1].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA2].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA2].tblLength) {
		pBuf[0][_DMAI_TBL_LEARNDATA2][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA2].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_LEARNDATA2][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_LEARNDATA2");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_LEARNDATA2][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_LEARNDATA2");
			return 1;
		}

        pDrvDip->writeReg((FDVT_FF_BASE_ADR_2 - 0x15022000), pBuf[0][_DMAI_TBL_LEARNDATA2][0]->phyAddr);

		LOG_INF("_DMAI_TBL_LEARNDATA2 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA2].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA2].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA2][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA2][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_LEARNDATA2][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA2].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA2].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA3].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA3].tblLength) {
		pBuf[0][_DMAI_TBL_LEARNDATA3][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA3].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_LEARNDATA3][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_LEARNDATA3");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_LEARNDATA3][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_LEARNDATA3");
			return 1;
		}

        pDrvDip->writeReg((FDVT_FF_BASE_ADR_3 - 0x15022000), pBuf[0][_DMAI_TBL_LEARNDATA3][0]->phyAddr);

		LOG_INF("_DMAI_TBL_LEARNDATA3 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA3].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA3].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA3][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA3][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_LEARNDATA3][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA3].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA3].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA4].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA4].tblLength) {
		pBuf[0][_DMAI_TBL_LEARNDATA4][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA4].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_LEARNDATA4][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_LEARNDATA4");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_LEARNDATA4][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_LEARNDATA4");
			return 1;
		}

        pDrvDip->writeReg((FDVT_FF_BASE_ADR_4 - 0x15022000), pBuf[0][_DMAI_TBL_LEARNDATA4][0]->phyAddr);

		LOG_INF("_DMAI_TBL_LEARNDATA4 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA4].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA4].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA4][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA4][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_LEARNDATA4][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA4].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA4].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA5].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA5].tblLength) {
		pBuf[0][_DMAI_TBL_LEARNDATA5][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA5].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_LEARNDATA5][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_LEARNDATA5");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_LEARNDATA5][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_LEARNDATA5");
			return 1;
		}

        pDrvDip->writeReg((FDVT_FF_BASE_ADR_5 - 0x15022000), pBuf[0][_DMAI_TBL_LEARNDATA5][0]->phyAddr);

		LOG_INF("_DMAI_TBL_LEARNDATA5 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA5].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA5].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA5][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA5][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_LEARNDATA5][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA5].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA5].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA6].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA6].tblLength) {
		pBuf[0][_DMAI_TBL_LEARNDATA6][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA6].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_LEARNDATA6][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_LEARNDATA6");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_LEARNDATA6][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_LEARNDATA6");
			return 1;
		}

        pDrvDip->writeReg((FDVT_FF_BASE_ADR_6 - 0x15022000), pBuf[0][_DMAI_TBL_LEARNDATA6][0]->phyAddr);

		LOG_INF("_DMAI_TBL_LEARNDATA6 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA6].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA6].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA6][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA6][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_LEARNDATA6][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA6].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA6].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA7].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA7].tblLength) {
		pBuf[0][_DMAI_TBL_LEARNDATA7][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA7].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_LEARNDATA7][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_LEARNDATA7");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_LEARNDATA7][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_LEARNDATA7");
			return 1;
		}

        pDrvDip->writeReg((FDVT_FF_BASE_ADR_7 - 0x15022000), pBuf[0][_DMAI_TBL_LEARNDATA7][0]->phyAddr);

		LOG_INF("_DMAI_TBL_LEARNDATA7 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA7].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA7].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA7][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA7][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_LEARNDATA7][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA7].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA7].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA8].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA8].tblLength) {
		pBuf[0][_DMAI_TBL_LEARNDATA8][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA8].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_LEARNDATA8][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_LEARNDATA8");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_LEARNDATA8][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_LEARNDATA8");
			return 1;
		}

        pDrvDip->writeReg((FDVT_FF_BASE_ADR_8 - 0x15022000), pBuf[0][_DMAI_TBL_LEARNDATA8][0]->phyAddr);

		LOG_INF("_DMAI_TBL_LEARNDATA8 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA8].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA8].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA8][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA8][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_LEARNDATA8][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA8].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA8].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA9].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA9].tblLength) {
		pBuf[0][_DMAI_TBL_LEARNDATA9][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA9].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_LEARNDATA9][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_LEARNDATA9");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_LEARNDATA9][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_LEARNDATA9");
			return 1;
		}

        pDrvDip->writeReg((FDVT_FF_BASE_ADR_9 - 0x15022000), pBuf[0][_DMAI_TBL_LEARNDATA9][0]->phyAddr);

		LOG_INF("_DMAI_TBL_LEARNDATA9 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA9].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA9].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA9][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA9][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_LEARNDATA9][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA9].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA9].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA10].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA10].tblLength) {
		pBuf[0][_DMAI_TBL_LEARNDATA10][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA10].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_LEARNDATA10][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_LEARNDATA10");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_LEARNDATA10][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_LEARNDATA10");
			return 1;
		}

        pDrvDip->writeReg((FDVT_FF_BASE_ADR_10 - 0x15022000), pBuf[0][_DMAI_TBL_LEARNDATA10][0]->phyAddr);

		LOG_INF("_DMAI_TBL_LEARNDATA10 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA10].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA10].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA10][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA10][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_LEARNDATA10][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA10].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA10].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA11].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA11].tblLength) {
		pBuf[0][_DMAI_TBL_LEARNDATA11][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA11].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_LEARNDATA11][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_LEARNDATA11");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_LEARNDATA11][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_LEARNDATA11");
			return 1;
		}

        pDrvDip->writeReg((FDVT_FF_BASE_ADR_11 - 0x15022000), pBuf[0][_DMAI_TBL_LEARNDATA11][0]->phyAddr);

		LOG_INF("_DMAI_TBL_LEARNDATA11 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA11].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA11].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA11][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA11][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_LEARNDATA11][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA11].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA11].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA12].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA12].tblLength) {
		pBuf[0][_DMAI_TBL_LEARNDATA12][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA12].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_LEARNDATA12][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_LEARNDATA12");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_LEARNDATA12][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_LEARNDATA12");
			return 1;
		}

        pDrvDip->writeReg((FDVT_FF_BASE_ADR_12 - 0x15022000), pBuf[0][_DMAI_TBL_LEARNDATA12][0]->phyAddr);

		LOG_INF("_DMAI_TBL_LEARNDATA12 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA12].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA12].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA12][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA12][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_LEARNDATA12][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA12].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA12].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA13].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA13].tblLength) {
		pBuf[0][_DMAI_TBL_LEARNDATA13][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA13].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_LEARNDATA13][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_LEARNDATA13");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_LEARNDATA13][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_LEARNDATA13");
			return 1;
		}

        pDrvDip->writeReg((FDVT_FF_BASE_ADR_13 - 0x15022000), pBuf[0][_DMAI_TBL_LEARNDATA13][0]->phyAddr);

		LOG_INF("_DMAI_TBL_LEARNDATA13 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA13].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA13].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA13][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA13][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_LEARNDATA13][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA13].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA13].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA14].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA14].tblLength) {
		pBuf[0][_DMAI_TBL_LEARNDATA14][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA14].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_LEARNDATA14][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_LEARNDATA14");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_LEARNDATA14][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_LEARNDATA14");
			return 1;
		}

        pDrvDip->writeReg((FDVT_FF_BASE_ADR_14 - 0x15022000), pBuf[0][_DMAI_TBL_LEARNDATA14][0]->phyAddr);

		LOG_INF("_DMAI_TBL_LEARNDATA14 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA14].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA14].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA14][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA14][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_LEARNDATA14][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA14].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA14].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA15].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA15].tblLength) {
		pBuf[0][_DMAI_TBL_LEARNDATA15][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA15].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_LEARNDATA15][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_LEARNDATA15");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_LEARNDATA15][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_LEARNDATA15");
			return 1;
		}

        pDrvDip->writeReg((FDVT_FF_BASE_ADR_15 - 0x15022000), pBuf[0][_DMAI_TBL_LEARNDATA15][0]->phyAddr);

		LOG_INF("_DMAI_TBL_LEARNDATA15 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA15].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA15].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA15][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA15][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_LEARNDATA15][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA15].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA15].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA16].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA16].tblLength) {
		pBuf[0][_DMAI_TBL_LEARNDATA16][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA16].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_LEARNDATA16][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_LEARNDATA16");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_LEARNDATA16][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_LEARNDATA16");
			return 1;
		}

        pDrvDip->writeReg((FDVT_FF_BASE_ADR_16 - 0x15022000), pBuf[0][_DMAI_TBL_LEARNDATA16][0]->phyAddr);

		LOG_INF("_DMAI_TBL_LEARNDATA16 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA16].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA16].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA16][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA16][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_LEARNDATA16][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA16].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA16].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA17].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA17].tblLength) {
		pBuf[0][_DMAI_TBL_LEARNDATA17][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA17].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_LEARNDATA17][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_LEARNDATA17");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_LEARNDATA17][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_LEARNDATA17");
			return 1;
		}

        pDrvDip->writeReg((FDVT_FF_BASE_ADR_17 - 0x15022000), pBuf[0][_DMAI_TBL_LEARNDATA17][0]->phyAddr);

		LOG_INF("_DMAI_TBL_LEARNDATA17 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA17].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA17].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA17][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_LEARNDATA17][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_LEARNDATA17][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA17].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA17].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_INPUTFRAME].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_INPUTFRAME].tblLength) {
		pBuf[0][_DMAI_TBL_INPUTFRAME][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_INPUTFRAME].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_INPUTFRAME][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_INPUTFRAME");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_INPUTFRAME][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_INPUTFRAME");
			return 1;
		}

		LOG_INF("_DMAI_TBL_INPUTFRAME srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_INPUTFRAME].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_INPUTFRAME].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_INPUTFRAME][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_INPUTFRAME][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_INPUTFRAME][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_INPUTFRAME].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_INPUTFRAME].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE0].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE0].tblLength) {
		pBuf[0][_DMAI_TBL_SCALE0][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE0].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_SCALE0][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_SCALE0");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_SCALE0][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_SCALE0");
			return 1;
		}

		LOG_INF("_DMAI_TBL_SCALE0 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE0].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE0].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_SCALE0][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_SCALE0][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_SCALE0][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE0].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE0].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE1].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE1].tblLength) {
		pBuf[0][_DMAI_TBL_SCALE1][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE1].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_SCALE1][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_SCALE1");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_SCALE1][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_SCALE1");
			return 1;
		}

		LOG_INF("_DMAI_TBL_SCALE1 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE1].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE1].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_SCALE1][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_SCALE1][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_SCALE1][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE1].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE1].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE2].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE2].tblLength) {
		pBuf[0][_DMAI_TBL_SCALE2][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE2].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_SCALE2][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_SCALE2");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_SCALE2][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_SCALE2");
			return 1;
		}

		LOG_INF("_DMAI_TBL_SCALE2 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE2].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE2].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_SCALE2][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_SCALE2][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_SCALE2][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE2].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE2].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE3].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE3].tblLength) {
		pBuf[0][_DMAI_TBL_SCALE3][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE3].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_SCALE3][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_SCALE3");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_SCALE3][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_SCALE3");
			return 1;
		}

		LOG_INF("_DMAI_TBL_SCALE3 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE3].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE3].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_SCALE3][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_SCALE3][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_SCALE3][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE3].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE3].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE4].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE4].tblLength) {
		pBuf[0][_DMAI_TBL_SCALE4][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE4].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_SCALE4][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_SCALE4");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_SCALE4][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_SCALE4");
			return 1;
		}

		LOG_INF("_DMAI_TBL_SCALE4 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE4].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE4].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_SCALE4][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_SCALE4][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_SCALE4][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE4].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE4].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE5].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE5].tblLength) {
		pBuf[0][_DMAI_TBL_SCALE5][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE5].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_SCALE5][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_SCALE5");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_SCALE5][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_SCALE5");
			return 1;
		}

		LOG_INF("_DMAI_TBL_SCALE5 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE5].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE5].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_SCALE5][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_SCALE5][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_SCALE5][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE5].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE5].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE6].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE6].tblLength) {
		pBuf[0][_DMAI_TBL_SCALE6][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE6].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_SCALE6][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_SCALE6");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_SCALE6][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_SCALE6");
			return 1;
		}

		LOG_INF("_DMAI_TBL_SCALE6 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE6].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE6].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_SCALE6][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_SCALE6][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_SCALE6][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE6].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE6].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE7].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE7].tblLength) {
		pBuf[0][_DMAI_TBL_SCALE7][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE7].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_SCALE7][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_SCALE7");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_SCALE7][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_SCALE7");
			return 1;
		}

		LOG_INF("_DMAI_TBL_SCALE7 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE7].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE7].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_SCALE7][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_SCALE7][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_SCALE7][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE7].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE7].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE8].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE8].tblLength) {
		pBuf[0][_DMAI_TBL_SCALE8][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE8].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_SCALE8][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_SCALE8");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_SCALE8][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_SCALE8");
			return 1;
		}

		LOG_INF("_DMAI_TBL_SCALE8 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE8].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE8].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_SCALE8][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_SCALE8][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_SCALE8][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE8].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE8].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE9].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE9].tblLength) {
		pBuf[0][_DMAI_TBL_SCALE9][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE9].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_SCALE9][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_SCALE9");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_SCALE9][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_SCALE9");
			return 1;
		}

		LOG_INF("_DMAI_TBL_SCALE9 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE9].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE9].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_SCALE9][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_SCALE9][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_SCALE9][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE9].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE9].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE10].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE10].tblLength) {
		pBuf[0][_DMAI_TBL_SCALE10][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE10].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_SCALE10][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_SCALE10");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_SCALE10][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_SCALE10");
			return 1;
		}

		LOG_INF("_DMAI_TBL_SCALE10 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE10].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE10].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_SCALE10][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_SCALE10][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_SCALE10][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE10].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE10].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE11].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE11].tblLength) {
		pBuf[0][_DMAI_TBL_SCALE11][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE11].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_SCALE11][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_SCALE11");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_SCALE11][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_SCALE11");
			return 1;
		}

		LOG_INF("_DMAI_TBL_SCALE11 srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE11].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE11].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_SCALE11][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_SCALE11][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_SCALE11][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE11].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE11].tblLength);
	}

	if (pInputInfo->DmaiTbls[0][_DMAI_TBL_FDOUT].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_FDOUT].tblLength) {
		pBuf[0][_DMAI_TBL_FDOUT][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_FDOUT].tblLength;

		if (pImemDrv->allocVirtBuf(pBuf[0][_DMAI_TBL_FDOUT][0]) < 0) {
			LOG_ERR(" imem alloc fail at %s\n", "_DMAI_TBL_FDOUT");
			return 1;
	    }
		if (pImemDrv->mapPhyAddr(pBuf[0][_DMAI_TBL_FDOUT][0]) < 0) {
			LOG_ERR(" imem map fail at %s\n", "_DMAI_TBL_FDOUT");
			return 1;
		}

        pDrvDip->writeReg((FDVT_FD_RLT_BASE_ADR - 0x15022000), pBuf[0][_DMAI_TBL_FDOUT][0]->phyAddr);

		LOG_INF("_DMAI_TBL_FDOUT srcTable=%p size=%d pa=%p, va=%p\n",
				pInputInfo->DmaiTbls[0][_DMAI_TBL_FDOUT].pTblAddr,
				pInputInfo->DmaiTbls[0][_DMAI_TBL_FDOUT].tblLength,
				(MUINT32 *)pBuf[0][_DMAI_TBL_FDOUT][0]->phyAddr, (MUINT32 *)pBuf[0][_DMAI_TBL_FDOUT][0]->virtAddr);

	    memcpy((MUINT8*)pBuf[0][_DMAI_TBL_FDOUT][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_FDOUT].pTblAddr,
					   pInputInfo->DmaiTbls[0][_DMAI_TBL_FDOUT].tblLength);
	}

    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr)) = pBuf[0][_DMAI_TBL_INPUTFRAME][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + (8 * _DMAI_TBL_SCALE0 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE0][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + 8 * (_DMAI_TBL_SCALE0 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE0][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + (8 * _DMAI_TBL_SCALE1 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE1][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + 8 * (_DMAI_TBL_SCALE1 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE1][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + (8 * _DMAI_TBL_SCALE2 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE2][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + 8 * (_DMAI_TBL_SCALE2 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE2][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + (8 * _DMAI_TBL_SCALE3 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE3][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + 8 * (_DMAI_TBL_SCALE3 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE3][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + (8 * _DMAI_TBL_SCALE4 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE4][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + 8 * (_DMAI_TBL_SCALE4 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE4][0]->phyAddr;    
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + (8 * _DMAI_TBL_SCALE5 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE5][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + 8 * (_DMAI_TBL_SCALE5 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE5][0]->phyAddr; 
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + (8 * _DMAI_TBL_SCALE6 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE6][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + 8 * (_DMAI_TBL_SCALE6 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE6][0]->phyAddr; 
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + (8 * _DMAI_TBL_SCALE7 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE7][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + 8 * (_DMAI_TBL_SCALE7 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE7][0]->phyAddr; 
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + (8 * _DMAI_TBL_SCALE8 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE8][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + 8 * (_DMAI_TBL_SCALE8 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE8][0]->phyAddr; 
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + (8 * _DMAI_TBL_SCALE9 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE9][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + 8 * (_DMAI_TBL_SCALE9 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE9][0]->phyAddr; 
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + (8 * _DMAI_TBL_SCALE10 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE10][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + 8 * (_DMAI_TBL_SCALE10 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE10][0]->phyAddr; 
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr + (8 * _DMAI_TBL_SCALE11 + 1) * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE11][0]->phyAddr;

    *((MUINT32 *)(pBuf[0][_DMAI_TBL_FDCONFIG][0]->virtAddr + 4 * _DMAI_TBL_SCALE0 * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE0][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_FDCONFIG][0]->virtAddr + 4 * _DMAI_TBL_SCALE1 * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE1][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_FDCONFIG][0]->virtAddr + 4 * _DMAI_TBL_SCALE2 * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE2][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_FDCONFIG][0]->virtAddr + 4 * _DMAI_TBL_SCALE3 * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE3][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_FDCONFIG][0]->virtAddr + 4 * _DMAI_TBL_SCALE4 * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE4][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_FDCONFIG][0]->virtAddr + 4 * _DMAI_TBL_SCALE5 * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE5][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_FDCONFIG][0]->virtAddr + 4 * _DMAI_TBL_SCALE6 * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE6][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_FDCONFIG][0]->virtAddr + 4 * _DMAI_TBL_SCALE7 * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE7][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_FDCONFIG][0]->virtAddr + 4 * _DMAI_TBL_SCALE8 * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE8][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_FDCONFIG][0]->virtAddr + 4 * _DMAI_TBL_SCALE9 * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE9][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_FDCONFIG][0]->virtAddr + 4 * _DMAI_TBL_SCALE10 * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE10][0]->phyAddr;
    *((MUINT32 *)(pBuf[0][_DMAI_TBL_FDCONFIG][0]->virtAddr + 4 * _DMAI_TBL_SCALE11 * sizeof(MUINT32))) = pBuf[0][_DMAI_TBL_SCALE11][0]->phyAddr;

    LOG_INF("Dump fdconfig in DRAM\n");
    for (i = 0; i < 12; i++)
    {
        LOG_INF("0x%08x 0x%08x 0x%08x 0x%08x\n", *((MUINT32 *)pBuf[0][_DMAI_TBL_FDCONFIG][0]->virtAddr+4*i), *((MUINT32 *)pBuf[0][_DMAI_TBL_FDCONFIG][0]->virtAddr+4*i+1), *((MUINT32 *)pBuf[0][_DMAI_TBL_FDCONFIG][0]->virtAddr+4*i+2), *((MUINT32 *)pBuf[0][_DMAI_TBL_FDCONFIG][0]->virtAddr+4*i+3));
    }
    LOG_INF("Dump rsconfig in DRAM\n");
    for (i = 0; i < 12; i++)
    {
        LOG_INF("0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x\n", *((MUINT32 *)pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr+8*i), *((MUINT32 *)pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr+8*i+1), *((MUINT32 *)pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr+8*i+2), *((MUINT32 *)pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr+8*i+3), *((MUINT32 *)pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr+8*i+4), *((MUINT32 *)pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr+8*i+5), *((MUINT32 *)pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr+8*i+6), *((MUINT32 *)pBuf[0][_DMAI_TBL_RSCONFIG][0]->virtAddr+8*i+7));
    }

    for (i = 0; i < sizeof(fdvt_reg_addr_frame01)/4; i++)
    {
        LOG_INF("FDVT_REG[0x%x] = 0x%x", fdvt_reg_addr_frame01[i], pDrvDip->readReg((fdvt_reg_addr_frame01[i] + FDVT_BASE - DIP1_BASE)));
    }

    LOG_INF("Ready to trigger FDVT...!!");
    getchar();

    LOG_INF("Enable FDVT...!!");
    pDrvDip->writeReg((FDVT_START - 0x15022000), 0x1);

    //LOG_INF("Enter Wait FDVT Done ...!!\n");
    //LOG_INF("press any key to wait for FDVT Done...\n");
    //getchar();

    return 0;
}

MINT32 Pattern_Stop_FDVT_1(MUINT32* _ptr, MUINT32* _ptrphy, MUINTPTR para)
{
    (void)_ptrphy;
    (void)para;
    PhyDipDrv* pDrvDip = (PhyDipDrv*)_ptr;

#if 1
    MUINT32 reg, reg2, times = 0;


#else
    ISP_WAIT_IRQ_ST irq;
    _uni;linkpath;
    irq.Clear = ISP_IRQ_CLEAR_WAIT;
    irq.UserKey = 0;
    irq.St_type = SIGNAL_INT;
    irq.Status = SW_PASS1_DON_ST;
    irq.Timeout = 3000;

    if(((ISP_DRV_CAM**)_ptr)[CAM_A]){
        LOG_INF("start wait cam_a sw p1 done\n");
        if(((ISP_DRV_CAM**)_ptr)[CAM_A]->waitIrq(&irq) == MFALSE){
            LOG_ERR(" wait CAM_A p1 done fail\n");
        }
    }
    irq.Timeout = 3000;
    if(((ISP_DRV_CAM**)_ptr)[CAM_B]){
        LOG_INF("start wait cam_b sw p1 done\n");
        if(((ISP_DRV_CAM**)_ptr)[CAM_B]->waitIrq(&irq) == MFALSE){
            LOG_ERR(" wait CAM_B p1 done fail\n");
        }
    }
#endif

#if FDVT_WHILE_ONE_TEST
    while (1)
    {
#endif
        do
        {
            reg = pDrvDip->readReg(FDVT_INT - 0x15022000);
            reg2 = pDrvDip->readReg(FDVT_INT_EN - 0x15022000);
            //LOG_INF("FDVT Done ? FDVT_INT = 0x%x, FDVT_INT_EN = 0x%x", reg, reg2);
        } while(reg == 0x0);
        LOG_INF("FDVT Done, run times = %d", ++times);
        pDrvDip->writeReg((FDVT_BASE - 0x15022000), 0x20000);
        while((pDrvDip->readReg(FDVT_BASE - 0x15022000)>>17) & 0x1);
        pDrvDip->writeReg((FDVT_BASE - 0x15022000), 0x10000);
        pDrvDip->writeReg((FDVT_BASE - 0x15022000), 0x00000);

#if FDVT_WHILE_ONE_TEST
        LOG_INF("Enable FDVT");
        pDrvDip->writeReg((FDVT_START - 0x15022000), 0x1);
    }
#endif

    return 0;
}

MINT32 Pattern_Loading_FDVT_1(MUINT32* _ptr, MUINT32* _ptrphy, MUINTPTR inputInfo)
{
    (void)_ptrphy;
    PhyDipDrv*    pDrvDip = (PhyDipDrv*)_ptr;
    TestInputInfo *pInputInfo = (TestInputInfo *)inputInfo;

    static const unsigned int pattern_inputframe_tbl[] = {
        #include "FDVT/FDVT_LPDVT/fdvt_in_frame01.h"
    };

    static const unsigned int pattern_fdconfig_tbl[] = {
        #include "FDVT/FDVT_LPDVT/fd_cofi_frame01.h"
    };

    static const unsigned int pattern_rsconfig_tbl[] = {
        #include "FDVT/FDVT_LPDVT/rs_confi_frame01.h"
    };

    static const unsigned int pattern_learndata0_tbl[] = {
        #include "FDVT/FDVT_LPDVT/learning_frame01_00.h"
    };

    static const unsigned int pattern_learndata1_tbl[] = {
        #include "FDVT/FDVT_LPDVT/learning_frame01_01.h"
    };

    static const unsigned int pattern_learndata2_tbl[] = {
        #include "FDVT/FDVT_LPDVT/learning_frame01_02.h"
    };

    static const unsigned int pattern_learndata3_tbl[] = {
        #include "FDVT/FDVT_LPDVT/learning_frame01_03.h"
    };

    static const unsigned int pattern_learndata4_tbl[] = {
        #include "FDVT/FDVT_LPDVT/learning_frame01_04.h"
    };

    static const unsigned int pattern_learndata5_tbl[] = {
        #include "FDVT/FDVT_LPDVT/learning_frame01_05.h"
    };

    static const unsigned int pattern_learndata6_tbl[] = {
        #include "FDVT/FDVT_LPDVT/learning_frame01_06.h"
    };

    static const unsigned int pattern_learndata7_tbl[] = {
        #include "FDVT/FDVT_LPDVT/learning_frame01_07.h"
    };

    static const unsigned int pattern_learndata8_tbl[] = {
        #include "FDVT/FDVT_LPDVT/learning_frame01_08.h"
    };

    static const unsigned int pattern_learndata9_tbl[] = {
        #include "FDVT/FDVT_LPDVT/learning_frame01_09.h"
    };

    static const unsigned int pattern_learndata10_tbl[] = {
        #include "FDVT/FDVT_LPDVT/learning_frame01_10.h"
    };

    static const unsigned int pattern_learndata11_tbl[] = {
        #include "FDVT/FDVT_LPDVT/learning_frame01_11.h"
    };

    static const unsigned int pattern_learndata12_tbl[] = {
        #include "FDVT/FDVT_LPDVT/learning_frame01_12.h"
    };

    static const unsigned int pattern_learndata13_tbl[] = {
        #include "FDVT/FDVT_LPDVT/learning_frame01_13.h"
    };

    static const unsigned int pattern_learndata14_tbl[] = {
        #include "FDVT/FDVT_LPDVT/learning_frame01_14.h"
    };

    static const unsigned int pattern_learndata15_tbl[] = {
        #include "FDVT/FDVT_LPDVT/learning_frame01_15.h"
    };

    static const unsigned int pattern_learndata16_tbl[] = {
        #include "FDVT/FDVT_LPDVT/learning_frame01_16.h"
    };

    static const unsigned int pattern_learndata17_tbl[] = {
        #include "FDVT/FDVT_LPDVT/learning_frame01_17.h"
    };    

    static const unsigned int pattern_scale0_tbl[] = {
        #include "FDVT/FDVT_LPDVT/rs_out_frame01_scale00.h"
    }; 

    static const unsigned int pattern_scale1_tbl[] = {
        #include "FDVT/FDVT_LPDVT/rs_out_frame01_scale01.h"
    };     

    static const unsigned int pattern_scale2_tbl[] = {
        #include "FDVT/FDVT_LPDVT/rs_out_frame01_scale02.h"
    };

    static const unsigned int pattern_scale3_tbl[] = {
        #include "FDVT/FDVT_LPDVT/rs_out_frame01_scale03.h"
    };

    static const unsigned int pattern_scale4_tbl[] = {
        #include "FDVT/FDVT_LPDVT/rs_out_frame01_scale04.h"
    };

    static const unsigned int pattern_scale5_tbl[] = {
        #include "FDVT/FDVT_LPDVT/rs_out_frame01_scale05.h"
    };

    static const unsigned int pattern_scale6_tbl[] = {
        #include "FDVT/FDVT_LPDVT/rs_out_frame01_scale06.h"
    };

    static const unsigned int pattern_scale7_tbl[] = {
        #include "FDVT/FDVT_LPDVT/rs_out_frame01_scale07.h"
    };

    static const unsigned int pattern_scale8_tbl[] = {
        #include "FDVT/FDVT_LPDVT/rs_out_frame01_scale08.h"
    };

    static const unsigned int pattern_scale9_tbl[] = {
        #include "FDVT/FDVT_LPDVT/rs_out_frame01_scale09.h"
    };

    static const unsigned int pattern_scale10_tbl[] = {
        #include "FDVT/FDVT_LPDVT/rs_out_frame01_scale10.h"
    };

    static const unsigned int pattern_scale11_tbl[] = {
        #include "FDVT/FDVT_LPDVT/rs_out_frame01_scale11.h"
    };

    static const unsigned int pattern_fdout_tbl[] = {
        #include "FDVT/FDVT_LPDVT/fd_out_frame01.h"
    };
    
    /* save dmai buffer location, for latter memory allocation and loading */
    pInputInfo->DmaiTbls[0][_DMAI_TBL_INPUTFRAME].pTblAddr = pattern_inputframe_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_INPUTFRAME].tblLength = sizeof(pattern_inputframe_tbl)/sizeof(pattern_inputframe_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_FDCONFIG].pTblAddr = pattern_fdconfig_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_FDCONFIG].tblLength = sizeof(pattern_fdconfig_tbl)/sizeof(pattern_fdconfig_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_RSCONFIG].pTblAddr = pattern_rsconfig_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_RSCONFIG].tblLength = sizeof(pattern_rsconfig_tbl)/sizeof(pattern_rsconfig_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA0].pTblAddr = pattern_learndata0_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA0].tblLength = sizeof(pattern_learndata0_tbl)/sizeof(pattern_learndata0_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA1].pTblAddr = pattern_learndata1_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA1].tblLength = sizeof(pattern_learndata1_tbl)/sizeof(pattern_learndata1_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA2].pTblAddr = pattern_learndata2_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA2].tblLength = sizeof(pattern_learndata2_tbl)/sizeof(pattern_learndata2_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA3].pTblAddr = pattern_learndata3_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA3].tblLength = sizeof(pattern_learndata3_tbl)/sizeof(pattern_learndata3_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA4].pTblAddr = pattern_learndata4_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA4].tblLength = sizeof(pattern_learndata4_tbl)/sizeof(pattern_learndata4_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA5].pTblAddr = pattern_learndata5_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA5].tblLength = sizeof(pattern_learndata5_tbl)/sizeof(pattern_learndata5_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA6].pTblAddr = pattern_learndata6_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA6].tblLength = sizeof(pattern_learndata6_tbl)/sizeof(pattern_learndata6_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA7].pTblAddr = pattern_learndata7_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA7].tblLength = sizeof(pattern_learndata7_tbl)/sizeof(pattern_learndata7_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA8].pTblAddr = pattern_learndata8_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA8].tblLength = sizeof(pattern_learndata8_tbl)/sizeof(pattern_learndata8_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA9].pTblAddr = pattern_learndata9_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA9].tblLength = sizeof(pattern_learndata9_tbl)/sizeof(pattern_learndata9_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA10].pTblAddr = pattern_learndata10_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA10].tblLength = sizeof(pattern_learndata10_tbl)/sizeof(pattern_learndata10_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA11].pTblAddr = pattern_learndata11_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA11].tblLength = sizeof(pattern_learndata11_tbl)/sizeof(pattern_learndata11_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA12].pTblAddr = pattern_learndata12_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA12].tblLength = sizeof(pattern_learndata12_tbl)/sizeof(pattern_learndata12_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA13].pTblAddr = pattern_learndata13_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA13].tblLength = sizeof(pattern_learndata13_tbl)/sizeof(pattern_learndata13_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA14].pTblAddr = pattern_learndata14_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA14].tblLength = sizeof(pattern_learndata14_tbl)/sizeof(pattern_learndata14_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA15].pTblAddr = pattern_learndata15_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA15].tblLength = sizeof(pattern_learndata15_tbl)/sizeof(pattern_learndata15_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA16].pTblAddr = pattern_learndata16_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA16].tblLength = sizeof(pattern_learndata16_tbl)/sizeof(pattern_learndata16_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA17].pTblAddr = pattern_learndata17_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_LEARNDATA17].tblLength = sizeof(pattern_learndata17_tbl)/sizeof(pattern_learndata17_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE0].pTblAddr = pattern_scale0_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE0].tblLength = sizeof(pattern_scale0_tbl)/sizeof(pattern_scale0_tbl[0])*4;    

    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE1].pTblAddr = pattern_scale1_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE1].tblLength = sizeof(pattern_scale1_tbl)/sizeof(pattern_scale1_tbl[0])*4; 

    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE2].pTblAddr = pattern_scale2_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE2].tblLength = sizeof(pattern_scale2_tbl)/sizeof(pattern_scale2_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE3].pTblAddr = pattern_scale3_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE3].tblLength = sizeof(pattern_scale3_tbl)/sizeof(pattern_scale3_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE4].pTblAddr = pattern_scale4_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE4].tblLength = sizeof(pattern_scale4_tbl)/sizeof(pattern_scale4_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE5].pTblAddr = pattern_scale5_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE5].tblLength = sizeof(pattern_scale5_tbl)/sizeof(pattern_scale5_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE6].pTblAddr = pattern_scale6_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE6].tblLength = sizeof(pattern_scale6_tbl)/sizeof(pattern_scale6_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE7].pTblAddr = pattern_scale7_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE7].tblLength = sizeof(pattern_scale7_tbl)/sizeof(pattern_scale7_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE8].pTblAddr = pattern_scale8_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE8].tblLength = sizeof(pattern_scale8_tbl)/sizeof(pattern_scale8_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE9].pTblAddr = pattern_scale9_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE9].tblLength = sizeof(pattern_scale9_tbl)/sizeof(pattern_scale9_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE10].pTblAddr = pattern_scale10_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE10].tblLength = sizeof(pattern_scale10_tbl)/sizeof(pattern_scale10_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE11].pTblAddr = pattern_scale11_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_SCALE11].tblLength = sizeof(pattern_scale11_tbl)/sizeof(pattern_scale11_tbl[0])*4;

    pInputInfo->DmaiTbls[0][_DMAI_TBL_FDOUT].pTblAddr = pattern_fdout_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_FDOUT].tblLength = sizeof(pattern_fdout_tbl)/sizeof(pattern_fdout_tbl[0])*4;

    MUINT32 i = 0;
#if 0
    LOG_INF("Dump InputFrame in DRAM\n");
    for (i = 0; i < 8; i++)
    {
        LOG_INF("0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x\n", *((MUINT32 *)pattern_inputframe_tbl+8*i), *((MUINT32 *)pattern_inputframe_tbl+8*i+1), *((MUINT32 *)pattern_inputframe_tbl+8*i+2), *((MUINT32 *)pattern_inputframe_tbl+8*i+3), *((MUINT32 *)pattern_inputframe_tbl+8*i+4), *((MUINT32 *)pattern_inputframe_tbl+8*i+5), *((MUINT32 *)pattern_inputframe_tbl+8*i+6), *((MUINT32 *)pattern_inputframe_tbl+8*i+7));
    }
#endif
    LOG_INF("Dump fdconfig in DRAM\n");
    for (i = 0; i < 12; i++)
    {
        LOG_INF("0x%08x 0x%08x 0x%08x 0x%08x\n", *((MUINT32 *)pattern_fdconfig_tbl+4*i), *((MUINT32 *)pattern_fdconfig_tbl+4*i+1), *((MUINT32 *)pattern_fdconfig_tbl+4*i+2), *((MUINT32 *)pattern_fdconfig_tbl+4*i+3));
    }

    LOG_INF("Dump rsconfig in DRAM\n");
    for (i = 0; i < 12; i++)
    {
        LOG_INF("0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x\n", *((MUINT32 *)pattern_rsconfig_tbl+8*i), *((MUINT32 *)pattern_rsconfig_tbl+8*i+1), *((MUINT32 *)pattern_rsconfig_tbl+8*i+2), *((MUINT32 *)pattern_rsconfig_tbl+8*i+3), *((MUINT32 *)pattern_rsconfig_tbl+8*i+4), *((MUINT32 *)pattern_rsconfig_tbl+8*i+5), *((MUINT32 *)pattern_rsconfig_tbl+8*i+6), *((MUINT32 *)pattern_rsconfig_tbl+8*i+7));
    }
#if 0    
    LOG_INF("Dump learnData0 in DRAM\n");
    for (i = 0; i < 8; i++)
    {
        LOG_INF("0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x\n", *((MUINT32 *)pattern_learndata0_tbl+8*i), *((MUINT32 *)pattern_learndata0_tbl+8*i+1), *((MUINT32 *)pattern_learndata0_tbl+8*i+2), *((MUINT32 *)pattern_learndata0_tbl+8*i+3), *((MUINT32 *)pattern_learndata0_tbl+8*i+4), *((MUINT32 *)pattern_learndata0_tbl+8*i+5), *((MUINT32 *)pattern_learndata0_tbl+8*i+6), *((MUINT32 *)pattern_learndata0_tbl+8*i+7));
    }    

    LOG_INF("Dump scaleData0 in DRAM\n");
    for (i = 0; i < 8; i++)
    {
        LOG_INF("0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x\n", *((MUINT32 *)pattern_scale0_tbl+8*i), *((MUINT32 *)pattern_scale0_tbl+8*i+1), *((MUINT32 *)pattern_scale0_tbl+8*i+2), *((MUINT32 *)pattern_scale0_tbl+8*i+3), *((MUINT32 *)pattern_scale0_tbl+8*i+4), *((MUINT32 *)pattern_scale0_tbl+8*i+5), *((MUINT32 *)pattern_scale0_tbl+8*i+6), *((MUINT32 *)pattern_scale0_tbl+8*i+7));
    }
#endif
    //cam_config_pre(pDrvDipPhy);
    
    for (i = 0; i < sizeof(fdvt_reg_addr_frame01)/4; i++)
    {
        pDrvDip->writeReg((fdvt_reg_addr_frame01[i] + FDVT_BASE - DIP1_BASE), fdvt_reg_value_frame01[i]);
    }

    for (i = 0; i < sizeof(fdvt_reg_addr_frame01)/4; i++)
    {
        LOG_INF("FDVT_REG[0x%x] = 0x%x", fdvt_reg_addr_frame01[i], pDrvDip->readReg((fdvt_reg_addr_frame01[i] + FDVT_BASE - DIP1_BASE)));
    }

	//cam_config_pre(pDrvDipPhy);

    //reg = pDrvDip->readReg(DIP_A_CTL_INT_EN - 0x15022000) | 0x80000000 ;
    //LOG_INF("DIP_A_CTL_INT_EN=0x%08x", reg);
    //pDrvDip->writeReg((DIP_A_CTL_INT_EN - 0x15022000), reg);
    //while(1);
    return 0;
}

MINT32 Pattern_BitTrue_FDVT_1(MUINT32* _ptr, MUINT32* _ptrphy, MUINTPTR inputInfo)
{
    (void)_ptr;
    (void)_ptrphy;
    (void)inputInfo;
    MINT32 ret=0;
    /*
    IspDrvDipPhy* pDrvDipPhy = (IspDrvDipPhy*)_ptrphy;
    IMEM_BUF_INFO**** pBuf = NULL;
    TestInputInfo *pInputInfo = (TestInputInfo *)inputInfo;
    UINT32 DMA_EN = 0,_tmp=0,_cnt=0;
    MUINT32 *pTable = NULL,*pMem = NULL;

    static const unsigned char golden_1_img2o[] = {
        #include "Dip/P2_BASIC/Golden/img2o_a_golden.dhex"
    };

    #define COMPARE(STR,STR2)\
            _tmp = (DMA_EN & DIP_X_REG_CTL_DMA_EN_##STR);\
            _cnt = 0;\
            while(_tmp != 0){\
                _cnt++;\
                _tmp >>= 1;\
            }\
            pTable = (MUINT32*)golden_1_##STR2;\
            pMem = (MUINT32*)pBuf[0][_cnt][0]->virtAddr;\
            _tmp = pBuf[0][_cnt][0]->size/sizeof(MUINT32);\
            _cnt = 0;\
            LOG_INF("###########################\n");\
            LOG_INF("cnt: %d, tmp: %d; pMem:%d, pTable:%d\n", _cnt, _tmp, *pMem, *pTable);\
            while((*pMem++ == *pTable++) && (_cnt++ != _tmp)){\
            }\
            if(_cnt != (_tmp+1)){\
                ret++;\
                LOG_ERR("%s bit true fail,%d_%d\n",#STR,_cnt,(_tmp+1));\
            }\
            else{\
                LOG_INF("%s bit true pass\n",#STR);\
            }\
            LOG_INF("###########################\n");\


    pBuf = (IMEM_BUF_INFO****)pInputInfo->pImemBufs;

    
    DMA_EN = DIP_READ_PHY_REG(DIP_HW_A, pDrvDipPhy, DIP_X_CTL_DMA_EN);
    COMPARE(IMG2O, img2o);

    LOG_INF("Exit Compare ...\n");
	//LOG_INF("press any key continuous\n");
    //getchar();
    */
    return ret;
}

MINT32 Pattern_release_FDVT(MUINT32* _ptr, MUINT32* _ptrphy, MUINTPTR BufPtr)
{
    (void)_ptr;
    (void)_ptrphy;
    IMEM_BUF_INFO**** pBuf;
    DipIMemDrv* pImemDrv = NULL;

    pImemDrv = DipIMemDrv::createInstance();
    pBuf = (IMEM_BUF_INFO****)BufPtr;
    for(MUINT32 j=0;j < (DIP_HW_MAX - DIP_HW_A);j++){
        for(MUINT32 i=0;i<32; i++){
            for(MUINT32 n=0;n<2;n++){
                if(pBuf[j][i][n]->size != 0)
                    pImemDrv->freeVirtBuf(pBuf[j][i][n]);
            }
        }
    }

    pImemDrv->uninit();
    pImemDrv->destroyInstance();

    return 0;
}

#define Total_case  2
#define CASE_OP     6   // 5 is for 1: isp drv init|fakeSensor,2:load MMU setting, 3:loading pattern(APMCU or CQ loading). 4:mem allocate + start, 5:stop, 6:deallocate

typedef MINT32 (*LDVT_DCB)(MUINT32*, MUINT32*, MUINTPTR);

LDVT_DCB FD_DCB_TBL[Total_case][CASE_OP] = {
    {Pattern_Loading_FDVT_1, Pattern_Start_FDVT_1, Pattern_Stop_FDVT_1, Pattern_BitTrue_FDVT_1, Pattern_release_FDVT},
};

int IspDrvFDVT_LDVT(void)
{
    int ret = 0;
    MUINT32 test_case;
    IMEM_BUF_INFO ****pimgBuf = NULL;
	PhyDipDrv*    pDrvDip = (PhyDipDrv*)PhyDipDrvImp::createInstance(DIP_HW_A);
    PhyDipDrv* pDrvDipPhy = (PhyDipDrv*)PhyDipDrvImp::createInstance(DIP_HW_A);
    TestInputInfo   testInput;
#if 0

	ISP_DRV_CAM* ptr;

	ptr = (ISP_DRV_CAM*)ISP_DRV_CAM::createInstance(CAM_A,ISP_DRV_CQ_THRE0,0,"Test_IspDrvCam_A");

	if(ptr== NULL){
       LOG_ERR("CAM_A create fail\n");
       return -1;
    }

            if(ptr->init("Test_IspDrvCam_A") == MFALSE){
                ptr->destroyInstance();
                LOG_ERR("CAM_A init failure\n");
                ptr = NULL;
                return -1;
            }
#endif				
    if(pDrvDip == NULL || pDrvDipPhy == NULL){
    	LOG_ERR("Drv create fail\n");
        return -1;
    }

	pDrvDip->init("isp_drv_fd LDVT test");
	pDrvDipPhy->init("isp_drv_fd_phy LDVTtest");
	
    if(pimgBuf == NULL){
        pimgBuf = (IMEM_BUF_INFO****)malloc(sizeof(IMEM_BUF_INFO***)*(1));
        for(MUINT32 i=0;i<(1);i++){
            pimgBuf[i] = (IMEM_BUF_INFO***)malloc(sizeof(IMEM_BUF_INFO**)*_DMAI_TBL_NUM);
            for(MUINT32 j=0;j<_DMAI_TBL_NUM;j++){
                pimgBuf[i][j] = (IMEM_BUF_INFO**)malloc(sizeof(IMEM_BUF_INFO*)*1);
                for(MUINT32 k=0;k<1;k++){
                    pimgBuf[i][j][k] = new IMEM_BUF_INFO();//calls default constructor
                }
            }
        }
    }
    testInput.pImemBufs= pimgBuf;

    //LOG_INF("##############################\n");
    //LOG_INF("case 0: Pass2-GOLDEN\n");
	//LOG_INF("case 1: Pass2-P2A\n");
    //LOG_INF("case 2: Start BW\n");
    //LOG_INF("##############################\n");
    //s = getchar();
    //test_case = atoi((const char*)&s);
    test_case = 0;
    //getchar();

    LOG_INF("start case:%d\n",test_case);
    switch(test_case){
        case 0:
		case 1:
            if((ret = FD_DCB_TBL[test_case][0]((MUINT32*)pDrvDip,(MUINT32*)pDrvDipPhy, (MUINTPTR)&testInput)) != 0){
                LOG_ERR(" case_%d step_2 fail\n",test_case);
                return 1;
            }
 
            if((ret = FD_DCB_TBL[test_case][1]((MUINT32*)pDrvDip,(MUINT32*)pDrvDipPhy, (MUINTPTR)&testInput)) != 0){
                    LOG_ERR(" case_%d step_3 fail\n",test_case);
                    return 1;
            }

            if((ret = FD_DCB_TBL[test_case][2]((MUINT32*)pDrvDip,(MUINT32*)pDrvDipPhy, DIP_HW_A)) != 0){
                LOG_ERR(" case_%d step_4 fail\n",test_case);
                return 1;
            }

            if((ret = FD_DCB_TBL[test_case][3]((MUINT32*)pDrvDip,(MUINT32*)pDrvDipPhy, (MUINTPTR)&testInput)) != 0){
                    LOG_ERR(" case_%d step_5 fail\n",test_case);
            }
            

            if((ret = FD_DCB_TBL[test_case][4]((MUINT32*)pDrvDip,(MUINT32*)pDrvDipPhy, (MUINTPTR)pimgBuf)) != 0){
                LOG_ERR(" case_%d step_6 fail\n",test_case);
                return 1;
            }
            break;
        default:
            LOG_ERR("unsupported case(%d)\n",test_case);
            return 1;
            break;
    }

    for(MUINT32 i=0;i<(DIP_HW_MAX - DIP_HW_A);i++){
        for(MUINT32 j=0;j<32;j++){
            for(MUINT32 k=0;k<2;k++)
                delete pimgBuf[i][j][k];
            free(pimgBuf[i][j]);
        }
        free(pimgBuf[i]);
    }
    pimgBuf = NULL;

	pDrvDip->uninit("isp_drv_dip LDVT test");
    pDrvDip->destroyInstance();

	pDrvDipPhy->uninit("isp_drv_dip_phy LDVTtest");
    pDrvDipPhy->destroyInstance();
#if 0
	if(ptr){
        ptr->uninit("Test_IspDrvCam_A");
        ptr->destroyInstance();
    }
#endif	
    return ret;
}
