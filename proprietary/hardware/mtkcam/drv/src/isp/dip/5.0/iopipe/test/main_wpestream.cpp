/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

///////////////////////////////////////////////////////////////////////////////
// No Warranty
// Except as may be otherwise agreed to in writing, no warranties of any
// kind, whether express or implied, are given by MTK with respect to any MTK
// Deliverables or any use thereof, and MTK Deliverables are provided on an
// "AS IS" basis.  MTK hereby expressly disclaims all such warranties,
// including any implied warranties of merchantability, non-infringement and
// fitness for a particular purpose and any warranties arising out of course
// of performance, course of dealing or usage of trade.  Parties further
// acknowledge that Company may, either presently and/or in the future,
// instruct MTK to assist it in the development and the implementation, in
// accordance with Company's designs, of certain softwares relating to
// Company's product(s) (the "Services").  Except as may be otherwise agreed
// to in writing, no warranties of any kind, whether express or implied, are
// given by MTK with respect to the Services provided, and the Services are
// provided on an "AS IS" basis.  Company further acknowledges that the
// Services may contain errors, that testing is important and Company is
// solely responsible for fully testing the Services and/or derivatives
// thereof before they are used, sublicensed or distributed.  Should there be
// any third party action brought against MTK, arising out of or relating to
// the Services, Company agree to fully indemnify and hold MTK harmless.
// If the parties mutually agree to enter into or continue a business
// relationship or other arrangement, the terms and conditions set forth
// hereunder shall remain effective and, unless explicitly stated otherwise,
// shall prevail in the event of a conflict in the terms in any agreements
// entered into between the parties.
////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2008, MediaTek Inc.
// All rights reserved.
//
// Unauthorized use, practice, perform, copy, distribution, reproduction,
// or disclosure of this information in whole or in part is prohibited.


#define LOG_TAG "wpestream_test"

#include <vector>

#include <sys/time.h>
#include <sys/stat.h>
#include <sys/prctl.h>

#include <stdio.h>
#include <stdlib.h>
//
#include <errno.h>
#include <fcntl.h>

#include <mtkcam/def/common.h>
#include <mtkcam/drv/iopipe/PostProc/INormalStream.h>

//
#include <semaphore.h>
#include <pthread.h>
#include <utils/threads.h>
#include <mtkcam/drv/iopipe/PostProc/IWpeStream.h>
#include <wpeunittest.h>
#include <mtkcam/drv/iopipe/PostProc/WpeUtility.h>

//#include <mtkcam/iopipe/PostProc/IFeatureStream.h>

//#include <mtkcam/imageio/ispio_pipe_ports.h>
#include <imem_drv.h>
//#include <mtkcam/drv/isp_drv.h>

#include <mtkcam/utils/imgbuf/IImageBuffer.h>
#include <utils/StrongPointer.h>
#include <mtkcam/utils/std/common.h>
#include <mtkcam/utils/imgbuf/ImageBufferHeap.h>
#include <mtkcam/utils/imgbuf/ISecureImageBufferHeap.h>

//
//#include "wpe/wpe_testcommon.h"

//


#include <ispio_pipe_ports.h>
#include <imem_drv.h>
#include <isp_drv.h>

#include "isp_drv_dip_phy.h"
//



using namespace std;
using namespace android;
using namespace NSCam;
using namespace NSIoPipe;
using namespace NSPostProc;

using namespace NSWpe;

/******************************************************************************
* save the buffer to the file
*******************************************************************************/
static bool saveBufToFile(char const*const fname, MUINT8 *const buf, MUINT32 const size);
/******************************************************************************
* WPE Test Case
*******************************************************************************/


int wpe_normal(int loop);
int wpe_standalone(int loop);
int wpe_standalone_420(int loop);
int wpe_secure_422(int type,int loop);


bool multi_enque_wpe_tc00_frames_WPE_Config();

MBOOL g_bWPECallback;

MVOID WPECallback(WPEParams& rParams);
MVOID WPE_multi_enque_wpe_tc00_request1_WPECallback(WPEParams& rParams);
MVOID WPE_multi_enque_wpe_tc00_request0_WPECallback(WPEParams& rParams);

MBOOL g_b_multi_enque_wpe_tc00_request0_WPECallback;
MBOOL g_b_multi_enque_wpe_tc00_request1_WPECallback;

int g_WpeMultipleBuffer = 0;
int g_WpeCount = 0;

pthread_t       WpeUserThread;
sem_t           WpeSem;
volatile bool            g_bWpeThreadTerminated = 0;

/*******************************************************************************
*  Main Function
*
********************************************************************************/


void* WpeThreadLoop(void *arg)
{
    (void)arg;
    int ret = 0;

    ::pthread_detach(::pthread_self());
    ::sem_wait(&WpeSem);

    while (g_bWpeThreadTerminated) {

        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("Sub Thread do direct link test");
        ret = wpe_standalone_420(2);
                        printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
        printf("*******************Sub  Thread*******************\n");
        printf("*******************Sub  Thread*******************\n");

    }
    ::sem_post(&WpeSem);
    printf("###################Sub  Wait###################\n");
    printf("###################Sub  Wait###################\n");
    printf("###################Sub  Wait###################\n");
    return NULL;
}


int test_wpeStream(int argc, char** argv)
{
    int ret = 0; 
    int testCase = 0;
	int testLoop = 0;
	int testType = 0;
    int i = 0;
    if (argc>=1)
    {
        testCase = atoi(argv[1]);
        testType = atoi(argv[2]);
        testLoop = atoi(argv[3]);
    }

    switch(testCase)
    {
        case 0:  //Unit Test Case One WPE Request.
            g_WPE_UnitTest_Num = WPE_DEFAULT_UT; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            ret = wpe_normal(testLoop);
            break;

		case 1:  //Unit Test Case One WPE Request.
            g_WPE_UnitTest_Num = WPE_TESTCASE_UT_0; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            ret = wpe_standalone(testLoop);
            break;

        case 2: //Two wpe requests at the same time.
            ::sem_init(&WpeSem,0, 0);
            g_WPE_UnitTest_Num = WPE_TESTCASE_UT_1; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            pthread_create(&WpeUserThread, NULL, WpeThreadLoop, (void*)NULL);
            g_bWpeThreadTerminated = 1;
            ::sem_post(&WpeSem);
            while(1)
            {
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("%%%%%%%%%%%% Main Thread do direct link test%%%%%%%%%%\n");
                ret = wpe_normal(2);

                printf("%%%%%%%%%%%% Main Thread do standalone test%%%%%%%%%%\n");
                ret = wpe_standalone(2);
                i++;
                printf("--- [WPE test times: %d, input loop: %d]\n", i, testLoop);
                //if(i == testLoop){break;}
                printf("&&&&&&&&&&&&&&&&&Main  Thread&&&&&&&&&&&&&&&&&&&&&\n");
                printf("&&&&&&&&&&&&&&&&&Main  Thread&&&&&&&&&&&&&&&&&&&&&\n");

            }
            g_bWpeThreadTerminated = 0;

            ::sem_wait(&WpeSem);
            break;

		case 3:  //Unit Test Case One WPE Request.
            g_WPE_UnitTest_Num = WPE_TESTCASE_UT_2; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            ret = wpe_standalone_420( testLoop);
            break;

		case 5:  //WPE Secure
            g_WPE_UnitTest_Num = WPE_TESTCASE_UT_3; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            ret = wpe_secure_422(testType,testLoop);
            break;

       default:
            break;
    }

    ret = 1;
    return ret;
}

MVOID WPECallback_Normal(QParams& rParams)
{
    printf("--- [WPE callback func]\n");
    (void)rParams;
    g_bWPECallback = MTRUE;
}

MVOID WPECallback(WPEParams& rParams)
{
    printf("--- [WPE callback func]\n");
    (void)rParams;
    g_bWPECallback = MTRUE;
}

#include "wpe/wpe_cachemiss/cachi_a.h"
#include "wpe/wpe_cachemiss/vec2i_a.h"
#include "wpe/wpe_cachemiss/vec3i_a.h"
#include "wpe/wpe_cachemiss/veci_a.h"
#include "wpe/wpe_cachemiss/wpe_tdri.h"
#include "wpe/wpe_cachemiss/wpeo_a.c"
#include "wpe/cachi_640_480_nv12.h"


int wpe_normal(int loop)
{
    int ret=0;
	int i;
	NSCam::NSIoPipe::NSWpe::WPEQParams enqueWpeParams;
	NSCam::NSIoPipe::NSWpe::WPEQParams enqueWpeParams1;

	printf("--- [WPE Direct Link Loop(%d)...Enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", loop);
	NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
	pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
	pStream->init("basicWPE", NSCam::NSIoPipe::EStreamPipeID_WarpEG);
	printf("--- [basicWPE...pStream init done]\n");

	DipIMemDrv* mpImemDrv=NULL;
	mpImemDrv=DipIMemDrv::createInstance();
	mpImemDrv->init();

	QParams enqueParams;
	FrameParams frameParams;
	FrameParams frameParams1;
		//frame tag
	//FrameParams.mvStreamTag.push_back(NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal);

	//MUINT32 Width = 0xF00, Height = 0x870, Stride = 0x1E00;
	//input image
	MUINT32 _imgi_w_= 0xF00, _imgi_h_= 0x870;

	MINT32 bufBoundaryInBytes[3] = {0, 0, 0};


	IMEM_BUF_INFO buf_imgi;
	buf_imgi.size= _imgi_w_ * _imgi_h_ * 2;

	mpImemDrv->allocVirtBuf(&buf_imgi);

	memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(cachi_a), _imgi_w_ * _imgi_h_ * 2);
		//imem buffer 2 image heap
	printf("--- [basicWPE(%d)...flag -1 ]\n", loop);
	printf("buf_imgi: VA(0x%" PRIxPTR "), PA(0x%" PRIxPTR ") size:(%d)\n",buf_imgi.virtAddr, buf_imgi.phyAddr, buf_imgi.size);
	IImageBuffer* srcBuffer;

	MUINT32 bufStridesInBytes[3] = {_imgi_w_ * 2, 0, 0};

	PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
	IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
	sp<ImageBufferHeap> pHeap;
	pHeap = ImageBufferHeap::create( "basicWPE", imgParam,portBufInfo,true);
	srcBuffer = pHeap->createImageBuffer();
	srcBuffer->incStrong(srcBuffer);
	srcBuffer->lockBuf("basicWPE",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);

	NSCam::NSIoPipe::Input src;
	src.mPortID=PORT_WPEI;
	src.mBuffer=srcBuffer;
	src.mPortID.group=0;
	frameParams.mvIn.push_back(src);
	printf("--- [basicWPE...push src done]\n");

	  //crop information
	MCrpRsInfo crop;
	crop.mFrameGroup=0;
	crop.mGroupID=1;
	MCrpRsInfo crop2;
	crop2.mFrameGroup=0;
	crop2.mGroupID=2;
	MCrpRsInfo crop3;
	crop3.mFrameGroup=0;
	crop3.mGroupID=3;
	crop.mCropRect.p_fractional.x=0;
	crop.mCropRect.p_fractional.y=0;
	crop.mCropRect.p_integral.x=0;
	crop.mCropRect.p_integral.y=0;
	crop.mCropRect.s.w=_imgi_w_;
	crop.mCropRect.s.h=_imgi_h_;
	crop.mResizeDst.w=_imgi_w_;
	crop.mResizeDst.h=_imgi_h_;
	crop2.mCropRect.p_fractional.x=0;
	crop2.mCropRect.p_fractional.y=0;
	crop2.mCropRect.p_integral.x=0;
	crop2.mCropRect.p_integral.y=0;
	crop2.mCropRect.s.w=_imgi_w_;
	crop2.mCropRect.s.h=_imgi_h_;
	crop2.mResizeDst.w=_imgi_w_;
	crop2.mResizeDst.h=_imgi_h_;
	crop3.mCropRect.p_fractional.x=0;
	crop3.mCropRect.p_fractional.y=0;
	crop3.mCropRect.p_integral.x=0;
	crop3.mCropRect.p_integral.y=0;
	crop3.mCropRect.s.w=_imgi_w_;
	crop3.mCropRect.s.h=_imgi_h_;
	crop3.mResizeDst.w=_imgi_w_;
	crop3.mResizeDst.h=_imgi_h_;

	frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
	frameParams1.mvCropRsInfo.push_back(crop);
    frameParams1.mvCropRsInfo.push_back(crop2);
    frameParams1.mvCropRsInfo.push_back(crop3);

	printf("--- [PORT_WPEI...push crop information done\n]");

	//output dma
	IMEM_BUF_INFO buf_out1;
	buf_out1.size=_imgi_w_*_imgi_h_*2;
	mpImemDrv->allocVirtBuf(&buf_out1);
	memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);

    printf("buf_out1: VA(0x%" PRIxPTR "), PA(0x%" PRIxPTR ") size:(%d)\n",buf_out1.virtAddr, buf_out1.phyAddr, buf_out1.size);
	IImageBuffer* outBuffer=NULL;
	MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
	PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
	IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
												MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
	sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "PORT_WPEI", imgParam_1,portBufInfo_1,true);
	outBuffer = pHeap_1->createImageBuffer();
	outBuffer->incStrong(outBuffer);
	outBuffer->lockBuf("PORT_WPEI",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);

	NSCam::NSIoPipe::Output dst;

	dst.mPortID = PORT_WDMAO;

	dst.mBuffer=outBuffer;
	dst.mPortID.group=0;
	frameParams.mvOut.push_back(dst);


	 //VECI Buf
	IMEM_BUF_INFO buf_wpe_veci;
    buf_wpe_veci.size = sizeof(veci_a);

    mpImemDrv->allocVirtBuf(&buf_wpe_veci);
    mpImemDrv->mapPhyAddr(&buf_wpe_veci);
	memcpy( (MUINT8*)(buf_wpe_veci.virtAddr), (MUINT8*)(veci_a), sizeof(veci_a));
	printf("buf_wpe_veci: VA(0x%" PRIxPTR "), PA(0x%" PRIxPTR ") size:(%d)\n",buf_wpe_veci.virtAddr, buf_wpe_veci.phyAddr, buf_wpe_veci.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wpe_veci);

    //VEC2I Buf
	IMEM_BUF_INFO buf_wpe_vec2i;
    buf_wpe_vec2i.size = sizeof(vec2i_a);
	//printf("buf_wpe_vec2i.size:%d\n",buf_wpe_vec2i.size);
	//
    mpImemDrv->allocVirtBuf(&buf_wpe_vec2i);
    mpImemDrv->mapPhyAddr(&buf_wpe_vec2i);
	printf("buf_wpe_vec2i: VA(0x%" PRIxPTR "), PA(0x%" PRIxPTR ") size:(%d)\n",buf_wpe_vec2i.virtAddr, buf_wpe_vec2i.phyAddr, buf_wpe_vec2i.size);
	memcpy( (MUINT8*)(buf_wpe_vec2i.virtAddr), (MUINT8*)(vec2i_a), sizeof(vec2i_a));
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wpe_vec2i);

	 //VEC3I Buf
	IMEM_BUF_INFO buf_wpe_vec3i;
    buf_wpe_vec3i.size = sizeof(vec3i_a);
	printf("buf_wpe_vec3i.size:%d\n",buf_wpe_vec3i.size);
	//
    mpImemDrv->allocVirtBuf(&buf_wpe_vec3i);
    mpImemDrv->mapPhyAddr(&buf_wpe_vec3i);
	printf("buf_wpe_vec3i: VA(0x%" PRIxPTR "), PA(0x%" PRIxPTR ") size:(%d)\n",buf_wpe_vec3i.virtAddr, buf_wpe_vec3i.phyAddr, buf_wpe_vec3i.size);
	memcpy( (MUINT8*)(buf_wpe_vec3i.virtAddr), (MUINT8*)(vec3i_a), sizeof(vec3i_a));
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wpe_vec3i);


	enqueWpeParams.warp_veci_info.virtAddr = buf_wpe_veci.virtAddr;
	enqueWpeParams.warp_veci_info.phyAddr= buf_wpe_veci.phyAddr;
	enqueWpeParams.warp_veci_info.bus_size = NSCam::NSIoPipe::NSWpe::WPE_BUS_SIZE_32_BITS;
	enqueWpeParams.warp_veci_info.stride = 0x00000A00;
	enqueWpeParams.warp_veci_info.width = 0x00000280;
	enqueWpeParams.warp_veci_info.height = 0x00000168;
	enqueWpeParams.warp_veci_info.veci_v_flip_en = 0;
	enqueWpeParams.warp_veci_info.addr_offset = 0;




	enqueWpeParams.warp_vec2i_info.virtAddr = buf_wpe_vec2i.virtAddr;
	enqueWpeParams.warp_vec2i_info.phyAddr= buf_wpe_vec2i.phyAddr;
	enqueWpeParams.warp_vec2i_info.bus_size = NSCam::NSIoPipe::NSWpe::WPE_BUS_SIZE_32_BITS;
	enqueWpeParams.warp_vec2i_info.stride = 0x00000A00;
	enqueWpeParams.warp_vec2i_info.width = 0x00000280;
	enqueWpeParams.warp_vec2i_info.height = 0x00000168;
	enqueWpeParams.warp_vec2i_info.veci_v_flip_en = 0;
	enqueWpeParams.warp_vec2i_info.addr_offset = 0;
	

	enqueWpeParams.warp_vec3i_info.virtAddr = buf_wpe_vec3i.virtAddr;
	enqueWpeParams.warp_vec3i_info.phyAddr= buf_wpe_vec3i.phyAddr;
	enqueWpeParams.warp_vec3i_info.bus_size = NSCam::NSIoPipe::NSWpe::WPE_BUS_SIZE_32_BITS;
	enqueWpeParams.warp_vec3i_info.stride = 0x00000A00;
	enqueWpeParams.warp_vec3i_info.width = 0x00000280;
	enqueWpeParams.warp_vec3i_info.height = 0x00000168;
	enqueWpeParams.warp_vec3i_info.veci_v_flip_en = 0;
	enqueWpeParams.warp_vec3i_info.addr_offset = 0;

	enqueWpeParams.wpe_mode = 1;

    enqueWpeParams.vgen_hmg_mode = 1;

	enqueWpeParams1.warp_veci_info.virtAddr = buf_wpe_veci.virtAddr;
	enqueWpeParams1.warp_veci_info.phyAddr= buf_wpe_veci.phyAddr;
	enqueWpeParams1.warp_veci_info.bus_size = NSCam::NSIoPipe::NSWpe::WPE_BUS_SIZE_32_BITS;
	enqueWpeParams1.warp_veci_info.stride = 0x00000A00;
	enqueWpeParams1.warp_veci_info.width = 0x00000280;
	enqueWpeParams1.warp_veci_info.height = 0x00000168;
	enqueWpeParams1.warp_veci_info.veci_v_flip_en = 0;
	enqueWpeParams1.warp_veci_info.addr_offset = 0;




	enqueWpeParams1.warp_vec2i_info.virtAddr = buf_wpe_vec2i.virtAddr;
	enqueWpeParams1.warp_vec2i_info.phyAddr= buf_wpe_vec2i.phyAddr;
	enqueWpeParams1.warp_vec2i_info.bus_size = NSCam::NSIoPipe::NSWpe::WPE_BUS_SIZE_32_BITS;
	enqueWpeParams1.warp_vec2i_info.stride = 0x00000A00;
	enqueWpeParams1.warp_vec2i_info.width = 0x00000280;
	enqueWpeParams1.warp_vec2i_info.height = 0x00000168;
	enqueWpeParams1.warp_vec2i_info.veci_v_flip_en = 0;
	enqueWpeParams1.warp_vec2i_info.addr_offset = 0;
	

	enqueWpeParams1.warp_vec3i_info.virtAddr = buf_wpe_vec3i.virtAddr;
	enqueWpeParams1.warp_vec3i_info.phyAddr= buf_wpe_vec3i.phyAddr;
	enqueWpeParams1.warp_vec3i_info.bus_size = NSCam::NSIoPipe::NSWpe::WPE_BUS_SIZE_32_BITS;
	enqueWpeParams1.warp_vec3i_info.stride = 0x00000A00;
	enqueWpeParams1.warp_vec3i_info.width = 0x00000280;
	enqueWpeParams1.warp_vec3i_info.height = 0x00000168;
	enqueWpeParams1.warp_vec3i_info.veci_v_flip_en = 0;
	enqueWpeParams1.warp_vec3i_info.addr_offset = 0;

	enqueWpeParams1.wpe_mode = 0;

    enqueWpeParams1.vgen_hmg_mode = 1;

	MCrpRsInfo vgenCropInfo;

	vgenCropInfo.mCropRect.p_integral.x = 0;
	vgenCropInfo.mCropRect.p_integral.y = 0;
	vgenCropInfo.mCropRect.p_fractional.x = 0;
	vgenCropInfo.mCropRect.p_fractional.y = 0;//0x00C16FFE;

	enqueWpeParams.mwVgenCropInfo.push_back(vgenCropInfo);
	enqueWpeParams1.mwVgenCropInfo.push_back(vgenCropInfo);


	enqueWpeParams.wpecropinfo.x_start_point = 0;
	enqueWpeParams.wpecropinfo.y_start_point = 0;
	enqueWpeParams.wpecropinfo.x_end_point = _imgi_w_ - 1;
	enqueWpeParams.wpecropinfo.y_end_point = _imgi_h_ - 1;

	NSCam::NSIoPipe::ExtraParam eParams;

	eParams.CmdIdx = NSCam::NSIoPipe::EPIPE_WPE_INFO_CMD;
	eParams.moduleStruct = (void *) &(enqueWpeParams);

    frameParams.mvExtraParam.push_back(eParams);

	enqueWpeParams1.wpecropinfo.x_start_point = 0;
	enqueWpeParams1.wpecropinfo.y_start_point = 0;
	enqueWpeParams1.wpecropinfo.x_end_point = _imgi_w_ - 1;
	enqueWpeParams1.wpecropinfo.y_end_point = _imgi_h_ - 1;

	NSCam::NSIoPipe::ExtraParam eParams1;

	eParams1.CmdIdx = NSCam::NSIoPipe::EPIPE_WPE_INFO_CMD;
	eParams1.moduleStruct = (void *) &(enqueWpeParams1);

    frameParams1.mvExtraParam.push_back(eParams1);


	enqueParams.mpfnCallback = WPECallback_Normal;
	enqueParams.mvFrameParams.push_back(frameParams);
    enqueParams.mvFrameParams.push_back(frameParams);

    printf("####wpei.PA(0x%" PRIxPTR "), wpeo.PA(0x%" PRIxPTR "), veci.PA(0x%" PRIxPTR "), vec2i.PA(0x%" PRIxPTR "), vec3i.PA(0x%" PRIxPTR ")\n",
              enqueParams.mvFrameParams[0].mvIn[0].mBuffer->getBufPA(0), enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufPA(0), buf_wpe_veci.phyAddr, buf_wpe_vec2i.phyAddr , buf_wpe_vec3i.phyAddr);


	printf("--- [basicWPE...push dst done\n]");

	// WPE -----

	printf("#########################################################\n");
	printf("###########WPE Direct Link Mode Start to Test !!!!###########\n");
	printf("#########################################################\n");

	i= 0;
	for(int i=0;i<loop;i++)
	{
		memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
		//memset((MUINT8*)(enqueParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

		//buffer operation
		//mpImemDrv->cacheFlushAll();
		printf("--- [Direct Link Loop(%d)...flush done\n]", i);
	    g_bWPECallback = MFALSE;
		//enque
		ret=pStream->enque(enqueParams);
		if(!ret)
		{
				printf("---ERRRRRRRRR [Direct Link Loop(%d)..enque fail\n]", i);
		}
		else
		{
				printf("---[Direct Link Loop(%d)..enque done\n]",i);
		}

        do{
			   usleep(100000);
			   if (MTRUE == g_bWPECallback)
			   {
				   break;
			   }
		}while(1);

	}

  //free
		  srcBuffer->unlockBuf("basicWPE");
		  mpImemDrv->freeVirtBuf(&buf_imgi);
		  outBuffer->unlockBuf("basicWPE");
		  mpImemDrv->freeVirtBuf(&buf_out1);


		  mpImemDrv->freeVirtBuf(&buf_wpe_veci);
		  mpImemDrv->freeVirtBuf(&buf_wpe_vec2i);
		  mpImemDrv->freeVirtBuf(&buf_wpe_vec3i);
		  printf("--- [Direct Link ...free memory done\n]");

		  //
		  pStream->uninit("basicWPE", NSCam::NSIoPipe::EStreamPipeID_WarpEG);
		  pStream->destroyInstance();
		  printf("--- [Direct Link ...pStream uninit done\n]");
		  mpImemDrv->uninit();
		  mpImemDrv->destroyInstance();

		  return ret;

}



int wpe_standalone(int loop)
{
    int ret=0;
	int i;
	NSCam::NSIoPipe::NSWpe::WPEQParams enqueWpeParams1;

	printf("--- [WPE Standalone Loop(%d)...Enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", loop);
	NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
	pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
	pStream->init("basicWPE", NSCam::NSIoPipe::EStreamPipeID_WarpEG);
	printf("--- [basicWPE...pStream init done]\n");

	DipIMemDrv* mpImemDrv=NULL;
	mpImemDrv=DipIMemDrv::createInstance();
	mpImemDrv->init();

	QParams enqueParams;
	FrameParams frameParams1;

		//frame tag
	//FrameParams.mvStreamTag.push_back(NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal);

	//MUINT32 Width = 0xF00, Height = 0x870, Stride = 0x1E00;
	//input image
	MUINT32 _imgi_w_= 0xF00, _imgi_h_= 0x870;

	MINT32 bufBoundaryInBytes[3] = {0, 0, 0};


	IMEM_BUF_INFO buf_imgi;
	buf_imgi.size= _imgi_w_ * _imgi_h_ * 2;

	mpImemDrv->allocVirtBuf(&buf_imgi);

	memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(cachi_a), _imgi_w_ * _imgi_h_ * 2);
		//imem buffer 2 image heap

	printf("buf_imgi: VA(0x%" PRIxPTR "), PA(0x%" PRIxPTR ") size:(%d)\n",buf_imgi.virtAddr, buf_imgi.phyAddr, buf_imgi.size);
	IImageBuffer* srcBuffer;

	MUINT32 bufStridesInBytes[3] = {_imgi_w_ * 2, 0, 0};

	PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
	IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
	sp<ImageBufferHeap> pHeap;
	pHeap = ImageBufferHeap::create( "basicWPE", imgParam,portBufInfo,true);
	srcBuffer = pHeap->createImageBuffer();
	srcBuffer->incStrong(srcBuffer);
	srcBuffer->lockBuf("basicWPE",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);

	NSCam::NSIoPipe::Input src;
	src.mPortID=PORT_WPEI;
	src.mBuffer=srcBuffer;
	src.mPortID.group=0;
	frameParams1.mvIn.push_back(src);
	printf("--- [basicWPE...push src done]\n");

	  //crop information
	MCrpRsInfo crop;
	crop.mFrameGroup=0;
	crop.mGroupID=1;
	MCrpRsInfo crop2;
	crop2.mFrameGroup=0;
	crop2.mGroupID=2;
	MCrpRsInfo crop3;
	crop3.mFrameGroup=0;
	crop3.mGroupID=3;
	crop.mCropRect.p_fractional.x=0;
	crop.mCropRect.p_fractional.y=0;
	crop.mCropRect.p_integral.x=0;
	crop.mCropRect.p_integral.y=0;
	crop.mCropRect.s.w=_imgi_w_;
	crop.mCropRect.s.h=_imgi_h_;
	crop.mResizeDst.w=_imgi_w_;
	crop.mResizeDst.h=_imgi_h_;
	crop2.mCropRect.p_fractional.x=0;
	crop2.mCropRect.p_fractional.y=0;
	crop2.mCropRect.p_integral.x=0;
	crop2.mCropRect.p_integral.y=0;
	crop2.mCropRect.s.w=_imgi_w_;
	crop2.mCropRect.s.h=_imgi_h_;
	crop2.mResizeDst.w=_imgi_w_;
	crop2.mResizeDst.h=_imgi_h_;
	crop3.mCropRect.p_fractional.x=0;
	crop3.mCropRect.p_fractional.y=0;
	crop3.mCropRect.p_integral.x=0;
	crop3.mCropRect.p_integral.y=0;
	crop3.mCropRect.s.w=_imgi_w_;
	crop3.mCropRect.s.h=_imgi_h_;
	crop3.mResizeDst.w=_imgi_w_;
	crop3.mResizeDst.h=_imgi_h_;

	frameParams1.mvCropRsInfo.push_back(crop);
    frameParams1.mvCropRsInfo.push_back(crop2);
    frameParams1.mvCropRsInfo.push_back(crop3);

	printf("--- [PORT_WPEI...push crop information done\n]");

	//output dma
	IMEM_BUF_INFO buf_out1;
	buf_out1.size=_imgi_w_*_imgi_h_*2;
	mpImemDrv->allocVirtBuf(&buf_out1);
	memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);

    printf("buf_out1: VA(0x%" PRIxPTR "), PA(0x%" PRIxPTR ") size:(%d)\n",buf_out1.virtAddr, buf_out1.phyAddr, buf_out1.size);
	IImageBuffer* outBuffer=NULL;
	MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
	PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
	IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
												MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
	sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "PORT_WPEI", imgParam_1,portBufInfo_1,true);
	outBuffer = pHeap_1->createImageBuffer();
	outBuffer->incStrong(outBuffer);
	outBuffer->lockBuf("PORT_WPEI",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);

	NSCam::NSIoPipe::Output dst;

	dst.mPortID = PORT_WPEO;

	dst.mBuffer=outBuffer;
	dst.mPortID.group=0;
	frameParams1.mvOut.push_back(dst);


	 //VECI Buf
	IMEM_BUF_INFO buf_wpe_veci;
    buf_wpe_veci.size = sizeof(veci_a);

    mpImemDrv->allocVirtBuf(&buf_wpe_veci);
    mpImemDrv->mapPhyAddr(&buf_wpe_veci);
	memcpy( (MUINT8*)(buf_wpe_veci.virtAddr), (MUINT8*)(veci_a), sizeof(veci_a));
	printf("buf_wpe_veci: VA(0x%" PRIxPTR "), PA(0x%" PRIxPTR ") size:(%d)\n",buf_wpe_veci.virtAddr, buf_wpe_veci.phyAddr, buf_wpe_veci.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wpe_veci);

    //VEC2I Buf
	IMEM_BUF_INFO buf_wpe_vec2i;
    buf_wpe_vec2i.size = sizeof(vec2i_a);
	//printf("buf_wpe_vec2i.size:%d\n",buf_wpe_vec2i.size);
	//
    mpImemDrv->allocVirtBuf(&buf_wpe_vec2i);
    mpImemDrv->mapPhyAddr(&buf_wpe_vec2i);
	printf("buf_wpe_vec2i: VA(0x%" PRIxPTR "), PA(0x%" PRIxPTR ") size:(%d)\n",buf_wpe_vec2i.virtAddr, buf_wpe_vec2i.phyAddr, buf_wpe_vec2i.size);
	memcpy( (MUINT8*)(buf_wpe_vec2i.virtAddr), (MUINT8*)(vec2i_a), sizeof(vec2i_a));
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wpe_vec2i);

	 //VEC3I Buf
	IMEM_BUF_INFO buf_wpe_vec3i;
    buf_wpe_vec3i.size = sizeof(vec3i_a);
	printf("buf_wpe_vec3i.size:%d\n",buf_wpe_vec3i.size);
	//
    mpImemDrv->allocVirtBuf(&buf_wpe_vec3i);
    mpImemDrv->mapPhyAddr(&buf_wpe_vec3i);
	printf("buf_wpe_vec3i: VA(0x%" PRIxPTR "), PA(0x%" PRIxPTR ") size:(%d)\n",buf_wpe_vec3i.virtAddr, buf_wpe_vec3i.phyAddr, buf_wpe_vec3i.size);
	memcpy( (MUINT8*)(buf_wpe_vec3i.virtAddr), (MUINT8*)(vec3i_a), sizeof(vec3i_a));
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wpe_vec3i);


	enqueWpeParams1.warp_veci_info.virtAddr = buf_wpe_veci.virtAddr;
	enqueWpeParams1.warp_veci_info.phyAddr= buf_wpe_veci.phyAddr;
	enqueWpeParams1.warp_veci_info.bus_size = NSCam::NSIoPipe::NSWpe::WPE_BUS_SIZE_32_BITS;
	enqueWpeParams1.warp_veci_info.stride = 0x00000A00;
	enqueWpeParams1.warp_veci_info.width = 0x00000280;
	enqueWpeParams1.warp_veci_info.height = 0x00000168;
	enqueWpeParams1.warp_veci_info.veci_v_flip_en = 0;
	enqueWpeParams1.warp_veci_info.addr_offset = 0;




	enqueWpeParams1.warp_vec2i_info.virtAddr = buf_wpe_vec2i.virtAddr;
	enqueWpeParams1.warp_vec2i_info.phyAddr= buf_wpe_vec2i.phyAddr;
	enqueWpeParams1.warp_vec2i_info.bus_size = NSCam::NSIoPipe::NSWpe::WPE_BUS_SIZE_32_BITS;
	enqueWpeParams1.warp_vec2i_info.stride = 0x00000A00;
	enqueWpeParams1.warp_vec2i_info.width = 0x00000280;
	enqueWpeParams1.warp_vec2i_info.height = 0x00000168;
	enqueWpeParams1.warp_vec2i_info.veci_v_flip_en = 0;
	enqueWpeParams1.warp_vec2i_info.addr_offset = 0;


	enqueWpeParams1.warp_vec3i_info.virtAddr = buf_wpe_vec3i.virtAddr;
	enqueWpeParams1.warp_vec3i_info.phyAddr= buf_wpe_vec3i.phyAddr;
	enqueWpeParams1.warp_vec3i_info.bus_size = NSCam::NSIoPipe::NSWpe::WPE_BUS_SIZE_32_BITS;
	enqueWpeParams1.warp_vec3i_info.stride = 0x00000A00;
	enqueWpeParams1.warp_vec3i_info.width = 0x00000280;
	enqueWpeParams1.warp_vec3i_info.height = 0x00000168;
	enqueWpeParams1.warp_vec3i_info.veci_v_flip_en = 0;
	enqueWpeParams1.warp_vec3i_info.addr_offset = 0;

	enqueWpeParams1.wpe_mode = 0;

    enqueWpeParams1.vgen_hmg_mode = 1;

	MCrpRsInfo vgenCropInfo;

	vgenCropInfo.mCropRect.p_integral.x = 0;
	vgenCropInfo.mCropRect.p_integral.y = 0;
	vgenCropInfo.mCropRect.p_fractional.x = 0;
	vgenCropInfo.mCropRect.p_fractional.y = 0;//0x00C16FFE;

	enqueWpeParams1.mwVgenCropInfo.push_back(vgenCropInfo);


	enqueWpeParams1.wpecropinfo.x_start_point = 0;
	enqueWpeParams1.wpecropinfo.y_start_point = 0;
	enqueWpeParams1.wpecropinfo.x_end_point = _imgi_w_ - 1;
	enqueWpeParams1.wpecropinfo.y_end_point = _imgi_h_ - 1;

	NSCam::NSIoPipe::ExtraParam eParams;

	eParams.CmdIdx = NSCam::NSIoPipe::EPIPE_WPE_INFO_CMD;
	eParams.moduleStruct = (void *) &(enqueWpeParams1);

    frameParams1.mvExtraParam.push_back(eParams);


	enqueParams.mpfnCallback = WPECallback_Normal;
	enqueParams.mvFrameParams.push_back(frameParams1);

    printf("####wpei.PA(0x%" PRIxPTR "), wpeo.PA(0x%" PRIxPTR "), veci.PA(0x%" PRIxPTR "), vec2i.PA(0x%" PRIxPTR "), vec3i.PA(0x%" PRIxPTR ")\n",
              enqueParams.mvFrameParams[0].mvIn[0].mBuffer->getBufPA(0), enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufPA(0), buf_wpe_veci.phyAddr, buf_wpe_vec2i.phyAddr , buf_wpe_vec3i.phyAddr);


	printf("--- [basicWPE...push dst done\n]");

	// WPE -----

	printf("#########################################################\n");
	printf("###########WPE Stand alone Mode Start to Test !!!!###########\n");
	printf("#########################################################\n");

	i= 0;
	for(int i=0;i<loop;i++)
	{
		memset((MUINT8*)(frameParams1.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
		//memset((MUINT8*)(enqueParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

		//buffer operation
		//mpImemDrv->cacheFlushAll();
		printf("--- [PORT_WPEI(%d)...flush done\n]", i);

	    g_bWPECallback = MFALSE;
		//enque
		ret=pStream->enque(enqueParams);
		if(!ret)
		{
				printf("---ERRRRRRRRR [Stand alone Mode(%d)..enque fail\n]", i);
		}
		else
		{
				printf("---[Stand alone Mode(%d)..enque done\n]",i);
		}



	   do{
			   usleep(100000);
			   if (MTRUE == g_bWPECallback)
			   {
				   break;
			   }
		   }while(1);


	}

  //free
		  srcBuffer->unlockBuf("basicWPE");
		  mpImemDrv->freeVirtBuf(&buf_imgi);
		  outBuffer->unlockBuf("basicWPE");
		  mpImemDrv->freeVirtBuf(&buf_out1);


		  mpImemDrv->freeVirtBuf(&buf_wpe_veci);
		  mpImemDrv->freeVirtBuf(&buf_wpe_vec2i);
		  mpImemDrv->freeVirtBuf(&buf_wpe_vec3i);

		  printf("--- [Stand alone Mode...free memory done\n]");

		  //
		  pStream->uninit("basicWPE", NSCam::NSIoPipe::EStreamPipeID_WarpEG);
		  pStream->destroyInstance();
		  printf("--- [Stand alone Mode...pStream uninit done\n]");
		  mpImemDrv->uninit();
		  mpImemDrv->destroyInstance();

		  return ret;

}

int wpe_standalone_420(int loop)
{
    int ret=0;
	int i;
	NSCam::NSIoPipe::NSWpe::WPEQParams enqueWpeParams1;

	printf("--- [WPE 420 Standalone Loop(%d)...Enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", loop);
	NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
	pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
	pStream->init("basicWPE_420", NSCam::NSIoPipe::EStreamPipeID_WarpEG);
	printf("--- [basicWPE 420 ...pStream init done]\n");

	DipIMemDrv* mpImemDrv=NULL;
	mpImemDrv=DipIMemDrv::createInstance();
	mpImemDrv->init();

	QParams enqueParams;
	FrameParams frameParams1;

		//frame tag
	//FrameParams.mvStreamTag.push_back(NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal);

	//MUINT32 Width = 0xF00, Height = 0x870, Stride = 0x1E00;
	//input image
	MUINT32 _imgi_w_= 640, _imgi_h_= 480;

	MINT32 bufBoundaryInBytes[3] = {0, 0, 0};


	IMEM_BUF_INFO buf_imgi;
	buf_imgi.size= sizeof(cachi_640_480_NV12);

	mpImemDrv->allocVirtBuf(&buf_imgi);

	memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(cachi_640_480_NV12), buf_imgi.size);
		//imem buffer 2 image heap

	printf("buf_imgi: VA(0x%" PRIxPTR "), PA(0x%" PRIxPTR ") size:(%d)\n",buf_imgi.virtAddr, buf_imgi.phyAddr, buf_imgi.size);
	IImageBuffer* srcBuffer;

	MUINT32 bufStridesInBytes[3] = {_imgi_w_, _imgi_w_, 0};//NV12 input, Y: same as width , UV: same as width

	PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
	IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_NV12),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 2);
	sp<ImageBufferHeap> pHeap;
	pHeap = ImageBufferHeap::create( "basicWPE_420_src", imgParam,portBufInfo,true);
	srcBuffer = pHeap->createImageBuffer();
	srcBuffer->incStrong(srcBuffer);
	srcBuffer->lockBuf("basicWPE_420_src",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);

	NSCam::NSIoPipe::Input src;
	src.mPortID=PORT_WPEI;
	src.mBuffer=srcBuffer;
	src.mPortID.group=0;
	frameParams1.mvIn.push_back(src);
	printf("--- [basicWPE...push src done]\n");

	  //crop information
	MCrpRsInfo crop;
	crop.mFrameGroup=0;
	crop.mGroupID=1;
	MCrpRsInfo crop2;
	crop2.mFrameGroup=0;
	crop2.mGroupID=2;
	MCrpRsInfo crop3;
	crop3.mFrameGroup=0;
	crop3.mGroupID=3;
	crop.mCropRect.p_fractional.x=0;
	crop.mCropRect.p_fractional.y=0;
	crop.mCropRect.p_integral.x=0;
	crop.mCropRect.p_integral.y=0;
	crop.mCropRect.s.w=_imgi_w_;
	crop.mCropRect.s.h=_imgi_h_;
	crop.mResizeDst.w=_imgi_w_;
	crop.mResizeDst.h=_imgi_h_;
	crop2.mCropRect.p_fractional.x=0;
	crop2.mCropRect.p_fractional.y=0;
	crop2.mCropRect.p_integral.x=0;
	crop2.mCropRect.p_integral.y=0;
	crop2.mCropRect.s.w=_imgi_w_;
	crop2.mCropRect.s.h=_imgi_h_;
	crop2.mResizeDst.w=_imgi_w_;
	crop2.mResizeDst.h=_imgi_h_;
	crop3.mCropRect.p_fractional.x=0;
	crop3.mCropRect.p_fractional.y=0;
	crop3.mCropRect.p_integral.x=0;
	crop3.mCropRect.p_integral.y=0;
	crop3.mCropRect.s.w=_imgi_w_;
	crop3.mCropRect.s.h=_imgi_h_;
	crop3.mResizeDst.w=_imgi_w_;
	crop3.mResizeDst.h=_imgi_h_;

	frameParams1.mvCropRsInfo.push_back(crop);
    frameParams1.mvCropRsInfo.push_back(crop2);
    frameParams1.mvCropRsInfo.push_back(crop3);

	printf("--- [PORT_WPEI...push crop information done\n]");

	//output dma
	IMEM_BUF_INFO buf_out1;
	buf_out1.size= sizeof(cachi_640_480_NV12);
	mpImemDrv->allocVirtBuf(&buf_out1);
	memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);

    printf("buf_out1: VA(0x%" PRIxPTR "), PA(0x%" PRIxPTR ") size:(%d)\n",buf_out1.virtAddr, buf_out1.phyAddr, buf_out1.size);
	IImageBuffer* outBuffer=NULL;
	MUINT32 bufStridesInBytes_1[3] = {_imgi_w_,_imgi_w_,0};
	PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
	IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_NV12),
												MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 2);
	sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicWPE_420_dst", imgParam_1,portBufInfo_1,true);
	outBuffer = pHeap_1->createImageBuffer();
	outBuffer->incStrong(outBuffer);
	outBuffer->lockBuf("basicWPE_420_dst",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);

	NSCam::NSIoPipe::Output dst;

	dst.mPortID = PORT_WPEO;

	dst.mBuffer=outBuffer;
	dst.mPortID.group=0;
	frameParams1.mvOut.push_back(dst);


	 //VECI Buf
	IMEM_BUF_INFO buf_wpe_veci;
    buf_wpe_veci.size = sizeof(veci_a);

    mpImemDrv->allocVirtBuf(&buf_wpe_veci);
    mpImemDrv->mapPhyAddr(&buf_wpe_veci);
	memcpy( (MUINT8*)(buf_wpe_veci.virtAddr), (MUINT8*)(veci_a), sizeof(veci_a));
	printf("buf_wpe_veci: VA(0x%" PRIxPTR "), PA(0x%" PRIxPTR ") size:(%d)\n",buf_wpe_veci.virtAddr, buf_wpe_veci.phyAddr, buf_wpe_veci.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wpe_veci);

    //VEC2I Buf
	IMEM_BUF_INFO buf_wpe_vec2i;
    buf_wpe_vec2i.size = sizeof(vec2i_a);
	//printf("buf_wpe_vec2i.size:%d\n",buf_wpe_vec2i.size);
	//
    mpImemDrv->allocVirtBuf(&buf_wpe_vec2i);
    mpImemDrv->mapPhyAddr(&buf_wpe_vec2i);
	printf("buf_wpe_vec2i: VA(0x%" PRIxPTR "), PA(0x%" PRIxPTR ") size:(%d)\n",buf_wpe_vec2i.virtAddr, buf_wpe_vec2i.phyAddr, buf_wpe_vec2i.size);
	memcpy( (MUINT8*)(buf_wpe_vec2i.virtAddr), (MUINT8*)(vec2i_a), sizeof(vec2i_a));
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wpe_vec2i);

	 //VEC3I Buf
	IMEM_BUF_INFO buf_wpe_vec3i;
    buf_wpe_vec3i.size = sizeof(vec3i_a);
	printf("buf_wpe_vec3i.size:%d\n",buf_wpe_vec3i.size);
	//
    mpImemDrv->allocVirtBuf(&buf_wpe_vec3i);
    mpImemDrv->mapPhyAddr(&buf_wpe_vec3i);
	printf("buf_wpe_vec3i: VA(0x%" PRIxPTR "), PA(0x%" PRIxPTR ") size:(%d)\n",buf_wpe_vec3i.virtAddr, buf_wpe_vec3i.phyAddr, buf_wpe_vec3i.size);
	memcpy( (MUINT8*)(buf_wpe_vec3i.virtAddr), (MUINT8*)(vec3i_a), sizeof(vec3i_a));
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wpe_vec3i);


	enqueWpeParams1.warp_veci_info.virtAddr = buf_wpe_veci.virtAddr;
	enqueWpeParams1.warp_veci_info.phyAddr= buf_wpe_veci.phyAddr;
	enqueWpeParams1.warp_veci_info.bus_size = NSCam::NSIoPipe::NSWpe::WPE_BUS_SIZE_32_BITS;
	enqueWpeParams1.warp_veci_info.stride = 0x00000A00;
	enqueWpeParams1.warp_veci_info.width = 0x00000280;
	enqueWpeParams1.warp_veci_info.height = 0x00000168;
	enqueWpeParams1.warp_veci_info.veci_v_flip_en = 0;
	enqueWpeParams1.warp_veci_info.addr_offset = 0;




	enqueWpeParams1.warp_vec2i_info.virtAddr = buf_wpe_vec2i.virtAddr;
	enqueWpeParams1.warp_vec2i_info.phyAddr= buf_wpe_vec2i.phyAddr;
	enqueWpeParams1.warp_vec2i_info.bus_size = NSCam::NSIoPipe::NSWpe::WPE_BUS_SIZE_32_BITS;
	enqueWpeParams1.warp_vec2i_info.stride = 0x00000A00;
	enqueWpeParams1.warp_vec2i_info.width = 0x00000280;
	enqueWpeParams1.warp_vec2i_info.height = 0x00000168;
	enqueWpeParams1.warp_vec2i_info.veci_v_flip_en = 0;
	enqueWpeParams1.warp_vec2i_info.addr_offset = 0;


	enqueWpeParams1.warp_vec3i_info.virtAddr = buf_wpe_vec3i.virtAddr;
	enqueWpeParams1.warp_vec3i_info.phyAddr= buf_wpe_vec3i.phyAddr;
	enqueWpeParams1.warp_vec3i_info.bus_size = NSCam::NSIoPipe::NSWpe::WPE_BUS_SIZE_32_BITS;
	enqueWpeParams1.warp_vec3i_info.stride = 0x00000A00;
	enqueWpeParams1.warp_vec3i_info.width = 0x00000280;
	enqueWpeParams1.warp_vec3i_info.height = 0x00000168;
	enqueWpeParams1.warp_vec3i_info.veci_v_flip_en = 0;
	enqueWpeParams1.warp_vec3i_info.addr_offset = 0;

	enqueWpeParams1.wpe_mode = 0;

    enqueWpeParams1.vgen_hmg_mode = 1;

	MCrpRsInfo vgenCropInfo;

	vgenCropInfo.mCropRect.p_integral.x = 0;
	vgenCropInfo.mCropRect.p_integral.y = 0;
	vgenCropInfo.mCropRect.p_fractional.x = 0;
	vgenCropInfo.mCropRect.p_fractional.y = 0;//0x00C16FFE;

	enqueWpeParams1.mwVgenCropInfo.push_back(vgenCropInfo);


	enqueWpeParams1.wpecropinfo.x_start_point = 0;
	enqueWpeParams1.wpecropinfo.y_start_point = 0;
	enqueWpeParams1.wpecropinfo.x_end_point = _imgi_w_ - 1;
	enqueWpeParams1.wpecropinfo.y_end_point = _imgi_h_ - 1;

	NSCam::NSIoPipe::ExtraParam eParams;

	eParams.CmdIdx = NSCam::NSIoPipe::EPIPE_WPE_INFO_CMD;
	eParams.moduleStruct = (void *) &(enqueWpeParams1);

    frameParams1.mvExtraParam.push_back(eParams);


	enqueParams.mpfnCallback = WPECallback_Normal;
	enqueParams.mvFrameParams.push_back(frameParams1);

    printf("####wpei.PA(0x%" PRIxPTR "), wpeo.PA(0x%" PRIxPTR "), veci.PA(0x%" PRIxPTR "), vec2i.PA(0x%" PRIxPTR "), vec3i.PA(0x%" PRIxPTR ")\n",
              enqueParams.mvFrameParams[0].mvIn[0].mBuffer->getBufPA(0), enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufPA(0), buf_wpe_veci.phyAddr, buf_wpe_vec2i.phyAddr , buf_wpe_vec3i.phyAddr);


	printf("--- [basicWPE...push dst done\n]");

	// WPE -----

	printf("#########################################################\n");
	printf("###########WPE Stand alone Mode Start to Test !!!!###########\n");
	printf("#########################################################\n");

	i= 0;
	for(int i=0;i<loop;i++)
	{
		memset((MUINT8*)(frameParams1.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
		//memset((MUINT8*)(enqueParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

		//buffer operation
		//mpImemDrv->cacheFlushAll();
		printf("--- [PORT_WPEI(%d)...flush done\n]", i);

	    g_bWPECallback = MFALSE;
		//enque
		ret=pStream->enque(enqueParams);
		if(!ret)
		{
				printf("---ENTERRRRRRRRR [ 420 Stand alone Mode(%d)..enque fail\n]", i);
		}
		else
		{
				printf("---[Stand alone Mode(%d)..enque done\n]",i);
		}



	   do{
			   usleep(100000);
			   if (MTRUE == g_bWPECallback)
			   {
				   break;
			   }
		   }while(1);


	}

  //free
		  srcBuffer->unlockBuf("basicWPE_420_drc");
		  mpImemDrv->freeVirtBuf(&buf_imgi);
		  outBuffer->unlockBuf("basicWPE_420_dst");
		  mpImemDrv->freeVirtBuf(&buf_out1);


		  mpImemDrv->freeVirtBuf(&buf_wpe_veci);
		  mpImemDrv->freeVirtBuf(&buf_wpe_vec2i);
		  mpImemDrv->freeVirtBuf(&buf_wpe_vec3i);

		  printf("--- [420 Stand alone Mode...free memory done\n]");

		  //
		  pStream->uninit("basicWPE_420", NSCam::NSIoPipe::EStreamPipeID_WarpEG);
		  pStream->destroyInstance();
		  printf("--- [ 420 Stand alone Mode...pStream uninit done\n]");
		  mpImemDrv->uninit();
		  mpImemDrv->destroyInstance();

		  return ret;

}

int wpe_secure_422(int type,int loop)
{
    int ret=0;
	int i;
	NSCam::NSIoPipe::NSWpe::WPEQParams enqueWpeParams;
	NSCam::NSIoPipe::NSWpe::WPEQParams enqueWpeParams1;

	printf("--- [WPE Direct Link Loop(%d)...Enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", loop);
	NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
	pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
	pStream->init("basicWPE", NSCam::NSIoPipe::EStreamPipeID_WarpEG, 1); // Secure tag
	printf("--- [basicWPE...pStream init done]\n");

	DipIMemDrv* mpImemDrv=NULL;
	mpImemDrv=DipIMemDrv::createInstance();
	mpImemDrv->init();

	QParams enqueParams;
	FrameParams frameParams;
	FrameParams frameParams1;

	frameParams.mSecureFra = 1;	// For secure path
		//frame tag
	//FrameParams.mvStreamTag.push_back(NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal);

	//MUINT32 Width = 0xF00, Height = 0x870, Stride = 0x1E00;
	//input image
	MUINT32 _imgi_w_= 0xF00, _imgi_h_= 0x870;

	MINT32 bufBoundaryInBytes[3] = {0, 0, 0};


	IMEM_BUF_INFO buf_imgi;
	buf_imgi.size= _imgi_w_ * _imgi_h_ * 2;

	//mpImemDrv->allocVirtBuf(&buf_imgi);

	//memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(cachi_a), _imgi_w_ * _imgi_h_ * 2);
		//imem buffer 2 image heap
	printf("--- [basicWPE(%d)...flag -1 ]\n", loop);
	printf("buf_imgi: VA(0x%" PRIxPTR "), PA(0x%" PRIxPTR ") size:(%d)\n",buf_imgi.virtAddr, buf_imgi.phyAddr, buf_imgi.size);
	IImageBuffer* srcBuffer;

	MUINT32 bufStridesInBytes[3] = {_imgi_w_ * 2, 0, 0};

	//PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
	IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
	 sp<ISecureImageBufferHeap> pHeap = NULL;
	 	pHeap = ISecureImageBufferHeap::create( "basicSecureWPE", imgParam,
        ISecureImageBufferHeap::AllocExtraParam(0, 1, 0, MFALSE, SecType::mem_secure/*mem_secure*/));

	if (pHeap == NULL) {
        printf("[basicSecureWPE] Stuff WPEI ImageBufferHeap create fail\n");
        return 0;
    }
	srcBuffer = pHeap->createImageBuffer();
	srcBuffer->incStrong(srcBuffer);
	srcBuffer->lockBuf("basicWPE",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);

	NSCam::NSIoPipe::Input src;
	src.mPortID=PORT_WPEI;
	src.mBuffer=srcBuffer;
	src.mPortID.group=0;
	src.mSecureTag = EDIPSecure_SECURE;
	frameParams.mvIn.push_back(src);
	printf("--- [basicWPE...push src done]\n");

	  //crop information
	MCrpRsInfo crop;
	crop.mFrameGroup=0;
	crop.mGroupID=1;
	MCrpRsInfo crop2;
	crop2.mFrameGroup=0;
	crop2.mGroupID=2;
	MCrpRsInfo crop3;
	crop3.mFrameGroup=0;
	crop3.mGroupID=3;
	crop.mCropRect.p_fractional.x=0;
	crop.mCropRect.p_fractional.y=0;
	crop.mCropRect.p_integral.x=0;
	crop.mCropRect.p_integral.y=0;
	crop.mCropRect.s.w=_imgi_w_;
	crop.mCropRect.s.h=_imgi_h_;
	crop.mResizeDst.w=_imgi_w_;
	crop.mResizeDst.h=_imgi_h_;
	crop2.mCropRect.p_fractional.x=0;
	crop2.mCropRect.p_fractional.y=0;
	crop2.mCropRect.p_integral.x=0;
	crop2.mCropRect.p_integral.y=0;
	crop2.mCropRect.s.w=_imgi_w_;
	crop2.mCropRect.s.h=_imgi_h_;
	crop2.mResizeDst.w=_imgi_w_;
	crop2.mResizeDst.h=_imgi_h_;
	crop3.mCropRect.p_fractional.x=0;
	crop3.mCropRect.p_fractional.y=0;
	crop3.mCropRect.p_integral.x=0;
	crop3.mCropRect.p_integral.y=0;
	crop3.mCropRect.s.w=_imgi_w_;
	crop3.mCropRect.s.h=_imgi_h_;
	crop3.mResizeDst.w=_imgi_w_;
	crop3.mResizeDst.h=_imgi_h_;

	frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
	frameParams1.mvCropRsInfo.push_back(crop);
    frameParams1.mvCropRsInfo.push_back(crop2);
    frameParams1.mvCropRsInfo.push_back(crop3);

	printf("--- [PORT_WPEI...push crop information done\n]");

	//output dma
	IMEM_BUF_INFO buf_out1;
	//buf_out1.size=_imgi_w_*_imgi_h_*2;
	//mpImemDrv->allocVirtBuf(&buf_out1);
	//memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);

    printf("buf_out1: VA(0x%" PRIxPTR "), PA(0x%" PRIxPTR ") size:(%d)\n",buf_out1.virtAddr, buf_out1.phyAddr, buf_out1.size);
	IImageBuffer* outBuffer=NULL;
	MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
	//PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
	IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
												MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
	sp<ISecureImageBufferHeap> pHeap_1 = ISecureImageBufferHeap::create( "basicSecureWPE", imgParam_1,
        ISecureImageBufferHeap::AllocExtraParam(0, 1, 0, MFALSE, SecType::mem_secure/*mem_secure*/));

	 if (pHeap_1 == NULL) {
        printf("[basicSecureWPE] Stuff WDMA ImageBufferHeap create fail\n");
        return 0;
    }
	outBuffer = pHeap_1->createImageBuffer();
	outBuffer->incStrong(outBuffer);
	outBuffer->lockBuf("PORT_WPEI",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);

	NSCam::NSIoPipe::Output dst;

	switch(type) {
			case 0:
				dst.mPortID=PORT_WPEO;
				dst.mSecureTag = EDIPSecure_SECURE;
				enqueWpeParams.wpe_mode = 0;
				break;
			case 1:
				dst.mPortID=PORT_WDMAO;
				dst.mSecureTag = EDIPSecure_SECURE;
				enqueWpeParams.wpe_mode = 1;
				break;
			default:
				dst.mPortID=PORT_WPEO;
				dst.mSecureTag = EDIPSecure_SECURE;
				enqueWpeParams.wpe_mode = 0;
				break;
		}

	//dst.mPortID = PORT_WDMAO;//PORT_WPEO;
	printf("[basicSecureWPE] type: %d, dst.mPortID %d\n", type, (MUINT32)dst.mPortID);
	dst.mBuffer=outBuffer;
	dst.mPortID.group=0;
	//dst.mSecureTag = EDIPSecure_SECURE;
	frameParams.mvOut.push_back(dst);


	 //VECI Buf
	IMEM_BUF_INFO buf_wpe_veci;
    buf_wpe_veci.size = sizeof(veci_a);

    mpImemDrv->allocVirtBuf(&buf_wpe_veci);
    mpImemDrv->mapPhyAddr(&buf_wpe_veci);
	memcpy( (MUINT8*)(buf_wpe_veci.virtAddr), (MUINT8*)(veci_a), sizeof(veci_a));
	printf("buf_wpe_veci: VA(0x%" PRIxPTR "), PA(0x%" PRIxPTR ") size:(%d)\n",buf_wpe_veci.virtAddr, buf_wpe_veci.phyAddr, buf_wpe_veci.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wpe_veci);

    //VEC2I Buf
	IMEM_BUF_INFO buf_wpe_vec2i;
    buf_wpe_vec2i.size = sizeof(vec2i_a);
	//printf("buf_wpe_vec2i.size:%d\n",buf_wpe_vec2i.size);
	//
    mpImemDrv->allocVirtBuf(&buf_wpe_vec2i);
    mpImemDrv->mapPhyAddr(&buf_wpe_vec2i);
	printf("buf_wpe_vec2i: VA(0x%" PRIxPTR "), PA(0x%" PRIxPTR ") size:(%d)\n",buf_wpe_vec2i.virtAddr, buf_wpe_vec2i.phyAddr, buf_wpe_vec2i.size);
	memcpy( (MUINT8*)(buf_wpe_vec2i.virtAddr), (MUINT8*)(vec2i_a), sizeof(vec2i_a));
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wpe_vec2i);

	 //VEC3I Buf
	IMEM_BUF_INFO buf_wpe_vec3i;
    buf_wpe_vec3i.size = sizeof(vec3i_a);
	printf("buf_wpe_vec3i.size:%d\n",buf_wpe_vec3i.size);
	//
    mpImemDrv->allocVirtBuf(&buf_wpe_vec3i);
    mpImemDrv->mapPhyAddr(&buf_wpe_vec3i);
	printf("buf_wpe_vec3i: VA(0x%" PRIxPTR "), PA(0x%" PRIxPTR ") size:(%d)\n",buf_wpe_vec3i.virtAddr, buf_wpe_vec3i.phyAddr, buf_wpe_vec3i.size);
	memcpy( (MUINT8*)(buf_wpe_vec3i.virtAddr), (MUINT8*)(vec3i_a), sizeof(vec3i_a));
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wpe_vec3i);


	enqueWpeParams.warp_veci_info.virtAddr = buf_wpe_veci.virtAddr;
	enqueWpeParams.warp_veci_info.phyAddr= buf_wpe_veci.phyAddr;
	enqueWpeParams.warp_veci_info.bus_size = NSCam::NSIoPipe::NSWpe::WPE_BUS_SIZE_32_BITS;
	enqueWpeParams.warp_veci_info.stride = 0x00000A00;
	enqueWpeParams.warp_veci_info.width = 0x00000280;
	enqueWpeParams.warp_veci_info.height = 0x00000168;
	enqueWpeParams.warp_veci_info.veci_v_flip_en = 0;
	enqueWpeParams.warp_veci_info.addr_offset = 0;




	enqueWpeParams.warp_vec2i_info.virtAddr = buf_wpe_vec2i.virtAddr;
	enqueWpeParams.warp_vec2i_info.phyAddr= buf_wpe_vec2i.phyAddr;
	enqueWpeParams.warp_vec2i_info.bus_size = NSCam::NSIoPipe::NSWpe::WPE_BUS_SIZE_32_BITS;
	enqueWpeParams.warp_vec2i_info.stride = 0x00000A00;
	enqueWpeParams.warp_vec2i_info.width = 0x00000280;
	enqueWpeParams.warp_vec2i_info.height = 0x00000168;
	enqueWpeParams.warp_vec2i_info.veci_v_flip_en = 0;
	enqueWpeParams.warp_vec2i_info.addr_offset = 0;


	enqueWpeParams.warp_vec3i_info.virtAddr = buf_wpe_vec3i.virtAddr;
	enqueWpeParams.warp_vec3i_info.phyAddr= buf_wpe_vec3i.phyAddr;
	enqueWpeParams.warp_vec3i_info.bus_size = NSCam::NSIoPipe::NSWpe::WPE_BUS_SIZE_32_BITS;
	enqueWpeParams.warp_vec3i_info.stride = 0x00000A00;
	enqueWpeParams.warp_vec3i_info.width = 0x00000280;
	enqueWpeParams.warp_vec3i_info.height = 0x00000168;
	enqueWpeParams.warp_vec3i_info.veci_v_flip_en = 0;
	enqueWpeParams.warp_vec3i_info.addr_offset = 0;



    enqueWpeParams.vgen_hmg_mode = 1;

	enqueWpeParams1.warp_veci_info.virtAddr = buf_wpe_veci.virtAddr;
	enqueWpeParams1.warp_veci_info.phyAddr= buf_wpe_veci.phyAddr;
	enqueWpeParams1.warp_veci_info.bus_size = NSCam::NSIoPipe::NSWpe::WPE_BUS_SIZE_32_BITS;
	enqueWpeParams1.warp_veci_info.stride = 0x00000A00;
	enqueWpeParams1.warp_veci_info.width = 0x00000280;
	enqueWpeParams1.warp_veci_info.height = 0x00000168;
	enqueWpeParams1.warp_veci_info.veci_v_flip_en = 0;
	enqueWpeParams1.warp_veci_info.addr_offset = 0;




	enqueWpeParams1.warp_vec2i_info.virtAddr = buf_wpe_vec2i.virtAddr;
	enqueWpeParams1.warp_vec2i_info.phyAddr= buf_wpe_vec2i.phyAddr;
	enqueWpeParams1.warp_vec2i_info.bus_size = NSCam::NSIoPipe::NSWpe::WPE_BUS_SIZE_32_BITS;
	enqueWpeParams1.warp_vec2i_info.stride = 0x00000A00;
	enqueWpeParams1.warp_vec2i_info.width = 0x00000280;
	enqueWpeParams1.warp_vec2i_info.height = 0x00000168;
	enqueWpeParams1.warp_vec2i_info.veci_v_flip_en = 0;
	enqueWpeParams1.warp_vec2i_info.addr_offset = 0;


	enqueWpeParams1.warp_vec3i_info.virtAddr = buf_wpe_vec3i.virtAddr;
	enqueWpeParams1.warp_vec3i_info.phyAddr= buf_wpe_vec3i.phyAddr;
	enqueWpeParams1.warp_vec3i_info.bus_size = NSCam::NSIoPipe::NSWpe::WPE_BUS_SIZE_32_BITS;
	enqueWpeParams1.warp_vec3i_info.stride = 0x00000A00;
	enqueWpeParams1.warp_vec3i_info.width = 0x00000280;
	enqueWpeParams1.warp_vec3i_info.height = 0x00000168;
	enqueWpeParams1.warp_vec3i_info.veci_v_flip_en = 0;
	enqueWpeParams1.warp_vec3i_info.addr_offset = 0;

	enqueWpeParams1.wpe_mode = 0;

    enqueWpeParams1.vgen_hmg_mode = 1;

	MCrpRsInfo vgenCropInfo;

	vgenCropInfo.mCropRect.p_integral.x = 0;
	vgenCropInfo.mCropRect.p_integral.y = 0;
	vgenCropInfo.mCropRect.p_fractional.x = 0;
	vgenCropInfo.mCropRect.p_fractional.y = 0;//0x00C16FFE;

	enqueWpeParams.mwVgenCropInfo.push_back(vgenCropInfo);
	enqueWpeParams1.mwVgenCropInfo.push_back(vgenCropInfo);


	enqueWpeParams.wpecropinfo.x_start_point = 0;
	enqueWpeParams.wpecropinfo.y_start_point = 0;
	enqueWpeParams.wpecropinfo.x_end_point = _imgi_w_ - 1;
	enqueWpeParams.wpecropinfo.y_end_point = _imgi_h_ - 1;

	NSCam::NSIoPipe::ExtraParam eParams;

	eParams.CmdIdx = NSCam::NSIoPipe::EPIPE_WPE_INFO_CMD;
	eParams.moduleStruct = (void *) &(enqueWpeParams);

    frameParams.mvExtraParam.push_back(eParams);

	enqueWpeParams1.wpecropinfo.x_start_point = 0;
	enqueWpeParams1.wpecropinfo.y_start_point = 0;
	enqueWpeParams1.wpecropinfo.x_end_point = _imgi_w_ - 1;
	enqueWpeParams1.wpecropinfo.y_end_point = _imgi_h_ - 1;

	NSCam::NSIoPipe::ExtraParam eParams1;

	eParams1.CmdIdx = NSCam::NSIoPipe::EPIPE_WPE_INFO_CMD;
	eParams1.moduleStruct = (void *) &(enqueWpeParams1);

    frameParams1.mvExtraParam.push_back(eParams1);


	enqueParams.mpfnCallback = WPECallback_Normal;
	enqueParams.mvFrameParams.push_back(frameParams);
   // enqueParams.mvFrameParams.push_back(frameParams);

    printf("####wpei.PA(0x%" PRIxPTR "), wpeo.PA(0x%" PRIxPTR "), veci.PA(0x%" PRIxPTR "), vec2i.PA(0x%" PRIxPTR "), vec3i.PA(0x%" PRIxPTR ")\n",
              enqueParams.mvFrameParams[0].mvIn[0].mBuffer->getBufPA(0), enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufPA(0), buf_wpe_veci.phyAddr, buf_wpe_vec2i.phyAddr , buf_wpe_vec3i.phyAddr);


	printf("--- [basicWPE...push dst done\n]");
    mpImemDrv->initSecureM4U();
	// WPE -----
	if (type ==1)
    {
	    printf("#########################################################\n");
	    printf("##### Secure WPE Direct Link  Mode Start to Test !!!#####\n");
	    printf("#########################################################\n");
	}
	else
	{
        printf("#########################################################\n");
	    printf("##### Secure WPE Stand Alone Mode Start to Test !!!!!####\n");
	    printf("#########################################################\n");
	}

	i= 0;
	for(int i=0;i<loop;i++)
	{
		//memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
		//memset((MUINT8*)(enqueParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

		//buffer operation
		//mpImemDrv->cacheFlushAll();
		printf("--- [Direct Link Loop(%d)...flush done\n]", i);
	    g_bWPECallback = MFALSE;
		//enque
		ret=pStream->enque(enqueParams);
		if(!ret)
		{
				printf("---ERRRRRRRRR [Loop(%d)..enque fail\n]", i);
		}
		else
		{
				printf("---[Loop(%d)..enque done\n]",i);
		}

        do{
			   usleep(100000);
			   if (MTRUE == g_bWPECallback)
			   {
				   break;
			   }
		}while(1);

	}

  //free
		  srcBuffer->unlockBuf("basicWPE");
		  //mpImemDrv->freeVirtBuf(&buf_imgi);
		  outBuffer->unlockBuf("basicWPE");
		  //mpImemDrv->freeVirtBuf(&buf_out1);


		  mpImemDrv->freeVirtBuf(&buf_wpe_veci);
		  mpImemDrv->freeVirtBuf(&buf_wpe_vec2i);
		  mpImemDrv->freeVirtBuf(&buf_wpe_vec3i);
		  printf("--- [Direct Link ...free memory done\n]");

		  //
		  pStream->uninit("basicWPE", NSCam::NSIoPipe::EStreamPipeID_WarpEG);
		  pStream->destroyInstance();
		  printf("--- [Direct Link ...pStream uninit done\n]");
		  mpImemDrv->uninit();
		  mpImemDrv->destroyInstance();

		  return ret;

}


/******************************************************************************
* save the buffer to the file
*******************************************************************************/
static bool
saveBufToFile(char const*const fname, MUINT8 *const buf, MUINT32 const size)  __attribute__((unused));
static bool
saveBufToFile(char const*const fname, MUINT8 *const buf, MUINT32 const size)
{
    int nw, cnt = 0;
    uint32_t written = 0;

    //LOG_INF("(name, buf, size) = (%s, %x, %d)", fname, buf, size);
    //LOG_INF("opening file [%s]\n", fname);
    int fd = ::open(fname, O_RDWR | O_CREAT, S_IRWXU);
    if (fd < 0) {
        printf(": failed to create file [%s]: %s \n", fname, ::strerror(errno));
        return false;
    }

    //LOG_INF("writing %d bytes to file [%s]\n", size, fname);
    while (written < size) {
        nw = ::write(fd,
                     buf + written,
                     size - written);
        if (nw < 0) {
            printf(": failed to write to file [%s]: %s\n", fname, ::strerror(errno));
            break;
        }
        written += nw;
        cnt++;
    }
    //LOG_INF("done writing %d bytes to file [%s] in %d passes\n", size, fname, cnt);
    ::close(fd);
    return true;
}



