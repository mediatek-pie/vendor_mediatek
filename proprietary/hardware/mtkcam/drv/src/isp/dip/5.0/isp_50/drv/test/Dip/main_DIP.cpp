#include <vector>
#include <list>
#include <stdio.h>
#include <stdlib.h>
//
#include <errno.h>
#include <fcntl.h>


//#include "isp_drv_cam.h"
#include "isp_drv_dip.h"
#include "isp_drv_dip_phy.h"
#include "isp_drv_dip_platform.h"
#include "Dip/project.h"
#include <imem_drv.h>

#undef LOG_TAG
#define LOG_TAG "Test_IspDrv_DIP"
#define LOG_DMA_INFO MTRUE
#define WHILE_ONE_TEST MTRUE

#define LOG_VRB(fmt, arg...)        printf("[%s]" fmt "\n", __func__, ##arg)
#define LOG_DBG(fmt, arg...)        printf("[%s]" fmt "\n", __func__, ##arg)
#define LOG_INF(fmt, arg...)        printf("[%s]" fmt "\n", __func__, ##arg)
#define LOG_WRN(fmt, arg...)        printf("[%s]" fmt "\n", __func__, ##arg)
#define LOG_ERR(fmt, arg...)        printf("error:[%s]" fmt "\n", __func__, ##arg)

enum {
    _DMAI_TBL_IMGI = 0,
    _DMAI_TBL_TDRI,
    _DMAI_TBL_UFDI,
    _DMAI_TBL_VIPI,
    _DMAI_TBL_VIP2I,
    _DMAI_TBL_LCEI,
    _DMAI_TBL_IMG2O,
    _DMAI_TBL_IMG2BO,
    _DMAI_TBL_IMG3O,
    _DMAI_TBL_IMG3BO,
    _DMAI_TBL_FEO,
    _DMAI_TBL_NUM,
};

struct TestInputInfo {
    struct DmaiTableInfo {
        const unsigned char     *pTblAddr;
        MUINT32                 tblLength;
    };

    IMEM_BUF_INFO           ****pImemBufs;
    DmaiTableInfo           DmaiTbls[1][_DMAI_TBL_NUM];

    TestInputInfo()
        {
            int i = 0, cam = 0;

            pImemBufs = NULL;
            for (cam = 0; cam < 1; cam++) {
                for (i = 0; i < _DMAI_TBL_NUM; i++) {
                    DmaiTbls[cam][i].pTblAddr = NULL;
                    DmaiTbls[cam][i].tblLength = 0;
                }
            }
        }
};

typedef struct {
	unsigned int LarbNum;
	unsigned int regOffset;;
	unsigned int regVal;;
} LARB_MMU_STRUCT;

//MUINT32 PhyDipDrv::readReg(MUINT32 Addr,MINT32 caller)
//{
 //   return this->m_pIspDrvImp->readReg(Addr, caller);
//}

MINT32 MMU_INIT(MUINT32* _ptr, MUINT32* _ptrphy, MUINTPTR para)
{
	(void)_ptr; (void)_ptrphy; (void)para;

/*
	if (ptrphy) {
		DIP_LARB_MMU_STRUCT larbInfo;
        int idx = 0;

        //LOG_INF("Config MMU Larb to PA at DIP A");

        larbInfo.regVal = 0;

        larbInfo.LarbNum = 2;
        //LOG_INF("config larb=%d offset=0x%x", larbInfo.LarbNum, larbInfo.regOffset);
        for (idx = 0; idx < 3; idx++) {
			larbInfo.regOffset = 0x380 + (idx << 2);
        	ptrphy->setDeviceInfo(_SET_LARB_MMU, (MUINT8 *)&larbInfo);

            larbInfo.regOffset = 0xf80 + (idx << 2);
            ptrphy->setDeviceInfo(_SET_LARB_MMU, (MUINT8 *)&larbInfo);
        }

        larbInfo.LarbNum = 3;
        for (idx = 0; idx < 5; idx++) {
			larbInfo.regOffset = 0x380 + (idx << 2);
            ptrphy->setDeviceInfo(_SET_LARB_MMU, (MUINT8 *)&larbInfo);

            larbInfo.regOffset = 0xf80 + (idx << 2);
            ptrphy->setDeviceInfo(_SET_LARB_MMU, (MUINT8 *)&larbInfo);
        }

        larbInfo.LarbNum = 5;
            //LOG_INF("config larb=%d offset=0x%x", larbInfo.LarbNum, larbInfo.regOffset);
        for (idx = 0; idx < 54; idx++) {
            larbInfo.regOffset = 0x380 + (idx << 2);
            ptrphy->setDeviceInfo(_SET_LARB_MMU, (MUINT8 *)&larbInfo);

            larbInfo.regOffset = 0xf80 + (idx << 2);
            ptrphy->setDeviceInfo(_SET_LARB_MMU, (MUINT8 *)&larbInfo);
        }

        larbInfo.LarbNum = 6;
            //LOG_INF("config larb=%d offset=0x%x", larbInfo.LarbNum, larbInfo.regOffset);
        for (idx = 0; idx < 19; idx++) {
            larbInfo.regOffset = 0x380 + (idx << 2);
            ptrphy->setDeviceInfo(_SET_LARB_MMU, (MUINT8 *)&larbInfo);

            larbInfo.regOffset = 0xf80 + (idx << 2);
            ptrphy->setDeviceInfo(_SET_LARB_MMU, (MUINT8 *)&larbInfo);

		}

    }
*/
    //ptrphy->setDeviceInfo(_SET_RESET_HW_MOD, NULL);
    return 0;
}

MINT32 Pattern_Start_1(MUINT32* _ptr, MUINT32* _ptrphy, MUINTPTR inputInfo)
{
    (void)_ptr;
    UINT32 i, reg, reg2, DMA_EN = 0;
    PhyDipDrv* pDrvDipPhy = (PhyDipDrv*)_ptrphy;
    IMEM_BUF_INFO**** pBuf;
    DipIMemDrv* pImemDrv = NULL;
    TestInputInfo *pInputInfo = (TestInputInfo *)inputInfo;
	dip_x_reg_t dipReg;

    pBuf = (IMEM_BUF_INFO****)pInputInfo->pImemBufs;


    pImemDrv = DipIMemDrv::createInstance();
    if(pImemDrv->init() < 0){
        LOG_ERR(" imem init fail\n");
        return 1;
    }
            
    DMA_EN = DIP_READ_PHY_REG(pDrvDipPhy, DIP_X_CTL_DMA_EN);
    LOG_INF("module_DIP:  enabled DMA:0x%x", DMA_EN);

    //getchar();

    
    if(DMA_EN & DIP_X_REG_CTL_DMA_EN_IMGI){
		if (pInputInfo->DmaiTbls[0][_DMAI_TBL_IMGI].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_IMGI].tblLength) {
			MUINT32 tmp = (DMA_EN & DIP_X_REG_CTL_DMA_EN_IMGI), cnt = 0;
			while (tmp != 0) {
				cnt++;
				tmp >>= 1;
			}

            LOG_INF("DIP_X_REG_CTL_DMA_EN_IMGI:  cnt:0x%x\n", cnt);

            pBuf[0][cnt][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_IMGI].tblLength;

			if (pImemDrv->allocVirtBuf(pBuf[0][cnt][0]) < 0) {
				LOG_ERR(" imem alloc fail at %s\n", "IMGI");
				return 1;
		    }
			if (pImemDrv->mapPhyAddr(pBuf[0][cnt][0]) < 0) {
				LOG_ERR(" imem map fail at %s\n", "IMGI");
				return 1;
			}

			DIP_WRITE_PHY_REG(pDrvDipPhy, DIP_X_IMGI_BASE_ADDR, pBuf[0][cnt][0]->phyAddr);

            #if LOG_DMA_INFO
			LOG_INF("IMGI srcTable=%p size=%d pa=%p, va=%p\n",
					pInputInfo->DmaiTbls[0][_DMAI_TBL_IMGI].pTblAddr,
					pInputInfo->DmaiTbls[0][_DMAI_TBL_IMGI].tblLength,
					(MUINT32 *)pBuf[0][cnt][0]->phyAddr, (MUINT32 *)pBuf[0][cnt][0]->virtAddr);
			LOG_INF("IMGI xsize=0x%x ysize=0x%x stride=0x%x\n",
					DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_IMGI_XSIZE, XSIZE),
					DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_IMGI_YSIZE, YSIZE),
					DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_IMGI_STRIDE, STRIDE));
            #endif

		    memcpy((MUINT8*)pBuf[0][cnt][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_IMGI].pTblAddr,
						   pInputInfo->DmaiTbls[0][_DMAI_TBL_IMGI].tblLength);
	   }
    }

    if(DMA_EN & DIP_X_REG_CTL_DMA_EN_VIPI){
        if (pInputInfo->DmaiTbls[0][_DMAI_TBL_VIPI].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_VIPI].tblLength) {
            MUINT32 tmp = (DMA_EN & DIP_X_REG_CTL_DMA_EN_VIPI), cnt = 0;
            while (tmp != 0) {
                cnt++;
                tmp >>= 1;
            }

            LOG_INF("DIP_X_REG_CTL_DMA_EN_VIPI:  cnt:0x%x\n", cnt);

            pBuf[0][cnt][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_VIPI].tblLength;

            if (pImemDrv->allocVirtBuf(pBuf[0][cnt][0]) < 0) {
                LOG_ERR(" imem alloc fail at %s\n", "VIPI");
                return 1;
            }
            if (pImemDrv->mapPhyAddr(pBuf[0][cnt][0]) < 0) {
                LOG_ERR(" imem map fail at %s\n", "VIPI");
                return 1;
            }

            DIP_WRITE_PHY_REG(pDrvDipPhy, DIP_X_VIPI_BASE_ADDR, pBuf[0][cnt][0]->phyAddr);

            #if LOG_DMA_INFO
            LOG_INF("VIPI srcTable=%p size=%d pa=%p, va=%p\n",
                    pInputInfo->DmaiTbls[0][_DMAI_TBL_VIPI].pTblAddr,
                    pInputInfo->DmaiTbls[0][_DMAI_TBL_VIPI].tblLength,
                    (MUINT32 *)pBuf[0][cnt][0]->phyAddr, (MUINT32 *)pBuf[0][cnt][0]->virtAddr);
            LOG_INF("VIPI xsize=0x%x ysize=0x%x stride=0x%x\n",
                    DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_VIPI_XSIZE, XSIZE),
                    DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_VIPI_YSIZE, YSIZE),
                    DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_VIPI_STRIDE, STRIDE));
            #endif

            memcpy((MUINT8*)pBuf[0][cnt][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_VIPI].pTblAddr,
                           pInputInfo->DmaiTbls[0][_DMAI_TBL_VIPI].tblLength);
       }
    }

    if(DMA_EN & DIP_X_REG_CTL_DMA_EN_VIP2I){
        if (pInputInfo->DmaiTbls[0][_DMAI_TBL_VIP2I].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_VIP2I].tblLength) {
            MUINT32 tmp = (DMA_EN & DIP_X_REG_CTL_DMA_EN_VIP2I), cnt = 0;
            while (tmp != 0) {
                cnt++;
                tmp >>= 1;
            }

            LOG_INF("DIP_X_REG_CTL_DMA_EN_VIP2I:  cnt:0x%x\n", cnt);

            pBuf[0][cnt][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_VIP2I].tblLength;

            if (pImemDrv->allocVirtBuf(pBuf[0][cnt][0]) < 0) {
                LOG_ERR(" imem alloc fail at %s\n", "VIP2I");
                return 1;
            }
            if (pImemDrv->mapPhyAddr(pBuf[0][cnt][0]) < 0) {
                LOG_ERR(" imem map fail at %s\n", "VIP2I");
                return 1;
            }

            DIP_WRITE_PHY_REG(pDrvDipPhy, DIP_X_VIP2I_BASE_ADDR, pBuf[0][cnt][0]->phyAddr);

            #if LOG_DMA_INFO
            LOG_INF("VIP2I srcTable=%p size=%d pa=%p, va=%p\n",
                    pInputInfo->DmaiTbls[0][_DMAI_TBL_VIP2I].pTblAddr,
                    pInputInfo->DmaiTbls[0][_DMAI_TBL_VIP2I].tblLength,
                    (MUINT32 *)pBuf[0][cnt][0]->phyAddr, (MUINT32 *)pBuf[0][cnt][0]->virtAddr);
            LOG_INF("VIP2I xsize=0x%x ysize=0x%x stride=0x%x\n",
                    DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_VIP2I_XSIZE, XSIZE),
                    DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_VIP2I_YSIZE, YSIZE),
                    DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_VIP2I_STRIDE, STRIDE));
            #endif

            memcpy((MUINT8*)pBuf[0][cnt][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_VIP2I].pTblAddr,
                           pInputInfo->DmaiTbls[0][_DMAI_TBL_VIP2I].tblLength);
       }
    }

    if(DMA_EN & DIP_X_REG_CTL_DMA_EN_LCEI){
        if (pInputInfo->DmaiTbls[0][_DMAI_TBL_LCEI].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_LCEI].tblLength) {
            MUINT32 tmp = (DMA_EN & DIP_X_REG_CTL_DMA_EN_LCEI), cnt = 0;
            while (tmp != 0) {
                cnt++;
                tmp >>= 1;
            }

            LOG_INF("DIP_X_REG_CTL_DMA_EN_LCEI:  cnt:0x%x\n", cnt);

            pBuf[0][cnt][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_LCEI].tblLength;

            if (pImemDrv->allocVirtBuf(pBuf[0][cnt][0]) < 0) {
                LOG_ERR(" imem alloc fail at %s\n", "LCEI");
                return 1;
            }
            if (pImemDrv->mapPhyAddr(pBuf[0][cnt][0]) < 0) {
                LOG_ERR(" imem map fail at %s\n", "LCEI");
                return 1;
            }

            DIP_WRITE_PHY_REG(pDrvDipPhy, DIP_X_LCEI_BASE_ADDR, pBuf[0][cnt][0]->phyAddr);

            #if LOG_DMA_INFO
            LOG_INF("LCEI srcTable=%p size=%d pa=%p, va=%p\n",
                    pInputInfo->DmaiTbls[0][_DMAI_TBL_LCEI].pTblAddr,
                    pInputInfo->DmaiTbls[0][_DMAI_TBL_LCEI].tblLength,
                    (MUINT32 *)pBuf[0][cnt][0]->phyAddr, (MUINT32 *)pBuf[0][cnt][0]->virtAddr);
            LOG_INF("LCEI xsize=0x%x ysize=0x%x stride=0x%x\n",
                    DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_LCEI_XSIZE, XSIZE),
                    DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_LCEI_YSIZE, YSIZE),
                    DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_LCEI_STRIDE, STRIDE));
            #endif

            memcpy((MUINT8*)pBuf[0][cnt][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_LCEI].pTblAddr,
                           pInputInfo->DmaiTbls[0][_DMAI_TBL_LCEI].tblLength);
       }
    }

    if(0){
        if (pInputInfo->DmaiTbls[0][_DMAI_TBL_TDRI].pTblAddr && pInputInfo->DmaiTbls[0][_DMAI_TBL_TDRI].tblLength) {
            /*
            MUINT32 tmp = (DMA_EN & DIP_X_REG_CTL_DMA_EN_TDRI), cnt = 0;
            while (tmp != 0) {
                cnt++;
                tmp >>= 1;
            }
            */
            MINT32 cnt = 31;

            LOG_INF("DIP_X_REG_CTL_DMA_EN_TDRI:  cnt:0x%x\n", cnt);

            pBuf[0][cnt][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_TDRI].tblLength;

            if (pImemDrv->allocVirtBuf(pBuf[0][cnt][0]) < 0) {
                LOG_ERR(" imem alloc fail at %s\n", "TDRI");
                return 1;
            }
            if (pImemDrv->mapPhyAddr(pBuf[0][cnt][0]) < 0) {
                LOG_ERR(" imem map fail at %s\n", "TDRI");
                return 1;
            }

            DIP_WRITE_PHY_REG(pDrvDipPhy, DIP_X_TDRI_BASE_ADDR, pBuf[0][cnt][0]->phyAddr);

            #if LOG_DMA_INFO
            LOG_INF("TDRI srcTable=%p size=%d pa=%p, va=%p\n",
                    pInputInfo->DmaiTbls[0][_DMAI_TBL_TDRI].pTblAddr,
                    pInputInfo->DmaiTbls[0][_DMAI_TBL_TDRI].tblLength,
                    (MUINT32 *)pBuf[0][cnt][0]->phyAddr, (MUINT32 *)pBuf[0][cnt][0]->virtAddr);
            /*
            LOG_INF("TDRI xsize=0x%x ysize=0x%x stride=0x%x\n",
                    DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_TDRI_XSIZE, XSIZE),
                    DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_TDRI_YSIZE, YSIZE),
                    DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_TDRI_STRIDE, STRIDE));
            */
            #endif

            memcpy((MUINT8*)pBuf[0][cnt][0]->virtAddr, (MUINT8*)pInputInfo->DmaiTbls[0][_DMAI_TBL_TDRI].pTblAddr,
                           pInputInfo->DmaiTbls[0][_DMAI_TBL_TDRI].tblLength);
       }
    }

    if(DMA_EN & DIP_X_REG_CTL_DMA_EN_IMG2O){
        MUINT32 tmp = (DMA_EN & DIP_X_REG_CTL_DMA_EN_IMG2O);
		MUINT32 cnt = 0;
        while(tmp != 0){
            cnt++;
            tmp >>= 1;
        }

		LOG_INF("DIP_X_REG_CTL_DMA_EN_IMG2O:  cnt:0x%x\n", cnt);

		reg = DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_IMG2O_YSIZE, YSIZE);
		reg2 = DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_IMG2O_STRIDE, STRIDE);

        LOG_INF("IMG2O buffer :  Y size + 1:0x%x ; Stride:0x%x ; szie:0x%x \n", reg +1, reg2, ( reg + 1) * reg2);

        //pBuf[0][cnt][0]->size = (reg + 1) * reg2 ; 
        pBuf[0][cnt][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_IMG2O].tblLength;

        if (pImemDrv->allocVirtBuf(pBuf[0][cnt][0]) < 0) {       
            LOG_ERR(" imem alloc fail at IMG2O\n");   
            return 1;                                       
        }  

		if (pImemDrv->mapPhyAddr(pBuf[0][cnt][0]) < 0) {         
            LOG_ERR(" imem map fail at IMG2O\n");     
            return 1;                                       
        }    

		memset((MUINT8*)pBuf[0][cnt][0]->virtAddr,0x0,pBuf[0][cnt][0]->size);

        #if LOG_DMA_INFO
        LOG_INF("IMG2O PA:0x%x\n", (unsigned int)pBuf[0][cnt][0]->phyAddr);       
        #endif
        
        DIP_WRITE_PHY_REG(pDrvDipPhy, DIP_X_IMG2O_BASE_ADDR, pBuf[0][cnt][0]->phyAddr);   
        
    }  

    if(DMA_EN & DIP_X_REG_CTL_DMA_EN_IMG2BO){
        MUINT32 tmp = (DMA_EN & DIP_X_REG_CTL_DMA_EN_IMG2BO);
		MUINT32 cnt = 0; 
        while(tmp != 0){        
            cnt++;              
            tmp >>= 1;          
        }  
        
		LOG_INF("DIP_X_REG_CTL_DMA_EN_IMG2BO:  cnt:0x%x\n", cnt);
		
		reg = DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_IMG2BO_YSIZE, YSIZE);
		reg2 = DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_IMG2BO_STRIDE, STRIDE);

        LOG_INF("IMG2BO buffer :  Y size + 1:0x%x ; Stride:0x%x ; szie:0x%x \n", reg +1, reg2, ( reg + 1) * reg2);

        //pBuf[0][cnt][0]->size = (reg + 1) * reg2 ; 
        pBuf[0][cnt][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_IMG2BO].tblLength;
		
        if (pImemDrv->allocVirtBuf(pBuf[0][cnt][0]) < 0) {       
            LOG_ERR(" imem alloc fail at IMG2BO\n");   
            return 1;                                       
        }  

		if (pImemDrv->mapPhyAddr(pBuf[0][cnt][0]) < 0) {         
            LOG_ERR(" imem map fail at IMG2BO\n");     
            return 1;                                       
        }    

		memset((MUINT8*)pBuf[0][cnt][0]->virtAddr,0x0,pBuf[0][cnt][0]->size);
        
        #if LOG_DMA_INFO
        LOG_INF("IMG2BO PA:0x%x\n", (unsigned int)pBuf[0][cnt][0]->phyAddr);       
        #endif
        
        DIP_WRITE_PHY_REG(pDrvDipPhy, DIP_X_IMG2BO_BASE_ADDR, pBuf[0][cnt][0]->phyAddr);   
        
    }  

    if(DMA_EN & DIP_X_REG_CTL_DMA_EN_IMG3O){
        MUINT32 tmp = (DMA_EN & DIP_X_REG_CTL_DMA_EN_IMG3O);
		MUINT32 cnt = 0; 
        while(tmp != 0){        
            cnt++;              
            tmp >>= 1;          
        }  
        
		LOG_INF("DIP_X_REG_CTL_DMA_EN_IMG3O:  cnt:0x%x\n", cnt);
		
		reg = DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_IMG3O_YSIZE, YSIZE);
		reg2 = DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_IMG3O_STRIDE, STRIDE);

        LOG_INF("IMG3O buffer :  Y size + 1:0x%x ; Stride:0x%x ; szie:0x%x \n", reg +1, reg2, ( reg + 1) * reg2);

        //pBuf[0][cnt][0]->size = (reg + 1) * reg2 ; 
		pBuf[0][cnt][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_IMG3O].tblLength;
        
        if (pImemDrv->allocVirtBuf(pBuf[0][cnt][0]) < 0) {       
            LOG_ERR(" imem alloc fail at IMG3O\n");   
            return 1;                                       
        }  

		if (pImemDrv->mapPhyAddr(pBuf[0][cnt][0]) < 0) {         
            LOG_ERR(" imem map fail at IMG3O\n");     
            return 1;                                       
        }    

		memset((MUINT8*)pBuf[0][cnt][0]->virtAddr,0x0,pBuf[0][cnt][0]->size);

        #if LOG_DMA_INFO
        LOG_INF("IMG3O PA:0x%x\n", (unsigned int)pBuf[0][cnt][0]->phyAddr);       
        #endif
        
        DIP_WRITE_PHY_REG(pDrvDipPhy, DIP_X_IMG3O_BASE_ADDR, pBuf[0][cnt][0]->phyAddr);   
        
    }  

    if(DMA_EN & DIP_X_REG_CTL_DMA_EN_IMG3BO){
        MUINT32 tmp = (DMA_EN & DIP_X_REG_CTL_DMA_EN_IMG3BO);
		MUINT32 cnt = 0; 
        while(tmp != 0){        
            cnt++;              
            tmp >>= 1;          
        }  
        
		LOG_INF("DIP_X_REG_CTL_DMA_EN_IMG3BO:  cnt:0x%x\n", cnt);
		
		reg = DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_IMG3BO_YSIZE, YSIZE);
		reg2 = DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_IMG3BO_STRIDE, STRIDE);

        LOG_INF("IMG3BO buffer :  Y size + 1:0x%x ; Stride:0x%x ; szie:0x%x \n", reg +1, reg2, ( reg + 1) * reg2);

        //pBuf[0][cnt][0]->size = (reg + 1) * reg2 ; 
        pBuf[0][cnt][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_IMG3BO].tblLength;
		
        if (pImemDrv->allocVirtBuf(pBuf[0][cnt][0]) < 0) {       
            LOG_ERR(" imem alloc fail at IMG3BO\n");   
            return 1;                                       
        }  

		if (pImemDrv->mapPhyAddr(pBuf[0][cnt][0]) < 0) {         
            LOG_ERR(" imem map fail at IMG3BO\n");     
            return 1;                                       
        }    

		memset((MUINT8*)pBuf[0][cnt][0]->virtAddr,0x0,pBuf[0][cnt][0]->size);

        #if LOG_DMA_INFO
        LOG_INF("IMG3BO PA:0x%x\n", (unsigned int)pBuf[0][cnt][0]->phyAddr);       
        #endif
        
        DIP_WRITE_PHY_REG(pDrvDipPhy, DIP_X_IMG3BO_BASE_ADDR, pBuf[0][cnt][0]->phyAddr);   
        
    } 

    if(DMA_EN & DIP_X_REG_CTL_DMA_EN_FEO){
        MUINT32 tmp = (DMA_EN & DIP_X_REG_CTL_DMA_EN_FEO);
		MUINT32 cnt = 0; 
        while(tmp != 0){        
            cnt++;              
            tmp >>= 1;          
        }  
        
		//LOG_INF("DIP_X_REG_CTL_DMA_EN_FEO:  cnt:0x%x\n", cnt);
		
		reg = DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_FEO_YSIZE, YSIZE);
		reg2 = DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_FEO_STRIDE, STRIDE);
		LOG_INF("FEO buffer :  Y size + 1:0x%x ; Stride:0x%x ; szie:0x%x \n", reg +1, reg2, ( reg + 1) * reg2);
        //pBuf[0][cnt][0]->size = (reg + 1) * reg2 ; 
		pBuf[0][cnt][0]->size = pInputInfo->DmaiTbls[0][_DMAI_TBL_FEO].tblLength;
        
        if (pImemDrv->allocVirtBuf(pBuf[0][cnt][0]) < 0) {       
            LOG_ERR(" imem alloc fail at FEO\n");   
            return 1;                                       
        }  

		if (pImemDrv->mapPhyAddr(pBuf[0][cnt][0]) < 0) {         
            LOG_ERR(" imem map fail at FEO\n");     
            return 1;                                       
        }    

		memset((MUINT8*)pBuf[0][cnt][0]->virtAddr,0x0,pBuf[0][cnt][0]->size);

        #if LOG_DMA_INFO
        LOG_INF("FEO PA:0x%x\n", (unsigned int)pBuf[0][cnt][0]->phyAddr);       
        #endif
        
        DIP_WRITE_PHY_REG(pDrvDipPhy, DIP_X_FEO_BASE_ADDR, pBuf[0][cnt][0]->phyAddr);   
        
    } 

	LOG_INF("========== DUMP DIP ==============");
	LOG_INF("DIP_X_CTL_START=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_START-(MUINT8*)&dipReg)), DIP_HW_A));
	LOG_INF("DIP_X_CTL_YUV_EN=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_YUV_EN-(MUINT8*)&dipReg)), DIP_HW_A));
	LOG_INF("DIP_X_CTL_YUV2_EN=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_YUV2_EN-(MUINT8*)&dipReg)), DIP_HW_A));
	LOG_INF("DIP_X_CTL_RGB_EN=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_RGB_EN-(MUINT8*)&dipReg)), DIP_HW_A));
	LOG_INF("DIP_X_CTL_DMA_EN=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_DMA_EN-(MUINT8*)&dipReg)), DIP_HW_A));
	LOG_INF("DIP_X_CTL_FMT_SEL=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_FMT_SEL-(MUINT8*)&dipReg)), DIP_HW_A));
	LOG_INF("DIP_X_CTL_PATH_SEL=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_PATH_SEL-(MUINT8*)&dipReg)), DIP_HW_A));
	LOG_INF("DIP_X_CTL_MISC_SEL=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_MISC_SEL-(MUINT8*)&dipReg)), DIP_HW_A));
	LOG_INF("DIP_X_CTL_TDR_CTL=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_TDR_CTL-(MUINT8*)&dipReg)), DIP_HW_A));
	LOG_INF("DIP_X_CTL_TDR_TILE=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_TDR_TILE-(MUINT8*)&dipReg)), DIP_HW_A));
	LOG_INF("DIP_X_CTL_TDR_TCM_EN=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_TDR_TCM_EN-(MUINT8*)&dipReg)), DIP_HW_A));
	LOG_INF("DIP_X_CTL_TDR_TCM2_EN=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_TDR_TCM2_EN-(MUINT8*)&dipReg)), DIP_HW_A));

    for(i=0x0;i<=0x3000;i+=20){
        LOG_INF("(0x%08x,0x%08x)(0x%08x,0x%08x)(0x%08x,0x%08x)(0x%08x,0x%08x)(0x%08x,0x%08x)",
            0x15022000+i,pDrvDipPhy->readReg(i, DIP_HW_A),0x15022000+i+4,pDrvDipPhy->readReg(i+4, DIP_HW_A),
            0x15022000+i+8,pDrvDipPhy->readReg(i+8, DIP_HW_A),0x15022000+i+12,pDrvDipPhy->readReg(i+12, DIP_HW_A),
            0x15022000+i+16,pDrvDipPhy->readReg(i+16, DIP_HW_A));
    }

    LOG_INF("Ready to trigger P2...!!");
    getchar();

    LOG_INF("Enable P2...!!");

    pDrvDipPhy->writeReg((DIP_A_CTL_SW_CTL - 0x15022000), 0x6);
    pDrvDipPhy->writeReg((DIP_A_CTL_SW_CTL - 0x15022000), 0x2);    
    pDrvDipPhy->writeReg((DIP_A_CTL_INT_STATUS - 0x15022000), 0x0);
    pDrvDipPhy->writeReg((DIP_A_CTL_INT_STATUSX - 0x15022000), 0x0);      
    DIP_WRITE_PHY_BITS(pDrvDipPhy, DIP_X_CTL_INT_EN, INT_WCLR_EN, 1);

    pDrvDipPhy->writeReg((DIP_A_CTL_START - 0x15022000), 0x1);
/*
	LOG_INF("========== DUMP DIP ==============");
	LOG_INF("DIP_X_CTL_START=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_START-(MUINT8*)&dipReg)), DIP_HW_A));
	LOG_INF("DIP_X_CTL_YUV_EN=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_YUV_EN-(MUINT8*)&dipReg)), DIP_HW_A));
	LOG_INF("DIP_X_CTL_YUV2_EN=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_YUV2_EN-(MUINT8*)&dipReg)), DIP_HW_A));
	LOG_INF("DIP_X_CTL_RGB_EN=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_RGB_EN-(MUINT8*)&dipReg)), DIP_HW_A));
	LOG_INF("DIP_X_CTL_DMA_EN=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_DMA_EN-(MUINT8*)&dipReg)), DIP_HW_A));
	LOG_INF("DIP_X_CTL_FMT_SEL=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_FMT_SEL-(MUINT8*)&dipReg)), DIP_HW_A));
	LOG_INF("DIP_X_CTL_PATH_SEL=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_PATH_SEL-(MUINT8*)&dipReg)), DIP_HW_A));
	LOG_INF("DIP_X_CTL_MISC_SEL=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_MISC_SEL-(MUINT8*)&dipReg)), DIP_HW_A));
	LOG_INF("DIP_X_CTL_TDR_CTL=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_TDR_CTL-(MUINT8*)&dipReg)), DIP_HW_A));
	LOG_INF("DIP_X_CTL_TDR_TILE=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_TDR_TILE-(MUINT8*)&dipReg)), DIP_HW_A));
	LOG_INF("DIP_X_CTL_TDR_TCM_EN=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_TDR_TCM_EN-(MUINT8*)&dipReg)), DIP_HW_A));
	LOG_INF("DIP_X_CTL_TDR_TCM2_EN=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_TDR_TCM2_EN-(MUINT8*)&dipReg)), DIP_HW_A));

    for(i=0x0;i<=0x3000;i+=20){
        LOG_INF("(0x%08x,0x%08x)(0x%08x,0x%08x)(0x%08x,0x%08x)(0x%08x,0x%08x)(0x%08x,0x%08x)",
            0x15022000+i,pDrvDipPhy->readReg(i, DIP_HW_A),0x15022000+i+4,pDrvDipPhy->readReg(i+4, DIP_HW_A),
            0x15022000+i+8,pDrvDipPhy->readReg(i+8, DIP_HW_A),0x15022000+i+12,pDrvDipPhy->readReg(i+12, DIP_HW_A),
            0x15022000+i+16,pDrvDipPhy->readReg(i+16, DIP_HW_A));
    }
*/
    //LOG_INF("Enter Wait P2 Done ...!!\n");
    //LOG_INF("press any key to wait for P2 Done...\n");
    //getchar();

    return 0;
}

MINT32 Pattern_Stop_1(MUINT32* _ptr, MUINT32* _ptrphy, MUINTPTR para)
{
    (void)para;
#if 1
    (void)_ptr;
    MUINT32 reg, val = 0, times = 0;
    PhyDipDrv* pDrvDipPhy = (PhyDipDrv*)_ptrphy;

#else
    ISP_WAIT_IRQ_ST irq;
    _uni;linkpath;
    irq.Clear = ISP_IRQ_CLEAR_WAIT;
    irq.UserKey = 0;
    irq.St_type = SIGNAL_INT;
    irq.Status = SW_PASS1_DON_ST;
    irq.Timeout = 3000;

    if(((ISP_DRV_CAM**)_ptr)[CAM_A]){
        LOG_INF("start wait cam_a sw p1 done\n");
        if(((ISP_DRV_CAM**)_ptr)[CAM_A]->waitIrq(&irq) == MFALSE){
            LOG_ERR(" wait CAM_A p1 done fail\n");
        }
    }
    irq.Timeout = 3000;
    if(((ISP_DRV_CAM**)_ptr)[CAM_B]){
        LOG_INF("start wait cam_b sw p1 done\n");
        if(((ISP_DRV_CAM**)_ptr)[CAM_B]->waitIrq(&irq) == MFALSE){
            LOG_ERR(" wait CAM_B p1 done fail\n");
        }
    }
#endif

#if WHILE_ONE_TEST
while (1)
{
#endif
    do
    {
        //LOG_INF("DIP_X_CTL_INT_EN=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_INT_EN-(MUINT8*)&dipReg)), DIP_HW_A));
        reg = DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_CTL_INT_STATUS, PASS2_DONE_STATUS);
        //reg = DIP_READ_PHY_REG(pDrvDipPhy, DIP_X_CTL_INT_STATUS);
        //LOG_INF("DIP_X_CTL_INT_STATUS=0x%08x", DIP_READ_PHY_REG(pDrvDipPhy, DIP_X_CTL_INT_STATUS));
        //LOG_INF("DIP_X_CTL_INT_STATUSX=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_INT_STATUSX-(MUINT8*)&dipReg)), DIP_HW_A));
        //LOG_INF("DIP_X_CTL_YUV_REQ_STATUS=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_YUV_REQ_STATUS-(MUINT8*)&dipReg)), DIP_HW_A));
        //LOG_INF("DIP_X_CTL_YUV2_REQ_STATUS=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_YUV2_REQ_STATUS-(MUINT8*)&dipReg)), DIP_HW_A));
        //LOG_INF("DIP_X_CTL_YUV_RDY_STATUS=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_YUV_RDY_STATUS-(MUINT8*)&dipReg)), DIP_HW_A));
        //LOG_INF("DIP_X_CTL_YUV2_RDY_STATUS=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_YUV2_RDY_STATUS-(MUINT8*)&dipReg)), DIP_HW_A));
        //LOG_INF("DIP_X_CTL_DMA_REQ_STATUS=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_DMA_REQ_STATUS-(MUINT8*)&dipReg)), DIP_HW_A));
        //LOG_INF("DIP_X_CTL_DMA_RDY_STATUS=0x%08x",pDrvDipPhy->readReg(((MUINT32)((MUINT8*)&dipReg.DIP_X_CTL_DMA_RDY_STATUS-(MUINT8*)&dipReg)), DIP_HW_A));
        
        //LOG_INF("P2 Done ? reg = 0x%x, reg2 = 0x%x, reg3 = 0x%x, regR = 0x%x, val = %d \n", reg, reg2, reg3, regR, val);
        val++;
    } while(reg == 0x0);
    LOG_INF("DIP Done, run times = %8d", ++ times);
    pDrvDipPhy->writeReg((DIP_A_CTL_SW_CTL - 0x15022000), 0x6);
    pDrvDipPhy->writeReg((DIP_A_CTL_SW_CTL - 0x15022000), 0x2);
    pDrvDipPhy->writeReg((DIP_A_CTL_INT_STATUS - 0x15022000), 0x0);
    pDrvDipPhy->writeReg((DIP_A_CTL_INT_STATUSX - 0x15022000), 0x0);  
#if WHILE_ONE_TEST    
    LOG_INF("Enable DIP");
    pDrvDipPhy->writeReg((DIP_A_CTL_START - 0x15022000), 0x1);
}
#endif
    return 0;
}


void cam_config_pre(PhyDipDrv* pDrvDip)
{

  //LOG_INF("========== Config_Pre ==============");

  pDrvDip->writeReg((DIP_A_CTL_YUV_DCM_DIS - 0x15022000), 0x0);
  pDrvDip->writeReg((DIP_A_CTL_YUV2_DCM_DIS - 0x15022000), 0x0);
  pDrvDip->writeReg((DIP_A_CTL_RGB_DCM_DIS - 0x15022000), 0x0);
  pDrvDip->writeReg((DIP_A_CTL_DMA_DCM_DIS - 0x15022000), 0x0);
  pDrvDip->writeReg((DIP_A_CTL_TOP_DCM_DIS - 0x15022000), 0x0);

/*
  reg = pDrvDip->readReg(DIP_A_CQ_EN - 0x15022000) & 0xfffeffff;
  LOG_INF("DIP_A_CQ_EN=0x%08x", reg);
  pDrvDip->writeReg((DIP_A_CQ_EN - 0x15022000), reg);
  LOG_INF("DIP_A_CQ_EN & 0xfffeffff =0x%08x", pDrvDip->readReg(DIP_A_CQ_EN - 0x15022000));

  //LOG_INF("DIP_A_CTL_INT_EN =0x%08x", pDrvDip->readReg(DIP_A_CTL_INT_EN- 0x15022000));
  val = pDrvDip->readReg(DIP_A_CTL_INT_EN- 0x15022000) | (1<<16) | (1<<17);  // pass2_done_en
  //LOG_INF("DIP_A_CTL_INT_EN | (1<<16) | (1<<17) =0x%08x",  val);
  val = val & 0x7fffffff;   // 0: read clear 1: write clear
  LOG_INF("val & 0x7fffffff =0x%08x",  val);
  pDrvDip->writeReg((DIP_A_CTL_INT_EN - 0x15022000), val);
  LOG_INF("DIP_A_CTL_INT_EN & 0x7fffffff =0x%08x",  pDrvDip->readReg(DIP_A_CTL_INT_EN- 0x15022000));
*/

}

MINT32 Pattern_Loading_1(MUINT32* _ptr, MUINT32* _ptrphy, MUINTPTR inputInfo)
{
    PhyDipDrv* pDrvDipPhy = (PhyDipDrv*)_ptrphy;
	PhyDipDrv*    pDrvDip = (PhyDipDrv*)_ptr;
    TestInputInfo *pInputInfo = (TestInputInfo *)inputInfo;
    MUINT32 reg = 0;

    static const unsigned char pattern_imgi_tbl[] = {
        #include "Dip/P2_LPDVT/imgi.h"
    };

    static const unsigned char pattern_vipi_tbl[] = {
        #include "Dip/P2_LPDVT/vipi.h"
    };

    static const unsigned char pattern_vip2i_tbl[] = {
        #include "Dip/P2_LPDVT/vip2i.h"
    };
#if 0
    static const unsigned char pattern_lcei_tbl[] = {
        #include "Dip/P2_LPDVT/lcei.h"
    };    
#endif
#if 0
    static const unsigned char pattern_tdri_tbl[] = {
        #include "Dip/P2_LPDVT/tdri.h"
    }; 
#endif
    static const unsigned char pattern_img2o_tbl[] = {
        #include "Dip/P2_LPDVT/img2o.h"
    }; 

    static const unsigned char pattern_img2bo_tbl[] = {
        #include "Dip/P2_LPDVT/img2bo.h"
    }; 

    static const unsigned char pattern_img3o_tbl[] = {
        #include "Dip/P2_LPDVT/img3o.h"
    }; 

    static const unsigned char pattern_img3bo_tbl[] = {
        #include "Dip/P2_LPDVT/img3bo.h"
    }; 
#if 0
    static const unsigned char pattern_feo_tbl[] = {
        #include "Dip/P2_LPDVT/feo.h"
    };  
#endif    
    /* save dmai buffer location, for latter memory allocation and loading */
    pInputInfo->DmaiTbls[0][_DMAI_TBL_IMGI].pTblAddr = pattern_imgi_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_IMGI].tblLength = sizeof(pattern_imgi_tbl)/sizeof(pattern_imgi_tbl[0]);

    pInputInfo->DmaiTbls[0][_DMAI_TBL_VIPI].pTblAddr = pattern_vipi_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_VIPI].tblLength = sizeof(pattern_vipi_tbl)/sizeof(pattern_vipi_tbl[0]);

    pInputInfo->DmaiTbls[0][_DMAI_TBL_VIP2I].pTblAddr = pattern_vip2i_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_VIP2I].tblLength = sizeof(pattern_vip2i_tbl)/sizeof(pattern_vip2i_tbl[0]);
#if 0
    pInputInfo->DmaiTbls[0][_DMAI_TBL_LCEI].pTblAddr = pattern_lcei_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_LCEI].tblLength = sizeof(pattern_lcei_tbl)/sizeof(pattern_lcei_tbl[0]);    
#endif
#if 0
    pInputInfo->DmaiTbls[0][_DMAI_TBL_TDRI].pTblAddr = pattern_tdri_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_TDRI].tblLength = sizeof(pattern_tdri_tbl)/sizeof(pattern_tdri_tbl[0]);  
#endif
    pInputInfo->DmaiTbls[0][_DMAI_TBL_IMG2O].pTblAddr = pattern_img2o_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_IMG2O].tblLength = sizeof(pattern_img2o_tbl)/sizeof(pattern_img2o_tbl[0]);

    pInputInfo->DmaiTbls[0][_DMAI_TBL_IMG2BO].pTblAddr = pattern_img2bo_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_IMG2BO].tblLength = sizeof(pattern_img2bo_tbl)/sizeof(pattern_img2bo_tbl[0]);

    pInputInfo->DmaiTbls[0][_DMAI_TBL_IMG3O].pTblAddr = pattern_img3o_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_IMG3O].tblLength = sizeof(pattern_img3o_tbl)/sizeof(pattern_img3o_tbl[0]);

    pInputInfo->DmaiTbls[0][_DMAI_TBL_IMG3BO].pTblAddr = pattern_img3bo_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_IMG3BO].tblLength = sizeof(pattern_img3bo_tbl)/sizeof(pattern_img3bo_tbl[0]);
#if 0
    pInputInfo->DmaiTbls[0][_DMAI_TBL_FEO].pTblAddr = pattern_feo_tbl;
    pInputInfo->DmaiTbls[0][_DMAI_TBL_FEO].tblLength = sizeof(pattern_feo_tbl)/sizeof(pattern_feo_tbl[0]);
#endif

/*    
    int i = 0;
    LOG_INF("Dump IMGI in DRAM\n");
    for (i = 0; i < 8; i++)
    {
        LOG_INF("0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x\n", *((MUINT32 *)pattern_imgi_tbl+8*i), *((MUINT32 *)pattern_imgi_tbl+8*i+1), *((MUINT32 *)pattern_imgi_tbl+8*i+2), *((MUINT32 *)pattern_imgi_tbl+8*i+3), *((MUINT32 *)pattern_imgi_tbl+8*i+4), *((MUINT32 *)pattern_imgi_tbl+8*i+5), *((MUINT32 *)pattern_imgi_tbl+8*i+6), *((MUINT32 *)pattern_imgi_tbl+8*i+7));
    }

    LOG_INF("Dump VIPI in DRAM\n");
    for (i = 0; i < 8; i++)
    {
        LOG_INF("0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x\n", *((MUINT32 *)pattern_vipi_tbl+8*i), *((MUINT32 *)pattern_vipi_tbl+8*i+1), *((MUINT32 *)pattern_vipi_tbl+8*i+2), *((MUINT32 *)pattern_vipi_tbl+8*i+3), *((MUINT32 *)pattern_vipi_tbl+8*i+4), *((MUINT32 *)pattern_vipi_tbl+8*i+5), *((MUINT32 *)pattern_vipi_tbl+8*i+6), *((MUINT32 *)pattern_vipi_tbl+8*i+7));
    }

    LOG_INF("Dump VIP2I in DRAM\n");
    for (i = 0; i < 8; i++)
    {
        LOG_INF("0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x\n", *((MUINT32 *)pattern_vip2i_tbl+8*i), *((MUINT32 *)pattern_vip2i_tbl+8*i+1), *((MUINT32 *)pattern_vip2i_tbl+8*i+2), *((MUINT32 *)pattern_vip2i_tbl+8*i+3), *((MUINT32 *)pattern_vip2i_tbl+8*i+4), *((MUINT32 *)pattern_vip2i_tbl+8*i+5), *((MUINT32 *)pattern_vip2i_tbl+8*i+6), *((MUINT32 *)pattern_vip2i_tbl+8*i+7));
    }

    LOG_INF("Dump LCEI in DRAM\n");
    for (i = 0; i < 8; i++)
    {
        LOG_INF("0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x\n", *((MUINT32 *)pattern_lcei_tbl+8*i), *((MUINT32 *)pattern_lcei_tbl+8*i+1), *((MUINT32 *)pattern_lcei_tbl+8*i+2), *((MUINT32 *)pattern_lcei_tbl+8*i+3), *((MUINT32 *)pattern_lcei_tbl+8*i+4), *((MUINT32 *)pattern_lcei_tbl+8*i+5), *((MUINT32 *)pattern_lcei_tbl+8*i+6), *((MUINT32 *)pattern_lcei_tbl+8*i+7));
    }    

    LOG_INF("Dump TDRI in DRAM\n");
    for (i = 0; i < 8; i++)
    {
        LOG_INF("0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x\n", *((MUINT32 *)pattern_tdri_tbl+8*i), *((MUINT32 *)pattern_tdri_tbl+8*i+1), *((MUINT32 *)pattern_tdri_tbl+8*i+2), *((MUINT32 *)pattern_tdri_tbl+8*i+3), *((MUINT32 *)pattern_tdri_tbl+8*i+4), *((MUINT32 *)pattern_tdri_tbl+8*i+5), *((MUINT32 *)pattern_tdri_tbl+8*i+6), *((MUINT32 *)pattern_tdri_tbl+8*i+7));
    }
*/
    //cam_config_pre(pDrvDipPhy);
    
    #include "Dip/P2_LPDVT/DIP.h"
	#include "Dip/P2_LPDVT/DIP_ANR_TBL.h"
	#include "Dip/P2_LPDVT/DIP_GGM_LUT.h"
	#include "Dip/P2_LPDVT/DIP_PCA_TBL.h"

	//cam_config_pre(pDrvDipPhy);

    reg = pDrvDip->readReg(DIP_A_CTL_INT_EN - 0x15022000) | 0x80000000 ;
    LOG_INF("DIP_A_CTL_INT_EN=0x%08x", reg);
    //pDrvDip->writeReg((DIP_A_CTL_INT_EN - 0x15022000), reg);
    return 0;
}

MINT32 Pattern_BitTrue_1(MUINT32* _ptr, MUINT32* _ptrphy, MUINTPTR inputInfo)
{
    (void)_ptr; (void)_ptrphy; (void)inputInfo;
    MINT32 ret=0;
    /*
    PhyDipDrv* pDrvDipPhy = (PhyDipDrv*)_ptrphy;
    IMEM_BUF_INFO**** pBuf = NULL;
    TestInputInfo *pInputInfo = (TestInputInfo *)inputInfo;
    UINT32 DMA_EN = 0,_tmp=0,_cnt=0;
    MUINT32 *pTable = NULL,*pMem = NULL;

    static const unsigned char golden_1_img2o[] = {
        #include "Dip/P2_BASIC/Golden/img2o_a_golden.dhex"
    };

    #define COMPARE(STR,STR2)\
            _tmp = (DMA_EN & DIP_X_REG_CTL_DMA_EN_##STR);\
            _cnt = 0;\
            while(_tmp != 0){\
                _cnt++;\
                _tmp >>= 1;\
            }\
            pTable = (MUINT32*)golden_1_##STR2;\
            pMem = (MUINT32*)pBuf[0][_cnt][0]->virtAddr;\
            _tmp = pBuf[0][_cnt][0]->size/sizeof(MUINT32);\
            _cnt = 0;\
            LOG_INF("###########################\n");\
            LOG_INF("cnt: %d, tmp: %d; pMem:%d, pTable:%d\n", _cnt, _tmp, *pMem, *pTable);\
            while((*pMem++ == *pTable++) && (_cnt++ != _tmp)){\
            }\
            if(_cnt != (_tmp+1)){\
                ret++;\
                LOG_ERR("%s bit true fail,%d_%d\n",#STR,_cnt,(_tmp+1));\
            }\
            else{\
                LOG_INF("%s bit true pass\n",#STR);\
            }\
            LOG_INF("###########################\n");\


    pBuf = (IMEM_BUF_INFO****)pInputInfo->pImemBufs;

    
    DMA_EN = DIP_READ_PHY_REG(pDrvDipPhy, DIP_X_CTL_DMA_EN);
    COMPARE(IMG2O, img2o);

    LOG_INF("Exit Compare ...\n");
	//LOG_INF("press any key continuous\n");
    //getchar();
    */
    return ret;
}

MINT32 Pattern_release(MUINT32* _ptr, MUINT32* _ptrphy, MUINTPTR BufPtr)
{
    (void)_ptr; (void)_ptrphy;
    IMEM_BUF_INFO**** pBuf;
    DipIMemDrv* pImemDrv = NULL;

    pImemDrv = DipIMemDrv::createInstance();
    pBuf = (IMEM_BUF_INFO****)BufPtr;
    for(MUINT32 j=0;j < (DIP_HW_MAX - DIP_HW_A);j++){
        for(MUINT32 i=0;i<32; i++){
            for(MUINT32 n=0;n<2;n++){
                if(pBuf[j][i][n]->size != 0)
                    pImemDrv->freeVirtBuf(pBuf[j][i][n]);
            }
        }
    }

    pImemDrv->uninit();
    pImemDrv->destroyInstance();

    return 0;
}

#define Total_case  2
#define CASE_OP     6   // 5 is for 1: isp drv init|fakeSensor,2:load MMU setting, 3:loading pattern(APMCU or CQ loading). 4:mem allocate + start, 5:stop, 6:deallocate

typedef MINT32 (*LDVT_DCB)(MUINT32*, MUINT32*, MUINTPTR);

LDVT_DCB DCB_TBL[Total_case][CASE_OP] = {
    {MMU_INIT, Pattern_Loading_1, Pattern_Start_1, Pattern_Stop_1, Pattern_BitTrue_1, Pattern_release},
};

void get_bw(MUINT32* _ptrphy)
{
    PhyDipDrv* pDrvDipPhy = (PhyDipDrv*)_ptrphy;
    LARB_MMU_STRUCT larbInfo;
    larbInfo.LarbNum = 0;
    larbInfo.regVal = 0;
    larbInfo.regOffset = 0x0;
    while(1)
    {
        pDrvDipPhy->setDeviceInfo(_SET_DBG_INT, (MUINT8 *)&larbInfo);
    }
}   

int IspDrvDip_LDVT(void)
{
    int ret = 0;
    int s;
    MUINT32 test_case;
    IMEM_BUF_INFO ****pimgBuf = NULL;
	PhyDipDrv*    pDrvDip = (PhyDipDrv*)PhyDipDrvImp::createInstance(DIP_HW_A);
    PhyDipDrv* pDrvDipPhy = (PhyDipDrv*)PhyDipDrvImp::createInstance(DIP_HW_A);
    TestInputInfo   testInput;
#if 0

	ISP_DRV_CAM* ptr;

	ptr = (ISP_DRV_CAM*)ISP_DRV_CAM::createInstance(CAM_A,ISP_DRV_CQ_THRE0,0,"Test_IspDrvCam_A");

	if(ptr== NULL){
       LOG_ERR("CAM_A create fail\n");
       return -1;
    }

            if(ptr->init("Test_IspDrvCam_A") == MFALSE){
                ptr->destroyInstance();
                LOG_ERR("CAM_A init failure\n");
                ptr = NULL;
                return -1;
            }
#endif				
    if(pDrvDip == NULL || pDrvDipPhy == NULL){
    	LOG_ERR("Drv create fail\n");
        return -1;
    }

	pDrvDip->init("isp_drv_dip LDVT test");
	pDrvDipPhy->init("isp_drv_dip_phy LDVTtest");
	
    if(pimgBuf == NULL){
        pimgBuf = (IMEM_BUF_INFO****)malloc(sizeof(IMEM_BUF_INFO***)*(DIP_HW_MAX+1));
        for(MUINT32 i=0;i<(DIP_HW_MAX - DIP_HW_A);i++){
            pimgBuf[i] = (IMEM_BUF_INFO***)malloc(sizeof(IMEM_BUF_INFO**)*32);
            for(MUINT32 j=0;j<32;j++){
                pimgBuf[i][j] = (IMEM_BUF_INFO**)malloc(sizeof(IMEM_BUF_INFO*)*2);
                for(MUINT32 k=0;k<2;k++){
                    pimgBuf[i][j][k] = new IMEM_BUF_INFO();//calls default constructor
                }
            }
        }
    }
    testInput.pImemBufs= pimgBuf;

    LOG_INF("##############################\n");
	LOG_INF("case 0: Pass2-P2A\n");
    LOG_INF("case 2: Start BW\n");
    LOG_INF("##############################\n");
    s = getchar();
    test_case = s;
    //test_case = 0;
    //getchar();
    if (test_case >= Total_case)
    {
        LOG_INF("test_case is bigger than Total_case:%d!!\n",test_case);
        return 0;
    }

    LOG_INF("start case:%d\n",test_case);
    switch(test_case){
        case 0:
		case 1:
#if 0            
            if((ret = DCB_TBL[test_case][0]((MUINT32*)ptr,(MUINT32*)pDrvDipPhy, DIP_HW_A)) != 0){
                LOG_ERR(" case_%d step_1 fail\n",test_case);
                return 1;
            }
#endif
            if((ret = DCB_TBL[test_case][1]((MUINT32*)pDrvDip,(MUINT32*)pDrvDipPhy, (MUINTPTR)&testInput)) != 0){
                LOG_ERR(" case_%d step_2 fail\n",test_case);
                return 1;
            }
 
            if((ret = DCB_TBL[test_case][2]((MUINT32*)pDrvDip,(MUINT32*)pDrvDipPhy, (MUINTPTR)&testInput)) != 0){
                    LOG_ERR(" case_%d step_3 fail\n",test_case);
                    return 1;
            }

            if((ret = DCB_TBL[test_case][3]((MUINT32*)pDrvDip,(MUINT32*)pDrvDipPhy, DIP_HW_A)) != 0){
                LOG_ERR(" case_%d step_4 fail\n",test_case);
                return 1;
            }

            if((ret = DCB_TBL[test_case][4]((MUINT32*)pDrvDip,(MUINT32*)pDrvDipPhy, (MUINTPTR)&testInput)) != 0){
                    LOG_ERR(" case_%d step_5 fail\n",test_case);
            }
            

            if((ret = DCB_TBL[test_case][5]((MUINT32*)pDrvDip,(MUINT32*)pDrvDipPhy, (MUINTPTR)pimgBuf)) != 0){
                LOG_ERR(" case_%d step_6 fail\n",test_case);
                return 1;
            }
            break;
		case 2:
            get_bw((MUINT32*)pDrvDipPhy);
            break;
        default:
            LOG_ERR("unsupported case(%d)\n",test_case);
            return 1;
            break;
    }

    for(MUINT32 i=0;i<(DIP_HW_MAX - DIP_HW_A);i++){
        for(MUINT32 j=0;j<32;j++){
            for(MUINT32 k=0;k<2;k++)
                delete pimgBuf[i][j][k];
            free(pimgBuf[i][j]);
        }
        free(pimgBuf[i]);
    }
    pimgBuf = NULL;

	pDrvDip->uninit("isp_drv_dip LDVT test");
    pDrvDip->destroyInstance();

	pDrvDipPhy->uninit("isp_drv_dip_phy LDVTtest");
    pDrvDipPhy->destroyInstance();
#if 0
	if(ptr){
        ptr->uninit("Test_IspDrvCam_A");
        ptr->destroyInstance();
    }
#endif	
    return ret;
}
