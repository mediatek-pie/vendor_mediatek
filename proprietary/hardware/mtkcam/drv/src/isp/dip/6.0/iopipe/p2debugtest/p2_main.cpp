/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

///////////////////////////////////////////////////////////////////////////////
// No Warranty
// Except as may be otherwise agreed to in writing, no warranties of any
// kind, whether express or implied, are given by MTK with respect to any MTK
// Deliverables or any use thereof, and MTK Deliverables are provided on an
// "AS IS" basis.  MTK hereby expressly disclaims all such warranties,
// including any implied warranties of merchantability, non-infringement and
// fitness for a particular purpose and any warranties arising out of course
// of performance, course of dealing or usage of trade.  Parties further
// acknowledge that Company may, either presently and/or in the future,
// instruct MTK to assist it in the development and the implementation, in
// accordance with Company's designs, of certain softwares relating to
// Company's product(s) (the "Services").  Except as may be otherwise agreed
// to in writing, no warranties of any kind, whether express or implied, are
// given by MTK with respect to the Services provided, and the Services are
// provided on an "AS IS" basis.  Company further acknowledges that the
// Services may contain errors, that testing is important and Company is
// solely responsible for fully testing the Services and/or derivatives
// thereof before they are used, sublicensed or distributed.  Should there be
// any third party action brought against MTK, arising out of or relating to
// the Services, Company agree to fully indemnify and hold MTK harmless.
// If the parties mutually agree to enter into or continue a business
// relationship or other arrangement, the terms and conditions set forth
// hereunder shall remain effective and, unless explicitly stated otherwise,
// shall prevail in the event of a conflict in the terms in any agreements
// entered into between the parties.
////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2008, MediaTek Inc.
// All rights reserved.
//
// Unauthorized use, practice, perform, copy, distribution, reproduction,
// or disclosure of this information in whole or in part is prohibited.


#define LOG_TAG "p2debugtest"

#include <vector>

#include <sys/time.h>
#include <sys/stat.h>
#include <sys/prctl.h>

#include <stdio.h>
#include <stdlib.h>
//
#include <errno.h>
#include <fcntl.h>

#include <mtkcam/def/common.h>

//
#include <mtkcam/drv/iopipe/PostProc/INormalStream.h>

#include <ispio_pipe_ports.h>
#include <imem_drv.h>
#include <isp_drv.h>

#include <mtkcam/utils/imgbuf/IImageBuffer.h>
#include <utils/StrongPointer.h>
#include <mtkcam/utils/std/common.h>
#include <mtkcam/utils/imgbuf/ImageBufferHeap.h>
//
//thread
#include <utils/threads.h>
#include <mtkcam/def/PriorityDefs.h>
//thread priority
#include <system/thread_defs.h>
#include <sys/resource.h>
#include <utils/ThreadDefs.h>
#include <pthread.h>
#include <semaphore.h>
#include "DpIspStream.h"
#include "isp_drv_dip_phy.h"
//
using namespace std;
using namespace android;
using namespace NSCam;
using namespace NSIoPipe;
using namespace NSPostProc;

/******************************************************************************
* save the buffer to the file
*******************************************************************************/
static bool
saveBufToFile(char const*const fname, MUINT8 *const buf, MUINT32 const size)
{
    int nw, cnt = 0;
    uint32_t written = 0;

    //LOG_INF("(name, buf, size) = (%s, %x, %d)", fname, buf, size);
    //LOG_INF("opening file [%s]\n", fname);
    int fd = ::open(fname, O_RDWR | O_CREAT, S_IRWXU);
    if (fd < 0) {
        printf(": failed to create file [%s]: %s \n", fname, ::strerror(errno));
        return false;
    }

	printf("writing %d bytes to file [%s]\n", size, fname);

    //LOG_INF("writing %d bytes to file [%s]\n", size, fname);
    while (written < size) {
        nw = ::write(fd,
                     buf + written,
                     size - written);
        if (nw < 0) {
            printf(": failed to write to file [%s]: %s\n", fname, ::strerror(errno));
            break;
        }
        written += nw;
        cnt++;
    }
    //LOG_INF("done writing %d bytes to file [%s] in %d passes\n", size, fname, cnt);
    ::close(fd);
    return true;
}

/*******************************************************************************
* Driver Test Case
********************************************************************************/
enum EDrvTestCase
{
    //
    eDrvTestCase_P2A = 0,      //  normal, P2A path
    //
    eDrvTestCase_VFB_FB = 1,       //  FB, P2B path
    //
    eDrvTestCase_MFB_Blending = 2 ,          //  blending, P2A
    //
    eDrvTestCase_MFB_Mixing = 3,      //  mixing
    //
    eDrvTestCase_VSS = 4,                //  vss, P2A
    //
    eDrvTestCase_P2B_Bokeh = 5,     //  Boken, P2B
    //
    eDrvTestCase_FE = 6,            // including FE module
    //
    eDrvTestCaseo_FM = 7,            // including FM module
     //
    eDrvTestCase_Color_Effect = 8,            // specific color effect path
};


/*******************************************************************************
*  Main Function
*
*  p2debugtest 1 2 0 1
*  1st Argument: P2 Test
*  2nd Argument: Test Case Number
*  3rd Argument(testType): Defined by Test Cause
*  4th Argument(loopNum): Run Test Case Times
*
********************************************************************************/
int test_P2A(int type,int loopNum);
int test_MFB_Blending(int type,int loopNum);
int test_Imgo_FullSize_ZoomIn(int type,int loopNum);
int test_P2A_LCE(int type,int loopNum);


int test_p2_main(int testNum, int testType, int loopNum) 
{
    int ret = 0;

    printf("##############################\n");
    printf("\n");
    printf("##############################\n");

    switch(testNum)
    {
        case eDrvTestCase_P2A:
            //ret=test_P2A(testType,loopNum);
            //ret=test_Imgo_FullSize_ZoomIn(testType,loopNum);
            ret=test_P2A_LCE(testType,loopNum);
            break;
        case eDrvTestCase_MFB_Blending:
            ret=test_MFB_Blending(testType,loopNum);
            break;
        default:
            break;
    }

    ret = 1;
    return ret;
}

#include "pic/imgi_5344_4016_yuy2.h"
#include "pic/tuningbuf.h"
#include "pic/tuningbuf_p2a.h"
#include "pic/imgo_zoom_tuningbuf.h"
#include "pic/tuningbuf_lce.h"
#include "pic/srz5.h"

/*********************************************************************************/
int test_P2A(int type,int loopNum)
{

    int ret=0;
    printf("--- [test_P2A(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("test_P2A");
    printf("--- [test_P2A(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

/*
    eImgFmt_FG_BAYER10
    01-01 00:03:02.541557   505  3773 I PostProcPipe: [configPipe] vInPorts:[0]:(0x211),w(4608),h(2592),stride(8640,0,0),type(0),idx(19),dir(0),Size(0x01585b00,0x00000000,0x00000000),VA(0xc67fa000,0x00000000,0x00000000),PA(0x13800000,0x00000000,0x00000000)
    01-01 00:03:02.541624   505  3773 I PostProcPipe: [configPipe] [Imgi][crop_1](x,f_x,y,f_y)=(0,0,0,0),(w,h,tarW,tarH)=(0,0,0,0)-[crop_2](x,f_x,y,f_y)=(0,0,0,0),(w,h,tarW,tarH)=(0,0,0,0)-[crop_3](x,f_x,y,f_y)=(1332,0,749,0),(w,h,tarW,tarH)=(1944,1094,720,1280),dupCqIdx(0)
    eImgFmt_STA_2BYTE
    01-01 00:03:02.541681   505  3773 I PostProcPipe: [configPipe] vInPorts:[1]:(0x222),w(320),h(240),stride(640,0,0),type(0),idx(26),dir(0),Size(0x00025800,0x00000000,0x00000000),VA(0xddcf2000,0x00000000,0x00000000),PA(0x21800000,0x00000000,0x00000000)
    
    HAL_PIXEL_FORMAT_YV12   = 0x32315659, // YCrCb 4:2:0 Planar
    01-01 00:03:02.541730   505  3773 I PostProcPipe: [configPipe] vInPorts:[2]:(0x32315659),w(1944),h(1094),stride(4608,2304,2304),type(0),idx(22),dir(0),Size(0x00b88000,0x002e2000,0x002e2000),VA(0xd87e4000,0xd936c000,0xd964e000),PA(0x0df00000,0x0ea88000,0x0ed6a000)
    01-01 00:03:02.541774   505  3773 I PostProcPipe: [configPipe] [vipi]ofst(0),rW/H(4608/2592)
   
    HAL_PIXEL_FORMAT_YCbCr_422_I        = 0x14, // YUY2
    01-01 00:03:02.541838   505  3773 I PostProcPipe: [configPipe] vOutPorts:[0]:(0x14),w(720),h(1280),stride(1472,0,0),type(0),idx(37),dir(1),Size(0x001cc000,0x00000000,0x00000000),VA(0xe1533000,0x00000000,0x00000000),PA(0x07500000,0x00000000,0x00000000)
   
    HAL_PIXEL_FORMAT_YV12   = 0x32315659, // YCrCb 4:2:0 Planar
01-01 00:03:02.541891   505  3773 I PostProcPipe: [configPipe] vOutPorts:[1]:(0x32315659),w(4608),h(2592),stride(4608,2304,2304),type(0),idx(32),dir(1),Size(0x00b88000,0x002e2000,0x002e2000),VA(0xdc5b2000,0xdd13a000,0xdd41c000),PA(0x08a00000,0x09588000,0x0986a000)
01-01 00:03:02.541954   505  3773 I PostProcPipe: [configPipe] hwmodule(8/0), dipCQ/dup/burst(0/0/0), drvSc(0), isWtf(1),tdr_tpipe(0),en_yuv/yuv2/rgb/dma/fmt_sel/ctl_sel/misc (0x1e07df8/0x30/0x1400f9/0x10e0f1/0x480409/0x16040/0x10), tcm(0xa/0x8fd8e1e2/0xe807b3c), last(1)
*/    

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=4608, _imgi_h_=2592;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=((_imgi_w_*10/8) * 3 / 2)*(_imgi_h_);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_5344_4016_yuy2), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [test_P2A(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) * 3 / 2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_FG_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "test_P2A", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("test_P2A",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [test_P2A(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);


    MUINT32 _lcei_w_=320, _lcei_h_=240;
    IMEM_BUF_INFO buf_lcei;
    buf_lcei.size=(_lcei_w_*2*_lcei_h_);
    mpImemDrv->allocVirtBuf(&buf_lcei);
    memcpy( (MUINT8*)(buf_lcei.virtAddr), (MUINT8*)(g_imgi_5344_4016_yuy2), buf_lcei.size);
    IImageBuffer* lcei_srcBuffer;
    MINT32 lcei_bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 lcei_bufStridesInBytes[3] = {(_lcei_w_*2), 0, 0};
    PortBufInfo_v1 lcei_portBufInfo = PortBufInfo_v1( buf_lcei.memID,buf_lcei.virtAddr,0,buf_lcei.bufSecu, buf_lcei.bufCohe);
    IImageBufferAllocator::ImgParam lcei_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_STA_2BYTE),MSize(_lcei_w_, _lcei_h_), lcei_bufStridesInBytes, lcei_bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> lcei_pHeap;
    lcei_pHeap = ImageBufferHeap::create( "test_P2A", lcei_imgParam,lcei_portBufInfo,true);
    lcei_srcBuffer = lcei_pHeap->createImageBuffer();
    lcei_srcBuffer->incStrong(lcei_srcBuffer);
    lcei_srcBuffer->lockBuf("test_P2A",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Input lcei_src;
    lcei_src.mPortID=PORT_LCEI;
    lcei_src.mBuffer=lcei_srcBuffer;
    lcei_src.mPortID.group=0;
    frameParams.mvIn.push_back(lcei_src);


    MUINT32 _vipi_w_=1944, _vipi_h_=1094;
    IMEM_BUF_INFO buf_vipi;
    buf_vipi.size=0x00b88000+(2*0x002e2000);
    mpImemDrv->allocVirtBuf(&buf_vipi);
    memcpy( (MUINT8*)(buf_vipi.virtAddr), (MUINT8*)(g_imgi_5344_4016_yuy2), buf_vipi.size);

    IImageBuffer* vipi_srcBuffer;
    MINT32 vipi_bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 vipi_bufStridesInBytes[3] = {4608, 2304, 2304};
    PortBufInfo_v1 vipi_portBufInfo = PortBufInfo_v1( buf_vipi.memID,buf_vipi.virtAddr,0,buf_vipi.bufSecu, buf_vipi.bufCohe);
    IImageBufferAllocator::ImgParam vipi_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_YV12),MSize(4608, 2624), vipi_bufStridesInBytes, vipi_bufBoundaryInBytes, 3);
    sp<ImageBufferHeap> vipi_pHeap;
    vipi_pHeap = ImageBufferHeap::create( "test_P2A", vipi_imgParam,vipi_portBufInfo,true);
    vipi_srcBuffer = vipi_pHeap->createImageBuffer();
    vipi_srcBuffer->incStrong(vipi_srcBuffer);
    vipi_srcBuffer->lockBuf("test_P2A",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    vipi_srcBuffer->setExtParam(MSize(_vipi_w_,_vipi_h_),0x0);
    Input vipi_src;
    vipi_src.mPortID=PORT_VIPI;
    vipi_src.mBuffer=vipi_srcBuffer;
    vipi_src.mPortID.group=0;
    frameParams.mvIn.push_back(vipi_src);

    printf("--- [test_P2A(%d)...push src done]\n", type);

    dip_x_reg_t tuningDat;
    //memcpy( (MUINT8*)(&tuningDat.DIP_X_CTL_START), (MUINT8*)(p2a_tuning_buffer), sizeof(dip_x_reg_t));

    frameParams.mTuningData = (MVOID*)&tuningDat;

    //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=0;
    crop.mCropRect.s.h=0;
    crop.mResizeDst.w=0;
    crop.mResizeDst.h=0;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=0;
    crop2.mCropRect.s.h=0;
    crop2.mResizeDst.w=0;
    crop2.mResizeDst.h=0;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=1332;
    crop3.mCropRect.p_integral.y=749;
    crop3.mCropRect.s.w=1944;
    crop3.mCropRect.s.h=1094;
    crop3.mResizeDst.w=720;
    crop3.mResizeDst.h=1280;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [test_P2A(%d)...push crop information done\n]", type);

    //output dma
    MUINT32 _wrot_w_=720, _wrot_h_=1280;
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=736*_wrot_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {1472,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                         MSize(_wrot_w_,_wrot_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "test_P2A", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("test_P2A",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WROTO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    dst.mTransform = eTransform_ROT_90;
    if (type == 1){
        dst.mPortID.capbility=EPortCapbility_Disp;
    }
    else{
        dst.mPortID.capbility=EPortCapbility_None;
    }
    frameParams.mvOut.push_back(dst);
    //ports[cnt].capbility   = qParam.mvIn[i].mPortID.capbility;

    IMEM_BUF_INFO buf_out2;
    MUINT32 _img3o_w_=4608, _img3o_h_=2592;
    buf_out2.size=0x00b88000+(2*0x002e2000);
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {4608,2304,2304};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YV12),MSize(_img3o_w_,2624),  bufStridesInBytes_2, bufBoundaryInBytes, 3);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "test_P2A", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("test_P2A",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    outBuffer_2->setExtParam(MSize(_img3o_w_,_img3o_h_),0x0);
    Output dst_2;
    dst_2.mPortID=PORT_IMG3O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [test_P2A(%d)...push dst done\n]", type);

    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [test_P2A(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
         printf("---ERRRRRRRRR [test_P2A(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
         printf("---[test_P2A(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A(%d)...enter while...........\n]", type);
        //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
         printf("---ERRRRRRRRR [test_P2A(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
         printf("---[test_P2A(%d_%d)..deque done\n]", type, i);
        }

        //dump image
        //char filename[256];
        //sprintf(filename, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
        //saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvOut[0].mBuffer->getBufVA(0)), _wrot_w_ *_wrot_h_ * 2);

        //char filename2[256];
        //sprintf(filename2, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _wrot_h_,_wrot_h_);
        //saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvOut[1].mBuffer->getBufVA(0)), _wrot_h_ *_wrot_h_ * 2);

        printf("--- [test_P2A(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("test_P2A");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    lcei_srcBuffer->unlockBuf("test_P2A");
    mpImemDrv->freeVirtBuf(&buf_lcei);
    vipi_srcBuffer->unlockBuf("test_P2A");
    mpImemDrv->freeVirtBuf(&buf_vipi);

    outBuffer->unlockBuf("test_P2A");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("test_P2A");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [test_P2A(%d)...free memory done\n]", type);

    //
    pStream->uninit("test_P2A");
    pStream->destroyInstance();
    printf("--- [test_P2A(%d)...pStream uninit done\n]", type);

    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;


}


/*********************************************************************************/
int test_MFB_Blending(int type,int loopNum)
{
    int ret=0;
#if 0
    printf("--- [testMFB_Blending(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("testMFB_Blending");
    printf("--- [testMFB_Blending(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

/*

01-01 00:07:00.100914   504  3044 I PostProcPipe: [PrintPipePortInfo] vInPorts:[0]:(0x14),w(5344),h(4016),stride(10688,0,0),type(0),idx(18),dir(0),Size(0x028ef400,0x00000000,0x00000000),VA(0xa6997000,0x00000000,0x00000000),PA(0x44400000,0x00000000,0x00000000),
01-01 00:07:00.100914   504  3044 I PostProcPipe:                       vInPorts:[1]:(0x14),w(5344),h(4016),stride(10688,0,0),type(0),idx(19),dir(0),Size(0x028ef400,0x00000000,0x00000000),VA(0xb873e000,0x00000000,0x00000000),PA(0x31a00000,0x00000000,0x00000000),
01-01 00:07:00.100914   504  3044 I PostProcPipe:                       vInPorts:[2]:(0x2200),w(5344),h(4016),stride(5344,0,0),type(0),idx(20),dir(0),Size(0x01477a00,0x00000000,0x00000000),VA(0xb0c6f000,0x00000000,0x00000000),PA(0x37900000,0x00000000,0x00000000),
01-01 00:07:00.100914   504  3044 I PostProcPipe:                       vInPorts:[3]:(0x2200),w(167),h(126),stride(167,0,0),type(0),idx(25),dir(0),Size(0x00005232,0x00000000,0x00000000),VA(0xe511e000,0x00000000,0x00000000),PA(0x34300000,0x00000000,0x00000000)
01-01 00:07:00.101076   504  3044 I PostProcPipe: [PrintPipePortInfo] vOutPorts:[0]:(0x14),w(5344),h(4016),stride(10688,0,0),type(0),idx(31),dir(1),Size(0x028ef400,0x00000000,0x00000000),VA(0xa40a7000,0x00000000,0x00000000),PA(0x46d00000,0x00000000,0x00000000),
01-01 00:07:00.101076   504  3044 I PostProcPipe:                       vOutPorts:[1]:(0x2200),w(5344),h(4016),stride(5344,0,0),type(0),idx(34),dir(1),Size(0x01477a00,0x00000000,0x00000000),VA(0xa2c2f000,0x00000000,0x00000000),PA(0x3f300000,0x00000000,0x00000000)
01-01 00:07:00.101143   504  3044 I PostProcPipe: [configPipe] SRZ4:in(167,125), crop(0_0x0/0_0x0, 167, 126),out(5344,4016)
01-01 00:07:00.101237   504  3044 I PostProcPipe: [configPipe] [Imgi][crop_1](x,f_x,y,f_y)=(0,0,0,0),(w,h,tarW,tarH)=(5344,4016,5344,4016)-[crop_2](x,f_x,y,f_y)=(0,0,0,0),(w,h,tarW,tarH)=(0,0,0,0)-[crop_3](x,f_x,y,f_y)=(0,0,0,0),(w,h,tarW,tarH)=(0,0,0,0),dupCqIdx(1),[vipi]ofst(0),rW/H(0/0)
01-01 00:07:00.101237   504  3044 I PostProcPipe: ,             isDipOnly(0), hwmodule(8/0), dipCQ/dup/burst(2/1/0), drvSc(2), isWtf(1),tdr_tpipe(0),en_yuv/yuv2/rgb/dma/fmt_sel/ctl_sel/misc (0x20015/0x8/0x80/0x2487/0x6/0x10/0x20), tcm(0xa/0x2202e/0x400581), last(1), CRZ_EN(1)


*/

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_MFB_Bld;

    //input image
    MUINT32 _imgi_w_=5344, _imgi_h_=4016;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_5344_4016_yuy2), buf_imgi.size);

    //imem buffer 2 image heap
    printf("--- [testMFB_Blending(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {_imgi_w_*2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "testMFB_Blending", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("testMFB_Blending",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- PORT_IMGI : [testMFB_Blending(%d)...flag -8]\n", type);

    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [testMFB_Blending(%d)...push src done]\n", type);

    MUINT32 _imgbi_w_=5344, _imgbi_h_=4016;
    IMEM_BUF_INFO buf_imgbi;
    buf_imgbi.size=_imgbi_w_*_imgbi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_imgbi);
    memcpy( (MUINT8*)(buf_imgbi.virtAddr), (MUINT8*)(g_imgi_5344_4016_yuy2), buf_imgbi.size);
    //imem buffer 2 image heap
    printf("--- [testMFB_Blending(%d)...flag -1 ]\n", type);
    IImageBuffer* imgbi_srcBuffer;
    MINT32 imgbi_bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 imgbi_bufStridesInBytes[3] = {_imgbi_w_*2, 0, 0};
    PortBufInfo_v1 imgbi_portBufInfo = PortBufInfo_v1( buf_imgbi.memID,buf_imgbi.virtAddr,0,buf_imgbi.bufSecu, buf_imgbi.bufCohe);
    IImageBufferAllocator::ImgParam imgbi_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgbi_w_, _imgbi_h_), imgbi_bufStridesInBytes, imgbi_bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> imgbi_pHeap;
    imgbi_pHeap = ImageBufferHeap::create( "testMFB_Blending", imgbi_imgParam,imgbi_portBufInfo,true);
    imgbi_srcBuffer = imgbi_pHeap->createImageBuffer();
    imgbi_srcBuffer->incStrong(imgbi_srcBuffer);
    imgbi_srcBuffer->lockBuf("testMFB_Blending",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- PORT_IMGBI : [testMFB_Blending(%d)...flag -8]\n", type);

    Input src_2;
    src_2.mPortID=PORT_IMGBI;
    src_2.mBuffer=imgbi_srcBuffer;
    src_2.mPortID.group=0;
    frameParams.mvIn.push_back(src_2);
    printf("--- [testMFB_Blending(%d)...push src_2 done]\n", type);


    IMEM_BUF_INFO buf_imgci;
    buf_imgci.size=_imgi_w_*_imgi_h_*1;
    mpImemDrv->allocVirtBuf(&buf_imgci);
    memcpy( (MUINT8*)(buf_imgci.virtAddr), (MUINT8*)(g_imgi_5344_4016_yuy2), buf_imgci.size);
    IImageBuffer* imgci_srcBuffer;
    MINT32 imgci_bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 imgci_bufStridesInBytes[3] = {_imgi_w_*1, 0, 0};
    PortBufInfo_v1 imgci_portBufInfo = PortBufInfo_v1( buf_imgci.memID,buf_imgci.virtAddr,0,buf_imgci.bufSecu, buf_imgci.bufCohe);
    IImageBufferAllocator::ImgParam imgci_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),MSize(_imgi_w_, _imgi_h_), imgci_bufStridesInBytes, imgci_bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pimgciHeap;
    pimgciHeap = ImageBufferHeap::create( "testMFB_Blending", imgci_imgParam,imgci_portBufInfo,true);
    imgci_srcBuffer = pimgciHeap->createImageBuffer();
    imgci_srcBuffer->incStrong(imgci_srcBuffer);
    imgci_srcBuffer->lockBuf("testMFB_Blending",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- PORT_IMGCI : [testMFB_Blending(%d)...flag -8]\n", type);

    Input src_3;
    src_3.mPortID=PORT_IMGCI;
    src_3.mBuffer=imgci_srcBuffer;
    src_3.mPortID.group=0;
    frameParams.mvIn.push_back(src_3);
    printf("--- [testMFB_Blending(%d)...push src_3 done]\n", type);


    IMEM_BUF_INFO buf_lcei;
    MUINT32 _lcei_w_=167, _lcei_h_=126;
    buf_lcei.size=_lcei_w_*_lcei_h_*1;
    mpImemDrv->allocVirtBuf(&buf_lcei);
    memcpy( (MUINT8*)(buf_lcei.virtAddr), (MUINT8*)(srz5_lcei), buf_lcei.size);
    IImageBuffer* lcei_srcBuffer;
    MINT32 lcei_bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 lcei_bufStridesInBytes[3] = {_lcei_w_*1, 0, 0};
    PortBufInfo_v1 lcei_portBufInfo = PortBufInfo_v1( buf_lcei.memID,buf_lcei.virtAddr,0,buf_lcei.bufSecu, buf_lcei.bufCohe);
    IImageBufferAllocator::ImgParam lcei_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),MSize(_lcei_w_, _lcei_h_), lcei_bufStridesInBytes, lcei_bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> plceiHeap;
    plceiHeap = ImageBufferHeap::create( "testMFB_Blending", lcei_imgParam,lcei_portBufInfo,true);
    lcei_srcBuffer = plceiHeap->createImageBuffer();
    lcei_srcBuffer->incStrong(lcei_srcBuffer);
    lcei_srcBuffer->lockBuf("testMFB_Blending",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- PORT_LCEI : [testMFB_Blending(%d)...flag -8]\n", type);

    Input src_4;
    src_4.mPortID=PORT_LCEI;
    src_4.mBuffer=lcei_srcBuffer;
    src_4.mPortID.group=0;
    if (type == 0){
        frameParams.mvIn.push_back(src_4);
    }
    printf("--- [testMFB_Blending(%d)...push src_4 done]\n", type);


    /* SRZ5 setting */
    ModuleInfo srzModule;
    srzModule.moduleTag = EDipModule_SRZ5;
    srzModule.frameGroup = 0;
    _SRZ_SIZE_INFO_ Srz5SizeInfo;

    Srz5SizeInfo.out_w         = _imgi_w_;
    Srz5SizeInfo.out_h         = _imgi_h_;
    
    Srz5SizeInfo.crop_w        = 167;
    Srz5SizeInfo.crop_h        = 126;
    Srz5SizeInfo.in_w          = 167;
    Srz5SizeInfo.in_h          = 125;
    
    srzModule.moduleStruct = reinterpret_cast<MVOID*>(&Srz5SizeInfo);
    if (type == 0){
        frameParams.mvModuleData.push_back(srzModule);
    }


    dip_x_reg_t tuningDat;
    //memcpy( (MUINT8*)(&tuningDat.DIP_X_CTL_START), (MUINT8*)(mfb_tuning_buffer), sizeof(dip_x_reg_t));

    if (type == 0){
       // tuningDat.DIP_X_MFB_LL_CON10.Raw = 0x00000001;
    }

    frameParams.mTuningData = (MVOID*)&tuningDat;

   //crop information
    MCrpRsInfo crop;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [testMFB_Blending(%d)...push crop information done\n]", type);

    //output dma

    IMEM_BUF_INFO buf_out1;
    MUINT32 _mfbo_w_= _imgi_w_;
    MUINT32 _mfbo_h_= _imgi_h_;
    buf_out1.size=_mfbo_w_*_mfbo_h_;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_mfbo_w_,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),
                                            MSize(_mfbo_w_,_mfbo_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "testMFB_Blending", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("testMFB_Blending",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testMFB_Blending(%d)...mfbo done\n]", type);

    Output dst;
    dst.mPortID=PORT_MFBO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "testMFB_Blending", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("testMFB_Blending",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG3O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [testMFB_Blending(%d)...push dst done\n]", type);


    for(int i=0;i<loopNum;i++)
    {
        printf("-------------------------------!!!\n]");
        printf("--- start to test loop(%d_) ---!!!\n]", i+1);
        printf("-------------------------------!!!\n]");

        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        
        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [testMFB_Blending(%d_)...flush done\n]", type);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testMFB_Blending(%d_)..enque fail\n]", type);
        }
        else
        {
            printf("---[testMFB_Blending(%d_)..enque done\n]",type);
        }

        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testMFB_Blending(%d_)..deque fail\n]",type);
        }
        else
        {
            printf("---[testMFB_Blending(%d_)..deque done\n]", type);
        }

    }

    //dump image
    char filename[256];
    sprintf(filename, "/data/P2iopipe_testMFB_Blending_process_type%d_package_img3o_%dx%d.yuv", type, _imgi_w_,_imgi_h_);
    saveBufToFile(filename, reinterpret_cast<MUINT8*>(buf_out2.virtAddr), _imgi_w_ *_imgi_h_ * 2);
    
    sprintf(filename, "/data/P2iopipe_testMFB_Blending_process_type%d_package_imgi_%dx%d.yuv", type, _imgi_w_,_imgi_h_);
    saveBufToFile(filename, reinterpret_cast<MUINT8*>(buf_imgi.virtAddr), _imgi_w_ *_imgi_h_ * 2);
    
    sprintf(filename, "/data/P2iopipe_testMFB_Blending_process_type%d_package_imgbi_%dx%d.yuv", type, _imgbi_w_,_imgbi_h_);
    saveBufToFile(filename, reinterpret_cast<MUINT8*>(buf_imgbi.virtAddr), _imgbi_w_ *_imgbi_h_ * 2);
    
    printf("--- [testMFB_Blending...save file done\n]");


    //free
    srcBuffer->unlockBuf("testMFB_Blending");
    mpImemDrv->freeVirtBuf(&buf_imgi);

    imgbi_srcBuffer->unlockBuf("testMFB_Blending");
    mpImemDrv->freeVirtBuf(&buf_imgbi);

    imgci_srcBuffer->unlockBuf("testMFB_Blending");
    mpImemDrv->freeVirtBuf(&buf_imgci);

    lcei_srcBuffer->unlockBuf("testMFB_Blending");
    mpImemDrv->freeVirtBuf(&buf_lcei);


    outBuffer->unlockBuf("testMFB_Blending");
    mpImemDrv->freeVirtBuf(&buf_out1);

    outBuffer_2->unlockBuf("testMFB_Blending");
    mpImemDrv->freeVirtBuf(&buf_out2);

    printf("--- [testMFB_Blending(%d)...free memory done\n]", type);

    //
    pStream->uninit("testMFB_Blending");
    pStream->destroyInstance();
    printf("--- [testMFB_Blending(%d)...pStream uninit done\n]", type);

    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

#endif
    return ret;
}


int test_Imgo_FullSize_ZoomIn(int type,int loopNum)
{
    
    int ret=0;
    printf("--- [test_Imgo_FullSize_ZoomIn(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("test_Imgo_FullSize_ZoomIn");
    printf("--- [test_Imgo_FullSize_ZoomIn(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

/*
    01-01 08:09:08.917   570  5346 D PostProcPipe: [queryCQ] sDrvScen:0
    01-01 08:09:08.918   570  5346 D PostProcPipe: [queryCQ] sDrvScen:0, dupCQIdx:1, RingBufIndex:34
    eImgFmt_BAYER10
    01-01 08:09:08.918   570  5346 D PostProcPipe: [configPipe] vInPorts:FrameNo(194),RequestNo(194),Timestamp(0xdbc8c),[0]:(0x2201),w(5344),h(4016),stride(6680,0x0,0x0),idx(19),dir(0),Size(0x01995880,0x0,0x0),VA(0xcc7ea000,0x0,0x0),PA(0x20700000,0x0,0x0)
    eImgFmt_STA_BYTE
    01-01 08:09:08.918   570  5346 D PostProcPipe: [configPipe] vInPorts:FrameNo(194),RequestNo(194),Timestamp(0xdbc8c),[1]:(0x2400),w(1536),h(16),stride(1536,0x0,0x0),idx(21),dir(0),Size(0x00006000,0x0,0x0),VA(0xf1c52000,0x0,0x0),PA(0x6800000,0x0,0x0)
    HAL_PIXEL_FORMAT_YV12
    01-01 08:09:08.918   570  5346 D PostProcPipe: [configPipe] vInPorts:FrameNo(194),RequestNo(194),Timestamp(0xdbc8c),[2]:(0x32315659),w(1440),h(1080),stride(1472,736,736),idx(22),dir(0),Size(0x00187000,0x00061c00,0x00061c00),VA(0xdb235000,0xdb3bc000,0xdb41dc00),PA(0x10600000,0x10787000,0x107e8c00)
    HAL_PIXEL_FORMAT_YCbCr_422_I        = 0x14, // YUY2
    01-01 08:09:08.918   570  5346 D PostProcPipe: [configPipe] vOutPorts:FrameNo(194),RequestNo(194),Timestamp(0xdbc8c),[0]:(0x14),w(1080),h(1440),stride(2176,0x0,0x0),idx(37),dir(1),Size(0x002fd000,0x0,0x0),VA(0xdf002000,0x0,0x0),PA(0x2f300000,0x0,0x0)
    HAL_PIXEL_FORMAT_YCbCr_422_I        = 0x14, // YUY2
    01-01 08:09:08.918   570  5346 D PostProcPipe: [configPipe] vOutPorts:FrameNo(194),RequestNo(194),Timestamp(0xdbc8c),[1]:(0x14),w(512),h(384),stride(1280,0x0,0x0),idx(30),dir(1),Size(0x00078000,0x0,0x0),VA(0xd673d000,0x0,0x0),PA(0x7b00000,0x0,0x0)
    HAL_PIXEL_FORMAT_YV12
    01-01 08:09:08.918   570  5346 D PostProcPipe: [configPipe] vOutPorts:FrameNo(194),RequestNo(194),Timestamp(0xdbc8c),[2]:(0x32315659),w(1440),h(1080),stride(1472,736,736),idx(32),dir(1),Size(0x00187000,0x00061c00,0x00061c00),VA(0xdafea000,0xdb171000,0xdb1d2c00),PA(0x10900000,0x10a87000,0x10ae8c00)
    01-01 08:09:08.918   570  5346 D PostProcPipe: [configPipe] [Imgi][crop_1](x,f_x,y,f_y)=(1904,0,1431,0),(w,h,tarW,tarH)=(1534,1150,512,384)-[crop_2](x,f_x,y,f_y)=(0,0,0,0),(w,h,tarW,tarH)=(0,0,0,0)-[crop_3](x,f_x,y,f_y)=(1904,0,1431,0),(w,h,tarW,tarH)=(1534,1150,1080,1440),dupCqIdx(1),[vipi]ofst(0),rW/H(1534/1150)
    01-01 08:09:08.918   570  5346 D PostProcPipe: ,             isDipOnly(0), moduleIdx(0), dipCQ/dup/burst(0/1/0), drvSc(0), isWtf(1),tdr_tpipe(0),en_yuv/yuv2/rgb/rgb2/dma/fmt_sel/ctl_sel/misc (0x11e27d78/0x300/0x1000889d/0x0/0x10d8e875/0x400409/0x16040/0xf0269454), tcm(0xa/0xff8e9ca/0x733d/0x6c6), last(1), CRZ_EN(1), MDP_CROP(0,0)

*/    

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=5344, _imgi_h_=4016;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=((_imgi_w_*10/8))*(_imgi_h_);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_5344_4016_yuy2), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [test_Imgo_FullSize_ZoomIn(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "test_P2A", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("test_Imgo_FullSize_ZoomIn",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [test_Imgo_FullSize_ZoomIn(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);


    MUINT32 _imgci_w_=1536, _imgci_h_=16;
    IMEM_BUF_INFO buf_imgci;
    buf_imgci.size=(_imgci_w_*_imgci_h_);
    mpImemDrv->allocVirtBuf(&buf_imgci);
    memcpy( (MUINT8*)(buf_imgci.virtAddr), (MUINT8*)(g_imgi_5344_4016_yuy2), buf_imgci.size);
    IImageBuffer* imgci_srcBuffer;
    MINT32 imgci_bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 imgci_bufStridesInBytes[3] = {(_imgci_w_), 0, 0};
    PortBufInfo_v1 imgci_portBufInfo = PortBufInfo_v1( buf_imgci.memID,buf_imgci.virtAddr,0,buf_imgci.bufSecu, buf_imgci.bufCohe);
    IImageBufferAllocator::ImgParam imgci_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_STA_BYTE),MSize(_imgci_w_, _imgci_h_), imgci_bufStridesInBytes, imgci_bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> imgci_pHeap;
    imgci_pHeap = ImageBufferHeap::create( "test_Imgo_FullSize_ZoomIn", imgci_imgParam,imgci_portBufInfo,true);
    imgci_srcBuffer = imgci_pHeap->createImageBuffer();
    imgci_srcBuffer->incStrong(imgci_srcBuffer);
    imgci_srcBuffer->lockBuf("test_Imgo_FullSize_ZoomIn",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Input imgci_src;
    imgci_src.mPortID=PORT_IMGCI;
    imgci_src.mBuffer=imgci_srcBuffer;
    imgci_src.mPortID.group=0;
    //frameParams.mvIn.push_back(imgci_src);

    //w(1440),h(1080),stride(1472,736,736),idx(22),dir(0),Size(0x00187000,0x00061c00,0x00061c00
    MUINT32 _vipi_w_=1440, _vipi_h_=1080;
    IMEM_BUF_INFO buf_vipi;
    buf_vipi.size=0x00187000+(2*0x00061c00);
    mpImemDrv->allocVirtBuf(&buf_vipi);
    memcpy( (MUINT8*)(buf_vipi.virtAddr), (MUINT8*)(g_imgi_5344_4016_yuy2), buf_vipi.size);

    IImageBuffer* vipi_srcBuffer;
    MINT32 vipi_bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 vipi_bufStridesInBytes[3] = {1472, 736, 736};
    PortBufInfo_v1 vipi_portBufInfo = PortBufInfo_v1( buf_vipi.memID,buf_vipi.virtAddr,0,buf_vipi.bufSecu, buf_vipi.bufCohe);
    IImageBufferAllocator::ImgParam vipi_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_YV12),MSize(1440, 1088), vipi_bufStridesInBytes, vipi_bufBoundaryInBytes, 3);
    sp<ImageBufferHeap> vipi_pHeap;
    vipi_pHeap = ImageBufferHeap::create( "test_Imgo_FullSize_ZoomIn", vipi_imgParam,vipi_portBufInfo,true);
    vipi_srcBuffer = vipi_pHeap->createImageBuffer();
    vipi_srcBuffer->incStrong(vipi_srcBuffer);
    vipi_srcBuffer->lockBuf("test_Imgo_FullSize_ZoomIn",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    vipi_srcBuffer->setExtParam(MSize(_vipi_w_,_vipi_h_),0x0);
    Input vipi_src;
    vipi_src.mPortID=PORT_VIPI;
    vipi_src.mBuffer=vipi_srcBuffer;
    vipi_src.mPortID.group=0;
    frameParams.mvIn.push_back(vipi_src);

    printf("--- [test_Imgo_FullSize_ZoomIn(%d)...push src done]\n", type);

    dip_x_reg_t tuningDat;
    //memcpy( (MUINT8*)(&tuningDat.DIP_X_CTL_START), (MUINT8*)(imgo_zoom_tuning_buffer), sizeof(dip_x_reg_t));

    frameParams.mTuningData = (MVOID*)&tuningDat;

    //[Imgi][crop_1](x,f_x,y,f_y)=(1904,0,1431,0),(w,h,tarW,tarH)=(1534,1150,512,384)-[crop_2](x,f_x,y,f_y)=(0,0,0,0),(w,h,tarW,tarH)=(0,0,0,0)-[crop_3](x,f_x,y,f_y)=(1904,0,1431,0),(w,h,tarW,tarH)=(1534,1150,1080,1440),dupCqIdx(1),[vipi]ofst(0),rW/H(1534/1150)


    //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=2004;
    crop.mCropRect.p_integral.y=1507;
    crop.mCropRect.s.w=1336;
    crop.mCropRect.s.h=1002;
    crop.mResizeDst.w=640;
    crop.mResizeDst.h=480;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=0;
    crop2.mCropRect.s.h=0;
    crop2.mResizeDst.w=0;
    crop2.mResizeDst.h=0;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=2004;
    crop3.mCropRect.p_integral.y=1507;
    crop3.mCropRect.s.w=1336;
    crop3.mCropRect.s.h=1002;
    crop3.mResizeDst.w=1080;
    crop3.mResizeDst.h=1440;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [test_Imgo_FullSize_ZoomIn(%d)...push crop information done\n]", type);


    //HAL_PIXEL_FORMAT_YCbCr_422_I        = 0x14, // YUY2
    //[configPipe] vOutPorts:FrameNo(194),RequestNo(194),Timestamp(0xdbc8c),[0]:(0x14),w(1080),h(1440),stride(2176,0x0,0x0),idx(37),dir(1),Size(0x002fd000,0x0,0x0),VA(0xdf002000,0x0,0x0),PA(0x2f300000,0x0,0x0)

    //output dma
    MUINT32 _wrot_w_=1080, _wrot_h_=1440;
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=0x002fd000;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {2176,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                         MSize(_wrot_w_,_wrot_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "test_Imgo_FullSize_ZoomIn", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("test_Imgo_FullSize_ZoomIn",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WROTO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    dst.mTransform = eTransform_ROT_90;
    dst.mPortID.capbility=EPortCapbility_Disp;
    frameParams.mvOut.push_back(dst);

    //HAL_PIXEL_FORMAT_YV12
    //[configPipe] vOutPorts:FrameNo(194),RequestNo(194),Timestamp(0xdbc8c),[2]:(0x32315659),w(1440),h(1080),stride(1472,736,736),idx(32),dir(1),Size(0x00187000,0x00061c00,0x00061c00),VA(0xdafea000,0xdb171000,0xdb1d2c00),PA(0x10900000,0x10a87000,0x10ae8c00)

    IMEM_BUF_INFO buf_out2;
    MUINT32 _img3o_w_=1336, _img3o_h_=1002;
    buf_out2.size=0x00187000*4+(4*2*0x00061c00);
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {1472,736,736};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    //IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YV12),MSize(1472,1088),  bufStridesInBytes_2, bufBoundaryInBytes, 3);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YV12),MSize(1472,4352),  bufStridesInBytes_2, bufBoundaryInBytes, 3);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "test_Imgo_FullSize_ZoomIn", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("test_Imgo_FullSize_ZoomIn",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [test_Imgo_FullSize_ZoomIn getBufOffsetInBytes(0)(0x%x)\n]", outBuffer_2->getBufOffsetInBytes(0));
    outBuffer_2->setExtParam(MSize(_img3o_w_,_img3o_h_),0x21db54);
    printf("--- [test_Imgo_FullSize_ZoomIn getBufOffsetInBytes(0)(0x%x)\n]", outBuffer_2->getBufOffsetInBytes(0));
    
    Output dst_2;
    dst_2.mPortID=PORT_IMG3O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    dst_2.mOffsetInBytes = 0x21db54;
    frameParams.mvOut.push_back(dst_2);

    //HAL_PIXEL_FORMAT_YCbCr_422_I        = 0x14, // YUY2
    //[configPipe] vOutPorts:FrameNo(194),RequestNo(194),Timestamp(0xdbc8c),[1]:(0x14),w(512),h(384),stride(1280,0x0,0x0),idx(30),dir(1),Size(0x00078000,0x0,0x0),VA(0xd673d000,0x0,0x0),PA(0x7b00000,0x0,0x0)

    IMEM_BUF_INFO buf_out3;
    MUINT32 _img2o_w_=640, _img2o_h_=480;
    buf_out3.size=0x00096000;
    mpImemDrv->allocVirtBuf(&buf_out3);
    memset((MUINT8*)buf_out3.virtAddr, 0xffffffff, buf_out3.size);
    IImageBuffer* outBuffer_3=NULL;
    MUINT32 bufStridesInBytes_3[3] = {1280,0,0};
    PortBufInfo_v1 portBufInfo_3 = PortBufInfo_v1( buf_out3.memID,buf_out3.virtAddr,0,buf_out3.bufSecu, buf_out3.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_3 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(640,480),  bufStridesInBytes_3, bufBoundaryInBytes, 3);
    sp<ImageBufferHeap> pHeap_3 = ImageBufferHeap::create( "test_Imgo_FullSize_ZoomIn", imgParam_3,portBufInfo_3,true);
    outBuffer_3 = pHeap_3->createImageBuffer();
    outBuffer_3->incStrong(outBuffer_3);
    outBuffer_3->lockBuf("test_Imgo_FullSize_ZoomIn",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    outBuffer_3->setExtParam(MSize(_img2o_w_,_img2o_h_),0x0);
    Output dst_3;
    dst_3.mPortID=PORT_IMG2O;
    dst_3.mBuffer=outBuffer_3;
    dst_3.mPortID.group=0;
    frameParams.mvOut.push_back(dst_3);
    ExtraParam extraparam;

    PQParam pqparam;
    DpPqParam param;
    param.enable = PQ_ULTRARES_EN | PQ_COLOR_EN;
    param.scenario = MEDIA_ISP_PREVIEW;

    extraparam.CmdIdx = EPIPE_MDP_PQPARAM_CMD;
    pqparam.WDMAPQParam = NULL;
    pqparam.WROTPQParam = &param;
    extraparam.moduleStruct = &pqparam;
#if 1

    frameParams.mvExtraParam.push_back(extraparam);
#endif
    enqueParams.mvFrameParams.push_back(frameParams);

    printf("--- [test_Imgo_FullSize_ZoomIn(%d)...push dst done\n]", type);

    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(frameParams.mvOut[2].mBuffer->getBufVA(0)), 0xffffffff, buf_out3.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [test_Imgo_FullSize_ZoomIn(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
         printf("---ERRRRRRRRR [test_Imgo_FullSize_ZoomIn(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
         printf("---[test_Imgo_FullSize_ZoomIn(%d_%d)..enque done\n]",type, i);
        }

        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
         printf("---ERRRRRRRRR [test_Imgo_FullSize_ZoomIn(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
         printf("---[test_Imgo_FullSize_ZoomIn(%d_%d)..deque done\n]", type, i);
        }

        printf("--- [test_Imgo_FullSize_ZoomIn(%d_%d)...save file done\n]", type,i);
    }

    do
    {
        usleep(100000);
    }
    while(1);

        //deque
        //wait a momet in fpga
        //usleep(5000000);

    //free
    srcBuffer->unlockBuf("test_Imgo_FullSize_ZoomIn");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    imgci_srcBuffer->unlockBuf("test_Imgo_FullSize_ZoomIn");
    mpImemDrv->freeVirtBuf(&buf_imgci);
    vipi_srcBuffer->unlockBuf("test_Imgo_FullSize_ZoomIn");
    mpImemDrv->freeVirtBuf(&buf_vipi);

    outBuffer->unlockBuf("test_Imgo_FullSize_ZoomIn");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("test_Imgo_FullSize_ZoomIn");
    mpImemDrv->freeVirtBuf(&buf_out2);
    outBuffer_3->unlockBuf("test_Imgo_FullSize_ZoomIn");
    mpImemDrv->freeVirtBuf(&buf_out3);

    printf("--- [test_Imgo_FullSize_ZoomIn(%d)...free memory done\n]", type);

    //
    pStream->uninit("test_Imgo_FullSize_ZoomIn");
    pStream->destroyInstance();
    printf("--- [test_Imgo_FullSize_ZoomIn(%d)...pStream uninit done\n]", type);

    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;

}



int test_P2A_LCE(int type,int loopNum)
{

    int ret=0;
    printf("--- [test_P2A_LCE(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("test_P2A_LCE");
    printf("--- [test_P2A_LCE(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

/*
    01-01 00:04:26.366495   582  4370 D PostProcPipe: [queryCQ] sDrvScen:0, dupCQIdx:1, RingBufIndex:3
    01-01 00:04:26.366597   582  4370 D PostProcPipe: [PrintPipePortInfo] 
    01-01 00:04:26.366597   582  4370 D PostProcPipe: vInPorts:FN(3),RN(3),TS(0x67e04),[0]:(0x2205),w(1920),h(1080),stride(3600,0,0),idx(19),dir(0),Size(0x003b5380,0x0,0x0),VA(0xc484c000,0x0,0x0),PA(0xb500000,0x0,0x0),offset(0x0)
    01-01 00:04:26.366597   582  4370 D PostProcPipe: vInPorts:FN(3),RN(3),TS(0x67e04),[1]:(0x2401),w(384),h(288),stride(768,0,0),idx(26),dir(0),Size(0x00048000,0x0,0x0),VA(0xe5962000,0x0,0x0),PA(0x4b00000,0x0,0x0),offset(0x0)
    01-01 00:04:26.366673   582  4370 D PostProcPipe: [PrintPipePortInfo] 
    01-01 00:04:26.366673   582  4370 D PostProcPipe: vOutPorts:FN(3),RN(3),TS(0x67e04),[0]:(0x14),w(1080),h(1920),stride(2176,0,0),idx(37),dir(1),Size(0x003fc000,0x0,0x0),VA(0xc5186000,0x0,0x0),PA(0x29c00000,0x0,0x0),offset(0x0)
    01-01 00:04:26.366673   582  4370 D PostProcPipe: vOutPorts:FN(3),RN(3),TS(0x67e04),[1]:(0x32315659),w(1920),h(1080),stride(1920,960,960),idx(32),dir(1),Size(0x001fe000,0x0007f800,0x0007f800),VA(0xcd11e000,0xcd31c000,0xcd39b800),PA(0x10400000,0x105fe000,0x1067d800),offset(0x0)
    01-01 00:04:26.366819   582  4370 D PostProcPipe: [configPipe] [Imgi][crop_1](x,f_x,y,f_y)=(0,0,0,0),(w,h,tarW,tarH)=(0,0,0,0)-[crop_2](x,f_x,y,f_y)=(0,0,0,0),(w,h,tarW,tarH)=(0,0,0,0)-[crop_3](x,f_x,y,f_y)=(0,0,0,0),(w,h,tarW,tarH)=(1920,1080,1080,1920),dupCqIdx(1),[vipi]ofst(0),rW/H(0/0)
    01-01 00:04:26.366819   582  4370 D PostProcPipe: ,             isDipOnly(0), moduleIdx(0), dipCQ/dup/burst(0/1/0), drvSc(0), isWtf(1),tdr_tpipe(0),en_yuv/yuv2/rgb/rgb2/dma/fmt_sel/ctl_sel/misc (0x10e039f8/0x3f0/0x101400f9/0x0/0x10d8e081/0x480009/0x16040/0xe49010), tcm(0xa/0x9bd8e023/0xe807b18/0x6c6), last(1), CRZ_EN(0), MDP_CROP(0,0)
*/    

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1920, _imgi_h_=1080;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=((_imgi_w_*10/8) * 3 / 2)*(_imgi_h_);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_5344_4016_yuy2), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [test_P2A_LCE(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) * 3 / 2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_FG_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "test_P2A_LCE", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("test_P2A_LCE",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [test_P2A_LCE(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);


    MUINT32 _lcei_w_=384, _lcei_h_=288;
    IMEM_BUF_INFO buf_lcei;
    buf_lcei.size=(_lcei_w_*2*_lcei_h_);
    mpImemDrv->allocVirtBuf(&buf_lcei);
    memcpy( (MUINT8*)(buf_lcei.virtAddr), (MUINT8*)(g_imgi_5344_4016_yuy2), buf_lcei.size);
    IImageBuffer* lcei_srcBuffer;
    MINT32 lcei_bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 lcei_bufStridesInBytes[3] = {(_lcei_w_*2), 0, 0};
    PortBufInfo_v1 lcei_portBufInfo = PortBufInfo_v1( buf_lcei.memID,buf_lcei.virtAddr,0,buf_lcei.bufSecu, buf_lcei.bufCohe);
    IImageBufferAllocator::ImgParam lcei_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_STA_2BYTE),MSize(_lcei_w_, _lcei_h_), lcei_bufStridesInBytes, lcei_bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> lcei_pHeap;
    lcei_pHeap = ImageBufferHeap::create( "test_P2A_LCE", lcei_imgParam,lcei_portBufInfo,true);
    lcei_srcBuffer = lcei_pHeap->createImageBuffer();
    lcei_srcBuffer->incStrong(lcei_srcBuffer);
    lcei_srcBuffer->lockBuf("test_P2A_LCE",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Input lcei_src;
    lcei_src.mPortID=PORT_LCEI;
    lcei_src.mBuffer=lcei_srcBuffer;
    lcei_src.mPortID.group=0;
    frameParams.mvIn.push_back(lcei_src);

    printf("--- [test_P2A_LCE(%d)...push src done]\n", type);

    dip_x_reg_t tuningDat;
    ///memcpy( (MUINT8*)(&tuningDat.DIP_X_CTL_START), (MUINT8*)(lce_tuning_buffer), sizeof(dip_x_reg_t));

    frameParams.mTuningData = (MVOID*)&tuningDat;

    //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=0;
    crop.mCropRect.s.h=0;
    crop.mResizeDst.w=0;
    crop.mResizeDst.h=0;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=0;
    crop2.mCropRect.s.h=0;
    crop2.mResizeDst.w=0;
    crop2.mResizeDst.h=0;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=1920;
    crop3.mCropRect.s.h=1080;
    crop3.mResizeDst.w=1080;
    crop3.mResizeDst.h=1920;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [test_P2A_LCE(%d)...push crop information done\n]", type);

    //output dma
    MUINT32 _wrot_w_=1080, _wrot_h_=1920;
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=1088*_wrot_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {2176,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                         MSize(_wrot_w_,_wrot_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "test_P2A_LCE", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("test_P2A_LCE",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WROTO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    dst.mTransform = eTransform_ROT_90;
    dst.mPortID.capbility=EPortCapbility_Disp;

    frameParams.mvOut.push_back(dst);
    //ports[cnt].capbility   = qParam.mvIn[i].mPortID.capbility;


    //PostProcPipe: vOutPorts:FN(3),RN(3),TS(0x67e04),[1]:(0x32315659),w(1920),h(1080),stride(1920,960,960),idx(32),dir(1),Size(0x001fe000,0x0007f800,0x0007f800),VA(0xcd11e000,0xcd31c000,0xcd39b800),PA(0x10400000,0x105fe000,0x1067d800),offset(0x0)

    IMEM_BUF_INFO buf_out2;
    MUINT32 _img3o_w_=1920, _img3o_h_=1080;
    buf_out2.size=0x001fe000+(2*0x0007f800);
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {1920,960,960};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YV12),MSize(_img3o_w_,1088),  bufStridesInBytes_2, bufBoundaryInBytes, 3);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "test_P2A_LCE", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("test_P2A_LCE",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    outBuffer_2->setExtParam(MSize(_img3o_w_,_img3o_h_),0x0);
    Output dst_2;
    dst_2.mPortID=PORT_IMG3O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [test_P2A_LCE(%d)...push dst done\n]", type);

    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [test_P2A_LCE(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
         printf("---ERRRRRRRRR [test_P2A_LCE(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
         printf("---[test_P2A_LCE(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A(%d)...enter while...........\n]", type);
        //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
         printf("---ERRRRRRRRR [test_P2A_LCE(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
         printf("---[test_P2A_LCE(%d_%d)..deque done\n]", type, i);
        }

        //dump image
        //char filename[256];
        //sprintf(filename, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
        //saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvOut[0].mBuffer->getBufVA(0)), _wrot_w_ *_wrot_h_ * 2);

        //char filename2[256];
        //sprintf(filename2, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _wrot_h_,_wrot_h_);
        //saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvOut[1].mBuffer->getBufVA(0)), _wrot_h_ *_wrot_h_ * 2);

        printf("--- [test_P2A_LCE(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("test_P2A_LCE");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    lcei_srcBuffer->unlockBuf("test_P2A_LCE");
    mpImemDrv->freeVirtBuf(&buf_lcei);

    outBuffer->unlockBuf("test_P2A_LCE");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("test_P2A_LCE");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [test_P2A_LCE(%d)...free memory done\n]", type);

    //
    pStream->uninit("test_P2A_LCE");
    pStream->destroyInstance();
    printf("--- [test_P2A_LCE(%d)...pStream uninit done\n]", type);

    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;


}



