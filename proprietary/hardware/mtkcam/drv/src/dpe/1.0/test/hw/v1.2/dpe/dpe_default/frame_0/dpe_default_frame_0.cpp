#include "dpe_default_frame_0.h"
#include "frame_0_dpe_confo_l_frame_00_01verif.h"
#include "frame_0_dpe_confo_r_frame_00_01verif.h"
#include "frame_0_dpe_dvi_l_frame_00verif.h"
#include "frame_0_dpe_dvi_r_frame_00verif.h"
#include "frame_0_dpe_dvo_l_frame_00_01verif.h"
#include "frame_0_dpe_dvo_r_frame_00_01verif.h"
#include "frame_0_dpe_imgi_l_frame_00verif.h"
#include "frame_0_dpe_imgi_r_frame_00verif.h"
#include "frame_0_dpe_maski_l_frame_00verif.h"
#include "frame_0_dpe_maski_r_frame_00verif.h"
#include "frame_0_dpe_respo_l_frame_00_01verif.h"
#include "frame_0_dpe_respo_r_frame_00_01verif.h"


unsigned int dpe_default_frame_0_golden_dpe_confo_l_size = dpe_default_frame_0_dpe_confo_l_frame_00_01_size;
char* dpe_default_frame_0_golden_dpe_confo_l_frame = &dpe_default_frame_0_dpe_confo_l_frame_00_01[0];

unsigned int dpe_default_frame_0_golden_dpe_confo_r_size = dpe_default_frame_0_dpe_confo_r_frame_00_01_size;
char* dpe_default_frame_0_golden_dpe_confo_r_frame = &dpe_default_frame_0_dpe_confo_r_frame_00_01[0];

unsigned int dpe_default_frame_0_golden_dpe_dvo_l_size = dpe_default_frame_0_dpe_dvo_l_frame_00_01_size;
char* dpe_default_frame_0_golden_dpe_dvo_l_frame = &dpe_default_frame_0_dpe_dvo_l_frame_00_01[0];

unsigned int dpe_default_frame_0_golden_dpe_dvo_r_size = dpe_default_frame_0_dpe_dvo_r_frame_00_01_size;
char* dpe_default_frame_0_golden_dpe_dvo_r_frame = &dpe_default_frame_0_dpe_dvo_r_frame_00_01[0];

unsigned int dpe_default_frame_0_golden_dpe_respo_l_size = dpe_default_frame_0_dpe_respo_l_frame_00_01_size;
char* dpe_default_frame_0_golden_dpe_respo_l_frame = &dpe_default_frame_0_dpe_respo_l_frame_00_01[0];

unsigned int dpe_default_frame_0_golden_dpe_respo_r_size = dpe_default_frame_0_dpe_respo_r_frame_00_01_size;
char* dpe_default_frame_0_golden_dpe_respo_r_frame = &dpe_default_frame_0_dpe_respo_r_frame_00_01[0];

void dpe_default_getframe_0GoldPointer(
	unsigned long* golden_dpe_dvo_l_frame,
	unsigned long* golden_dpe_dvo_r_frame,
	unsigned long* golden_dpe_confo_l_frame,
	unsigned long* golden_dpe_confo_r_frame,
	unsigned long* golden_dpe_respo_l_frame,
	unsigned long* golden_dpe_respo_r_frame
)
{
*golden_dpe_confo_l_frame = (unsigned long)&dpe_default_frame_0_dpe_confo_l_frame_00_01[0];
*golden_dpe_confo_r_frame = (unsigned long)&dpe_default_frame_0_dpe_confo_r_frame_00_01[0];
*golden_dpe_dvo_l_frame = (unsigned long)&dpe_default_frame_0_dpe_dvo_l_frame_00_01[0];
*golden_dpe_dvo_r_frame = (unsigned long)&dpe_default_frame_0_dpe_dvo_r_frame_00_01[0];
*golden_dpe_respo_l_frame = (unsigned long)&dpe_default_frame_0_dpe_respo_l_frame_00_01[0];
*golden_dpe_respo_r_frame = (unsigned long)&dpe_default_frame_0_dpe_respo_r_frame_00_01[0];
}
