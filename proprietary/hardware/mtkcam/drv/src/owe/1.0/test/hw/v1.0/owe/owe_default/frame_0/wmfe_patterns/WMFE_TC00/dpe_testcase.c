#include "dpe_testcase.h"

#include "frame_0_dpe_setting_00.h"
#include "frame_1_dpe_setting_00.h"
#include "frame_2_dpe_setting_00.h"
#include "frame_3_dpe_setting_00.h"
#include "frame_4_dpe_setting_00.h"

CTP_STATUS_T TS_FPGA_DVE_Test_1(CTP_INPUT_DATA_T *psInput, CTP_OUTPUT_DATA_T *psOutput)
{

	frame_0_DPE_Config();
	frame_1_DPE_Config();
	frame_2_DPE_Config();
	frame_3_DPE_Config();
	frame_4_DPE_Config();

	psOutput->eType = CTP_OUTPUT_RESULT;
	psOutput->eResult = CTP_RESULT_PASS;
	return CTP_SUCCESS;

}
