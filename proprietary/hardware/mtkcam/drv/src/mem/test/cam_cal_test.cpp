
#define LOG_TAG "EEPROM_Test"


#include <utils/Errors.h>
#include <cutils/properties.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <mtkcam/drv/mem/cam_cal_drv.h>
#include <mtkcam/drv/IHalSensor.h>


#define PRINT_MSG(fmt, arg...)    printf("[%s]" fmt, __FUNCTION__, ##arg)
#define PRINT_WRN(fmt, arg...)    printf("[%s]Warning(%5d):" fmt, __FUNCTION__, __LINE__, ##arg)
#define PRINT_ERR(fmt, arg...)    printf("[%s]Err(%5d):" fmt, __FUNCTION__, __LINE__, ##arg)


using namespace NSCam;
using namespace android;

/*******************************************************************************
*
********************************************************************************/
void show_Sensors()
{
    IHalSensorList* const pHalSensorList = IHalSensorList::get();
    if(pHalSensorList) {
        pHalSensorList->searchSensors();
        unsigned int const sensorNum = pHalSensorList->queryNumberOfSensors();

        PRINT_MSG("sensorNum %d \n",sensorNum);
        for( unsigned int i = 0; i < sensorNum; i++) {
            int sensorIndex = i;
            PRINT_MSG("name:%s type:%d \n", pHalSensorList->queryDriverName(sensorIndex), pHalSensorList->queryType(sensorIndex));
            PRINT_MSG("index:%d, SensorDevIdx:%d \n", sensorIndex, pHalSensorList->querySensorDevIdx(sensorIndex));
        }

    }

}



/*******************************************************************************
* test_EEPROM
********************************************************************************/
void test_EEPROM(int senidx)
{
    char const *const szCallerName = "R1";
    IHalSensor *pHalSensor = NULL;
    IHalSensor::ConfigParam configParam;
    SensorStaticInfo sensorStaticInfo;
    SensorDynamicInfo sensorDynamicInfo;
    CAM_CAL_DATA_STRUCT GetCamCalData;
    PCAM_CAL_DATA_STRUCT pCamcalData = &GetCamCalData;

    IHalSensorList*const pHalSensorList = IHalSensorList::get();
    pHalSensorList->searchSensors();
    int const sensorNum = pHalSensorList->queryNumberOfSensors();

    if(senidx < sensorNum) {
        int sensorIndexDual = pHalSensorList->querySensorDevIdx(senidx);
        int sensorIndex = senidx;

        PRINT_MSG("name:%s type:%d \n", pHalSensorList->queryDriverName(sensorIndex), pHalSensorList->queryType(sensorIndex));
        PRINT_MSG("index:%d, SensorDevIdx:%d \n", sensorIndex, pHalSensorList->querySensorDevIdx(sensorIndex));

        pHalSensorList->querySensorStaticInfo(sensorIndexDual, &sensorStaticInfo);


        PRINT_MSG("Sensor index:%d, sensor id:0x%x\n", sensorIndex, sensorStaticInfo.sensorDevID);

        pHalSensor = pHalSensorList->createSensor(szCallerName, sensorIndex);

        if(!pHalSensor)
        {
            PRINT_ERR("createSensor fail");
            return;
        }

        PRINT_MSG("pHalSensor->powerOn start\n");
        pHalSensor->powerOn(szCallerName, 1, (MUINT *)&sensorIndex);

        PRINT_MSG("get eeprom start\n");
        CamCalDrvBase *p = CamCalDrvBase::createInstance();
        for (unsigned int cmd = (unsigned int) CAMERA_CAM_CAL_DATA_MODULE_VERSION;
                cmd < (unsigned int) CAMERA_CAM_CAL_DATA_LIST; cmd++) {
            p->GetCamCalCalData(sensorIndexDual, (CAMERA_CAM_CAL_TYPE_ENUM) cmd, pCamcalData);
        }
        PRINT_MSG("S2aVer = 0x%x\n", (pCamcalData->Single2A).S2aVer);

        PRINT_MSG("pHalSensor->powerOff\n");
        pHalSensor->powerOff(szCallerName, 1, (MUINT *)&sensorIndex);
    } else {
        PRINT_ERR("sensor idx = %d, but only %d sensors\n", senidx, sensorNum);
    }
}

/*******************************************************************************
*                 Main Function
********************************************************************************/
int main(int argc, char** argv)
{
    int ret = 0;
    int sensorIdx = 0;
    PRINT_MSG("sizeof long : %d\n", (int)sizeof(long));
    if (argc < 2) {
        show_Sensors();
        PRINT_MSG("Param: 1 <sensorIdx>\n");
        PRINT_MSG("<sensorIdx> : main(0), Sub(1), Main2(2), sub2(3)\n");
        return -1;
    }
    if(argc > 1)
        sensorIdx = atoi(argv[1]);

    PRINT_MSG("argc num:%d\n", argc);
    test_EEPROM(sensorIdx);
    PRINT_MSG("done\n");

    return ret;
}

