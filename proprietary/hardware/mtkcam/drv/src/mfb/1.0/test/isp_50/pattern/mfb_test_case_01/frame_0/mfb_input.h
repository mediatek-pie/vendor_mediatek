#include "mfb_input_bldi.h"
#include "mfb_input_bld2i.h"
#include "mfb_input_bld3i.h"
#include "mfb_input_bld4i.h"
#include "mfb_input_tdri.h"
extern char mfb_input_bld2i[];
#define mfb_input_bld2i_size 65536
extern char mfb_input_bld3i[];
#define mfb_input_bld3i_size 32768
extern char mfb_input_bld4i[];
#define mfb_input_bld4i_size 8192
extern char mfb_input_bldi[];
#define mfb_input_bldi_size 65536
extern char mfb_input_tdri[];
#define mfb_input_tdri_size 192
