/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

///////////////////////////////////////////////////////////////////////////////
// No Warranty
// Except as may be otherwise agreed to in writing, no warranties of any
// kind, whether express or implied, are given by MTK with respect to any MTK
// Deliverables or any use thereof, and MTK Deliverables are provided on an
// "AS IS" basis.  MTK hereby expressly disclaims all such warranties,
// including any implied warranties of merchantability, non-infringement and
// fitness for a particular purpose and any warranties arising out of course
// of performance, course of dealing or usage of trade.  Parties further
// acknowledge that Company may, either presently and/or in the future,
// instruct MTK to assist it in the development and the implementation, in
// accordance with Company's designs, of certain softwares relating to
// Company's product(s) (the "Services").  Except as may be otherwise agreed
// to in writing, no warranties of any kind, whether express or implied, are
// given by MTK with respect to the Services provided, and the Services are
// provided on an "AS IS" basis.  Company further acknowledges that the
// Services may contain errors, that testing is important and Company is
// solely responsible for fully testing the Services and/or derivatives
// thereof before they are used, sublicensed or distributed.  Should there be
// any third party action brought against MTK, arising out of or relating to
// the Services, Company agree to fully indemnify and hold MTK harmless.
// If the parties mutually agree to enter into or continue a business
// relationship or other arrangement, the terms and conditions set forth
// hereunder shall remain effective and, unless explicitly stated otherwise,
// shall prevail in the event of a conflict in the terms in any agreements
// entered into between the parties.
////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2008, MediaTek Inc.
// All rights reserved.
//
// Unauthorized use, practice, perform, copy, distribution, reproduction,
// or disclosure of this information in whole or in part is prohibited.

 
#define LOG_TAG "mfbstream_test"

#include <vector>

#include <sys/time.h>
#include <sys/stat.h>
#include <sys/prctl.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
//
#include <errno.h>
#include <fcntl.h>

#include <mtkcam/def/common.h>

//
#include <semaphore.h>
#include <pthread.h>
#include <utils/threads.h>
#include <mtkcam/drv/iopipe/PostProc/IEgnStream.h>
//#include <mfbunittest.h>

//#include <mtkcam/iopipe/PostProc/IFeatureStream.h>

//#include <mtkcam/imageio/ispio_pipe_ports.h>
#include <imem_drv.h>
//#include <mtkcam/drv/isp_drv.h>

#include <mtkcam/utils/imgbuf/IImageBuffer.h>
#include <utils/StrongPointer.h>
#include <mtkcam/utils/std/common.h>
#include <mtkcam/utils/imgbuf/ImageBufferHeap.h>

#include <mfbcommon.h>
//

#define TEST_CASE_420_2P 0
#define TEST_CASE_422_1P 1
#define TEST_CASE TEST_CASE_420_2P
#if (TEST_CASE == TEST_CASE_420_2P)
#include "pattern/mfb_test_case_00/frame_0/mfb_input.h"
#include "pattern/mfb_test_case_00/frame_0/mfb_output.h"
#elif (TEST_CASE == TEST_CASE_422_1P)
#include "pattern/mfb_test_case_01/frame_0/mfb_input.h"
#include "pattern/mfb_test_case_01/frame_0/mfb_output.h"
#endif

using namespace std;
using namespace android;
using namespace NSCam;
using namespace NSIoPipe;
using namespace NSEgn;

/******************************************************************************
* save the buffer to the file
*******************************************************************************/
/*MUINT32 comp_roi_mem_with_file(char* pGolden, short mode, MUINTPTR base_addr, int width, int height, MUINT32 stride,
 *                               int start_x, int start_y, int end_x, int end_y, MUINTPTR mask_base_addr, 
 *                               MUINT32 mask_size, MUINT32 mask_stride);
 */
static bool
saveBufToFile(char const*const fname, MUINT8 *const buf, MUINT32 const size);
/******************************************************************************
* MFB Test Case
*******************************************************************************/
//#define EIS_OFFLINE
//#define WINDOW
#define DEFAULT (1)

extern "C" {
//#include <mfb_default/frame_0/frame_0.h>
}

int mfb_default();

MBOOL g_bMFBCallback;

//typedef MVOID                   (*PFN_CALLBACK_T)(DVEParams& rParams);

MVOID MFBCallback(EGNParams<MFBConfig>& rParams);

pthread_t       MfbUserThread;
sem_t           MfbSem;
volatile bool            g_bMfbThreadTerminated = 0;

/*******************************************************************************
*  Main Function 
*  
********************************************************************************/

MBOOL g_b_multi_enque_mfb_tc00_request0_MFBCallback;
int g_bMultipleBuffer = 0;
int g_MfbCount = 0;
/*
MVOID MFB_multi_enque_mfb_tc00_request0_MFBCallback(MFBParams& rParams)
{
    printf("--- [request 0 callback func]\n");
    if (g_bMultipleBuffer == 1)
    {
        if (g_MfbCount%2==0)
        {
            printf("--- [MFB frame(0/1) in request 0 callback func]\n");            
        }
        else
        {
            printf("--- [MFB frame(1/1) in request 0 callback func]\n");        
        } 
    }
    else
    {
        printf("--- [MFB frame(0/0) in request 0callback func]\n");
    }
    g_b_multi_enque_mfb_tc00_request0_MFBCallback = MTRUE;
    g_MfbCount++; 
}

MVOID MFB_multi_enque_mfb_tc00_request1_MFBCallback(MFBParams& rParams)
{
    printf("--- [request 1 callback func]\n");    g_b_multi_enque_mfb_tc00_request1_MFBCallback = MTRUE;
}
*/
bool BitTrueCheck(char* hw, char* golden, unsigned int size)
{
    unsigned int i;
    //unsigned int count = 0;
    for (i = 0;i < size; i++)
    {
        if (hw[i] != golden[i])
        {
            //count ++;
            //printf("idx(%d) of hw(0x%x) is not the same as the golden answer(0x%x). golden size:%d\n", i, hw, golden, size);
            printf("Bit-true(%d of %d) Fail!, HW (0x%x) : GOLD (0x%x)\n", i, size, hw[i], golden[i]);
            return false;
        }
    }
    return true;
}

bool multi_enque_mfb_tc00_frames_MFB_Config()
{
    int ret = 0;

    NSCam::NSIoPipe::NSEgn::IEgnStream<MFBConfig>* pStream;
    pStream= NSCam::NSIoPipe::NSEgn::IEgnStream<MFBConfig>::createInstance("tc00_multi_enque");
    pStream->init();

    DipIMemDrv* mpImemDrv = NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    EGNParams<MFBConfig> rMfbParams;
    rMfbParams.mpEngineID = eMFB;
    rMfbParams.mpfnCallback = MFBCallback;

    //Input Buffer Size
        unsigned int bldi_buf_size = mfb_input_bldi_size;
        unsigned int bld2i_buf_size = mfb_input_bld2i_size;
        unsigned int bld3i_buf_size = mfb_input_bld3i_size;
        unsigned int bld4i_buf_size = mfb_input_bld4i_size;
#if (TEST_CASE == TEST_CASE_420_2P)
        unsigned int bldbi_buf_size = mfb_input_bldbi_size;
        unsigned int bld2bi_buf_size = mfb_input_bld2bi_size;
#endif    
        unsigned int tdri_buf_size = mfb_input_tdri_size;    
    
    //Output Buffer Size
        unsigned int bldo_buf_size = mfb_output_bldo_size;
        unsigned int bld2o_buf_size = mfb_output_bld2o_size;
#if (TEST_CASE == TEST_CASE_420_2P)
        unsigned int bldbo_buf_size = mfb_output_bldbo_size;
#endif
        unsigned int golden_bldo_buf_size = mfb_output_bldo_size;
        unsigned int golden_bld2o_buf_size = mfb_output_bld2o_size;
#if (TEST_CASE == TEST_CASE_420_2P)  
        unsigned int golden_bldbo_buf_size = mfb_output_bldbo_size;    
#endif

        char* bldi_frmbuf;
        char* bld2i_frmbuf;
        char* bld3i_frmbuf;
        char* bld4i_frmbuf;
#if (TEST_CASE == TEST_CASE_420_2P)
        char* bldbi_frmbuf;
        char* bld2bi_frmbuf;
#endif    
        char* tdri_frmbuf;    
    
    //HW Output Buffer Pointer
        char* hw_bldo_frmbuf;
        char* hw_bld2o_frmbuf;
#if (TEST_CASE == TEST_CASE_420_2P)
        char* hw_bldbo_frmbuf;
#endif
    //Golden Answer
        char* golden_bldo_frmbuf = &mfb_output_bldo[0];
        char* golden_bld2o_frmbuf = &mfb_output_bld2o[0];
#if (TEST_CASE == TEST_CASE_420_2P)
        char* golden_bldbo_frmbuf = &mfb_output_bldbo[0];
#endif

#if (TEST_CASE == TEST_CASE_422_1P)
    unsigned int bldi_w = 256;
    unsigned int bldi_h = 128;
    unsigned int bld2i_w = 256;
    unsigned int bld2i_h = 128;
    unsigned int bld3i_w = 256;
    unsigned int bld3i_h = 128;
    unsigned int bld4i_w = 128;
    unsigned int bld4i_h = 64;
    unsigned int bldo_w = 256;
    unsigned int bldo_h = 128;
    unsigned int bld2o_w = 256;
    unsigned int bld2o_h = 128;
    unsigned int tdri_w = 200000;
    unsigned int tdri_h = 1;
#elif (TEST_CASE == TEST_CASE_420_2P)
    unsigned int bldi_w = 256;
    unsigned int bldi_h = 128;
    unsigned int bld2i_w = 256;
    unsigned int bld2i_h = 128;
    unsigned int bld3i_w = 256;
    unsigned int bld3i_h = 128;
    unsigned int bld4i_w = 128;
    unsigned int bld4i_h = 64;
    unsigned int bldo_w = 256;
    unsigned int bldo_h = 128;
    unsigned int bld2o_w = 256;
    unsigned int bld2o_h = 128;
    unsigned int tdri_w = 200000;
    unsigned int tdri_h = 1;
#endif

    printf("--- [test_mfb_default...start to allocate ImgBuf\n");

#if (TEST_CASE == TEST_CASE_422_1P)
    IMEM_BUF_INFO buf_bldi;
    buf_bldi.size=bldi_buf_size;
    mpImemDrv->allocVirtBuf(&buf_bldi);
    memset( (MUINT8*)buf_bldi.virtAddr, 0xffffffff, buf_bldi.size);
    memcpy( (MUINT8*)(buf_bldi.virtAddr), (MUINT8*)(mfb_input_bldi), buf_bldi.size);
    IImageBuffer* ImgBuf_bldi = 0x0;
    MINT32 bufBoundaryInBytes1[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes1[3] = {bldi_w*2, 0, 0};
    PortBufInfo_v1 portBufInfo1 = PortBufInfo_v1( buf_bldi.memID, buf_bldi.virtAddr, 0, buf_bldi.bufSecu, buf_bldi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2), MSize(bldi_w, bldi_h), bufStridesInBytes1, bufBoundaryInBytes1, 1);
    sp<ImageBufferHeap> pHeap1;
    pHeap1 = ImageBufferHeap::create( "basicMFB", imgParam1, portBufInfo1, true);
    ImgBuf_bldi = pHeap1->createImageBuffer();
    ImgBuf_bldi->incStrong(ImgBuf_bldi);
    ImgBuf_bldi->lockBuf("basicMFB",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [test_mfb_default...ImgBuf bldi done, VA: 0x%lx, PA: 0x%lx\n", ImgBuf_bldi->getBufVA(0), ImgBuf_bldi->getBufPA(0));
#elif (TEST_CASE == TEST_CASE_420_2P)
    IMEM_BUF_INFO buf_bldi;
    buf_bldi.size=bldi_buf_size + bldbi_buf_size;
    mpImemDrv->allocVirtBuf(&buf_bldi);
    memset( (MUINT8*)buf_bldi.virtAddr, 0xffffffff, buf_bldi.size);
    memcpy( (MUINT8*)(buf_bldi.virtAddr), (MUINT8*)(mfb_input_bldi), bldi_buf_size);
    memcpy( (MUINT8*)(buf_bldi.virtAddr + bldi_buf_size), (MUINT8*)(mfb_input_bldbi), bldbi_buf_size);
    IImageBuffer* ImgBuf_bldi = 0x0;
    MINT32 bufBoundaryInBytes1[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes1[3] = {bldi_w, bldi_w, 0};
    PortBufInfo_v1 portBufInfo1 = PortBufInfo_v1( buf_bldi.memID, buf_bldi.virtAddr, 0, buf_bldi.bufSecu, buf_bldi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam1 = IImageBufferAllocator::ImgParam((eImgFmt_NV12), MSize(bldi_w, bldi_h), bufStridesInBytes1, bufBoundaryInBytes1, 2);
    sp<ImageBufferHeap> pHeap1;
    pHeap1 = ImageBufferHeap::create( "basicMFB", imgParam1, portBufInfo1, true);
    ImgBuf_bldi = pHeap1->createImageBuffer();
    ImgBuf_bldi->incStrong(ImgBuf_bldi);
    ImgBuf_bldi->lockBuf("basicMFB",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [test_mfb_default...ImgBuf bldi done, Plane1: VA: 0x%lx, PA: 0x%lx\n", ImgBuf_bldi->getBufVA(0), ImgBuf_bldi->getBufPA(0));
    printf("--- [test_mfb_default...ImgBuf bldi done, Plane2: VA: 0x%lx, PA: 0x%lx\n", ImgBuf_bldi->getBufVA(1), ImgBuf_bldi->getBufPA(1));
#endif

#if (TEST_CASE == TEST_CASE_422_1P)
    IMEM_BUF_INFO buf_bld2i;
    buf_bld2i.size=bld2i_buf_size;
    mpImemDrv->allocVirtBuf(&buf_bld2i);
    memset( (MUINT8*)buf_bld2i.virtAddr, 0xffffffff, buf_bld2i.size);    
    memcpy( (MUINT8*)(buf_bld2i.virtAddr), (MUINT8*)(mfb_input_bld2i), buf_bld2i.size);
    IImageBuffer* ImgBuf_bld2i = 0x0;
    MINT32 bufBoundaryInBytes2[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes2[3] = {bld2i_w*2, 0, 0};
    PortBufInfo_v1 portBufInfo2 = PortBufInfo_v1( buf_bld2i.memID,buf_bld2i.virtAddr,0,buf_bld2i.bufSecu, buf_bld2i.bufCohe);
    IImageBufferAllocator::ImgParam imgParam2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(bld2i_w, bld2i_h), bufStridesInBytes2, bufBoundaryInBytes2, 1);
    sp<ImageBufferHeap> pHeap2;
    pHeap2 = ImageBufferHeap::create( "basicMFB", imgParam2,portBufInfo2,true);
    ImgBuf_bld2i = pHeap2->createImageBuffer();
    ImgBuf_bld2i->incStrong(ImgBuf_bld2i);
    ImgBuf_bld2i->lockBuf("basicMFB",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);    
    printf("--- [test_mfb_default...ImgBuf bld2i done, VA: 0x%lx, PA: 0x%lx\n", ImgBuf_bld2i->getBufVA(0), ImgBuf_bld2i->getBufPA(0));
#elif (TEST_CASE == TEST_CASE_420_2P)
    IMEM_BUF_INFO buf_bld2i;
    buf_bld2i.size=bld2i_buf_size + bld2bi_buf_size;
    mpImemDrv->allocVirtBuf(&buf_bld2i);
    memset( (MUINT8*)buf_bld2i.virtAddr, 0xffffffff, buf_bld2i.size);    
    memcpy( (MUINT8*)(buf_bld2i.virtAddr), (MUINT8*)(mfb_input_bld2i), bld2i_buf_size);
    memcpy( (MUINT8*)(buf_bld2i.virtAddr + bld2i_buf_size), (MUINT8*)(mfb_input_bld2bi), bld2bi_buf_size);
    IImageBuffer* ImgBuf_bld2i = 0x0;
    MINT32 bufBoundaryInBytes2[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes2[3] = {bld2i_w, bld2i_w, 0};
    PortBufInfo_v1 portBufInfo2 = PortBufInfo_v1( buf_bld2i.memID,buf_bld2i.virtAddr,0,buf_bld2i.bufSecu, buf_bld2i.bufCohe);
    IImageBufferAllocator::ImgParam imgParam2 = IImageBufferAllocator::ImgParam((eImgFmt_NV12),MSize(bld2i_w, bld2i_h), bufStridesInBytes2, bufBoundaryInBytes2, 2);
    sp<ImageBufferHeap> pHeap2;
    pHeap2 = ImageBufferHeap::create( "basicMFB", imgParam2,portBufInfo2,true);
    ImgBuf_bld2i = pHeap2->createImageBuffer();
    ImgBuf_bld2i->incStrong(ImgBuf_bld2i);
    ImgBuf_bld2i->lockBuf("basicMFB",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [test_mfb_default...ImgBuf bld2i done, Plane1: VA: 0x%lx, PA: 0x%lx\n", ImgBuf_bld2i->getBufVA(0), ImgBuf_bld2i->getBufPA(0));
    printf("--- [test_mfb_default...ImgBuf bld2i done, Plane2: VA: 0x%lx, PA: 0x%lx\n", ImgBuf_bld2i->getBufVA(1), ImgBuf_bld2i->getBufPA(1));
#endif

    IMEM_BUF_INFO buf_bld3i;
    buf_bld3i.size=bld3i_buf_size;
    mpImemDrv->allocVirtBuf(&buf_bld3i);
    memset( (MUINT8*)buf_bld3i.virtAddr, 0xffffffff, buf_bld3i.size);
    memcpy( (MUINT8*)(buf_bld3i.virtAddr), (MUINT8*)(mfb_input_bld3i), buf_bld3i.size);
    IImageBuffer* ImgBuf_bld3i = 0x0;
    MINT32 bufBoundaryInBytes3[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes3[3] = {bld3i_w, 0, 0};
    PortBufInfo_v1 portBufInfo3 = PortBufInfo_v1( buf_bld3i.memID,buf_bld3i.virtAddr,0,buf_bld3i.bufSecu, buf_bld3i.bufCohe);
    IImageBufferAllocator::ImgParam imgParam3 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),MSize(bld3i_w, bld3i_h), bufStridesInBytes3, bufBoundaryInBytes3, 1);
    sp<ImageBufferHeap> pHeap3;
    pHeap3 = ImageBufferHeap::create( "basicMFB", imgParam3,portBufInfo3,true);
    ImgBuf_bld3i = pHeap3->createImageBuffer();
    ImgBuf_bld3i->incStrong(ImgBuf_bld3i);
    ImgBuf_bld3i->lockBuf("basicMFB",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);

    printf("--- [test_mfb_default...ImgBuf bld3i done, VA: 0x%lx, PA: 0x%lx\n", ImgBuf_bld3i->getBufVA(0), ImgBuf_bld3i->getBufPA(0));

    IMEM_BUF_INFO buf_bld4i;
    buf_bld4i.size=bld4i_buf_size;
    mpImemDrv->allocVirtBuf(&buf_bld4i);
    memset( (MUINT8*)buf_bld4i.virtAddr, 0xffffffff, buf_bld4i.size);
    memcpy( (MUINT8*)(buf_bld4i.virtAddr), (MUINT8*)(mfb_input_bld4i), buf_bld4i.size);
    IImageBuffer* ImgBuf_bld4i = 0x0;
    MINT32 bufBoundaryInBytes4[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes4[3] = {bld4i_w, 0, 0};
    PortBufInfo_v1 portBufInfo4 = PortBufInfo_v1( buf_bld4i.memID,buf_bld4i.virtAddr,0,buf_bld4i.bufSecu, buf_bld4i.bufCohe);
    IImageBufferAllocator::ImgParam imgParam4 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),MSize(bld4i_w, bld4i_h), bufStridesInBytes4, bufBoundaryInBytes4, 1);
    sp<ImageBufferHeap> pHeap4;
    pHeap4 = ImageBufferHeap::create( "basicMFB", imgParam4,portBufInfo4,true);
    ImgBuf_bld4i = pHeap4->createImageBuffer();
    ImgBuf_bld4i->incStrong(ImgBuf_bld4i);
    ImgBuf_bld4i->lockBuf("basicMFB",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);

    printf("--- [test_mfb_default...ImgBuf bld4i done, VA: 0x%lx, PA: 0x%lx\n", ImgBuf_bld4i->getBufVA(0), ImgBuf_bld4i->getBufPA(0));

#if (TEST_CASE == TEST_CASE_422_1P)
    IMEM_BUF_INFO buf_bldo;
    buf_bldo.size=bldo_buf_size;
    mpImemDrv->allocVirtBuf(&buf_bldo);
    hw_bldo_frmbuf = (char *)buf_bldo.virtAddr;
    memset( (MUINT8*)buf_bldo.virtAddr, 0xffffffff, buf_bldo.size);
    //memcpy( (MUINT8*)(buf_bldo.virtAddr), (MUINT8*)(mfb_output_bldo), sizeof(mfb_output_bldo));
    IImageBuffer* ImgBuf_bldo = 0x0;
    MINT32 bufBoundaryInBytes5[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes5[3] = {bldo_w*2, 0, 0};
    PortBufInfo_v1 portBufInfo5 = PortBufInfo_v1( buf_bldo.memID,buf_bldo.virtAddr,0,buf_bldo.bufSecu, buf_bldo.bufCohe);
    IImageBufferAllocator::ImgParam imgParam5 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(bldo_w, bldo_h), bufStridesInBytes5, bufBoundaryInBytes5, 1);
    sp<ImageBufferHeap> pHeap5;
    pHeap5 = ImageBufferHeap::create( "basicMFB", imgParam5, portBufInfo5, true);
    ImgBuf_bldo = pHeap5->createImageBuffer();
    ImgBuf_bldo->incStrong(ImgBuf_bldo);
    ImgBuf_bldo->lockBuf("basicMFB",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [test_mfb_default...ImgBuf bldo done, VA: 0x%lx, PA: 0x%lx\n", ImgBuf_bldo->getBufVA(0), ImgBuf_bldo->getBufPA(0));
#elif (TEST_CASE == TEST_CASE_420_2P)
    IMEM_BUF_INFO buf_bldo;
    buf_bldo.size=bldo_buf_size + bldbo_buf_size;
    mpImemDrv->allocVirtBuf(&buf_bldo);
    hw_bldo_frmbuf = (char *)buf_bldo.virtAddr;
    hw_bldbo_frmbuf = (char *)(buf_bldo.virtAddr + bldo_buf_size);
    memset( (MUINT8*)buf_bldo.virtAddr, 0xffffffff, buf_bldo.size);
    //memcpy( (MUINT8*)(buf_bldo.virtAddr), (MUINT8*)(mfb_output_bldo), sizeof(mfb_output_bldo));
    //memcpy( (MUINT8*)(buf_bldo.virtAddr + bldo_buf_size), (MUINT8*)(mfb_output_bldbo), sizeof(mfb_output_bldbo));
    IImageBuffer* ImgBuf_bldo = 0x0;
    MINT32 bufBoundaryInBytes5[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes5[3] = {bldo_w, bldo_w, 0};
    PortBufInfo_v1 portBufInfo5 = PortBufInfo_v1( buf_bldo.memID,buf_bldo.virtAddr,0,buf_bldo.bufSecu, buf_bldo.bufCohe);
    IImageBufferAllocator::ImgParam imgParam5 = IImageBufferAllocator::ImgParam((eImgFmt_NV12),MSize(bldo_w, bldo_h), bufStridesInBytes5, bufBoundaryInBytes5, 2);
    sp<ImageBufferHeap> pHeap5;
    pHeap5 = ImageBufferHeap::create( "basicMFB", imgParam5, portBufInfo5, true);
    ImgBuf_bldo = pHeap5->createImageBuffer();
    ImgBuf_bldo->incStrong(ImgBuf_bldo);
    ImgBuf_bldo->lockBuf("basicMFB",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [test_mfb_default...ImgBuf bldo done, Plane1: VA: 0x%lx, PA: 0x%lx\n", ImgBuf_bldo->getBufVA(0), ImgBuf_bldo->getBufPA(0));
    printf("--- [test_mfb_default...ImgBuf bldo done, Plane2: VA: 0x%lx, PA: 0x%lx\n", ImgBuf_bldo->getBufVA(1), ImgBuf_bldo->getBufPA(2));
#endif

    IMEM_BUF_INFO buf_bld2o;
    buf_bld2o.size=bld2o_buf_size;
    mpImemDrv->allocVirtBuf(&buf_bld2o);
    hw_bld2o_frmbuf = (char *)buf_bld2o.virtAddr;
    //memcpy( (MUINT8*)(buf_bld2o.virtAddr), (MUINT8*)(mfb_output_bld2o), buf_bld2o.size);
    IImageBuffer* ImgBuf_bld2o = 0x0;
    MINT32 bufBoundaryInBytes6[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes6[3] = {bld2o_w, 0, 0};
    PortBufInfo_v1 portBufInfo6 = PortBufInfo_v1( buf_bld2o.memID,buf_bld2o.virtAddr,0,buf_bld2o.bufSecu, buf_bld2o.bufCohe);
    IImageBufferAllocator::ImgParam imgParam6 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),MSize(bld2o_w, bld2o_h), bufStridesInBytes6, bufBoundaryInBytes6, 1);
    sp<ImageBufferHeap> pHeap6;
    pHeap6 = ImageBufferHeap::create( "basicMFB", imgParam6,portBufInfo6,true);
    ImgBuf_bld2o = pHeap6->createImageBuffer();
    ImgBuf_bld2o->incStrong(ImgBuf_bld2o);
    ImgBuf_bld2o->lockBuf("basicMFB",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);

    printf("--- [test_mfb_default...ImgBuf bld2o done, VA: 0x%lx, PA: 0x%lx\n", ImgBuf_bld2o->getBufVA(0), ImgBuf_bld2o->getBufPA(0));

    IMEM_BUF_INFO buf_tdri;
    buf_tdri.size=tdri_w*tdri_h;//tdri_buf_size;
    mpImemDrv->allocVirtBuf(&buf_tdri);
    //printf("--- [test_mfb_default...tdri memory copy\n");  
    //memcpy( (MUINT8*)(buf_tdri.virtAddr), (MUINT8*)(mfb_input_tdri), sizeof(mfb_input_tdri));
    IImageBuffer* ImgBuf_tdri = 0x0;
    MINT32 bufBoundaryInBytes7[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes7[3] = {tdri_w, 0, 0};
    PortBufInfo_v1 portBufInfo7 = PortBufInfo_v1( buf_tdri.memID,buf_tdri.virtAddr,0,buf_tdri.bufSecu, buf_tdri.bufCohe);
    IImageBufferAllocator::ImgParam imgParam7 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),MSize(tdri_w, tdri_h), bufStridesInBytes7, bufBoundaryInBytes7, 1);
    sp<ImageBufferHeap> pHeap7;
    pHeap7 = ImageBufferHeap::create( "basicMFB", imgParam7,portBufInfo7,true);
    printf("--- [test_mfb_default...create ImageBuffer\n");    
    ImgBuf_tdri = pHeap7->createImageBuffer();
    ImgBuf_tdri->incStrong(ImgBuf_tdri);
    ImgBuf_tdri->lockBuf("basicMFB",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);

    printf("--- [test_mfb_default...ImgBuf tdri done, VA: 0x%lx, PA: 0x%lx\n", ImgBuf_tdri->getBufVA(0), ImgBuf_tdri->getBufPA(0));

    printf("--- [test_mfb_default...start to config MFB\n");

    struct MFBConfig mfbconfig;

    mfbconfig.Mfb_inbuf_baseFrame = ImgBuf_bldi;
    mfbconfig.Mfb_inbuf_refFrame = ImgBuf_bld2i;
    mfbconfig.Mfb_inbuf_blendWeight = ImgBuf_bld3i;
    mfbconfig.Mfb_inbuf_confidenceMap = ImgBuf_bld4i;
    mfbconfig.Mfb_inbuf_tdri = ImgBuf_tdri;
    mfbconfig.Mfb_outbuf_frame = ImgBuf_bldo;
    mfbconfig.Mfb_outbuf_blendWeight = ImgBuf_bld2o;

    printf("--- [test_mfb_default...DMA Config done\n");
    mfbconfig.Mfb_bldmode = 1;
#if (TEST_CASE == TEST_CASE_422_1P)
    mfbconfig.Mfb_format = FMT_MFB_YUV422;
#elif (TEST_CASE == TEST_CASE_420_2P)
    mfbconfig.Mfb_format = FMT_MFB_YUV420_2P;
#endif
    mfbconfig.Mfb_srzEn = 1;

    printf("--- [test_mfb_default...Start to config SRZ5\n");
    mfbconfig.Mfb_srz5Info.in_w = 128;
    mfbconfig.Mfb_srz5Info.in_h = 64;
    mfbconfig.Mfb_srz5Info.out_w = 256;
    mfbconfig.Mfb_srz5Info.out_h = 128;
    mfbconfig.Mfb_srz5Info.crop_x = 0;
    mfbconfig.Mfb_srz5Info.crop_y = 0;
    mfbconfig.Mfb_srz5Info.crop_w = 128;
    mfbconfig.Mfb_srz5Info.crop_h = 64;

    printf("--- [test_mfb_default...Start to config tuning regs\n");
#if (TEST_CASE == TEST_CASE_422_1P)
    mfbconfig.Mfb_tuningReg.MFB_CON = 0x00009601;
    mfbconfig.Mfb_tuningReg.MFB_LL_CON1 = 0x07a20000;
    mfbconfig.Mfb_tuningReg.MFB_LL_CON2 = 0xbdac9b03;
    mfbconfig.Mfb_tuningReg.MFB_LL_CON3 = 0x2e7e1019;
    mfbconfig.Mfb_tuningReg.MFB_LL_CON4 = 0x00000000;
    mfbconfig.Mfb_tuningReg.MFB_EDGE = 0x0000000f;
    mfbconfig.Mfb_tuningReg.MFB_LL_CON5 = 0xc72f6d24;
    mfbconfig.Mfb_tuningReg.MFB_LL_CON6 = 0x8e1b7c2e;
    mfbconfig.Mfb_tuningReg.MFB_LL_CON7 = 0x0093fdfe;
    mfbconfig.Mfb_tuningReg.MFB_LL_CON8 = 0x00332b2a;
    mfbconfig.Mfb_tuningReg.MFB_LL_CON9 = 0x002b2516;
    mfbconfig.Mfb_tuningReg.MFB_LL_CON10 = 0x00009a01;
    mfbconfig.Mfb_tuningReg.MFB_MBD_CON0 = 0x00000000;
    mfbconfig.Mfb_tuningReg.MFB_MBD_CON1 = 0x02020201;
    mfbconfig.Mfb_tuningReg.MFB_MBD_CON2 = 0x23220a0e;
    mfbconfig.Mfb_tuningReg.MFB_MBD_CON3 = 0x2d2c2f2e;
    mfbconfig.Mfb_tuningReg.MFB_MBD_CON4 = 0x2e332e2e;
    mfbconfig.Mfb_tuningReg.MFB_MBD_CON5 = 0x3e2e2e2e;
    mfbconfig.Mfb_tuningReg.MFB_MBD_CON6 = 0x07000000;
    mfbconfig.Mfb_tuningReg.MFB_MBD_CON7 = 0x0202022b;
    mfbconfig.Mfb_tuningReg.MFB_MBD_CON8 = 0x04002d0f;
    mfbconfig.Mfb_tuningReg.MFB_MBD_CON9 = 0x00343404;
    mfbconfig.Mfb_tuningReg.MFB_MBD_CON10 = 0x34340606;

    mfbconfig.Mfb_tuningBuf = (unsigned int *)malloc(4 * 23);
    unsigned int tuningBuf[23] = {0x00009601, 0x07a20000, 0xbdac9b03, 0x2e7e1019, 0x00000000, 0x0000000f, 0xc72f6d24,
                                    0x8e1b7c2e, 0x0093fdfe, 0x00332b2a, 0x002b2516, 0x00009a01, 0x00000000, 0x02020201,
                                    0x23220a0e, 0x2d2c2f2e, 0x2e332e2e, 0x3e2e2e2e, 0x07000000, 0x0202022b, 0x04002d0f,
                                    0x00343404, 0x34340606};
    memcpy(mfbconfig.Mfb_tuningBuf, (void *)tuningBuf, 4*23);
#elif (TEST_CASE == TEST_CASE_420_2P)
    mfbconfig.Mfb_tuningReg.MFB_CON = 0x00009601; 
    mfbconfig.Mfb_tuningReg.MFB_LL_CON1 = 0x32E70000; 
    mfbconfig.Mfb_tuningReg.MFB_LL_CON2 = 0xDBC56B06; 
    mfbconfig.Mfb_tuningReg.MFB_LL_CON3 = 0x2E7E1019; 
    mfbconfig.Mfb_tuningReg.MFB_LL_CON4 = 0x0; 
    mfbconfig.Mfb_tuningReg.MFB_EDGE = 0xF; 
    mfbconfig.Mfb_tuningReg.MFB_LL_CON5 = 0x382EE11C; 
    mfbconfig.Mfb_tuningReg.MFB_LL_CON6 = 0xE816AC23; 
    mfbconfig.Mfb_tuningReg.MFB_LL_CON7 = 0x7FA2FC; 
    mfbconfig.Mfb_tuningReg.MFB_LL_CON8 = 0x1A3828; 
    mfbconfig.Mfb_tuningReg.MFB_LL_CON9 = 0xF3523; 
    mfbconfig.Mfb_tuningReg.MFB_LL_CON10 = 0xAA01; 
    mfbconfig.Mfb_tuningReg.MFB_MBD_CON0 = 0x0; 
    mfbconfig.Mfb_tuningReg.MFB_MBD_CON1 = 0x3C1E1409; 
    mfbconfig.Mfb_tuningReg.MFB_MBD_CON2 = 0x2214113E; 
    mfbconfig.Mfb_tuningReg.MFB_MBD_CON3 = 0xB0B3F3F; 
    mfbconfig.Mfb_tuningReg.MFB_MBD_CON4 = 0x3E1D0E0B; 
    mfbconfig.Mfb_tuningReg.MFB_MBD_CON5 = 0x3F3F3F3F; 
    mfbconfig.Mfb_tuningReg.MFB_MBD_CON6 = 0x3835291F; 
    mfbconfig.Mfb_tuningReg.MFB_MBD_CON7 = 0x3B3B3B3B; 
    mfbconfig.Mfb_tuningReg.MFB_MBD_CON8 = 0x2013B3B; 
    mfbconfig.Mfb_tuningReg.MFB_MBD_CON9 = 0x72A1F1B; 
    mfbconfig.Mfb_tuningReg.MFB_MBD_CON10 = 0x2F2D2307;

    mfbconfig.Mfb_tuningBuf = (unsigned int *)malloc(4 * 23);
    unsigned int tuningBuf[23] = {0x00009601, 0x32E70000, 0xDBC56B06, 0x2E7E1019, 0x00000000, 0x0000000F, 0x382EE11C,
                                    0xE816AC23, 0x007FA2FC, 0x001A3828, 0x000F3523, 0x0000AA01, 0x00000000, 0x3C1E1409,
                                    0x2214113E, 0x0B0B3F3F, 0x3E1D0E0B, 0x3F3F3F3F, 0x3835291F, 0x3B3B3B3B, 0x02013B3B,
                                    0x072A1F1B, 0x2F2D2307};
    memcpy(mfbconfig.Mfb_tuningBuf, (void *)tuningBuf, 4*23);

#endif  


    printf("--- [test_mfb_default...start to push_back\n");
    rMfbParams.mEGNConfigVec.push_back(mfbconfig);

    g_bMFBCallback = MFALSE;

    printf("--- [test_mfb_default...start to enque\n");

    /*
    *   enque the request(s)
    */
    ret=pStream->EGNenque(rMfbParams);
    if(!ret)
    {
        printf("---MFB Enque ERRRRRRRRR [.mfb enque fail\n]");
    }
    else
    {
        printf("---MFB Enque [.mfb enque done\n]");
    }

    do{
        usleep(100000);
        if (MTRUE == g_bMFBCallback)
        {
            break;
        }
    }while(1);

    do
    {
        //Check Bit True Pass or Not?
        printf("bit true start: BLDO\n");
        if (BitTrueCheck(hw_bldo_frmbuf, golden_bldo_frmbuf, bldo_buf_size) == false)
        {
            printf("bldo bit true fail!!\n");
            while(1);
            break;
        }
        printf("bit true start: BLD2O\n");        
        if (BitTrueCheck(hw_bld2o_frmbuf, golden_bld2o_frmbuf, bld2o_buf_size) == false)
        {
            printf("bld2o bit true fail!!\n");
            while(1);
            break;
        }
#if (TEST_CASE == TEST_CASE_420_2P)        
        printf("bit true start: BLDBO\n");        
        if (BitTrueCheck(hw_bldbo_frmbuf, golden_bldbo_frmbuf, bldbo_buf_size) == false)
        {
            printf("bldbo bit true fail!!\n");
            while(1);
            break;
        }
#endif
        printf("mfb bittrue is pass!!\n");
        break;
    }while (1);


#if 0
    ImgBuf_bldi->unlockBuf("basicMFB");
    mpImemDrv->freeVirtBuf(&buf_bldi);
    ImgBuf_bld2i->unlockBuf("basicMFB");
    mpImemDrv->freeVirtBuf(&buf_bld2i);
    ImgBuf_bld3i->unlockBuf("basicMFB");
    mpImemDrv->freeVirtBuf(&buf_bld3i);
    ImgBuf_bld4i->unlockBuf("basicMFB");
    mpImemDrv->freeVirtBuf(&buf_bld4i);
    ImgBuf_bldo->unlockBuf("basicMFB");
    mpImemDrv->freeVirtBuf(&buf_bldo);
    ImgBuf_bld2o->unlockBuf("basicMFB");
    mpImemDrv->freeVirtBuf(&buf_bld2o);
    ImgBuf_tdri->unlockBuf("basicMFB");
    mpImemDrv->freeVirtBuf(&buf_tdri);
#endif
    mpImemDrv->uninit();
    pStream->uninit();

    return ret;
}



#if 0 
void* MfbThreadLoop(void *arg)
{
    int ret = 0; 

    ::pthread_detach(::pthread_self());
    ::sem_wait(&MfbSem);

    while (g_bMfbThreadTerminated) {

        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("Sub Thread do mfb_test_case_00_frame_3_MFB_Config\n");
        ret=mfb_test_case_00_frame_3_MFB_Config();
        printf("Sub Thread do mfb_test_case_00_frame_4_MFB_Config\n");
        ret=mfb_test_case_00_frame_4_MFB_Config();
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
    }
    ::sem_post(&MfbSem);
    printf("###################Sub  Wait###################\n");
    printf("###################Sub  Wait###################\n");
    printf("###################Sub  Wait###################\n");
    return NULL;
}
void* MfbThreadLoop_OneReqHaveOneBuffer(void *arg)
{
    int ret = 0; 

    ::pthread_detach(::pthread_self());
    ::sem_wait(&MfbSem);

    while (g_bMfbThreadTerminated) {

        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        ret = multi_enque_mfb_test_case_00_frame_2_MFB_Config();
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
    }
    ::sem_post(&MfbSem);
    printf("###################Sub  Wait###################\n");
    printf("###################Sub  Wait###################\n");
    printf("###################Sub  Wait###################\n");
    return NULL;
}
void* MfbThreadLoop_OneReqHaveTwoBuffer(void *arg)
{
    int ret = 0; 

    ::pthread_detach(::pthread_self());
    ::sem_wait(&MfbSem);

    while (g_bMfbThreadTerminated) {

        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        ret = multi_enque_mfb_test_case_00_frame_2_MFB_Config();
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
    }
    ::sem_post(&MfbSem);
    printf("###################Sub  Wait###################\n");
    printf("###################Sub  Wait###################\n");
    printf("###################Sub  Wait###################\n");
    return NULL;
}
#endif
int test_mfbStream(int argc, char** argv)
{
    int ret = 0; 
    int testCase = 0;
    int i;
    if (argc>=1)
    {
        testCase = atoi(argv[0]);
    }

    switch(testCase)
    {
#if 0    
        case 0:  //Unit Test Case One MFB Request.
            //g_MFB_UnitTest_Num = MFB_DEFAULT_UT; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            ret = mfb_default();
            break;
#endif            
        case 2: //Two mfb requests at the same time.
            //g_MFB_UnitTest_Num = MFB_TESTCASE_UT_0; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            g_bMultipleBuffer = 0;
            ret = multi_enque_mfb_tc00_frames_MFB_Config();

            //g_frame2_bOneRequestHaveManyBuffer = 0;
            //ret = multi_enque_mfb_test_case_00_frame_2_MFB_Config();
            break;
        default:
            break;
    }
    
    ret = 1;
    return ret; 
}

MVOID MFBCallback(EGNParams<MFBConfig>& rParams)
{
    (void) rParams;
    int i = 0 ;
    int size = rParams.mEGNConfigVec.size();
    for (; i < size; i++){
        MUINT32 reg1,reg2;
        reg1 = rParams.mEGNConfigVec.at(i).feedback.reg1;
        reg2 = rParams.mEGNConfigVec.at(i).feedback.reg2;
        printf("Feedback statistics are:(0x%x,0x%x)\n", reg1,reg2);
    }
    printf("--- [MFB callback func]\n");
    g_bMFBCallback = MTRUE;

    
}

/******************************************************************************
* save the buffer to the file
*******************************************************************************/
static bool
saveBufToFile(char const*const fname, MUINT8 *const buf, MUINT32 const size)
{
    int nw, cnt = 0;
    uint32_t written = 0;

    //LOG_INF("(name, buf, size) = (%s, %x, %d)", fname, buf, size);
    //LOG_INF("opening file [%s]\n", fname);
    int fd = ::open(fname, O_RDWR | O_CREAT, S_IRWXU);
    if (fd < 0) {
        printf(": failed to create file [%s]: %s \n", fname, ::strerror(errno));
        return false;
    }

    //LOG_INF("writing %d bytes to file [%s]\n", size, fname);
    while (written < size) {
        nw = ::write(fd,
                     buf + written,
                     size - written);
        if (nw < 0) {
            printf(": failed to write to file [%s]: %s\n", fname, ::strerror(errno));
            break;
        }
        written += nw;
        cnt++;
    }
    //LOG_INF("done writing %d bytes to file [%s] in %d passes\n", size, fname, cnt);
    ::close(fd);
    return true;
}



