/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
 *     TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#ifndef _ISENINF_DRV_H_
#define _ISENINF_DRV_H_

#include "seninf_drv_def.h"

/*******************************************************************************
*
********************************************************************************/
class SeninfDrv {
public:
    static SeninfDrv* getInstance();
#ifdef CONFIG_MTK_CAM_SECURE
    static SeninfDrv* getInstance(MBOOL isSecure);
#endif
    virtual int setSeninfCsi(void *pCsi, SENINF_SOURCE_ENUM src) = 0;
    virtual int init() = 0;
    virtual int uninit() = 0;
    virtual int configMclk(SENINF_MCLK_PARA *pmclk_para, unsigned long pcEn) = 0;
    virtual int sendCommand(int cmd, unsigned long arg1 = 0, unsigned long arg2 = 0, unsigned long arg3 = 0) = 0;
    virtual int setSeninfTopMuxCtrl(SENINF_MUX_ENUM seninfMuXIdx, SENINF_ENUM seninfSrc) = 0;
    virtual int setSeninfCamTGMuxCtrl(unsigned int targetCamTG, SENINF_MUX_ENUM muxSrc) = 0;
    virtual int getSeninfTopMuxCtrl(SENINF_MUX_ENUM mux) = 0;
    virtual int getSeninfCamTGMuxCtrl(unsigned int targetCam) = 0;
    virtual int setSeninfVC(SENINF_ENUM SenInfsel, unsigned int vc0Id, unsigned int vc1Id, unsigned int vc2Id, unsigned int vc3Id, unsigned int vc4Id, unsigned int vc5Id) = 0;
    virtual int setSeninfMuxCtrl(SENINF_MUX_ENUM mux, unsigned long hsPol, unsigned long vsPol, SENINF_SOURCE_ENUM inSrcTypeSel, TG_FORMAT_ENUM inDataType, unsigned int twoPxlMode) = 0;
    virtual MBOOL isMUXUsed(SENINF_MUX_ENUM mux) = 0;
    virtual int enableMUX(SENINF_MUX_ENUM mux) = 0;
    virtual int disableMUX(SENINF_MUX_ENUM mux) = 0;
    virtual int setMclk(MUINT8 mclkIdx, MBOOL pcEn, unsigned long mclkSel) = 0;
    virtual int setMclkIODrivingCurrent(MUINT32 mclkIdx, unsigned long ioDrivingCurrent) = 0;
    virtual int setTestModel(bool en, unsigned int dummypxl,unsigned int vsync, unsigned int line, unsigned int pxl) = 0;
    virtual int getN3DDiffCnt(MUINT32 *pCnt) = 0;
    virtual SENINF_CSI_INFO* getCSIInfo(CUSTOM_CFG_CSI_PORT mipiPort) = 0;
    virtual int powerOff(void *pCsi) = 0;
    virtual int mutexLock() = 0;
    virtual int mutexUnlock() = 0;
    virtual int reset(SENINF_ENUM seninf) = 0;
protected:
    virtual ~SeninfDrv() {};
};

#endif

