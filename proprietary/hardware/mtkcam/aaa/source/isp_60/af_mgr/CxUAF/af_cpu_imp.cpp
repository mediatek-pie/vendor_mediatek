/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#define LOG_TAG "af_cpu_v3"

#ifndef ENABLE_MY_LOG
#define ENABLE_MY_LOG       (1)
#endif

#include <utils/threads.h>  // For Mutex::Autolock.
#include <cutils/properties.h>
#include <aaa_types.h>
#include <aaa_error_code.h>
#include <kd_camera_feature.h>
#include <mtkcam/utils/std/Log.h>
//#include <faces.h>
#include <private/aaa_hal_private.h>
#include <aaa_hal_if.h>
#include <camera_custom_nvram.h>
#include <af_param.h>
#include <pd_param.h>
#include <awb_param.h>
#include <ae_param.h>
#include <af_tuning_custom.h>
#include <drv/isp_reg.h>
//
#include <cct_feature.h>
#include <flash_param.h>
#include <isp_tuning.h>
//#include <isp_tuning_mgr.h>
#include <mcu_drv.h>

#include <af_feature.h>
class NvramDrvBase;
using namespace android;
#include <dbg_aaa_param.h>
#include <dbg_af_param.h>

#include "af_cpu_imp.h"

using namespace NS3Av3;


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
IAfCxU* AfCPU::getInstance(MINT32 sensorDev)
{
    if (sensorDev == ESensorDev_MainSecond)
    {
        static AfCPU singleton2(sensorDev);
        return &singleton2;
    }
    else if (sensorDev == ESensorDev_Sub)
    {
        static AfCPU singleton3(sensorDev);
        return &singleton3;
    }
    else if (sensorDev == ESensorDev_SubSecond)
    {
        static AfCPU singleton4(sensorDev);
        return &singleton4;
    }
    else
    {
        static AfCPU singleton1(sensorDev);
        return &singleton1;
    }
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL AfCPU::init(MINT32 sensorDev, MINT32 sensorIdx)
{
    CAM_LOGD("%s ", __FUNCTION__);

    m_sensorDev = sensorDev;
    m_sensorIdx = sensorIdx;

    return MTRUE;
}


MBOOL AfCPU::config(ConfigAFInput_T &configInput, ConfigAFOutput_T &configOutput)
{
    CAM_LOGD("%s ", __FUNCTION__);

    // First time HW setting is got after calling initAF.
    ConfigHWReg(*(configInput.defaultConfig), configOutput);

    return MTRUE;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL AfCPU::start(InitAFInput_T input, InitAFOutput_T &output)
{
    CAM_LOGD("%s ", __FUNCTION__);
    // Get hybrid AF instance.
#if USE_OPEN_SOURCE_AF
    m_pIAfAlgo = NS3A::IAfAlgo::createInstance<NS3A::EAAAOpt_OpenSource>( m_sensorDev);
#else
    m_pIAfAlgo = NS3A::IAfAlgo::createInstance<NS3A::EAAAOpt_MTK>( m_sensorDev);
#endif

    if( m_pIAfAlgo)
    {
        m_pIAfAlgo->setAFParam( *(input.defaultParam), *(input.defaultCfg), *(input.afNVRAM), *(input.dualCamNVRAM));
        m_pIAfAlgo->initAF( *(input.afInput), output);
        m_pIAfAlgo->setAFMode( input.libAfMode);
        //m_pIAfAlgo->updateAFtableBoundary( input.otpInfPos, input.otpMacroPos, input.otpAfTableStr, input.otpAfTableEnd);
#if 0 // fix build error
        m_pIAfAlgo->updateAFtableBoundary( input.otpInfPos, input.otpMacroPos);
        m_pIAfAlgo->updateMiddleAFPos(input.otpMiddlePos);
#endif
    }


    return MTRUE;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL AfCPU::stop()
{
    CAM_LOGD("%s ", __FUNCTION__);

    // uninit hybrid AF
    if( m_pIAfAlgo)
    {
        m_pIAfAlgo->destroyInstance();
        m_pIAfAlgo = NULL;
    }
    return MTRUE;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL AfCPU::uninit()
{
    CAM_LOGD("%s ", __FUNCTION__);

    return MTRUE;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL AfCPU::resume()
{
    CAM_LOGD("%s ", __FUNCTION__);
    return MTRUE;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL AfCPU::suspend()
{
    CAM_LOGD("%s ", __FUNCTION__);
    return MTRUE;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL AfCPU::abort()
{
    CAM_LOGD("%s ", __FUNCTION__);
    return MTRUE;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL AfCPU::doAF(DoAFInput_T AFINPUT)
{
    CAM_LOGD("%s ", __FUNCTION__);
//    m_pIAfAlgo->handleAF( AFINPUT, AFOUTPUT);
//    ConfigHWReg(*(configInput.defaultConfig), configOutput);

    return MTRUE;
}
MBOOL AfCPU::getOutput(DoAFOutput_T &AFOUTPUT)
{
    CAM_LOGD("%s ", __FUNCTION__);
    return MTRUE;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL AfCPU::ConfigHWReg(AF_CONFIG_T &sInHWCfg, ConfigAFOutput_T &ConfigOutput)
{
    //-------------
    // AF HAL control flow :
    //-------------
    CAM_LOGD( "%s Dev(%d): before config : ROI (X,Y,W,H)=( %d, %d, %d, %d)",
              __FUNCTION__,
              m_sensorDev,
              sInHWCfg.sRoi.i4X,
              sInHWCfg.sRoi.i4Y,
              sInHWCfg.sRoi.i4W,
              sInHWCfg.sRoi.i4H);

    CAM_LOGD( "%s Dev(%d): before config : BLKNUM (%d,%d), BLKSIZE (%d,%d)",
              __FUNCTION__,
              m_sensorDev,
              *(ConfigOutput.hwConfigInfo.hwBlkNumX),
              *(ConfigOutput.hwConfigInfo.hwBlkNumY),
              *(ConfigOutput.hwConfigInfo.hwBlkSizeX),
              *(ConfigOutput.hwConfigInfo.hwBlkSizeY));

    ISP_AF_CONFIG_T::getInstance(m_sensorDev).AFConfig(&sInHWCfg, &ConfigOutput.hwConfigInfo, ConfigOutput.resultConfig);

    CAM_LOGD( "%s Dev(%d): after config : ROI (X,Y,W,H)=( %d, %d, %d, %d)",
              __FUNCTION__,
              m_sensorDev,
              sInHWCfg.sRoi.i4X,
              sInHWCfg.sRoi.i4Y,
              sInHWCfg.sRoi.i4W,
              sInHWCfg.sRoi.i4H);

    CAM_LOGD( "%s Dev(%d): after config : BLKNUM (%d,%d), BLKSIZE (%d,%d)",
              __FUNCTION__,
              m_sensorDev,
              *(ConfigOutput.hwConfigInfo.hwBlkNumX),
              *(ConfigOutput.hwConfigInfo.hwBlkNumY),
              *(ConfigOutput.hwConfigInfo.hwBlkSizeX),
              *(ConfigOutput.hwConfigInfo.hwBlkSizeY));



    // error log : should not be happened.
    if( sInHWCfg.AF_BLK_XNUM != *(ConfigOutput.hwConfigInfo.hwBlkNumX) || sInHWCfg.AF_BLK_YNUM != *(ConfigOutput.hwConfigInfo.hwBlkNumY) )
    {
        CAM_LOGE( "BLKNUM has been OVERWRITED !!! : [ROI][X,Y,W,H] (%d,%d,%d,%d) -> (%d,%d,%d,%d), [BLKNUM][X,Y] (%d,%d) -> (%d,%d)",
                  sInHWCfg.sRoi.i4X,       sInHWCfg.sRoi.i4Y,       sInHWCfg.sRoi.i4W,       sInHWCfg.sRoi.i4H,
                  ConfigOutput.hwConfigInfo.hwArea->i4X, ConfigOutput.hwConfigInfo.hwArea->i4Y, ConfigOutput.hwConfigInfo.hwArea->i4W, ConfigOutput.hwConfigInfo.hwArea->i4H,
                  sInHWCfg.AF_BLK_XNUM,   sInHWCfg.AF_BLK_YNUM,
                  *(ConfigOutput.hwConfigInfo.hwBlkNumX), *(ConfigOutput.hwConfigInfo.hwBlkNumY));
    }

    return S_AF_OK;
}

