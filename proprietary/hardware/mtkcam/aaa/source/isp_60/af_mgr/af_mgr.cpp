/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#define LOG_TAG "af_mgr_v3"

#ifndef ENABLE_MY_LOG
#define ENABLE_MY_LOG       (1)
#endif

#include <utils/threads.h>  // For Mutex::Autolock.
#include <sys/stat.h>
#include <sys/time.h>
#include <cutils/properties.h>
#include <cutils/atomic.h>
#include <aaa_types.h>
#include <aaa_error_code.h>
#include <kd_camera_feature.h>
#include <mtkcam/utils/std/Log.h>
#include <mtkcam/utils/std/Trace.h>
#include <aaa_trace.h>
#include <faces.h>
#include <private/aaa_hal_private.h>
#include <camera_custom_nvram.h>
#include <af_param.h>
#include <pd_param.h>
#include <af_tuning_custom.h>
#include <mcu_drv.h>
#include <mtkcam/drv/IHalSensor.h>
#include <mtkcam/utils/sys/SensorProvider.h>
#include <cct_feature.h>
#include <af_feature.h>
#include <af_define.h>

#include <dbg_aaa_param.h>
#include <dbg_af_param.h>

#include "af_mgr.h"
#include <nvbuf_util.h>
#include "aaa_common_custom.h"
#include <pd_mgr_if.h>
#include "private/PDTblGen.h"
#include <laser_mgr_if.h>
//
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>

//
#include "mtkcam/utils/metadata/client/mtk_metadata_tag.h"

//configure HW
#include <StatisticBuf.h>

#include <math.h>
#include <android/sensor.h>             // for g/gyro sensor listener
#include <mtkcam/utils/sys/SensorListener.h>    // for g/gyro sensor listener


#define SENSOR_ACCE_POLLING_MS  20
#define SENSOR_GYRO_POLLING_MS  20
#define SENSOR_ACCE_SCALE       100
#define SENSOR_GYRO_SCALE       100

#define LASER_TOUCH_REGION_W    0
#define LASER_TOUCH_REGION_H    0

#define AF_ENLOG_STATISTIC 2
#define AF_ENLOG_ROI 4

#define DBG_MSG_BUF_SZ 1024 //byte

#if defined(HAVE_AEE_FEATURE)
#include <aee.h>
#define AEE_ASSERT_AF(String) \
    do { \
        aee_system_warning( \
                            "af_mgr", \
                            NULL, \
                            DB_OPT_DEFAULT|DB_OPT_FTRACE, \
                            String); \
    } while(0)
#else
#define AEE_ASSERT_AF(String)
#endif

// LaunchCamTrigger
#define AESTABLE_TIMEOUT 0
#define VALIDPD_TIMEOUT  0

#define GYRO_THRESHOLD 15
#define MAX_PDMOVING_COUNT 6

using namespace NS3Av3;
using namespace NSCam;
using namespace NSIoPipe;
using namespace NSCamIOPipe;
using namespace NSCam::Utils;

#define CXUAF_ENABLE 1

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
AfMgr& AfMgr::getInstance( MINT32 sensorDev)
{
    if (sensorDev == ESensorDev_MainSecond)
    {
        static AfMgr singleton2(sensorDev);
        return singleton2;
    }
    else if (sensorDev == ESensorDev_Sub)
    {
        static AfMgr singleton3(sensorDev);
        return singleton3;
    }
    else if (sensorDev == ESensorDev_SubSecond)
    {
        static AfMgr singleton4(sensorDev);
        return singleton4;
    }
    else
    {
        static AfMgr singleton1(sensorDev);
        return singleton1;
    }
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
AfMgr::AfMgr( MINT32 eSensorDev) :
    m_i4CurrSensorDev(eSensorDev),
    m_i4Users(0),
    m_CCTUsers(0),
    m_pMcuDrv(MNULL),
    m_pIAfAlgo(m_pIAfAlgo),
    m_pAFParam(MNULL),
    m_bLDAFEn(MFALSE),
    m_bGryoVd(MFALSE),
    m_bAcceVd(MFALSE),
    s_pIAfCxU(MNULL),
    m_ptrNVRam(MNULL),
    m_i4CurrSensorId(0x1),
    m_i4CurrModuleId(-1),
    m_i4CurrLensId(0),
    m_i4DgbLogLv(0),
    m_i4EnableAF(-1)
{
    memset( &m_sAFInput,       0, sizeof(AF_INPUT_T));
    memset( &m_sAFOutput,      0, sizeof(AF_OUTPUT_T));
    memset( &m_sArea_Center,   0, sizeof(AREA_T));
    memset( &m_sArea_APCmd,    0, sizeof(AREA_T));
    memset( &m_sArea_APCheck,  0, sizeof(AREA_T));
    memset( &m_sArea_OTFD,     0, sizeof(AREA_T));
    memset( &m_sArea_Bokeh,    0, sizeof(AREA_T));
    memset( &m_sCallbackInfo,  0, sizeof(CHECK_AF_CALLBACK_INFO_T));
    memset( &m_sIspSensorInfo, 0, sizeof(ISP_SENSOR_INFO_T));

    m_i4TGSzW = m_i4TGSzH = 0;
    m_i4BINSzW = m_i4BINSzH = 0;
    m_eCurAFMode     = MTK_CONTROL_AF_MODE_EDOF; /*force to waitting AP send setting mode command. [Ref : setafmode function]*/
    m_bIsFullScan    = FALSE;
    m_aCurEMAFFlag   = m_aPreEMAFFlag = 0;   // for EMAF mode select: FullScan / AdvFullScan / Temperature Calibration
    m_aAdvFSRepeatTime = 10;
    m_i4FullScanStep = 0;
    m_AETargetMode   = AE_MODE_NORMAL;
    m_eLIB3A_AFMode  = LIB3A_AF_MODE_AFS;
    m_i4SensorIdx    = 0;
    m_i4SensorMode   = SENSOR_SCENARIO_ID_NORMAL_PREVIEW;
    m_ptrIHalSensor          = NULL;
    m_bEnablePD      = MFALSE;
    m_i4PDAF_support_type    = PDAF_SUPPORT_NA;
    m_ptrLensNVRam           = NULL;
    m_eCamScenarioMode       = AF_CAM_SCENARIO_NUM;
    m_eNVRAMIndex            = AF_CAM_SCENARIO_NUM_2;
    m_i4OISDisable           = MFALSE;
    m_i4InitPos              = 0;
    m_i4LensPosExit          = 0;
    mpSensorProvider         = nullptr;
    m_bNeedCheckSendCallback = MFALSE;
    i4IsLockAERequest = 0;
    // LaunchCamTrigger : disable at the first
    m_i4LaunchCamTriggered_Prv = m_i4LaunchCamTriggered = E_LAUNCH_AF_IDLE;
    m_i4AEStableFrameCount = -1;
    m_i4ValidPDFrameCount = -1;
    m_i4AEStableTriggerTimeout = AESTABLE_TIMEOUT;
    m_i4ValidPDTriggerTimeout = VALIDPD_TIMEOUT;
    m_i4AFMaxAreaNum = -1;
    m_i4IsLockForLaunchCamTrigger = 0;

    m_i4IsCAFWithoutFace = 0;

    m_bForceDoAlgo = MFALSE;
    m_i4isAEStable = MFALSE;
    memset(m_u1LensFileName, 0, sizeof(MUINT8)*32);
    m_u1LensFileName[0] = '\0';
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
AfMgr::~AfMgr()
{}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MINT32 AfMgr::init( MINT32 i4SensorIdx)
{
    CAM_LOGD( "AF-CMD-%-15s: + Dev %d", __FUNCTION__, m_i4CurrSensorDev);

    MINT32 ret = 1;

    Mutex::Autolock lock(m_Lock);

    if( m_i4Users==0)
    {
        /* Init af_mgr, when no user calls af_mgr init.*/

        // initial property
        m_i4DgbLogLv        = property_get_int32("vendor.debug.af_mgr.enable", 0);
        m_i4DbgAfegainQueue = property_get_int32("vendor.debug.afegainqueue", 0);
        m_i4DbgPDVerifyEn   = property_get_int32("vendor.debug.pd_verify_flow.enable", 0);

        /**
         * initial nonvolatilize data :
         * Which meas that the following parameters will not be changed once sensor mode is changed.
         * When sensor mode is changed, stop/start will be executed.
         */

        memset( &m_sMgrExif[0],    0, sizeof( AAA_DEBUG_TAG_T)*MGR_EXIF_SIZE);
        memset( &m_sMgrCapExif[0], 0, sizeof( AAA_DEBUG_TAG_T)*MGR_CAPTURE_EXIF_SIZE);
        memset( &m_sMgrTSExif[0],  0, sizeof( AAA_DEBUG_TAG_T)*MGR_TS_EXIF_SIZE);

        // initial flow control parameters.
        m_i4MgrExifSz      = 0;
        m_i4MgrCapExifSz   = 0;
        m_i4MgrTsExifSz    = 0;
        m_i4SensorIdx      = i4SensorIdx;
        m_bMZHostEn        = 0;
        m_eCamScenarioMode = AF_CAM_SCENARIO_NUM; /* update in setCamScenarioMode function*/
        m_eNVRAMIndex      = AF_CAM_SCENARIO_NUM_2;
        m_i4AFMaxAreaNum   = -1;
        m_bPauseAF         = MFALSE;
        m_i4UnPauseReqNum  = 0;
        m_i4IsEnableFVInFixedFocus = -1;
        s_pIAfCxU = MNULL;

        // Get hybrid AF instance.
#if USE_OPEN_SOURCE_AF
        m_pIAfAlgo=NS3A::IAfAlgo::createInstance<NS3A::EAAAOpt_OpenSource>( m_i4CurrSensorDev);
#else
        m_pIAfAlgo=NS3A::IAfAlgo::createInstance<NS3A::EAAAOpt_MTK>( m_i4CurrSensorDev);
#endif

        initSD();

        initLD();

        /* Sensor Provider */
        initSP();

        ret = 0;
    }
    else
    {
        /**
         *  Do not init af_mgr :
         *  1. User Cnt >= 1 : af_mgr is still used.
         *  2. User Cnt   < 0  : wrong host flow.
         */
        CAM_LOGD( "AF-CMD-%-15s: no init, user %d", __FUNCTION__, m_i4Users);
    }

    android_atomic_inc( &m_i4Users);
    CAM_LOGD( "AF-CMD-%-15s: EnAF %d, Algo(%p), users %d", __FUNCTION__, m_i4EnableAF, (void*)m_pIAfAlgo, m_i4Users);

    return ret;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MINT32 AfMgr::camPwrOn()
{
    // To reset parameter for starting a camera.

    CAM_LOGD( "AF-CMD-%-15s: + Dev %d", __FUNCTION__, m_i4CurrSensorDev);

    memset( &m_sCurLensInfo,   0, sizeof( LENS_INFO_T));
    m_eCurAFMode    = MTK_CONTROL_AF_MODE_EDOF;
    m_eLIB3A_AFMode = LIB3A_AF_MODE_OFF;
    m_eAFStatePre   = m_eAFState = E_AF_INACTIVE;
    m_bLock          = MFALSE;
    m_bNeedLock      = MFALSE;
    m_bLatchROI           = MFALSE;
    m_bWaitForceTrigger   = MFALSE;
    m_bForceTrigger       = MFALSE;
    m_bTriggerCmdVlid     = MFALSE;
    m_ptrLensNVRam        = NULL;
    m_i4MFPos             = -1;
    m_i4InitPos           = 0;
    m_i4DbgMotorMPos      = 1024;
    m_i4DbgMotorMPosPre   = 1024;
    m_i4DbgMotorDisable   = 0;

    CAM_LOGD( "AF-CMD-%-15s: -", __FUNCTION__);

    return 0;
}

MINT32 AfMgr::config()
{
    CAM_LOGD( "AF-CMD-%-15s: + Dev %d", __FUNCTION__, m_i4CurrSensorDev);
    Mutex::Autolock lock( m_Lock);
    MINT32 err=0;
    resetAFParams();
    ISP_AF_CONFIG_T::getInstance(m_i4CurrSensorDev).resetParam();

    if(!s_pIAfCxU)
    {
        s_pIAfCxU = IAfCxU::getInstance(m_i4CurrSensorDev);
        if(!s_pIAfCxU)
        {
            CAM_LOGE("AF-CMD-%-15s: Dev(0x%04x), IAfCxU pointer NULL", __FUNCTION__, m_i4CurrSensorDev);
            err=1;
        }
    }
    /*--------------------------------------------------------------------------------------------------------
     *                                       get sensor related information
     *--------------------------------------------------------------------------------------------------------*/
    IHalSensorList* const pIHalSensorList = MAKE_HalSensorList();
    if( !pIHalSensorList)
    {
        CAM_LOGE("AF-CMD-%-15s: Dev(0x%04x), pIHalSensorList NULL", __FUNCTION__, m_i4CurrSensorDev);
        err = 1;
    }
    if( !err)
    {
        // To get sensor information
        if( m_ptrIHalSensor==NULL)
        {
            m_ptrIHalSensor = pIHalSensorList->createSensor( LOG_TAG, m_i4SensorIdx);
        }
        switch( m_i4CurrSensorDev)
        {
            case ESensorDev_Main :
                pIHalSensorList->querySensorStaticInfo( NSCam::SENSOR_DEV_MAIN, &rSensorStaticInfo);
                break;
            case ESensorDev_Sub:
                pIHalSensorList->querySensorStaticInfo( NSCam::SENSOR_DEV_SUB, &rSensorStaticInfo);
                break;
            case ESensorDev_MainSecond:
                pIHalSensorList->querySensorStaticInfo( NSCam::SENSOR_DEV_MAIN_2, &rSensorStaticInfo);
                break;
            case ESensorDev_SubSecond:
                pIHalSensorList->querySensorStaticInfo(  NSCam::SENSOR_DEV_SUB_2, &rSensorStaticInfo);
                break;
            default:
                CAM_LOGE( "AF-CMD-%-15s: Invalid sensor device: %d", __FUNCTION__, m_i4CurrSensorDev);
                err = 1;
                break;
        }
    }
    if (!err)
    {
        // To get sensorId from the sensorStaticInfo
        m_i4CurrSensorId = rSensorStaticInfo.sensorDevID;
        // To get lensId based on the sensorId
        if( m_pMcuDrv == NULL)
        {
            /*******************************************************
             * lensSearch MUST be done before get data from NVRAM !!
             *******************************************************/
            MUINT32 ModuleID = 0;
            AAA_TRACE_D("GetModuleIDFromCamCal");
            if (m_i4CurrModuleId == -1)
            {
                /* In order to get module id, it must be call CamCal */
                CAM_CAL_DATA_STRUCT pCalData;
                CamCalDrvBase* pCamCalDrvObj = CamCalDrvBase::createInstance();
                CAMERA_CAM_CAL_TYPE_ENUM eCamCalDataType = CAMERA_CAM_CAL_DATA_PART_NUMBER;
                MINT32 i4SensorDevID = -1;
                switch (m_i4CurrSensorDev)
                {
                case ESensorDev_Main:
                    i4SensorDevID = SENSOR_DEV_MAIN;
                    break;
                case ESensorDev_Sub:
                    i4SensorDevID = SENSOR_DEV_SUB;
                    break;
                case ESensorDev_MainSecond:
                    i4SensorDevID = SENSOR_DEV_MAIN_2;
                    break;
                case ESensorDev_SubSecond:
                    i4SensorDevID = SENSOR_DEV_SUB_2;
                    break;
                default:
                    break;
                }
                if (i4SensorDevID != -1)
                {
                    MINT32 ret = pCamCalDrvObj->GetCamCalCalData(m_i4CurrSensorDev, eCamCalDataType, (void*)&pCalData);
                    m_i4CurrModuleId = (ret == CAM_CAL_ERR_NO_ERR) ? (pCalData.PartNumber[0] << 8) + pCalData.PartNumber[1] : 0;
                    CAM_LOGD( "AF-%-15s: Dev(0x%04x) Module ID (%d), ret (%d) -", __FUNCTION__, m_i4CurrSensorDev, m_i4CurrModuleId, ret);
                }
            }
            AAA_TRACE_END_D;
            ModuleID = (m_i4CurrModuleId > 0) ? m_i4CurrModuleId : 0;

            memset(m_u1LensFileName, 0, sizeof(MUINT8)*32);
            m_u1LensFileName[0] = '\0';
            m_pMcuDrv = MCUDrv::getInstance(m_i4CurrSensorDev);
            m_pMcuDrv->lensSearch(m_i4CurrSensorId, ModuleID);
            m_i4CurrLensId = m_pMcuDrv->getCurrLensID();
            m_pMcuDrv->getCurrLensName(m_u1LensFileName);
        }
        m_i4EnableAF = (getAFMaxAreaNum() > 0) ? 1 : 0;// isAFSupport == hasAFLens || ForceEnableFVInFixedFocus
        MINT32 IsAFSupport = isAFSupport();
        CAM_LOGD( "AF-%-15s: Dev(0x%04x), SensorID (0x%04x), ModuleId (0x%04x), LensId (0x%04x), LensFileName(%s) EnableAF (%d), IsAFSupport(%d)",
                  __FUNCTION__,
                  m_i4CurrSensorDev,
                  m_i4CurrSensorId,
                  m_i4CurrModuleId,
                  m_i4CurrLensId,
                  m_u1LensFileName,
                  m_i4EnableAF,
                  IsAFSupport);

        // To get NVRAM based on lensId
        if (m_pMcuDrv)
        {
            m_pMcuDrv->setLensNvramIdx();
        }
        MINT32 err1 = NvBufUtil::getInstance().getBufAndRead( CAMERA_NVRAM_DATA_LENS, m_i4CurrSensorDev, (void*&)m_ptrLensNVRam);
        if(err1 ==0 && m_ptrLensNVRam)
        {
            for(int i=0; i<AF_CAM_SCENARIO_NUM_2; i++)
            {
                CAM_LOGD_IF(m_i4DgbLogLv, "LENS NVRAM set[%d] path: %s", i, m_ptrLensNVRam->AF[i].rFilePath);
            }

            if( m_eNVRAMIndex < AF_CAM_SCENARIO_NUM_2)
            {
                m_ptrNVRam = &(m_ptrLensNVRam->AF[m_eNVRAMIndex]);
                CAM_LOGD("AF-CMD-%-15s: Dev(0x%04x),  Nvram Idx(%d), NVRAM File Path %s",
                         __FUNCTION__,
                         m_i4CurrSensorDev,
                         m_eNVRAMIndex,
                         m_ptrNVRam->rFilePath);
                CAM_LOGD( "AF-CMD-%-15s: Dev(0x%04x), ParamInfo : Nvram Idx(%d), [nvram] ver(%d), Sz(%d, %zu, %zu, %zu, %zu, %zu, %zu),Normal Num(%d)",
                          __FUNCTION__,
                          m_i4CurrSensorDev,
                          m_eNVRAMIndex,
                          m_ptrLensNVRam->Version,
                          MAXIMUM_NVRAM_CAMERA_LENS_FILE_SIZE,
                          sizeof(UINT32),
                          sizeof(FOCUS_RANGE_T),
                          sizeof(AF_NVRAM_T),
                          sizeof(PD_NVRAM_T),
                          sizeof(DUALCAM_NVRAM_T),
                          MAXIMUM_NVRAM_CAMERA_LENS_FILE_SIZE-sizeof(UINT32)-sizeof(FOCUS_RANGE_T)-sizeof(AF_NVRAM_T)-sizeof(PD_NVRAM_T)-sizeof(DUALCAM_NVRAM_T),
                          m_ptrNVRam->rAFNVRAM.sAF_Coef.sTABLE.i4NormalNum);
            }
            else
            {
                CAM_LOGE( "AF-CMD-%-15s: CamScenarioMode is not be updated by 3A framework!!", __FUNCTION__);
                err = 1;
            }

            // set AF-sync parameter to hybrid AF
            SyncAFReadDatabase();
        }
        else
        {
            CAM_LOGE( "AF-CMD-%-15s: AfAlgo NvBufUtil get buf fail!", __FUNCTION__);
            err = 1;
        }
    }
    if(!err)
    {
        /*--------------------------------------------------------------------------------------------------------
         *                                    get pass1 related information
         *--------------------------------------------------------------------------------------------------------*/
        // Get sensor information :
        // TG/BIN size
        sendAFNormalPipe( NSCam::NSIoPipe::NSCamIOPipe::ENPipeCmd_GET_TG_OUT_SIZE, (MINTPTR)(&m_i4TGSzW),  (MINTPTR)(&m_i4TGSzH),0);
        sendAFNormalPipe( NSCam::NSIoPipe::NSCamIOPipe::ENPipeCmd_GET_BIN_INFO,    (MINTPTR)(&m_i4BINSzW), (MINTPTR)(&m_i4BINSzH), 0);
        CAM_LOGD( "AF-%-15s: Dev(0x%04x), TGSZ: W %d, H %d, BINSZ: W %d, H %d",
                  __FUNCTION__,
                  m_i4CurrSensorDev,
                  m_i4TGSzW,
                  m_i4TGSzH,
                  m_i4BINSzW,
                  m_i4BINSzH);
        if( (m_sAFInput.sEZoom.i4W == 0) || (m_sAFInput.sEZoom.i4H == 0))
        {
            m_sAFInput.sEZoom.i4W = m_i4TGSzW;
            m_sAFInput.sEZoom.i4H = m_i4TGSzH;
        }
        //check ZSD or not
        m_sAFInput.i4IsZSD = ( m_i4TGSzW==(MUINT32)rSensorStaticInfo.captureWidth) && ( m_i4TGSzH==(MUINT32)rSensorStaticInfo.captureHeight)? TRUE : FALSE;

        // Initial crop region information and center ROI coordinate will be updated automatically in SetCropRegionInfo.
        // NVRAM and TGSize are required for this.
        SetCropRegionInfo( 0, 0, (MUINT32)m_i4TGSzW, (MUINT32)m_i4TGSzH, AF_MGR_CALLER);

        // To get info from SensorInfo for AdptComp
        MINT32 PixelClk=0;
        m_ptrIHalSensor->sendCommand( m_i4CurrSensorDev, SENSOR_CMD_GET_PIXEL_CLOCK_FREQ, (MINTPTR)&PixelClk, 0,0); // (Hz)
        m_sAFInput.PixelClk = PixelClk; //(Hz)
        MINT32 FrameSyncPixelLineNum=0;
        m_ptrIHalSensor->sendCommand( m_i4CurrSensorDev, SENSOR_CMD_GET_FRAME_SYNC_PIXEL_LINE_NUM, (MINTPTR)&FrameSyncPixelLineNum, 0,0); // (Pixel)
        m_sAFInput.PixelInLine = 0x0000FFFF & FrameSyncPixelLineNum; //(Pixel)

        // get default AFParam/AFConfig and doISPConfig
        getAFParam( m_i4CurrSensorDev, &m_pAFParam);
        AF_CONFIG_T const *ptrHWCfgDef = NULL;
        getAFConfig( m_i4CurrSensorDev, &ptrHWCfgDef);
        if( m_pAFParam && ptrHWCfgDef)
        {
            memcpy( &m_sHWCfg, ptrHWCfgDef, sizeof(AF_CONFIG_T));
            // 1. To get sensorDev & sensorIdx
            // 2. To get AfAlgo handle.
            s_pIAfCxU->init(m_i4CurrSensorDev, m_i4SensorIdx);

            ConfigAFInput_T configInput;
            if (m_i4BINSzW != 0 && m_i4TGSzW != 0)
            {
                m_sHWCfg.sTG_SZ.i4W  = m_i4TGSzW;
                m_sHWCfg.sTG_SZ.i4H  = m_i4TGSzH;
                m_sHWCfg.sBIN_SZ.i4W = m_i4BINSzW;
                m_sHWCfg.sBIN_SZ.i4H = m_i4BINSzH;
            }
            else
            {
                CAM_LOGE( "AF-CMD%-15s: WRONG TG/BIN size : TG[%d,%d], BIN[%d,%d]",
                          __FUNCTION__,
                          m_i4TGSzW, m_i4TGSzH,
                          m_i4BINSzW, m_i4BINSzH);
            }
            configInput.defaultConfig = &m_sHWCfg;

            ConfigAFOutput_T configOutput;

            configOutput.hwConfigInfo.hwArea      = &m_sArea_HW;
            configOutput.hwConfigInfo.hwBlkNumX   = &m_i4HWBlkNumX;
            configOutput.hwConfigInfo.hwBlkNumY   = &m_i4HWBlkNumY;
            configOutput.hwConfigInfo.hwBlkSizeX  = &m_i4HWBlkSizeX;
            configOutput.hwConfigInfo.hwBlkSizeY  = &m_i4HWBlkSizeY;
            configOutput.hwConfigInfo.hwEnExtMode = &m_i4HWEnExtMode;
            configOutput.resultConfig             = &m_sAFResultConfig;

            s_pIAfCxU->config(configInput, configOutput);  // IspMgrAFStateStart();
        }
        else
        {
            CAM_LOGE( "AF-CMD-%-15s: get af_tuning_custom parameters fail !! AFParam(%p), AFConfig(%p)",
                      __FUNCTION__,
                      m_pAFParam,
                      ptrHWCfgDef);
            err=1;
        }
    }
    m_i4EnableAF = (err==0) ? 1 : 0;

    CAM_LOGD( "AF-CMD-%-15s: -, m_i4EnableAF: %d", __FUNCTION__, m_i4EnableAF);

    return err;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MINT32 AfMgr::start()
{
    CAM_LOGD( "AF-CMD-%-15s: + Dev %d", __FUNCTION__, m_i4CurrSensorDev);
    Mutex::Autolock lock( m_Lock);

    MINT32 err=0;
    if(!err)
    {
        // LaunchCamTrigger
        m_i4AEStableTriggerTimeout = m_ptrNVRam->rAFNVRAM.i4EasyTuning[10]>0 ? m_ptrNVRam->rAFNVRAM.i4EasyTuning[10] : m_i4AEStableTriggerTimeout;
        m_i4ValidPDTriggerTimeout  = m_ptrNVRam->rAFNVRAM.i4EasyTuning[11]>0 ? m_ptrNVRam->rAFNVRAM.i4EasyTuning[11] : m_i4ValidPDTriggerTimeout;
        // checkSendCallback info : update the target form NVRAM
        m_sCallbackInfo.isAfterAutoMode     = 0;
        m_sCallbackInfo.isSearching         = AF_SEARCH_DONE;
        m_sCallbackInfo.CompSet_PDCL.Target = m_ptrNVRam->rAFNVRAM.i4EasyTuning[20];
        m_sCallbackInfo.CompSet_FPS.Target  = m_ptrNVRam->rAFNVRAM.i4EasyTuning[21];
        m_sCallbackInfo.CompSet_ISO.Target  = m_ptrNVRam->rAFNVRAM.i4EasyTuning[22];
        CAM_LOGD("AF-%-15s: Dev(0x%04x), needCheckSendCallback %d, CallbackInfo.Targets: PDCL %d, FPS %d, ISO %d",
                 __FUNCTION__,
                 m_i4CurrSensorDev,
                 m_bNeedCheckSendCallback,
                 m_sCallbackInfo.CompSet_PDCL.Target,
                 m_sCallbackInfo.CompSet_FPS.Target,
                 m_sCallbackInfo.CompSet_ISO.Target);

        /*--------------------------------------------------------------------------------------------------------
        *                                           Set Depth AF Info
        *--------------------------------------------------------------------------------------------------------*/
        startSD();

        /*--------------------------------------------------------------------------------------------------------
         *                                          init laser driver
         *--------------------------------------------------------------------------------------------------------*/
        startLD();

        /*--------------------------------------------------------------------------------------------------------
         *                                          init laser driver
         *--------------------------------------------------------------------------------------------------------*/
        IPDMgr::getInstance().isModuleEnable( m_i4CurrSensorDev, m_bEnablePD);

        /*--------------------------------------------------------------------------------------------------------
         *                                          initial Hybrid AF algorithm
         *--------------------------------------------------------------------------------------------------------*/
        // Currently, multi zone is supported as PDAF is on.
        m_sAFInput.i4IsMZ = MFALSE;
        // default using center ROI. m_sArea_Center is updated after calling SetCropRegionInfo.
        m_sAFInput.sAFArea.i4Count  = 1;
        m_sAFInput.sAFArea.sRect[0] = m_sArea_Center;
        memcpy( &(m_sAFInput.sLensInfo), &m_sCurLensInfo, sizeof( LENS_INFO_T));

        // input TG and HW coordinate to hybridAF
        m_sAFInput.sTGSz            = AF_COORDINATE_T( m_i4TGSzW,  m_i4TGSzH);
        m_sAFInput.sHWSz            = AF_COORDINATE_T( m_i4BINSzW, m_i4BINSzH);
        m_sAFInput.i4IsEnterCam     = 1; // default
        m_sAFInput.i2AdvFSRepeat    = m_aAdvFSRepeatTime;
        m_sAFInput.i4IsExtStatMode  = 1;
        m_sAFInput.i4FullScanStep   = m_i4FullScanStep;

        if (m_i4IsEnableFVInFixedFocus)
        {
            m_sAFOutput.sAFStatConfig.sRoi.i4X = 0;
            m_sAFOutput.sAFStatConfig.sRoi.i4Y = 0;
            m_sAFOutput.sAFStatConfig.sRoi.i4W = m_i4TGSzW;
            m_sAFOutput.sAFStatConfig.sRoi.i4H = m_i4TGSzH;
        }


        /*-----------------------------------------------------------------------------------------------------
         *
         *                                            Control Hybrid AF
         *
         *-----------------------------------------------------------------------------------------------------*/
        m_eAFStatePre = m_eAFState;


        /*-----------------------------------------------------------------------------------------------------
         *
         *                                            init af_mgr flow control
         *
         *-----------------------------------------------------------------------------------------------------*/
        m_i4IsAFSearch_PreState = m_i4IsAFSearch_CurState = m_sAFOutput.i4IsAfSearch;
        m_i4IsSelHWROI_PreState = m_i4IsSelHWROI_CurState = m_sAFOutput.i4IsSelHWROI;

        /*-----------------------------------------------------------------------------------------------------
         *                                            AF Manager initial end
         *-----------------------------------------------------------------------------------------------------*/
        if( m_bLock==MTRUE)
        {
            m_pIAfAlgo->cancel();
            m_pIAfAlgo->lock();
            CAM_LOGD( "AF-%-15s: LockAF", __FUNCTION__);
            m_bLock     = MTRUE;
            m_bNeedLock = MFALSE;
            // algo locked for preCapture ==> disable LaunchCamTrigger
            m_i4LaunchCamTriggered = (!m_i4IsLockForLaunchCamTrigger) ? E_LAUNCH_AF_DONE  : m_i4LaunchCamTriggered;
        }
        m_bGetMetaData = MTRUE;


#if !CXUAF_ENABLE
        if( m_pIAfAlgo)
        {
            m_pIAfAlgo->setAFParam( (*m_pAFParam), m_sHWCfg, m_ptrNVRam->rAFNVRAM, m_ptrNVRam->rDualCamNVRAM);
            m_pIAfAlgo->initAF( m_sAFInput, m_sAFOutput);
            m_pIAfAlgo->setAFMode( m_eLIB3A_AFMode);
        }
#endif

        // Get AF calibration data. Should do this after setAFParam is called.
        if( m_ptrNVRam->rAFNVRAM.i4ReadOTP==TRUE)
        {
            readOTP(CAMERA_CAM_CAL_DATA_3A_GAIN);
        }

#if CXUAF_ENABLE
        InitAFInput_T InitInput;
        // value
        InitInput.otpInfPos     = m_i4InfPos;
        InitInput.otpMacroPos   = m_i4MacroPos;
        InitInput.otpMiddlePos  = m_i4MiddlePos;
        InitInput.libAfMode     = m_eLIB3A_AFMode;
        // reference
        InitInput.otpAfTableStr = &m_i4AFTabStr;
        InitInput.otpAfTableEnd = &m_i4AFTabEnd;
        InitInput.afInput       = &m_sAFInput;
        InitInput.defaultParam  = m_pAFParam;
        InitInput.defaultCfg    = &m_sHWCfg;
        InitInput.afNVRAM       = &(m_ptrNVRam->rAFNVRAM);
        InitInput.dualCamNVRAM  = &(m_ptrNVRam->rDualCamNVRAM);

        InitAFOutput_T InitOutput = m_sAFOutput;

        s_pIAfCxU->start(InitInput, InitOutput);
#endif

        //---------------------------------------- init MCU ------------------------------
        if (m_pMcuDrv == NULL)
        {
            CAM_LOGE( "AF-%-15s: McuDrv::createInstance fail", __FUNCTION__);
            err = 1;
        }
        else
        {
            CAM_LOGD( "AF-%-15s: initMCU Dev %d, initPos %d", __FUNCTION__, m_i4CurrSensorDev, m_i4InitPos);
            m_pMcuDrv->init(m_i4InitPos);
        }
    }

    m_i4EnableAF = (err==0) ? 1 : 0;

    CAM_LOGD( "AF-%-15s: -, m_i4EnableAF = %d", __FUNCTION__, m_i4EnableAF);
    return err;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MINT32 AfMgr::stop()
{
    Mutex::Autolock lock( m_Lock);

    //uninitial isp_mgr_af_stat for configure HW
#if CXUAF_ENABLE
    if(s_pIAfCxU)
    {
        s_pIAfCxU->stop();
        s_pIAfCxU->uninit();
    }
#else
    IspMgrAFStatStop();
#endif
    //store Nno-volatilize informaiton.
    getLensInfo( m_sCurLensInfo);
    CAM_LOGD( "AF-%-15s: + Dev %d, Record : Mode (%d)%d, Pos %d",
              __FUNCTION__,
              m_i4CurrSensorDev,
              m_eCurAFMode,
              m_eLIB3A_AFMode,
              m_sCurLensInfo.i4CurrentPos );

    //reset parameters.
    m_bRunPDEn       = MFALSE;
    m_bEnablePD      = MFALSE;
    m_i4PDAF_support_type = PDAF_SUPPORT_NA;

    if( m_ptrIHalSensor)
    {
        m_ptrIHalSensor->destroyInstance( LOG_TAG);
        m_ptrIHalSensor = NULL;
    }

    UpdateState( EVENT_CMD_STOP);

    CAM_LOGD( "AF-%-15s: -", __FUNCTION__);
    return 0;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MINT32 AfMgr::camPwrOff()
{
    //Camera Power Off, call by HAL, MW
    CAM_LOGD( "AF-%-15s: + Dev %d", __FUNCTION__, m_i4CurrSensorDev);

    if( m_pMcuDrv)
    {
        CAM_LOGD( "AF-%-15s: uninitMcuDrv - Dev: %d", __FUNCTION__, m_i4CurrSensorDev);
        m_pMcuDrv->uninit();
        m_pMcuDrv->destroyInstance();
        m_pMcuDrv = NULL;
    }

    camPwrOffLD();

    CAM_LOGD( "AF-%-15s: -", __FUNCTION__);
    return 0;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MINT32 AfMgr::uninit()
{
    CAM_LOGD( "AF-%-15s: +", __FUNCTION__);

    MINT32 ret = 1;

    Mutex::Autolock lock(m_Lock);

    if( m_i4Users==1)
    {
        /* Uninit af_mgr, when last user calls af_mgr uninit.*/

        // restore AF-sync parameters
        //SyncAFWriteDatabase();
#if !CXUAF_ENABLE
        // uninit hybrid AF
        if( m_pIAfAlgo)
        {
            m_pIAfAlgo->destroyInstance();
            m_pIAfAlgo = NULL;
        }
#endif

        // Clear EMAF flag when leaving camera
        m_bIsFullScan = MFALSE;
        m_aCurEMAFFlag = 0;
        m_aPreEMAFFlag = 0;
        m_aAdvFSRepeatTime = 10;

        /* Sensor Provider */
        unintSP();

        ret = 0;
    }
    else
    {
        /**
         *  Do not uninit af_mgr :
         *  1. User Cnt   >1 : af_mgr is still used.
         *  2. User Cnt <=0  : wrong host flow.
         */
        CAM_LOGD( "AF-%-15s: no uninit, user %d", __FUNCTION__, m_i4Users);
    }

    android_atomic_dec( &m_i4Users);
    CAM_LOGD( "AF-%-15s: Algo(%p), users %d", __FUNCTION__, (void*)m_pIAfAlgo, m_i4Users);

    CAM_LOGD( "AF-%-15s: -", __FUNCTION__);
    return ret;
}

MINT32 AfMgr::process(AFInputData_T data, AFCommand_T command)
{
    CAM_LOGD( "AF-%-15s: +", __FUNCTION__);
    MINT32 err = 0;

    // 1. doCommand
    CAM_LOGD("[%s] AFCommand: requestNum = %d, afMode = %d, MZOn = %d, pauseAF = %d, ePrecapture = %d, eAutofocus = %d, triggerAF = %d",
             __FUNCTION__,
             command.requestNum,
             command.afMode,
             command.multiZoneEnable,
             command.pauseAF,
             command.ePrecapture,
             command.eAutofocus,
             command.triggerAF);

    SetCurFrmNum( command.requestNum);
    setAFMode( command.afMode, AF_CMD_CALLER);   // todo
    setFocusDistance( command.focusDistance);
    setMultiZoneEnable( command.multiZoneEnable);
    SetCropRegionInfo( command.cropRegion_X,
                       command.cropRegion_Y,
                       command.cropRegion_W,
                       command.cropRegion_H,
                       AF_CMD_CALLER);
    setAFArea( command.afRegions);
    SetPauseAF( command.pauseAF);   // todo

    if(command.ePrecapture == AfCommand_Start)
    {
        WaitTriggerAF(MTRUE);
    }
    else if(command.ePrecapture == AfCommand_Cancel)
    {
        WaitTriggerAF(MFALSE);
    }

    if(command.eAutofocus == AfCommand_Start)
    {
        autoFocus();
    }
    else if(command.eAutofocus == AfCommand_Cancel)
    {
        cancelAutoFocus();
    }

    if(command.triggerAF)
    {
        triggerAF(AF_CMD_CALLER);
    }

    // 2. doData
    CAM_LOGD_IF(m_i4DgbLogLv, "[%s] ispAEResult frameNum = %d, isAEStable = %d, isAELock = %d, deltaIndex = %d, afeGain = %d, exposureTime=%d, realISOValue = %d, isAEScenarioChange = %d, aeFinerEVIdxBase = %d, aeIdxCurrentF = %d, lightValue_x10 = %d",
                __FUNCTION__,
                data.ispResult.FrameNum,
                data.ispResult.isAEStable,
                data.ispResult.isAELock,
                data.ispResult.deltaIndex,
                data.ispResult.afeGain,
                data.ispResult.exposuretime,
                data.ispResult.realISOValue,
                data.ispResult.isAEScenarioChange,
                data.ispResult.aeFinerEVIdxBase,
                data.ispResult.aeIdxCurrentF,
                data.ispResult.lightValue_x10);
    CAM_LOGD_IF(m_i4DgbLogLv, "[%s] isFlashFrm = %d, tsAFDone = %d, nvramIndex = %d, ptrAFStt = 0x%8x",
                __FUNCTION__,
                data.isFlashFrm,
                data.tsAFDone,
                data.nvramIndex,
                data.ptrAFStt);

    AdptCompTimeData_T tempData;
    tempData.TS_AFDone = data.tsAFDone;
    setAdptCompInfo( tempData);

    // 3. doAlgo
    doAF(MNULL);

    CAM_LOGD( "AF-%-15s: -", __FUNCTION__);
    return err;
}
MINT32 AfMgr::getResult(AFResult_T &result)
{
    CAM_LOGD( "AF-%-15s: +", __FUNCTION__);
    MINT32 err = 0;

#if 0
    // still missing result
    android::Vector<MINT32> afRegions;
    MINT32 afSceneChange;
    MUINT64 focusValue;
    //FSC_FRM_INFO_T fscInfo;
    MUINT32* ptrDebugInfo;
#endif
    result.resultNum         = m_u4ReqMagicNum;
    result.afMode            = m_eCurAFMode;
    result.afState           = m_eAFState;
    result.lensState         = m_lensState;
    result.lensFocusDistance = m_sFocusDis.i4Dist;
    result.lockAERequest     = i4IsLockAERequest;
    result.lensPosition      = m_sAFOutput.i4AFPos;
    result.isFocusFinish     = (m_eAFState!=E_AF_PASSIVE_SCAN)    && (m_eAFState!=E_AF_ACTIVE_SCAN);
    result.isFocused         = (m_eAFState==E_AF_PASSIVE_FOCUSED) || (m_eAFState==E_AF_FOCUSED_LOCKED);

    getFocusArea( result.afRegions);
    getFocusRange(&(result.lensFocusRange_near), &(result.lensFocusRange_far));

    //getDebugInfo(*(result.ptrDebugInfo));

    CAM_LOGD("[%s] resultNum = %d, afMode = %d, afState = %d, lensState = %d, lensFocusDistance = %d, lockAERequest = %d, lensPosition = %d, isFocusFinish = %d, isFocused = %d",
             __FUNCTION__,
             result.resultNum,
             result.afMode,
             result.afState,
             result.lensState,
             result.lensFocusDistance,
             result.lockAERequest,
             result.lensPosition,
             result.isFocusFinish,
             result.isFocused);

    CAM_LOGD( "AF-%-15s: -", __FUNCTION__);
    return err;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MUINT64 AfMgr::MoveLensTo( MINT32 &i4TargetPos, MUINT32 u4Caller)
{
    MUINT64 TS_BeforeMoveMCU = 0; // timestamp of before moveMCU
    MUINT64 TS_AfterMoveMCU = 0;  // timestamp of after moveMCU

    if( m_pMcuDrv)
    {
        if( m_i4DbgMotorDisable==1)
        {
            if( m_i4DbgMotorMPos<1024 && m_i4DbgMotorMPos!=m_i4DbgMotorMPosPre)
            {
                m_i4DbgMotorMPosPre = m_i4DbgMotorMPos;
                m_pMcuDrv->moveMCU( m_i4DbgMotorMPos);
            }
        }
        else if( m_i4MvLensTo!=i4TargetPos)
        {
            if( u4Caller==AF_MGR_CALLER)
            {
                CAM_LOGD("#(%5d,%5d) %s Dev(%d) DAC(%d)", m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__, m_i4CurrSensorDev, i4TargetPos);
            }
            else
            {
                CAM_LOGD("#(%5d,%5d) cmd-%s Dev(%d) DAC(%d)", m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__, m_i4CurrSensorDev, i4TargetPos);
            }

            CAM_LOGD_IF( m_i4DgbLogLv&4, "%s+", __FUNCTION__);
            TS_BeforeMoveMCU = getTimeStamp_us();
            if (m_pMcuDrv->moveMCU( i4TargetPos))
            {
                m_i4MvLensTo = i4TargetPos;
            }
            TS_AfterMoveMCU = getTimeStamp_us();
            CAM_LOGD_IF( m_i4DgbLogLv&4, "%s-", __FUNCTION__);
        }

        if( m_i4DbgOISDisable==1)
        {
            MUINT32 DbgOISPos = property_get_int32("vendor.debug.af_ois.position", 0);
            // DbgOISPos(XXXXYYYY) = X * 10000 + Y; Ex: 10000200
            if (m_i4DbgOISPos!=DbgOISPos)
            {
                CAM_LOGD("Set OIS Position %d", DbgOISPos);
                m_pMcuDrv->setMCUParam(0x02, DbgOISPos);
                m_i4DbgOISPos = DbgOISPos;
            }
        }
    }
    else
    {
        CAM_LOGD_IF( m_i4DgbLogLv, "%s Fail, Dev %d", __FUNCTION__, m_i4CurrSensorDev);
    }

    if (TS_BeforeMoveMCU!=0)
    {
        // update time stamp of moveMCU
        return (MUINT64)((TS_BeforeMoveMCU + TS_AfterMoveMCU)/2);
    }
    else
    {
        // inherit the original one
        return m_u8MvLensTS;
    }
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID  AfMgr::getHWCfgReg(AFResultConfig_T *pResultConfig)
{
    memcpy(pResultConfig, &m_sAFResultConfig, sizeof(AFResultConfig_T));
    CAM_LOGD("HW-%s Enable(%d)", __FUNCTION__, m_sAFResultConfig.enableAFHw);
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::SetPauseAF( MBOOL bIsPause)
{
    if (m_bPauseAF != bIsPause)
    {
        if (bIsPause == MTRUE)
        {
            CAM_LOGD( "#(%5d,%5d) cmd-%s %d->%d (PAUSE)",
                      m_u4ReqMagicNum,
                      m_u4StaMagicNum,
                      __FUNCTION__,
                      m_bPauseAF,
                      bIsPause);

            LockAlgo(AF_CMD_CALLER);
        }
        else
        {
            if (m_i4UnPauseReqNum == 0)
            {
                m_i4UnPauseReqNum = m_u4ReqMagicNum; // The unpause event wait to take effect.

                CAM_LOGD( "#(%5d,%5d) cmd-%s %d->%d (WAIT TO UNPAUSE)",
                          m_u4ReqMagicNum,
                          m_u4StaMagicNum,
                          __FUNCTION__,
                          m_bPauseAF,
                          bIsPause);
            }
            else
            {
                if (m_u4StaMagicNum >= m_i4UnPauseReqNum) // The unpause event takes effect from ReqMagNum.
                {
                    CAM_LOGD( "#(%5d,%5d) cmd-%s %d->%d (CONTINUE)",
                              m_u4ReqMagicNum,
                              m_u4StaMagicNum,
                              __FUNCTION__,
                              m_bPauseAF,
                              bIsPause);

                    UnlockAlgo(AF_CMD_CALLER);
                    m_i4UnPauseReqNum = 0;
                }
            }
        }
    }

    return S_AF_OK;
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::setAFMode( MINT32 eAFMode, MUINT32 u4Caller)
{
    if( m_i4EnableAF==0)
    {
        return S_AF_OK;
    }

    if( m_eCurAFMode==eAFMode)
    {
        return S_AF_OK;
    }

    /**
     *  Before new af mode is set, setting af area command is sent.
     */
    if( u4Caller==AF_MGR_CALLER)
    {
        CAM_LOGD( "#(%5d,%5d) %s Dev(%d):ctl_afmode(%d)",
                  m_u4ReqMagicNum,
                  m_u4StaMagicNum,
                  __FUNCTION__,
                  m_i4CurrSensorDev,
                  eAFMode);
    }
    else
    {
        CAM_LOGD( "#(%5d,%5d) cmd-%s Dev(%d):ctl_afmode(%d)",
                  m_u4ReqMagicNum,
                  m_u4StaMagicNum,
                  __FUNCTION__,
                  m_i4CurrSensorDev,
                  eAFMode);
    }

    m_eCurAFMode = eAFMode;

    LIB3A_AF_MODE_T preLib3A_AfMode = m_eLIB3A_AFMode;
    switch( m_eCurAFMode)
    {
        case MTK_CONTROL_AF_MODE_OFF :
            m_eLIB3A_AFMode = LIB3A_AF_MODE_MF; /*API2:The auto-focus routine does not control the lens. Lens  is controlled by the application.*/
            break;
        case MTK_CONTROL_AF_MODE_AUTO :
            if(m_aCurEMAFFlag==0)// Auto Focus for Single shot
            {
                m_eLIB3A_AFMode = LIB3A_AF_MODE_AFS;
            }
            else
            {
                // Engineer Mode: Full Scan
                if(m_aCurEMAFFlag == EM_AF_FLAG_FULLSCAN_NORMAL) // Full Scan
                {
                    m_eLIB3A_AFMode = LIB3A_AF_MODE_FULLSCAN;
                }
                else if(m_aCurEMAFFlag == (EM_AF_FLAG_FULLSCAN_NORMAL|EM_AF_FLAG_FULLSCAN_ADVANCE))// Advanced Full Scan
                {
                    m_eLIB3A_AFMode = LIB3A_AF_MODE_ADVFULLSCAN;
                }
                else if(m_aCurEMAFFlag == (EM_AF_FLAG_FULLSCAN_NORMAL|EM_AF_FLAG_TEMP_CALI))// Temperature Calibration
                {
                    m_eLIB3A_AFMode = LIB3A_AF_MODE_TEMP_CALI;
                }
            }
            break;
        case MTK_CONTROL_AF_MODE_MACRO :
            m_eLIB3A_AFMode = LIB3A_AF_MODE_MACRO;
            break;
        case MTK_CONTROL_AF_MODE_CONTINUOUS_VIDEO :
            m_eLIB3A_AFMode = LIB3A_AF_MODE_AFC_VIDEO;
            break;
        case MTK_CONTROL_AF_MODE_CONTINUOUS_PICTURE :
            m_eLIB3A_AFMode = LIB3A_AF_MODE_AFC;
            break;
        case MTK_CONTROL_AF_MODE_EDOF :
            m_eLIB3A_AFMode = LIB3A_AF_MODE_OFF;
            break;
        default :
            m_eLIB3A_AFMode = LIB3A_AF_MODE_AFS;
            break;
    }

    // checking whether it is valid for calculation ap roi.
    if( m_eCurAFMode==MTK_CONTROL_AF_MODE_AUTO)
    {
        // ap roi is calculated during af mode is configured as auto mode.
        m_sPDRois[eIDX_ROI_ARRAY_AP].valid = MTRUE;
    }
    else
    {
        m_sPDRois[eIDX_ROI_ARRAY_AP].valid = MFALSE;
    }


    // log only.
    if( u4Caller==AF_MGR_CALLER)
    {
        CAM_LOGD( "#(%5d,%5d) %s Dev(%d):lib_afmode %d->%d",
                  m_u4ReqMagicNum,
                  m_u4StaMagicNum,
                  __FUNCTION__,
                  m_i4CurrSensorDev,
                  preLib3A_AfMode,
                  m_eLIB3A_AFMode);
    }
    else
    {
        CAM_LOGD( "#(%5d,%5d) cmd-%s Dev(%d):lib_afmode %d->%d",
                  m_u4ReqMagicNum,
                  m_u4StaMagicNum,
                  __FUNCTION__,
                  m_i4CurrSensorDev,
                  preLib3A_AfMode,
                  m_eLIB3A_AFMode);
    }

    //Set mode to hybrid AF algorithm
    if( m_pIAfAlgo)
        m_pIAfAlgo->setAFMode( m_eLIB3A_AFMode);
    else
        CAM_LOGD( "Null m_pIAfAlgo");


    //update parameters and status.
    UpdateState( EVENT_CMD_CHANGE_MODE);
    return S_AF_OK;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
E_AF_STATE_T AfMgr::getAFState()
{
    E_AF_STATE_T ret;

    if (m_u4ReqMagicNum<=AF_START_MAGIC_NUMBER && m_eAFState==E_AF_PASSIVE_SCAN)
    {
        /**
         * If AF starts searching at first request.
         * Force to return inactive state as initial state.
         */
        ret = E_AF_INACTIVE;
    }
    else
    {
        ret = m_eAFState;
    }

    if(AP_NOT_SHOW_FDROI_AUTO && m_i4IsCAFWithoutFace==1 && ret==NS3A::E_AF_PASSIVE_SCAN && m_sAFOutput.i4ROISel==AF_ROI_SEL_FD)
    {
        // For AP which WOULD NOT show Face ROI automatically while CAFing
        // faces show up while CAFing
        // overwrite AfState for MW to send callback
        if(m_bNeedLock)
        {
            ret = E_AF_FOCUSED_LOCKED;
        }
        else
        {
            ret = E_AF_PASSIVE_FOCUSED;
        }
        if( m_eAFStatePre!=ret)
        {
            CAM_LOGD("cmd-%s Dev %d : ret = %d, Face shows up while CAFing => Send callback to clear CAF ROI",
                     __FUNCTION__,
                     m_i4CurrSensorDev,
                     ret);
        }
    }
    else
    {
        if( m_eAFStatePre!=ret)
        {
            CAM_LOGD( "cmd-%s Dev %d : %d->%d",
                      __FUNCTION__,
                      m_i4CurrSensorDev,
                      m_eAFStatePre,
                      ret);
        }
    }


    m_eAFStatePre = ret;


    return ret;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::getFocusAreaResult( std::vector<MINT32> &vecOutPos, std::vector<MUINT8> &vecOutRes, MINT32 &i4OutSzW, MINT32 &i4OutSzH)
{
    // the FocusArea would be updated to the HalMeta
    // FocusArea: (X,Y,W,H) -> (L,T,R,B)
    // (X,Y) = Center point of the ROI

    vecOutPos.clear();
    vecOutRes.clear();
    i4OutSzW = 0;
    i4OutSzH = 0;

    if( m_bMZAFEn)
    {
        MUINT32 num = static_cast<MUINT32>(m_sAFOutput.sROIStatus.i4TotalNum);
        if( num<=MAX_MULTI_ZONE_WIN_NUM)
        {
            MINT32 x,y;
            MUINT8 res;
            for( MUINT32 i=0; i<num; i++)
            {
                x   = m_sAFOutput.sROIStatus.sROI[i].i4X + m_sAFOutput.sROIStatus.sROI[i].i4W/2;
                y   = m_sAFOutput.sROIStatus.sROI[i].i4Y + m_sAFOutput.sROIStatus.sROI[i].i4H/2;
                res = m_sAFOutput.sROIStatus.sROI[i].i4Info;

                /*The order of vector vecOutPos is (x1)->(y1)->(x2)->(y2)*/
                vecOutPos.push_back(x);
                vecOutPos.push_back(y);
                vecOutRes.push_back(res);

                CAM_LOGD_IF( m_i4DgbLogLv,
                             "%s [%d] X(%4d), Y(%4d), W(%4d), H(%4d), Res(%d)",
                             __FUNCTION__,
                             i,
                             m_sAFOutput.sROIStatus.sROI[i].i4X,
                             m_sAFOutput.sROIStatus.sROI[i].i4Y,
                             m_sAFOutput.sROIStatus.sROI[i].i4W,
                             m_sAFOutput.sROIStatus.sROI[i].i4H,
                             m_sAFOutput.sROIStatus.sROI[i].i4Info);
            }
            /* All W and H should be the same*/
            i4OutSzW = m_sAFOutput.sROIStatus.sROI[0].i4W;
            i4OutSzH = m_sAFOutput.sROIStatus.sROI[0].i4H;
        }
    }

    CAM_LOGD_IF( m_i4DgbLogLv&4,
                 "%s %zu %zu",
                 __FUNCTION__,
                 vecOutPos.size(),
                 vecOutRes.size());

    return S_AF_OK;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::getFocusArea( std::vector<MINT32> &vecOut)
{
    // the FocusArea would be updated to the AppMeta
    // FocusArea: (X,Y,W,H) -> (L,T,R,B)
    // (X,Y) = LeftTop point of the ROI
    vecOut.clear();
    /*
     * The order of vector vecOut is :
     * (type)->(number of ROI)->(left_1)->(top_1)->(right_1)->(bottom_1)->(result_1)->(left_2)->(top_2)->(right_2)->(bottom_2)...
     */
    if( m_bMZAFEn)
    {
        MINT32 type = 0;
        vecOut.push_back(type);

        MUINT32 num = static_cast<MUINT32>(m_sAFOutput.sROIStatus.i4TotalNum);
        if( (MAX_MULTI_ZONE_WIN_NUM<num))
        {
            num = 0;
        }
        vecOut.push_back(num);

        for( MUINT32 i=0; i<num; i++)
        {
            vecOut.push_back( m_sAFOutput.sROIStatus.sROI[i].i4X);
            vecOut.push_back( m_sAFOutput.sROIStatus.sROI[i].i4Y);
            vecOut.push_back((m_sAFOutput.sROIStatus.sROI[i].i4X+m_sAFOutput.sROIStatus.sROI[i].i4W));
            vecOut.push_back((m_sAFOutput.sROIStatus.sROI[i].i4Y+m_sAFOutput.sROIStatus.sROI[i].i4H));
            vecOut.push_back( m_sAFOutput.sROIStatus.sROI[i].i4Info);

            CAM_LOGD_IF( m_i4DgbLogLv,
                         "%s [%d] X(%4d), Y(%4d), W(%4d), H(%4d), Res(%d)",
                         __FUNCTION__,
                         i,
                         m_sAFOutput.sROIStatus.sROI[i].i4X,
                         m_sAFOutput.sROIStatus.sROI[i].i4Y,
                         m_sAFOutput.sROIStatus.sROI[i].i4W,
                         m_sAFOutput.sROIStatus.sROI[i].i4H,
                         m_sAFOutput.sROIStatus.sROI[i].i4Info);
        }

    }
    else
    {
        MINT32 type = 0;
        vecOut.push_back(type);

        AREA_T AreaOnUI;
        MUINT32 TwinStatus;
        IspMgrAFGetROIFromHw(AreaOnUI, TwinStatus);
        CAM_LOGD_IF( m_i4DgbLogLv,
                     "%s isTwin(%d), ROI from CAM_A Reg X(%4d), Y(%4d), W(%4d), H(%4d), Res(%d)",
                     __FUNCTION__,
                     TwinStatus,
                     AreaOnUI.i4X,
                     AreaOnUI.i4Y,
                     AreaOnUI.i4W,
                     AreaOnUI.i4H,
                     m_sArea_TypeSel);
        // check if TwinMode on
        MINT32 num  = TwinStatus==0 ? 1 : 2;
        vecOut.push_back(num);
        // pass AFROI to UI
        vecOut.push_back( AreaOnUI.i4X);
        vecOut.push_back( AreaOnUI.i4Y);
        vecOut.push_back((AreaOnUI.i4X+AreaOnUI.i4W));
        vecOut.push_back((AreaOnUI.i4Y+AreaOnUI.i4H));
        vecOut.push_back( m_sArea_TypeSel);
        if(TwinStatus)
        {
            vecOut.push_back( m_sArea_HW.i4X);
            vecOut.push_back( m_sArea_HW.i4Y);
            vecOut.push_back((m_sArea_HW.i4X+m_sArea_HW.i4W));
            vecOut.push_back((m_sArea_HW.i4Y+m_sArea_HW.i4H));
            vecOut.push_back( m_sArea_TypeSel);
            CAM_LOGD_IF( m_i4DgbLogLv,
                         "%s ROI X(%4d), Y(%4d), W(%4d), H(%4d), Res(%d)",
                         __FUNCTION__,
                         m_sArea_HW.i4X,
                         m_sArea_HW.i4Y,
                         m_sArea_HW.i4W,
                         m_sArea_HW.i4H,
                         m_sArea_TypeSel);
        }
    }
    return S_AF_OK;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::setNVRAMIndex(MUINT32 a_eNVRAMIndex)
{
    /**********************************************************
     * This function should be called before af mgr is started!!
     **********************************************************/
    // [change mode] or [unvalid mode]
    if( (a_eNVRAMIndex!=m_eNVRAMIndex) || (m_eNVRAMIndex>=AF_CAM_SCENARIO_NUM_2))
    {
        // if original CamScenarioMode is unvalid, reset it to CAM_SCENARIO_PREVIEW
        if(m_eNVRAMIndex>=AF_CAM_SCENARIO_NUM_2)
        {
            m_eNVRAMIndex = 0;
        }
        CAM_LOGD( "#(%5d,%5d) Dev(0x%04x), %s %d -> %d",
                  m_u4ReqMagicNum,
                  m_u4StaMagicNum,
                  m_i4CurrSensorDev,
                  __FUNCTION__,
                  m_eNVRAMIndex,
                  a_eNVRAMIndex);

        if ( a_eNVRAMIndex < AF_CAM_SCENARIO_NUM_2)
        {
            if( m_ptrLensNVRam != NULL)
            {
                /*************************************************************************************************
                 *
                 * m_ptrLensNVRam is initialized in af mgr start function.
                 * m_ptrLensNVRam!=NULL means AF NVRAM data is valid, so NVRAM data should be updated in algroithm.
                 *
                 *************************************************************************************************/
                m_ptrNVRam = &(m_ptrLensNVRam->AF[a_eNVRAMIndex]);
                CAM_LOGD_IF( m_i4DgbLogLv,
                             "#(%5d,%5d) %s [Path]%s [nvram][Offset]%d [Normal Num]%d",
                             m_u4ReqMagicNum,
                             m_u4StaMagicNum,
                             __FUNCTION__,
                             m_ptrNVRam->rFilePath,
                             m_ptrNVRam->rAFNVRAM.sAF_Coef.sTABLE.i4Offset,
                             m_ptrNVRam->rAFNVRAM.sAF_Coef.sTABLE.i4NormalNum);

                // LaunchCamTrigger
                if(m_ptrNVRam->rAFNVRAM.i4EasyTuning[10]>0)  // the timeout of AEStable-wait-Face is not default value
                {
                    m_i4AEStableTriggerTimeout = m_ptrNVRam->rAFNVRAM.i4EasyTuning[10];
                }
                if(m_ptrNVRam->rAFNVRAM.i4EasyTuning[10]>0)  // the timeout of ValidPD-wait-Face is not default value
                {
                    m_i4ValidPDTriggerTimeout = m_ptrNVRam->rAFNVRAM.i4EasyTuning[11];
                }

                // checkSendCallback info : update the target form NVRAM
                m_sCallbackInfo.CompSet_PDCL.Target = m_ptrNVRam->rAFNVRAM.i4EasyTuning[20];
                m_sCallbackInfo.CompSet_ISO.Target = m_ptrNVRam->rAFNVRAM.i4EasyTuning[22];
                m_sCallbackInfo.CompSet_FPS.Target = m_ptrNVRam->rAFNVRAM.i4EasyTuning[21];

                if( m_pIAfAlgo)
                {
                    AF_CONFIG_T const *ptrHWCfgDef = NULL;
                    getAFConfig( m_i4CurrSensorDev, &ptrHWCfgDef);
                    m_pIAfAlgo->setAFParam( (*m_pAFParam), (*ptrHWCfgDef), m_ptrNVRam->rAFNVRAM, m_ptrNVRam->rDualCamNVRAM);
                    // initial hybrid AF algorithm
                    m_pIAfAlgo->initAF( m_sAFInput, m_sAFOutput);

                    if( m_bLock!=MTRUE)
                    {
                        m_pIAfAlgo->cancel();
                    }
                }
                // Get AF calibration data. Should do this after setAFParam is called.
                if( m_ptrNVRam->rAFNVRAM.i4ReadOTP==TRUE)
                {
                    readOTP(CAMERA_CAM_CAL_DATA_3A_GAIN);
                }
            }
            else
            {
                CAM_LOGD( "#(%5d,%5d) %s NVRAM is not initialized, update m_eCamScenarioMode only, no need update to algo", m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__);
            }

            //
            m_eNVRAMIndex = a_eNVRAMIndex;
        }
        else
        {
            CAM_LOGD( "#(%5d,%5d) %s Camera NVRAM index invalid (%d), no need update to algo", m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__, a_eNVRAMIndex);
        }

    }

    return S_AF_OK;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::setFDInfo( MVOID *a_sFaces)
{
    MRESULT ret = E_3A_ERR;

    if ( m_pIAfAlgo!=NULL)
    {
        ret = setOTFDInfo( a_sFaces, 0);
    }
    else
    {
        CAM_LOGD( "[%s] Null algo ptr", __FUNCTION__);
    }

    return ret;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::setOTInfo( MVOID *a_sOT)
{
    MRESULT ret = E_3A_ERR;

    if ( m_pIAfAlgo!=NULL)
    {
        ret = setOTFDInfo( a_sOT, 1);
    }
    else
    {
        CAM_LOGD( "[%s] Null algo ptr", __FUNCTION__);
    }

    return ret;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MINT32 AfMgr::getAFMaxAreaNum()
{
    if (m_i4AFMaxAreaNum == -1)
    {
        IHalSensorList* const pIHalSensorList = MAKE_HalSensorList();

        SensorStaticInfo rSensorStaticInfo;
        switch( m_i4CurrSensorDev)
        {
            case ESensorDev_Main:
                pIHalSensorList->querySensorStaticInfo( NSCam::SENSOR_DEV_MAIN, &rSensorStaticInfo);
                break;
            case ESensorDev_Sub:
                pIHalSensorList->querySensorStaticInfo( NSCam::SENSOR_DEV_SUB, &rSensorStaticInfo);
                break;
            case ESensorDev_MainSecond:
                pIHalSensorList->querySensorStaticInfo( NSCam::SENSOR_DEV_MAIN_2, &rSensorStaticInfo);
                break;
            case ESensorDev_SubSecond:
                pIHalSensorList->querySensorStaticInfo( NSCam::SENSOR_DEV_SUB_2, &rSensorStaticInfo);
                break;
            default:
                CAM_LOGE( "Invalid sensor device: %d", m_i4CurrSensorDev);
                break;
        }

        MCUDrv *pMcuDrv = MCUDrv::getInstance(m_i4CurrSensorDev);
        m_i4AFMaxAreaNum = pMcuDrv->isLensSupport(rSensorStaticInfo.sensorDevID)==0 ? 0 : AF_WIN_NUM_SPOT;

        CAM_LOGD( "[%s] (%d)", __FUNCTION__, m_i4AFMaxAreaNum);

        if (property_get_int32("vendor.debug.af_motor.fixedfocus", 1) > 0) /* Force to disable AF */
        {
            m_i4AFMaxAreaNum = 0;
            CAM_LOGD( "[%s] (%d) by ADB", __FUNCTION__, m_i4AFMaxAreaNum);
            CAM_LOGE( "[%s] force to disable AF [remove]", __FUNCTION__);
        }
    }

    return (m_i4AFMaxAreaNum > 0) ? AF_WIN_NUM_SPOT : 0;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MINT32 AfMgr::isAFSupport()
{
    MINT32 isAFSupport = getAFMaxAreaNum();

    if (m_i4IsEnableFVInFixedFocus == -1)
    {
        if (isAFSupport == 0)
        {
            m_i4IsEnableFVInFixedFocus = ForceEnableFVInFixedFocus(m_i4CurrSensorDev);

            MINT32 type = property_get_int32("vendor.debug.af_fv.switch", 0);

            if (m_i4IsEnableFVInFixedFocus == 0 && type == 1)
                m_i4IsEnableFVInFixedFocus = 1;

            if (m_i4IsEnableFVInFixedFocus == 1 && type == 2)
                m_i4IsEnableFVInFixedFocus = 0;
        }
        else
        {
            m_i4IsEnableFVInFixedFocus = 0;
        }
    }

    isAFSupport |= m_i4IsEnableFVInFixedFocus;

    return isAFSupport;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MINT32 AfMgr::getMaxLensPos()
{
    MINT32 maxLensPos = 0;
    if( m_i4EnableAF && m_pMcuDrv)
    {
        mcuMotorInfo MotorInfo;
        m_pMcuDrv->getMCUInfo( &MotorInfo);
        maxLensPos = (MINT32)MotorInfo.u4MacroPosition;
    }
    CAM_LOGD_IF( m_i4DgbLogLv, "[%s] EnableAF(%d), MCU (%p), MaxLensPos(%d)", __FUNCTION__, m_i4EnableAF, (void*)m_pMcuDrv, maxLensPos);

    return maxLensPos;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MINT32 AfMgr::getMinLensPos()
{
    MINT32 minLensPos = 0;
    if( m_i4EnableAF && m_pMcuDrv)
    {
        mcuMotorInfo MotorInfo;
        m_pMcuDrv->getMCUInfo( &MotorInfo);
        minLensPos = (MINT32)MotorInfo.u4InfPosition;
    }
    CAM_LOGD_IF( m_i4DgbLogLv, "[%s] EnableAF(%d), MCU (%p), MinLensPos(%d)", __FUNCTION__, m_i4EnableAF, (void*)m_pMcuDrv, minLensPos);

    return minLensPos;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MINT32 AfMgr::getAFTableMacroIdx()
{
    if(m_ptrNVRam->rAFNVRAM.sAF_Coef.sTABLE.i4NormalNum>1)
        return m_ptrNVRam->rAFNVRAM.sAF_Coef.sTABLE.i4NormalNum-1;
    else
        return m_ptrNVRam->rAFNVRAM.sAF_Coef.sTABLE.i4MacroNum-1;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MINT32 AfMgr::getAFTableIdxNum()
{
    if(m_ptrNVRam->rAFNVRAM.sAF_Coef.sTABLE.i4NormalNum>0)
        return m_ptrNVRam->rAFNVRAM.sAF_Coef.sTABLE.i4NormalNum;
    else
        return m_ptrNVRam->rAFNVRAM.sAF_Coef.sTABLE.i4MacroNum;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID* AfMgr::getAFTable()
{
    MINT32 i4Pos[AF_TABLE_NUM];
    for(MINT32 i=0; i<AF_TABLE_NUM; i++) i4Pos[i]=0;
    if(m_ptrNVRam->rAFNVRAM.sAF_Coef.sTABLE.i4NormalNum>0)
    {
        return (MVOID*)m_ptrNVRam->rAFNVRAM.sAF_Coef.sTABLE.i4Pos;
    }
    else //AF table method 2
    {
        MINT32 i4TBLStar = m_sDAF_TBL.af_dac_min;
        MINT32 i4TBLEnd = m_sDAF_TBL.af_dac_max;
        MINT32 i4TBLStep = m_ptrNVRam->rAFNVRAM.sAF_Coef.sTABLE.i4Pos[21];

        if((i4TBLStar<0||i4TBLStar>960)||(i4TBLEnd<i4TBLStar||i4TBLEnd>1023)||(i4TBLStep<1||i4TBLStep>256))
            return (MVOID*)i4Pos;

        MINT32 i4TBLNum = (i4TBLEnd - i4TBLStar)/i4TBLStep + 1;
        if(i4TBLNum < 3 || i4TBLNum> AF_TABLE_NUM)
            return (MVOID*)i4Pos;

        for(MINT32 i=0; i<i4TBLNum; i++)
        {
            i4Pos[i] = i4TBLStar + i4TBLStep*i;
            if( i4Pos[i] > 1023)   i4Pos[i]=1023;
        }
    }
    return (MVOID*)i4Pos;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::setMFPos( MINT32 a_i4Pos, MUINT32 u4Caller)
{
    if( u4Caller==AF_MGR_CALLER)
    {
        CAM_LOGD( "%s Dev %d : %d",
                  __FUNCTION__,
                  m_i4CurrSensorDev,
                  a_i4Pos);
    }
    else
    {
        CAM_LOGD_IF( m_i4DgbLogLv,
                     "cmd-%s Dev %d : %d",
                     __FUNCTION__,
                     m_i4CurrSensorDev,
                     a_i4Pos);
    }



    if( (m_eLIB3A_AFMode == LIB3A_AF_MODE_MF) &&
            (m_i4MFPos != a_i4Pos) &&
            (0<=a_i4Pos) &&
            (a_i4Pos<=1023))
    {
        if( u4Caller==AF_MGR_CALLER)
        {
            CAM_LOGD( "%s set MF pos (%d)->(%d)",
                      __FUNCTION__,
                      m_sAFOutput.i4AFPos,
                      a_i4Pos);
        }
        else
        {
            CAM_LOGD( "cmd-%s set MF pos (%d)->(%d)",
                      __FUNCTION__,
                      m_sAFOutput.i4AFPos,
                      a_i4Pos);
        }

        m_i4MFPos = a_i4Pos;

        if( m_pIAfAlgo)
        {
            m_pIAfAlgo->setMFPos( m_i4MFPos);
            m_pIAfAlgo->trigger();
        }
        else
        {
            CAM_LOGD( "Null m_pIAfAlgo");
        }
    }
    else
    {
        if( u4Caller==AF_MGR_CALLER)
        {
            CAM_LOGD( "%s skip set MF pos (%d)->(%d), lib_afmode(%d)",
                      __FUNCTION__,
                      m_sAFOutput.i4AFPos,
                      a_i4Pos,
                      m_eLIB3A_AFMode);
        }
        else
        {
            CAM_LOGD_IF( m_i4DgbLogLv,
                         "cmd-%s skip set MF pos (%d)->(%d), lib_afmode(%d)",
                         __FUNCTION__,
                         m_sAFOutput.i4AFPos,
                         a_i4Pos,
                         m_eLIB3A_AFMode);
        }
    }
    return S_AF_OK;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::setFullScanstep( MINT32 a_i4Step)
{
    //if (m_i4FullScanStep != a_i4Step)
    {
        CAM_LOGD( "[setFullScanstep]%x", a_i4Step);

        m_i4FullScanStep = a_i4Step;
        m_aCurEMAFFlag = EM_AF_FLAG_NONE; // clear FullScan flag
        if( m_i4FullScanStep>0) /* Step > 0 , set Full Scan Mode */
        {
            MINT32 FullScanStep = property_get_int32("vendor.debug.af_fullscan.step", 0);
            if (FullScanStep > 0)
            {
                m_i4FullScanStep = (m_i4FullScanStep & 0xFFFF0000) | (FullScanStep & 0x0000FFFF);
                CAM_LOGD( "[adb prop][setFullScanstep] %x", m_i4FullScanStep);
            }
            m_bIsFullScan = MTRUE;
            m_sAFInput.i4FullScanStep = m_i4FullScanStep;

            // for Advanced Full Scan
            m_aCurEMAFFlag |= EM_AF_FLAG_FULLSCAN_NORMAL;
            m_aAdvFSRepeatTime = property_get_int32("vendor.mtk.client.em.af_advfs.rpt", 10);

            if(property_get_int32("vendor.mtk.client.em.af_advfs.enable", 0) == 1)
            {
                m_aCurEMAFFlag |= EM_AF_FLAG_FULLSCAN_ADVANCE;
            }
            else if (property_get_int32("vendor.mtk.client.em.af_cali_flag", 0) == 1)
            {
                m_aCurEMAFFlag |= EM_AF_FLAG_TEMP_CALI;
            }
        }
        else
        {
            m_bIsFullScan = MFALSE;
        }

        if(m_aCurEMAFFlag != m_aPreEMAFFlag)
        {
            setAFMode(MTK_CONTROL_AF_MODE_EDOF, AF_MGR_CALLER);
            setAFMode(MTK_CONTROL_AF_MODE_AUTO, AF_MGR_CALLER);
            m_aPreEMAFFlag = m_aCurEMAFFlag;
        }
    }
    return S_AF_OK;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::getAFRefWin( CameraArea_T &rWinSize)
{
    //The coordinate is base on TG coordinate which is applied binning information.
    if (m_i4EnableAF == 0)
    {
        CAM_LOGD_IF( m_i4DgbLogLv,"%s", __FUNCTION__);
        rWinSize.i4Left = 0;
        rWinSize.i4Right = 0;
        rWinSize.i4Top = 0;
        rWinSize.i4Bottom = 0;
        return;
    }
    else
    {
        rWinSize.i4Left = m_sArea_HW.i4X;
        rWinSize.i4Right = m_sArea_HW.i4X + m_sArea_HW.i4W;
        rWinSize.i4Top = m_sArea_HW.i4Y;
        rWinSize.i4Bottom = m_sArea_HW.i4Y + m_sArea_HW.i4H;

        rWinSize.i4Left = rWinSize.i4Left * m_i4BINSzW / m_i4TGSzW;
        rWinSize.i4Right = rWinSize.i4Right * m_i4BINSzW / m_i4TGSzW;
        rWinSize.i4Top = rWinSize.i4Top * m_i4BINSzH / m_i4TGSzH;
        rWinSize.i4Bottom = rWinSize.i4Bottom * m_i4BINSzH / m_i4TGSzH;
    }
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::getAF2AEInfo(AF2AEInfo_T &rAFInfo)
{
    rAFInfo.i4IsAFDone      = (m_sAFOutput.i4IsAfSearch==AF_SEARCH_DONE)?1:0;
    rAFInfo.i4AfDac         = m_sCurLensInfo.i4CurrentPos;
    rAFInfo.i4IsSceneStable = m_sAFOutput.i4IsSceneStable;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::setAE2AFInfo( AE2AFInfo_T &rAEInfo)
{
    if (m_i4EnableAF == 0)
    {
        m_sAFInput.i8GSum       = 50;
        m_sAFInput.i4ISO        = 100;
        m_sAFInput.i4IsAEStable = 1;
        m_sAFInput.i4SceneLV    = 80;
        return;
    }

    m_i4isAEStable = ((rAEInfo.i4IsAEStable==1)||(rAEInfo.i4IsAELocked==1)) ? 1: 0;

    m_sAFInput.i8GSum             = rAEInfo.iYvalue;
    m_sAFInput.i4ISO              = rAEInfo.i4ISO;
    m_sAFInput.i4IsAEStable       = m_i4isAEStable;
    m_sAFInput.i4SceneLV          = rAEInfo.i4SceneLV;
    m_sAFInput.i4ShutterValue     = rAEInfo.ishutterValue;
    m_sAFInput.i4IsFlashFrm       = rAEInfo.i4IsFlashFrm;
    m_sAFInput.i4AEBlockAreaYCnt  = rAEInfo.i4AEBlockAreaYCnt;
    m_sAFInput.pAEBlockAreaYvalue = rAEInfo.pAEBlockAreaYvalue;
    memcpy( m_aAEBlkVal, rAEInfo.aeBlockV, 25);

    if( m_i4isAEStable==1)
    {
        UpdateState( EVENT_AE_IS_STABLE);
    }

    if( m_i4DgbLogLv)
    {
        CAM_LOGD_IF( m_i4DgbLogLv,
                     "#(%5d,%5d) cmd-%s Dev(%d) GSum(%lld) ISO(%d) AEStb(%d,%d) AELock(%d) LV(%d) shutter(%d) flash(%d) DeltaBV(%d) AEBlk(%d)(%p)",
                     m_u4ReqMagicNum,
                     m_u4StaMagicNum,
                     __FUNCTION__,
                     m_i4CurrSensorDev,
                     (long long)m_sAFInput.i8GSum,
                     m_sAFInput.i4ISO,
                     m_sAFInput.i4IsAEStable,
                     rAEInfo.i4IsAEStable,
                     rAEInfo.i4IsAELocked,
                     rAEInfo.i4SceneLV,
                     m_sAFInput.i4ShutterValue,
                     m_sAFInput.i4IsFlashFrm,
                     m_sAFInput.i4DeltaBV,
                     m_sAFInput.i4AEBlockAreaYCnt,
                     (void*)m_sAFInput.pAEBlockAreaYvalue);

    }
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::setIspSensorInfo2AF(ISP_SENSOR_INFO_T ispSensorInfo)
{
    CAM_LOGD_IF( m_i4DgbLogLv,
                 "cmd-%s FrameId(%5d), AfeGain(%5d), IspGain(%4d), AEStable(%d), AELock(%d), hlrEnable(%d), Eposuretime(%d), DeltaIdx(%4d), RealISO(%d), MaxISO(%d), AEStableCnt(%d), bAEScenarioChange(%d), u4AEFinerEVIdxBase(%d), u4AEidxCurrentF(%d)",
                 __FUNCTION__,
                 ispSensorInfo.i4FrameId,
                 ispSensorInfo.u4AfeGain,
                 ispSensorInfo.u4IspGain,
                 ispSensorInfo.bAEStable,
                 ispSensorInfo.bAELock,
                 ispSensorInfo.bHLREnable,
                 ispSensorInfo.u4Eposuretime,
                 ispSensorInfo.i4deltaIndex,
                 ispSensorInfo.u4RealISOValue,
                 ispSensorInfo.u4MaxISO,
                 ispSensorInfo.u4AEStableCnt,
                 ispSensorInfo.bAEScenarioChange,
                 ispSensorInfo.u4AEFinerEVIdxBase,
                 ispSensorInfo.u4AEidxCurrentF);

    m_vISQueue.pushHeadAnyway(ispSensorInfo);

    if (m_i4DbgAfegainQueue & 0x1)
    {
        MUINT32 front = m_vISQueue.head;
        MUINT32 end = m_vISQueue.tail;

        CAM_LOGD("--> Head:%d, Tail:%d, FrameId:%d, AfeGain:%d, IspGain:%d, hlrEnable:%d",
                 front,
                 end,
                 ispSensorInfo.i4FrameId,
                 ispSensorInfo.u4AfeGain,
                 ispSensorInfo.u4IspGain,
                 ispSensorInfo.bHLREnable);

        if (end > front)
        {
            front += m_vISQueue.queueSize;
        }

        for (MUINT32 i=front; i>end; i--)
        {
            MUINT32 idx = i % m_vISQueue.queueSize;
            CAM_LOGD("AfeGain qIdx(%d): frameId(%d), afeGain(%d), ispGain(%d), hlrEnable:%d",
                     idx,
                     m_vISQueue.content[idx].i4FrameId,
                     m_vISQueue.content[idx].u4AfeGain,
                     m_vISQueue.content[idx].u4IspGain,
                     m_vISQueue.content[idx].bHLREnable);
        }
    }
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MINT32 AfMgr::PDPureRawInterval ()
{
#ifdef PD_PURE_RAW_INTERVAL
    return PD_PURE_RAW_INTERVAL;
#else
    return 1;
#endif
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::passAFBuffer( MVOID *ptrInAFData)
{
    //---------------------------------------Statistic data information--------------------------------------------
    StatisticBufInfo *ptrStatInfo = reinterpret_cast<StatisticBufInfo *>( ptrInAFData);
    m_u4StaMagicNum   = ptrStatInfo->mMagicNumber;
    m_u4FrameCount    = ptrStatInfo->mFrameCount;
    m_u4ConfigHWNum   = ptrStatInfo->mConfigNum;
    m_u4ConfigLatency = ptrStatInfo->mConfigLatency;

    m_sAFInput.i4IsSupportN2Frame = (m_u4ConfigLatency == 3) ? 0 : 1;

    //Got AF statistic from DMA buffer.
    AAA_TRACE_D("ConvertBufToStat (%d)", m_u4StaMagicNum);
    ConvertDMABufToStat( m_sAFOutput.i4AFPos, ptrInAFData, m_sAFInput.sStatProfile);
    AAA_TRACE_END_D;

    return S_AF_OK;
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MINT32 AfMgr::isFocusFinish()
{
    MINT32 ret = 1;
    CAM_LOGD_IF(m_i4DgbLogLv, "%s, m_eAFState=%d", __FUNCTION__, m_eAFState);
    if( (m_eAFState == E_AF_PASSIVE_SCAN) ||
            (m_eAFState == E_AF_ACTIVE_SCAN) )
    {
        ret = 0;
    }
    return ret;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MINT32 AfMgr::isFocused()
{
    MINT32 ret = 0;

    if( (m_eAFState == E_AF_PASSIVE_FOCUSED) ||
            (m_eAFState == E_AF_FOCUSED_LOCKED))
    {
        ret = 1;
    }

    CAM_LOGD_IF( m_i4DgbLogLv,
                 "%s %d %d",
                 __FUNCTION__,
                 m_eAFState,
                 ret);
    return ret;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::getDebugInfo( AF_DEBUG_INFO_T &rAFDebugInfo)
{
    MRESULT ret = E_3A_ERR;

    CAM_LOGD_IF( m_i4DgbLogLv&4,
                 "%s Dev %d",
                 __FUNCTION__,
                 m_i4CurrSensorDev);

    //reset.
    memset( &rAFDebugInfo, 0, sizeof(AF_DEBUG_INFO_T));

    /* Do not modify following oder: */

    //1. Hybrid AF library
    if( m_pIAfAlgo)
    {
        ret = m_pIAfAlgo->getDebugInfo( rAFDebugInfo);
    }

    //2. PD library
    if( m_i4PDAF_support_type!=PDAF_SUPPORT_NA)
    {
        //Not support open pd library.
        if( m_bEnablePD)
        {
            ret = IPDMgr::getInstance().GetDebugInfo( m_i4CurrSensorDev, rAFDebugInfo);
        }
    }

    //3. af mgr
    ret = GetMgrDbgInfo( rAFDebugInfo);
    return ret;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::TimeOutHandle()
{
    CAM_LOGD( "#(%5d,%5d) SPECIAL_EVENT cmd-%s Dev(%d)",
              m_u4ReqMagicNum,
              m_u4StaMagicNum,
              __FUNCTION__,
              m_i4CurrSensorDev);

    m_pIAfAlgo->cancel();
    UpdateState(EVENT_SEARCHING_END);
}

MFLOAT AfMgr::getFocusDistance()
{
    MINT32 fdacidx=0;
    MINT32 i4tblLL;
    MINT32 i4ResultDist  = 100;

    if( m_i4EnableAF==0)
    {
        CAM_LOGD_IF(m_i4DgbLogLv, "cmd-%s no AF", __FUNCTION__);
        return 0.0;
    }


    MINT32 LensPos;
    if( m_eLIB3A_AFMode == LIB3A_AF_MODE_MF)
    {
        /* In MF mode, algorithm will take some step to target position.
                  So directly using MFpos instead of using m_sAFOutput.i4AFPos.*/
        LensPos = m_i4MFPos;
    }
    else
    {
        LensPos = m_sAFOutput.i4AFPos;
    }


    // Lens position is changed, calculating new focus distance :
    if( m_sFocusDis.i4LensPos!=LensPos)
    {
        i4tblLL = m_pAFParam->i4TBLL;
        if( LensPos <= m_pAFParam->i4Dacv[i4tblLL-1]) /*infinity*/
        {
            i4ResultDist = m_pAFParam->i4Dist[i4tblLL-1];
        }
        else if( m_pAFParam->i4Dacv[0] <= LensPos) /*macro*/
        {
            i4ResultDist = m_pAFParam->i4Dist[0];

        }
        else
        {
            for(fdacidx=0; fdacidx< i4tblLL ; fdacidx++)
            {
                if( LensPos >  m_pAFParam->i4Dacv[fdacidx])
                    break;
            }
            if(fdacidx==0)
            {
                i4ResultDist = m_pAFParam->i4Dist[0];
            }
            else
            {
                i4ResultDist=
                    ( m_pAFParam->i4Dist[fdacidx]   * (m_pAFParam->i4Dacv[fdacidx-1] - LensPos)
                      + m_pAFParam->i4Dist[fdacidx-1] * (LensPos - m_pAFParam->i4Dacv[fdacidx]))
                    /(m_pAFParam->i4Dacv[fdacidx-1] - m_pAFParam->i4Dacv[fdacidx] );
            }
            if( i4ResultDist<=0)
            {
                i4ResultDist = m_pAFParam->i4Dist[i4tblLL-1];
            }
        }

        MFLOAT i4Dist = 1000.0/ ((MFLOAT)i4ResultDist);

        CAM_LOGD_IF(m_i4DgbLogLv, "cmd-%s Pos %d->%d, Dis %f->%f\n",
                    __FUNCTION__,
                    m_sFocusDis.i4LensPos,
                    LensPos,
                    m_sFocusDis.i4Dist,
                    i4Dist);

        //record.
        m_sFocusDis.i4LensPos = LensPos;
        m_sFocusDis.i4Dist = i4Dist;
    }

    return m_sFocusDis.i4Dist;
}

MVOID AfMgr::getFocusRange( MFLOAT *vnear, MFLOAT *vfar)
{
    MINT32 i4tblLL;
    MINT32 fdacidx       =0;
    MINT32 i4ResultDist  = 100;
    MINT32 i4ResultRange = 100;
    MINT32 i4ResultNear  = 100;
    MINT32 i4ResultFar   = 100;
    MFLOAT retDist       = 0.0;

    if( m_i4EnableAF==0)
    {
        CAM_LOGD_IF(m_i4DgbLogLv, "no focus lens");
        *vnear = 1/0.6;
        *vfar  = 1/3.0;
        return;
    }

    i4tblLL = m_pAFParam->i4TBLL;
    for( fdacidx=0; fdacidx<i4tblLL; fdacidx++)
    {
        if( m_sAFOutput.i4AFPos>m_pAFParam->i4Dacv[fdacidx])
            break;
    }

    if(fdacidx==0)
    {
        i4ResultDist = m_pAFParam->i4Dist[0];
    }
    else
    {
        i4ResultDist=
            ( m_pAFParam->i4Dist[fdacidx  ] * (m_pAFParam->i4Dacv[fdacidx-1] - m_sAFOutput.i4AFPos       )  +
              m_pAFParam->i4Dist[fdacidx-1] * (m_sAFOutput.i4AFPos          - m_pAFParam->i4Dacv[fdacidx])) /
            ( m_pAFParam->i4Dacv[fdacidx-1] - m_pAFParam->i4Dacv[fdacidx]);

        i4ResultRange=
            ( m_pAFParam->i4FocusRange[fdacidx  ] * (m_pAFParam->i4Dacv[fdacidx-1] - m_sAFOutput.i4AFPos       )  +
              m_pAFParam->i4FocusRange[fdacidx-1] * (m_sAFOutput.i4AFPos          - m_pAFParam->i4Dacv[fdacidx])) /
            ( m_pAFParam->i4Dacv[fdacidx-1] - m_pAFParam->i4Dacv[fdacidx]);
    }
    if( i4ResultDist <=0)  i4ResultDist= m_pAFParam->i4Dist[i4tblLL-1];
    if( i4ResultRange<=0) i4ResultRange= m_pAFParam->i4Dist[i4tblLL-1];

    i4ResultNear = i4ResultDist - (i4ResultRange/2);
    i4ResultFar  = i4ResultDist + (i4ResultRange/2);

    *vnear = 1000.0/ ((MFLOAT)i4ResultNear);
    *vfar  = 1000.0/ ((MFLOAT)i4ResultFar);

    //CAM_LOGD("[getFocusRange] [%f, %f]", *vnear,*vfar);
}

MINT32 AfMgr::getLensState  ()
{
    MINT32 ret = 0;

    if(m_i4GyroValue>GYRO_THRESHOLD && m_sAFOutput.i4IsAfSearch==AF_SEARCH_TARGET_MOVE)
    {
        // FAKE STATIONARY : Phone Moving while TRACKING
        ret = 0;
        if(m_bForceCapture==0)
        {
            CAM_LOGW("%s Phone Moving while TRACKING, ForceCapture: LensState = %d", __FUNCTION__, ret);
        }
        m_bForceCapture = 1;
        m_i4ContinuePDMovingCount=0;
    }
    else if(m_sAFOutput.i4IsAfSearch==AF_SEARCH_TARGET_MOVE)
    {
        m_i4ContinuePDMovingCount += (m_i4ContinuePDMovingCount<MAX_PDMOVING_COUNT) ? 1 : 0;
        if(m_i4ContinuePDMovingCount>=MAX_PDMOVING_COUNT)
        {
            // FAKE STATIONARY : Keep PDMoving with the stable phone for more than 6 frames
            ret = 0;
            if(m_bForceCapture==0)
            {
                CAM_LOGW("%s Keep PDMoving for more than %d frames, ForceCapture: LensState = %d", __FUNCTION__, MAX_PDMOVING_COUNT, ret);
            }
            m_bForceCapture = 1;
        }
        else
        {
            // MOVING : PDMoving
            ret = 1;
            m_bForceCapture = 0;
        }
    }
    else if(m_sAFOutput.i4IsAfSearch!=AF_SEARCH_DONE)
    {
        // MOVING : FineSearch or ContrastAF
        ret = 1;
        m_i4ContinuePDMovingCount=0;
        m_bForceCapture = 0;
    }
    else
    {
        // STATIONARY
        ret = 0;
        m_i4ContinuePDMovingCount=0;
        m_bForceCapture = 0;
    }


    m_i4MvLensToPre = m_i4MvLensTo;

    m_lensState = ret;

    return ret;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL AfMgr::sem_wait_rt(sem_t *pSem, nsecs_t reltime, const char* info)
{
    CAM_LOGD_IF(m_i4DgbLogLv&4, "[%s] pSem(%p), reltime(%lld), info(%s)", __FUNCTION__, pSem, (long long)reltime, info);
    struct timespec ts;

    if (clock_gettime(CLOCK_REALTIME, &ts) == -1)
        CAM_LOGE("error in clock_gettime! Please check\n");

    ts.tv_sec  += reltime/1000000000;
    ts.tv_nsec += reltime%1000000000;
    if (ts.tv_nsec >= 1000000000)
    {
        ts.tv_nsec -= 1000000000;
        ts.tv_sec += 1;
    }

    int s = sem_timedwait(pSem, &ts);
    if (s == -1)
    {
        if (errno == ETIMEDOUT)
        {
            return MFALSE;
        }
        else
        {
            CAM_LOGD_IF(m_i4DgbLogLv, "[%s][%s]sem_timedwait() errno = %d\n", __FUNCTION__, info, errno);
        }
    }

    return MTRUE;
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MINT32 AfMgr::getDAFtbl( MVOID **ptbl)
{
    *ptbl = &m_sDAF_TBL;
    return DAF_TBL_QLEN;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MINT32 AfMgr::sendAFCtrl(MUINT32 eAFCtrl, MINTPTR iArg1, MINTPTR iArg2)
{
    MINT32 i4Ret = MTRUE;
    switch (eAFCtrl)
    {
        case EAFMgrCtrl_GetFocusValue:
            m_sFRMInfo.i8FocusValue = m_sAFOutput.i8AFValue;
            m_sFRMInfo.i4LensPos    = m_sAFOutput.i4AFPos;
            for(int i=0; i<3; i++)
            {
                m_sFRMInfo.GyroValue[i] = m_i4GyroInfo[i];
            }
            m_sFRMInfo.AFROI[0]     = m_sArea_Focusing.i4X;
            m_sFRMInfo.AFROI[1]     = m_sArea_Focusing.i4Y;
            m_sFRMInfo.AFROI[2]     = m_sArea_Focusing.i4W;
            m_sFRMInfo.AFROI[3]     = m_sArea_Focusing.i4H;
            m_sFRMInfo.AFROI[4]     = (m_sArea_TypeSel == AF_ROI_SEL_FD) ? 1 : 0;
            *(reinterpret_cast<MINT32*>(iArg1)) = m_u4StaMagicNum;
            *(reinterpret_cast<AF_FRAME_INFO_T*>(iArg2)) = m_sFRMInfo;
            //
            if(m_i4DgbLogLv)
            {
                CAM_LOGD_IF(m_i4DgbLogLv,
                            "[%s] sttNum(#%d) FV(%d) DAC(%d) Gyro(%d, %d, %d) ROI(%d,%d,%d,%d,%d)",
                            __FUNCTION__,
                            m_u4StaMagicNum,
                            (MUINT32)m_sFRMInfo.i8FocusValue,
                            m_sFRMInfo.i4LensPos,
                            m_sFRMInfo.GyroValue[0], m_sFRMInfo.GyroValue[1], m_sFRMInfo.GyroValue[2],
                            m_sFRMInfo.AFROI[0], m_sFRMInfo.AFROI[1], m_sFRMInfo.AFROI[2], m_sFRMInfo.AFROI[3], m_sFRMInfo.AFROI[4]);
            }
            break;
    }
    return i4Ret;
}
