/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#define LOG_TAG "isp_mgr_obc"

#ifndef ENABLE_MY_LOG
    #define ENABLE_MY_LOG       (1)
#endif

#include <cutils/properties.h>
#include <aaa_types.h>
#include <aaa_error_code.h>
#include <mtkcam/utils/std/Log.h>
#include "isp_mgr.h"
#include "isp_mgr_helper.h"

namespace NSIspTuningv3
{

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  OBC
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ISP_MGR_OBC_T&
ISP_MGR_OBC_T::
getInstance(ESensorDev_T const eSensorDev)
{
    switch (eSensorDev)
    {
    case ESensorDev_Main: //  Main Sensor
        return  ISP_MGR_OBC_DEV<ESensorDev_Main>::getInstance();
    case ESensorDev_MainSecond: //  Main Second Sensor
        return  ISP_MGR_OBC_DEV<ESensorDev_MainSecond>::getInstance();
    case ESensorDev_Sub: //  Sub Sensor
        return  ISP_MGR_OBC_DEV<ESensorDev_Sub>::getInstance();
    case ESensorDev_SubSecond: //  Main Second Sensor
        return  ISP_MGR_OBC_DEV<ESensorDev_SubSecond>::getInstance();
    default:
        CAM_LOGE("eSensorDev = %d", eSensorDev);
        return  ISP_MGR_OBC_DEV<ESensorDev_Main>::getInstance();
    }
}

template <>
ISP_MGR_OBC_T&
ISP_MGR_OBC_T::
put(MUINT8 SubModuleIndex, ISP_NVRAM_OBC_T const& rParam)
{
    PUT_REG_INFO_MULTI(SubModuleIndex, CTL,                    ctl);
    PUT_REG_INFO_MULTI(SubModuleIndex, DBS,                    dbs);
    PUT_REG_INFO_MULTI(SubModuleIndex, GRAY_BLD_0,             gray_bld_0);
    PUT_REG_INFO_MULTI(SubModuleIndex, GRAY_BLD_1,             gray_bld_1);
    PUT_REG_INFO_MULTI(SubModuleIndex, GRAY_BLD_2,             gray_bld_2);
    PUT_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_R0,            bias_lut_r0);
    PUT_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_R1,            bias_lut_r1);
    PUT_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_R2,            bias_lut_r2);
    PUT_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_R3,            bias_lut_r3);
    PUT_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_G0,            bias_lut_g0);
    PUT_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_G1,            bias_lut_g1);
    PUT_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_G2,            bias_lut_g2);
    PUT_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_G3,            bias_lut_g3);
    PUT_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_B0,            bias_lut_b0);
    PUT_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_B1,            bias_lut_b1);
    PUT_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_B2,            bias_lut_b2);
    PUT_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_B3,            bias_lut_b3);
    PUT_REG_INFO_MULTI(SubModuleIndex, WBG_RB,                 wbg_rb);
    PUT_REG_INFO_MULTI(SubModuleIndex, WBG_G,                  wbg_g);
    PUT_REG_INFO_MULTI(SubModuleIndex, WBIG_RB,                wbig_rb);
    PUT_REG_INFO_MULTI(SubModuleIndex, WBIG_G,                 wbig_g);
    PUT_REG_INFO_MULTI(SubModuleIndex, OBG_RB,                 obg_rb);
    PUT_REG_INFO_MULTI(SubModuleIndex, OBG_G,                  obg_g);
    PUT_REG_INFO_MULTI(SubModuleIndex, OFFSET_R,               offset_r);
    PUT_REG_INFO_MULTI(SubModuleIndex, OFFSET_GR,              offset_gr);
    PUT_REG_INFO_MULTI(SubModuleIndex, OFFSET_GB,              offset_gb);
    PUT_REG_INFO_MULTI(SubModuleIndex, OFFSET_B,               offset_b);
    PUT_REG_INFO_MULTI(SubModuleIndex, HDR,                    hdr);


    return  (*this);
}


template <>
ISP_MGR_OBC_T&
ISP_MGR_OBC_T::
get(MUINT8 SubModuleIndex, ISP_NVRAM_OBC_T& rParam)
{
    GET_REG_INFO_MULTI(SubModuleIndex, CTL,                    ctl);
    GET_REG_INFO_MULTI(SubModuleIndex, DBS,                    dbs);
    GET_REG_INFO_MULTI(SubModuleIndex, GRAY_BLD_0,             gray_bld_0);
    GET_REG_INFO_MULTI(SubModuleIndex, GRAY_BLD_1,             gray_bld_1);
    GET_REG_INFO_MULTI(SubModuleIndex, GRAY_BLD_2,             gray_bld_2);
    GET_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_R0,            bias_lut_r0);
    GET_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_R1,            bias_lut_r1);
    GET_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_R2,            bias_lut_r2);
    GET_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_R3,            bias_lut_r3);
    GET_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_G0,            bias_lut_g0);
    GET_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_G1,            bias_lut_g1);
    GET_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_G2,            bias_lut_g2);
    GET_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_G3,            bias_lut_g3);
    GET_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_B0,            bias_lut_b0);
    GET_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_B1,            bias_lut_b1);
    GET_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_B2,            bias_lut_b2);
    GET_REG_INFO_MULTI(SubModuleIndex, BIAS_LUT_B3,            bias_lut_b3);
    GET_REG_INFO_MULTI(SubModuleIndex, WBG_RB,                 wbg_rb);
    GET_REG_INFO_MULTI(SubModuleIndex, WBG_G,                  wbg_g);
    GET_REG_INFO_MULTI(SubModuleIndex, WBIG_RB,                wbig_rb);
    GET_REG_INFO_MULTI(SubModuleIndex, WBIG_G,                 wbig_g);
    GET_REG_INFO_MULTI(SubModuleIndex, OBG_RB,                 obg_rb);
    GET_REG_INFO_MULTI(SubModuleIndex, OBG_G,                  obg_g);
    GET_REG_INFO_MULTI(SubModuleIndex, OFFSET_R,               offset_r);
    GET_REG_INFO_MULTI(SubModuleIndex, OFFSET_GR,              offset_gr);
    GET_REG_INFO_MULTI(SubModuleIndex, OFFSET_GB,              offset_gb);
    GET_REG_INFO_MULTI(SubModuleIndex, OFFSET_B,               offset_b);
    GET_REG_INFO_MULTI(SubModuleIndex, HDR,                    hdr);

    return  (*this);
}

MBOOL
ISP_MGR_OBC_T::
apply_P1(MUINT8 SubModuleIndex, const RAWIspCamInfo& rRawIspCamInfo, TuningMgr& rTuning, MINT32 i4SubsampleIdex)
{
    MBOOL bOBC_EN = isEnable(SubModuleIndex);

#if 0
    //useing case
    rTuning.updateEngine(eTuningMgrFunc_OBC_R1, bOBC_R1_EN, i4SubsampleIdex);
    TUNING_MGR_WRITE_BITS_CAM((&rTuning), CAMCTL_R1_CAMCTL_EN, CAMCTL_OBC_R1_EN, bOBC_R1_EN, i4SubsampleIdex);
    ISP_MGR_CTL_EN_P1_T::getInstance(m_eSensorDev).setEnable_OBC_R1(bOBC_R1_EN);

    if(bOBC_R1_EN){
        MUINT32 GAIN_R = rRawIspCamInfo.rRPG.gain_2.bits.RPG_GAIN_R;
        MUINT32 GAIN_B = rRawIspCamInfo.rRPG.gain_1.bits.RPG_GAIN_B;
        MUINT32 GAIN_G = (rRawIspCamInfo.rRPG.gain_2.bits.RPG_GAIN_GR + rRawIspCamInfo.rRPG.gain_1.bits.RPG_GAIN_GB + 1) >> 1;

        reinterpret_cast<ISP_CAM_OBC_R1_GAIN_0_T*>(REG_INFO_VALUE_PTR(CAM_OBC_R1_GAIN_0))->OBC_R1_GAIN_R = GAIN_R;
        reinterpret_cast<ISP_CAM_OBC_R1_GAIN_0_T*>(REG_INFO_VALUE_PTR(CAM_OBC_R1_GAIN_0))->OBC_R1_GAIN_B = GAIN_B;
        reinterpret_cast<ISP_CAM_OBC_R1_GAIN_1_T*>(REG_INFO_VALUE_PTR(CAM_OBC_R1_GAIN_1))->OBC_R1_GAIN_G = GAIN_G;

        MUINT32 IVGN_R = ((1<<19)/(GAIN_R+1))>>1;
        MUINT32 IVGN_B = ((1<<19)/(GAIN_B+1))>>1;
        MUINT32 IVGN_G = ((1<<19)/(GAIN_G+1))>>1;

        reinterpret_cast<ISP_CAM_OBC_R1_GAIN_0_T*>(REG_INFO_VALUE_PTR(CAM_OBC_R1_IVGN_0))->OBC_R1_GAIN_R = IVGN_R;
        reinterpret_cast<ISP_CAM_OBC_R1_GAIN_0_T*>(REG_INFO_VALUE_PTR(CAM_OBC_R1_IVGN_0))->OBC_R1_GAIN_B = IVGN_B;
        reinterpret_cast<ISP_CAM_OBC_R1_GAIN_1_T*>(REG_INFO_VALUE_PTR(CAM_OBC_R1_IVGN_1))->OBC_R1_GAIN_G = IVGN_G;


        // Register setting
        rTuning.tuningMgrWriteRegs(static_cast<TUNING_MGR_REG_IO_STRUCT*>(m_pRegInfo), m_u4RegInfoNum, i4SubsampleIdex);
    }

    // Register setting
    rTuning.tuningMgrWriteRegs(static_cast<TUNING_MGR_REG_IO_STRUCT*>(m_pRegInfo[SubModuleIndex]), m_u4RegInfoNum, i4SubsampleIdex);

    //using case
    dumpRegInfoP1("OBC_R1");
#endif

    return  MTRUE;
}



MBOOL
ISP_MGR_OBC_T::
apply_P2(MUINT8 SubModuleIndex, const RAWIspCamInfo& rRawIspCamInfo, dip_x_reg_t* pReg)
{
    MBOOL bOBC_EN = isEnable(SubModuleIndex);

#if 0
    //useing case
    // TOP
    ISP_WRITE_ENABLE_BITS(pReg, DIP_X_CTL_RGB2_EN, OBC_D1_EN, bOBC_D1_EN);

    ISP_NVRAM_PGN_T pgn;
    getIspHWBuf(m_eSensorDev, pgn );

    MUINT32 GAIN_R = pgn.gain_2.bits.PGN_GAIN_R;
    MUINT32 GAIN_B = pgn.gain_1.bits.PGN_GAIN_B;
    MUINT32 GAIN_G = (pgn.gain_2.bits.PGN_GAIN_GR + pgn.gain_1.bits.PGN_GAIN_GB + 1) >> 1;

    reinterpret_cast<ISP_CAM_OBC_R1_GAIN_0_T*>(REG_INFO_VALUE_PTR(DIP_X_OBC_D1_GAIN_0))->OBC_R1_GAIN_R = GAIN_R;
    reinterpret_cast<ISP_CAM_OBC_R1_GAIN_0_T*>(REG_INFO_VALUE_PTR(DIP_X_OBC_D1_GAIN_0))->OBC_R1_GAIN_B = GAIN_B;
    reinterpret_cast<ISP_CAM_OBC_R1_GAIN_1_T*>(REG_INFO_VALUE_PTR(DIP_X_OBC_D1_GAIN_1))->OBC_R1_GAIN_G = GAIN_G;

    MUINT32 IVGN_R = ((1<<19)/(GAIN_R+1))>>1;
    MUINT32 IVGN_B = ((1<<19)/(GAIN_B+1))>>1;
    MUINT32 IVGN_G = ((1<<19)/(GAIN_G+1))>>1;

    reinterpret_cast<ISP_CAM_OBC_R1_GAIN_0_T*>(REG_INFO_VALUE_PTR(DIP_X_OBC_D1_IVGN_0))->OBC_R1_GAIN_R = IVGN_R;
    reinterpret_cast<ISP_CAM_OBC_R1_GAIN_0_T*>(REG_INFO_VALUE_PTR(DIP_X_OBC_D1_IVGN_0))->OBC_R1_GAIN_B = IVGN_B;
    reinterpret_cast<ISP_CAM_OBC_R1_GAIN_1_T*>(REG_INFO_VALUE_PTR(DIP_X_OBC_D1_IVGN_1))->OBC_R1_GAIN_G = IVGN_G;
    //reinterpret_cast<DIP_X_OBC_D1_HDR_T*>(REG_INFO_VALUE_PTR(DIP_X_OBC_D1_HDR))->OBC_R1_HDR_OSCTH = 4090;


    // Register setting
    writeRegs(static_cast<RegInfo_T*>(m_pRegInfo[SubModuleIndex]), m_u4RegInfoNum, pReg);


    //useing case
    dumpRegInfo("OBC_D1");
#endif

    return  MTRUE;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  MOBC
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ISP_MGR_MOBC_T&
ISP_MGR_MOBC_T::
getInstance(ESensorDev_T const eSensorDev)
{
    switch (eSensorDev)
    {
    case ESensorDev_Main: //  Main Sensor
        return  ISP_MGR_MOBC_DEV<ESensorDev_Main>::getInstance();
    case ESensorDev_MainSecond: //  Main Second Sensor
        return  ISP_MGR_MOBC_DEV<ESensorDev_MainSecond>::getInstance();
    case ESensorDev_Sub: //  Sub Sensor
        return  ISP_MGR_MOBC_DEV<ESensorDev_Sub>::getInstance();
    case ESensorDev_SubSecond: //  Main Second Sensor
        return  ISP_MGR_MOBC_DEV<ESensorDev_SubSecond>::getInstance();
    default:
        CAM_LOGE("eSensorDev = %d", eSensorDev);
        return  ISP_MGR_MOBC_DEV<ESensorDev_Main>::getInstance();
    }
}

MBOOL
ISP_MGR_MOBC_T::
apply_P1(MUINT8 SubModuleIndex, const RAWIspCamInfo& rRawIspCamInfo, TuningMgr& rTuning, MINT32 i4SubsampleIdex)
{
    MBOOL bMOBC_EN = isEnable(SubModuleIndex);

#if 0
    //useing case
    rTuning.updateEngine(eTuningMgrFunc_MOBC_R1, bMOBC_R1_EN, i4SubsampleIdex);
    TUNING_MGR_WRITE_BITS_CAM((&rTuning), CAMCTL_R1_CAMCTL_EN, CAMCTL_OBC_R1_EN, bOBC_R1_EN, i4SubsampleIdex);
    ISP_MGR_CTL_EN_P1_T::getInstance(m_eSensorDev).setEnable_OBC_R1(bOBC_R1_EN);
#endif


#if 0
    if(bOBC_R1_EN){
        MUINT32 GAIN_R = rRawIspCamInfo.rRPG.gain_2.bits.RPG_GAIN_R;
        MUINT32 GAIN_B = rRawIspCamInfo.rRPG.gain_1.bits.RPG_GAIN_B;
        MUINT32 GAIN_G = (rRawIspCamInfo.rRPG.gain_2.bits.RPG_GAIN_GR + rRawIspCamInfo.rRPG.gain_1.bits.RPG_GAIN_GB + 1) >> 1;

        reinterpret_cast<ISP_CAM_OBC_R1_GAIN_0_T*>(REG_INFO_VALUE_PTR(CAM_OBC_R1_GAIN_0))->OBC_R1_GAIN_R = GAIN_R;
        reinterpret_cast<ISP_CAM_OBC_R1_GAIN_0_T*>(REG_INFO_VALUE_PTR(CAM_OBC_R1_GAIN_0))->OBC_R1_GAIN_B = GAIN_B;
        reinterpret_cast<ISP_CAM_OBC_R1_GAIN_1_T*>(REG_INFO_VALUE_PTR(CAM_OBC_R1_GAIN_1))->OBC_R1_GAIN_G = GAIN_G;

        MUINT32 IVGN_R = ((1<<19)/(GAIN_R+1))>>1;
        MUINT32 IVGN_B = ((1<<19)/(GAIN_B+1))>>1;
        MUINT32 IVGN_G = ((1<<19)/(GAIN_G+1))>>1;

        reinterpret_cast<ISP_CAM_OBC_R1_GAIN_0_T*>(REG_INFO_VALUE_PTR(CAM_OBC_R1_IVGN_0))->OBC_R1_GAIN_R = IVGN_R;
        reinterpret_cast<ISP_CAM_OBC_R1_GAIN_0_T*>(REG_INFO_VALUE_PTR(CAM_OBC_R1_IVGN_0))->OBC_R1_GAIN_B = IVGN_B;
        reinterpret_cast<ISP_CAM_OBC_R1_GAIN_1_T*>(REG_INFO_VALUE_PTR(CAM_OBC_R1_IVGN_1))->OBC_R1_GAIN_G = IVGN_G;


        // Register setting
        rTuning.tuningMgrWriteRegs(static_cast<TUNING_MGR_REG_IO_STRUCT*>(m_pRegInfo), m_u4RegInfoNum, i4SubsampleIdex);
    }

    // Register setting
    rTuning.tuningMgrWriteRegs(static_cast<TUNING_MGR_REG_IO_STRUCT*>(m_pRegInfo[SubModuleIndex]), m_u4RegInfoNum, i4SubsampleIdex);

    //using case
    dumpRegInfoP1("OBC_R1");
#endif

    return  MTRUE;
}

}
