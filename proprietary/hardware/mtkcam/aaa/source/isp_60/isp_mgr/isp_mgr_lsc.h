/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#ifndef _ISP_MGR_LSC_H_
#define _ISP_MGR_LSC_H_

#include <lsc/ILscBuf.h>
#include <lsc/ILscTbl.h>

/* Dynamic Bin */
#include <mtkcam/drv/iopipe/CamIO/Cam_Notify.h>

namespace NSIspTuningv3
{
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  LSC
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
typedef class ISP_MGR_LSC : public ISP_MGR_BASE_T
{
    typedef ISP_MGR_LSC    MyType;
private:
    MBOOL m_fgOnOff;
    MINT32 m_FD;

    enum
    {
        ERegInfo_LSCI_R1_LSCI_BASE_ADDR,
        ERegInfo_LSCI_R1_LSCI_OFST_ADDR,
        ERegInfo_LSCI_R1_LSCI_XSIZE,
        ERegInfo_LSCI_R1_LSCI_YSIZE,
        ERegInfo_LSCI_R1_LSCI_STRIDE,
        ERegInfo_LSC_R1_LSC_START,
        ERegInfo_LSC_R1_LSC_CTL1 = ERegInfo_LSC_R1_LSC_START,
        ERegInfo_LSC_R1_LSC_CTL2,
        ERegInfo_LSC_R1_LSC_CTL3,
        ERegInfo_LSC_R1_LSC_LBLOCK,
        ERegInfo_LSC_R1_LSC_RATIO_0,
        ERegInfo_LSC_R1_LSC_GAIN_TH,
        ERegInfo_LSC_R1_LSC_RATIO_1,
        ERegInfo_NUM
    };

    RegInfo_T m_rIspRegInfo[ERegInfo_NUM];

    //dynamic bin
    MBOOL m_isBin;
    NSIspTuning::ILscTbl::Config m_rawConfig;
    NSIspTuning::ILscTbl::Config m_binConfig;

protected:
    ISP_MGR_LSC(ESensorDev_T const eSensorDev)
        : ISP_MGR_BASE_T(m_rIspRegInfo, ERegInfo_NUM, eSensorDev)
        , m_fgOnOff(MFALSE)
        , m_FD(0)
        , m_isBin(MFALSE)
    {
        ::memset(m_rIspRegInfo, 0, sizeof(RegInfo_T)*ERegInfo_NUM);
        INIT_REG_INFO_ADDR_P1(LSCI_R1_LSCI_BASE_ADDR);
        INIT_REG_INFO_ADDR_P1(LSCI_R1_LSCI_OFST_ADDR);
        INIT_REG_INFO_ADDR_P1(LSCI_R1_LSCI_XSIZE);
        INIT_REG_INFO_ADDR_P1(LSCI_R1_LSCI_YSIZE);
        INIT_REG_INFO_ADDR_P1(LSCI_R1_LSCI_STRIDE);
        INIT_REG_INFO_ADDR_P1(LSC_R1_LSC_CTL1);
        INIT_REG_INFO_ADDR_P1(LSC_R1_LSC_CTL2);
        INIT_REG_INFO_ADDR_P1(LSC_R1_LSC_CTL3);
        INIT_REG_INFO_ADDR_P1(LSC_R1_LSC_LBLOCK);
        INIT_REG_INFO_ADDR_P1(LSC_R1_LSC_RATIO_0);
        INIT_REG_INFO_ADDR_P1(LSC_R1_LSC_GAIN_TH);
        INIT_REG_INFO_ADDR_P1(LSC_R1_LSC_RATIO_1);
    }

    virtual ~ISP_MGR_LSC() {}
public:
    static MyType&  getInstance(ESensorDev_T const eSensorDev);

public: // Interfaces.

    template <class ISP_xxx_T>
    MyType& put(ISP_xxx_T const& rParam);

    template <class ISP_xxx_T>
    MyType& get(ISP_xxx_T & rParam);

    MyType& putRatio(MUINT32 u4Ratio);
    MyType& putAddr(MUINT32 u4BaseAddr);
    MUINT32 getAddr();

    // override to do nothing
    MBOOL reset();

    MBOOL putBuf(NSIspTuning::ILscBuf& rBuf);
    MBOOL putBufAndRatio(NSIspTuning::ILscBuf& rBuf, MUINT32 ratio);

    MBOOL isBin();
    MBOOL setIsBin(MBOOL isBin);
    MBOOL setLSCconfigParam(NSIspTuning::ILscTbl::Config &rRawConfig, NSIspTuning::ILscTbl::Config &rBinConfig);
    NSIspTuning::ILscTbl::Config getRawConfig();
    NSIspTuning::ILscTbl::Config getBinConfig();
    MBOOL reconfig(MVOID* rDBinInfo, MVOID* rOutRegCfg);

    MBOOL apply(RAWIspCamInfo& rRawIspCamInfo, TuningMgr& rTuning, MINT32 i4SubsampleIdex=0);
    //MBOOL apply(EIspProfile_T eIspProfile, ESensorTG_T const eSensorTG, cam_reg_t* pReg);
    MVOID enableLsc(MBOOL enable);
    MBOOL isEnable(void);
} ISP_MGR_LSC_T;

template <ESensorDev_T const eSensorDev>
class ISP_MGR_LSC_DEV : public ISP_MGR_LSC_T
{
public:
    static
    ISP_MGR_LSC_DEV&
    getInstance()
    {
        static ISP_MGR_LSC_DEV<eSensorDev> singleton;
        return singleton;
    }
    virtual MVOID destroyInstance() {}

    ISP_MGR_LSC_DEV()
        : ISP_MGR_LSC_T(eSensorDev)
    {}

    virtual ~ISP_MGR_LSC_DEV() {}

};

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  LSC2 for Pass2
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
typedef class ISP_MGR_LSC2 : public ISP_MGR_BASE_T
{
    typedef ISP_MGR_LSC2    MyType;
private:
    MBOOL m_fgOnOff;

    enum
    {
        ERegInfo_IMGCI_D1A_IMGCI_BASE_ADDR,
        ERegInfo_IMGCI_D1A_IMGCI_OFST_ADDR,
        ERegInfo_IMGCI_D1A_IMGCI_XSIZE,
        ERegInfo_IMGCI_D1A_IMGCI_YSIZE,
        ERegInfo_IMGCI_D1A_IMGCI_STRIDE,
        ERegInfo_LSC_D1A_LSC_START,
        ERegInfo_LSC_D1A_LSC_CTL1 = ERegInfo_LSC_D1A_LSC_START,
        ERegInfo_LSC_D1A_LSC_CTL2,
        ERegInfo_LSC_D1A_LSC_CTL3,
        ERegInfo_LSC_D1A_LSC_LBLOCK,
        ERegInfo_LSC_D1A_LSC_RATIO_0,
        ERegInfo_LSC_D1A_LSC_GAIN_TH,
        ERegInfo_LSC_D1A_LSC_RATIO_1,
        ERegInfo_NUM
    };

    RegInfo_T m_rIspRegInfo[ERegInfo_NUM];

protected:
    ISP_MGR_LSC2(ESensorDev_T const eSensorDev)
        : ISP_MGR_BASE_T(m_rIspRegInfo, ERegInfo_NUM, eSensorDev)
        , m_fgOnOff(MFALSE)
    {
        ::memset(m_rIspRegInfo, 0, sizeof(RegInfo_T)*ERegInfo_NUM);
        INIT_REG_INFO_ADDR_P2(IMGCI_D1A_IMGCI_BASE_ADDR);
        INIT_REG_INFO_ADDR_P2(IMGCI_D1A_IMGCI_OFST_ADDR);
        INIT_REG_INFO_ADDR_P2(IMGCI_D1A_IMGCI_XSIZE);
        INIT_REG_INFO_ADDR_P2(IMGCI_D1A_IMGCI_YSIZE);
        INIT_REG_INFO_ADDR_P2(IMGCI_D1A_IMGCI_STRIDE);
        //INIT_REG_INFO_ADDR_P2(LSC_D1A_LSC_START);
        //Chooo no this register
        INIT_REG_INFO_ADDR_P2(LSC_D1A_LSC_CTL1);
        INIT_REG_INFO_ADDR_P2(LSC_D1A_LSC_CTL2);
        INIT_REG_INFO_ADDR_P2(LSC_D1A_LSC_CTL3);
        INIT_REG_INFO_ADDR_P2(LSC_D1A_LSC_LBLOCK);
        INIT_REG_INFO_ADDR_P2(LSC_D1A_LSC_RATIO_0);
        INIT_REG_INFO_ADDR_P2(LSC_D1A_LSC_GAIN_TH);
        INIT_REG_INFO_ADDR_P2(LSC_D1A_LSC_RATIO_1);
    }

    virtual ~ISP_MGR_LSC2() {}
public:
    static MyType&  getInstance(ESensorDev_T const eSensorDev);

public: // Interfaces.

    template <class ISP_xxx_T>
    MyType& put(ISP_xxx_T const& rParam);

    template <class ISP_xxx_T>
    MyType& get(ISP_xxx_T & rParam);

    MyType& putAddr(MUINT32 u4BaseAddr);
    MUINT32 getAddr();

    // override to do nothing
    MBOOL reset();

    MBOOL putBuf(NSIspTuning::ILscBuf& rBuf);

    MBOOL apply(const RAWIspCamInfo& rRawIspCamInfo, dip_x_reg_t* pReg);
    MVOID enableLsc(MBOOL enable);
    MBOOL isEnable(void);
} ISP_MGR_LSC2_T;

template <ESensorDev_T const eSensorDev>
class ISP_MGR_LSC2_DEV : public ISP_MGR_LSC2_T
{
public:
    static
    ISP_MGR_LSC2_DEV&
    getInstance()
    {
        static ISP_MGR_LSC2_DEV<eSensorDev> singleton;
        return singleton;
    }
    virtual MVOID destroyInstance() {}

    ISP_MGR_LSC2_DEV()
        : ISP_MGR_LSC2_T(eSensorDev)
    {}

    virtual ~ISP_MGR_LSC2_DEV() {}

};


};
#endif

