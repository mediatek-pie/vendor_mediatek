/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#define LOG_TAG "isp_mgr_ltm"

#ifndef ENABLE_MY_LOG
    #define ENABLE_MY_LOG       (1)
#endif

#include <cutils/properties.h>
#include <aaa_types.h>
#include <aaa_error_code.h>
#include <mtkcam/utils/std/Log.h>
#include "isp_mgr.h"

namespace NSIspTuningv3
{
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  LTMS
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ISP_MGR_LTMS_T&
ISP_MGR_LTMS_T::
getInstance(ESensorDev_T const eSensorDev)
{
    switch (eSensorDev)
    {
    case ESensorDev_Main: //  Main Sensor
        return  ISP_MGR_LTMS_DEV<ESensorDev_Main>::getInstance();
    case ESensorDev_MainSecond: //  Main Second Sensor
        return  ISP_MGR_LTMS_DEV<ESensorDev_MainSecond>::getInstance();
    case ESensorDev_Sub: //  Sub Sensor
        return  ISP_MGR_LTMS_DEV<ESensorDev_Sub>::getInstance();
    case ESensorDev_SubSecond: //  Main Second Sensor
        return  ISP_MGR_LTMS_DEV<ESensorDev_SubSecond>::getInstance();
    default:
        CAM_LOGE("eSensorDev(%d)", eSensorDev);
        return  ISP_MGR_LTMS_DEV<ESensorDev_Main>::getInstance();
    }
}

template <>
ISP_MGR_LTMS_T&
ISP_MGR_LTMS_T::
put(MUINT8 SubModuleIndex, ISP_NVRAM_LTMS_T const& rParam)
{
    PUT_REG_INFO_MULTI(SubModuleIndex, CTRL,            ctrl);
    PUT_REG_INFO_MULTI(SubModuleIndex, BLK_NUM,         blk_num);
    PUT_REG_INFO_MULTI(SubModuleIndex, BLK_SZ,          blk_sz);
    PUT_REG_INFO_MULTI(SubModuleIndex, BLK_AREA,        blk_area);
    PUT_REG_INFO_MULTI(SubModuleIndex, DETAIL,          detail);
    PUT_REG_INFO_MULTI(SubModuleIndex, HIST,            hist);
    PUT_REG_INFO_MULTI(SubModuleIndex, FLTLINE,         fltline);
    PUT_REG_INFO_MULTI(SubModuleIndex, FLTBLK,          fltblk);
    PUT_REG_INFO_MULTI(SubModuleIndex, CLIP,            clip);
    PUT_REG_INFO_MULTI(SubModuleIndex, MAX_DIV,         max_div);
    PUT_REG_INFO_MULTI(SubModuleIndex, CFG,             cfg);
    PUT_REG_INFO_MULTI(SubModuleIndex, RESET,           reset);
    PUT_REG_INFO_MULTI(SubModuleIndex, INTEN,           inten);
    PUT_REG_INFO_MULTI(SubModuleIndex, INTSTA,          intsta);
    PUT_REG_INFO_MULTI(SubModuleIndex, STATUS,          status);
    PUT_REG_INFO_MULTI(SubModuleIndex, INPUT_COUNT,     input_count);
    PUT_REG_INFO_MULTI(SubModuleIndex, OUTPUT_COUNT,    output_count);
    PUT_REG_INFO_MULTI(SubModuleIndex, CHKSUM,          chksum);
    PUT_REG_INFO_MULTI(SubModuleIndex, IN_SIZE,         in_size);
    PUT_REG_INFO_MULTI(SubModuleIndex, OUT_SIZE,        out_size);
    PUT_REG_INFO_MULTI(SubModuleIndex, ACT_WINDOW_X,    act_window_x);
    PUT_REG_INFO_MULTI(SubModuleIndex, ACT_WINDOW_Y,    act_window_y);
    PUT_REG_INFO_MULTI(SubModuleIndex, OUT_DATA_NUM,    out_data_num);
    PUT_REG_INFO_MULTI(SubModuleIndex, DUMMY_REG,       dummy_reg);
    PUT_REG_INFO_MULTI(SubModuleIndex, SRAM_CFG,        sram_cfg);
    PUT_REG_INFO_MULTI(SubModuleIndex, ATPG,            atpg);
    PUT_REG_INFO_MULTI(SubModuleIndex, SHADOW_CTRL,     shadow_ctrl);
    PUT_REG_INFO_MULTI(SubModuleIndex, HIST_R,          hist_r);
    PUT_REG_INFO_MULTI(SubModuleIndex, HIST_B,          hist_b);
    PUT_REG_INFO_MULTI(SubModuleIndex, HIST_C,          hist_c);
    PUT_REG_INFO_MULTI(SubModuleIndex, FLATLINE_R,      flatline_r);
    PUT_REG_INFO_MULTI(SubModuleIndex, FLATBLK_B,       flatblk_b);
    PUT_REG_INFO_MULTI(SubModuleIndex, BLK_R_AREA,      blk_r_area);
    PUT_REG_INFO_MULTI(SubModuleIndex, BLK_B_AREA,      blk_b_area);
    PUT_REG_INFO_MULTI(SubModuleIndex, BLK_C_AREA,      blk_c_area);

    return  (*this);
}


template <>
ISP_MGR_LTMS_T&
ISP_MGR_LTMS_T::
get(MUINT8 SubModuleIndex, ISP_NVRAM_LTMS_T & rParam)
{
    GET_REG_INFO_MULTI(SubModuleIndex, CTRL,            ctrl);
    GET_REG_INFO_MULTI(SubModuleIndex, BLK_NUM,         blk_num);
    GET_REG_INFO_MULTI(SubModuleIndex, BLK_SZ,          blk_sz);
    GET_REG_INFO_MULTI(SubModuleIndex, BLK_AREA,        blk_area);
    GET_REG_INFO_MULTI(SubModuleIndex, DETAIL,          detail);
    GET_REG_INFO_MULTI(SubModuleIndex, HIST,            hist);
    GET_REG_INFO_MULTI(SubModuleIndex, FLTLINE,         fltline);
    GET_REG_INFO_MULTI(SubModuleIndex, FLTBLK,          fltblk);
    GET_REG_INFO_MULTI(SubModuleIndex, CLIP,            clip);
    GET_REG_INFO_MULTI(SubModuleIndex, MAX_DIV,         max_div);
    GET_REG_INFO_MULTI(SubModuleIndex, CFG,             cfg);
    GET_REG_INFO_MULTI(SubModuleIndex, RESET,           reset);
    GET_REG_INFO_MULTI(SubModuleIndex, INTEN,           inten);
    GET_REG_INFO_MULTI(SubModuleIndex, INTSTA,          intsta);
    GET_REG_INFO_MULTI(SubModuleIndex, STATUS,          status);
    GET_REG_INFO_MULTI(SubModuleIndex, INPUT_COUNT,     input_count);
    GET_REG_INFO_MULTI(SubModuleIndex, OUTPUT_COUNT,    output_count);
    GET_REG_INFO_MULTI(SubModuleIndex, CHKSUM,          chksum);
    GET_REG_INFO_MULTI(SubModuleIndex, IN_SIZE,         in_size);
    GET_REG_INFO_MULTI(SubModuleIndex, OUT_SIZE,        out_size);
    GET_REG_INFO_MULTI(SubModuleIndex, ACT_WINDOW_X,    act_window_x);
    GET_REG_INFO_MULTI(SubModuleIndex, ACT_WINDOW_Y,    act_window_y);
    GET_REG_INFO_MULTI(SubModuleIndex, OUT_DATA_NUM,    out_data_num);
    GET_REG_INFO_MULTI(SubModuleIndex, DUMMY_REG,       dummy_reg);
    GET_REG_INFO_MULTI(SubModuleIndex, SRAM_CFG,        sram_cfg);
    GET_REG_INFO_MULTI(SubModuleIndex, ATPG,            atpg);
    GET_REG_INFO_MULTI(SubModuleIndex, SHADOW_CTRL,     shadow_ctrl);
    GET_REG_INFO_MULTI(SubModuleIndex, HIST_R,          hist_r);
    GET_REG_INFO_MULTI(SubModuleIndex, HIST_B,          hist_b);
    GET_REG_INFO_MULTI(SubModuleIndex, HIST_C,          hist_c);
    GET_REG_INFO_MULTI(SubModuleIndex, FLATLINE_R,      flatline_r);
    GET_REG_INFO_MULTI(SubModuleIndex, FLATBLK_B,       flatblk_b);
    GET_REG_INFO_MULTI(SubModuleIndex, BLK_R_AREA,      blk_r_area);
    GET_REG_INFO_MULTI(SubModuleIndex, BLK_B_AREA,      blk_b_area);
    GET_REG_INFO_MULTI(SubModuleIndex, BLK_C_AREA,      blk_c_area);

    return  (*this);
}

MBOOL
ISP_MGR_LTMS_T::
apply_P1(MUINT8 SubModuleIndex, const RAWIspCamInfo& rRawIspCamInfo, TuningMgr& rTuning, MINT32 i4SubsampleIdex)
{

    return  MTRUE;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  LTM
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ISP_MGR_LTM_T&
ISP_MGR_LTM_T::
getInstance(ESensorDev_T const eSensorDev)
{
    switch (eSensorDev)
    {
    case ESensorDev_Main: //  Main Sensor
        return  ISP_MGR_LTM_DEV<ESensorDev_Main>::getInstance();
    case ESensorDev_MainSecond: //  Main Second Sensor
        return  ISP_MGR_LTM_DEV<ESensorDev_MainSecond>::getInstance();
    case ESensorDev_Sub: //  Sub Sensor
        return  ISP_MGR_LTM_DEV<ESensorDev_Sub>::getInstance();
    case ESensorDev_SubSecond: //  Main Second Sensor
        return  ISP_MGR_LTM_DEV<ESensorDev_SubSecond>::getInstance();
    default:
        CAM_LOGE("eSensorDev(%d)", eSensorDev);
        return  ISP_MGR_LTM_DEV<ESensorDev_Main>::getInstance();
    }
}

template <>
ISP_MGR_LTM_T&
ISP_MGR_LTM_T::
put(MUINT8 SubModuleIndex, ISP_NVRAM_LTM_T const& rParam)
{
    PUT_REG_INFO_MULTI(SubModuleIndex, CTRL,            ctrl);
    PUT_REG_INFO_MULTI(SubModuleIndex, BLK_NUM,         blk_num);
    PUT_REG_INFO_MULTI(SubModuleIndex, BLK_SZ,          blk_sz);
    PUT_REG_INFO_MULTI(SubModuleIndex, BLK_DIVX,        blk_divx);
    PUT_REG_INFO_MULTI(SubModuleIndex, BLK_DIVY,        blk_divy);
    PUT_REG_INFO_MULTI(SubModuleIndex, MAX_DIV,         max_div);
    PUT_REG_INFO_MULTI(SubModuleIndex, CLIP,            clip);
    PUT_REG_INFO_MULTI(SubModuleIndex, TILE_NUM,        tile_num);
    PUT_REG_INFO_MULTI(SubModuleIndex, TILE_CNTX,       tile_cntx);
    PUT_REG_INFO_MULTI(SubModuleIndex, TILE_CNTY,       tile_cnty);
    PUT_REG_INFO_MULTI(SubModuleIndex, CFG,             cfg);
    PUT_REG_INFO_MULTI(SubModuleIndex, RESET,           reset);
    PUT_REG_INFO_MULTI(SubModuleIndex, INTEN,           inten);
    PUT_REG_INFO_MULTI(SubModuleIndex, INTSTA,          intsta);
    PUT_REG_INFO_MULTI(SubModuleIndex, STATUS,          status);
    PUT_REG_INFO_MULTI(SubModuleIndex, INPUT_COUNT,     input_count);
    PUT_REG_INFO_MULTI(SubModuleIndex, OUTPUT_COUNT,    output_count);
    PUT_REG_INFO_MULTI(SubModuleIndex, CHKSUM,          chksum);
    PUT_REG_INFO_MULTI(SubModuleIndex, TILE_SIZE,       tile_size);
    PUT_REG_INFO_MULTI(SubModuleIndex, TILE_EDGE,       tile_edge);
    PUT_REG_INFO_MULTI(SubModuleIndex, TILE_CROP,       tile_crop);
    PUT_REG_INFO_MULTI(SubModuleIndex, DUMMY_REG,       dummy_reg);
    PUT_REG_INFO_MULTI(SubModuleIndex, SRAM_CFG,        sram_cfg);
    PUT_REG_INFO_MULTI(SubModuleIndex, SRAM_STATUS,     sram_status);
    PUT_REG_INFO_MULTI(SubModuleIndex, ATPG,            atpg);
    PUT_REG_INFO_MULTI(SubModuleIndex, SHADOW_CTRL,     shadow_ctrl);
    PUT_REG_INFO_MULTI(SubModuleIndex, SELRGB_GRAD0,    selrgb_grad0);
    PUT_REG_INFO_MULTI(SubModuleIndex, SELRGB_GRAD1,    selrgb_grad1);
    PUT_REG_INFO_MULTI(SubModuleIndex, SELRGB_GRAD2,    selrgb_grad2);
    PUT_REG_INFO_MULTI(SubModuleIndex, SELRGB_GRAD3,    selrgb_grad3);
    PUT_REG_INFO_MULTI(SubModuleIndex, SELRGB_TH0,      selrgb_th0);
    PUT_REG_INFO_MULTI(SubModuleIndex, SELRGB_TH1,      selrgb_th1);
    PUT_REG_INFO_MULTI(SubModuleIndex, SELRGB_TH2,      selrgb_th2);
    PUT_REG_INFO_MULTI(SubModuleIndex, SELRGB_TH3,      selrgb_th3);
    PUT_REG_INFO_MULTI(SubModuleIndex, SELRGB_SLP0,     selrgb_slp0);
    PUT_REG_INFO_MULTI(SubModuleIndex, SELRGB_SLP1,     selrgb_slp1);
    PUT_REG_INFO_MULTI(SubModuleIndex, SELRGB_SLP2,     selrgb_slp2);
    PUT_REG_INFO_MULTI(SubModuleIndex, SELRGB_SLP3,     selrgb_slp3);
    PUT_REG_INFO_MULTI(SubModuleIndex, SELRGB_SLP4,     selrgb_slp4);
    PUT_REG_INFO_MULTI(SubModuleIndex, SELRGB_SLP5,     selrgb_slp5);
    PUT_REG_INFO_MULTI(SubModuleIndex, SELRGB_SLP6,     selrgb_slp6);
    PUT_REG_INFO_MULTI(SubModuleIndex, OUT_STR,         out_str);
    PUT_REG_INFO_MULTI(SubModuleIndex, SRAM_PINGPONG,   sram_pingpong);

    return  (*this);
}


template <>
ISP_MGR_LTM_T&
ISP_MGR_LTM_T::
get(MUINT8 SubModuleIndex, ISP_NVRAM_LTM_T & rParam)
{
    GET_REG_INFO_MULTI(SubModuleIndex, CTRL,            ctrl);
    GET_REG_INFO_MULTI(SubModuleIndex, BLK_NUM,         blk_num);
    GET_REG_INFO_MULTI(SubModuleIndex, BLK_SZ,          blk_sz);
    GET_REG_INFO_MULTI(SubModuleIndex, BLK_DIVX,        blk_divx);
    GET_REG_INFO_MULTI(SubModuleIndex, BLK_DIVY,        blk_divy);
    GET_REG_INFO_MULTI(SubModuleIndex, MAX_DIV,         max_div);
    GET_REG_INFO_MULTI(SubModuleIndex, CLIP,            clip);
    GET_REG_INFO_MULTI(SubModuleIndex, TILE_NUM,        tile_num);
    GET_REG_INFO_MULTI(SubModuleIndex, TILE_CNTX,       tile_cntx);
    GET_REG_INFO_MULTI(SubModuleIndex, TILE_CNTY,       tile_cnty);
    GET_REG_INFO_MULTI(SubModuleIndex, CFG,             cfg);
    GET_REG_INFO_MULTI(SubModuleIndex, RESET,           reset);
    GET_REG_INFO_MULTI(SubModuleIndex, INTEN,           inten);
    GET_REG_INFO_MULTI(SubModuleIndex, INTSTA,          intsta);
    GET_REG_INFO_MULTI(SubModuleIndex, STATUS,          status);
    GET_REG_INFO_MULTI(SubModuleIndex, INPUT_COUNT,     input_count);
    GET_REG_INFO_MULTI(SubModuleIndex, OUTPUT_COUNT,    output_count);
    GET_REG_INFO_MULTI(SubModuleIndex, CHKSUM,          chksum);
    GET_REG_INFO_MULTI(SubModuleIndex, TILE_SIZE,       tile_size);
    GET_REG_INFO_MULTI(SubModuleIndex, TILE_EDGE,       tile_edge);
    GET_REG_INFO_MULTI(SubModuleIndex, TILE_CROP,       tile_crop);
    GET_REG_INFO_MULTI(SubModuleIndex, DUMMY_REG,       dummy_reg);
    GET_REG_INFO_MULTI(SubModuleIndex, SRAM_CFG,        sram_cfg);
    GET_REG_INFO_MULTI(SubModuleIndex, SRAM_STATUS,     sram_status);
    GET_REG_INFO_MULTI(SubModuleIndex, ATPG,            atpg);
    GET_REG_INFO_MULTI(SubModuleIndex, SHADOW_CTRL,     shadow_ctrl);
    GET_REG_INFO_MULTI(SubModuleIndex, SELRGB_GRAD0,    selrgb_grad0);
    GET_REG_INFO_MULTI(SubModuleIndex, SELRGB_GRAD1,    selrgb_grad1);
    GET_REG_INFO_MULTI(SubModuleIndex, SELRGB_GRAD2,    selrgb_grad2);
    GET_REG_INFO_MULTI(SubModuleIndex, SELRGB_GRAD3,    selrgb_grad3);
    GET_REG_INFO_MULTI(SubModuleIndex, SELRGB_TH0,      selrgb_th0);
    GET_REG_INFO_MULTI(SubModuleIndex, SELRGB_TH1,      selrgb_th1);
    GET_REG_INFO_MULTI(SubModuleIndex, SELRGB_TH2,      selrgb_th2);
    GET_REG_INFO_MULTI(SubModuleIndex, SELRGB_TH3,      selrgb_th3);
    GET_REG_INFO_MULTI(SubModuleIndex, SELRGB_SLP0,     selrgb_slp0);
    GET_REG_INFO_MULTI(SubModuleIndex, SELRGB_SLP1,     selrgb_slp1);
    GET_REG_INFO_MULTI(SubModuleIndex, SELRGB_SLP2,     selrgb_slp2);
    GET_REG_INFO_MULTI(SubModuleIndex, SELRGB_SLP3,     selrgb_slp3);
    GET_REG_INFO_MULTI(SubModuleIndex, SELRGB_SLP4,     selrgb_slp4);
    GET_REG_INFO_MULTI(SubModuleIndex, SELRGB_SLP5,     selrgb_slp5);
    GET_REG_INFO_MULTI(SubModuleIndex, SELRGB_SLP6,     selrgb_slp6);
    GET_REG_INFO_MULTI(SubModuleIndex, OUT_STR,         out_str);
    GET_REG_INFO_MULTI(SubModuleIndex, SRAM_PINGPONG,   sram_pingpong);

    return  (*this);
}

MBOOL
ISP_MGR_LTM_T::
apply_P1(MUINT8 SubModuleIndex, const RAWIspCamInfo& rRawIspCamInfo, TuningMgr& rTuning, MINT32 i4SubsampleIdex)
{

    return  MTRUE;
}

MBOOL
ISP_MGR_LTM_T::
apply_P2(MUINT8 SubModuleIndex, const RAWIspCamInfo& rRawIspCamInfo, dip_x_reg_t* pReg)
{

    return  MTRUE;
}



}

