/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#define LOG_TAG "isp_mgr_hlr"

#ifndef ENABLE_MY_LOG
    #define ENABLE_MY_LOG       (1)
#endif

#include <cutils/properties.h>
#include <aaa_types.h>
#include <aaa_error_code.h>
#include <mtkcam/utils/std/Log.h>
#include "isp_mgr.h"

namespace NSIspTuningv3
{
#define CLAMP(x,min,max)       (((x) > (max)) ? (max) : (((x) < (min)) ? (min) : (x)))

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  HLR
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ISP_MGR_HLR_T&
ISP_MGR_HLR_T::
getInstance(ESensorDev_T const eSensorDev)
{
    switch (eSensorDev)
    {
    case ESensorDev_Main: //  Main Sensor
        return  ISP_MGR_HLR_DEV<ESensorDev_Main>::getInstance();
    case ESensorDev_MainSecond: //  Main Second Sensor
        return  ISP_MGR_HLR_DEV<ESensorDev_MainSecond>::getInstance();
    case ESensorDev_Sub: //  Sub Sensor
        return  ISP_MGR_HLR_DEV<ESensorDev_Sub>::getInstance();
    case ESensorDev_SubSecond: //  Main Second Sensor
        return  ISP_MGR_HLR_DEV<ESensorDev_SubSecond>::getInstance();
    default:
        CAM_LOGE("eSensorDev(%d)", eSensorDev);
        return  ISP_MGR_HLR_DEV<ESensorDev_Main>::getInstance();
    }
}

template <>
ISP_MGR_HLR_T&
ISP_MGR_HLR_T::
put(MUINT8 SubModuleIndex, ISP_NVRAM_HLR_T const& rParam)
{
    PUT_REG_INFO_MULTI(SubModuleIndex, EST_Y0,      est_y0);
    PUT_REG_INFO_MULTI(SubModuleIndex, EST_Y1,      est_y1);
    PUT_REG_INFO_MULTI(SubModuleIndex, EST_Y2,      est_y2);
    PUT_REG_INFO_MULTI(SubModuleIndex, EST_Y3,      est_y3);
    PUT_REG_INFO_MULTI(SubModuleIndex, EST_X0,      est_x0);
    PUT_REG_INFO_MULTI(SubModuleIndex, EST_X1,      est_x1);
    PUT_REG_INFO_MULTI(SubModuleIndex, EST_X2,      est_x2);
    PUT_REG_INFO_MULTI(SubModuleIndex, EST_X3,      est_x3);
    PUT_REG_INFO_MULTI(SubModuleIndex, EST_S0,      est_s0);
    PUT_REG_INFO_MULTI(SubModuleIndex, EST_S1,      est_s1);
    PUT_REG_INFO_MULTI(SubModuleIndex, EST_S2,      est_s2);
    PUT_REG_INFO_MULTI(SubModuleIndex, LMG,         lmg);
    PUT_REG_INFO_MULTI(SubModuleIndex, PRT,         prt);
    PUT_REG_INFO_MULTI(SubModuleIndex, CLP,         clp);
    PUT_REG_INFO_MULTI(SubModuleIndex, EFCT,        efct);
    PUT_REG_INFO_MULTI(SubModuleIndex, CTL,         ctl);
    PUT_REG_INFO_MULTI(SubModuleIndex, CTL2,        ctl2);

    return  (*this);
}


template <>
ISP_MGR_HLR_T&
ISP_MGR_HLR_T::
get(MUINT8 SubModuleIndex, ISP_NVRAM_HLR_T & rParam)
{
    GET_REG_INFO_MULTI(SubModuleIndex, EST_Y0,      est_y0);
    GET_REG_INFO_MULTI(SubModuleIndex, EST_Y1,      est_y1);
    GET_REG_INFO_MULTI(SubModuleIndex, EST_Y2,      est_y2);
    GET_REG_INFO_MULTI(SubModuleIndex, EST_Y3,      est_y3);
    GET_REG_INFO_MULTI(SubModuleIndex, EST_X0,      est_x0);
    GET_REG_INFO_MULTI(SubModuleIndex, EST_X1,      est_x1);
    GET_REG_INFO_MULTI(SubModuleIndex, EST_X2,      est_x2);
    GET_REG_INFO_MULTI(SubModuleIndex, EST_X3,      est_x3);
    GET_REG_INFO_MULTI(SubModuleIndex, EST_S0,      est_s0);
    GET_REG_INFO_MULTI(SubModuleIndex, EST_S1,      est_s1);
    GET_REG_INFO_MULTI(SubModuleIndex, EST_S2,      est_s2);
    GET_REG_INFO_MULTI(SubModuleIndex, LMG,         lmg);
    GET_REG_INFO_MULTI(SubModuleIndex, PRT,         prt);
    GET_REG_INFO_MULTI(SubModuleIndex, CLP,         clp);
    GET_REG_INFO_MULTI(SubModuleIndex, EFCT,        efct);
    GET_REG_INFO_MULTI(SubModuleIndex, CTL,         ctl);
    GET_REG_INFO_MULTI(SubModuleIndex, CTL2,        ctl2);

    return  (*this);
}

MBOOL
ISP_MGR_HLR_T::
apply_P1(MUINT8 SubModuleIndex, const RAWIspCamInfo& rRawIspCamInfo, TuningMgr& rTuning, MINT32 i4SubsampleIdex)
{

    return  MTRUE;
}

MBOOL
ISP_MGR_HLR_T::
apply_P2(MUINT8 SubModuleIndex, const RAWIspCamInfo& rRawIspCamInfo, dip_x_reg_t* pReg)
{

    return  MTRUE;
}




}

