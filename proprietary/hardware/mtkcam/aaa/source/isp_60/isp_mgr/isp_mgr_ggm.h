/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#ifndef _ISP_MGR_GGM_H_
#define _ISP_MGR_GGM_H_

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  GGM
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#define INIT_GGM_ADDR(reg)\
    INIT_REG_INFO_ADDR_P1_MULTI(EGGM_R1 ,reg, GGM_R1_GGM_);\
    INIT_REG_INFO_ADDR_P1_MULTI(EGGM_R2 ,reg, GGM_R2_GGM_);\
    INIT_REG_INFO_ADDR_P2_MULTI(EGGM_D1 ,reg, GGM_D1A_GGM_);\
    INIT_REG_INFO_ADDR_P2_MULTI(EGGM_D2 ,reg, GGM_D2A_GGM_);\
    INIT_REG_INFO_ADDR_P2_MULTI(EGGM_D3 ,reg, GGM_D3A_GGM_)

typedef class ISP_MGR_GGM : public ISP_MGR_BASE_T
{
    typedef ISP_MGR_GGM    MyType;
private:
    enum
    {
        EGGM_R1, //for Pass1 YUV
        EGGM_R2, //for Pass1 YUV Face
        EGGM_D1, //for Fix
        EGGM_D2, //for Mono
        EGGM_D3, //for Adaptive GGM
        ESubModule_NUM
    };
    MBOOL m_bEnable[ESubModule_NUM];
    MBOOL m_bCCTEnable[ESubModule_NUM];

    enum
    {
        ERegInfo_CTRL,
        //ERegInfo_SRAM_PINGPONG,
        ERegInfo_NUM
    };

    RegInfo_T m_rIspRegInfo[ESubModule_NUM][(ERegInfo_NUM + GGM_LUT_SIZE)];

protected:
    ISP_MGR_GGM(ESensorDev_T const eSensorDev)
        : ISP_MGR_BASE_T(m_rIspRegInfo, ERegInfo_NUM, eSensorDev)
    {
        for(int i=0; i<ESubModule_NUM; i++){
            m_bEnable[i]    = MFALSE;
            m_bCCTEnable[i] = MFALSE;
            ::memset(m_rIspRegInfo[i], 0, sizeof(RegInfo_T)* (ERegInfo_NUM + GGM_LUT_SIZE));
        }
        // register info addr init
        INIT_GGM_ADDR(CTRL);
        //INIT_GGM_ADDR(SRAM_PINGPONG);

        MUINT32 u4StartAddr[ESubModule_NUM];
        u4StartAddr[EGGM_R1]= REG_ADDR_P1(GGM_R1_GGM_LUT[0]);
        u4StartAddr[EGGM_R2]= REG_ADDR_P1(GGM_R2_GGM_LUT[0]);
        u4StartAddr[EGGM_D1]= REG_ADDR_P2(GGM_D1A_GGM_LUT[0]);
        u4StartAddr[EGGM_D2]= REG_ADDR_P2(GGM_D2A_GGM_LUT[0]);
        u4StartAddr[EGGM_D3]= REG_ADDR_P2(GGM_D3A_GGM_LUT[0]);
        for(int i=0; i<ESubModule_NUM; i++){
            reinterpret_cast<REG_GGM_R1_GGM_CTRL*>(REG_INFO_VALUE_PTR_MULTI(i, CTRL))->Bits.GGM_END_VAR = 1023;
            reinterpret_cast<REG_GGM_R1_GGM_CTRL*>(REG_INFO_VALUE_PTR_MULTI(i, CTRL))->Bits.GGM_RMP_VAR = 15;
            for (MINT32 j = 0; j < GGM_LUT_SIZE; j++) {
                m_rIspRegInfo[i][ERegInfo_NUM + j].val = 0 ;
                m_rIspRegInfo[i][ERegInfo_NUM + j].addr = u4StartAddr[i] + 4*i;
            }
        }
    }

    virtual ~ISP_MGR_GGM() {}

public:
    static MyType&  getInstance(ESensorDev_T const eSensorDev);

public: // Interfaces.

    template <class ISP_xxx_T>
    MyType& put(MUINT8 SubModuleIndex, ISP_xxx_T const& rParam);

    template <class ISP_xxx_T>
    MyType& get(MUINT8 SubModuleIndex, ISP_xxx_T & rParam);

    MBOOL
    isEnable(MUINT8 SubModuleIndex)
    {
        return m_bEnable[SubModuleIndex];
    }

    MVOID
    setEnable(MUINT8 SubModuleIndex, MBOOL bEnable)
    {
        m_bEnable[SubModuleIndex] = bEnable;
    }

    MBOOL
    isCCTEnable(MUINT8 SubModuleIndex)
    {
        return m_bCCTEnable[SubModuleIndex];
    }

    MVOID
    setCCTEnable(MUINT8 SubModuleIndex, MBOOL bEnable)
    {
        m_bCCTEnable[SubModuleIndex] = bEnable;
    }

    MBOOL apply_P1(MUINT8 SubModuleIndex, const RAWIspCamInfo& rRawIspCamInfo, TuningMgr& rTuning, MINT32 i4SubsampleIdex=0);

    MBOOL apply_P2(MUINT8 SubModuleIndex, const RAWIspCamInfo& rRawIspCamInfo, dip_x_reg_t* pReg);

} ISP_MGR_GGM_T;

template <ESensorDev_T const eSensorDev>
class ISP_MGR_GGM_DEV : public ISP_MGR_GGM_T
{
public:
    static
    ISP_MGR_GGM_T&
    getInstance()
    {
        static ISP_MGR_GGM_DEV<eSensorDev> singleton;
        return singleton;
    }
    virtual MVOID destroyInstance() {}

    ISP_MGR_GGM_DEV()
        : ISP_MGR_GGM_T(eSensorDev)
    {}

    virtual ~ISP_MGR_GGM_DEV() {}

};


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  IGGM
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#define INIT_IGGM_ADDR(reg)\
    INIT_REG_INFO_ADDR_P2_MULTI(EIGGM_D1 ,reg, IGGM_D1A_IGGM_)

typedef class ISP_MGR_IGGM : public ISP_MGR_BASE_T
{
    typedef ISP_MGR_IGGM    MyType;
private:
    enum
    {
        EIGGM_D1, //for Fix
        ESubModule_NUM
    };
    MBOOL m_bEnable[ESubModule_NUM];
    MBOOL m_bCCTEnable[ESubModule_NUM];

    enum
    {
        ERegInfo_CTRL,
        ERegInfo_NUM
    };

    RegInfo_T m_rIspRegInfo[ESubModule_NUM][ERegInfo_NUM];
    RegInfo_T m_rIspRegInfo_TBL_RG[ESubModule_NUM][IGGM_LUT_SIZE];
    RegInfo_T m_rIspRegInfo_TBL_B[ESubModule_NUM][IGGM_LUT_SIZE];

protected:
    ISP_MGR_IGGM(ESensorDev_T const eSensorDev)
        : ISP_MGR_BASE_T(m_rIspRegInfo, ERegInfo_NUM, eSensorDev)
    {
        for(int i=0; i<ESubModule_NUM; i++){
            m_bEnable[i]    = MFALSE;
            m_bCCTEnable[i] = MFALSE;
            ::memset(m_rIspRegInfo[i], 0, sizeof(RegInfo_T)*ERegInfo_NUM);
        }
        // register info addr init
        INIT_IGGM_ADDR(CTRL);

        MUINT32 u4StartAddr_rg[ESubModule_NUM];
        MUINT32 u4StartAddr_b[ESubModule_NUM];
        u4StartAddr_rg[0]= REG_ADDR_P2(IGGM_D1A_IGGM_LUT_RG[0]);
        u4StartAddr_b[0] = REG_ADDR_P2(IGGM_D1A_IGGM_LUT_B[0]);
        for(int i=0; i<ESubModule_NUM; i++){
            reinterpret_cast<IGGM_REG_D1A_IGGM_CTRL*>(REG_INFO_VALUE_PTR_MULTI(i, CTRL))->Bits.IGGM_END_VAR = 4095;
            for (MINT32 j = 0; j < IGGM_LUT_SIZE; j++) {
                m_rIspRegInfo_TBL_RG[i][j].val  = 0 ;
                m_rIspRegInfo_TBL_RG[i][j].addr = u4StartAddr_rg[i] + 4*i;
                m_rIspRegInfo_TBL_B[i][j].val   = 0 ;
                m_rIspRegInfo_TBL_B[i][j].addr  = u4StartAddr_b[i] + 4*i;
            }
        }
    }

    virtual ~ISP_MGR_IGGM() {}

public:
    static MyType&  getInstance(ESensorDev_T const eSensorDev);

public: // Interfaces.

    template <class ISP_xxx_T>
    MyType& put(MUINT8 SubModuleIndex, ISP_xxx_T const& rParam);

    template <class ISP_xxx_T>
    MyType& get(MUINT8 SubModuleIndex, ISP_xxx_T & rParam);

    MBOOL
    isEnable(MUINT8 SubModuleIndex)
    {
        return m_bEnable[SubModuleIndex];
    }

    MVOID
    setEnable(MUINT8 SubModuleIndex, MBOOL bEnable)
    {
        m_bEnable[SubModuleIndex] = bEnable;
    }

    MBOOL
    isCCTEnable(MUINT8 SubModuleIndex)
    {
        return m_bCCTEnable[SubModuleIndex];
    }

    MVOID
    setCCTEnable(MUINT8 SubModuleIndex, MBOOL bEnable)
    {
        m_bCCTEnable[SubModuleIndex] = bEnable;
    }

    MBOOL apply_P2(MUINT8 SubModuleIndex, const RAWIspCamInfo& rRawIspCamInfo, dip_x_reg_t* pReg);

} ISP_MGR_IGGM_T;

template <ESensorDev_T const eSensorDev>
class ISP_MGR_IGGM_DEV : public ISP_MGR_IGGM_T
{
public:
    static
    ISP_MGR_IGGM_T&
    getInstance()
    {
        static ISP_MGR_IGGM_DEV<eSensorDev> singleton;
        return singleton;
    }
    virtual MVOID destroyInstance() {}

    ISP_MGR_IGGM_DEV()
        : ISP_MGR_IGGM_T(eSensorDev)
    {}

    virtual ~ISP_MGR_IGGM_DEV() {}

};


#endif

