#define LOG_TAG "CctSvrIF"

extern "C" {
#include <linux/fb.h>
#include <linux/kd.h>
#include <linux/mtkfb.h>
}

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <mtkcam/main/acdk/AcdkTypes.h>
//#include "AcdkLog.h"
//#include "AcdkCctBase.h"
//#include "cct_main.h"
//#include "cct_calibration.h"
//#include <mtkcam/drv/IHalSensor.h>
//#include "AcdkErrCode.h"

//#include "cct_if.h"
//#include "cct_imp.h"
//#include <mtkcam/main/acdk/cct/cct_ErrCode.h>

#include <cutils/log.h>
#include <pthread.h>
#include "cct_server.h"
/*
*    @CCT_IF.CPP
*    CCT_IF provides user to do camera calibration or tuning by means of
*    mixing following commands. Which involoved with
*    3A, ISP, sensor, NVRAM, Calibration, ACDK releated.
*
*    CCT FW working model is based on ACDK framework, so it is required make
*    sure AcdkCctBase object is available before CCTIF Open.
*
*    CCT FW presents the following APIs:
*
*    CCTIF_Open
*    CCTIF_Close
*    CCTIF_Init
*    CCTIF_DeInit
*    CCTIF_IOControl
*/


CctServer *gpCctSvrObj = NULL;


#ifdef __cplusplus
extern "C" {
#endif


MBOOL CctSvrIF_Init(MINT32 sensorID)
{
    ALOGD("[%s] Start\n", __FUNCTION__);

    if (gpCctSvrObj == NULL)
        gpCctSvrObj = new CctServer( (CCT_SENSOR_TYPE_T)sensorID );

    ALOGD("[%s] End\n", __FUNCTION__);

    return /*err*/MTRUE;
}


MBOOL CctSvrIF_DeInit()
{
    ALOGD("[%s] Start\n", __FUNCTION__);

    if(gpCctSvrObj) {
    	delete gpCctSvrObj;
    	gpCctSvrObj = NULL;
    } else
    	return MFALSE;

    ALOGD("[%s] End\n", __FUNCTION__);
    return MTRUE;
}


MBOOL CctSvrIF_Ctrl(MUINT32 cmd)
{
    ALOGD("[%s] CctSvrIF cmd = 0x%x\n", __FUNCTION__, cmd);

    if(gpCctSvrObj) {
    		gpCctSvrObj->CCT_ServerCtrl((CCT_SVR_CTL_T)cmd);
    } else {
        ALOGD("[%s] no CctServer object\n", __FUNCTION__);
        return MFALSE;
    }

    ALOGD("[%s] End\n", __FUNCTION__);

    return MTRUE;
}

#ifdef __cplusplus
} // extern "C"
#endif
