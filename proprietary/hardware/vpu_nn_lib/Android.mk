LOCAL_PATH := $(call my-dir)

MY_PLATFORM := $(shell echo $(MTK_PLATFORM) | tr A-Z a-z )
SERVICE_MODULE := android.hardware.neuralnetworks@1.1-service-apunn

include $(CLEAR_VARS)
LOCAL_MODULE := $(SERVICE_MODULE)
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
LOCAL_INIT_RC := $(SERVICE_MODULE).rc
LOCAL_SRC_FILES := $(MY_PLATFORM)/arm64/$(SERVICE_MODULE)
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

LOCAL_SHARED_LIBRARIES := \
	libbase \
	libdl \
	libhardware \
	libhidlbase \
	libhidlmemory \
	libhidltransport \
	libtextclassifier_hash \
	liblog \
	libutils \
	android.hardware.neuralnetworks@1.0 \
	android.hardware.neuralnetworks@1.1 \
	android.hidl.allocator@1.0 \
	android.hidl.memory@1.0 \
	libion \
	libcutils \
	libc++ \
	libion_mtk \
	libvpu \
	libneuropilot_hal_utils \


LOCAL_PREFER_SOURCE_PATH := $(MTK_PATH_SOURCE)/hardware/vpu_nn

include $(MTK_PREBUILT)

