LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/hardware/gpu_ext))
include $(CLEAR_VARS)
LOCAL_MODULE := libged_sys
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_PATH := $(PRODUCT_OUT)/system/lib64
LOCAL_MODULE_SUFFIX := .so
LOCAL_SHARED_LIBRARIES_64 := libbinder libcutils libutils liblog libsync libnativewindow libhidlbase libhidltransport android.hardware.power@1.1 vendor.mediatek.hardware.power@2.0 libc++ libc libm libdl
LOCAL_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)/ged/include
LOCAL_MULTILIB := 64
LOCAL_SRC_FILES_64 := arm64_$(TARGET_ARCH_VARIANT)_$(TARGET_CPU_VARIANT)/libged_sys.so
include $(BUILD_PREBUILT)
endif

ifeq (,$(wildcard vendor/mediatek/proprietary/hardware/gpu_ext))
include $(CLEAR_VARS)
LOCAL_MODULE := libged_sys
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_PATH := $(PRODUCT_OUT)/system/lib
LOCAL_MODULE_SUFFIX := .so
LOCAL_SHARED_LIBRARIES := libbinder libcutils libutils liblog libsync libnativewindow libhidlbase libhidltransport android.hardware.power@1.1 vendor.mediatek.hardware.power@2.0 libc++ libc libm libdl
LOCAL_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)/ged/include
LOCAL_MULTILIB := 32
LOCAL_SRC_FILES_32 := arm_$(TARGET_ARCH_VARIANT)_$(TARGET_CPU_VARIANT)/libged_sys.so
include $(BUILD_PREBUILT)
endif
