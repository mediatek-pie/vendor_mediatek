#ifndef _MTK_ADP_H
#define _MTK_ADP_H

#include "MTKADPType.h"
#include "MTKADPErrCode.h"

typedef enum
{
    ADP_EXE_ENV_UREE = 0, //Android
    ADP_EXE_ENV_MTEE,     //Geniezone
    ADP_EXE_ENV_MAX
}   ADP_EXE_ENV_ENUM;

/*****************************************************************************
Feature Control Enum and Structure
******************************************************************************/
typedef enum
{
    ADP_FEATURE_BEGIN,              // minimum of feature id
    ADP_FEATURE_GET_WORKBUF_SIZE,   // feature id to query buffer size
    ADP_FEATURE_SET_WORKBUF_ADDR,   // feature id to set working buffer address
    ADP_FEATURE_SET_PROC_INFO,      // feature id to set proc info

    ADP_FEATURE_RESET_MEM,
    ADP_FEATURE_TEST_MTEE_MEM,
    ADP_FEATURE_MAX                 // maximum of feature id
}   ADP_FEATURE_ENUM;


typedef enum
{
    ADP_INPUT_IMAGE_DISABLE,
    ADP_INPUT_IMAGE_GRAY,
    ADP_INPUT_IMAGE_YCbCr444,
    ADP_INPUT_IMAGE_YUY2,
}   ADP_INPUT_IMAGE_FORMAT_ENUM;

typedef enum
{
    ADP_ROTATE_0 = 0,
    ADP_ROTATE_COUNTER_CLOCKWISE_90 = 1,
    ADP_ROTATE_CLOCKWISE_90 = 2,
    ADP_ROTATE_180 = 3,
}   ADP_ROTATE_FLAG_ENUM;

typedef enum
{
    ADP_FLIP_HORIZONTAL = 0x1,
    ADP_FLIP_VERTICAL = 0x2,
}   ADP_FLIP_FLAG_ENUM;

#define ADP_PARAM_KEY_MAX_LEN 32
typedef struct ADP_TUNING_PARAM
{
    char szKey[ADP_PARAM_KEY_MAX_LEN];
    MINT32 i4Value;

    ADP_TUNING_PARAM();
}ADP_TUNING_PARAM_T, *P_ADP_TUNING_PARAM_T;

typedef struct ADP_TUNING_INFO
{
    MUINT32 u4NumOfParam;
    ADP_TUNING_PARAM_T* prParams;

    ADP_TUNING_INFO();
} ADP_TUNING_INFO_T, *P_ADP_TUNING_INFO_T;

typedef struct ADP_BUFFER_INFO
{
    //For MTEE
    MBOOL bIsSharedMem;
    MUINT32 u4Handle0;
    MUINT32 u4Handle1;
    MUINT32 u4Handle2;
    MUINT32 u4Handle3;

    //For UREE
    MINT8 *pcBuffer0;
    MUINT32 u4Size0;  //size in bytes
    MINT8 *pcBuffer1;
    MUINT32 u4Size1;  //size in bytes
    MINT8 *pcBuffer2;
    MUINT32 u4Size2;  //size in bytes
    MINT8 *pcBuffer3;
    MUINT32 u4Size3;  //size in bytes

    //For all platform
    MUINT16 u2Width, u2Height;

    ADP_BUFFER_INFO();//initialize all variables to zero.
} ADP_BUFFER_INFO_T, *P_ADP_BUFFER_INFO_T;

typedef struct 
{
    union
    {
        MUINT32 u4ExeEnvMode;
        ADP_EXE_ENV_ENUM eExeEnvMode; //ADP_EXE_ENV_UREE or ADP_EXE_ENV_MTEE
    };

    union
    {
        MUINT32 u4RotateFlag;
        ADP_ROTATE_FLAG_ENUM eRotateFlag; //ADP_ROTATE_0, ADP_ROTATE_COUNTER_CLOCKWISE_90, ADP_ROTATE_CLOCKWISE_90, ADP_ROTATE_180
    };

    MUINT32 u4FlipFlag; // 0: Copy without fliping, 1: ADP_FLIP_HORIZONTAL, 2: ADP_FLIP_VERTICAL, 3: ADP_FLIP_HORIZONTAL|ADP_FLIP_VERTICAL
    MINT32 i4DepthMode; // Main L or R [0]Left [1]Right
    union
    {
        MUINT32 u4InputImgFormat;
        ADP_INPUT_IMAGE_FORMAT_ENUM eInputImgFormat; //Disable, Grey, YUV420
    };

    MUINT16 u2ImgInX;
    MUINT16 u2ImgInY; //Input Color/Gray Img ROI
    MUINT16 u2ImgInW;
    MUINT16 u2ImgInH; //Input Color/Gray Img size

    MUINT16 u2DepthInX;
    MUINT16 u2DepthInY; //Input Disparity map ROI
    MUINT16 u2DepthInW;
    MUINT16 u2DepthInH; //Input Disparity map size
    
    MUINT16 u2DepthOutW;
    MUINT16 u2DepthOutH; //Output Disparity map size, Engine

    ADP_TUNING_INFO_T rTuningInfo;
} ADP_INIT_INFO_T, *P_ADP_INIT_INFO_T;

typedef struct 
{
    ADP_BUFFER_INFO_T rBufferImgIn; //input color/gray img
    ADP_BUFFER_INFO_T rBufferConfIn; //input color/gray img
    ADP_BUFFER_INFO_T rBufferDepthInL; //input left Disparity raw
    ADP_BUFFER_INFO_T rBufferDepthInR; //input right Disparity raw

    ADP_BUFFER_INFO_T rBufferDepthOut; //output Disparity result
} ADP_PROC_INFO_T, *P_ADP_PROC_INFO_T;

typedef struct
{
    MUINT32 u4Size;
} ADP_WORKING_BUFFER_INFO_T, *P_ADP_WORKING_BUFFER_INFO_T;

typedef struct ADP_WORKING_BUFFER_ADDR
{
    MUINT32 u4Size;
    MINT8 *pcBuffer;

    ADP_WORKING_BUFFER_ADDR();
} ADP_WORKING_BUFFER_ADDR_T, *P_ADP_WORKING_BUFFER_ADDR_T;


typedef struct ADP_UTIL_MEM_RESET
{
    
    MINT8 *pcBuffer; 
    MUINT32 u4Handle;

    //Only support YUY2 and Gray format.
    //ADP_INPUT_IMAGE_GRAY buffer size : u4Width*u4Height.
    //ADP_INPUT_IMAGE_YUY2 buffer size : u4Width*2*u4Height.
    MUINT32 u4InputImgFormat;

    MUINT32 u4Width, u4Height;

    MUINT16 u2BorderU;
    MUINT16 u2BorderD;
    MUINT16 u2BorderL;
    MUINT16 u2BorderR;

    MUINT8 ucResetValue;
    MUINT8 ucBorderValue;

    MBOOL bBorderOnly;

    ADP_UTIL_MEM_RESET();
} ADP_UTIL_MEM_RESET_T, *P_ADP_UTIL_MEM_RESET_T;

/*******************************************************************************
*
********************************************************************************/
class MTKADP {
public:
    static MTKADP* createInstance();
    virtual void   destroyInstance() = 0;

    virtual ~MTKADP() {}
    // Process Control
    virtual MRESULT Init(ADP_INIT_INFO_T* pInitInData); //return E_ADP_MTEE_IS_DISABLED if can't find the MTEE TA in MTEE mode
    virtual MRESULT Main(void);    // START
    virtual MRESULT Reset(void);                         // RESET

    // Feature Control
    //          FeatureID             |           pParaIn             |           pParaOut
    //  ADP_FEATURE_GET_WORKBUF_SIZE  |  NULL                         |  P_ADP_WORKING_BUFFER_INFO_T    
    //  ADP_FEATURE_SET_WORKBUF_ADDR  |  P_ADP_WORKING_BUFFER_ADDR_T  |  NULL                 
    //  ADP_FEATURE_SET_PROC_INFO     |  P_ADP_PROC_INFO_T            |  NULL                 
    //  ADP_FEATURE_RESET_MEM         |  P_ADP_UTIL_MEM_RESET_T       |  NULL   (return E_ADP_MTEE_IS_DISABLED if can't find the MTEE TA.)
    virtual MRESULT FeatureCtrl(ADP_FEATURE_ENUM FeatureID, void* pParaIn, void* pParaOut);
private:

};

#endif